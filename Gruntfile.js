module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),

		// You need to specify username and password in an .ftppass file for the ftpush to work!!
		ftpush: {
		  afk: {
			auth: {
			  host: 'test.afk-filmkreis.de',
			  authKey: 'afk'
			},
			src: 'afk-filmkreis.de/public_html/protected/',
			dest: '/public_html/protected/',
			simple: true
		  }
		}
	});

	// Load all files starting with `grunt-`
	require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);

	// Tell Grunt what to do when we type "grunt" into the terminal
	grunt.registerTask('default', ['ftpush:afk']);

};

