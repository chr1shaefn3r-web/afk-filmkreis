<?php

// this contains the application parameters that can be maintained via GUI
return array(
// this is displayed in the header section
	'title'=>'My Yii Blog',
// this is used in error pages
	'adminEmail'=>'webmaster@example.com',
// number of posts displayed per page
	'postsPerPage'=>10,
// the copyright information displayed in the footer section
	'copyrightInfo'=>'Copyright &copy; 2009 by My Company.',
	'reCaptcha' => array(
		'publicKey'=> '6LcYYcgSAAAAABIaxw52YE7GKCanxqnvnTbrjXcy',
		'privateKey'=>'6LcYYcgSAAAAANMdMMXFaC61Vk4V4qrWEsjJa82O',
	),
	
	'baseurls' => array(
		'img'=> '/files/img/',
	),
);
