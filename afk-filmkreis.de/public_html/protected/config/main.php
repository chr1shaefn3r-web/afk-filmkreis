<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Akademischer Filmkreis Karlsruhe e.V.',

// preloading 'log' component
	'preload'=>array('log'),

// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.extensions.yiidebugtb.*', //our extension
),

	'defaultController'=>'post',
	'language' =>'de_DE',

    'modules'=>array(
        'gii'=>array(
            'class'=>'system.gii.GiiModule',
            'password'=>'dsfuihsdfi',
		'ipFilters'=>array('*'),
// 'newFileMode'=>0666,
// 'newDirMode'=>0777,
),
),
// application components
	'components'=>array(
/*
 'user'=>array(
 // enable cookie-based authentication
 'allowAutoLogin'=>true,
 ),
 */
/*
 'db'=>array(
 'connectionString' => 'sqlite:protected/data/blog.db',
 'tablePrefix' => 'tbl_',
 ),
 */
// uncomment the following to use a MySQL database

		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=afk_test',
			'emulatePrepare' => true,
			'username' => 'afk_test',
			'password' => 'TzUpchBZE8wJTFED',
			'charset' => 'utf8',
			'tablePrefix' => 'tbl_',
),
		'cache' => array(
			'class'=>'system.caching.CDbCache'
			),

		'errorHandler'=>array(
            'errorAction'=>'site/error',
			),
        'urlManager'=>array(
        	'urlFormat'=>'path',
 			'showScriptName' => true,
        	'rules'=>array(
        		'post/<id:\d+>/<title:.*?>'=>'post/view',
        		'posti/<id:\d+>'=>'post/index',
        		'posts/<tag:.*?>'=>'post/index',
				'film/<id:\d+>'=>'film/view',
				'reihen/<id:\d+>'=>'reihen/view',
				'joomla/index.php'=>'joomla/index',
        		'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
			),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
			array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
			),
			// uncomment the following to show log messages on web pages

			/*array(
			 'class'=>'CWebLogRoute',
			 ),
			 */
			array(
			      'class'=>'XWebDebugRouter',
			      'config'=>'alignLeft, opaque, runInDebug, fixedPos, collapsed, yamlStyle',
			      'levels'=>'error, warning, trace, profile, info',
			),
			array(
                    'class'=>'CEmailLogRoute',
                    'levels'=>'guestbook',
                    'emails'=>'thimo.britsch@afk-filmkreis.de',
            ),

			),
			),
			),

			// application-level parameters that can be accessed
			// using Yii::app()->params['paramName']
	'params'=>require(dirname(__FILE__).'/params.php'),
			);