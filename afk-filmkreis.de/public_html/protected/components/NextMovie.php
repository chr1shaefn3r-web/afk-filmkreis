<?php

Yii::import('zii.widgets.CPortlet');

class NextMovie extends CPortlet
{
	public function init()
	{
		$this->title=CHtml::encode("Nächste Vorstellung");
		parent::init();
	}

	protected function renderContent()
	{
		$model=Yii::app()->cache->get('nextmovie');
		if($model===false)
		{
			$model = Vorstellungen::getNextVorstellung();
			Yii::app()->cache->set('nextmovie',$model, 1800);
		}
		$this->render('nextmovie',array(
			'model'=>$model,
		));
	}
}