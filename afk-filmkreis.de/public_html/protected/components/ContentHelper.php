<?php
class ContentHelper{


	public static function parseContent($content){
		$content = self::parseLinks($content);
		$content = self::parseImageBase($content);
		//$content = nl2br($content);
		//$content = str_replace('/joomla/images/stories/afk/', '{IMG}', $content);
		$content = self::parseOwnFunctions($content);
		return $content;
	}
	
	public static function parseOwnFunctions($content){
		
		$content = preg_replace_callback(
        	'#\[randorder\](.+)\[\/randorder\]#iUs',
        	function ($matches) {
        		$parts = explode("<br>", $matches[1]);
				
				shuffle($parts);
        	
          	  return implode("<br>", $parts);
      	  	},
        	$content
    	);// Zufällige Aufzählung (Siehe Spendertafel)
		
		
		return $content;
		
	}
	
	private static function OwnFuncRandorder($list){
		return "HALLO";
	}

	public static function parseLinks($content){
		$content = preg_replace("/\{URL\}(.*)\|(.*)\{\/URL\}/Usi", CHtml::link('$2',array('$1')), $content);

		return $content;
	}

	public static function parseImageBase($content){
		$content = preg_replace("/{IMG}/", Yii::app()->params['baseurls']['img'], $content);
		return $content;
	}

	public static function parseCorrectKollation($content){
		$content = preg_replace("/Ã¤/", 'ä', $content);
		$content = preg_replace("/Ã¶/", 'ö', $content);
		$content = preg_replace("/Ã¼/", 'ü', $content);
		$content = preg_replace("/ÃŸ/", 'ß', $content);
		$content = preg_replace("/Ã„/", 'Ä', $content);
		$content = preg_replace("/Ãœ/", 'Ü', $content);
		$content = preg_replace("/Ã–/", 'Ö', $content);

		$content = preg_replace("/Ã³/", 'ó', $content);
		$content = preg_replace("/â€ž/", '„', $content);
		$content = preg_replace("/â€/", '”', $content);
		$content = preg_replace("/Ã³/", 'ó', $content);
		return $content;
	}
	
	public static function parseURLCreate($content){
		$content = preg_replace("/ä/", 'ae', $content);
		$content = preg_replace("/ü/", 'ue', $content);
		$content = preg_replace("/ö/", 'oe', $content);
		$content = preg_replace("/Ä/", 'Ae', $content);
		$content = preg_replace("/Ü/", 'Ue', $content);
		$content = preg_replace("/Ö/", 'Oe', $content);
		
		$content = preg_replace("/ /", '_', $content);
		$content = preg_replace("/\//", '_', $content);
		return $content;
	}
	
	public static function createKapitelJumper($before="", $after=""){
		return "&lt; ".CHtml::link('vorheriges Kapitel', array('site/page', 'view'=>$before))." | ".(($after=="")?(''):CHtml::link('nächstes Kapitel', array('site/page', 'view'=>$after)))." &gt;";
	}
}