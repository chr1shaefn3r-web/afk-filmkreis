<?php

Yii::import('zii.widgets.CPortlet');

class Vote extends CPortlet
{
	public function init()
	{
		$this->title=CHtml::encode("Erstie-Wunschfilm:");
		parent::init();
	}

	protected function renderContent()
	{
		$this->render('vote');
	}
}