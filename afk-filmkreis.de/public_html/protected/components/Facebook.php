<?php

Yii::import('zii.widgets.CPortlet');

class Facebook extends CPortlet
{
	public function init()
	{
		$this->title=CHtml::encode("Wir sind auch bei Facebook:");
		parent::init();
	}

	protected function renderContent()
	{
		$this->render('facebook');
	}
}