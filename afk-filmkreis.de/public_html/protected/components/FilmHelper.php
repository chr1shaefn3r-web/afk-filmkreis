<?php

class FilmHelper{
	
	private static $start = 1954;
	
	
	public static function createSemesterDropDownList(){
		$arr = array();
		
		for ($i=date("Y"); $i >= self::$start; $i--) {
			if($i != date("Y") && date('m')<8){
				$arr['w.'.$i] = "Wintersemester " . $i . "/" . ($i+1);
			} 
			
			$arr['s.'.$i] = "Sommersemester " . $i;
			
		}
		
		//print_r($arr);
		return $arr;
	}
	
	public static function getCurrentSemester(){
		if(date('m')>1 and date('m')<8){
			return 's.'.date('Y');
		}else{
			return 'w.'.date('Y');
		}
	}
	
	public static function createFilmInfo($info, $replace){
		if(!empty($info) && $info != ","){
			return str_replace("{I}", $info, $replace);
		}
	}
	
	public static function createFilmInfoByArray(array $arr){
		$str = "";
		foreach ($arr as $value) {
			$str .= self::createFilmInfo($value['info'], $value['replace']);
		}
		
		return $str;
	}
}


?>