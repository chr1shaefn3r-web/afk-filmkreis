<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.harunfarocki')?>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-romualdkarmakar"></a>
<h3>Blicke, die nicht richten<br>
<small>Die Filme von Romuald Karmakar</small>
</h3>


<p>COUP DE BOULE beginnt mit einer Nacht&uuml;bung der franz&ouml;sischen Armee.
Schemen robben durchs Dunkel, halblaut gebr&uuml;llte Befehle betonen die martialische
Atmosph&auml;re. Der erste Augenblick verwandelt eine Wahrnehmung in ein Klischee: 
Nun spielen sie wieder Krieg, das ist die Assoziation, die sich beim Betrachten 
der Szene unwillk&uuml;rlich einstellt. Aber wer sind diese Schatten, denen 
Romuald Karmakar 1987 einen 25-min&uuml;tigen Dokumentarfilm gewidmet hat?
Die Antwort verwandelt ein Klischee in eine Wahrnehmung;
Sie sind Einzelne, die beim Milit&auml;r eine seltsame Form der 
Zusammengeh&ouml;rigkeit exerzieren.</p>


<p>Romuald Karmakar, Jahrgang 1965, ist als Sohn einer franz&ouml;sischen 
Mutter und eines indischen Vaters 1987 zum franz&ouml;sischen Wehrdienst 
eingezogen worden. Der Bataillonsphotograph, der eigentlich den 
Ereignissen des Kasernenhoflebens ein "besonderes Gewicht geben" soll, 
holt stattdessen seine Kameraden vor die Super-Acht-Kamera, die er 
am Ende eines Urlaubs eingeschmuggelt hat. Er filmt ein Ph&auml;nomen, das in
Frankreich verbreiteter ist als in Deutschland: "Coup de boule", 
das Austeilen von Kopfn&uuml;ssen. Aber die jungen Soldaten,
die er vor die Kamera und ins Rampenlicht bittet, als g&auml;lte es,
sich auf einer Probeb&uuml;hne als Schauspielnachwuchs zu bew&auml;hren,
treten nicht gegeneinander an. Sobald sie ihren Namen, 
Alter und Dienstgrad genannt haben, rennen sie mit dem Kopf
gegen einen Spind: <i>"Zum Spa&szlig;, f&uuml;r alle Kameraden"</i>. Die 
Vorstellung individueller Lebensl&auml;ufe scheint bei diesem 
kollektiven Wettlauf um Anerkennung auf der Strecke zu bleiben. 
Doch die Jungen, die ihre Beulen wie Auszeichnungen tragen, l&auml;cheln,
wenn sie gegen den Spind krachen. Von der eingedellten T&uuml;r
l&ouml;sen sich Zettel, die mit Rei&szlig;n&auml;geln angepinnt waren. Der 
Realit&auml;t des Aufpralls bleibt es &uuml;berlassen, die Lust an der 
Deformation, die befremdliche Wucht dieser Selbstdarstellung zu
kommentieren. Auch das nervenaufreibende Quietschen, das den
Film &uuml;berkommt, wenn die Jungs ihre eisernen Bettgestelle 
stemmen, ist so ein Kommentar. Von den Kasernierten erf&auml;hrt man
nichts &uuml;ber den Zweck ihrer au&szlig;erdienstlichen &Uuml;bungen. <i>"Fuck
you"</i>, sagen sie, wenn sie stemmend und rammend auf ihre 
Art ihren Kopf durchsetzen, aber man wei&szlig; nicht so recht, 
wen sie meinen: den Spind, das Bett, das Leben, die Kamera, 
sich selber, den Vorgesetzten. Am Ende verschwinden die 
Armeeangeh&ouml;rigen, denen Karmakar f&uuml;r 25 Minuten ein Gesicht, ein 
An-Sehen gegeben hat, wieder in der Anonymit&auml;t des 
Truppenalltags. Im Dunkeln, das Klischees gebiert. Karmakars 
Beobachtungen, die weder von Erkentnissen noch von Bekenntnissen 
auf den einen Begriff gebracht werden, der ein gebr&auml;uchliches 
Urteil &uuml;ber Personen und Handlungsweisen
erm&ouml;glicht, haben linken wie rechten Betrachtern gleichmerma&szlig;en Kopfschmerzen 
bereitet. 14 Tage Bau, drei Monate auf Bew&auml;hrung, hat die franz&ouml;sische Armee 
ihrem unautorisierten Dokumentaristen als Disziplinarstrafe zugedacht. <i>"Alle Leute,
die in dem Film mitgemacht haben"</i>, so der in M&uuml;nchen lebende Filmemacher in 
einem Interview mit Rolf Aurich (in <i>filmw&auml;rts</i> No 17), <i>"mussten 
unterschreiben, dass ich ihnen kein Geld gegeben habe"</i>. Die Beteiligten wurden 
des Drogenmissbrauchs beschuldigt. Offiziere, die den Film nie sahen, erkannten in 
Karmakar einen <i>"irgendwie linken, subversiven Typen"</i>. Dieses
"irgendwie", hilfloser Ausdruck des Unfassbaren, ist an Karmakar haftengeblieben. 
Auf der Berlinale 1988, die COUP DE BOULE pr&auml;sentierte, wurde der unorthodoxe, 
autodidaktische Filmemacher von einem eher links orientierten Publikum "irgendwie" 
der Verkl&auml;rung faschistoider Rituale und Zurichtungsmechanismen verd&auml;chtigt.</p>


<p>Die Absurdit&auml;t dieses Vorwurfs h&auml;tte bereits Karmakars erster Film entkr&auml;ften 
k&ouml;nnen. EINE FREUNDSCHAFT IN DEUTSCHLAND, 1985 mit minimaler Finanzierung als Spiel 
unter Freunden entstanden, versucht sich mit anarchistischer Verve an einem 
fiktiven Dokument von Hitlers M&uuml;nchener Studienjahren. Karmakar selbst hat in 
EINE FREUNDSCHAFT IN DEUTSCHLAND die Rolle eines humorlosen und verklemmten
Junghitlers &uuml;bernommen, dem erst ein Faschingsball Aufschluss &uuml;ber die 
Wirkung seiner sp&auml;teren Uniform verschafft. Der Hitlergru&szlig; verdankt sich 
in dieser malizi&ouml;sen Auslegung, die sich auf Recherche und Phantasie 
gleicherma&szlig;en gro&szlig;z&uuml;gig einl&auml;sst, dem steifen Winken, mit dem ein &ouml;sterreichischer 
Spie&szlig;b&uuml;rger karnevalistischen Frohsinn &uuml;bt. Von Verharmlosung, auch dies
ein Vorwurf, der Karmakars Filme mit Regelm&auml;&szlig;igkeit trifft,
kann dennoch keine Rede sein. Karmakars Hitler ist schon in
jungen Jahren ein narzisstischer Triebt&auml;ter, der die begehrte Cousine 
in den Tod treibt, sich &uuml;ber ihrem Selbstmord aber keineswegs 
von seinen Putschvorbereitungen ablenken l&auml;sst.</p>


<p>
<i>"Alles Dokumentarische ist real, alles Fiktive nicht unbedingt
falsch"</i>, hat Karmakar seinem siebzigmin&uuml;tigen Hitlerflm vorangestellt. 
So selten der Regisseur sich im Film erkl&auml;rt, seine (Selbst-)Versicherungen 
halten vor wie eiserne Reserven. Nicht nur EINE FREUNDSCHAFT IN DEUTSCHLAND, 
sondern auch COUP DE BOULE folgt diesem Vorsatz, der die Grenzen zwischen 
Spiel- und Dokumentarfilm zu bedenken gibt. Eine Realit&auml;t, die erst f&uuml;r die 
Kamera in Erscheinung tritt, gibt seinen Kurzfilmen den Anschein der 
Inszenierung, das Gepr&auml;ge von Minidramen. Dabei ist es vor allem Karmakars 
Themenwahl, die eine spontane Ann&auml;herung an die "ungeformte Realit&auml;t" 
vereitelt. Karmakars Interesse gilt Menschen, die vielerlei Gr&uuml;nde
haben, nicht in der &Ouml;ffentlichkeit auf sich aufmerksam zu machen. 
Seien es nun die franz&ouml;sischen Betreiber von Hahnenk&auml;mpfen, die 1988 
f&uuml;r GALLODROME ausnahmsweise eine Drehgenehmigung erteilten, seien es 
die Pitbullhalter vom Hamburger Kiez, die Karmakar 1989 f&uuml;r seinen Film 
HUNDE AUS SAMT UND STAHL die Haust&uuml;ren &ouml;ffneten: Zun&auml;chst wird in den
Filmen immer der Blickwinkel sichtbar, in dem eine sonst eher
unzug&auml;ngliche Gruppe von Au&szlig;enseitern sich dargestellt und gesehen wissen 
will. Karmakars au&szlig;ergew&ouml;hnliche Begabung, Fragen zu stellen, die nicht 
denunzieren, Blicke zu riskieren, die nicht richten, Unverst&auml;ndnis zu 
formulieren, nicht aber das ungl&auml;ubige Staunen des Bildungsb&uuml;rgers, 
erweitert den anf&auml;nglich gew&auml;hrten Spielraum. W&auml;hrend SPIEGEL und STERN, RTL
plus und die TAGESTHEMEN 1989, in einem Jahr, das die h&auml;ufigsten und 
furchtbarsten Pitbull-Angriffe zu verzeichnen hatte, Bisswunden zeigen 
und ein Verbot der Kampfhunde diskutieren, l&auml;sst sich Romuald Karmakar 
auf Gespr&auml;che &uuml;ber Eleganz und Edelmut der vierbeinigen Kampfmaschinen 
ein. Dass seine Interviewpartner, bis auf eine Ausnahme, aus der Zuh&auml;lterszene 
kommen oder Ex-Legion&auml;re sind, best&auml;tigt naheliegende Vermutungen &uuml;ber 
Training und Einsatz der Hunde. Das Fernsehen gibt sich mit solchen 
Fakten gern zufrieden. Karmakar akzeptiert sie und erf&auml;hrt mehr. Was 
sich ein Fernsehredakteur beim Thema Kiez-Szene vorstellt, hat sich 
Karmakar gefragt - und die Antwort gleich mitgeliefert: <i>"Einen Typen, 
der f&auml;hrt in seinem Mercedes die Reeperbahn runter, steigt dann irgendwo aus
im Halteverbot, auf der R&uuml;ckbank des Autos sitzen ein, zwei
Pitbulls, mit denen er zu einer Frau geht und die am besten noch
zusammenschl&auml;gt"</i>.</p>


<p>HUNDE AUS SAMT UND STAHL verhehlt nicht die Unberechenbarkeit 
der Portr&auml;tierten, dazu ist ihre Selbstdarstellung zu pr&auml;zise. 
Aber er f&ouml;rdert etwas zu Tage, das das Klischee geflissentlich 
unterschl&auml;gt: die Selbstverst&auml;ndlichkeit, mit der sich
ein Milieu als menschlich, als "normal" behauptet, das 
sich - seinem Ruf zufolge - eigentlich au&szlig;erhalb aller zivilisatorischer
&Uuml;bereink&uuml;nfte begreifen m&uuml;sste. Stattdessen entdeckt der Film
eine Gem&uuml;tslage, die den Pitbull-Liebhaber nur graduell vom 
fanatischen Normalhundbesitzer unterscheidet. Eine urdeutsche 
Hundeliebe gl&auml;ttet die mi&szlig;trauischen Gesichtsz&uuml;ge der Kiez-Kenner, 
wenn sie von ihren Pitbulls schw&auml;rmen. Ein Hund wie
ein Freund, ein Hund wie ein Vaterland, ein Hund wie die Familie. 
Die Treueschw&uuml;re, die Lobeshymnen, die Liebeserkl&auml;rungen an den 
todbringenden W&auml;chter, sie entspringen einer Sentimentalisierung 
der Gewalt, die alles andere als unb&uuml;rgerlich - und alles andere 
als undeutsch ist. Typen, denen man nicht im hellsten Sonnenschein 
begegnen m&ouml;chte, kuscheln sich mit sabbernden Viechern auf Pl&uuml;schsofas 
und beteuern Verst&auml;ndnis f&uuml;r einen Pitbull, der Sonntagsmorgens ins Ehebett dr&auml;ngt:
<i>"Wie ein Kind"</i>, sagen sie, <i>"es sind Menschen"</i>. Emp&ouml;rung
k&ouml;nnte das Klima, in dem solche S&auml;tze wachsen wie harmlos
aussehende Giftpilze, nie hervorbringen. Fragt Karmakar nicht
nach, so hat er gute Gr&uuml;nde. Dann muss der Zuschauer eben
nachh&ouml;ren. <i>"F&uuml;r mich"</i>, sagt der Filmemacher mit der 
schwierigen Klientel, <i>"ist eben genau das "Halbe" 
interessant - die Leute l&uuml;gen ja auch manchmal in die Kamera. 
Und in dieser L&uuml;ge steckt f&uuml;r mich mehr Ehrlichkeit als in der 
Wahrheit, die sie anderen anbieten"</i>.</p>


<p>Erkenntnis, die sich in einem Zwischenreich konstituiert, zwischen 
den Positionen, die man einnehmen kann, ohne das Gegen&uuml;ber an die Kamera, 
ohne sich selbst an die Gleichg&uuml;ltigkeit zu verraten, muss die Zusammenarbeit 
von Karmakar und Flatz erm&ouml;glicht haben. In der Welt der extremen M&auml;nnerspiele und
M&auml;nnerphantasien, die der Regisseur neugierig und obsessiv durchstreift, nimmt 
sich DEMONTAGE IX, die Dokumentation einer Performance des &ouml;sterreichischen 
Aktionsk&uuml;nstlers Flatz, denn auch eher als Zwischenspiel aus. Nicht, dass 
DEMONTAGE IX thematisch aus dem Rahmen fiele: Wiederum geht es um
eine ausgesprochen m&auml;nnliche Vorliebe f&uuml;r das Opfer und die
Gewalt, f&uuml;r Leiden und Selbst&uuml;berwindung, den Exhibitionismus des K&ouml;rpers, 
die Introvertiertheit der Sprache und des Gef&uuml;hls. Aber die Bearbeitung des 
fremden Materials hat Karmakar eine &auml;sthetische Strategie abverlangt, 
die seine Arbeiten bis dahin nicht hatten, nicht haben konnten, nicht wollten.</p>


<p>DEMONTAGE IX dokumentiert eine Performance, die ans Selbstm&ouml;rderische grenzt. 
Zwischen zwei Metallplatten baumelt kopf&uuml;ber ein K&ouml;rper. Ein "Gl&ouml;ckner" setzt den 
K&ouml;rper am Seil in Bewegung, bis er mit den st&auml;hlernen W&auml;nden kollidiert. 11 Minuten 
lang bringt das Pendel die "Stahlglocke" zum T&ouml;nen. Solange h&auml;lt sich die Kamera 
auf Distanz. Es bleibt ungewiss, ob es sich um einen Menschen oder um eine Puppe 
handelt. Der Blick des Betrachters, der sich kein Bild vom Ausma&szlig; seines
Unbehagens machen kann, ist ungesch&uuml;tzt wie selten im Kino. Vor der Konstruktion aus 
Fleisch und Stahl nimmt ein Paar Haltung an. Acht Minuten lang tanzen die beiden 
iranischen Europameister Walzer.</p>


<p>Der Walzer als kleinb&uuml;rgerliche Einstimmung aufs faschistoide "Wunschkonzert" hat 
in der deutschen Filmgeschichte einen Ton angegeben, der bis heute nachklingt. Die 
Sentimentalit&auml;t, die sich beim Walzer in Gang setzt und w&auml;hrend des 
Nationalsozialismus &uuml;ber Leichen hinwegging, ist so offensichtlich, dass sie 
als Kontrastmittel eigentlich nicht mehr taugt. Aber die Performance zielt 
auf die &Uuml;berdehnung der sentimentalen Bewegung. In der Wiederholung der 
Tanzschritte zeigt sich die nackte Anstrengung. Der hartleibige Ritus der 
Gewalt und_ das Ritual der leichtf&uuml;&szlig;igen Besch&ouml;nigung zehren gleicherma&szlig;en an
den K&ouml;rpern, das ist der eigentliche Affront. Ein dritter Abschnitt, der 
&uuml;ber das Abfilmen der Performance radikal hinausgeht, setzt das Auspendeln 
des K&ouml;rpers mit der Musik in Verbindung. Die Musik macht den Anblick ertr&auml;glich: 
Es sind diese pr&auml;zis inszenierten Momente der (Selbst-)Manipulation, denen mit 
Emp&ouml;rung wiederum nicht beizukommen ist. Nicht der Film ist ein Skandal, sondern 
die Leichtigkeit, mit der sich Wahrnehmung und Wertigkeiten beeintr&auml;chtigen lassen. 
Nach zwanzig Minuten wird der K&ouml;rper eines jungen Mannes vom Seil genommen. Dass in 
der Debatte um den Film allzu selbstverst&auml;ndlich von einer "Kreuzabnahme" die Rede 
war, bezeugt nur, in welchem Ma&szlig;e das Augenmerk auf die sinnstiftenden
Codices der Kunst, der Geschichte, der Kunstgeschichte fixiert ist. Der Mythos 
der Gewalt und der Gewaltt&auml;tigkeit sind Grundmuster der Performance, so wie 
Karmakars Kamera letztlich zur Entmythisierung des Geschehens beitr&auml;gt. Der Moment der
Erl&ouml;sung wird in der Wiederholung profanisiert. Zweimal (und aus unterschiedlichen 
Kameradistanzen) zeigt Karmakar, wie der K&ouml;rper vom Seil genommen wird.</p>


<p>Moralischen oder existenzialistischen Deutungen macht der Film 
keinen Mut. DEMONTAGE IX liefert kein Argument gegen Gewalt und Folter. 
Nicht, weil Karmakar indifferent w&auml;re. Seine Neugierde gilt dem, 
was sich noch (auf-) zeigen l&auml;sst, nicht der Gewissheit, sondern der Unstimmigkeit.</p>


<p>Ungew&ouml;hnlich fr&uuml;h, zumal f&uuml;r einen jungen deutschen Filmemacher, wurde 
Karmakars Filmschaffen in M&uuml;nchen 1989 retrospektiv gezeigt; 1990 folgte eine 
Retrospektive des Saarbr&uuml;ckener Max-Oph&uuml;ls-Festivals. 1992 wurde DEMONTAGE IX 
auf den Oberhausener Kurzfilmtagen ausgezeichnet. Alexander Kluge hat Karmakar 
schon seit geraumer Zeit entdeckt, nicht als Nachwuchs, sondern als Kollegen. 
Die Filmkritik hat sich mittlerweile &uuml;berrregional Gedanken dar&uuml;ber gemacht, warum 
der deutsche Film zutode gef&ouml;rdert wurde, w&auml;hrend einer wie 
Karmakar seine Filme allein finanzieren musste.</p>


<p>WARHEADS, ein Film &uuml;ber S&ouml;ldner, Legion&auml;re und Ex-Legion&auml;re, h&auml;tte 
etlichen F&ouml;rdergremien Gelegenheit zur Wiedergutmachung gegeben. Gef&ouml;rdert wurde 
der Film aus dem Jahr 1992 letztlich vom Kuratorium junger deutscher Film, von
der Berliner Filmf&ouml;rderung und dem Hamburger Filmb&uuml;ro - allerdings erst nach 
diversen Absagen. G&auml;nzlich zur&uuml;ckgenommen hat sich das Filmb&uuml;ro-Nordrhein Westfalen, 
dessen Argumentation die Annahme best&auml;tigt, dass es angebracht sein k&ouml;nnte,
Film(fach)leute in F&ouml;rdergremien zu berufen. Nur eine Wahrnehmung, die nicht dem 
Film, sondern wer wei&szlig; wessen politischen und moralischen &Uuml;berzeugungen Referenzen erweist,
konnte ein derart missliches Urteil hervorbringen. Das Gremium, lie&szlig; Nordrhein-Westfalens 
Filmb&uuml;ro wissen, <i>"f&ouml;rdere keine militaristischen Filme"</i>. Dieser Meinung schloss sich 
die Filmbewertungsstelle an, die dem Film kein Pr&auml;dikat zuerkennen mochte, ihm daf&uuml;r aber eine 
Beschreibung mit auf den Weg gab, die die Einrichtung von Deutschkursen f&uuml;r FBW-Protokollanten
nahelegt: <i>"In qu&auml;lender L&auml;nge"</i>, so der Bewertungsauschuss &uuml;ber
den dreist&uuml;ndigen Film, <i>"ziehen Bilder &uuml;ber die Leinwand, die
teilweise wie unbearbeitetes Rohmaterial von Amateurfilmern
wirken. Die Unverbindlichkeit der Aussage bleibt im Affirmativen stecken 
und wird noch durch die Anbieterei in devoter Haltung gegen&uuml;ber den 
Interviewpartnern verst&auml;rkt. Der Filmemacher setzt sich leider nicht in 
analytischer Form mit den eingef&uuml;hrten Personen oder m&ouml;glichen Themen des 
Films auseinander. Vielmehr weist die additive H&auml;ufung aller m&ouml;glichen
Aspekte auf eine mangelnde Dramaturgie und ein extrem oberfl&auml;chliches 
und erschreckend naives pers&ouml;nliches Interesse am S&ouml;ldnertum hin"</i>. 
Fehler sind Bestandteil dieses Gutachtens.
Aber folgenreicher als Schreibfehler und Stilbl&uuml;ten es sein k&ouml;nnen, 
ist der Denkfehler, WARHEADS als Nachhut eines in der
Tat kritikw&uuml;rdigen Kriegs- und S&ouml;ldnergenres auszumachen.
Nicht, dass allein die dokumentarische Aufbereitung der Erinnerungen 
gewesener und aktiver Legion&auml;re Karmakar vom Verdacht der besch&ouml;nigenden 
Parteinahme befreite. Aber in der Geschichte des deutschen Dokumentarfilms 
haben sich eher jene Darbietungen der Propaganda schuldig gemacht, in denen
ohne Unterlass Bilder kommentiert, montiert und gedeutet wurden, in denen 
dramaturgisch aus-, und zugerichtet wurde, was andere anrichteten. 
Karmakar ist diese Methode so fern, wie die moderne, bisweilen auch nur modische 
Form der Filmanalyse im Film, die die FBW einklagt. Stattdessen hat er sich
abermals auf ein Milieu eingelassen, von dem in Spionagethrillern und Krimis 
eher spekulativ die Rede ist, von dem kaum jemand wei&szlig;, und nur selten 
jemand es genauer wissen will.</p>


<p>Eine Anzeige in der M&uuml;nchener Abendzeitung ("Ex-Legion&auml;r f&uuml;r Filmprojekt in USA gesucht") 
hat Karmakar in Kontakt mit G&uuml;nther Aschenbrenner gebracht. WARHEADS, Teil I, widmet
sich &uuml;berwiegend der Befragung und Selbstdarstellung des geb&uuml;rtigen Deutschen, 
der zwischen 1958 und 1978 unter anderem als Fallschirmspringer in Algerien, 
Stra&szlig;enbauer in Franz&ouml;sisch-Guayana und Teilnehmer an 17 &uuml;berseeischen
Atomversuchen der franz&ouml;sischen Fremdenlegion gedient hat.
Aschenbrenners Erinnerungen, die aus der Sicht eines ranghohen 
Ex-Legion&auml;rs der Legion fraglos nur Ruhm und Ehre zusprechen, 
konterkarieren die paramilit&auml;rische Weltsicht auf eine
Weise, die dem Erz&auml;hler selbst nicht bewusst ist. Dem Umfeld
seiner eigenen Familie, so Aschenbrenner, sei er entflohen,
<i>"weil man &uuml;berall daran erinnert wurde, dass man aus einer Nazifamilie kam"</i>. 
Die Fremdenlegion wird zur Ersatzfamilie des 1939 geborenen Deutschen: 
Nicht etwa, weil sie sich von den politischen Vorgaben der leiblichen Eltern 
unterscheidet, wohl aber, weil sie ihre faschistoiden Tendenzen unschlagbar zur
(Truppen-)Moral stilisiert. Warum die Seele der Legion deutsch
gewesen sei, will Karmakar wissen. Die Antwort legt eben jene Familienbande 
blo&szlig;, aus denen sich Aschenbrenner vorgeblich hatte l&ouml;sen wollen. 
Trainingsstil, M&auml;rsche und Disziplin seien so deutsch gewesen wie die Kriegsgefangenen, 
die die Wahl zwischen Legion und fortgesetzter Gefangenschaft gehabt
h&auml;tten: <i>"Abends im Foyer"</i>, so Aschenbrenner unbek&uuml;mmert,
<i>"sang man auch Nazilieder"</i>. Karmakars Interesse an den Reaktionen 
der franz&ouml;sischen Offiziere, die immerhin das Dritte Reich bek&auml;mpft hatten, 
indignieren Aschenbrenner in einem Ma&szlig;e, das filmintern keinerlei Analyse 
bedarf: <i>"Offziere"</i>, so der Befragte, <i>"sind Pers&ouml;nlichkeiten, 
die nach zwei Jahren wechseln"</i>. Was bleibt, ist die Truppe.</p>


<p>Es ist diese Form der selbstredenden Enth&uuml;llung, die WARHEADS zu einem 
erstaunlichen Film macht. Seit HALF LIFE von Dennis O'Rourke, der 1985 Zeugen 
f&uuml;r die von Amerika ma&szlig;stabsgerecht vorbereiteten Atomkatastrophen auf den 
Bikini-Atollen zum Plaudern brachte, hat sich milit&auml;risches Selbstverst&auml;ndnis 
kaum je mehr derart ungeniert vor laufender Kamera pr&auml;sentiert. Nicht, dass 
Karmakars Gew&auml;hrsmann Kriegsgeheimnisse verriete. Aber Aufbau und Drill der 
Legion, die F&uuml;hrung "hauseigener" Bordelle, die es der Legion als Zuh&auml;lter 
erm&ouml;glicht, den kargen Sold ihrer Bediensteten auf Umwegen wieder
einzukassieren, sowie die Systematik einer Frauenfeindlichkeit,
die aus den Regeln und Vorsichtsma&szlig;nahmen f&uuml;r den Fall einer der 
eher seltenen Eheschlie&szlig;ungen spricht, geben sich in
Aschenbrenners noch erz&auml;hltaktisch militarisierter Weltenordnung alles 
andere als "unverbindlich".</p>


<p>Aus den Filmausschnitten, die in einem paramilit&auml;rischen Trainingscamp in Mississippi 
zustandegekommen sind, l&auml;sst sich  dagegen ablesen, was Betrachter, die Gesehenes von Geh&ouml;rtem
nicht trennen, dazu verleiten mag, von der Affirmation des Films f&uuml;r sein Thema zu sprechen. 
In dem Camp findet die Verst&auml;ndigung &uuml;ber das N&ouml;tigste - <i>"so entsichert man diese Waffe,
so legt sie los"</i> - auf englisch statt, man h&ouml;rt deutsche und franz&ouml;sische Laute: 
<i>"Die Seele der Legion ist &uuml;berall"</i>. Ein Ausbilder &uuml;bt mit den M&auml;nnern, 
die sich ob ihrer Tarnkappen und dem Dreck zum Verwechseln &auml;hnlich sehen, das Kapern von
Wagen in Feindesland oder Anschleichen unter versch&auml;rften Bedingungen. Als es gilt, 
sich drei Minuten lang einem Reizgas auszusetzen, nimmt der Ausbilder den Kameramann zur 
Kenntnis - und ins Visier: <i>"Das stinkt so"</i>, sagt der Mann zufrieden, 
<i>"dass ihr noch was davon abkriegt"</i>.</p>


<p>Wer etwas mitkriegen will von den extremen Willenskundgebungen dieser 
verzerrten Gesichter, diesen verzerrten Perspektiven und Pers&ouml;nlichkeiten, 
muss "etwas davon abkriegen", das ist die Regel, der Karmakars Team unterliegt. 
Sie verlangt nicht nach Anteilnahme, aber nach Teilnahme. Kriegsbeobachtern gleich 
haben die fremden Zuschauer, die t&ouml;dlichen Handgriffen auf die Finger gucken, der 
Mimesis Tribut zu zollen. Die &Uuml;bung, das macht das humorlose, der Ausnahme, der Abweichung 
nicht gewachsene Ritual sichtbar, ist der Ernstfall.</p>


<p>In dem Gespr&auml;ch mit Rolf Aurich hat Karmakar sich zu Recht schon vor 
Beginn des Projekts Gedanken &uuml;ber die schauspielerischen F&auml;higkeiten seiner 
Mitarbeiter gemacht: <i>"Du bist da zwei Wochen, schl&auml;fst im Freien, musst 
im Grunde einen Kampfanzug tragen, bist auf Patrouille. Es muss also jemand sein, der
sich in dieser Szene, die politisch nat&uuml;rlich eine ganz bestimmte F&auml;rbung hat, bewegen 
kann, der nicht anf&auml;ngt, mit dir politische Diskussionen zu f&uuml;hren. Ich kann keine 
Tunte mitnehmen. Oder jemand, der einen Zopf hat oder einen Vollbart, oder jemand, 
der aussieht wie Ho Tschi Minh. Da wird es halt knapper mit den Kameraleuten."</i>
</p>


<p>F&uuml;r die Zeit, in der das Team sich im Lager aufh&auml;lt, muss sich
die Kamera wie ein Legion&auml;r, wie ein S&ouml;ldner bewegen - wenn
sie denn etwas sehen will. So sind die Kriech&uuml;bungen der Truppe
nicht aus der Draufsicht, sondern im Kriechen gefilmt. Dass sich
Karmaka dabei nicht anpasst, sondern den vorgeschriebenen Bewegungsablauf 
zur Reflexion filmischer Vorgehensweisen nutzt, zeigt sich anhand einer 
Schie&szlig;&uuml;bung. W&auml;hrend die Studenten genannten Teilnehmer der "Survival School" 
hintereinander ein Ziel aufs Korn nehmen, h&auml;lt auch die Kamera an einer 
Schussposition fest. Die Wiederholung, die kein Spielfilm besser in
Szene setzen k&ouml;nnte, um das Resultat eines <i>shots</i> zu optimieren, 
wird zum irritierenden Bestandteil der Dokumentation. Je unnachgiebiger und 
"standfester" Karmakar das Ritual der Wiederholung festh&auml;lt, die Kampfschreie, 
die exakt jene Drehung der Hand beantworten, die Tritte, die akkurat jene Stimmlage
treffen m&uuml;ssen, desto unwirklicher wird die Perfektion der Observierten: Man muss 
sich - und das ist Ziel dieser Art &Uuml;bung - in Erinnerung rufen, dass es sich nicht 
um flei&szlig;ige Bruce-Lee-Imitatoren handelt, sondern um Spezialisten, die sich 
weltweit zur Erledigung jeder nur denkbaren, politischen Dreckarbeit andienen.</p>


<p>Wie weit dabei Tat und Selbstdarstellung auseinanderklaffen, 
l&auml;sst sich besonders sch&ouml;n nachvollziehen, wenn G&uuml;nther
Aschenbrenner im zweiten Teil des Films von seiner Altersvorsorge 
spricht. Auch ein Job als Sicherheitschef der deutschen
Firm Ortrag, die Atomm&uuml;ll experimenthalber einfach ins All
schie&szlig;t, war f&uuml;r den Ex-Legion&auml;r kein Problem. Seit 1989 ist
der Mann mit dem symboltr&auml;chtigen Namen in Afrika besch&auml;ftigt.</p>


<p>Einen nachdenklicheren Gespr&auml;chspartner, der Aberwitz und
Irrsinn seiner geliehenen (Kriegs-)Positionen erstaunlich luzide 
aufzudr&ouml;seln versteht, hat Karmakar in dem englischen S&ouml;ldner Karl 
gefunden. 1950 in Liverpool geboren, hat sich der ehemalige 
Seemann seit 15 Jahren auf das Dasein als S&ouml;ldner verlegt.
1991 sucht ihn Karmaka in Kroatien auf. In Gospic,
vier Kilometer von der Front entfernt, filmt er nicht Leichen,
sondern Einschussl&ouml;cher in H&auml;userw&auml;nden, das unendlich langsame 
Vorw&auml;rtskommen der &ouml;rtlichen Feuerwehr, die bl&auml;uliche
K&auml;lte einer Welt, in der der Mord an Tausenden von Menschen
zu einer unwirklich wirkenden Handlung ger&auml;t. Auf einem ehemaligen 
Aussichtspunkt installiert Karl Raketen, setzt ihnen
"Warheads" auf, Sprengk&ouml;pfe, die wie silbrige Karnevalskappen 
aussehen. Die Bastelei an den Raketen erinnert an die Handhabung 
eines Sylvesterfeuerwerks. Eine schwarze Rauchspur, die Risse in das 
Porzellan des Himmels sprengt, begleitet die Detonation; weit entfernt 
von den Initiatoren und den Beobachtern des kriegerischen Akts ist ein 
Einschlag zu h&ouml;ren. Die Abstraktion dieses Vernichtungsschlags hat etwas 
vom Grauen eines Trick-Films, der als Dokumentation endet.</p>


<p>Im Februar 1992 kehrt Karl nach England zur&uuml;ck. Karmakar
nimmt sein Interview wieder auf. Mag sein, dass die "Warheads", 
die Kahlk&ouml;pfe der Legion, Wirrk&ouml;pfe sind, Karl ist es
nicht. Der Mann hat einen Realit&auml;tssinn, der das gesungene 
Kameradschaftsbrimborium der Legion, ihre faschistoiden 
Kleider- und Fickvorschriften, ihre Lust an der Unterwerfung und dem
Unterwerfen einer Ineffektivit&auml;t zeigt, die sich im Krieg nicht
rechnet: Mit welchem Hut auf dem Kopf er t&ouml;te, das sei ihm
so gleich wie das Ideal, in dessen Auftrag rund um den S&ouml;ldner
gestorben und get&ouml;tet wird. Was Karl sich eingesteht, das macht
ihn f&uuml;r die betr&uuml;gerischen Selbstbetr&uuml;ger der salbungsvollen Legion 
zum Immoralisten. Zwar t&ouml;ten auch Legion&auml;re, ohne nach
Auftraggebern zu fragen, aber die h&ouml;here Daseinsform als Mitglied 
einer nationalistischen Elitetruppe scheint die Mittel zu heiligen. 
Karl macht sich auch weniger Illusionen &uuml;ber die Moral
der Welt&ouml;ffentlichkeit. Die Brigaden der UNO erscheinen dem
professionellen Skeptiker naiv: <i>"Die haben noch nicht gek&auml;mpft.
Sonst w&uuml;rden sie es nicht umsonst tun."</i>
</p>


<p>Zynismus als letzte Zuflucht? Karmakar hat sich f&uuml;r WARHEADS 
ein anderes Ende vorbehalten. In Gospic hat er eine junge Kroatin in 
Uniform getroffen, die aus M&uuml;nchen angereist ist, um Kroatiens Freiheit 
zu verteidigen. &Uuml;ber ihren Krieg hat sie genaue Vorstellungen: 
<i>"Ich werde Serben t&ouml;ten"</i>. Auch wie der
Frieden aussehen soll, wei&szlig; sie: <i>"Serben, die sich dann noch 
nach Kroatien trauen, erschie&szlig;e ich"</i>, sagt sie hasserf&uuml;llt. Wenig 
sp&auml;ter sieht man das M&auml;dchen zusammen mit anderen jungen Frauen bei einer 
Schie&szlig;&uuml;bung. Ein Ausbilder legt ihnen das Gewehr auf die Schultern, die Frauen 
kichern und lachen, ein Lachen f&uuml;r die Verlegenheit, ein Lachen f&uuml;r die Angst.</p>

    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
