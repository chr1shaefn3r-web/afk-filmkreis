<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.robertkramer','docs.dokumentarfilm.romualdkarmakar')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-harunfarocki"></a>
<h3>Eine Anstrengung - &uuml;berfl&uuml;ssig?<br>
<small>Portr&auml;t des Filmemachers und Publizisten Harun Farocki</small>
</h3>


<p>&Uuml;ber den in Berlin lebenden Filmemacher Harun Farocki und seine Arbeit zu schreiben, f&auml;llt
leicht und schwer zugleich. Ihre gedankliche Exposition und ihre Materialstruktur laden zum genauen Hinschauen ein
und belohnen mit einer F&uuml;lle von Entdeckungen. Im ersten Moment des Schreibens dr&auml;ngen sich vor die Filme aber die 
Situationen, in denen ich die Filme von Harun Farocki sah, sie im Kino selbst zeigte und mit dem Regisseur &ouml;ffentlich dar&uuml;ber sprach.
Das erste Mal, als ich Farocki bei einer Diskussion erlebte - 1978 auf der Duisburger Filmwoche -, verweigerte er sich den 
Zuschauern, die eine interpretative Nachbesserung seines Films "Zwischen zwei Kriegen" (1978) forderten. Farocki provozierte eine
gereizte Stimmung. Einige Monate sp&auml;ter, im Essener Kino "Zelluloid", lie&szlig; sich Farocki mit den wenigen Zuschauern &uuml;beraus 
geduldig auf jede Nachfrage zum Stoff und zu seiner politischen Interpretation ein. Ich will beide Verhaltensweisen weder 
gegeneinander ausspielen noch zu Konstituenten seines Charakters erheben, vielmehr als einen Erfahrungsgegensatz festhalten: 
wie die Vielzahl seiner Filme sind die Diskussionen mit Farocki reich an Unterschieden. Dass ich nach der K&ouml;lner Vorf&uuml;hrung des
Films "Bilder der Welt und Inschrift des Krieges" (1988) auf dem Nachhauseweg von der Diskussion von jemandem, der - um kein
Missverst&auml;ndnis aufkommen zu lassen - weder den Film gesehen hatte noch der Diskussion gefolgt war, einen Schlag ins Gesicht 
bekam, geh&ouml;rt zu diesen Erfahrungen ebenso hinzu wie das Erlebnis eines Wochenend-Seminars im K&ouml;lner Filmhaus, zu dem ich 
Farocki als einen der Referenten eingeladen hatte. Farocki wollte ausdr&uuml;cklich erst am Sonntagmorgen kommen, weil er am Samstag
seit Jahr und Tag Fu&szlig;ball spiele, und das nicht f&uuml;r das K&ouml;lner Seminar aufzugeben gedachte.
Am sehr k&uuml;hlen Sonntagmorgen beschlichen mich Zweifel, ob Farocki p&uuml;nktlich eintreffen
werde. In der Nacht hatte es die Umstellung zur Sommerzeit gegeben, so dass er noch eine Stunde fr&uuml;her aufstehen und ein Flugzeug
nehmen musste. Doch p&uuml;nktlich wie ein Buchhalter stand Farocki um zehn Uhr Sommerzeit mit seinen Filmb&uuml;chsen vor 
dem Versammlungslokal und war bester Laune.</p>


<h4>BREITES SPEKTRUM</h4>


<p>M&uuml;sste man die Filme, die Harun Farocki (geb. 1944 in Neutitschein, damals: Sudetengau) in den letzten Jahren im Auftrag des
Fernsehens (vor allem f&uuml;r den WDR und das ZDF) oder mit Geldern der kulturellen Filmf&ouml;rderungen in Hamburg und 
Nordrhein-Westfalen gedreht hat, klassifizieren, h&auml;tte man sie in einem groben Zugriff auf f&uuml;nf
Gruppen zu verteilen. Die erste versammelt dokumentarische Arbeiten, die einen einzelnen 
Arbeitsvorgang - etwa die Herstellung eines Fotos f&uuml;r den "Playboy" beispielsweise in
"Ein Bild" (1983) oder die Entwicklung einer Werbekampagne in "Image und Umsatz oder Wie kann man einen Schuh herstellen" (l989)
- festhalten und in einem schl&uuml;ssigen Filmablauf verdichten. Eine zweite Gruppe lie&szlig;e sich aus Filmen zusammenstellen. in denen
K&uuml;nstler und ihre Arbeitsweise portr&auml;tiert werden: 
"Zur Ansicht: Peter Weiss" oder "Georg K. Glaser  -  Schriftsteller  und Schmied" (1989). Die dritte enthielte 
medienkritische Studien ("Der &Auml;rger mit den Bildern", 1973) und Filmessays, die sich in historischen 
Exkursen des Zusammenhangs der Wahrnehmung mit der industriellen Produktion annehmen: 
"Bilder der Welt und Inschrift des Krieges" (1988) und "Wie man sieht" (1986). Eine vierte Gruppe enthielt
jene Filme, in denen Farocki sich inszenatorischer Formen bediente: "Zwischen zwei Kriegen" und "Etwas wird sichtbar" (1983)
sowie "Betrogen" (1985). Hier ist die klassifikatorische Willk&uuml;r am gr&ouml;&szlig;ten. In die f&uuml;nfte Gruppe k&auml;men schlie&szlig;lich die 
kleinen Beitr&auml;ge f&uuml;r die "Sesamstra&szlig;e", "Sandm&auml;nnchen" oder f&uuml;r die Filmredaktion des WDR. Gerecht wird man Arbeiten Harun Farockis
aber nur, wenn man sie einzeln und zusammen und im historischen Kontext studiert, wie das Motiv des Vietnamkrieges in den 
fr&uuml;hen Agitationsfilmen (etwa "Nicht l&ouml;schbares Feuer", 1969) in seinem Spielfilm "Etwas wird sichtbar" genauestens untersucht, also
auseinandergenommen und wieder neu zusammengesetzt wird;  wie  die  &auml;sthetische Kargheit der Essayfilme sich gegen die von
ihm kritisierte Bebilderungs- und Verlautbarungstechnik des Fernsehfeatures entwickelt hat; wie er den historischen Spuren 
der Arbeiterbewegung folgte, ohne diese zu glorifizieren oder zur Rechtfertigung der eigenen Praxis zu idealisieren; wie er sich neugierig
auf die  Verh&auml;ltnisse hierzulande  einlie&szlig;, ohne sich von ihnen &uuml;berw&auml;ltigen zu lassen, und deshalb imstande war, sie gleichsam zu
skelettisieren; wie er mit den Schriftstellern Peter Weiss, Heiner M&uuml;ller und Georg K. Glaser spricht, um etwas von ihrer 
materialistischen Arbeitsweise zu erfahren und so auch - bescheiden, aber sp&uuml;rbar - etwas von sich selbst mitzuteilen; 
wie sich die 16mm-Filme (etwa "Zwischen den Kriegen") von den auf 35mm gedrehten ("Etwas wird sichtbar") unterscheiden 
und die ersteren wiederum von einer Videoarbeit ("Image und Umsatz"), schlie&szlig;lich wie Musik und Sprache eingesetzt
werden und wie Farocki montiert.</p>


<p>Sein j&uuml;ngster Film "Leben - BRD" (1990) wurde im Rahmen des "Internationalen Forums des jungen Films" der "Berlinale" 
uraufgef&uuml;hrt und von der Redaktion "Kleines Fernsehspiel" kurz darauf Programm des ZDF gezeigt. Derzeit ist er in den 
Programmkinos zu sehen. Farocki besuchte und filmte 32 Lern-, &Uuml;bungs-, Therapie- und Teststunden in der ganzen 
Bundesrepublik Deutschland und montierte sie zu einer unkommentierten Folge von Kurzszenen - aneinandergeschnitten 
nach Stichworten, Oppositionen, Assoziationen, Bewegungen und Gesten. "Leben - BRD" setzt aus der F&uuml;lle der 
Details das Bild einer Gesellschaft zusammen, in der das Geb&auml;ren  und das Sterben, das Schreien und die Menschenpflege, 
das Stra&szlig;e &uuml;berqueren und das T&ouml;ten in staatlichen oder privaten Institutionen gelehrt und gelernt wird, ja, 
gelernt werden muss. Das wahre mechanische Ballett tanzen nicht die Maschinen, sondern die Menschen, die sich
nach einer Musik bewegen, die sich aus schw&uuml;lstigen Phrasen der Sozialp&auml;dagogik ("Magst Du, dass ich Dir helfe?"), 
B&uuml;rokratie ("Die Schulung zielt darauf ab, durch eine Kommunikationsaufnahme  eine  konkrete Zielansprache zu erreichen") 
und Therapeutik ("Sp&uuml;r in Dich hinein!") speist.</p>


<h4>DER MENSCH: DISZIPLINIERT</h4>


<p>Die gesammelten Szenen scheinen die Annahme zu st&uuml;tzen, hierzulande herrsche eine
Versicherungs- und Vorsorgementalit&auml;t vor, in der Gl&uuml;ck wie Elend durch Sozialtechniken
diszipliniert und von ihrem Grad der Unberechenbarkeit befreit werden sollen. Doch 
"Leben - BRD" ersch&ouml;pft sich nicht in einer solchen Interpretation. Andere lassen sich 
ebenfalls denken - etwa die, dass die Arbeitsgesellschaft, der die Arbeit ausgeht, zu 
neuen spielerischen Formen der Arbeit dr&auml;ngt; auch solche Deutung wird vom Film unterst&uuml;tzt.
Wichtiger noch: die Teilnehmer an den Spielen, Tests und Therapiesitzungen werden
nicht zu Belegst&uuml;cken irgendwelcher Thesen degradiert. Sie behalten in Abstufungen 
etwas von ihrer W&uuml;rde.</p>


<p>Das hat seine Ursache in der Arbeitsweise Farockis: Farocki hat die Szenen so geschnitten,
dass selbst noch der unsinnigste Vorgang sich gleichsam selbst erkl&auml;rt. Und so pr&auml;gt sich
manches Gesicht - etwa das des Kindes mit den spillerigen Haaren zu Beginn, des 
Tippelbruders, der lieber Brath&auml;hnchen kaufen statt m&uuml;hsam das Kochen erlernen m&ouml;chte, oder
einer &auml;lteren Frau, die in der Gruppentherapie zu spielen hat, sie sei ihr Herd - ebenso ein
wie der Satz, den eine junge Mutter sagt: <i>"Ich sch&auml;me mich, dass ich von diesem Mann ein
Kind habe"</i>.</p>


<p>"Leben - BRD" hat nach der Fernsehausstrahlung in der Presse ein gro&szlig;es Echo 
gefunden. Es scheint, als sei es Farocki endlich gelungen, auch Fernsehkritiker dazu zu 
zwingen, in Ruhe zuzuschauen, statt sich von der sicheren Hand eines Kommentars f&uuml;hren zu
lassen. Wie in fr&uuml;heren F&auml;llen die Ablehnung seiner Arbeiten aussah, war 1988 in der Jury
des Adolf-Grimme-Preises mitzuerleben, als sich eine Mehrheit gegen eine Auszeichnung
seines Films "Die Schulung" (1987) bildete. Die einen waren gegen ihn, da man das, was
er an Selbstdisziplinierung und Subjektentkernung zeige, ohnehin schon alles wisse. Die
anderen lehnten ihn ab, weil der Film gar nichts erkl&auml;re und die Zuschauer ohne 
notwendige Meinung &uuml;ber das Gesehene zur&uuml;cklasse. Ahnlich "heiter" mag es unter den 
Beisitzern in der Filmbewertungsstelle Wiesbaden (FBW) zugegangen sein, als sie mit dem
Fu&szlig;ballergebnis von "3:2" dem Film "Leben - BRD" ein  Pr&auml;dikat  absprachen.
Ihre "Kennzeichnung" des Films ist es ebenso wert, zitiert zu werden 
(<i>"... Dokumentarfilm/Thesenfilm/Rollenspielele/angepasstes  Verhalten/langatmig/filmisch  unergiebig"</i>...) 
wie die gutachterlichen Verst&ouml;&szlig;e gegen Grammatik und Logik: 
<i>"Es wird obendrein zur &uuml;berfl&uuml;ssigen Anstrengung: der Durcheinander- und
Zusammenschnitt von verschiedenen Handlungsabl&auml;ufen, als Gestaltungsprinzip 
ausgegeben, verlangt bei der gedanklichen Zur&uuml;ckordnung der Themen zus&auml;tzliche 
Konzentration."</i> Wenn die Beisitzer von "sinnlichen Elementen" sprechen, 
dann klingt das nicht, als spr&auml;chen sie &uuml;ber einen Film, den man in
der Tat sehen muss, um ihn zu begreifen.</p>


<p>Harun Farocki geh&ouml;rte zu den relegierten Studenten des ersten Jahrgangs an der 
"Deutschen Film- und Fernsehakademie Berlin" (dffb). Aber, so schrieb er 1984 in 
einer Brosch&uuml;re der Akademie, das wisse heute kaum jemand mehr: 
<i>"Auch heutige dffb-Studenten nicht, gro&szlig;e Verwunderung, wenn ich 
erz&auml;hle, dass Daniel Schmid da war und erst recht bei diesem Pl&uuml;schtierdirektor 
Wolfgang Petersen (wohl der Einzige aus dem Jahrgang, der noch lange Haare hat)."</i>
In seinen Aufs&auml;tzen hat Farocki &uuml;ber das Zustandekommen seiner Filme mehrfach und beinahe
regelm&auml;&szlig;ig Rechenschaft abgelegt: <i>"Mit Biografie, so es eine gibt, hat 
das nichts zu tun. Es geht um die Schilderung von Arbeitsverh&auml;ltnissen."</i> 
Doch die Zeitschrift "Filmkritik", in der er &uuml;ber zehn Jahre publizierte und 
redaktionell betreute, erscheint seit 1984 nicht mehr. Farocki soll, 
so hei&szlig;t es, noch auf einigen kompletten Jahrg&auml;ngen sitzen, als w&auml;re
er ein Spekulant, der auf die Wertsteigerung seiner Ware wartet.</p>


<p>Um zu beschreiben, was man in den alten Heften der "Filmkritik" alles finden kann,
eine letzte Geschichte um Farocki: Unter den &uuml;ber 200 eingereichten Produktionen der 
Duisburger Filmwoche 1989 war auch ein kurzer Film aus der Deutschen Film- und 
Fernsehakademie Berlin, der in der Flut der gewichtigen Themenfilme zu den 
Konflikten und Problemen der Welt unterging. Nur das Musikst&uuml;ck, das er 
verwandte, blieb einigen Mitgliedern der Auswahlkommission tagelang im
Ohr. Als Komponistin wurde im Abspann die S&auml;ngerin Nico genannt. Die Versuche, das
namenlose St&uuml;ck in den Folgemonaten zu identifizieren und auf einer der zahlreichen
Platten der Ex-K&ouml;lnerin zu entdecken, scheiterten. Von Nico waren im letzten Jahrzehnt
zu viele Zusammenstellungen und Live-Aufnahmen auf Platte ver&ouml;ffentlicht worden,
w&auml;hrend die alten Originalplatten aus dem Handel genommen wurden.</p>


<p>Per Zufall stie&szlig; ich Monate sp&auml;ter in einer Ramschkiste f&uuml;r 
Billig-CDs auf den Titel. Er wurde auf der Platte "Desertshore" 
ver&ouml;ffentlicht, die Nico 1970 gemeinsam mit ihrem Produzenten 
John Cale aufnahm, und hei&szlig;t "Le petit chevalier". Das Cover zieren Fotos
aus dem Film "La Cicatrice Interieure", den Philippe Garrel im selben 
Jahr mit Nico in der Hauptrolle drehte. &Uuml;ber den Film konnte ich
nur wenige Informationen finden (etwa in Ulrich Gregors  "Geschichte des 
Films ab 1960"). Erst jetzt entdeckte ich in einem Heft der "Filmkritik" 
so etwas wie eine Beschreibung: <i>"Der Film ist kein Handlungsfilm...
erst sitzt Nico auf einem Stein und klagt etwas in dem Tonfall, in dem 
das Gretchen im 'Faust' "Meine Mutter die Hur..." intoniert,
sie gibt einen Laut und horcht in sich hinein. ... um die Biegung des 
Weges kommt ein Sch&auml;fer mit seinen Schafen. Nun reicht der
Sch&auml;fer Nico die Gei&szlig;, die sie entgegennimmt wie der Zugabfertiger 
den Postpack, dann f&auml;hrt die Kamera ein St&uuml;ck mit dem Sch&auml;fer
parallel, entl&auml;sst ihn aus der Mitfahrt und sieht ihm und der 
Herde nach, bis alle hinter der Wegbiegung verschwunden sind, wobei die
Abblende mit dem allerletzten unschuldigen Tierchen einsetzt."</i>
</p>


<p>Autor dieses 1982 geschriebenen Textes ist Harun Farocki. 
Und sein Artikel, der mit dem Satz "Ein Film wie ein Plattencover" 
beginnt, ziert Vor- und R&uuml;ckseite der Langspielplatte "Desertshore" 
von Nico. Er endet mit dem Satz: <i>"Weil dieser Film zehn Jahre
alt ist und solche Filme heute nicht mehr gemacht werden, es keine 
Rockpoesie mehr gibt, die sich als neue Weltsprache entwirft,
kommt mir dieser Film wie aus einem vergangenen heroischen 
Zeitalter vor."</i>
</p>


<p>Der Sinn f&uuml;r Pr&auml;zision, der sich in der Beschreibung der Filmszene 
ausdr&uuml;ckt, sowie das Pathos eines unerf&uuml;llten Wunsches der
Gegenwart, der sich im Schlusssatz andeutet, geh&ouml;ren zum Bild des 
Filmemachers und Publizisten Harun Farocki. Es bleibt zu hoffen,
dass seine Filme nicht in den Ramschkisten der Medienbranche 
verschwinden und dass sie ihr Publikum im Lauf der Jahre finden 
werden.</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
