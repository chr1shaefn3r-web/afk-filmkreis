
<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.kingkong','docs.dokumentarfilm.littlebird')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-krieg"></a>
<h3>Fliegende Pfarrer und Hollywood im Krieg</h3>


<p>Zugegeben lassen sich in vielen F&auml;llen einfache Gr&uuml;nde vermuten, die einen Regisseur
veranlassen k&ouml;nnten, eher einen Dokumentarfilm als einen Spielfilm zu drehen.</p>


<p>Auff&auml;llig ist zum Beispiel, dass Dokumentarfilme meist zu den fr&uuml;hen Filmen der Regisseure
geh&ouml;ren. Hier liegen rein &auml;u&szlig;erliche Gr&uuml;nde nahe; es ist billiger und weniger aufwendig, einen
kleinen, guten Dokumentarfilm zu machen, als einen kleinen, guten Spielfilm, denn es sind die
fiktiven Elemente, die Geld verschlingen: Drehbuch, Kost&uuml;me, Bauten, Schauspieler,
Tricktechnik etc. (Dies ist wohl auch einer der Gr&uuml;nde, warum so viele Independent-Filme
dokumentarischen Charakter haben; man denke nur an die fr&uuml;hen Filme von Jim Jarmusch.
Durch den Verzicht auf einen kommerziellen Produzenten verringert sich der Etat, was direkte
Auswirkungen auf den Gebrauch teurer fiktiver Elemente hat.)</p>


<p>Andererseits bietet der Dokumentarfilm eine ideale M&ouml;glichkeit, die Technik des
Filmemachens zu erproben, ohne sich mit dem zus&auml;tzlichen Ballast, den eine
Spielfilmhandlung bedeutet, zu beschweren. Stanley Kubricks ersten beiden Filme DAY OF
THE FIGHT (1951 &uuml;ber einen Boxkampf) und FLYING PADRE (1952, &uuml;ber einen Priester,
der im Flugzeug zu den Mitgliedern seiner Gemeinde fliegt) fallen in diese Kategorie. Kubrick
hat sich bei diesen Filmen um alles selbst gek&uuml;mmert: Szenario, Regie, Kamera, Ton und
Schnitt.</p>


<p>Wenn man von der R&uuml;stungsindustrie absieht, hat wohl das Kino am meisten vom Krieg
profitiert, und sei es nur in Form von Kriegspropagandafilmen. Neben Regisseuren, die daf&uuml;r
ihr Genre nicht verlassen haben (wie Chaplin oder Hitchcock), gibt es einige bekannte
Spielfilmregisseure, die pl&ouml;tzlich mit Kriegsdokumentationen &uuml;berraschten. So zum Beispiel
(ideologisch weniger &uuml;berraschend) der Westernregisseur John Ford, oder (schon etwas
erstaunlicher) der monumentale John Huston. Kurios auch der produktive Frank Capra mit
Titeln wie PRELUDE TO WAR (1943) und THE NAZIS STRIKE (1943). Bekannt geworden
ist er f&uuml;r Filme wie ARSEN UND SPITZENH&Auml;UBCHEN und (gesellschaftskritische)
Kom&ouml;dien.</p>


<p>Die Liste lie&szlig;e sich beliebig fortsetzen, aber man merkt bereits: diese Filme passen selten zum
restlichen Werk der Regisseure, die auch meist nicht sehr stolz auf sie waren (Stanley Kubrick
nannte seinen FLYING PADRE schlicht "silly"). Sicherlich wird der Dokumentarfilm von
Spielfilmregisseuren eher lieblos behandelt, es gibt ihn beinahe nur als Auftragsarbeit oder
(finanzielle) Verlegenheitsl&ouml;sung. Ein Regisseur, der nicht nur etwas, sondern auch sich
darstellen m&ouml;chte, wird als Medium wohl kaum den Dokumentarfilm w&auml;hlen.</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
