<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-sinn"></a>
<h3>Der Sinn des Ganzen</h3>

<p>Durch die &uuml;berm&auml;ssige Flut von Lehrfilmen und uninspirierten Fernsehdokumentationen ist
der Ruf von Dokumentarfilmen ziemlich ramponiert worden. Kaum jemand erwartet von einer
Dokumentation einen unterhaltsamen Kinoabend. Dass genau das Gegenteil der Fall sein kann,
dass Dokumentarfilme nicht nur dokumentieren und informieren, sondern auch unterhalten,
mitreissen und mitf&uuml;hlen lassen k&ouml;nnen, das zeigen die Filme, die in diesem Programm laufen
und die auch in den n&auml;chsten Semestern ihren verdienten Platz auf der Kinoleinwand finden
werden.</p>


<p>In diesem Semester wird die historische Entwicklung des Dokumentarfilms mit ihren 
verschiedenen Auspr&auml;gungen (vom ethnographischen bis hin zum Agitpropfilm) ein klein wenig
angerissen: NANOOK OF THE NORTH von Robert Flaherty (1922), LAS HURDES von Luis
Bu&ntilde;uel (1932) und PAUL JACOBS UND DIE ATOMBANDE von Jack Willis und Saul Landau
(1978). W&auml;hrend man in diesen Filmen - ebenso im umstrittenen STAU - JETZT GEHT'S LOS
von Thomas Heise - in Teilen noch den Dokumentarfilm wiederfindet, wie man ihn zu kennen
glaubt, so wirken VATERS LAND (Peter Krieg), THE THIN BLUE LINE (Errol Morris) und STEP
ACROSS THE BORDER (Nicolas Humbert, Werner Penzel) eher wie Bildercollagen, vielschichtig
und emotional.</p>


<p>In den letzten Jahren hat sich ausserdem gezeigt, dass bei einer zunehmenden Anzahl von
Filmen die Unterscheidung in Dokumentar- oder Spielfilm, die zun&auml;chst so einfach erscheinen
mag, immer schwieriger wird. Auch dieses 'Grenzgebiet' zwischen Spiel- und Dokumentarfilm
soll sowohl in diesem Semester als auch in den kommenden mit herausragenden Beispielen
beleuchtet werden (in diesem Semester: L. 627 von Bertrand Tavernier und ZELIG von Woody
Allen als Spielfilme mit dokumentarisch-realistischem Hintergrund und formalen Stilmitteln
des Dokumentarfilms zur Steigerung der Authentizit&auml;t sowie Errol Morris' THE THIN BLUE
LINE als ein Dokumentarfilm mit inszenierter Wirklichkeit).</p>


<p>Die ganze Bandbreite von Filmen, die mit dem Verdikt 'Dokumentarfilm' belegt werden,
zeigt das kleine Music Special, welches drei Filme vereint, die man zun&auml;chst als 'Musikfilme'
bezeichnen w&uuml;rde.</p>


<p>Musikfilme besch&auml;ftigen sich normalerweise in einer eher unfilmischen und uninspirierten
Art und Weise mit Proben, Konzerten oder Tourneen ber&uuml;hmter Musiker. Wer sich f&uuml;r die
Musik interessiert, gar ein Fan ist, der wird diese meist bunt zusammengew&uuml;rfelten Konzertausschnitte 
goutieren. Wem die Musik nichts sagt, dem wird auch der Film nichts bringen,
aus dem einfachen Grunde weil der Film eigentlich gar keine filmische Ebene besitzt, sondern
lediglich die Musik zu den Zuschauerinnen &uuml;bertragen m&ouml;chte.</p>


<p>Ein guter Dokumentarfilm, ein Musikfilm muss daher mehr sein als eine Anh&auml;ufung von
Bildern &uuml;ber gitarrenzertr&uuml;mmernde Pop-Ikonen und wunderkerzenwinkende Massen. Michael
Wadleighs WOODSTOCK ist von den gezeigten drei Filmen dem dokumentierenden Konzertfilm
noch am n&auml;chsten. Was diesen Film allerdings aus der Konzertfilm-Dutzendware heraushebt,
sind nicht nur sein epochales Sujet und seine epische L&auml;nge, sondern auch und gerade 
Filmisches: ein intelligenter Schnitt, witzig eingesetzte splitscreen-Technik und ein Auge 
f&uuml;r die Kleinigkeiten, die Wesentliches aussagen. Alles in allem ist dieser Film nicht nur Dokument 
eines historischen Konzerts, er vermittelt auch etwas vom Flower-Power-Lebensgef&uuml;hl seiner
Entstehungszeit.</p>


<p>Stilistisch und thematisch schon wesentlich abstrakter und kaum noch als Konzertfilm zu
bezeichnen (man achte z.B. auf die Positionen, die die Kamera einnimmt) ist Jonathan Demmes
STOP MAKING SENSE. Das Konzert der Talking Heads wird in kleine St&uuml;cke geteilt und zu 
kleinen Episoden zusammengesetzt. Keine st&ouml;renden Informationen, keine peinlichen 
backstage-Interviews, keine gr&ouml;lenden Fans, daf&uuml;r aber Musik und Bilder, die eher poetischen Charakter
haben.</p>


<p>Noch ein St&uuml;ck weiter geht Humbert&amp;Penzels STEP ACROSS THE BORDER: In diesem Film
werden kaum noch Bilder aus Konzerten verwendet, sondern neue Bilder zur Musik erfunden.
Man muss die Musik nicht m&ouml;gen, die Musiker nicht kennen und wird trotzdem in den Sog
dieser Bilder geraten, die eher assoziativ die verschiedensten Gedanken oder Gef&uuml;hle wecken.
Eine perfekte Symbiose von Ton und Bild ist dieser Film von einem 'Dokument' weit entfernt
und trifft genau den Nerv der Zuschauerinnen.</p>

    
<a name="sec-ueberblick"></a>
<h3>Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms</h3>


<h4>Filmdokumente</h4>


<p>Die Geschichte des Dokumentarfilms beginnt 
im strengen Sinn mit den ersten Filmen &uuml;berhaupt, 
die die Gebr&uuml;der Lumi&egrave;re
1895 in Paris machten. Der &uuml;berwiegende
Teil der fr&uuml;hen Filmaufnahmen besteht aus
Dokumenten allt&auml;glicher Handlungen. Um
diese aufzuzeichnen wurde die Kamera auf
einem Stativ fest montiert und die Szene
wurde in einer einzigen Einstellung abgedreht, 
bis die Filmrolle leer war. Diese Bilder 
verloren schnell den Reiz des neuen und
schon 1896 wurden die Mittel des Schnitts,
der Montage oder der Einstellungswechsel
in ersten Spielfilmen entwickelt. Ab 1908
entstanden dann die ersten dedizierten 
Dokumentarfilme  als  Vorprogramm  zum
Unterhaltungsfilm. In dieser Zeit entstanden
auch  kunstlos  gedrehte  abendf&uuml;llende
Nachrichtenfilme wie z.B. der Bericht &uuml;ber
Scotts S&uuml;dpolexpedition (H. Pontiges).</p>


<h4>Der fr&uuml;he russische Film und seine Auswirkungen</h4>


<p>Der Film wurde in der Sowjetunion schon
fr&uuml;h als Mittel zur Verbreitung von revolution&auml;rem 
Gedankengut gef&ouml;rdert. So diente
der Film w&auml;hrend der Oktoberrevolution
als Chronist und Propagandamittel. Erste
k&uuml;nstlerische Ans&auml;tze bei Dokumentationen
lassen sich bei russischen Filmen finden, die
in  der  Zeit  unmittelbar  nach  der
Oktoberrevolution gedreht wurden. Um
den Mangel an Filmmaterial zu kaschieren,
waren die Chronisten gezwungen, mit 
aufwendigen Montagetechniken und schnellen
Schnittfolgen die reinen Dokumentationen
mit bewusst eingesetzten k&uuml;nstlerischen
Mitteln aufzuwerten. Alle bisherigen 
Gesetze und Konstruktionsgewohnheiten des
Films wurden bewusst verletzt. In den
Jahren nach 1923 wurde erstmals eine
Theorie dokumentarischer Filmpraxis durch
Dsiga  Wertows  Manifest  "Kinooki"
("Filmauge" oder "Kinoglas") formuliert:
<i>"Bis auf den heutigen Tag haben wir die
Kamera vergewaltigt und sie gezwungen,
die Arbeit unseres Auges zu kopieren. [...]
Von heute an werden wir die Kamera 
befreien und werden sie in entgegengesetzter
Richtung, weit entfernt vom Kopieren, 
arbeiten lassen. Alle Schw&auml;chen des 
menschlichen Auges an den Tag bringen! Wir 
treten ein f&uuml;r Kinoglas, das  im Chaos der
Bewegungen die Resultate f&uuml;r die eigene
Bewegung aufsp&uuml;rt, wir treten ein f&uuml;r 
Kinoglas mit seiner Dimension von Zeit und
Raum [...] Befreit von zeitlichen und 
r&auml;umlichen Eingrenzungen, stelle ich beliebige
Punkte des Universums gegen&uuml;ber, unabh&auml;ngig 
davon, wo ich sie aufgenommen habe. 
Dies ist mein Weg zur Schaffung einer 
neuen Wahrnehmung der Welt."</i> <small>(1923)</small>
Hervorzuheben sind hier besonders die
Filme "Ein Sechstel der Erde" (1926)
und "Der  Mann  mit der  Kamera" (1929).</p>


<h4>Die Kamera erschlie&szlig;t die Welt</h4>


<p>Etwa zeitgleich, aber dennoch unabh&auml;ngig
von dem Stil des russischen Dokumentarfilms 
verfilmte Robert Flaherty das Leben
eines Eskimos "Nanook of the North" (1922), 
den er w&auml;hrend zwei Jahren in der
Arktis begleitete. In dem Film werden 
Naturereignisse wie Schneest&uuml;rme, aber auch
Landschaftsaufnahmen erstmals als dramaturgische 
Elemente eingesetzt. Dieser Flaherty geh&ouml;rt zu der 
gro&szlig;en Gruppe der "Abenteurer mit der Kamera", die in dieser
Zeit zu exotischen Zielen ausschw&auml;rmten
um dem heimischen Publikum Bilder von
anderen Kulturen zu pr&auml;sentieren. Der unerwartete 
Kassenerfolg f&uuml;hrte im Anschluss
dazu, dass eine Reihe kommerzieller Filme
im lyrischen Stil &uuml;ber die Lebensweise
"primitiver" St&auml;mme produziert wurde.</p>


<p>Der russische Film hatte zu dieser Zeit 
dennoch starken Einfluss auf die europ&auml;ische
Avantgarde. Mt der konsequenten Ablehnung 
der Werte und Ideen, die Hollywood
repr&auml;sentierte, entstanden Filme wie Walter
Ruttmanns  "Berlin  -  Sinfonie  der
Gro&szlig;stadt" (1927), die durch Montage
der Einzelbilder rhythmische Strukturen mit
neuer &Auml;sthetik hervorbrachten und den
Kunstcharakter des Films konsequent betonten.</p>


<p>In den ersten drei&szlig;ig Jahren der 
Dokumentarfilmgeschichte haben sich somit bereits
die grundlegenden Ans&auml;tze entwickelt, die
sich in den Dokumentarfilmschulen der
darauffolgenden Jahrzehnte in immer neuen
Variationen wiederfinden sollten:
<ul>

<li>Der  aufkl&auml;rerische,  erzieherische
und auch manipulierende Dokumentarfilm
(Propagandafilm), der das Publikum zwar
belehren, ihm vornehmlich jedoch als Mittel
der Identifikation dienen sollte. Nicht 
Abbildung der Wirklichkeit, sondern Schaffung 
eines Weltbildes ist das Ziel.</li>


<li>Der informative, zum Teil allein
durch  die  Themenwahl  spektakul&auml;re
"objektive" Dokumentarfilm. Er ist in dem
Sinn objektiv, dass das Gezeigte von au&szlig;en
betrachtet wird und keine Wertung vom
Filmemacher mitgeliefert wird.</li>

<li>Der betont subjektive, assoziative,
k&uuml;nstlerische Film, der jedoch lange Zeit
aus den gro&szlig;en Kinos verbannt war und als
avantgardistischer Kurzfilm nur von einer
"Elite" zur Kenntnis genommen wurde.</li>

</ul>
</p>


<h4>Aufkl&auml;rung aus England</h4>


<p>Eine weitere wichtige Dokumentarfilmbewegung 
begann 1929 in England mit der Auff&uuml;hrung von 
John Griersons Film "Drifters", der die Fahrt eines 
Fischdampfers schildert, der zum Heringsfang 
ausl&auml;uft. In Folge entstanden &uuml;ber 1OO Filme,
die in England zu einer sozialen Institution
mit der Funktion eines informationspolitischen 
Mediums wurden. F&uuml;r Merson war
der Film zun&auml;chst weder zur Unterhaltung
noch zur Kunst bestimmt, sondern zur Propaganda. 
Nach seiner Auffassung war es ein zeitgem&auml;&szlig;es 
Mittel der Industriegesellschaft, die Staatsb&uuml;rger 
aufzukl&auml;ren und zu erziehen.</p>


<p>In den USA der 20er und 30er Jahre waren
zwei Haupttypen des Dokumentarfilms popul&auml;r: 
Die Reisebeschreibung, die sich
meist auf die klischeehafte Beschreibung
von romantischen St&auml;tten beschr&auml;nkte, und
der kurze "interest"-Film &uuml;ber irgendein
aktuelles Thema.</p>

<p>Im Verlauf der 30er Jahre erlahmte der
russische Dokumentarfilm unter den Auflagen  
des  "sozialistischen Realismus",
w&auml;hrend in anderen L&auml;ndern das sozialistische 
Gedankengut von kleinen Gruppen
avantgardistischer Filmemacher verbreitet
wurde. Als Beispiele stehen Bu&ntilde;uels "Las
Hurdes" (Spanien 1932) und Joris Ivens
"Komsomol" (1932).</p>


<p>Im deutschen Film des Dritten Reichs
spielte der Dokumentarfilm als wesentliches
Instrument  des  staatlichen  Propagandaapparates 
eine &uuml;bergeordnete Rolle. Besonders hervorzuheben 
sind hier die sp&auml;teren Wochenschauen sowie die Filme Leni
Riefenstahls, die sich nicht nur durch die
Inszenierung spektakul&auml;rer Massenszenen
auszeichnete ("Triumph  des  Willens"
1934), sondern auch durch einen brillanten
Schnitt auszeichnen. Der im Gr&ouml;&szlig;enwahn
gedrehte Film "Olympia" (1938), der die
Olympiade von Berlin  "dokumentiert",
wurde aus 800 OOO Metern (480 Stunden)
Filmmaterial zusammengeschnitten und ist
bis heute vor allem wegen der zwiesp&auml;ltig
faszinierenden &Auml;sthetik von den unz&auml;hligen
langweiligen  Olympiadedokumentationen
noch eine der sehenswertesten.</p>


<p>Mit Beginn des Zweiten Weltkriegs entstanden 
in Deutschland die mehr auf Terror
zugeschnittenen Agitationsfilme, die die
fr&uuml;hen Siege der deutschen Armee in frappierender   
Eindringlichkeit   schildern ("Feuertaufe" 194l).</p>


<p>In den alliierten L&auml;ndern brachte der Ausbruch 
des Krieges viele Filmemacher aller
Genres zum Propagandafilm. Als Beispiele
seien hier britische Durchhaltefilme wie
Harry Watts "Target for tonight" (194O)
oder die amerikanische "Why we fight"-Serie, 
bei der prominente Hollywood-Regisseure wie 
John Ford und John Huston unter der Leitung von 
Frank Capra arbeiteten.</p>


<p>In der Nachkriegszeit nahmen Auftragsarbeiten 
f&uuml;r die Industrie zu, bei denen sich
die Filmemacher den Forderungen der
Auftraggeber beugen mussten. Auch die
stereotypen Reisefilme mussten sich den
kommerziellen Interessen der Fremdenverkehrsvereine 
beugen und verloren damit  noch mehr an 
Individualit&auml;t und Reiz.</p>


<p>Familienunterhaltung boten in den 50er
Jahren auch Disneys abendf&uuml;llende Naturfilme ("Die W&uuml;ste lebt"), 
allerdings untergrub das Fernsehen diese Art von 
popul&auml;ren  Dokumentarfilmen, so dass Bekanntheitsgrad 
und Verbreitung schlie&szlig;lich bis weit unter das 
Vorkriegsniveau zur&uuml;ckfiel.</p>


<h4>Das cin&eacute;ma v&eacute;rit&eacute;</h4>


<p>Der zunehmend unpr&auml;zise und dadurch
entwertete Begriff "dokumentarisch" f&uuml;hrte
dazu, dass neuere Gruppen ihn f&uuml;r sich ablehnten. 
Es entstanden neue Bewegungen wie das 
"free cinema" (England), das "direct cinema" (USA) 
oder das "cin&eacute;ma v&eacute;rit&eacute;" (Frankreich).</p>


<p>Das Wort vom "cin&eacute;ma v&eacute;rit&eacute;" fiel zum 
ersten Mal im Zusammenhang mit Jean
Rouchs Interviewfilm "Chronique d'un &eacute;t&eacute;"
(1961). Rouch forderte, dass der Einsatz
des Aufnahmeapparates die Objektivit&auml;t des
tats&auml;chlichen Geschehens, das registriert
werden soll, nicht beeintr&auml;chtigen d&uuml;rfe.
Das technische Problem, auf das das
Verlangen nach Authentizit&auml;t stie&szlig;, war
erst mit der Entwicklung der handlichen 16mm-Kameras 
und der Transistonechnik ann&auml;hernd zufriedenstellend 
l&ouml;sbar. Die Beobachtung mit der Kamera sollte nach
M&ouml;glichkeit so durchgef&uuml;hrt werden, dass
deren Anwesenheit auf die Protagonisten in
ihrer jeweiligen Situation und auf die 
Ereignisse, die in jedem Moment unvorhergesehen 
eintreten konnten, keinen Einfluss
haben sollte. Die Dokumentaristen sollten
demnach reale Vorg&auml;nge sichtbar machen
und ihnen den Augenblick der Wahrheit
entreissen. Der Stil und das Konzept des
cin&eacute;ma v&eacute;rit&eacute; beeinflusste in der Folge auch
die Regisseure der Nouvelle Vague, setzte
sich aber erst mit zahlreichen 
Fernsehausstrahlungen auch beim breiten Publikum
durch.</p>


<p>Auch der wohl popul&auml;rste und bestimmt
kommerziell erfolgreichste Dokumentarfilm
"Woodstock" (Mike Wadleigh, 1970)
enth&auml;lt noch Elemente der V&eacute;rit&eacute;-Fotographie, 
die er allerdings mit einer fl&uuml;ssigen
Schnittfolge verband. In der Folge entstanden 
noch zahlreiche, qualitativ zum Teil
stark umstrittene Konzertfilme, die versuchten,
an den Erfolg anzukn&uuml;pfen.</p>


<p>Das cin&eacute;ma v&eacute;rit&eacute; verliert in den Folgejahren 
dann allerdings an Stellenwert, weil
mehr und mehr erkannt wird, dass der
Wegfall des Kommentars und die neutrale
Position der Filmemacher keine absolute
Objektivit&auml;t garantieren, da die Autoren 
zumindest entscheiden m&uuml;ssen, was in den
Film mit aufgenommen wird und damit
doch unmittelbar eingreifen.</p>


<h4>Die 60er und 70er Jahre</h4>


<p>In den sp&auml;ten 60er und fr&uuml;hen 70er Jahren
wurden dann besonders in den Vereinigen
Staaten politische Dokumentarfilme gedreht, 
bei denen eine neue Form des Journalismus 
von der Objektivit&auml;t abkam und es
zulie&szlig;, dass sich die Autoren konsequent
mit einbringen. Hier seien vor allem "The
Selling of the Pentagon" (Peter Davis, 1971) 
oder die sp&auml;ten Filme von &Eacute;mile de
Antonio genannt. Er konzentrierte sich in
Filmen wie "America is Hard to See"
(1970) auf eindringliche Analysen, die in
collagenhaften Konfrontationen sich selbst
enth&uuml;llende Bilder enthalten. Auch die feministischen 
Filme der fr&uuml;hen 70 er Jahre
sind hier einzuordnen. So zeichnen sich die
Filme von Amalie Rothschild oder Donna
Deitch durch eine typische Mischung von
politischem Engagement,  Selbstreflexion
und pers&ouml;nlicher Aussage aus.</p>


<p>Neben diesen zum gro&szlig;en Teil unabh&auml;ngigen 
Produktionen existierten auch Gruppen
etablierter Dokumentarfilmer, denen das
Fernsehen mit von ihm finanzierten Projekten 
zu einer gewissen Popularit&auml;t verhalf.</p>


<p>In der Bundesrepublik fristete der 
Dokumentarfilm bis in die 60er Jahre hinein eine
recht k&uuml;mmerliche Existenz in den Reservaten des 
Industrie- und Kulturfilms. Erst
mit Peter Nestler und Klaus Wildenhahn erhielt
der deutsche Dokumentarfilm durch
geduldiges Herangehen an die Themen und
eine flexible Gestaltung ohne streng 
vorgefasstes Konzept eine neue Qualit&auml;t.</p>


<p>Auch durchaus provokante Stellungnahmen,  
wie  "Der Kampf um 11%" (Michael Busse, Thomas Mitscherlich und
J&uuml;rgen Peters, 1972), in dem der Verlauf
eines Arbeitskampfes aus der Perspektive
der Beteiligten heraus dargestellt wird, stehen 
f&uuml;r diese Entwicklung.</p>

<a name="sec-wildenhahn"></a>
<h3>Das Abenteuer Dokumentarfilm<br>
<small>Zum 60. Geburtstag von Klaus Wildenhahn</small>
</h3>

<p>
<i>"Der eigenproduzierte lange
Dokumentarfilm im Fernsehen geht wahrscheinlich
seinem Ende entgegen."</i> So
lautete die pessimistische Prognose von 
Werner Filmer, stellvertretender Chefredakteur des
Programmbereichs Politik im WDR auf einem
Symposium &uuml;ber den "Dokumentarfilm im
Fernsehen der Bundesrepublik" im Oktober
vergangenen Jahres an der Universit&auml;t 
Marburg. Dass Ausnahmen diese Prognose in der
Tendenz best&auml;tigen, mag ein schwacher Trost
sein. Aber es gibt sie immerhin noch, auch
wenn der Dokumentarfilm heute redaktionell
weitgehend heimatlos zwischen den verschiedenen 
Programmbereichen der Kultur, dem
Fernsehspiel und der aktuellen Politik vagabundiert.</p>


<p>Der Dokumentarist Klaus Wildenhahn, der am
19. Juni seinen 60. Geburtstag feierte, ist eine
solche Ausnahmeerscheinung. Er macht seit
knapp 25 Jahren lange Dokumentarfilme f&uuml;rs
Fernsehen. Etliche davon sind zu Klassikern
des Mediums geworden und ihr Autor inzwischen 
zu einem respektablen "Fossil", dem
man gleichwohl noch viele produktive Jahre im
"&ouml;ffentlich-rechtlichen" Fernsehmedium w&uuml;nschen 
m&ouml;chte, das er als "gute Erbschaft unserer Besatzungsm&auml;chte" 
sch&auml;tzt, weil es f&uuml;r ihn <i>"das nicht-komerzielle in 
Arbeitsweise und Methode"</i> erm&ouml;glicht.</p>


<h4>SOLIDARISCH MIT DEN MENSCHEN</h4>

<p>Es sind Filme &uuml;ber die Arbeit und den Alltag,
&uuml;ber Landarbeit, Bauarbeit, Industriearbeit;
aber auch: St&uuml;cke &uuml;ber die M&uuml;hen der Kunst,
die Schwierigkeiten &auml;sthetischer Produktion.
Das klingt thematisch nach Lehrfilm und biederem 
Bildungsprogramm. Doch dem ist zum
Gl&uuml;ck nicht so, auch wenn Wildenhahn - seit
1964 als festangestellter Dokumentarfilm-Regisseur 
beim NDR - derzeit in der Redaktion
"Philosophie, Geschichte, Bildung" beheimatet
ist. Seine Filme gehen stets mit voraussetzungsloser 
Neugier an ihre Themen heran.
Dem Prinzip der teilnehmenden Beobachtung
verpflichtet, solidarisch mit den Menschen, die
er zeigt, machen sie den Zuschauer zum Augen- und 
Ohrenzeugen von Entdeckungsreisen.</p>


<p>In Wildenhahns Filmen kommen Menschen zu
Wort und ins Bild, die das Fernsehen nur allzu
gern zugunsten von Expertenbefragungen und
besserwisserischem Kommentar vernachl&auml;ssigt.</p>


<p>Sie unterscheiden sich krass von dem, was der
Fernsehprogramm-Alltag an dokumentarischen Formen 
bereith&auml;lt und haben nichts gemein mit den &uuml;blichen 
ins 45-Minuten-Zeitschema eingepassten Fernsehfeatures. 
Lange synchron gedrehte Einstellungen nehmen sich
Zeit f&uuml;r die Personen, die Wildenhahn mit einer
nur schwer beschreibbaren emotionalen Qualit&auml;t 
portr&auml;tiert. Sein sparsamer Kommentar, den
er stets selbst spricht, ist ebenso unpr&auml;tenti&ouml;s
wie die Filmbilder. Er informiert den Zuschauer 
in knappen Stichworten &uuml;ber die Vorbereitung 
und den Ablauf der Dreharbeiten,
macht ihn zusammen mit dem Originalton zum
Ohrenzeugen der Recherche.</p>


<h4>REISEN DURCH LANDSCHAFTEN</h4>


<p>Wildenhahns Filme sind auch Filme der Bewegung 
durch Landschaften: topographische und
historische Entdeckungsfahrten; Reisen, deren
Routen geographisch wie historisch aus der 
vorindustriell-agrarisch gepr&auml;gten Provinz ("In der
Fremde", 1967; "Die Liebe zum Land", 1973/74) bis 
in die ehemaligen, jetzt ausgebluteten
Metropolen der Schwerindustrie f&uuml;hren und
vor den Problemen der Zukunft einer 
Arbeitsgesellschaft, der die Arbeit ausgeht, nicht den
Blick verschlie&szlig;en ("Stillegung", 1987; "Rheinhausen Herbst 1988").
Jenseits der expliziten Inhalte bleibt ihr atmosph&auml;risches 
Gesp&uuml;r f&uuml;r die Topographie, f&uuml;r Bilder und T&ouml;ne, die die 
Lebenswelt der Protagonisten mit viel Sinn f&uuml;r scheinbar Beil&auml;ufiges
detailreich erfassen, im Ged&auml;chtnis: Eine Dorfstra&szlig;e 
in Ostfriesland abends; Arbeiter in einem Bus zur 
Fr&uuml;hschicht unterwegs ("Emden geht nach USA", 1975/76); 
der Blick aus dem Fenster einer Zechensiedlung im Ruhrgebiet,
sonntagsnachmittags ("Der Nachwelt eine Botschaft", 1979/80); 
der Blick aus einem Ballettstudio auf hochsommerliche New Yorker Stra&szlig;en 
("498, Third Avenue", 1967). Wildenhahn
hatte schon fr&uuml;h den Mut zur Entdeckung der
Langsamkeit, zu Pausen und den Erz&auml;hlduktus 
strukturierenden Leerstellen mit Sensibilit&auml;t 
f&uuml;r das Treffende im scheinbar Peripheren.</p>


<h4>PRAXIS UND THEORIE</h4>

<p>Wildenhahn ist nicht nur ein Praktiker, sondern
im Lauf der Jahre auch ein Theoretiker seines
Metiers geworden. Man hat ihm vor allem nach
der Publikation seines Buches &uuml;ber 
"Synthetischen und Dokumentarischen Film" (1972) 
Dogmatismus, Abneigung gegen experimentelle
Formen und die Propagierung einer Film&auml;sthetik, 
die durch &auml;u&szlig;erste N&auml;he zum Objekt die
Aufl&ouml;sung der Autorenposition anstrebt, vorgeworfen. 
Doch seine Dokumentarfilme waren
&auml;sthetisch immer viel reicher als seine damals
vertretene theoretische Position, waren immer
auch experimentell, ohne freilich das Experimentelle 
besonders zu betonen. Sie verleugneten in der Montage, 
der Organisation des filmischen Materials, keineswegs 
ihren Autorenstandpunkt.</p>


<p>Sein Filmhandwerk lernte Wildenhahn von
1961 bis 1964 als "Realisator" in der Redaktion
des zeitkritischen Magazins "Panorama" beim
NDR. Realisator sein hie&szlig;_ damals: die Stories
der Journalisten in Film.  umzusetzen, Texte zu
bebildern. Das Bild spielte dabei eine 
untergeordnete Rolle. Das war zu einer Zeit, als die
in den USA durch Richard Leacock, Don Allen
Pennebaker und die Gebr&uuml;der Maysles entwickelte 
neue Dokumentarfilmtechnik des "Direct
Cinema" auch in Deutschland bekannt wurde.
Die "Direct Cinema"-Technik bedeutete eine
Revolution f&uuml;r die &Auml;sthetik des Dokumentarfilms. 
Bis dahin hatten die weitgehend unbeweglichen 
35mm-Kameras wie die damaligen M&ouml;glichkeiten 
der Tonaufzeichnungen weder spontane Aufnahmen 
noch synchronen Originalton erlaubt. Man kam deshalb
nicht ohne eine Nachinszenierung aus. Mit den
neuen, ger&auml;uscharmen 16mm-Kameras
und der synchronen Tonaufzeichnung war der
journalistisch aktuelle Zugriff auf die Realit&auml;t,
die direkte Reportage m&ouml;glich geworden. Diese
neuen Kameras und Tonaufnahmeger&auml;te, lichtstarke 
Optik und hochempfindliches Filmmaterial erlaubten es,
mit minimalem technischem und
personellem Aufwand in langen Einstellungen
Abl&auml;ufe aus der Situation heraus durchg&auml;ngig zu
verfolgen. Mit Leacock Pennebaker und Maysles 
machte Wildenhahn 1964 ein Interview 
f&uuml;r die Sendung "Der Filmclub" des
dritten Fernsehprogramms des NDR. <i>"Das war
die zeitlich fixierbare Initialz&uuml;ndung"</i>, erinnert
er sich sp&auml;ter, <i>"ab dann konnte ich die &uuml;blichen
Fernsehdokumentationen nicht mehr herstellen, 
die ich gut drei Jahre lang mit gro&szlig;er Energie 
und Freude gemacht hatte"</i>.</p>


<h4>SUCHE NACH DER CHRONOLOGIE</h4>

<p>Die Beobachtung aus der Situation heraus
pr&auml;gte den Stil der Filme Wildenhahns in den
60er und 70er Jahren. Sein erster langer Film
f&uuml;r die NDR-Fernsehspielabteilung Egon
Monks, in die er 1967 hin&uuml;berwechselte, entstand 
in dreimonatiger Drehzeit mit einer zwei
Mann-Equipe. "In der Fremde" verfolgt den
Bau eines Futtermittel-Silos in der 
norddeutschen Provinz. Der dramaturgische Aufbau 
dieser _Langzeitbeobachtung verdeutlicht ein 
weiteres Arbeitsprinzip Wildenhahns: das Auffinden 
einer Chronologie in der Alltagsbeobachtung 
von Arbeitsvorg&auml;ngen, einer Chronologie,
die sich nicht nur an der Aufnahmezeit 
orientiert, sondern auch an den Beziehungen der
dargestellten Personen verfolgt wird. "In der
Fremde" zeigt die Arbeitsbedingungen der 
Betonbauer, Zimmerleute, Eisenflechter und
Hilfsarbeiter, die rund um die Uhr in zwei
Schichten arbeiten, weit weg von Heim und
Familie.</p>


<p>Die Dramaturgie ergibt sich aus der Situation
vor Ort. Sie wird nicht nachtr&auml;glich am Schneidetisch 
entwickelt. Drehzeit und Zeit der Recherche fallen 
zusammen. Die Montage, die dramaturgische Verdichtung 
der Ereignisse resultiert aus der Situationslogik am 
Drehort, sie findet hier quasi schon in der Kamera statt. 
"In der Fremde" wurde in einem Drehverh&auml;ltnis
von l:15 aufgenommen. Das war das Doppelte
der damals &uuml;blichen Fernsehnorm. Die Hauptabteilung 
Fernsehspiel des NDR verf&uuml;gte seinerzeit &uuml;ber gute 
Sendepl&auml;tze. Sie konnte mehr Geld in ein solches Filmprojekt
investieren als etwa aktuelle Redaktionen. Bis beute ist 
Wildenhahn als festangestellter Dokumentarfilmregisseur 
privilegierter als Auftragsproduzenten, f&uuml;r die l&auml;ngere 
Drehzeiten nur durch finanzielle Selbstausbeutung erm&ouml;glicht 
werden.</p>


<p>Sein zweiteiliger Film "Die Liebe zum Land"
(1973/74), dessen ironischer Titel auf die harten
Lebens- und Arbeitsbedingungen der hier portr&auml;tierten 
Bauern und Landarbeiter anspielt,
hatte bereits eine Vorf&uuml;hrdauer von zweieinhalb 
Stunden. Die Gesamtl&auml;nge des Dokumentarfilmzyklus' 
"Emden geht nach USA" betr&auml;gt f&uuml;nf_ Stunden. An vier 
einst&uuml;ndige Dokumentarteile schlie&szlig;t ein poetisches 
Portr&auml;t der Region Ostfriesland mit dem Titel "Im Norden das
Meer, im Westen der Fluss, im S&uuml;den das Moor,
im Osten Vorurteile" an. Die Dreharbeiten erstreckten 
sich &uuml;ber einen Zeitraum von sieben Monaten.</p>


<p>Der Filmzyklus beginnt mit einem Portr&auml;t des
Ferdinand Dierks, IG Metall Vertrauensk&ouml;rperleiter 
bei VW Emden, des gr&ouml;&szlig;ten Arbeitgebers der Region 
Ostfriesland. Das Portr&auml;t leitet
in die Chronologie der Ereignisse angesichts
der drohenden Produktionsverlagerung des
VW-Werkes Emden in die USA &uuml;ber. Die Vorbereitungen 
zu einer Protestkundgebung gegen
die Pl&auml;ne der Konzernspitze zeigen die 
unterschiedlichen Interessen von Gewerkschaftsbasis 
und der IG-Metall-F&uuml;hrung, die die Pl&auml;ne
des VW-Aufsichtsrats offenbar mittr&auml;gt.</p>


<p>"Emden geht nach USA" ist ein chronologisch
erz&auml;hltes Lehrst&uuml;ck &uuml;ber innergewerkschaftliche 
Machtstrukturen und den Prozess der
Willensbildung "von unten". Kommentar und
Textinserts fassen die Reportageszenen inhaltlich 
zusammen, liefern zus&auml;tzliche Informationen 
&uuml;ber den Aufbau und die Entscheidungsstrukturen 
einer Gewerkschaftsorganisation.
Diese Form wirkt heute streckenweise etwas
rigide, zu sehr vom Impetus des Didaktischen
gepr&auml;gt. Doch das intensive, in jeder Szene 
mitschwingende pers&ouml;nliche Interesse an den 
portr&auml;tierten Arbeitern, die atmosph&auml;rische 
Qualit&auml;t und Dichte der Beobachtung bebt den 
Emden-Filmzyklus &uuml;ber die exemplarische 
Innenansicht eines gewerkschaftlichen 
Willensbildungsprozesses hinaus.</p>


<h4>DER WEG IN DIE GESCHICHTE</h4>


<p>Nach dem Filmzyklus "Emden geht nach USA"
drehte Wildenhahn im Ruhrgebiet. Sein Weg
aus der norddeutschen Provinz in die ausgeblutete 
Metropole der Schwerindustrie und der Arbeiterbewegung 
war auch ein Weg aus der Aktualit&auml;t in die Geschichte, 
ein Weg von der dokumentarischen Chronologie der 
Gegenwart in die historische Spurensuche. In M&uuml;lheim an
der Ruhr lernte er den Arbeiter-Schriftsteller
G&uuml;nther Westerhoff kennen. Mit ihm zusammen 
machte er in den Jahren von 1979 bis 1981
drei Filme: "Der Nachwelt eine Botschaft",
"Bandonion I - Deutsche Tangos", "Bandonion II: Tango im Exil". 
Diese Trilogie ist stilistisch
ganz anders als der Emden-Zyklus: nachdenklicher, 
suchender, offener in der Form.
Sie beginnt mit einem Portr&auml;t des ehemaligen
Zechenschlossers Westerhoff. Er schreibt Gedichte und 
Kurzgeschichten. Gedichte, in denen er seine Erfahrungen, 
das Leben in der Zechensiedlung und am Arbeitsplatz dem 
Vergessen entreissen, wieder hervorholen und weitergeben 
will. Wildenhahn sieht und h&ouml;rt ihm zu,
zeigt seine Gedichte, liest sie vor, zeigt Westerhoff 
bei einer Lesung. Gemeinsam suchen sie
die Orte auf, auf die sich die Texte beziehen. Eine Art 
filmische Co-Lekt&uuml;re also, ein doppeltes
Erinnern. Ein Erinnern das schwer ist. Denn
nur noch Spuren verweisen auf jene Welt, die in
Westerhoffs Texten zu Sprache kommt. Zu
Bildern kommt sie nurmehr indirekt. Die Zeche, 
auf der Westerhoff gearbeitet hat, ist l&auml;ngst
abgerissen. Das gibt dem ersten Teil dieser
Film-Trilogie eine melancholische Grundstimmung. 
Die aber wird aufgehoben durch eine
Entdeckung, die Wildenhahns Portr&auml;t in eine
Recherche ausweitet: Westerhoff spielt Bandonion.</p>


<p>Wildenhahn begibt sich zusammen mit Westerhoff
in "Bandonion I" und "Bandonion II"
auf eine spannende Forschungsreise in die Geschichte 
dieses Musikinstruments, der Leute,
die es gespielt haben und noch spielen, und seiner Musik, 
dem Tango. Was Westerhoff mit
seinen Gedichten tat, greift Wildenhahn in den
beiden Bandonion-Filmen auf: er entrei&szlig;t die
Geschichte einer proletarischen Musikkultur
dem Vergessen, der kulturellen Anonymit&auml;t.
Wildenhahns Entdeckerfreude findet ihren
Ausdruck in der liebevollen Genauigkeit, mit
der er in "Bandonion II" den Komponisten
Mauricio Kagel und den ehemaligen Fliesenleger 
und examinierten Musiklehrer Klaus
Gutjahr, der in &uuml;ber 800 Stunden Arbeit selbst
ein Bandonion gebaut hat, im Gespr&auml;ch miteinander 
zeigt. Gutjahr erkl&auml;rt Kagel seinen
"Neubau": Man sieht Handwerker unter sich,
die voneinander lernen, Hier scheint eine Utopie
gegl&uuml;ckt, der "kulturelle Graben" zwischen
b&uuml;rgerlicher Avantgarde und Arbeiterkultur
&uuml;berwunden.</p>


<p>Die Besch&auml;ftigung mit dem Bandonion bringt
Wildenhahn ein Jahr sp&auml;ter, im Fr&uuml;hjahr 1982,
auf eine Kunstproduzentin, die ganz &auml;hnlich
wie G&uuml;nther Westerhoff ihre Kunst als &auml;sthetische 
Formulierung von Lebenserfahrung begreift: 
die in Wuppertal arbeitende Choreographin Pina
Bausch und ihr Tanztheater. Ihr
St&uuml;ck mit dem Titel "Bandonion" hat Wildenhahn 
neugierig gemacht, und diese Neugier annonciert 
sein Filmtitel "Was tun Pina Bausch und ihre T&auml;nzer 
in Wuppertal?" Pina Bausch
stellt Fragen an ihr Ensemble. Die T&auml;nzerinnen
und T&auml;nzer bekommen konkrete Aufgaben.
Sie sollen mit Gesten Gef&uuml;hle "erz&auml;hlen". Dieser 
Verarbeitungsprozess von gesellschaftlicher
Erfahrung in Kunstproduktion interessiert Wildenhahn. 
Er betrifft auch seine Arbeit als Dokumentarist ganz 
elementar. Denn er stellt in
diesem Film zwei Bereiche, die nichts miteinander zu 
tun haben, als Ausdruck unserer kulturellen Situation 
nebeneinander: das avantgardistische Tanztheater und die 
invalide Flie&szlig;bandarbeiterin Ruth Gr&uuml;n. Kunst und Arbeitswelt 
bleiben unvers&ouml;hnt. Aber: So wie Ruth
Gr&uuml;n in ihren Erz&auml;hlungen &uuml;ber die Flie&szlig;bandarbeit 
eine eigene Sprache f&uuml;r diese Lebenssituation 
findet, so findet sie Pina Bausch in ihren
Choreographien. Und was Bausch probiert,
probiert ebenso Wildenhahn: die Vermittlung
gesellschaftlicher Erfahrung in &auml;sthetische 
Praxis. Insofern betreibt dieser Film auch eine Art
Selbstreflexion.</p>


<p>Man kann dar&uuml;ber spekulieren, ob die Selbstreflexion, 
die Suche nach Vorbildern und
die Selbstvergewisserung dokumentarischen
Handwerks f&uuml;r Wildenhahn in_ genau dem Moment
wichtig wurde, als er feststellen musste,
dass er mit seinen bisher gedrehten Filmen &uuml;ber
die Arbeiterbewegung an einem Endpunkt 
angelangt war: den der st&auml;ndigen Reproduktion
ihres Untergangs. Seine zwei "Yorkshire"-Filme
&uuml;ber den englischen Arbeiterstreik 1984
und seine letzten beiden Ruhrgebietsfilme
"Stillegung" (&uuml;ber das Thyssen-Stahlwerk in
Oberhausen) und "Rheinhausen Herbst 1988"
vertrauen sich nicht mehr  einzelnen Protagonisten an. 
Sie ersetzen das Prinzip der Chronologie, der Identifikation mit 
den Protagonisten durch die mehrdimensionale subjektive Ann&auml;herung 
von aussen.</p>


<h4>HINWENDUNG ZU DEN PIONIEREN</h4>


<p>Die Selbstreflexion als Gespr&auml;ch &uuml;ber das
Handwerk und die Erinnerung an die Geschichte des 
Dokumentarfilms ist explizites Thema
im 1983 gedrehten "Film f&uuml;r Bossak und Leacock". 
Diese Reisereportage formuliert im Portr&auml;t dieser 
beiden Dokumentaristen seiner
"V&auml;ter", wie Wildenhahn sie nennt, so etwas
wie sein k&uuml;nstlerisches Credo. Er beschreibt
darin die Lehren, die er f&uuml;r sich selbst aus der
Arbeit des Polen Jerzy Bossak und des Amerikaners 
Richard Leacock gezogen hat.</p>


<p>Jerzy Bossak, den Pionier des polnischen Dokumentarfilms,
Mentor einer ganzen Generation polnischer Dokumentaristen, beobachtet
Wildenhahn w&auml;hrend der Oberhausener Kurzfilmtage im Gespr&auml;ch mit dem 
polnischen Experimentalfilm-Regisseur Rybczinsky. Es gebt
um die Dialektik von Form und Inhalt beim filmischen Handwerk. 
Bossak sagt zu Rybczinksy: <i>"Du musst nachdenken, den Inhalt finden,
dann werden wir &uuml;ber die Form sprechen.
Wenn Du nichts zu sagen hast, wirst Du nie
einen Film schaffen."</i> Dann Leacock, im Gespr&auml;ch mit 
Filmstudenten in seiner K&uuml;che, in Boston/Massachusetts:  
<i>"Entscheidend  ist, wollt ihr gestalten oder wollt ihr was 
entdecken?"</i> Und zu Wildenhahn: <i>"Fast alle meine
Studenten wollen letztendlich gestalten, sicher
sein, was passiert. Das ist langweilig."</i>
</p>


<p>Jerzy Bossak und Richard Leacock haben in
ihrem filmischen Handwerk eine individuelle
dokumentarische Haltung entwickelt. Das ist
bei Leacock die gleicherma&szlig;en technisch wie
journalistisch inspirierte Neugier des Entdeckers. 
F&uuml;r ihn gleicht dokumentarische Filmarbeit einer 
Entdeckungsreise: <i>"Wir waren wie
Diebe; abends f&uuml;hrten wir uns vor, was wir gestohlen hatten."</i>
So res&uuml;miert Leacock seine Art der leidenschaftlichen 
Wirklichkeitsbeobachtung bei den Dreharbeiten zu "Primary".
Einer Beobachtung, die dann in der Montage
zur spannenden Chronik gesellschaftlicher und
politischer Ereignisse verdichtet wurde.</p>


<h4>HISTORIKER UND ENTDECKER</h4>


<p>Bossaks Autorenhaltung bestimmt sich aus der
Moralit&auml;t des Historikers, der die filmisch dokumentierte 
Gegenwart (f&uuml;r ihn als Dokumentaristen waren das die letzten 
Kriegs- und ersten Nachkriegsjahre im zerst&ouml;rten Polen) dem 
kollektiven Ged&auml;chtnis der Gesellschaft aufbewahren m&ouml;chte. 
Eines Historikers freilich, den der Geschichtsprozess, 
retrospektivisch betrachtet, stets aufs Neue zum Pessimisten 
verurteilt. Er gleicht dem melancholischen "Engel der Geschichte", 
den Walter Benjamin in der neunten
These seiner Abhandlung "&Uuml;ber den Begriff der Geschichte" beschrieb. 
Jenem "Angelus Novus" des Bildes von Paul Klee, der in die Vergangenheit 
blickt und den ein Sturmwind - nach Benjamin "das was wir Fortschritt nennen" - 
aus dem Paradies in die Zukunft treibt, ohne dass er ihr je
ansichtig wird.</p>


<p>Beide Arbeitsweisen, die des Historikers wie
die des Entdeckers, der gerne auf Reisen geht,
hat Wildenhahn f&uuml;r sich n&uuml;tzlich machen k&ouml;nnen. 
Ein Pessimist ist er bis heute aber nicht
geworden.</p>


<p>Gegenw&auml;rtig arbeitet er an einem Film &uuml;ber ein 
Team von Bauarbeitern und Bauingenieuren, die in
Dresden historische Bauten restaurieren. Der
bisher volkseigene Betrieb wird derzeit unter
Beteiligung einer M&uuml;nchner Firma in eine Kapitalgesellschaft umgewandelt. 
Man darf gespannt sein auf Wildenhahns Blick in die
deutsch-deutsche Geschichte aus der Perspektive einer derzeit so 
spannenden Gegenwart. Auf dem Leipziger Dokumentarfilmfestival soll
der Film uraufgef&uuml;hrt werden.</p>


<p>Klaus Gronenborn</p>
    
<a name="sec-dokuessay"></a>
<h3>Dokumentarfilme - Vom Dokument zum Essay</h3>
      

<p>Am Anfang steht das Dokument. Ein historischer Augenblick, 
eine Alltagsaufnahme, ein Landschaftsbild oder ein Gespr&auml;ch.
Im Gegensatz zu einer Inszenierung im Theater
oder im Spielfilm soll das Dokument ein m&ouml;glichst
authentisches Abbild der Wirklichkeit sein.</p>


<p>Dass diese Authentizit&auml;t einen unerreichbaren
Idealfall darstellt, war den meisten Filmemachern
bereits fr&uuml;hzeitig klar. Die Diskussion dar&uuml;ber, ob
ein Dokument die Wirklichkeit ad&auml;quat abbilden
kann oder gar <i>soll</i>, f&uuml;llt B&auml;nde. Aber es ist unmittelbar 
verst&auml;ndlich, dass z.B. ein Bosnier andere Dokumente von 
Bosnien-Herzegovina aufnehmen und auswerten w&uuml;rde als ein 
Franzose - ganz egal, wie objektiv beide sein m&ouml;chten.</p>


<h4>Dokumente &amp; Dokumentarfilme</h4>


<p>Ebenso leuchtet ein, dass ein Dokument alleine
noch keinen eigenst&auml;ndigen Dokumentarfilm ergibt. 
Aus diesem Grunde z&auml;hlen Dokumente und
ihre nicht inhaltlich motivierte Aneinanderreihung
(Wochenschauen, Nachrichten o.a.) gemeinhin
nicht zu den Dokumentar<i>filmen</i>.</p>


<p>Zu diesen mehr oder weniger sinn- und lieblos 
aneinandergereihten Dokumenten z&auml;hlen auch
viele andere nicht-fiktive Filme: Tierfilme, in denen 
Heinz Sielmann an irgendwelchen Wuscheltieren 
herumfummelt oder die in den sechziger Jahren weitverbreiteten 
Reisefilme (TRAUMSTRASSEN DER WELT, FLYING CLIPPER etc.), die
den wiederaufbauenden Deutschen an exotischere
Schaupl&auml;tze als das Deutsche Eck entf&uuml;hren sollten. 
Obwohl gerade diese klischeehaften Postkartenfilme 
als 'Dokumentarfilme' firmierten, tendiert
ihr Wirklichkeitsbezug gegen Null.</p>


<p>Die neckische postmoderne Variante der Reise-
und Fluchtfilme in eine bessere Welt sind die
diffus-esoterischen Naturfilme &agrave; la KOYANISQATSI
(1984) oder BARAKA (1992), die in elegischen
Dokumenten die urt&uuml;mliche Natur preisen und
uns mit dem Holzhammer suggerieren, dass im
Get&uuml;mmel der Gro&szlig;st&auml;dte doch irgendetwas 
verloren gegangen sein muss.</p>


<p>Ebenfalls nicht-fiktiv aber ebenfalls kein 
Dokumentarfilm im eigentlichen Sinne ist der Lehrfilm. 
Diese Art von schulischem Zeitvertreib hat
wesentlich dazu beigetragen, dass der Dokumentarfilm 
ganz allgemein als belehrend, dr&ouml;ge, statisch
und altmodisch gilt. Interessant ist &uuml;brigens, dass
diese Meisterwerke an Sachlichkeit wie z.B. 
SCHAFSCHEREN IN WESTAUSTRALIEN, DIE SEGNUNGEN DER PETROCHEMIE 
oder DAS BEWEGENDE LEBEN DES MENDELEJEW beinahe 
ausschlie&szlig;lich im Kinder- und Jugendprogramm der 
Fernsehanstalten landen, sei es in der SENDUNG MIT DER
MAUS, sei es im TELEKOLLEG CHEMIE. Dass der
belehrende und informative Aspekt von Dokumentarfilmen 
bei weitem nicht der einzige und vor allem nicht der 
dominierende ist, verliert man beim
&Auml;lterwerden leicht aus den Augen.</p>


<p>Eine finale Steigerung des Lehrfilms stellt dann
der Industriefilm dar, der letztlich nichts anderes 
ist als ein auf Pseudoobjektivit&auml;t getrimmter
Werbefilm. Da so ziemlich jeder in seinem Leben
einem Lehr- oder Industriefilm beiwohnen musste,
aber nur wenige einen 'echten' Dokumentarfilm gesehen 
haben, verwundert es nicht, wenn viele ablehnend reagieren, 
wenn von Dokumentarfilmen die Rede ist.</p>


<h4>Der beschreibende Dokumentarfilm</h4>


<p>Im Gegensatz zum Postkarten-Reisefilm setzt sich
der ethnographische Film das Ziel, das Leben 'fremder' 
Kulturen durch teilnehmende Beobachtung und unter 
Einbeziehen der Betroffenen m&ouml;glichst objektiv zu 
dokumentieren. Dass dies ein frommer Wunsch ist, 
insbesondere wenn ein Europ&auml;er mit seiner Erziehung 
und seinem Hintergrund eine 'fremde Kultur' abbildet, 
soll hier nicht weiter ausgef&uuml;hrt werden.</p>


<p>Aber immerhin, der ethnographische Film ist
ein eigenst&auml;ndiger Dokumentar<i>film</i>, nicht mehr nur
ein Dokument. Diese vielleicht spitzfindig erscheinende 
Unterscheidung wurde bereits 1923 von
Dsiga Wertow in einer Theorie dokumentarischer
Filmpraxis erl&auml;utert:</p>


<p>
<i>"Bis auf den heutigen Tag haben wir die
Kamera vergewaltigt und sie gezwungen, 
die Arbeit unseres Auges zu kopieren. 
Und je besser die Kopie war, desto h&ouml;her 
wurde die Aufnahme bewertet. Von heute an werden wir die
Kamera befreien und werden sie in entgegengesetzter Richtung, 
weit entfernt vom Kopieren, arbeiten lassen. Alle
Schw&auml;chen des menschlichen Auges an den Tag bringen!"</i>
</p>


<p>Die Wirklichkeit wird also nicht - wie im einfachen 
Dokument - durch eine simple Abbildung
dargestellt, sondern zerlegt und analysiert. Das
Auge schaut nicht nur, der Kopf verarbeitet die
Bilder auch. Oder wie es die Theoretiker des britischen 
DOCUMENTARY MOVEMENT formuliert haben:</p>


<p>
<i>"We have to interpret creatively and in
social terms the life of the people as it
exists in reality."</i>
</p>


<p>Die 'Kreativit&auml;t' der FilmemacherIn besteht in
der Verwendung von filmischen Mitteln wie Montage, 
Bildkomposition, Auslassungen, Dramatisierung oder 
Gliederung. Dadurch erh&auml;lt der Film ein
Eigenleben und wirkt &uuml;ber seine Information hinaus 
durch einen dramatischen Zusammenhang auf
die Zuschauer.  Die 'Realit&auml;t' ergibt sich daraus,
dass der beschreibende Dokumentarfilm seinen 'Objekten' 
ein Forum gibt, sie vertritt und zu Wort
kommen l&auml;sst. Diese Art von Dokumentarfilmen,
die immer noch im wesentlichen Dokumente verwendet, 
diese aber inhaltlich und dramaturgisch
anordnet, kann man <b>beschreibende Dokumentarfilme</b> nennen.</p>


<p>Ein Beispiel f&uuml;r diese Art von Filmen w&auml;re
NANOOK OF THE NORTH, einer der ersten beschreibenden Dokumentarfilme 
und der erste ethnographische Film. Regisseur Flaherty hat nicht
wahllos sch&ouml;ne Aufnahmen von Eskimos aneinandergereiht, 
sondern seine Szenen vielmehr dramaturgisch montiert, 
ja den Eskimo Nanook nahezu zum Hauptdarsteller seines 
Filmes gemacht. Die Besch&auml;ftigung mit dem 'Dokument' seines Films
und das Einbeziehen der eigenen und der abgefilmten Personen macht 
aus der Ansammlung von Dokumenten einen Dokumentarfilm. Auf der anderen
Seite macht der dramaturgische Aufbau gerade diesen 
Film - aber auch viele andere beschreibende
Dokumentarfilme - immer mehr zu einem Spielfilm. In einen guten 
beschreibenden Dokumentarfilm werden die ZuschauerInnen genauso ihre 
Hoffnungen, Tr&auml;ume und Gedanken projizieren, wie sie
dies bei einem Spielfilm tun - der ungeheure Erfolg von 
NANOOK bei Presse und Publikum und die
Art und Weise, wie der Film aufgenommen wurde,
belegen dies. Dass der 'Held' des Films in seiner
fotogenen exotischen Wirklichkeit erfroren ist, hat
an dieser Wirkung nichts ge&auml;ndert.</p>


<p>Der beschreibende Dokumentarfilm ist aber keinesfalls 
auf die Vergangenheit beschr&auml;nkt. Noch heute gibt es viele 
und auch gute Dokumentarfilme, die nichts anderes tun, als 
ein bestimmtes Ereignis oder eine bestimmte Entwicklung zu 
dokumentieren.</p>


<p>Der Niedergang der osteurop&auml;ischen Gerontokratien und 
speziell f&uuml;r die BRD die Wiedervereinigung hat eine ganze Reihe 
von Dokumentarfilmen initiiert, welche ohne essay- oder collagenhaft
zu werden (zu diesen Begriffen sp&auml;ter mehr) die
Ereignisse ihrer Zeit b&uuml;ndeln und die Stimmungen
wiedergeben (LEIPZIG IM HERBST oder DRESDEN, OKTOBER 1989 u.v.a.).</p>


<p>Im Gegensatz zu Frankreich und auch Westdeutschland hat der eher 
assoziativ-k&uuml;nstlerische Essay in der DDR, sprich bei der DEFA, keine
Tradition gehabt. Insofern verwundert es nicht,
wenn der geradlinige schn&ouml;rkellose deskriptive Dokumentarfilm 
auch heute noch von vielen Filmemachern im Osten Deutschlands bevorzugt wird.</p>


<h4>Der Agitpropfilm</h4>


<p>Am Anfang dieses Textes wurde dargelegt, dass ein
Dokumentarfilm die 'Realit&auml;t' kaum objektiv abbilden kann, 
sondern sie letztlich kreativ und in ihren sozialen und 
kulturellen Zusammenh&auml;ngen intepretiert.</p>


<p>Diese Erkenntnis wurde sehr bald in die Forderung umgesetzt, 
dass ein Dokumentarfilm, ja &uuml;berhaupt jeder Film, eine Politisierung 
des gesellschaftlichen Bewusstseins zum Ziel haben sollte. 
In den zwanziger Jahren lehnten viele Filmemacher den
damals inhaltlich und formal an den Bed&uuml;rfnissen der b&uuml;rgerlichen 
Hochkultur orientierten Spielfilm ab.  Die Aufgaben des Mediums Film seien
weder Kunst noch Unterhaltung, sondern <i>Propaganda</i>. Auf den Dokumentarfilm bezogen: 
Ein Film ist nicht nur Information und Beschreibung,
sondern auch die <i>Erzeugung einer neuen Wirklichkeit</i>.</p>


<p>Wenngleich viele dieser Illusionen heute nur noch
im Poesiealbum der Weltrevolution stehen und sich
die proletarischen Massen eher f&uuml;r Sport&uuml;bertragungen 
als f&uuml;r ihre soziale Wirklichkeit interessieren, der <b>Agitprop</b>- oder 
Propagandafilm ist ein fester Bestandteil der Filmkultur vieler L&auml;nder
geworden. Und dies nicht (nur) im negativ-manipulativen Sinne wie es das Wort 
'Propaganda' suggeriert, sondern eher im Sinn einer politisch motivierten 
(Gegen-)Aufkl&auml;rung.</p>


<p>Vom beschreibenden Dokumentarfilm unterscheidet sich der Agitpropfilm 
durch seine eindeutige Absicht, die Zuschauer aufzukl&auml;ren oder vom
eigenen Standpunkt zu &uuml;berzeugen. Diese Absicht
ist von Anfang an zu erkennen und ein guter Agitpropfilm 
ist damit allemal ehrlicher als ein seri&ouml;s
verpackter, vorgeblich objektiver Dokumentarfilm,
der seinen Standpunkt verleugnet.  Wesentliches
Stilmittel des Propagandafilms - und gutes Unterscheidungskriterium 
gegen den beobachtenden Dokumentarfilm - ist die pointierte Montage der 
Dokumente (analog zur 'Montage der Attraktionen' im Spielfilm), die z.B. darin 
bestehen kann, dass zwei vollkommen gegens&auml;tzliche Dokumente so 
gegeneinander gestellt werden, dass die Aussage des Films 
offensichtlich wird. Der gesprochene Kommentar in Agitpropfilmen 
stellt meist eine deutlich gef&auml;rbte Meinungs&auml;usserung dar (eine Analogie
w&auml;re die Unterscheidung Nachricht - Kommentar,
die Zeitungsredaktionen zu treffen k&ouml;nnen glauben).</p>


<p>Vom Essayfilm hingegen unterscheidet sich der
Agitproptilm dadurch, dass er immer noch mit 'konventionellen Dokumenten' 
arbeitet (Nachrichtenbilder, Interviews, historische Dokumente, 
Alltagsszenen etc.) und diese nicht assoziativ verschl&uuml;sselt
montiert, sondern einer klar verst&auml;ndlichen, inhaltlich motivierten 
Dramaturgie folgt.</p>


<p>Der Film PAUL JACOBSs UND DIE ATOMBANDE
von Saul Landau z.B. ist in dem Sinne ein Propagandafilm, dass er 
Gegenpropaganda ist gegen eine angeblich neutrale und staatstragende 
'Wahrheit', die von (staatlichen) Medien und verschiedenen 
Interessengruppen beeinflusst, wenn nicht sogar definiert wird.  
Gerade die Problematik der Nutzung der Kernspaltung, die auch in 
diesem Film das Thema liefert, ist ein Paradebeispiel f&uuml;r 
permanent verf&auml;lschte Staats- und Industriepropaganda,
die zu einem ganzen Wust an Filmen gef&uuml;hrt hat,
in denen nett argumentierend oder deutlich polemisierend 
Gegenpropaganda - oder je nach Standpunkt: (Gegen)Aufkl&auml;rung - betrieben wird.</p>


<p>Im Zuge der vielf&auml;ltigen B&uuml;rgerrechts- und
Emanzipationsbewegungen sind schliesslich in den
letzten zwanzig Jahren gerade auch in Deutschland
eine Unmenge von Agitpropfilmen entstanden, die
sich direkt an die entsprechende 'Bewegung' richten, zentrale Ereignisse 
dokumentieren und zu Aktionen, Protest o.a. aufrufen. Diese 
Bewegungsfilme, teilweise faszinierende Zeitdokumente mit
politischem Anspruch, teilweise alternative Familienvideos mit 
Betroffenheitsappell, gab es zu jeder besseren Sitzblockade, jeder R&auml;umung von 
Jugendzentren und zu vielen Bauplatzbesetzungen. Diese
Filme haben in summa &uuml;brigens eine ganze Menge
Menschen erreicht und den Verleih sowie die Produktion von 
Dokumentarfilmen kurzfristig boomen
lassen. Offenbar waren diese Filme aber in ihrer
Mehrzahl nicht gut genug, um dauerhaft ein gr&ouml;sseres Publikum 
halten zu k&ouml;nnen. Auch der politisch
aufgekl&auml;rte Zeitgenosse scheut offenbar das Risiko
'Dokumentarfilm' und h&auml;ngt seinen Gedanken lieber in 
neuseel&auml;ndischen Klavierfilmen nach.</p>


<p>Auch OKTOBER von S. Eisenstein, der monumentale Film 
zur gleichnamigen Revolution, gilt
vielen heute als Propagandafilm - obwohl er
mit Schauspielern teilweise in Kulissen inszeniert
wurde. Letztlich ist es ja auch egal, ob Matrosen den 
Winterpalast st&uuml;rmen oder Schauspieler
in Matrosenkost&uuml;men, das Ziel des Filmes bleibt
das Gleiche: Preisen der Notwendigkeit und der
Errungenschaften der russischen Revolution. Dieser 
Spielfilm wollte authentische Bilder liefern, Geschichte 
nachstellen, und noch heute werden die
Bilder aus diesem Film in vielen ernsthaften Publikationen 
zur Geschichte der Oktoberrevolution
(unwissentlich) als 'Dokumente' verwendet.</p>


<p>Ein Propagandafilm im Wortsinne dagegen ist
DER EWIGE JUDE. Gerade dieser Film wurde
jedoch im Dritten Reich als 'authentische Dokumentation' 
&uuml;ber das Judentum angek&uuml;ndigt. Dies sollte bei den durch 
permanente Propaganda orientierungslos gewordenen Zuschauern besondere
Seriosit&auml;t suggerieren.  Einmal abgesehen davon,
dass viele der angeblichen 'Dokumente' gestellt waren, 
ist der Film durch Auswahl und Zusammenstellung dieser Bilder nichts 
anderes als eine propagandistische Bebilderung der deutschen 
Rassenideologie.</p>


<h4>Der Essayfilm</h4>


<p>Es gibt eine grosse Anzahl von Dokumentarfilmen, die
eine Fortentwicklung des rein abbildenden Dokuments und der 
analysierenden Beschreibung darstellen. Diese Filme wollen 
nicht die Wirklichkeit abbilden, sondern Bilder im Kopf der Zuschauerin
entstehen lassen. Bilder wecken Gef&uuml;hle und Assoziationen, 
die mit dem abgefilmten Sujet unmittelbar nichts mehr zu tun haben m&uuml;ssen.</p>


<p>Ein <b>Essayfilm</b> hat zwar meistens ein Thema
und verwendet manchmal 'konventionelle' Dokumente (Filmausschnitte, 
Gespr&auml;chsfetzen, Landschaftsaufnahmen, Fotografien o.a.), 
aber er hat nicht mehr den themenorientierten Duktus eines
Faktenfilms oder einer Beschreibung.</p>


<p>Ein Essayfilm ist subjektiv und m&ouml;chte Gedanken und Gef&uuml;hle 
visualisieren. Das Dokument wird dabei den Gedanken der Autorin untergeordnet.
Viele formale Merkmale, die viele Dokumentarfilme
auf den ersten Blick erkennbar machen, fehlen oder
fallen nicht ins Gewicht: statische, realistisch wirkende Aufnahmen, 
ordnender Kommentar, lineare Abfolge zusammenh&auml;ngender Dokumente. 
Charakteristisch f&uuml;r den Essayfilm ist neben der eher assoziativen 
Verarbeitung von Thema oder Idee ein hohes Mass an &Auml;sthetik und 
Formenbewusstsein.</p>


<p>Als Beispiel mag hier STEP ACROSS THE BORDER von Mumbert/Penzel dienen. Das 'Objekt'
des Films, der britische Musiker Fred Frith, wird
dem Publikum nicht nahegebracht &uuml;ber informative biographische 
Details aus seiner Kindergartenzeit, sondern &uuml;ber seine Musik und seine 
K&ouml;rpersprache.  Die Musik wiederum wird nicht einfach abgespielt, 
sondern bebildert. Collagenhaft reiht der Film lange Kamerafahrten, 
experimentelle Stadtansichten und Statements aneinander
und unterlegt diese Bilder mit einer Musik, die die
vielf&auml;ltigen Eindr&uuml;cke noch weiter verschwimmen
l&auml;sst. Vieles wird stilisiert und in Szene gesetzt
wie in einem Spielfilm. Humbert und Penzel zeigen
nicht die 'Wirklichkeit', sondern ihre Gedanken, die
sie haben, wenn diese Wirklichkeit auf sie wirkt.
Dies macht - wie bereits gesagt - im Prinzip jeder 
Dokumentarfilm, aber im Essayfilm ist dieser
Prozess inhaltlich und formal zu Ende gebracht.</p>


<p>Man kann im Moment den Eindruck gewinnen,
dass der eher linear aufgebaute und wenig verfremdete beschreibende 
Dokumentarfilm etwas aus der Mode gekommen ist und die meisten neueren 
Kino-Dokumentarfilme essayistischen Charakter haben
oder mit den formalen Mitteln des Spielfilms arbeiten. 
Dies bedeutet allerdings nicht, dass der Essayfilm eine Erfindung 
der 'Neuzeit' ist. Schon Dsiga Wertow ordnet seine Bilder einer These 
oder einem Kommentar unter. Im Prinzip handelt es sich
dabei um Essay-Filme, die Tatsache, dass die Dokumente ihren 
eigenst&auml;ndigen Charakter verlieren und nur Versatzst&uuml;cke eines gr&ouml;&szlig;eren Gedankens
sind, hat zur Bezeichnung <b>synthetischer Film</b>
gef&uuml;hrt. Solche Filme kommen zwar ohne Schauspieler und ohne 
gestellte Szenen aus, sind also keine Spielfilme, aber sie sind mindestens genauso
weit von einer Abbildung der Wirklichkeit entfernt
wie diese.</p>


<p>Historische Marksteine dieses synthetischen
Films waren KINOGLAS von Dsiga Wertow (1923)
und THE SPANISH EARTH von Joris Ivens (1937).</p>
    
<a name="sec-drama"></a>
<h3>Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?</h3>

<h4>Die Akteure:</h4>

<p>SERGEJ M. EISENSTEIN, 95 Jahre, einer der
bedeutendsten Spielfilmregisseure aller Zeiten.
Tr&auml;ger des Titels <i>verdienter Arbeiter der Kunst</i>
(1935) und des <i>Stalin-Preises 1.Klasse</i>. Aus
seiner Theaterarbeit hatte E. eine experimentelle, 
mit vielen Zirkuselementen versehene
(also "zirzensische") Inszenierung entwickelt.
1923 entwarf er seine Theorie der <i>Montage
der Attraktionen</i>. Unter ATTRAKTIONEN_ verstand E. 
<i>"...jedes aggressive theatralische Element, 
jedes Element, das die Gedanken und die Psyche des Zuschauers 
beeinflusst"</i>; mit derartigen "mathematisch berechneten" 
Attraktionen wollte er seine Zuschauer wachr&uuml;tteln
und sie auf den Weg der Erkenntnis f&uuml;hren.
Typische Merkmale seiner Filme aus den 20er
Jahren sind das Fehlen von "b&uuml;rgerlichen"
Helden; daf&uuml;r erscheint die Masse als neuer
Held auf der Leinwand. Sein erster Langfilm STREIK (1925) 
enth&auml;lt trotz seines ernsten Themas - blutige Niederschlagung 
eines Streiks in einer vorrevolution&auml;ren
Fabrik - noch viele skurrile, sogar humorvolle Elemente; 
so werden die verschiedenen Polizeispitzel mit Tieren verglichen 
und in absonderlichen Verkleidungen vorgef&uuml;hrt, die
man sogleich durchschaut. F&uuml;r ihre Provokationen bedient sich die 
Polizei einer Armee der Unterwelt, die in Tonnen haust, 
akrobatische Kunstst&uuml;ckchen vollf&uuml;hrt und geradewegs 
der DREIGROSCHENOPER entsprungen scheint. Weitere Filme E. sind 
PANZERKREUZER POTEMKIN mit der ber&uuml;hmten
Treppenszene in Odessa, OKTOBER, ALEXANDER NEWSKIJ und 
IWAH DER SCHRECKLICHE.</p>


<p>DZIGA  WERTOW, 97 Jahre, begr&uuml;ndete
1923 die Theorie des Dokumentarfilms mit
seinem Manifest KINOKI.UMSTURZ. <i>Kinoki</i>,
w&ouml;rtlich Filmaugen, nannte sich die Herstellergruppe 
der von W. geleiteten staatlichen
Wochenschau, doch verstand sich diese Gruppe
als Vorhut einer Massenbewegung, durch die
sich das Filmwesen entsprechend den Erfordernissen 
der proletarischen Revolution neu
organisieren w&uuml;rde. Weitere Lieblingsbegriffe
dieser Gruppe sind Kinoglas = Filmauge
und Kinoprawda = Kinowahrheit, gleichzeitig
auch der Titel von Wertows dokumentarischer
Wochenschau ab 1922. Aus der Arbeit der
Wochenschauen und der Agit-Z&uuml;ge (Eisenbahnz&uuml;ge, 
die, mit einer Druckerei und kompletten Filmeinrichtungen 
versehen, an die Fronten des B&uuml;rgerkriegs entsandt wurden,
um dort unter den Truppen revolution&auml;re
Aufkl&auml;rungsarbeit zu leisten und gleichzeitig
Wochenschauszenen zu filmen) entwickelte W.
besonders die <i>formalen</i> Mittel des Dokumentarfilms, 
z.B. Gro&szlig;aufnahmen und die rhythmische Gliederung einzelner 
Sequenzen als Mittel der Aussage. Gleichzeitig wurde er ein
unerbittlicher Verfechter der Auffassung, dass
jegliche fiktive Handlung im Film prinzipiell
sch&auml;dlich und abzuschaffen sei. Neben vielen Dokumentarfilmen 
im Rahmen der Kinoprawda wurden vor allem zwei Langfilme
ber&uuml;hmt: DER MANN MIT DER KAMERA (1929) und EIN SECHSTEL DER ERDE (1926).
W. wurde 1935 der bedeutendste sowjetische
Filmorden, der <i>Rote Stern am Band</i>, verliehen.</p>


<table summary="">
	
<speaker name="Eisenstein" id="e"></speaker>
	
<speaker name="Wertow" id="w"></speaker>
	
<speaker name="Gott" id="g"></speaker>
	
<tr>
<td colspan="2"><i>Dziga Wertow und Sergej Eisenstein in der H&ouml;lle, 
wo ihnen in einer Endlosschleife der neue Wenders-Film vorgef&uuml;hrt wird. 
Anl&auml;sslich eines Hafturlaubs treffen sie sich dort zu einem
Spaziergang im Park und streiten sich, wie so oft, &uuml;ber einen alten 
Aufsatz Wertows von 1925, in dem er Eisensteins neuen Film 
STREIK kritisiert</i></td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">F&uuml;r mich ist es ziemlich egal, mit welchen 
Mitteln ein Film arbeitet, ob er ein Schauspielerfilm ist mit 
inszenierten Bildern oder ein Dokumentarfilm. In einem guten Film
geht es um die Wahrheit und nicht um die Wirklichkeit.</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top"><i>(zitiert aus seinen Werken)</i>;
Das Objektiv ist pr&auml;zis, unfehlbar und muss mitten in den Ereignissen, 
den wirklichen Geschehnissen sein: Aufnahmen ausserhalb der Ateliers, 
ohne Berufsschauspieler, Drehb&uuml;cher, Sujets und gebaute Dekorationen. 
In diesem Sinne ist dein STREIK ein Versuch, einige Konstruktionsmethoden 
der KINOPRAWDA und des KINOGLAS dem Spielfilm aufzupfropfen. 
Montageaufbau, Auswahl der aufzunehmenden Einstellungen und Komposition 
der Zwischentitel in diesem Film sind dem KINOKI verwandt. Zutiefst 
fremd ist den KINOKI die schauspielerische Darstellung, auf die sich 
dein Film gr&uuml;ndet. Zutiefst fremd ist ihnen jedes theatralisch-zirzensische 
Moment, jede artifizielle dekadente Hypertrophie...
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top"><i>(unterbricht)</i> 
Ist ja schon gut, ich kenne deine verbitterte Lamentiererei gegen alles, 
was nicht das "Leben so zeigt wie es ist", gegen die "stumme Ekstase" und 
das "Theater f&uuml;r Dummk&ouml;pfe". Aber solltest du nach 7O Jahren nicht so 
langsam gemerkt haben, dass das Objektiv niemals pr&auml;zis und unfehlbar sein 
kann. Blo&szlig;es Abfilmen der Wirklichkeit allein offenbart noch keinesfalls 
die Wahrheit in der Wirklichkeit. Schon in dem Moment, in dem du die 
Kamera auf ein Motiv h&auml;ltst, hast du eine subjektive Auswahl getroffen. 
Gerade du, Genosse Wertow, solltest wissen, dass auch bei sogenannten 
Dokumentarfilmen der Manipulation T&uuml;r und Tor ge&ouml;ffnet sind.
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Nat&uuml;rlich kann sich der Zuschauer nie vor 
gestellten Bildern und anderer Irref&uuml;hrungen sch&uuml;tzen. Aber ich bleibe dabei: 
ein Dokumentarfilm soll zun&auml;chst eine Abbildung der Wirklichkeit sein.
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">Ein Dokumentarfilm kann aber auch sehr subjektiv 
sein, ja sogar Gef&uuml;hle vermitteln. Es gibt sehr pers&ouml;nliche 
Erfahrungsberichte von Filmemachern, mit ganz klar subjektivem
Standpunkt. Und wer w&uuml;rde bei einem Dokumentarfilm &uuml;ber die wunderbaren 
Landschaften auf der Krim nicht geradezu romantisch werden?
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Das ist nat&uuml;rlich richtig - auch mir wollen jedes 
Jahr wieder die Tr&auml;nen der R&uuml;hrung kommen, wenn ich das Volk geschlossen 
an der Erntefront sehe. Aber das sind Empfindungen, die erst im Zuschauer 
ausgel&ouml;st werden. Die Bilder selbst sind gewissermassen noch neutral.
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top"><i>(Schiebt gedankenverloren einen herumstehenden 
Kinderwagen hin und her)</i> Man kann aber leicht seine Aufnahmen so 
montieren, eventuell noch mit einer geeigneten Musik unterlegt, dass schon 
der Film keineswegs mehr neutral, sondern z.B. &auml;u&szlig;erst pathetisch wirkt.
Damit habe ich Erfahrung! Und was unterscheidet dann eigentlich einen 
Dokumentarfilm noch von einem Spielfilm, wenn er sowohl subjektiv als auch 
objektiv ist, die Wirklichkeit abbildet und auch Gef&uuml;hle vermittelt?
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Gerade hier sehe ich noch wichtige Unterschiede: 
ein wesentliches Merkmal jedes Spielfilms ist die 
<i>dramatische Verdichtung</i> der Geschehnisse. Der Dokumentarfilm dagegen 
soll wirkliche Ereignisse aufzeichnen, ohne etwas wegzulassen, und das oft 
auch noch in Echtzeit.
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">Das ist wieder dein Altersstarrsinn. Nur weil 
du dich weigerst, andere als deine eigenen Dokumentarfilme anzusehen, 
ist dir wohl entgangen, dass_jeder gute Dokumentarfilmer sein
Material gliedert und eine Ordnung oder eine Tendenz in seinen Film bringt. 
Das hat schon der gute Flaherty in NANOOK gemerkt, aber auch f&uuml;r WOODSTOCK 
z.B. haben Michael Wadleigh, Martin Scorsese und Thelma Schoonmaker aus 
Tonnen von Originalmaterial einen Film nach ihren Vorstellungen mit ihrem 
Rhythmus gebaut. Nat&uuml;rlich sind auch solche Filme <i>dramatisch verdichet</i>.
Der einzige Unterschied ist der, dass beim Spielfilm die Handlung und die 
Gliederung schon vorher feststehen, wohingegen beim Dokumentarfilm
erst im Nachhinein dramatisiert wird.
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top"><i>(genervt)</i> 
Gut, ich mach's dir noch einfacher: der Spielfilm verwendet Schauspieler, 
der Dokumentarfilm nicht! Wenn ich an deine ganzen Schmachtfilme denke, 
die du mit unf&auml;higen Schauspielern f&uuml;r Stalin gedreht hast - was hat das 
noch mit Wahrheit, was hat das noch mit der Wirklichkeit zu tun?
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">Lass gef&auml;lligst mein Sp&auml;twerk aus dem Spiel! 
Aber wenn du den Einsatz von Schauspielern als Unterscheidungskriterium 
anf&uuml;hrst, beziehst du dich auf reine &Auml;u&szlig;erlichkeiten. Da kannst du ja 
gleich sagen: einen Dokumentarfilm erkennt man an der verwackelten 
Kamera, dem grobk&ouml;rnigen Film, schlechten Ton, ins Bild h&auml;ngenden 
Mikrophonen usw. Das ist nicht mehr besonders glaubw&uuml;rdig, seit in 
zahllosen Spielfilmen mit diesen Klischees munter gespielt 
wird - "pseudodokumentarische" Filme nennt man das wohl. Schau dir
dazu blo&szlig; HUSBANDS AND WIVES von Woody Allen an!
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Ich schaue mir diesen neumodischen Kram nicht an. 
Die formalen Kriterien taugen sicherlich nicht allein dazu, einen 
Dokumentarfilm zu charakterisieren. Aber es bleibt doch f&uuml;r einen 
&uuml;berwiegenden Teil der Dokumentarfilme das Kennzeichen, dass der 
Produktionsprozess des Films nicht kaschiert wird - wenn z.B. der 
Filmemacher bei Interviews die Fragen vor der Kamera stellt. Wichtiger 
noch scheint mir, dass auf Effekte weitgehend verzichtet wird, die die 
Zuschauer in die Handlung 'saugen', also etwa technische oder k&uuml;nstlerische 
Tricks, Licht- und Toneffekte oder &uuml;berhaupt Filmmusik.
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">Wenn man die Unzahl von mittelm&auml;&szlig;igen 
Interviewfilmen und Reportagen im Fernsehen betrachtet, hast du sicher recht. 
Aber die formalen Kriterien allein treffen eben noch keine klare 
Unterscheidung. Ich kann mich nur wiederholen: ein Spielfilm kann wesentlich 
"dokumentarischer" sein als mancher nach deinen Reinheitsgeboten gedrehter 
Dokumentarfilm. Ein Film ist <i>dann</i> objektiv und dokumentarisch, wenn er 
die <i>Wahrheit</i> zeigt, aber noch lange nicht, wenn er blo&szlig; 
<i>Wirklickeit</i> abbildet. Nicht umsonst finden sich Szenen aus
meinen - inszenierten - Filmen heutzutage in Geschichtsb&uuml;chern...
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top"><i>(wirft ein)</i>...und in zehn Jahren findest 
du Standbilder aus JURASSIC PARK in den Biologieb&uuml;chern...
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top"><i>(l&auml;sst sich nicht aus dem Konzept bringen)</i>
Dokumentarisch ist, was einen realen Sachverhalt ausdr&uuml;ckt. Es kommt nicht 
so sehr auf das Dokumentarische der Objekte und Menschen an - das ist 
sowieso eine Fiktion, der Manipulation sind keine Schranken 
gesetzt - sondern vielmehr auf das Dokumentarische des dargestellten 
Faktums selber, das dann eigentlich in jeder Beziehung rekonstruiert sein 
kann.</td>
</tr>

	
<tr>
<td valign="top">Gott:</td><td valign="top">... H&auml;h?</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top"><i>(l&auml;sst vor Schreck den Kinderwagen los, der eine 
in der Gegend herumstehende Treppe hinunterrollt)</i> 
Um Himmels Willen, hast du mich erschreckt... Also pass auf, ich werde dir
nun ein Gleichnis erz&auml;hlen: hast du den Film RASHOMON von Akira Kurosawa 
gesehen?  ... Wahrscheinlich nicht, schlie&szlig;lich ist der Regisseur ja Heide. 
Jedenfalls geht es in diesem Film um einen Mord, der von vier verschiedenen 
Zeugen auf jeweils verschiedene, jedoch f&uuml;r sich vollkommen schl&uuml;ssige 
und glaubw&uuml;rdige Art berichtet wird. Nur am Rande: einige Jahrzehnte sp&auml;ter 
wird der <i>Dokumentarfilm</i> THE THIN BLUE LINE dasselbe dramaturgische
Mittel verwenden. Stell dir vor, einer dreht nun einen Dokumentarfilm, in 
dem er nur einen Zeugen &uuml;ber den Mord befragt; zweifellos w&uuml;rden damit Dzigas 
Kriterien - authentische Schaupl&auml;tze, am tats&auml;chlichen Geschehen beteiligte 
Menschen - erf&uuml;llt, doch k&auml;me dabei nicht einmal die halbe Wahrheit heraus. 
Mir scheint als Maxime f&uuml;r den Dokumentarfilm die <i>Wahrheit</i> wichtiger 
als die platte Wirklichkeit, auch wenn dabei mit Schauspielern und
im Studio nachgestellten Szenen gearbeitet wird.
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Das geht mir eindeutig zu weit; es gibt einfach den 
Unterschied zwischen 'fiction' und 'nonfiction', wie unsere amerikanischen 
Kollegen sagen w&uuml;rden. Aber vielleicht kommen wir auf einem anderen Weg 
weiter, n&auml;mlich wenn wir danach fragen: "Was will der Regisseur?" und: 
"Was findet das Publikum?".
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">Ja, was will der Regisseur? Manipulation des
Publikums z.B., da sind wir uns sicher einig.</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Bei meinem <i>Roten Stern am Band</i>, das will 
ich meinen!</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">Gut, Manipulation ist ein Punkt. Ein anderer, der 
f&uuml;r die meisten Dokumentarfilme in der Schule und im Fernsehen von gro&szlig;er 
Bedeutung ist, ist die Vermittlung yon Wissen. Wissen &uuml;ber andere L&auml;nder, 
&uuml;ber Tiere, &uuml;ber Geschichte oder politische Fragen. Viel interessanter 
scheint mir zu sein, dass gerade die guten Kino-Dokumentarfilme mehr wollen, 
als nur Wissen zu vermitteln! Genauso wie in Spielfilmen sollen in guten 
Dokumentarfilmen Gef&uuml;hle geweckt werden, Tr&auml;ume und Assoziationen und 
Ahnungen. Umgekehrt nutzen jedoch auch viele fortschrittliche Filmemacher 
den Spielfilm als Vehikel, um auf unterhaltende Weise Meinungen und Wissen 
zu transportieren.
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Fortschrittlich ist allein der Verzicht auf die 
Filmzauberei. Ich kann es nur immer wieder wiederholen: Nieder mit der 
Verszenung des Alltags! F&uuml;r eine wahrhafte Kinofizierung!
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top"><i>(beruhigt ihn)</i> 
Ja, ja, das Filmdrama ist Opium f&uuml;r das Volk... aber noch einmal zur&uuml;ck
zur Frage, warum denn die Zuschauer in Dokumentarfilme gehen.
</td>
</tr>

	
<tr>
<td valign="top">Gott:</td><td valign="top"><i>(mischt sich ein)</i> 
Jetzt seid ihr schon so lange hier unten und k&ouml;nnt in Ruhe hinter das
Geschehen blicken - und ihr habt immer noch nicht kapiert, dass die 
Menschen eben bescheuert sind! Sie gehen nur noch in Dokumentarfilme, 
wenn sie zuf&auml;llig das Thema interessiert. Fr&uuml;her war es die politische 
und soziale Situation der Massen, heute scheinen es eher Musikgruppen, 
Autorennen oder Reiseziele zu sein...
</td>
</tr>


	
<tr>
<td valign="top">Wertow:</td><td valign="top"><i>(fl&uuml;stert EISENSTEIN zu)</i> 
Der Alte ist wieder mal geladen... wahrscheinlich sind ihm gerade 
seine letzten Sch&auml;fchen davongelaufen... <i>(laut, den Blick nach oben)</i> 
jaja, dass der Dokumentarfilm auch formal brillant sein kann, attraktiv 
wie SERGEJS vielger&uuml;hmten Filme und genausowenig langweilig, das scheint 
sich leider noch nicht herumgesprochen zu haben.
</td>
</tr>

	
<tr>
<td colspan="2"><i>(...Leider werden Eisenstein und Wertow hier abberufen 
zu den Dreharbeiten an dem Passionsfilm im im Fegefeuer, PURGATORY PARK).
</i></td>
</tr>

</table>
   
    
<a name="sec-bitomsky"></a>
<h3>Spurensuche<br>
<small>Der Filmemacher Hartmut Bitomsky</small>
</h3>


<p>In den Reihen all der ungest&uuml;men (Dokumentar-)Filmer, die momentan das 
Ph&auml;nomen der deutschen Wiedervereinigung in Bildern zu begreifen versuchen,
d&uuml;rfte Hartmut Bitomsky nicht zu finden sein. Bitomsky hat stets der 
Versuchung widerstanden, dem jeweiligen "Ruf der Geschichte" nachzulaufen. 
Sein filmisches Interesse setzt vielmehr da ein, wo sich diese Ereignisse 
zu Geschichten verdichten, die sich in den unterschiedlichsten Archiven 
als Profan-Mythen ablagern. Seit nunmehr 20 Jahren versucht er in seinen 
Filmen beharrlich diese Archive, die seit Beginn dieses Jahrhunderts auch 
Archive des Kinos sind, zu entdecken, zu verstehen, wie sie aufgebaut werden, 
wie sie funktionieren, um dann unter ihren Verzerrungen und F&auml;lschungen 
Spuren einer Wirklichkeit freizulegen, der so einfach nicht beizukommen ist.
</p>


<p>l942 in Bremen geboren, studierte Harmut Bitomsky ab 1962 zun&auml;chst an der 
FU Berlin Germanistik, Theaterwissenschaft und Publizistik und wechselte 
1966 an die neugegr&uuml;ndete "Deutsche Film- und Fernsehakademie". Zwei
Jahre sp&auml;ter, auf dem H&ouml;hepunkt der Studentenrevolte, wurde er zusammen 
mit anderen Kommilitonen wegen politischer Aktivit&auml;ten
relegiert. Seitdem lebt er als Autor und freier Filmemacher in Berlin. 
1970 drehte er zusammen mit seinem Freund und langj&auml;hrigen Weggef&auml;hrten 
Harun Farocki (vgl. "Eine Anstrengung - &uuml;berfl&uuml;ssig?" in <i>"film-dienst"</i> 10/1990, Seite 14) seinen ersten Film f&uuml;r den Westdeutschen Rundfunk: 
"Die Teilung aller Tage", mehr eine gefilmte denn eine filmische 
Einf&uuml;hrung in die Grundbegriffe der marxistischen Theorie, in Anlehnung an 
Bert Brecht mit "Lehrfilm" untertitelt. F&uuml;r denselben Sender
entstanden bis heute 14 weitere Produktionen. Darunter sogenannte 
"medienkritische Filme" wie "Kressin und..." (1973) oder "Kino/Kritik"
(1974), essayistische Portr&auml;ts des englischen Dokumentarfilmers 
Humphrey Jennings (1975, bzw. 1976), eine Studie &uuml;ber John Ford (1976), 
der zweiteilige Spielfilm "Karawane der W&ouml;rter" (1977), die vierteilige 
Dokumentation einer Reise auf dem legend&auml;ren nordamerikanischen 
"Highway 40 West" (1980/81) und schlie&szlig;lich zwei Filme, die den Formen 
und der Funktionsweise  der faschistischen &Auml;sthetik nachsp&uuml;ren - nicht 
im Bereich des Milit&auml;rischen, wo sie nach landl&auml;ufiger Meinung ihre
deutlichste Auspr&auml;gung fand, sondern im vermeintlich "Zivilen", wo sie ihre 
gr&ouml;&szlig;te Effizienz hatte.</p>


<h4>SPUREN IM "KULTURFILM"</h4>


<p>In "Deutschlandbilder" (1983) sch&auml;lt Hartmut Bitomsky aus sogenannten 
"Kulturflmen", die damals zur unverf&auml;nglichen "Erbauung" regelm&auml;&szlig;ig 
im Vorprogramm der Kinovorstellungen liefen, ein in h&ouml;chstem Ma&szlig;e 
ideologisch verf&auml;rbtes Deutschland-Bild heraus, das die Nazis
dem Volk auf subtile Weise als sein eigenes pr&auml;sentierten. Bitomskys 
Fazit: <i>"Die Kulturfilme funktionierten wie ein umgekehrtes Plebiszit:
das Regime best&auml;tigt sein Volk, weil es sich so anstellig zeigt und 
schaffensfroh mitmacht."</i>
</p>


<p>"Reichsautobahn" (1986) verdankt sich urspr&uuml;nglich dem Umstand, 
dass Bitomsky bei der Sichtung jener "Kulturfilme" st&auml;ndig auf Episoden 
stie&szlig;, die sich eigentlich nur als Werbefilme f&uuml;r dieses, damals in Teilen 
fertiggestellte, Gro&szlig;projekt nationalsozialistischer Verkehrsplanung 
verstehen lie&szlig;en.</p>


<p>Wozu braucht eine Autobahn, nahezu der Inbegriff blanker Funktionalit&auml;t, 
"Werbung"? Einmal neugierig geworden, stie&szlig; Bitomsky auf andere 
Ungereimtheiten eines Bauwerkes, dessen Qualit&auml;ten eher im Mythischen, 
denn im Funktionalen lagen. Als Arbeitsbeschaffungsma&szlig;nahme blieb ihre 
Auswirkung auf den Arbeitsmarkt bescheiden. Verkehrspolitisch war
sie unsinnig, da die Nazi-Parole von der "mobilen Gesellschaft" vorerst 
nicht mehr als eine k&uuml;hne Vision war. Mit einem Belag, der selbst f&uuml;r 
LKWs zu d&uuml;nn war, konnte ihr Bau auch nicht unter milit&auml;rstrategischen 
Gesichtspunkten erfolgt sein. Wie Bitomsky mit einer F&uuml;lle von 
Archivmaterial belegt, wurde <i>"die Autobahn... gleich von Anfang an 
zu einem k&uuml;nstlichen Gegenstand erhoben. Sie wurde von Malern gemalt, 
von Photographen photographiert, von Dichtern besungen, von Romanciers 
beschrieben. (...) Die Filme, Gem&auml;lde, Photos, B&uuml;cher und Gedichte hielten 
als ihre Fassade her. Ein potemkinsches Projekt"</i>. Die betreffenden 
"Kulturfilme" waren also nicht Werbefilme f&uuml;r die neue Autobahn, sondern 
nur Teil eines gigantischen Werbefeldzuges mit dem Titel "Reichsautobahn".</p>


<p>Zumindest in thematischer Hinsicht kn&uuml;pft Hartmut Bitomsky in seinem 
bisher letzten Film "Der VW-Komplex" (1989) an seinen Vorg&auml;nger an. Es geht 
erneut um Autos und um Stra&szlig;en. Er begibt sich in jene Produktionsst&auml;tte, 
die von Hitler gegr&uuml;ndet wurde, seine Vision des "mobilen Volkes" zwar 
programmatisch im Namen tr&auml;gt, sie jedoch erst in der Nachkriegszeit zur 
Realit&auml;t werden lassen sollte. Wie die Reichsautobahn nicht darin aufging,
dass auf ihr Autos fuhren, ist "VW" nicht auf einen Ort, eine Stadt zu 
reduzieren, wo Autos hergestellt werden. Bitomsky verbindet Analyse mit 
freier Assoziation, kontrastiert die vollmundige Firmen-Philosophie der 
"Verbindung von Vergangenheit und Zukunft" mit Archivbildern, die das 
offiziell gern ausgesparte einklagen. Aber ebenso irritiert wie fasziniert 
folgt er auch der Stra&szlig;e, auf der heutzutage Autos hergestellt werden, 
bevor sie auf anderen als Schrotthaufen enden k&ouml;nnen: der gespenstische 
Prozess der vollautomatischen Produktion.</p>


<p>In "Reichsautobahn" hatte Bitomsky aus einem zeitgen&ouml;ssischen Buch 
&uuml;ber den Autobahnbau zitiert: <i>"Nirgends verdr&auml;ngt jedoch die 
Maschine die menschliche Arbeit. (...) Und wenn man in dem weitverstreuten 
Gel&auml;nde auch nur wenige Menschen zu erblicken glaubt - sie sind da."</i>
Nicht der Entwicklungsstand damaliger Technologie machte dieses Statement 
erforderlich, sondern die ideologische Parole vom Volk, das emsig seine 
Zukunft gestaltet. Auch im heutigen VW-Werk ist der Arbeiter nat&uuml;rlich noch
nicht "ausgestorben". Aber die Kamera muss ihn suchen und findet ihn als 
Appendix einer gigantischen Maschinerie.</p>


<h4>PRODUKTIONSBEDINGUNGEN DER (KINO-)WIRKLICHKEIT</h4>

<p>Bitomsky hat als Filmemacher im Laufe der Jahre eine eigenst&auml;ndige 
Filmsprache und Methodik entwickelt, in die seine gleichzeitige Arbeit 
als Filmtheoretiker eingeflossen ist. Er gab die Schriften von And&eacute;i Bazin 
und Bel&aacute; Bal&aacute;sz heraus und war langj&auml;hriger Redakteur der 1984 eingestellten 
Zeitschrift "Filmkritik". 1972 erschien sein Buch <i>"Die R&ouml;te des Rots 
von Technicolor. Kinorealit&auml;t und Produktionswirklichkeit."</i>, in dem er 
die marxistische Filmtheorie mit dem Ansatz des Strukturalismus zu verbinden 
versucht. Dort schreibt er: <i>"Eine vornehmlich ge&uuml;bte Form der 
Ideologiekritik an Kunstwerken ist puritanisches Erbe, sie sch&auml;tzt alles
Vergn&uuml;gen gering, und sie kann sich das Vergn&uuml;gen, das ein Publikum mit 
einem Film hat, &uuml;berhaupt nicht erkl&auml;ren: weil sie nur das am Film 
wahrhaben will, was man nicht sehen kann - die Ideologie. Diese 
Ideologiekritik liest den Film wie einen heimlich in den Film 
hineingesteckten Kommentar &uuml;ber die Realit&auml;t; sie liest 
den Film nicht."</i>
</p>


<p>Diese Anstrengung, sich auf die Materialit&auml;t der origin&auml;ren Filmsprache 
(oder auch die "filmische Realit&auml;t") einzulassen, hat Bitomsky seinen 
Zuschauern immer wieder abverlangt. Freilich nicht, um eine jeder 
Wirklichkeit entr&uuml;ckte Welt des sch&ouml;nen Scheins nahezubringen, sondern 
um die Tauglichkeit des Films als eine eigenst&auml;ndige Form der Erkenntnis 
und der Erfahrung zu sichern, die als solche grunds&auml;tzliche 
Konstruktionsprinzipien von "Wirklichkeit" sichtbar machen kann. In dieser
Doppelbewegung ist Bitomsky in seinen Filmen gleicherma&szlig;en am mythischen 
Gehalt des Realen wie am realen Gehalt des Mythischen interessiert. 
Dabei kann seine Arbeit nicht die eines Interpreten sein, der etwas zu 
erkl&auml;ren verm&ouml;chte, sondern nur die eines Dekonstrukteurs, der die 
materiellen Schichten scheinbar evidenter Bedeutungen freilegt. 
(<i>"Die Geschichte gegen den Strich b&uuml;rsten"</i>, hat Walter
Benjamin das einmal genannt.) In seinem einzigen f&uuml;r das Kino gedrehten 
Spielfilm, "Auf Biegen oder Brechen" (1974), hat Bitomsky versucht, 
diesen Grad der Reflexivit&auml;t mit dem subjektiven (Er-)Leben und der 
"Lust am Kino" zu verbinden. Eine geradlinige Story, die trotzdem nicht 
die trivialen Muster fiktionaler Komplexit&auml;tsreduktion wiederholt. Der 
Versuch, diesen Gordischen Knoten mit redlichen Mitteln zu l&ouml;sen, 
fiel nicht sonderlich &uuml;berzeugend aus.</p>


<h4>DIE BEWEGUNG IM STATISCHEN</h4>

<p>Das eigenwilligste Verfahren, das Hartmut Bitomsky in seinen Filmen 
entwickelt hat, scheint fast aus einem Misstrauen gegen&uuml;ber dem eigenen 
Medium, dem Film, zu resultieren. Immer wieder l&ouml;st er Sequenzen in einzelne 
Fotos (nicht Standbilder!) auf, die er dann vor seine Filmkamera h&auml;lt. Das 
"Einfrieren" der Bewegung hat jedoch erstaunlicherweise keine Verflachung, 
sondern eine Intensivierung zur Folge. In "Reichsautobahn" wird zun&auml;chst 
eine Sequenz gezeigt, die einen emsig schaufelnden Adolf Hitler beim ersten 
Spatenstich zeigt. In einzelne Fotos aufgel&ouml;st, verzerrt sich dieses
Gesicht zur grotesken Maske. Zugleich gewinnen Personen im Hintergrund 
des Bildes, die im laufenden Film nur als Staffage auszumachen waren, 
pl&ouml;tzlich Konturen, werden als Beteiligte sichtbar.</p>


<p>Was verbirgt ein Film, wenn er das zeigt, was die Kamera sieht? In 
seinem Essayfilm "Das Kino und der Tod" (1988) beschr&auml;nkt sich Bitomsky
&uuml;ber 45 Minuten auf dieses Verfahren. Zu Beginn fragt man sich irritiert, 
warum er nicht gleich die betreffenden Filmausschnitte pr&auml;sentiert. 
Am Ende hat man manch altbekannten Film nur in ein paar Fotos v&ouml;llig neu 
gesehen. Aber auch ein Vefahren, das nicht immer gleicherma&szlig;en 
effizient ist. Wenn er in "Der VW-Komplex" Fotos durchbl&auml;ttert, auf denen 
ein Fotograf Fahrzeugbauteile zu bizarren Kunstobjekten stilisiert 
hat, will sich erhellende Differenz nicht einschieben.</p>


<h4>DIE STIMME UND DIE DIFFERENZ</h4>

<p>Im Gegensatz zu Harun Farocki, der bei seinen letzten Filmen auf 
jeglichen Kommentar verzichtet hat, spielt die Sprache (seine Sprache) 
in den Filmen von Hartmut Bitomsky ein Rolle, wie sie ihr sonst wohl nur 
noch in den Filmen Alexander Kluges zukommt. Dabei ist die Art, wie er 
mit seiner sonoren Nasalstimme in einer an Monotonie nicht zu 
&uuml;berbietenden Melodik einen schlichten Hauptsatz an den n&auml;chsten
reiht, anfangs schon fast eine Beleidigung f&uuml;r jedes halbwegs geschulte 
Geh&ouml;r. Es braucht seine Zeit, bis man dahinterkommt, dass es eigentlich 
auch gar nicht um das geht, was er sagt. So funktioniert dieser Kommentar 
vielfach am besten da, wo er gar keiner ist, wo die Stimme nicht das 
Sichtbare erkl&auml;rt oder interpretiert, sondern es scheinbar nur verdoppelt.</p>


<p>Wo man bei jedem anderen Regisseur den "m&uuml;ndiger Zuschauer" einklagen 
w&uuml;rde, gelingt es Bitomsky vielfach, eine eigent&uuml;mliche Differenz zwischen 
dem Visuellen und dem Akustischen zu erzeugen, die zu einem wirksamen 
Korrektiv der immer drohenden "Objektivit&auml;t" der Bilder wird. Zugleich aber 
auch immer ein sensibler Balanceakt, der unvermittelt in manieriertes Pathos 
umkippen kann. Wo Bitomsky denselben Sprachduktus einsetzt, um die Bilder 
zu interpretieren, also von der Differenz zur Synthese wechselt, ger&auml;t das 
Ganze auch schon einmal zur Platit&uuml;de. In "Das Kino und der Tod" stellt er 
die Frage, warum der Tod bzw. das Sterben im Film eine derart gro&szlig;e Rolle 
spielt. Die Antwort: <i>"Die Gesellschaft handelt so, damit das Leben 
in ihr als lebenswert erscheint."</i> Nicht, dass die grunds&auml;tzliche 
Richtigkeit dieser These zu bestreiten w&auml;re; es die Art,
wie der Satz gesprochen wird, die hier als filmisches Mittel nicht nicht 
funktioniert, da er die Bilder nicht akzentuiert, sondern von ihnen ablenkt.
</p>
    
<a name="sec-vondokuzuspiel"></a>
<h3>Als die Wirklichkeit laufen lernte...<br>
<small>Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm</small>
</h3>
    

<p>
<i>"Dokumentarfilme haben mich immer besonders angezogen. Sie stellen 
eine Form des Filmemachens dar, die einem erlaubt, die Dinge so zu filmen, 
wie sie passieren - ohne einzugreifen. Die Kamera liegt auf der Lauer, wie 
ein J&auml;ger, und wartet auf Bilder, die die Wirklichkeit produziert. Dann kam 
eine Zeit, in der ich das Interesse an dieser Art der Dokumentation, am 
cin&eacute;ma v&eacute;rit&eacute;, verlor, weil ich ihre Beschr&auml;nkungen erkannte. Wenn man
darauf wartet, dass etwas Wichtiges passiert, geschieht entweder gar nichts 
oder nur etwas v&ouml;llig Belangloses. Man kann zwanzig Tage lang hinter seiner 
versteckten Kamera lauern, wie ich es f&uuml;r meinen kubanischen Film 
GENTE EN LA PLAYA getan habe, und am Ende hat man lediglich die Oberfl&auml;che 
der Dinge aufgenommen.... Deshalb habe ich mich dem Spielfilm zugewandt, 
um eine Geschichte zu erz&auml;hlen und um mit Schauspielern zu arbeiten. Mit 
anderen Worten, ich habe angefangen, die Art von Filmen zu drehen, die ich 
als junger Mann verachtet habe."</i> <small>(Nestor Almendros in "A Man with a Camera")</small>
</p>


<p>Der letztes Jahr verstorbene Kameramann Nestor Almendros steht 
beispielhaft f&uuml;r eine Reihe von Filmemachern, die ihre Karriere mit 
Dokumentarfilmen begonnen haben, bevor sie mit Spielfilmen einem 
breiten Publikum bekannt wurden. Die im Umgang mit dem Dokumentarfilm 
gewonnenen F&auml;higkeiten kommen ihrer Arbeit mit fiktiven Stoffen hierbei
meist zugute. So zeichnet fast alle Filme von Almendros der sparsame 
Umgang mit kameratechnischen Mitteln aus. Wo immer es ging, versuchte er 
mit der f&uuml;r den Dokumentarfilm obligatorischen "available light photography" 
auszukommen, d.h. die Dinge nach M&ouml;glichkeit ohne Hilfe zus&auml;tzlicher 
Ausleuchtung abzubilden.</p>


<p>Seine wichtigsten Dokumentarfilme drehte Almendros mit dem iranischen 
Filmemacher Barbet Schroeder; GENERAL IDI AMIN DADA (1974) und 
KOKO, THE TALXING GORILLA (1977). Mit Roberto Rossellini (ROMA, 
CITTA APERTA, 1945) drehte er im Auftrag der franz&ouml;sischen Regierung 
eine Dokumentation &uuml;ber eines der am kontroversesten diskutierten
Geb&auml;ude in Paris: LE CENTRE GEORGES POMPIDOU (1977).</p>


<p>Almendros' Kollege, der amerikanische Kameramann Haskell Wexler, 
war auf dem Gebiet des Dokumentarfilms noch wesentlich intensiver 
t&auml;tig (auch als Regisseur), bevor er sich dem Spielfilm zuwandte. Zu seinen 
bekanntesten Dokumentationen geh&ouml;ren der halbdokumentarische 
MEDIUM COOL (1969), THE BUS (1965) und NEWSREEL (1969). Mit den Filmen 
BRAZIL: A REPORT ON TORTURE (1971) und INTERVIEWS WITH MY LAI VETERANS (1971) 
verl&auml;sst er die Position des Beobachters und bezieht gewollt Stellung.
Zu Haskell Wexlers wichtigsten Spielfilmarbeiten geh&ouml;ren 
IN THE HEAT OF THE NIGHT (1967, R.: Norman Jewison), 
THE CONVERSATION (1973, R.: Francis Ford Coppola) und
ONE FLEW OVER THE CUCKOO'S NEST (1975, R.: Milos Forman). F&uuml;r seine
Kameraarbeit zu WHO'S AFRAID OF VIRGINIA WOOLF (1966, R.: Mike Nichols) wurde
er sogar mit dem Oscar ausgezeichnet</p>

    
<a name="sec-kingkong"></a>
<h3>King Kongs Kinderstube<br>
<small>Dokumentarfilm als Abenteuer</small>
</h3>


<p>Kameram&auml;nner scheinen schon immer wichtige Vermittler zwischen Spiel- und
Dokumentarfilm gewesen zu sein.</p>

<p>Ernest B. Schoedsack (1893-1979) verfolgte und filmte als Berichterstatter den ersten
Weltkrieg direkt an der Front. Er geh&ouml;rte zu den damals nicht seltenen Menschen, die ihre
Abenteuerlust in extremen Situationen auslebten. Das Festland Europa bot in jener Zeit mit
seinen vielen Kriegen dazu reichlich Gelegenheit. So begegnete er 1925 in Polen einem
anderen Abenteurer: Merian C.Cooper, der dort in der Luftwaffe k&auml;mpfte. In die
Filmgeschichte sollten die beiden 1933 mit ihrem Fantasy-Klassiker KING KONG eingehen,
bei dem Schoedsack Regie f&uuml;hrte_</p>


<p>Zuvor hatten Cooper und Schoedsack bereits mit zwei spektakul&auml;ren Dokumentarfilmen
Aufsehen erregt: GRASS - A NATIONS STRUGGLE FOR LIFE (1925) und CHANG (1927). 
Geholfen hat ihnen indirekt ihr Regiekollege Robert Flaherty. Nachdem dessen Film
NANOOK OF THE NORTH (1922) beim amerikanischen Publikum erfolgreich gewesen ist,
waren die Studios bereit, Geld in Dokumentarfilme vor exotischem Hintergrund zu investieren.
GRASS schildert die gefahrenvolle und verlustreiche Suche der Bakthiaris, einem
Nomadenvolk im Iran, nach neuem Weideland f&uuml;r ihre Herden. Bereits in ihrem ersten
Dokumentarfilm lassen Cooper und Schoedsack ihre Neigung zum gro&szlig;en Abenteuer
erkennen: das Nomadenvolk auf seiner Wanderung wird effektvoll vor den beeindruckenden
Landschaftspanoramen in Szene gesetzt. Allerdings wird auch der Tod nicht ausgespart. Wenn
der Nomadenstamm einen rei&szlig;enden Strom oder einen Gletscher &uuml;berquert, wirkt dies zwar
wie eine spannende Szene aus einem Tarzanfilm; die Menschen, die bei den halsbrecherischen
Unternehmungen ums Leben kommen, sind jedoch echte Todesopfer und nicht nur
austauschbare Statisten.</p>


<p>CHANG erz&auml;hlt die Geschichte vom Kampf einer Familie im siamesischen Dschungel. Noch
deutlicher als in GRASS wird hier Coopers Hang zum Inszenieren und Konstruieren. Wenn es
die Handlung unterst&uuml;tzt oder zur Spannung beitr&auml;gt, werden einige Szenen (z.B. die
Elefantenstampede) sogar nachgestellt.</p>


<p>Mit ihren beiden Dokumentarfilm-Klassikern haben Cooper und Schoedsack nicht nur den
optischen Standard f&uuml;r s&auml;mtliche Tarzan- und Dschungelabenteuerfilme gesetzt, sondern auch
ihre nachfolgenden Spielfilme MOST DANGEROUS GAME (1932) und KING KONG (1933) vorweggenommen. 
Die Themen und Schaupl&auml;tze bleiben die gleichen (Dschungel, &Uuml;berlebenskampf in feindlicher Umgebung), 
nur die Sichtweise ist eine andere; einmal dokumentarisch, einmal fiktiv.</p>


<p>Ein anderer Regisseur und Kameraspezialist, der Gefallen an exotischen Schaupl&auml;tzen
fand und den Dokumentarfilm als Abenteuer verstand, war der bereits erw&auml;hnte Robert
Flaherty. Mit NANOOK OF THE NORTH (1922) und MOANA (1926) hat er die
Dokumentation in eine neue Richtung gelenkt und sie f&uuml;r ein gr&ouml;&szlig;eres Publikum &uuml;berhaupt erst
interessant gemacht. Flaherty wurde sp&auml;ter von seinem Studio (Paramount) h&auml;ufig eingesetzt,
wenn es galt, andere Regisseure bei Szenen im Dschungel oder in der Wildnis k&uuml;nstlerisch zu
beraten. So hat er z.B. f&uuml;r William S. Van Dyke die Au&szlig;enaufnahmen zu WHITE SHADOWS
OF THE SOUTH SEA (1928) gedreht, einem Film im Stil der FOUR FEATHERS von Schoedsack.</p>


<p>Entt&auml;uscht von der romantisierenden Art dieses Films, die den dokumentarischen Aspekt
zur&uuml;ckdr&auml;ngte, nahm er das Angebot eines realistischen Regisseurs an, mit diesem einen Film
&uuml;ber die S&uuml;dsee zu drehen. Durch diese Zusammenarbeit entstand einer der Klassiker der
Filmgeschichte: TABU von Friedrich Wilhelm Murnau.</p>


<p>Schon in seinem vorangegangenen Film OUR DAILY BREAD (1929) hatte Murnau eine
fiktive Geschichte mit einer Dokumentation &uuml;ber die Entstehung des Brotes
zusammengeschnitten.</p>


<p>Auch in TABU benutzte Murnau diese Technik, die dokumentarischen Abschnitte und die
Geschichte (die Murnau zusammen mit Flaherty ausgearbeitet hatte) sind aber enger verwoben.
Dies f&uuml;hrte zu Meinungsverschiedenheiten zwischen Murnau und Flaherty. Flaherty war als
geborener Dokumentarfilmer ausschlie&szlig;lich am sozialen Aspekt der Geschichte interessiert,
d.h. an den Lebensbedingungen der Eingeborenen, die als Perlentaucher von chinesischen
H&auml;ndlern ausgebeutet werden. Murnau hingegen war zwar Realist, doch das bedeutete f&uuml;r ihn
nicht, dass er die Wirklichkeit naturalistisch abzubilden hatte. Schon bei Schoedsack zeigte sich,
dass ein Regisseur im Zweifelsfall bereit ist, f&uuml;r einen Effekt die Realit&auml;t zu opfern. Murnau
geht in TABU einen wesentlichen Schritt weiter. Er zeigt die "gl&uuml;cklichen Inseln" der S&uuml;dsee,
und damit eine Realit&auml;t, die schon nicht mehr existierte, als er zu filmen begann. Zwar erscheint
in seinem Film auch die Zivilisation (die chinesischen H&auml;ndler), doch nur als Bestandteil der
Geschichte des Films. Die bemerkenswerten Szenen, die man von diesem Film beh&auml;lt, sind
diejenigen, in denen die verlorene Wirklichkeit der S&uuml;dsee beschworen wird: die badenden
M&auml;dchen, die ausfahrenden Fischer, die Landschaft...</p>


<p>Diese Idealisierung ist f&uuml;r den Dokumentarfilm eigentlich untypisch (wenn man von
Kurzfilmen, Industriedokumentationen und &Auml;hnlichem absieht).</p>


<p>Der Dokumentarfilm steht in enger Verbindung zum (literarischen) Realismus bzw.
Naturalismus. Dieser entstand in der Ablehnung des romantischen Idealisierens, zugunsten
einer ungesch&ouml;nten Sicht der Welt, und dies war meist die Welt der Armen. Man denke an
Georg Hauptmanns DIE WEBER und sein PHANTOM (das von Murnau 1922 verfilmt
wurde). Dies zeigt sich auch in den Brecht-Verfilmungen der zwanziger Jahre, die heute als
Zeitdokumente bewertet werden. Dieser moralische Aspekt des Realismus/Naturalismus ging
in den Surrealismus ein und veranlasste Bu&ntilde;uel, einen Dokumentarfilm zu drehen.</p>


<p>Bu&ntilde;uel hatte sich in der &Ouml;ffentlichkeit mit seinen beiden Skandalfilmen UN CHIEN
ANDALOU (1928) und L'AGE D'OR (1930) als surrealistischer Filmemacher par excellence
etabliert. In beiden Filmen &uuml;berwiegt der poetische Aspekt des Surrealismus. Mit LAS
HURDES (1932) wendet sich Bu&ntilde;uel von dieser Art des Surrealismus ab. Es geht ihm nicht
mehr nur um Poesie, er will auch etwas mitteilen.</p>


<p>Mit seinem Kameramann Eli Lotar macht sich Bu&ntilde;uel auf in die spanische Berglandschaft der
Hurdes. Die Menschen, die dort weitab von der Zivilisation einen t&auml;glichen Kampf gegen die
Natur f&uuml;hren, sind kranke Menschen. Sie wissen, dass sie den Kampf verlieren werden. Ihre
Erl&ouml;sung ist erst der Tod. Bu&ntilde;uel zeigt dieses Elend in klaren, uninszenierten Bildern. Das
macht einen Gro&szlig;teil ihrer grausamen, erschreckenden Wirkung aus. Indem er das poetische
Moment beinahe ganz zur&uuml;cknimmt, erreicht er eine umso st&auml;rkere Wirkung. Die wenigen
Stilmittel, die Bu&ntilde;uel verwendet, arbeiten mit Widerspr&uuml;chen: etwa der n&uuml;chterne, fast
wissenschaftliche Kommentar, mit dem die Bilder unterlegt sind, oder Brahms Vierte
Symphonie als Begleitmusik.</p>


<p>In den sp&auml;teren Filmen von Bu&ntilde;uel kamen die beiden Gegens&auml;tze: Poesie und Realismus
(Moral) zu einem ausgewogeneren Verh&auml;ltnis. Meistens stand die Poesie im Vordergrund. Nur
noch in LOS OLVIDADOS ist die poetische Seite &auml;hnlich stark zur&uuml;ckgenommen und das
moralische Anliegen so deutlich.</p>
    
<a name="sec-krieg"></a>
<h3>Fliegende Pfarrer und Hollywood im Krieg</h3>


<p>Zugegeben lassen sich in vielen F&auml;llen einfache Gr&uuml;nde vermuten, die einen Regisseur
veranlassen k&ouml;nnten, eher einen Dokumentarfilm als einen Spielfilm zu drehen.</p>


<p>Auff&auml;llig ist zum Beispiel, dass Dokumentarfilme meist zu den fr&uuml;hen Filmen der Regisseure
geh&ouml;ren. Hier liegen rein &auml;u&szlig;erliche Gr&uuml;nde nahe; es ist billiger und weniger aufwendig, einen
kleinen, guten Dokumentarfilm zu machen, als einen kleinen, guten Spielfilm, denn es sind die
fiktiven Elemente, die Geld verschlingen: Drehbuch, Kost&uuml;me, Bauten, Schauspieler,
Tricktechnik etc. (Dies ist wohl auch einer der Gr&uuml;nde, warum so viele Independent-Filme
dokumentarischen Charakter haben; man denke nur an die fr&uuml;hen Filme von Jim Jarmusch.
Durch den Verzicht auf einen kommerziellen Produzenten verringert sich der Etat, was direkte
Auswirkungen auf den Gebrauch teurer fiktiver Elemente hat.)</p>


<p>Andererseits bietet der Dokumentarfilm eine ideale M&ouml;glichkeit, die Technik des
Filmemachens zu erproben, ohne sich mit dem zus&auml;tzlichen Ballast, den eine
Spielfilmhandlung bedeutet, zu beschweren. Stanley Kubricks ersten beiden Filme DAY OF
THE FIGHT (1951 &uuml;ber einen Boxkampf) und FLYING PADRE (1952, &uuml;ber einen Priester,
der im Flugzeug zu den Mitgliedern seiner Gemeinde fliegt) fallen in diese Kategorie. Kubrick
hat sich bei diesen Filmen um alles selbst gek&uuml;mmert: Szenario, Regie, Kamera, Ton und
Schnitt.</p>


<p>Wenn man von der R&uuml;stungsindustrie absieht, hat wohl das Kino am meisten vom Krieg
profitiert, und sei es nur in Form von Kriegspropagandafilmen. Neben Regisseuren, die daf&uuml;r
ihr Genre nicht verlassen haben (wie Chaplin oder Hitchcock), gibt es einige bekannte
Spielfilmregisseure, die pl&ouml;tzlich mit Kriegsdokumentationen &uuml;berraschten. So zum Beispiel
(ideologisch weniger &uuml;berraschend) der Westernregisseur John Ford, oder (schon etwas
erstaunlicher) der monumentale John Huston. Kurios auch der produktive Frank Capra mit
Titeln wie PRELUDE TO WAR (1943) und THE NAZIS STRIKE (1943). Bekannt geworden
ist er f&uuml;r Filme wie ARSEN UND SPITZENH&Auml;UBCHEN und (gesellschaftskritische)
Kom&ouml;dien.</p>


<p>Die Liste lie&szlig;e sich beliebig fortsetzen, aber man merkt bereits: diese Filme passen selten zum
restlichen Werk der Regisseure, die auch meist nicht sehr stolz auf sie waren (Stanley Kubrick
nannte seinen FLYING PADRE schlicht "silly"). Sicherlich wird der Dokumentarfilm von
Spielfilmregisseuren eher lieblos behandelt, es gibt ihn beinahe nur als Auftragsarbeit oder
(finanzielle) Verlegenheitsl&ouml;sung. Ein Regisseur, der nicht nur etwas, sondern auch sich
darstellen m&ouml;chte, wird als Medium wohl kaum den Dokumentarfilm w&auml;hlen.</p>
    
<a name="sec-littlebird"></a>
<h3>Fly Little Bird<br>
<small>oder das Aufbegehren der n&auml;chsten Generation</small>
</h3>


<p> R&uuml;ckblickend scheinen f&uuml;r die Regisseure des New Hollywood vor allem zwei
Einfl&uuml;sse stilpr&auml;gend gewesen zu sein: die Roger Corman Factory (in der fast alle
Erfolgsregisseure der siebziger und achtziger Jahre als Drehbuchautoren, Cutter, Ausstatter
oder Kameraassistenten angefangen haben) und der Dokumentarfilm in Verbindung mit
popul&auml;rer Musik. Als bekanntestes Beispiel kann sicherlich Martin Scorsese (TAXI DRIVER,
1975; GOOD FELLAS, 1990) angesehen werden. Kaum ein anderer moderner Filmemacher 
kann so gut mit dem Genre des Dokumentarfilms umgehen und hat diesem so viel f&uuml;r seine
Spielfilme abgewonnen. Scorseses erste Begegnung mit dem Medium war Michael Wadleighs
legend&auml;rer Konzertfilm WOODSTOCK (1969). Wadleigh, der sp&auml;ter selber vom Dokumentar- 
zum Spielfilm gewechselt ist (WOLFEN, 1981), wollte dem Mammutkonzert eine Ikone
schaffen, ein Dokument, das sowohl die Ereignisse auf der B&uuml;hne als auch die Stimmung unter
den G&auml;sten einf&auml;ngt. Deshalb lie&szlig; er zwei Kamerateams gleichzeitig drehen. Heraus kam eine
unglaubliche Menge an Rohmaterial. Die eigentliche Leistung des Films bestand nun darin, das
Material zu ordnen und in einem Rhythmus zu montieren, der die Atmosph&auml;re auf dem Festival
ann&auml;hernd wiedergeben sollte. Der Verdienst f&uuml;r diese Leistung liegt bei Martin Scorsese und
Thelma Schoonmakers, die beide als Cutter angestellt waren. WOODSTOCK erwies sich als
ausgesprochen originell montiert und nimmt einiges an Schnittechniken vorweg, was f&uuml;r viele
sp&auml;tere Scorsese-Filme stilpr&auml;gend wurde; so ist das Bild in WOODSTOCK &uuml;ber weite
Strecken in zwei oder mehr Abschnitte unterteilt, um das Geschehen von mehreren
Standpunkten aus zu zeigen oder unabh&auml;ngige Handlungsstr&auml;nge nebeneinander zu stellen. In
seinem fr&uuml;hen Gangsterfilm MEAN STREETS (1974) greift Scorsese auf diese Technik zur&uuml;ck. 
In anderen Filmen, z.B. GOOD FELLAS (1990) h&auml;lt er das Bild an der
entscheidenden Stelle f&uuml;r einen kurzen Moment an.</p>


<p>Einen eigenen Musik-Dokumentarfilm konnte Scorsese 1976 drehen; THE LAST WALTZ, der
das Abschiedskonzert von Bob Dylans Rockgruppe The Band aufzeichnet. Scorsese wollte das
Konzert mit filmischen Mitteln einfangen, die in ihrer Ehrlichkeit und Geradlinigkeit der Musik
von The Band entsprechen. Schnitt und Mischung gestalteten sich &auml;u&szlig;erst schwierig, so dass
der Film erst nach 18 Monaten in die Kinos kam. Trotz aller Schwierigkeiten und
Verz&ouml;gerungen wurde THE LAST WALTZ als bester Musikfilm seit WOODSTOCK gefeiert.
Der Film wurde auf 35mm-Material gedreht, was f&uuml;r einen Dokumentarfilm damals noch recht
ungew&ouml;hnlich war, ihm jedoch eine starke optische Eindringlichkeit verleiht. Mit Hilfe eines
300 Seiten dicken Drehplans, der alle Texte und Griffwechsel auflistet, choreographiert
Scorsese seine acht Kameram&auml;nner (darunter Laszlo Kovacs, Vilmos Zsigmond und Michael
Chapman). Zum ersten Mal nahm man 24 Tonspuren auf, die dann f&uuml;r den Soundtrack auf vier
Dolby-Stereo-Spuren zusammengespielt wurden.</p>


<p>Im dokumentarischen Bereich hat sich Scorsese eine zweite Karriere aufgebaut. Nach seinem
eigenen STREET SCENES (1970), der Stimmungsbilder vom Stra&szlig;enleben in New York
zeichnet, war er als Cutter und Koproduzent an einigen Dokumentationen beteiligt. Interessant
ist au&szlig;erdem noch Scorseses Portrait seiner Eltern in ITALIANAMERICAN (1974), die
gelegentlich Kurzauftritte in seinen Spielfilmen haben. Wenn Mutter Scorsese Robert de Niro
in GOOD FELLAS begr&uuml;&szlig;t, dann begr&uuml;&szlig;en sich tats&auml;chlich zwei alte Freunde. Detailgetreue
Milieuschilderungen vom Leben der italienischen Einwanderer in Little Italy ziehen sich wie ein
roter Faden durch die Filme von Martin Scorsese.</p>


<p>Ein anderes Beispiel ist der amerikanische Filme- und Mythenmacher Francis Ford
Coppola (THE GODFATHER, 1971; APOCALYPSE NOW, 1979). Coppola hatte Ende der
siebziger Jahre den Traum, eine Gemeinschaft unabh&auml;ngiger Filmemacher jenseits von
Hollywood zu gr&uuml;nden. AMERICAN ZOETROPE nannte er seine kurzlebige
Produktionsgesellschaft, die zun&auml;chst Werbe- und Dokumentarfilme herstellte. George Lucas
(AMERICAN GRAFFITI, 1974; STAR WARS, 1977) geh&ouml;rte zu Coppolas fr&uuml;hesten
Weggef&auml;hrten und Sch&uuml;lern. Seine erste Arbeit f&uuml;r Coppola war FILMMAKER (1968), eine
Dokumentation &uuml;ber die Dreharbeiten zu dessen Road-Movie THE RAIN PEOPLE (1968).
Zuvor hatte Lucas bereits den Western MACKENNA'S GOLD (1967) dokumentiert. Auch
heute ist es noch &uuml;blich, dass junge Filmemacher als Starthilfe mit der Dokumentation von
Filmen bereits erfolgreicher Kollegen beauftragt werden; so durfte die Jungfilmerin Katja von
Garnier (ABGESCHMINKT, 1992) die Dokumentation zur Entstehung von Wolfgang Petersens
Thriller IN THE LINE OF FIRE (1993) drehen.</p>


<p>Auch zahlreiche andere Regisseure arbeiten gerne mit den Mitteln des
Dokumentarfilms. So z.B. der Brite John Schlesinger (MIDNIGHT COWBOY, 1968; THE
FALCON AND THE SNOWMAN, 1985), der selber als Dokumentarfilmer angefangen hat
(TERMINUS, 1960) und in seinen Filmen aus den siebziger Jahren gerne eine n&uuml;chterne,
unterk&uuml;hlte Filmsprache verwendet, die sich mit ihrem distanzierten Blick auf das Geschehen
deutlich an den klassischen Dokumentarfilm anlehnt. Seinen Thriller MARATHON MAN
(1976) er&ouml;ffnet er mit einer Parallelmontage: man sieht den im Central Park von New York
trainierenden Dustin Hoffman beim Laufen. Dazu werden kurze Aufnahmen eines farbigen
Langstreckenl&auml;ufers aus Ken Ichikawas Olympia-Dokumentation TOKYO OLYMPIAD (1964)
montiert. Schlesinger f&uuml;hrt uns also gleich zu Beginn zwei L&auml;ufer vor: einen wirklichen
Sportler und einen Schauspieler. Der Sportler schaut beim Laufen immer wieder direkt in die
ihn begleitende Kamera, deren Pr&auml;senz dem Zuschauer eher unbewusst bleibt, weil sie nur in
einem Ausschnitt (close-up) verharrt. Trotz der Bewegung des L&auml;ufers wirkt die Einstellung
sehr ruhig. Die Kamera im MARATHON MAN verh&auml;lt sich wie ein Begleiter. Sie folgt Dustin
Hoffman und macht seine Bewegungen mit. Hierbei liefert sie Bilder, die hektisch und trotz
steadycam leicht verwackelt wirken. Sie entsprechen also dem, was man sich normalerweise
unter dokumentarischen Aufnahmen vorstellt. Die fiktive Szene wirkt dokumentarischer als die
Szene aus dem eigentlichen Dokumentarfilm.</p>


<p>Diese Kombination gibt den Rhythmus des Films vor und schafft eine eigenartig verhaltene
Spannung, die vor allem in den Suspense- und Action-Szenen zum Tragen kommt.</p>

    
<a name="sec-letzterblick"></a>
<h3>Ein letzter Blick in die Runde<br>
<small>(was wir vorher vergessen haben...)</small>
</h3>


<p>Dieser Aufsatz sollte gezeigt haben, dass die Gemeinsamkeiten zwischen Dokumentar-
und Spielfilm erheblicher sind, als man auf den ersten Blick annehmen k&ouml;nnte.
Als Beweis k&ouml;nnen die Filmemacher gelten, denen eine Idee oder ein Thema wichtiger ist als
die Form. Schoedsack und der Dschungel, Bu&ntilde;uel und die Moral.</p>


<p>Weitere Beispiele dr&auml;ngen sich auf. Walt Disneys zuckerwattes&uuml;&szlig;er Stil blieb nicht auf seine
Zeichentrickfilme beschr&auml;nkt. Die Tiere in seinen Lehrfilmen (am bekanntesten wohl THE
LIVING DESERT (1953); THE VANISHING PRAIRIE, Oscar f&uuml;r den besten
Dokumentarfilm 1954) werden ebenso wie seine zahllosen Trickfiguren durch Musik und
Montage mit menschlichen Charaktereigenschaften belegt. Wahlweise denke man auch an
seinen MEIN FREUND, DAS ATOM.</p>


<p>Ernsthaftere Absichten verfolgte W.S.van Dyke, der bereits weiter oben erw&auml;hnt wurde.
Bekannt wurde er durch zahlreiche Spielfilme wie TARZAN THE APE MAN (1932, mit
Johnny Weissm&uuml;ller), THE THIN MAN (1934), SAN FRANCISCO (1936, featuring Clark
Gable, Spencer Tracy und unglaubliche Spezialeffekte) und IT'S A WONDERFUL WORLD
(1939). Weniger bekannt ist, dass er auch als Kameramann und Regisseur zu den wichtigsten
Dokumentarfilmern geh&ouml;rt. Sein Anliegen ist dabei ein soziales und kritisches. In THE CITY
(1939) besch&auml;ftigt er sich mit den Problemen der Gro&szlig;st&auml;dte. VLLEY TOWN (194O)
beschreibt die Situation von Arbeitern in einer amerikanischen Kleinstadt w&auml;hrend der
Depression. Auch in seinen Spielfilmen sind diese Elemente, wenn auch meist nur
unterschwellig, vertreten (vgl. THE THIN MAN).</p>


<p>Der Designer und Filmarchitekt Ralph McQuarrie arbeitete zun&auml;chst f&uuml;r die Boeing-Company
und die NASA, wo er Lehr- und Werbefilme &uuml;ber bemannte Raumfl&uuml;ge und
Planetenerkundung drehte. Mitte der siebziger Jahre wurde der junge Filmemacher George
Lucas auf ihn aufmerksam und verpflichtete ihn f&uuml;r sein Science-Fiction Projekt STAR WARS
(1977). McQuarrie wurde der erste feste Mitarbeiter von Lucasfilm und k&uuml;nstlerischer Leiter
der Visual-Effects Abteilung Industrial Light and Magic. Einige der innovativsten Fahrzeuge,
Raumschiffe und Roboter in der Geschichte des phantastischen Films stammen von ihm. Aus
seiner Zeit bei der NASA brachte er die Erfahrung mit, dass High-Tech, sobald sie einmal im
Gebrauch ist, nicht lange hochgl&auml;nzend und neu aussehen kann. Deshalb sehen seine
Raumschiffe in STAR WARS leicht verstaubt und abgenutzt aus. Damit setzte er den optischen
Standard f&uuml;r die meisten nachfolgenden Weltraumabenteuer.</p>


<p>Diese &Uuml;bersicht kann nat&uuml;rlich nicht vollst&auml;ndig sein, viele wichtige Namen mussten
ausgelassen werden: 
Ren&eacute; Clair (THE EIFFEL TOWER, 1927; SOUS LES TAITS DE PARIS, 1930), 
Jean Epstein (FINIS TERRAE, 194l; LA CHUTE DE LA MAISON USHER, 1928), 
Robert Siodmak (MENSCHEN AM SONNTAG, 1929; THE SPIRAL STAIRCASE, 1945), 
Leni Riefenstahl (OLYMPIA, 1938; DAS BLAUE LICHT, 1932),
Georges Franju (LE METRO, 1934; LE GRAND MELIES, 1952; LES SANG DES BETES, 1949; LES YEUX SANS VISAGE, 1959), 
Jean Renoir (LA GRANDE ILLUSION, 1937; A SALUTE TO FRANCE, 1944), 
Josef von Sternberg (THE TOWN,1944; DER BLAUE ENGEL, 1930), 
Carol Reed (THE TRUE GLORY, 1945; THE THIRD MAN, 1949),
Jonathan Demme (STOP MAKING SENSE, 1985; THE SILENCE OF THE LAMBS, 1991), 
Michael Apted (BRING ON THE NIGHT, 1985; GORKI PARK, 1982), 
Phil Janou (U2-RATTLE AND HUM, 1988; 3 O'CLOCK HIGH, 1989), 
Martha Coolidge_(DAVID OFF AND ON, 1973; AN OLD-FASHIONED WOMAN, 1974; RAMBLING ROSE, 1990), 
Alain Resnais (NUIT ET BROUILLARD, 1955; LE CHANT DU STYR&Egrave;NE, 1958; LOIN DU VIET-NAM, 1967; HIROSHIMA - MON AMOUR, 1959), 
Louis Malle (CALCUTTA, 1969; AND THE PURSUIT OF HAPPINESS, 1986; GOD'S COUNTRY, 1985; ATLANTIC CITY, USA, 1980), 
Jean-Luc Godard (LOIN DU VIET-NAM, 1967; A BOUT DE SOUFFLE, 1960), 
Bertrand Tavemier (LA GUERRE SANS NOM, 1991; ROUND MIDNIGHT, 1986), 
Michelangelo Antonioni (NETTEZZA URBANA, 1948; BLOW-UP, 1966), 
Michael Moore (ROGER &amp; ME, 1988; Arbeiten f&uuml;r Steven Spielbergs TV-Serie AMAZING STORIES), 
Jim McBride (DAVID HOLZMAN'S DIARY, 1968; THE BIG EASY, 1986), 
Wim Wenders (TOKYO-GA, 1985; AUFZEICHNUNGEN ZU KLEIDERN UND ST&Auml;DTEN, 1988; CHAMBRE 666 - N'IMPORTE QUAND..., 1983; PARIS, TEXAS, 1985), 
John Korty (LANGUAGE OF FACES, 1961; AN INTERVIEW WITH BRUCE GORDON, 1964; OLIVER'S STORY, 1978), 
Pierre Schoendorffer (ANDERSON PLATOON, 1967; A FACE OF WAR, 1968; DIEN BIEN PUH, 1991),
Harold Becker (BLIND GARY DAVIS, 1963; SEA OF LOVE, 1989), 
Brian de Palma (RESPONSIBLE EYE, 1966; DIONYSUS IN 69, 1969; CARRIE, 1976, SCARFACE, 1983), 
William Friedkin (12 Dokumentationen f&uuml;r das NBC-Bildungsprogramm; FRENCH CONNECTION, 1971; THE EXORCIST, 1973), 
et cetera, et cetera...
</p>
    
<a name="sec-robertkramer"></a>
<h3>Portrait: Robert Kramer in M&uuml;nchen</h3>


<p>Das M&uuml;nchner Dokumentarfilmfestival will
kein Festival der Erstentdeckungen sein. Es
bietet in seinem internationalen Programm
eine interessante Auswahl von Produktionen
des Vorjahrs, gr&ouml;&szlig;tenteils auf anderen Festivals gesichtet. 
Dazu kommt eine Regionalschau (erstaunlich gut besucht) mit neuesten
Produktionen hiesiger "DokumentaristInnen". Ereignis beim diesj&auml;hrigen Festival
waren die vier Filme von Robert Kramer, die
als Hommage ins Programm aufgenommen
waren: MILESTONES (1972-75), SCENES FROM
THE PORTUGUESE CLASS STRUGGLE (1977),
DOC'S KINGDOM (1987), ROUTE ONE/USA (1989). 
Im Berliner "Arsenal" und im Kommunalen Kino Frankfurt hatte es 
kurz vorher vollst&auml;ndige Retrospektiven des Werks von
Robert Kramer gegeben.</p>


<p>Dokumentarfilme sind ein ungeliebtes Genre.
Diese Filme scheinen uns gerade das vorenthalten zu wollen, 
was "der Film", "das Kino" uns versprechen. Truffaut mochte Dokumentarisches nicht. 
Er war der Ansicht, dass man solche Filme nur in kostenlosen Vorf&uuml;hrungen dem Publikum anbieten d&uuml;rfte: 
<i>"... das ist kein Witz, zum Beispiel waren die 
Reaktionen der Zuschauer von CHRONIQUE D'UN &Eacute;T&Eacute; einander diametral entgegengesetzt, je nachdem, ob die 
Zuschauer Eintritt zahlen mussten oder nicht"</i> <small>(aus: Lettre contre le cin&eacute;ma-v&eacute;rit&eacute;)</small>). Die besten Dokumentarfilme
sind aber gerade so beschaffen, dass sie die Genregrenzen transzendieren. Finden und
Erfinden geh&ouml;ren zusammen, bringen Kino-Wahrheit nur dann hervor, wenn sie miteinander 
ins Spiel gebracht werden.</p>


<p>In MILESTONES spielt Robert Kramer mit der Stilisierung &agrave; la cin&eacute;ma-v&eacute;rit&eacute; derart gekonnt,
dass man das durchaus Inszenierte des Films erst allm&auml;hlich, anfangs ungl&auml;ubig, wahrnimmt. 
Der Kritiker der S&uuml;ddeutschen Zeitung, der 1977 &uuml;ber den Film schrieb, hat von
dem Konstruierten, dem Erfundenen des Films nichts bemerkt. Er nannte ihn einen
Film, <i>"in dem nur Zeugnisse und Dokumente aus dem Alltag amerikanischer Minderheiten
gesammelt und kommentarlos einander gegegen&uuml;bergestellt sind (...). Die Gespr&auml;chspartner 
kommen den Filmemachern weit genug entgegen: mit einer Offenheit, die bei uns
exhibitionistisch wirken w&uuml;rde (...)"</i>. Nun
h&auml;tte der Kritiker schon anhand der credits
stutzig werden m&uuml;sen, wo von Rollen und
ihren Darstellern die Rede ist. Dass Kramers
Figuren fiktionalisierte Versionen der Leute
sind, die sie spielen, hat z.B. Vincent Canby
(New  York  Times,  2.11.75)  ausf&uuml;hrlich
beschrieben:  <i>"Grace Paley, Autorin von
Kurzgeschichten, spielt eine Filmemacherin,
die eng mit der Anti-Kriegs-Arbeit verbunden
ist (...). David C. Stone, Produzent des
Films, spielt mit kantigem Witz einen Kneipenbesitzer, 
dessen radikales Engagement mit
der Zeit eingeschlafen ist (...). John Douglas,
der Co-Regisseur des Films, hat eine Hauptrolle als 
blinder T&ouml;pfer, dessen Werkstatt vor&uuml;bergehend 
zum Refugium f&uuml;r alte Freunde
wird, die in ver&auml;nderten Zeiten nach einem
neuen Lebensstil suchen (...)"</i>.</p>


<p>Der Film hat Traumsequenzen, z.B. die Alptraumbilder 
des Vietnamheimkehrers, er hat
offenkundig spielfilmhaft inszenierte Passagen: 
den Einbruch, die Vergewaltigung. Nur
am Schluss gibt es pures cin&eacute;ma-v&eacute;rit&eacute;: wenn
die Geburt eines Kindes in einer kommuneartigen 
Gemeinschaft gezeigt wird. Tats&auml;chlich
ist dieser peinigend lange Schlussteil eine der
wenige Schwachstellen des Films. Man sieht
einerseits ein schlecht gespieltes Geb&auml;rtheater, 
andererseits l&auml;sst der reale Geburtsvorgang
keinen Raum f&uuml;r das Spiel, und so l&auml;uft
streckenweise eine Art medizinischer Lehrfilm ab.</p>


<p>MILESTONES ist ein gro&szlig;er und bewegender
Film von dreieinviertel Stunden L&auml;nge, in
dem Dutzende von Figuren (zumeist Wei&szlig;e
der gehobenen Mittelschicht), die nur teilweise 
direkt miteinander zu tun haben, auf
der Suche nach einem stimmigen Lebensstil
gezeigt werden. Nicht um "Minderheiten"
geht es in diesem Film, sondern um <i>die</i> kleine
radikale Minderheit, die in den 60er Jahren
als Aktivisten zum "Movement" (B&uuml;rgerrechtsbewegung, 
Protest gegen den Vietnamkrieg) geh&ouml;rten und die sich nun in 
den Siebzigern in einzelnen, privateren Suchbewegungen zerfasern.</p>


<p>
<i>"Es schien, als w&auml;ren wir das Zentrum der
Dinge gewesen"</i>, sagt ein junger Mann, der
sich an Demonstrationen in Washington, an
Black Panthers etc. erinnert. Nun geht er mit
einem Freund von damals durch eine Vorstadt&ouml;dnis 
und erz&auml;hlt vom Scheitern oppositioneller 
Gewerkschaftsgruppen in der Automobilfabrik, in der er arbeitet. Ein anderer
Protagonist &uuml;berlegt sich, als Eremit in den
H&ouml;hlen der Hopi-Indianer zu leben. Er sa&szlig;
im Gef&auml;ngnis, weil er Deserteuren zur Flucht
ins Ausland verhalf. Er steht stellvertretend f&uuml;r
eine Gruppe, die es nun nicht mehr gibt.
Andere haben sich aufs Land zur&uuml;ckgezogen,
erproben "nat&uuml;rliche" Lebensformen, frei
von den Zw&auml;ngen einer todess&uuml;chtigen Zivilisation, 
zugleich erleben sie "nat&uuml;rliche Bindungen" als Fesseln. 
Andere machen Sozialarbeit mit Strafgefangenen. Wieder andere
machen  B&uuml;cher  oder  Filme  &uuml;ber  ihr
Engagement.</p>


<p>Der Engagierte, dem es nicht gelingt, sein
Engagement zu professionalisieren (seinen
Lebensunterhalt damit zu verdienen), wird
Fundamentalist. Dabei sind Professionalismus 
und Fundamentalismus Schrumpfformen
des Engagements. Das Engagement aber ist
selbst schon eine Schrumpfform der Existenz, insofern dabei 
Leidenschaften auf Interessen reduziert werden. So hat Peter 
Sloterdijk die Verlaufsform von "Bewegungen"
geschrieben. Spannend an den Suchbewegungen der Figuren in MILESTONES ist nun, dass
sie meist als Scheitern, Flucht, Katastrophe
erlebt werden, zugleich aber - von au&szlig;en
gesehen - der Versuch sind, gerade vom
Reduktionismus des Engagements freizukommen, ohne dessen Impulse zu verraten. Wie
kann ein stimmiges Leben gegen die t&ouml;dliche
Zivilisation gelingen? Wie kann man Selbstfindung, 
Sinnfindung, Geldfindung harmonisieren?</p>


<p>Ein Hippie-P&auml;rchen verabschiedet sich von
der Landkommune. Mit zwei Kindern und
einem klapprigen Auto geht man auf Achse.
Eine Panne. Die Kinder stopfen Schnee in den
Benzintank. Die Frau muss sich &uuml;ber den
Motor beugen, in der Rastst&auml;tte gibt's nur
Junkfood, in der Garage fragt sie nach einem
Job. Der Mann will nicht mit ihr &uuml;ber ihr
Gef&uuml;hlswirrwarr reden. Sie f&uuml;hlt sich einer
Maschinerie ausgeliefert, der sie entkommen
wollte. Die Grundstimmung des Films ist
zuerst Scheitern, Flucht, Katastrophe. Die
Geburt am Schluss wird oft als Hoffnungssymbol 
interpretiert. Das ist nicht falsch, aber
bezeichnend ist, dass der Film sich hier auf
jenen Aspekt der Geburt konzentriert, der
Schmerz, H&ouml;llenfahrt durch den Geburtskanal 
hei&szlig;t. Dass der Film die Zeitstimmung
genauestens trifft, steht au&szlig;er Frage.</p>


<p>In einem Interview der Cahiers du Cin&eacute;ma
(Dezember 78) hat man Robert Kramer darauf 
hingewiesen, dass Straub/Huillet MILESTONES 
gar nicht m&ouml;gen und dem Film u.a.
vorwerfen, dass es der Film eines verw&ouml;hnten
amerikanischen Kindes sei. Kramer antwortet
ausweichend: da sei was dran, aber dieser
Aspekt werde im Film selbst bedacht und 
<i>"...ich verstehe die Filme von Straub nicht recht.
Ich habe MOSES UND ARON in Portugal gesehen, 
es war sehr eigenartig, ihn unter diesen
Umst&auml;nden zu sehen"</i>.</p>


<p>SCENES FROM THE PORTUGUESE CLASS STRUGGLE hei&szlig;t der Film, den Kramer 1977
(zusammen mit Philip Spinelli) in Portugal
gedreht hat. MILESSTONES war ein offenes
Geflecht  von  tagebuchartig  registrierten
Lebenswegen Einzelner, eine F&uuml;lle pr&auml;zis
geformter Gespr&auml;chssituationen, eine Reflexion 
&uuml;ber den allt&auml;glichen Lebensstil, dieser
Film nun erscheint als das krasse Gegenteil;
ein High-Speed-Agitprop-Streifen, in dem die
Ereignisse der portugiesischen Revolution
durchgeh&auml;ckselt werden. Die Bilder flitzen
vor&uuml;ber wie die Briefe in der Verteilermaschinerie 
auf dem Hauptpostamt, und der fast
unabl&auml;ssige Kommentar verteilt die Bilder (in
denen niemand &uuml;ber knappe Statements hinaus zu Wort kommt) in die K&auml;stchen und
stempelt sie ab: hier spricht eine Speerspitze
des Imperialismus, hier ein Vertreter des Volkes. 
Der Kommentar sortiert nach Kategorien
einer Klassenanalyse auf der Linie der Partei
Otelo de Cavalhos. Das Volk und seine Spontaneit&auml;t 
werden zum strahlenden Mythos, die
Revolution wird zum Volksfest, das B&ouml;sewichter st&ouml;ren wollen, und die 
Kommentarstimmen definieren, was die wahre Stimme
des Volkes ist. Manche Analysen m&ouml;gen richtig 
sein, manche Hintergrundinformationen
neu und aufschlussreich, insgesamt aber donnern 
die Ereignisse vor&uuml;ber wie ein Sturzbach. Die Filmemacher m&ouml;gen sich 
vorgekommen sein wie Fische im Wasser, aber sie
kanalisieren nur Str&ouml;me von Bildern, die von
sich her nichts sagen d&uuml;rfen. Merkw&uuml;rdigerweise 
vermittelt auch dieser Film eine panische, katastrophische Grundstimmung. 
Vom Tempo und der Begrifflichkeit des Kommentars 
abgesehen, hat der Film die konventionelle Machart eines TV-Beitrags.</p>


<p>Zehn Jahre sp&auml;ter drehte Kramer noch einen
Film in Portugal: DOC'S KINGDOM. Ein amerikanischer 
Arzt (gespielt von dem Journalisten Paul McIsaac, der im n&auml;chsten Film der
Reisebegleiter auf der <i>Route One</i> sein wird)
dreht sich in Selbstgespr&auml;chen um sich selbst.
Nicht nur die Massen sind verschwunden,
auch in der n&auml;chsten Umgebung des Doc ist
es leer. Im Hospital, wo er seinen Dienst tut,
mit R&ouml;ntgenbildern hantiert, gibt es nur mit
einem Patienten eher unverbindliche Wortwechsel. 
Auch in der Kneipe nur ein knappes
Miteinanderreden mit dem Wirt. Docs K&ouml;nigreich hei&szlig;t Einsamkeit, Isolation. Nur zwei
Ereignisse st&ouml;ren ihn darin - vor&uuml;bergehend - auf. Das Fenster wird ihm zertr&uuml;mmert
und es werden Parolen auf den Boden vor seinem Haus gemalt. Die Bedrohung bleibt
undefiniert, erh&auml;lt kein Gesicht. Vielleicht sind es ehemalige Kombattanten, die den
Gringo forthaben wollen. Zuerst wappnet er sich gegen die Unbekannten, sp&auml;ter wirft er
seine Pistole ins Meer. Dann aber erh&auml;lt er Besuch aus Amerika: der erwachsene Sohn
(eines der herangewachsenen Kinder aus MILESTONES?) will wissen, wie sein Vater
aussieht und was er treibt. Es kommt zu einem gro&szlig;en, 
sch&ouml;n in der Schwebe gehaltenen Gespr&auml;ch zwischen beiden, das kl&auml;rt,
warum ihre Leben getrennt verlaufen sind
und weiter verlaufen werden.</p>


<p>Kramer zeichnet das Bild dieser Einsamkeit
in einer ruhigen, eleganten Inszenierung. Ein
Spielfilm, in den ein Dokumentarfilm &uuml;ber
Einsamkeitsgesten integriert ist. Dramaturgisch ist das Gravitationszentrum 
das Vater-Sohn-Gespr&auml;ch, eine Art Showdown. Doc ist
ein Schiffbr&uuml;chiger, ein Gestrandeter. Unterwegs nach dem Nirgendwoland Utopia, ist er
im Niemandsland am Rande Lissabons gelandet. Was sich im Film untergr&uuml;ndig, 
andeutungsweise sp&uuml;rbar macht, ist im Zusammenhang der anderen Filme Kramers deutlich.
Der legend&auml;re Gr&uuml;nder Lissabons ist Odysseus. F&uuml;r Doc aber gibt es keine Heimkehr,
nur eine R&uuml;ckkehr in die USA im n&auml;chsten
Film, um den Stand der Dinge dort zu durchleuchten. Einsamkeit, Resignation, Verzweiflung..., 
aber in diesem Film passiert etwas
Merkw&uuml;rdiges: die katastrophische Grundstimmung ist verschwunden. Vielleicht liegt
das an der spezifisch portugiesischen Form
der Melancholie, <i>Saudade</i> genannt, die nicht
nur Trauer und Verzweiflung ist, sondern
auch - Reinhold Schneider hat das so gesagt - Sch&ouml;nheit, sogar Erl&ouml;sung, 
<i>"... saudade, deren Wesen ihre Unbegrenztheit und Undefiniertheit, 
die in Melodie sich l&ouml;sende uranf&auml;ngliche irdische Unerf&uuml;llbarkeit der Kreatur ist"</i>.</p>


<p>Eine Steigerung dieser Sch&ouml;nheit und Erl&ouml;stheit, mit der sich Kramer anscheinend in 
Portugal infiziert hat, findet sich in ROUTE ONE/USA, dem Film, der an L&auml;nge (vier
Stunden), Bedeutung und Kraft das Gegenst&uuml;ck, Weiterf&uuml;hrung der Fragen und Echo zu
MILESTONES ist. (Siehe auch epd Film 6190, S. 15).  Am deutlichsten wird das Nichtkatastrophische 
dieses Films in manchen Gespr&auml;chen, wenn sich Augenblicke einverst&auml;ndigen Stillschweigens ergeben, die sind
wie: Ein Engel geht durchs Zimmer. So im Gespr&auml;ch mit der alten Indianerin, die z&ouml;gert
und schlie&szlig;lich nicht mehr weiter aus ihrer Geschichte erz&auml;hlen will.  Doc bel&auml;sst es
dabei. So auch ein Augenblick stillen Einverst&auml;ndnisses im Weinkeller des portugiesischen 
Kaufmanns. Aber auch in den Begegnungen mit dem "gegnerischen" Amerika,
mit den fanatischen Predigern, Rassisten, militanten Patrioten etc. gibt es eine Ruhe des
Blicks, ein Standhalten des Anblicks, in dem
die Personen geschont sind und gerade so in
ihrer Merkw&uuml;rdigkeit, Verbohrtheit, ihrem
bisweilen gef&auml;hrlichen Wahn scharf gesehen
werden. Kramer ist nicht mehr der nerv&ouml;se
Chronist, eher in der Art eines Bluess&auml;ngers
berichtet er von seinen Begegnungen. Zu
Beginn der Reise zitiert Doc einige Verse
Walt Whitmans aus dem "Gesang von der
freien Stra&szlig;e". Der Klang dieses Gedichts
scheint Rhythmus und Melodie des ganzen
Films zu pr&auml;gen. <i>"Nur der Kern jedes Dings
n&auml;hrt"</i>,  hei&szlig;t  es  bei  Whitman.  Kramer
beschreibt seinen Blick auf die Dinge so:
<i>"Mit der gr&ouml;&szlig;tm&ouml;glichen Konzentration die
Anziehungskr&auml;fte sichtbar machen, die von
jedem betrachteten Ding ausgehen. Das hei&szlig;t
respektvollen Umgang mit dem Vorhandenen
zu finden. Mein Blick sch&uuml;tzt die Personen:
ich werde mich ihrer nicht bedienen, um ein
Argument zu belegen. Sie sind da, weil sie da
sind."</i>
</p>


<p>Diesen Blick gab es auch schon in fr&uuml;heren
Filmen Kramers, aber nicht in dieser vertrauensvollen Intensit&auml;t. In allen Filmen Kramers
gibt es als Hauptimpuls die Suche nach dem intensiven Augenblick. Intensit&auml;t ist aber eine
sehr ambivalente Kategorie. Sie kann einer &Auml;sthetik des Schocks, Alptraums, Erschreckens 
zugeh&ouml;ren, aber auch einer des Geltenlassens. In einem fr&uuml;hen Interview spricht
Kramer einmal davon, er wolle inmitten einer Bombe sein, wenn sie explodiert. Es gibt die
Intensit&auml;t einer Explosion oder die einer Begegnung, in der eine Gestalt sich offenbart.</p>
    
<a name="sec-harunfarocki"></a>
<h3>Eine Anstrengung - &uuml;berfl&uuml;ssig?<br>
<small>Portr&auml;t des Filmemachers und Publizisten Harun Farocki</small>
</h3>


<p>&Uuml;ber den in Berlin lebenden Filmemacher Harun Farocki und seine Arbeit zu schreiben, f&auml;llt
leicht und schwer zugleich. Ihre gedankliche Exposition und ihre Materialstruktur laden zum genauen Hinschauen ein
und belohnen mit einer F&uuml;lle von Entdeckungen. Im ersten Moment des Schreibens dr&auml;ngen sich vor die Filme aber die 
Situationen, in denen ich die Filme von Harun Farocki sah, sie im Kino selbst zeigte und mit dem Regisseur &ouml;ffentlich dar&uuml;ber sprach.
Das erste Mal, als ich Farocki bei einer Diskussion erlebte - 1978 auf der Duisburger Filmwoche -, verweigerte er sich den 
Zuschauern, die eine interpretative Nachbesserung seines Films "Zwischen zwei Kriegen" (1978) forderten. Farocki provozierte eine
gereizte Stimmung. Einige Monate sp&auml;ter, im Essener Kino "Zelluloid", lie&szlig; sich Farocki mit den wenigen Zuschauern &uuml;beraus 
geduldig auf jede Nachfrage zum Stoff und zu seiner politischen Interpretation ein. Ich will beide Verhaltensweisen weder 
gegeneinander ausspielen noch zu Konstituenten seines Charakters erheben, vielmehr als einen Erfahrungsgegensatz festhalten: 
wie die Vielzahl seiner Filme sind die Diskussionen mit Farocki reich an Unterschieden. Dass ich nach der K&ouml;lner Vorf&uuml;hrung des
Films "Bilder der Welt und Inschrift des Krieges" (1988) auf dem Nachhauseweg von der Diskussion von jemandem, der - um kein
Missverst&auml;ndnis aufkommen zu lassen - weder den Film gesehen hatte noch der Diskussion gefolgt war, einen Schlag ins Gesicht 
bekam, geh&ouml;rt zu diesen Erfahrungen ebenso hinzu wie das Erlebnis eines Wochenend-Seminars im K&ouml;lner Filmhaus, zu dem ich 
Farocki als einen der Referenten eingeladen hatte. Farocki wollte ausdr&uuml;cklich erst am Sonntagmorgen kommen, weil er am Samstag
seit Jahr und Tag Fu&szlig;ball spiele, und das nicht f&uuml;r das K&ouml;lner Seminar aufzugeben gedachte.
Am sehr k&uuml;hlen Sonntagmorgen beschlichen mich Zweifel, ob Farocki p&uuml;nktlich eintreffen
werde. In der Nacht hatte es die Umstellung zur Sommerzeit gegeben, so dass er noch eine Stunde fr&uuml;her aufstehen und ein Flugzeug
nehmen musste. Doch p&uuml;nktlich wie ein Buchhalter stand Farocki um zehn Uhr Sommerzeit mit seinen Filmb&uuml;chsen vor 
dem Versammlungslokal und war bester Laune.</p>


<h4>BREITES SPEKTRUM</h4>


<p>M&uuml;sste man die Filme, die Harun Farocki (geb. 1944 in Neutitschein, damals: Sudetengau) in den letzten Jahren im Auftrag des
Fernsehens (vor allem f&uuml;r den WDR und das ZDF) oder mit Geldern der kulturellen Filmf&ouml;rderungen in Hamburg und 
Nordrhein-Westfalen gedreht hat, klassifizieren, h&auml;tte man sie in einem groben Zugriff auf f&uuml;nf
Gruppen zu verteilen. Die erste versammelt dokumentarische Arbeiten, die einen einzelnen 
Arbeitsvorgang - etwa die Herstellung eines Fotos f&uuml;r den "Playboy" beispielsweise in
"Ein Bild" (1983) oder die Entwicklung einer Werbekampagne in "Image und Umsatz oder Wie kann man einen Schuh herstellen" (l989)
- festhalten und in einem schl&uuml;ssigen Filmablauf verdichten. Eine zweite Gruppe lie&szlig;e sich aus Filmen zusammenstellen. in denen
K&uuml;nstler und ihre Arbeitsweise portr&auml;tiert werden: 
"Zur Ansicht: Peter Weiss" oder "Georg K. Glaser  -  Schriftsteller  und Schmied" (1989). Die dritte enthielte 
medienkritische Studien ("Der &Auml;rger mit den Bildern", 1973) und Filmessays, die sich in historischen 
Exkursen des Zusammenhangs der Wahrnehmung mit der industriellen Produktion annehmen: 
"Bilder der Welt und Inschrift des Krieges" (1988) und "Wie man sieht" (1986). Eine vierte Gruppe enthielt
jene Filme, in denen Farocki sich inszenatorischer Formen bediente: "Zwischen zwei Kriegen" und "Etwas wird sichtbar" (1983)
sowie "Betrogen" (1985). Hier ist die klassifikatorische Willk&uuml;r am gr&ouml;&szlig;ten. In die f&uuml;nfte Gruppe k&auml;men schlie&szlig;lich die 
kleinen Beitr&auml;ge f&uuml;r die "Sesamstra&szlig;e", "Sandm&auml;nnchen" oder f&uuml;r die Filmredaktion des WDR. Gerecht wird man Arbeiten Harun Farockis
aber nur, wenn man sie einzeln und zusammen und im historischen Kontext studiert, wie das Motiv des Vietnamkrieges in den 
fr&uuml;hen Agitationsfilmen (etwa "Nicht l&ouml;schbares Feuer", 1969) in seinem Spielfilm "Etwas wird sichtbar" genauestens untersucht, also
auseinandergenommen und wieder neu zusammengesetzt wird;  wie  die  &auml;sthetische Kargheit der Essayfilme sich gegen die von
ihm kritisierte Bebilderungs- und Verlautbarungstechnik des Fernsehfeatures entwickelt hat; wie er den historischen Spuren 
der Arbeiterbewegung folgte, ohne diese zu glorifizieren oder zur Rechtfertigung der eigenen Praxis zu idealisieren; wie er sich neugierig
auf die  Verh&auml;ltnisse hierzulande  einlie&szlig;, ohne sich von ihnen &uuml;berw&auml;ltigen zu lassen, und deshalb imstande war, sie gleichsam zu
skelettisieren; wie er mit den Schriftstellern Peter Weiss, Heiner M&uuml;ller und Georg K. Glaser spricht, um etwas von ihrer 
materialistischen Arbeitsweise zu erfahren und so auch - bescheiden, aber sp&uuml;rbar - etwas von sich selbst mitzuteilen; 
wie sich die 16mm-Filme (etwa "Zwischen den Kriegen") von den auf 35mm gedrehten ("Etwas wird sichtbar") unterscheiden 
und die ersteren wiederum von einer Videoarbeit ("Image und Umsatz"), schlie&szlig;lich wie Musik und Sprache eingesetzt
werden und wie Farocki montiert.</p>


<p>Sein j&uuml;ngster Film "Leben - BRD" (1990) wurde im Rahmen des "Internationalen Forums des jungen Films" der "Berlinale" 
uraufgef&uuml;hrt und von der Redaktion "Kleines Fernsehspiel" kurz darauf Programm des ZDF gezeigt. Derzeit ist er in den 
Programmkinos zu sehen. Farocki besuchte und filmte 32 Lern-, &Uuml;bungs-, Therapie- und Teststunden in der ganzen 
Bundesrepublik Deutschland und montierte sie zu einer unkommentierten Folge von Kurzszenen - aneinandergeschnitten 
nach Stichworten, Oppositionen, Assoziationen, Bewegungen und Gesten. "Leben - BRD" setzt aus der F&uuml;lle der 
Details das Bild einer Gesellschaft zusammen, in der das Geb&auml;ren  und das Sterben, das Schreien und die Menschenpflege, 
das Stra&szlig;e &uuml;berqueren und das T&ouml;ten in staatlichen oder privaten Institutionen gelehrt und gelernt wird, ja, 
gelernt werden muss. Das wahre mechanische Ballett tanzen nicht die Maschinen, sondern die Menschen, die sich
nach einer Musik bewegen, die sich aus schw&uuml;lstigen Phrasen der Sozialp&auml;dagogik ("Magst Du, dass ich Dir helfe?"), 
B&uuml;rokratie ("Die Schulung zielt darauf ab, durch eine Kommunikationsaufnahme  eine  konkrete Zielansprache zu erreichen") 
und Therapeutik ("Sp&uuml;r in Dich hinein!") speist.</p>


<h4>DER MENSCH: DISZIPLINIERT</h4>


<p>Die gesammelten Szenen scheinen die Annahme zu st&uuml;tzen, hierzulande herrsche eine
Versicherungs- und Vorsorgementalit&auml;t vor, in der Gl&uuml;ck wie Elend durch Sozialtechniken
diszipliniert und von ihrem Grad der Unberechenbarkeit befreit werden sollen. Doch 
"Leben - BRD" ersch&ouml;pft sich nicht in einer solchen Interpretation. Andere lassen sich 
ebenfalls denken - etwa die, dass die Arbeitsgesellschaft, der die Arbeit ausgeht, zu 
neuen spielerischen Formen der Arbeit dr&auml;ngt; auch solche Deutung wird vom Film unterst&uuml;tzt.
Wichtiger noch: die Teilnehmer an den Spielen, Tests und Therapiesitzungen werden
nicht zu Belegst&uuml;cken irgendwelcher Thesen degradiert. Sie behalten in Abstufungen 
etwas von ihrer W&uuml;rde.</p>


<p>Das hat seine Ursache in der Arbeitsweise Farockis: Farocki hat die Szenen so geschnitten,
dass selbst noch der unsinnigste Vorgang sich gleichsam selbst erkl&auml;rt. Und so pr&auml;gt sich
manches Gesicht - etwa das des Kindes mit den spillerigen Haaren zu Beginn, des 
Tippelbruders, der lieber Brath&auml;hnchen kaufen statt m&uuml;hsam das Kochen erlernen m&ouml;chte, oder
einer &auml;lteren Frau, die in der Gruppentherapie zu spielen hat, sie sei ihr Herd - ebenso ein
wie der Satz, den eine junge Mutter sagt: <i>"Ich sch&auml;me mich, dass ich von diesem Mann ein
Kind habe"</i>.</p>


<p>"Leben - BRD" hat nach der Fernsehausstrahlung in der Presse ein gro&szlig;es Echo 
gefunden. Es scheint, als sei es Farocki endlich gelungen, auch Fernsehkritiker dazu zu 
zwingen, in Ruhe zuzuschauen, statt sich von der sicheren Hand eines Kommentars f&uuml;hren zu
lassen. Wie in fr&uuml;heren F&auml;llen die Ablehnung seiner Arbeiten aussah, war 1988 in der Jury
des Adolf-Grimme-Preises mitzuerleben, als sich eine Mehrheit gegen eine Auszeichnung
seines Films "Die Schulung" (1987) bildete. Die einen waren gegen ihn, da man das, was
er an Selbstdisziplinierung und Subjektentkernung zeige, ohnehin schon alles wisse. Die
anderen lehnten ihn ab, weil der Film gar nichts erkl&auml;re und die Zuschauer ohne 
notwendige Meinung &uuml;ber das Gesehene zur&uuml;cklasse. Ahnlich "heiter" mag es unter den 
Beisitzern in der Filmbewertungsstelle Wiesbaden (FBW) zugegangen sein, als sie mit dem
Fu&szlig;ballergebnis von "3:2" dem Film "Leben - BRD" ein  Pr&auml;dikat  absprachen.
Ihre "Kennzeichnung" des Films ist es ebenso wert, zitiert zu werden 
(<i>"... Dokumentarfilm/Thesenfilm/Rollenspielele/angepasstes  Verhalten/langatmig/filmisch  unergiebig"</i>...) 
wie die gutachterlichen Verst&ouml;&szlig;e gegen Grammatik und Logik: 
<i>"Es wird obendrein zur &uuml;berfl&uuml;ssigen Anstrengung: der Durcheinander- und
Zusammenschnitt von verschiedenen Handlungsabl&auml;ufen, als Gestaltungsprinzip 
ausgegeben, verlangt bei der gedanklichen Zur&uuml;ckordnung der Themen zus&auml;tzliche 
Konzentration."</i> Wenn die Beisitzer von "sinnlichen Elementen" sprechen, 
dann klingt das nicht, als spr&auml;chen sie &uuml;ber einen Film, den man in
der Tat sehen muss, um ihn zu begreifen.</p>


<p>Harun Farocki geh&ouml;rte zu den relegierten Studenten des ersten Jahrgangs an der 
"Deutschen Film- und Fernsehakademie Berlin" (dffb). Aber, so schrieb er 1984 in 
einer Brosch&uuml;re der Akademie, das wisse heute kaum jemand mehr: 
<i>"Auch heutige dffb-Studenten nicht, gro&szlig;e Verwunderung, wenn ich 
erz&auml;hle, dass Daniel Schmid da war und erst recht bei diesem Pl&uuml;schtierdirektor 
Wolfgang Petersen (wohl der Einzige aus dem Jahrgang, der noch lange Haare hat)."</i>
In seinen Aufs&auml;tzen hat Farocki &uuml;ber das Zustandekommen seiner Filme mehrfach und beinahe
regelm&auml;&szlig;ig Rechenschaft abgelegt: <i>"Mit Biografie, so es eine gibt, hat 
das nichts zu tun. Es geht um die Schilderung von Arbeitsverh&auml;ltnissen."</i> 
Doch die Zeitschrift "Filmkritik", in der er &uuml;ber zehn Jahre publizierte und 
redaktionell betreute, erscheint seit 1984 nicht mehr. Farocki soll, 
so hei&szlig;t es, noch auf einigen kompletten Jahrg&auml;ngen sitzen, als w&auml;re
er ein Spekulant, der auf die Wertsteigerung seiner Ware wartet.</p>


<p>Um zu beschreiben, was man in den alten Heften der "Filmkritik" alles finden kann,
eine letzte Geschichte um Farocki: Unter den &uuml;ber 200 eingereichten Produktionen der 
Duisburger Filmwoche 1989 war auch ein kurzer Film aus der Deutschen Film- und 
Fernsehakademie Berlin, der in der Flut der gewichtigen Themenfilme zu den 
Konflikten und Problemen der Welt unterging. Nur das Musikst&uuml;ck, das er 
verwandte, blieb einigen Mitgliedern der Auswahlkommission tagelang im
Ohr. Als Komponistin wurde im Abspann die S&auml;ngerin Nico genannt. Die Versuche, das
namenlose St&uuml;ck in den Folgemonaten zu identifizieren und auf einer der zahlreichen
Platten der Ex-K&ouml;lnerin zu entdecken, scheiterten. Von Nico waren im letzten Jahrzehnt
zu viele Zusammenstellungen und Live-Aufnahmen auf Platte ver&ouml;ffentlicht worden,
w&auml;hrend die alten Originalplatten aus dem Handel genommen wurden.</p>


<p>Per Zufall stie&szlig; ich Monate sp&auml;ter in einer Ramschkiste f&uuml;r 
Billig-CDs auf den Titel. Er wurde auf der Platte "Desertshore" 
ver&ouml;ffentlicht, die Nico 1970 gemeinsam mit ihrem Produzenten 
John Cale aufnahm, und hei&szlig;t "Le petit chevalier". Das Cover zieren Fotos
aus dem Film "La Cicatrice Interieure", den Philippe Garrel im selben 
Jahr mit Nico in der Hauptrolle drehte. &Uuml;ber den Film konnte ich
nur wenige Informationen finden (etwa in Ulrich Gregors  "Geschichte des 
Films ab 1960"). Erst jetzt entdeckte ich in einem Heft der "Filmkritik" 
so etwas wie eine Beschreibung: <i>"Der Film ist kein Handlungsfilm...
erst sitzt Nico auf einem Stein und klagt etwas in dem Tonfall, in dem 
das Gretchen im 'Faust' "Meine Mutter die Hur..." intoniert,
sie gibt einen Laut und horcht in sich hinein. ... um die Biegung des 
Weges kommt ein Sch&auml;fer mit seinen Schafen. Nun reicht der
Sch&auml;fer Nico die Gei&szlig;, die sie entgegennimmt wie der Zugabfertiger 
den Postpack, dann f&auml;hrt die Kamera ein St&uuml;ck mit dem Sch&auml;fer
parallel, entl&auml;sst ihn aus der Mitfahrt und sieht ihm und der 
Herde nach, bis alle hinter der Wegbiegung verschwunden sind, wobei die
Abblende mit dem allerletzten unschuldigen Tierchen einsetzt."</i>
</p>


<p>Autor dieses 1982 geschriebenen Textes ist Harun Farocki. 
Und sein Artikel, der mit dem Satz "Ein Film wie ein Plattencover" 
beginnt, ziert Vor- und R&uuml;ckseite der Langspielplatte "Desertshore" 
von Nico. Er endet mit dem Satz: <i>"Weil dieser Film zehn Jahre
alt ist und solche Filme heute nicht mehr gemacht werden, es keine 
Rockpoesie mehr gibt, die sich als neue Weltsprache entwirft,
kommt mir dieser Film wie aus einem vergangenen heroischen 
Zeitalter vor."</i>
</p>


<p>Der Sinn f&uuml;r Pr&auml;zision, der sich in der Beschreibung der Filmszene 
ausdr&uuml;ckt, sowie das Pathos eines unerf&uuml;llten Wunsches der
Gegenwart, der sich im Schlusssatz andeutet, geh&ouml;ren zum Bild des 
Filmemachers und Publizisten Harun Farocki. Es bleibt zu hoffen,
dass seine Filme nicht in den Ramschkisten der Medienbranche 
verschwinden und dass sie ihr Publikum im Lauf der Jahre finden 
werden.</p>
    
<a name="sec-romualdkarmakar"></a>
<h3>Blicke, die nicht richten<br>
<small>Die Filme von Romuald Karmakar</small>
</h3>


<p>COUP DE BOULE beginnt mit einer Nacht&uuml;bung der franz&ouml;sischen Armee.
Schemen robben durchs Dunkel, halblaut gebr&uuml;llte Befehle betonen die martialische
Atmosph&auml;re. Der erste Augenblick verwandelt eine Wahrnehmung in ein Klischee: 
Nun spielen sie wieder Krieg, das ist die Assoziation, die sich beim Betrachten 
der Szene unwillk&uuml;rlich einstellt. Aber wer sind diese Schatten, denen 
Romuald Karmakar 1987 einen 25-min&uuml;tigen Dokumentarfilm gewidmet hat?
Die Antwort verwandelt ein Klischee in eine Wahrnehmung;
Sie sind Einzelne, die beim Milit&auml;r eine seltsame Form der 
Zusammengeh&ouml;rigkeit exerzieren.</p>


<p>Romuald Karmakar, Jahrgang 1965, ist als Sohn einer franz&ouml;sischen 
Mutter und eines indischen Vaters 1987 zum franz&ouml;sischen Wehrdienst 
eingezogen worden. Der Bataillonsphotograph, der eigentlich den 
Ereignissen des Kasernenhoflebens ein "besonderes Gewicht geben" soll, 
holt stattdessen seine Kameraden vor die Super-Acht-Kamera, die er 
am Ende eines Urlaubs eingeschmuggelt hat. Er filmt ein Ph&auml;nomen, das in
Frankreich verbreiteter ist als in Deutschland: "Coup de boule", 
das Austeilen von Kopfn&uuml;ssen. Aber die jungen Soldaten,
die er vor die Kamera und ins Rampenlicht bittet, als g&auml;lte es,
sich auf einer Probeb&uuml;hne als Schauspielnachwuchs zu bew&auml;hren,
treten nicht gegeneinander an. Sobald sie ihren Namen, 
Alter und Dienstgrad genannt haben, rennen sie mit dem Kopf
gegen einen Spind: <i>"Zum Spa&szlig;, f&uuml;r alle Kameraden"</i>. Die 
Vorstellung individueller Lebensl&auml;ufe scheint bei diesem 
kollektiven Wettlauf um Anerkennung auf der Strecke zu bleiben. 
Doch die Jungen, die ihre Beulen wie Auszeichnungen tragen, l&auml;cheln,
wenn sie gegen den Spind krachen. Von der eingedellten T&uuml;r
l&ouml;sen sich Zettel, die mit Rei&szlig;n&auml;geln angepinnt waren. Der 
Realit&auml;t des Aufpralls bleibt es &uuml;berlassen, die Lust an der 
Deformation, die befremdliche Wucht dieser Selbstdarstellung zu
kommentieren. Auch das nervenaufreibende Quietschen, das den
Film &uuml;berkommt, wenn die Jungs ihre eisernen Bettgestelle 
stemmen, ist so ein Kommentar. Von den Kasernierten erf&auml;hrt man
nichts &uuml;ber den Zweck ihrer au&szlig;erdienstlichen &Uuml;bungen. <i>"Fuck
you"</i>, sagen sie, wenn sie stemmend und rammend auf ihre 
Art ihren Kopf durchsetzen, aber man wei&szlig; nicht so recht, 
wen sie meinen: den Spind, das Bett, das Leben, die Kamera, 
sich selber, den Vorgesetzten. Am Ende verschwinden die 
Armeeangeh&ouml;rigen, denen Karmakar f&uuml;r 25 Minuten ein Gesicht, ein 
An-Sehen gegeben hat, wieder in der Anonymit&auml;t des 
Truppenalltags. Im Dunkeln, das Klischees gebiert. Karmakars 
Beobachtungen, die weder von Erkentnissen noch von Bekenntnissen 
auf den einen Begriff gebracht werden, der ein gebr&auml;uchliches 
Urteil &uuml;ber Personen und Handlungsweisen
erm&ouml;glicht, haben linken wie rechten Betrachtern gleichmerma&szlig;en Kopfschmerzen 
bereitet. 14 Tage Bau, drei Monate auf Bew&auml;hrung, hat die franz&ouml;sische Armee 
ihrem unautorisierten Dokumentaristen als Disziplinarstrafe zugedacht. <i>"Alle Leute,
die in dem Film mitgemacht haben"</i>, so der in M&uuml;nchen lebende Filmemacher in 
einem Interview mit Rolf Aurich (in <i>filmw&auml;rts</i> No 17), <i>"mussten 
unterschreiben, dass ich ihnen kein Geld gegeben habe"</i>. Die Beteiligten wurden 
des Drogenmissbrauchs beschuldigt. Offiziere, die den Film nie sahen, erkannten in 
Karmakar einen <i>"irgendwie linken, subversiven Typen"</i>. Dieses
"irgendwie", hilfloser Ausdruck des Unfassbaren, ist an Karmakar haftengeblieben. 
Auf der Berlinale 1988, die COUP DE BOULE pr&auml;sentierte, wurde der unorthodoxe, 
autodidaktische Filmemacher von einem eher links orientierten Publikum "irgendwie" 
der Verkl&auml;rung faschistoider Rituale und Zurichtungsmechanismen verd&auml;chtigt.</p>


<p>Die Absurdit&auml;t dieses Vorwurfs h&auml;tte bereits Karmakars erster Film entkr&auml;ften 
k&ouml;nnen. EINE FREUNDSCHAFT IN DEUTSCHLAND, 1985 mit minimaler Finanzierung als Spiel 
unter Freunden entstanden, versucht sich mit anarchistischer Verve an einem 
fiktiven Dokument von Hitlers M&uuml;nchener Studienjahren. Karmakar selbst hat in 
EINE FREUNDSCHAFT IN DEUTSCHLAND die Rolle eines humorlosen und verklemmten
Junghitlers &uuml;bernommen, dem erst ein Faschingsball Aufschluss &uuml;ber die 
Wirkung seiner sp&auml;teren Uniform verschafft. Der Hitlergru&szlig; verdankt sich 
in dieser malizi&ouml;sen Auslegung, die sich auf Recherche und Phantasie 
gleicherma&szlig;en gro&szlig;z&uuml;gig einl&auml;sst, dem steifen Winken, mit dem ein &ouml;sterreichischer 
Spie&szlig;b&uuml;rger karnevalistischen Frohsinn &uuml;bt. Von Verharmlosung, auch dies
ein Vorwurf, der Karmakars Filme mit Regelm&auml;&szlig;igkeit trifft,
kann dennoch keine Rede sein. Karmakars Hitler ist schon in
jungen Jahren ein narzisstischer Triebt&auml;ter, der die begehrte Cousine 
in den Tod treibt, sich &uuml;ber ihrem Selbstmord aber keineswegs 
von seinen Putschvorbereitungen ablenken l&auml;sst.</p>


<p>
<i>"Alles Dokumentarische ist real, alles Fiktive nicht unbedingt
falsch"</i>, hat Karmakar seinem siebzigmin&uuml;tigen Hitlerflm vorangestellt. 
So selten der Regisseur sich im Film erkl&auml;rt, seine (Selbst-)Versicherungen 
halten vor wie eiserne Reserven. Nicht nur EINE FREUNDSCHAFT IN DEUTSCHLAND, 
sondern auch COUP DE BOULE folgt diesem Vorsatz, der die Grenzen zwischen 
Spiel- und Dokumentarfilm zu bedenken gibt. Eine Realit&auml;t, die erst f&uuml;r die 
Kamera in Erscheinung tritt, gibt seinen Kurzfilmen den Anschein der 
Inszenierung, das Gepr&auml;ge von Minidramen. Dabei ist es vor allem Karmakars 
Themenwahl, die eine spontane Ann&auml;herung an die "ungeformte Realit&auml;t" 
vereitelt. Karmakars Interesse gilt Menschen, die vielerlei Gr&uuml;nde
haben, nicht in der &Ouml;ffentlichkeit auf sich aufmerksam zu machen. 
Seien es nun die franz&ouml;sischen Betreiber von Hahnenk&auml;mpfen, die 1988 
f&uuml;r GALLODROME ausnahmsweise eine Drehgenehmigung erteilten, seien es 
die Pitbullhalter vom Hamburger Kiez, die Karmakar 1989 f&uuml;r seinen Film 
HUNDE AUS SAMT UND STAHL die Haust&uuml;ren &ouml;ffneten: Zun&auml;chst wird in den
Filmen immer der Blickwinkel sichtbar, in dem eine sonst eher
unzug&auml;ngliche Gruppe von Au&szlig;enseitern sich dargestellt und gesehen wissen 
will. Karmakars au&szlig;ergew&ouml;hnliche Begabung, Fragen zu stellen, die nicht 
denunzieren, Blicke zu riskieren, die nicht richten, Unverst&auml;ndnis zu 
formulieren, nicht aber das ungl&auml;ubige Staunen des Bildungsb&uuml;rgers, 
erweitert den anf&auml;nglich gew&auml;hrten Spielraum. W&auml;hrend SPIEGEL und STERN, RTL
plus und die TAGESTHEMEN 1989, in einem Jahr, das die h&auml;ufigsten und 
furchtbarsten Pitbull-Angriffe zu verzeichnen hatte, Bisswunden zeigen 
und ein Verbot der Kampfhunde diskutieren, l&auml;sst sich Romuald Karmakar 
auf Gespr&auml;che &uuml;ber Eleganz und Edelmut der vierbeinigen Kampfmaschinen 
ein. Dass seine Interviewpartner, bis auf eine Ausnahme, aus der Zuh&auml;lterszene 
kommen oder Ex-Legion&auml;re sind, best&auml;tigt naheliegende Vermutungen &uuml;ber 
Training und Einsatz der Hunde. Das Fernsehen gibt sich mit solchen 
Fakten gern zufrieden. Karmakar akzeptiert sie und erf&auml;hrt mehr. Was 
sich ein Fernsehredakteur beim Thema Kiez-Szene vorstellt, hat sich 
Karmakar gefragt - und die Antwort gleich mitgeliefert: <i>"Einen Typen, 
der f&auml;hrt in seinem Mercedes die Reeperbahn runter, steigt dann irgendwo aus
im Halteverbot, auf der R&uuml;ckbank des Autos sitzen ein, zwei
Pitbulls, mit denen er zu einer Frau geht und die am besten noch
zusammenschl&auml;gt"</i>.</p>


<p>HUNDE AUS SAMT UND STAHL verhehlt nicht die Unberechenbarkeit 
der Portr&auml;tierten, dazu ist ihre Selbstdarstellung zu pr&auml;zise. 
Aber er f&ouml;rdert etwas zu Tage, das das Klischee geflissentlich 
unterschl&auml;gt: die Selbstverst&auml;ndlichkeit, mit der sich
ein Milieu als menschlich, als "normal" behauptet, das 
sich - seinem Ruf zufolge - eigentlich au&szlig;erhalb aller zivilisatorischer
&Uuml;bereink&uuml;nfte begreifen m&uuml;sste. Stattdessen entdeckt der Film
eine Gem&uuml;tslage, die den Pitbull-Liebhaber nur graduell vom 
fanatischen Normalhundbesitzer unterscheidet. Eine urdeutsche 
Hundeliebe gl&auml;ttet die mi&szlig;trauischen Gesichtsz&uuml;ge der Kiez-Kenner, 
wenn sie von ihren Pitbulls schw&auml;rmen. Ein Hund wie
ein Freund, ein Hund wie ein Vaterland, ein Hund wie die Familie. 
Die Treueschw&uuml;re, die Lobeshymnen, die Liebeserkl&auml;rungen an den 
todbringenden W&auml;chter, sie entspringen einer Sentimentalisierung 
der Gewalt, die alles andere als unb&uuml;rgerlich - und alles andere 
als undeutsch ist. Typen, denen man nicht im hellsten Sonnenschein 
begegnen m&ouml;chte, kuscheln sich mit sabbernden Viechern auf Pl&uuml;schsofas 
und beteuern Verst&auml;ndnis f&uuml;r einen Pitbull, der Sonntagsmorgens ins Ehebett dr&auml;ngt:
<i>"Wie ein Kind"</i>, sagen sie, <i>"es sind Menschen"</i>. Emp&ouml;rung
k&ouml;nnte das Klima, in dem solche S&auml;tze wachsen wie harmlos
aussehende Giftpilze, nie hervorbringen. Fragt Karmakar nicht
nach, so hat er gute Gr&uuml;nde. Dann muss der Zuschauer eben
nachh&ouml;ren. <i>"F&uuml;r mich"</i>, sagt der Filmemacher mit der 
schwierigen Klientel, <i>"ist eben genau das "Halbe" 
interessant - die Leute l&uuml;gen ja auch manchmal in die Kamera. 
Und in dieser L&uuml;ge steckt f&uuml;r mich mehr Ehrlichkeit als in der 
Wahrheit, die sie anderen anbieten"</i>.</p>


<p>Erkenntnis, die sich in einem Zwischenreich konstituiert, zwischen 
den Positionen, die man einnehmen kann, ohne das Gegen&uuml;ber an die Kamera, 
ohne sich selbst an die Gleichg&uuml;ltigkeit zu verraten, muss die Zusammenarbeit 
von Karmakar und Flatz erm&ouml;glicht haben. In der Welt der extremen M&auml;nnerspiele und
M&auml;nnerphantasien, die der Regisseur neugierig und obsessiv durchstreift, nimmt 
sich DEMONTAGE IX, die Dokumentation einer Performance des &ouml;sterreichischen 
Aktionsk&uuml;nstlers Flatz, denn auch eher als Zwischenspiel aus. Nicht, dass 
DEMONTAGE IX thematisch aus dem Rahmen fiele: Wiederum geht es um
eine ausgesprochen m&auml;nnliche Vorliebe f&uuml;r das Opfer und die
Gewalt, f&uuml;r Leiden und Selbst&uuml;berwindung, den Exhibitionismus des K&ouml;rpers, 
die Introvertiertheit der Sprache und des Gef&uuml;hls. Aber die Bearbeitung des 
fremden Materials hat Karmakar eine &auml;sthetische Strategie abverlangt, 
die seine Arbeiten bis dahin nicht hatten, nicht haben konnten, nicht wollten.</p>


<p>DEMONTAGE IX dokumentiert eine Performance, die ans Selbstm&ouml;rderische grenzt. 
Zwischen zwei Metallplatten baumelt kopf&uuml;ber ein K&ouml;rper. Ein "Gl&ouml;ckner" setzt den 
K&ouml;rper am Seil in Bewegung, bis er mit den st&auml;hlernen W&auml;nden kollidiert. 11 Minuten 
lang bringt das Pendel die "Stahlglocke" zum T&ouml;nen. Solange h&auml;lt sich die Kamera 
auf Distanz. Es bleibt ungewiss, ob es sich um einen Menschen oder um eine Puppe 
handelt. Der Blick des Betrachters, der sich kein Bild vom Ausma&szlig; seines
Unbehagens machen kann, ist ungesch&uuml;tzt wie selten im Kino. Vor der Konstruktion aus 
Fleisch und Stahl nimmt ein Paar Haltung an. Acht Minuten lang tanzen die beiden 
iranischen Europameister Walzer.</p>


<p>Der Walzer als kleinb&uuml;rgerliche Einstimmung aufs faschistoide "Wunschkonzert" hat 
in der deutschen Filmgeschichte einen Ton angegeben, der bis heute nachklingt. Die 
Sentimentalit&auml;t, die sich beim Walzer in Gang setzt und w&auml;hrend des 
Nationalsozialismus &uuml;ber Leichen hinwegging, ist so offensichtlich, dass sie 
als Kontrastmittel eigentlich nicht mehr taugt. Aber die Performance zielt 
auf die &Uuml;berdehnung der sentimentalen Bewegung. In der Wiederholung der 
Tanzschritte zeigt sich die nackte Anstrengung. Der hartleibige Ritus der 
Gewalt und_ das Ritual der leichtf&uuml;&szlig;igen Besch&ouml;nigung zehren gleicherma&szlig;en an
den K&ouml;rpern, das ist der eigentliche Affront. Ein dritter Abschnitt, der 
&uuml;ber das Abfilmen der Performance radikal hinausgeht, setzt das Auspendeln 
des K&ouml;rpers mit der Musik in Verbindung. Die Musik macht den Anblick ertr&auml;glich: 
Es sind diese pr&auml;zis inszenierten Momente der (Selbst-)Manipulation, denen mit 
Emp&ouml;rung wiederum nicht beizukommen ist. Nicht der Film ist ein Skandal, sondern 
die Leichtigkeit, mit der sich Wahrnehmung und Wertigkeiten beeintr&auml;chtigen lassen. 
Nach zwanzig Minuten wird der K&ouml;rper eines jungen Mannes vom Seil genommen. Dass in 
der Debatte um den Film allzu selbstverst&auml;ndlich von einer "Kreuzabnahme" die Rede 
war, bezeugt nur, in welchem Ma&szlig;e das Augenmerk auf die sinnstiftenden
Codices der Kunst, der Geschichte, der Kunstgeschichte fixiert ist. Der Mythos 
der Gewalt und der Gewaltt&auml;tigkeit sind Grundmuster der Performance, so wie 
Karmakars Kamera letztlich zur Entmythisierung des Geschehens beitr&auml;gt. Der Moment der
Erl&ouml;sung wird in der Wiederholung profanisiert. Zweimal (und aus unterschiedlichen 
Kameradistanzen) zeigt Karmakar, wie der K&ouml;rper vom Seil genommen wird.</p>


<p>Moralischen oder existenzialistischen Deutungen macht der Film 
keinen Mut. DEMONTAGE IX liefert kein Argument gegen Gewalt und Folter. 
Nicht, weil Karmakar indifferent w&auml;re. Seine Neugierde gilt dem, 
was sich noch (auf-) zeigen l&auml;sst, nicht der Gewissheit, sondern der Unstimmigkeit.</p>


<p>Ungew&ouml;hnlich fr&uuml;h, zumal f&uuml;r einen jungen deutschen Filmemacher, wurde 
Karmakars Filmschaffen in M&uuml;nchen 1989 retrospektiv gezeigt; 1990 folgte eine 
Retrospektive des Saarbr&uuml;ckener Max-Oph&uuml;ls-Festivals. 1992 wurde DEMONTAGE IX 
auf den Oberhausener Kurzfilmtagen ausgezeichnet. Alexander Kluge hat Karmakar 
schon seit geraumer Zeit entdeckt, nicht als Nachwuchs, sondern als Kollegen. 
Die Filmkritik hat sich mittlerweile &uuml;berrregional Gedanken dar&uuml;ber gemacht, warum 
der deutsche Film zutode gef&ouml;rdert wurde, w&auml;hrend einer wie 
Karmakar seine Filme allein finanzieren musste.</p>


<p>WARHEADS, ein Film &uuml;ber S&ouml;ldner, Legion&auml;re und Ex-Legion&auml;re, h&auml;tte 
etlichen F&ouml;rdergremien Gelegenheit zur Wiedergutmachung gegeben. Gef&ouml;rdert wurde 
der Film aus dem Jahr 1992 letztlich vom Kuratorium junger deutscher Film, von
der Berliner Filmf&ouml;rderung und dem Hamburger Filmb&uuml;ro - allerdings erst nach 
diversen Absagen. G&auml;nzlich zur&uuml;ckgenommen hat sich das Filmb&uuml;ro-Nordrhein Westfalen, 
dessen Argumentation die Annahme best&auml;tigt, dass es angebracht sein k&ouml;nnte,
Film(fach)leute in F&ouml;rdergremien zu berufen. Nur eine Wahrnehmung, die nicht dem 
Film, sondern wer wei&szlig; wessen politischen und moralischen &Uuml;berzeugungen Referenzen erweist,
konnte ein derart missliches Urteil hervorbringen. Das Gremium, lie&szlig; Nordrhein-Westfalens 
Filmb&uuml;ro wissen, <i>"f&ouml;rdere keine militaristischen Filme"</i>. Dieser Meinung schloss sich 
die Filmbewertungsstelle an, die dem Film kein Pr&auml;dikat zuerkennen mochte, ihm daf&uuml;r aber eine 
Beschreibung mit auf den Weg gab, die die Einrichtung von Deutschkursen f&uuml;r FBW-Protokollanten
nahelegt: <i>"In qu&auml;lender L&auml;nge"</i>, so der Bewertungsauschuss &uuml;ber
den dreist&uuml;ndigen Film, <i>"ziehen Bilder &uuml;ber die Leinwand, die
teilweise wie unbearbeitetes Rohmaterial von Amateurfilmern
wirken. Die Unverbindlichkeit der Aussage bleibt im Affirmativen stecken 
und wird noch durch die Anbieterei in devoter Haltung gegen&uuml;ber den 
Interviewpartnern verst&auml;rkt. Der Filmemacher setzt sich leider nicht in 
analytischer Form mit den eingef&uuml;hrten Personen oder m&ouml;glichen Themen des 
Films auseinander. Vielmehr weist die additive H&auml;ufung aller m&ouml;glichen
Aspekte auf eine mangelnde Dramaturgie und ein extrem oberfl&auml;chliches 
und erschreckend naives pers&ouml;nliches Interesse am S&ouml;ldnertum hin"</i>. 
Fehler sind Bestandteil dieses Gutachtens.
Aber folgenreicher als Schreibfehler und Stilbl&uuml;ten es sein k&ouml;nnen, 
ist der Denkfehler, WARHEADS als Nachhut eines in der
Tat kritikw&uuml;rdigen Kriegs- und S&ouml;ldnergenres auszumachen.
Nicht, dass allein die dokumentarische Aufbereitung der Erinnerungen 
gewesener und aktiver Legion&auml;re Karmakar vom Verdacht der besch&ouml;nigenden 
Parteinahme befreite. Aber in der Geschichte des deutschen Dokumentarfilms 
haben sich eher jene Darbietungen der Propaganda schuldig gemacht, in denen
ohne Unterlass Bilder kommentiert, montiert und gedeutet wurden, in denen 
dramaturgisch aus-, und zugerichtet wurde, was andere anrichteten. 
Karmakar ist diese Methode so fern, wie die moderne, bisweilen auch nur modische 
Form der Filmanalyse im Film, die die FBW einklagt. Stattdessen hat er sich
abermals auf ein Milieu eingelassen, von dem in Spionagethrillern und Krimis 
eher spekulativ die Rede ist, von dem kaum jemand wei&szlig;, und nur selten 
jemand es genauer wissen will.</p>


<p>Eine Anzeige in der M&uuml;nchener Abendzeitung ("Ex-Legion&auml;r f&uuml;r Filmprojekt in USA gesucht") 
hat Karmakar in Kontakt mit G&uuml;nther Aschenbrenner gebracht. WARHEADS, Teil I, widmet
sich &uuml;berwiegend der Befragung und Selbstdarstellung des geb&uuml;rtigen Deutschen, 
der zwischen 1958 und 1978 unter anderem als Fallschirmspringer in Algerien, 
Stra&szlig;enbauer in Franz&ouml;sisch-Guayana und Teilnehmer an 17 &uuml;berseeischen
Atomversuchen der franz&ouml;sischen Fremdenlegion gedient hat.
Aschenbrenners Erinnerungen, die aus der Sicht eines ranghohen 
Ex-Legion&auml;rs der Legion fraglos nur Ruhm und Ehre zusprechen, 
konterkarieren die paramilit&auml;rische Weltsicht auf eine
Weise, die dem Erz&auml;hler selbst nicht bewusst ist. Dem Umfeld
seiner eigenen Familie, so Aschenbrenner, sei er entflohen,
<i>"weil man &uuml;berall daran erinnert wurde, dass man aus einer Nazifamilie kam"</i>. 
Die Fremdenlegion wird zur Ersatzfamilie des 1939 geborenen Deutschen: 
Nicht etwa, weil sie sich von den politischen Vorgaben der leiblichen Eltern 
unterscheidet, wohl aber, weil sie ihre faschistoiden Tendenzen unschlagbar zur
(Truppen-)Moral stilisiert. Warum die Seele der Legion deutsch
gewesen sei, will Karmakar wissen. Die Antwort legt eben jene Familienbande 
blo&szlig;, aus denen sich Aschenbrenner vorgeblich hatte l&ouml;sen wollen. 
Trainingsstil, M&auml;rsche und Disziplin seien so deutsch gewesen wie die Kriegsgefangenen, 
die die Wahl zwischen Legion und fortgesetzter Gefangenschaft gehabt
h&auml;tten: <i>"Abends im Foyer"</i>, so Aschenbrenner unbek&uuml;mmert,
<i>"sang man auch Nazilieder"</i>. Karmakars Interesse an den Reaktionen 
der franz&ouml;sischen Offiziere, die immerhin das Dritte Reich bek&auml;mpft hatten, 
indignieren Aschenbrenner in einem Ma&szlig;e, das filmintern keinerlei Analyse 
bedarf: <i>"Offziere"</i>, so der Befragte, <i>"sind Pers&ouml;nlichkeiten, 
die nach zwei Jahren wechseln"</i>. Was bleibt, ist die Truppe.</p>


<p>Es ist diese Form der selbstredenden Enth&uuml;llung, die WARHEADS zu einem 
erstaunlichen Film macht. Seit HALF LIFE von Dennis O'Rourke, der 1985 Zeugen 
f&uuml;r die von Amerika ma&szlig;stabsgerecht vorbereiteten Atomkatastrophen auf den 
Bikini-Atollen zum Plaudern brachte, hat sich milit&auml;risches Selbstverst&auml;ndnis 
kaum je mehr derart ungeniert vor laufender Kamera pr&auml;sentiert. Nicht, dass 
Karmakars Gew&auml;hrsmann Kriegsgeheimnisse verriete. Aber Aufbau und Drill der 
Legion, die F&uuml;hrung "hauseigener" Bordelle, die es der Legion als Zuh&auml;lter 
erm&ouml;glicht, den kargen Sold ihrer Bediensteten auf Umwegen wieder
einzukassieren, sowie die Systematik einer Frauenfeindlichkeit,
die aus den Regeln und Vorsichtsma&szlig;nahmen f&uuml;r den Fall einer der 
eher seltenen Eheschlie&szlig;ungen spricht, geben sich in
Aschenbrenners noch erz&auml;hltaktisch militarisierter Weltenordnung alles 
andere als "unverbindlich".</p>


<p>Aus den Filmausschnitten, die in einem paramilit&auml;rischen Trainingscamp in Mississippi 
zustandegekommen sind, l&auml;sst sich  dagegen ablesen, was Betrachter, die Gesehenes von Geh&ouml;rtem
nicht trennen, dazu verleiten mag, von der Affirmation des Films f&uuml;r sein Thema zu sprechen. 
In dem Camp findet die Verst&auml;ndigung &uuml;ber das N&ouml;tigste - <i>"so entsichert man diese Waffe,
so legt sie los"</i> - auf englisch statt, man h&ouml;rt deutsche und franz&ouml;sische Laute: 
<i>"Die Seele der Legion ist &uuml;berall"</i>. Ein Ausbilder &uuml;bt mit den M&auml;nnern, 
die sich ob ihrer Tarnkappen und dem Dreck zum Verwechseln &auml;hnlich sehen, das Kapern von
Wagen in Feindesland oder Anschleichen unter versch&auml;rften Bedingungen. Als es gilt, 
sich drei Minuten lang einem Reizgas auszusetzen, nimmt der Ausbilder den Kameramann zur 
Kenntnis - und ins Visier: <i>"Das stinkt so"</i>, sagt der Mann zufrieden, 
<i>"dass ihr noch was davon abkriegt"</i>.</p>


<p>Wer etwas mitkriegen will von den extremen Willenskundgebungen dieser 
verzerrten Gesichter, diesen verzerrten Perspektiven und Pers&ouml;nlichkeiten, 
muss "etwas davon abkriegen", das ist die Regel, der Karmakars Team unterliegt. 
Sie verlangt nicht nach Anteilnahme, aber nach Teilnahme. Kriegsbeobachtern gleich 
haben die fremden Zuschauer, die t&ouml;dlichen Handgriffen auf die Finger gucken, der 
Mimesis Tribut zu zollen. Die &Uuml;bung, das macht das humorlose, der Ausnahme, der Abweichung 
nicht gewachsene Ritual sichtbar, ist der Ernstfall.</p>


<p>In dem Gespr&auml;ch mit Rolf Aurich hat Karmakar sich zu Recht schon vor 
Beginn des Projekts Gedanken &uuml;ber die schauspielerischen F&auml;higkeiten seiner 
Mitarbeiter gemacht: <i>"Du bist da zwei Wochen, schl&auml;fst im Freien, musst 
im Grunde einen Kampfanzug tragen, bist auf Patrouille. Es muss also jemand sein, der
sich in dieser Szene, die politisch nat&uuml;rlich eine ganz bestimmte F&auml;rbung hat, bewegen 
kann, der nicht anf&auml;ngt, mit dir politische Diskussionen zu f&uuml;hren. Ich kann keine 
Tunte mitnehmen. Oder jemand, der einen Zopf hat oder einen Vollbart, oder jemand, 
der aussieht wie Ho Tschi Minh. Da wird es halt knapper mit den Kameraleuten."</i>
</p>


<p>F&uuml;r die Zeit, in der das Team sich im Lager aufh&auml;lt, muss sich
die Kamera wie ein Legion&auml;r, wie ein S&ouml;ldner bewegen - wenn
sie denn etwas sehen will. So sind die Kriech&uuml;bungen der Truppe
nicht aus der Draufsicht, sondern im Kriechen gefilmt. Dass sich
Karmaka dabei nicht anpasst, sondern den vorgeschriebenen Bewegungsablauf 
zur Reflexion filmischer Vorgehensweisen nutzt, zeigt sich anhand einer 
Schie&szlig;&uuml;bung. W&auml;hrend die Studenten genannten Teilnehmer der "Survival School" 
hintereinander ein Ziel aufs Korn nehmen, h&auml;lt auch die Kamera an einer 
Schussposition fest. Die Wiederholung, die kein Spielfilm besser in
Szene setzen k&ouml;nnte, um das Resultat eines <i>shots</i> zu optimieren, 
wird zum irritierenden Bestandteil der Dokumentation. Je unnachgiebiger und 
"standfester" Karmakar das Ritual der Wiederholung festh&auml;lt, die Kampfschreie, 
die exakt jene Drehung der Hand beantworten, die Tritte, die akkurat jene Stimmlage
treffen m&uuml;ssen, desto unwirklicher wird die Perfektion der Observierten: Man muss 
sich - und das ist Ziel dieser Art &Uuml;bung - in Erinnerung rufen, dass es sich nicht 
um flei&szlig;ige Bruce-Lee-Imitatoren handelt, sondern um Spezialisten, die sich 
weltweit zur Erledigung jeder nur denkbaren, politischen Dreckarbeit andienen.</p>


<p>Wie weit dabei Tat und Selbstdarstellung auseinanderklaffen, 
l&auml;sst sich besonders sch&ouml;n nachvollziehen, wenn G&uuml;nther
Aschenbrenner im zweiten Teil des Films von seiner Altersvorsorge 
spricht. Auch ein Job als Sicherheitschef der deutschen
Firm Ortrag, die Atomm&uuml;ll experimenthalber einfach ins All
schie&szlig;t, war f&uuml;r den Ex-Legion&auml;r kein Problem. Seit 1989 ist
der Mann mit dem symboltr&auml;chtigen Namen in Afrika besch&auml;ftigt.</p>


<p>Einen nachdenklicheren Gespr&auml;chspartner, der Aberwitz und
Irrsinn seiner geliehenen (Kriegs-)Positionen erstaunlich luzide 
aufzudr&ouml;seln versteht, hat Karmakar in dem englischen S&ouml;ldner Karl 
gefunden. 1950 in Liverpool geboren, hat sich der ehemalige 
Seemann seit 15 Jahren auf das Dasein als S&ouml;ldner verlegt.
1991 sucht ihn Karmaka in Kroatien auf. In Gospic,
vier Kilometer von der Front entfernt, filmt er nicht Leichen,
sondern Einschussl&ouml;cher in H&auml;userw&auml;nden, das unendlich langsame 
Vorw&auml;rtskommen der &ouml;rtlichen Feuerwehr, die bl&auml;uliche
K&auml;lte einer Welt, in der der Mord an Tausenden von Menschen
zu einer unwirklich wirkenden Handlung ger&auml;t. Auf einem ehemaligen 
Aussichtspunkt installiert Karl Raketen, setzt ihnen
"Warheads" auf, Sprengk&ouml;pfe, die wie silbrige Karnevalskappen 
aussehen. Die Bastelei an den Raketen erinnert an die Handhabung 
eines Sylvesterfeuerwerks. Eine schwarze Rauchspur, die Risse in das 
Porzellan des Himmels sprengt, begleitet die Detonation; weit entfernt 
von den Initiatoren und den Beobachtern des kriegerischen Akts ist ein 
Einschlag zu h&ouml;ren. Die Abstraktion dieses Vernichtungsschlags hat etwas 
vom Grauen eines Trick-Films, der als Dokumentation endet.</p>


<p>Im Februar 1992 kehrt Karl nach England zur&uuml;ck. Karmakar
nimmt sein Interview wieder auf. Mag sein, dass die "Warheads", 
die Kahlk&ouml;pfe der Legion, Wirrk&ouml;pfe sind, Karl ist es
nicht. Der Mann hat einen Realit&auml;tssinn, der das gesungene 
Kameradschaftsbrimborium der Legion, ihre faschistoiden 
Kleider- und Fickvorschriften, ihre Lust an der Unterwerfung und dem
Unterwerfen einer Ineffektivit&auml;t zeigt, die sich im Krieg nicht
rechnet: Mit welchem Hut auf dem Kopf er t&ouml;te, das sei ihm
so gleich wie das Ideal, in dessen Auftrag rund um den S&ouml;ldner
gestorben und get&ouml;tet wird. Was Karl sich eingesteht, das macht
ihn f&uuml;r die betr&uuml;gerischen Selbstbetr&uuml;ger der salbungsvollen Legion 
zum Immoralisten. Zwar t&ouml;ten auch Legion&auml;re, ohne nach
Auftraggebern zu fragen, aber die h&ouml;here Daseinsform als Mitglied 
einer nationalistischen Elitetruppe scheint die Mittel zu heiligen. 
Karl macht sich auch weniger Illusionen &uuml;ber die Moral
der Welt&ouml;ffentlichkeit. Die Brigaden der UNO erscheinen dem
professionellen Skeptiker naiv: <i>"Die haben noch nicht gek&auml;mpft.
Sonst w&uuml;rden sie es nicht umsonst tun."</i>
</p>


<p>Zynismus als letzte Zuflucht? Karmakar hat sich f&uuml;r WARHEADS 
ein anderes Ende vorbehalten. In Gospic hat er eine junge Kroatin in 
Uniform getroffen, die aus M&uuml;nchen angereist ist, um Kroatiens Freiheit 
zu verteidigen. &Uuml;ber ihren Krieg hat sie genaue Vorstellungen: 
<i>"Ich werde Serben t&ouml;ten"</i>. Auch wie der
Frieden aussehen soll, wei&szlig; sie: <i>"Serben, die sich dann noch 
nach Kroatien trauen, erschie&szlig;e ich"</i>, sagt sie hasserf&uuml;llt. Wenig 
sp&auml;ter sieht man das M&auml;dchen zusammen mit anderen jungen Frauen bei einer 
Schie&szlig;&uuml;bung. Ein Ausbilder legt ihnen das Gewehr auf die Schultern, die Frauen 
kichern und lachen, ein Lachen f&uuml;r die Verlegenheit, ein Lachen f&uuml;r die Angst.</p>

    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><a href="#sec-sinn">Der Sinn des Ganzen</a></small>
</li>
<li>
<small><a href="#sec-ueberblick">Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms</a></small>
</li>
<li>
<small><a href="#sec-wildenhahn">Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn</a></small>
</li>
<li>
<small><a href="#sec-dokuessay">Dokumentarfilme - Vom Dokument zum Essay</a></small>
</li>
<li>
<small><a href="#sec-drama">Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?</a></small>
</li>
<li>
<small><a href="#sec-bitomsky">Spurensuche: Der Filmemacher Hartmut Bitomsky</a></small>
</li>
<li>
<small><a href="#sec-vondokuzuspiel">Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm</a></small>
</li>
<li>
<small><a href="#sec-kingkong">King Kongs Kinderstube: Dokumentarfilm als Abenteuer</a></small>
</li>
<li>
<small><a href="#sec-krieg">Fliegende Pfarrer und Hollywood im Krieg</a></small>
</li>
<li>
<small><a href="#sec-littlebird">Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation</a></small>
</li>
<li>
<small><a href="#sec-letzterblick">Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)</a></small>
</li>
<li>
<small><a href="#sec-robertkramer">Portrait: Robert Kramer in M&uuml;nchen</a></small>
</li>
<li>
<small><a href="#sec-harunfarocki">Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki</a></small>
</li>
<li>
<small><a href="#sec-romualdkarmakar">Blicke, die nicht richten: Die Filme von Romuald Karmakar</a></small>
</li>
</ul>
