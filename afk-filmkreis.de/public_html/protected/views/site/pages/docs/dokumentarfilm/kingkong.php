<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.vondokuzuspiel','docs.dokumentarfilm.krieg')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-kingkong"></a>
<h3>King Kongs Kinderstube<br>
<small>Dokumentarfilm als Abenteuer</small>
</h3>


<p>Kameram&auml;nner scheinen schon immer wichtige Vermittler zwischen Spiel- und
Dokumentarfilm gewesen zu sein.</p>

<p>Ernest B. Schoedsack (1893-1979) verfolgte und filmte als Berichterstatter den ersten
Weltkrieg direkt an der Front. Er geh&ouml;rte zu den damals nicht seltenen Menschen, die ihre
Abenteuerlust in extremen Situationen auslebten. Das Festland Europa bot in jener Zeit mit
seinen vielen Kriegen dazu reichlich Gelegenheit. So begegnete er 1925 in Polen einem
anderen Abenteurer: Merian C.Cooper, der dort in der Luftwaffe k&auml;mpfte. In die
Filmgeschichte sollten die beiden 1933 mit ihrem Fantasy-Klassiker KING KONG eingehen,
bei dem Schoedsack Regie f&uuml;hrte_</p>


<p>Zuvor hatten Cooper und Schoedsack bereits mit zwei spektakul&auml;ren Dokumentarfilmen
Aufsehen erregt: GRASS - A NATIONS STRUGGLE FOR LIFE (1925) und CHANG (1927). 
Geholfen hat ihnen indirekt ihr Regiekollege Robert Flaherty. Nachdem dessen Film
NANOOK OF THE NORTH (1922) beim amerikanischen Publikum erfolgreich gewesen ist,
waren die Studios bereit, Geld in Dokumentarfilme vor exotischem Hintergrund zu investieren.
GRASS schildert die gefahrenvolle und verlustreiche Suche der Bakthiaris, einem
Nomadenvolk im Iran, nach neuem Weideland f&uuml;r ihre Herden. Bereits in ihrem ersten
Dokumentarfilm lassen Cooper und Schoedsack ihre Neigung zum gro&szlig;en Abenteuer
erkennen: das Nomadenvolk auf seiner Wanderung wird effektvoll vor den beeindruckenden
Landschaftspanoramen in Szene gesetzt. Allerdings wird auch der Tod nicht ausgespart. Wenn
der Nomadenstamm einen rei&szlig;enden Strom oder einen Gletscher &uuml;berquert, wirkt dies zwar
wie eine spannende Szene aus einem Tarzanfilm; die Menschen, die bei den halsbrecherischen
Unternehmungen ums Leben kommen, sind jedoch echte Todesopfer und nicht nur
austauschbare Statisten.</p>


<p>CHANG erz&auml;hlt die Geschichte vom Kampf einer Familie im siamesischen Dschungel. Noch
deutlicher als in GRASS wird hier Coopers Hang zum Inszenieren und Konstruieren. Wenn es
die Handlung unterst&uuml;tzt oder zur Spannung beitr&auml;gt, werden einige Szenen (z.B. die
Elefantenstampede) sogar nachgestellt.</p>


<p>Mit ihren beiden Dokumentarfilm-Klassikern haben Cooper und Schoedsack nicht nur den
optischen Standard f&uuml;r s&auml;mtliche Tarzan- und Dschungelabenteuerfilme gesetzt, sondern auch
ihre nachfolgenden Spielfilme MOST DANGEROUS GAME (1932) und KING KONG (1933) vorweggenommen. 
Die Themen und Schaupl&auml;tze bleiben die gleichen (Dschungel, &Uuml;berlebenskampf in feindlicher Umgebung), 
nur die Sichtweise ist eine andere; einmal dokumentarisch, einmal fiktiv.</p>


<p>Ein anderer Regisseur und Kameraspezialist, der Gefallen an exotischen Schaupl&auml;tzen
fand und den Dokumentarfilm als Abenteuer verstand, war der bereits erw&auml;hnte Robert
Flaherty. Mit NANOOK OF THE NORTH (1922) und MOANA (1926) hat er die
Dokumentation in eine neue Richtung gelenkt und sie f&uuml;r ein gr&ouml;&szlig;eres Publikum &uuml;berhaupt erst
interessant gemacht. Flaherty wurde sp&auml;ter von seinem Studio (Paramount) h&auml;ufig eingesetzt,
wenn es galt, andere Regisseure bei Szenen im Dschungel oder in der Wildnis k&uuml;nstlerisch zu
beraten. So hat er z.B. f&uuml;r William S. Van Dyke die Au&szlig;enaufnahmen zu WHITE SHADOWS
OF THE SOUTH SEA (1928) gedreht, einem Film im Stil der FOUR FEATHERS von Schoedsack.</p>


<p>Entt&auml;uscht von der romantisierenden Art dieses Films, die den dokumentarischen Aspekt
zur&uuml;ckdr&auml;ngte, nahm er das Angebot eines realistischen Regisseurs an, mit diesem einen Film
&uuml;ber die S&uuml;dsee zu drehen. Durch diese Zusammenarbeit entstand einer der Klassiker der
Filmgeschichte: TABU von Friedrich Wilhelm Murnau.</p>


<p>Schon in seinem vorangegangenen Film OUR DAILY BREAD (1929) hatte Murnau eine
fiktive Geschichte mit einer Dokumentation &uuml;ber die Entstehung des Brotes
zusammengeschnitten.</p>


<p>Auch in TABU benutzte Murnau diese Technik, die dokumentarischen Abschnitte und die
Geschichte (die Murnau zusammen mit Flaherty ausgearbeitet hatte) sind aber enger verwoben.
Dies f&uuml;hrte zu Meinungsverschiedenheiten zwischen Murnau und Flaherty. Flaherty war als
geborener Dokumentarfilmer ausschlie&szlig;lich am sozialen Aspekt der Geschichte interessiert,
d.h. an den Lebensbedingungen der Eingeborenen, die als Perlentaucher von chinesischen
H&auml;ndlern ausgebeutet werden. Murnau hingegen war zwar Realist, doch das bedeutete f&uuml;r ihn
nicht, dass er die Wirklichkeit naturalistisch abzubilden hatte. Schon bei Schoedsack zeigte sich,
dass ein Regisseur im Zweifelsfall bereit ist, f&uuml;r einen Effekt die Realit&auml;t zu opfern. Murnau
geht in TABU einen wesentlichen Schritt weiter. Er zeigt die "gl&uuml;cklichen Inseln" der S&uuml;dsee,
und damit eine Realit&auml;t, die schon nicht mehr existierte, als er zu filmen begann. Zwar erscheint
in seinem Film auch die Zivilisation (die chinesischen H&auml;ndler), doch nur als Bestandteil der
Geschichte des Films. Die bemerkenswerten Szenen, die man von diesem Film beh&auml;lt, sind
diejenigen, in denen die verlorene Wirklichkeit der S&uuml;dsee beschworen wird: die badenden
M&auml;dchen, die ausfahrenden Fischer, die Landschaft...</p>


<p>Diese Idealisierung ist f&uuml;r den Dokumentarfilm eigentlich untypisch (wenn man von
Kurzfilmen, Industriedokumentationen und &Auml;hnlichem absieht).</p>


<p>Der Dokumentarfilm steht in enger Verbindung zum (literarischen) Realismus bzw.
Naturalismus. Dieser entstand in der Ablehnung des romantischen Idealisierens, zugunsten
einer ungesch&ouml;nten Sicht der Welt, und dies war meist die Welt der Armen. Man denke an
Georg Hauptmanns DIE WEBER und sein PHANTOM (das von Murnau 1922 verfilmt
wurde). Dies zeigt sich auch in den Brecht-Verfilmungen der zwanziger Jahre, die heute als
Zeitdokumente bewertet werden. Dieser moralische Aspekt des Realismus/Naturalismus ging
in den Surrealismus ein und veranlasste Bu&ntilde;uel, einen Dokumentarfilm zu drehen.</p>


<p>Bu&ntilde;uel hatte sich in der &Ouml;ffentlichkeit mit seinen beiden Skandalfilmen UN CHIEN
ANDALOU (1928) und L'AGE D'OR (1930) als surrealistischer Filmemacher par excellence
etabliert. In beiden Filmen &uuml;berwiegt der poetische Aspekt des Surrealismus. Mit LAS
HURDES (1932) wendet sich Bu&ntilde;uel von dieser Art des Surrealismus ab. Es geht ihm nicht
mehr nur um Poesie, er will auch etwas mitteilen.</p>


<p>Mit seinem Kameramann Eli Lotar macht sich Bu&ntilde;uel auf in die spanische Berglandschaft der
Hurdes. Die Menschen, die dort weitab von der Zivilisation einen t&auml;glichen Kampf gegen die
Natur f&uuml;hren, sind kranke Menschen. Sie wissen, dass sie den Kampf verlieren werden. Ihre
Erl&ouml;sung ist erst der Tod. Bu&ntilde;uel zeigt dieses Elend in klaren, uninszenierten Bildern. Das
macht einen Gro&szlig;teil ihrer grausamen, erschreckenden Wirkung aus. Indem er das poetische
Moment beinahe ganz zur&uuml;cknimmt, erreicht er eine umso st&auml;rkere Wirkung. Die wenigen
Stilmittel, die Bu&ntilde;uel verwendet, arbeiten mit Widerspr&uuml;chen: etwa der n&uuml;chterne, fast
wissenschaftliche Kommentar, mit dem die Bilder unterlegt sind, oder Brahms Vierte
Symphonie als Begleitmusik.</p>


<p>In den sp&auml;teren Filmen von Bu&ntilde;uel kamen die beiden Gegens&auml;tze: Poesie und Realismus
(Moral) zu einem ausgewogeneren Verh&auml;ltnis. Meistens stand die Poesie im Vordergrund. Nur
noch in LOS OLVIDADOS ist die poetische Seite &auml;hnlich stark zur&uuml;ckgenommen und das
moralische Anliegen so deutlich.</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
