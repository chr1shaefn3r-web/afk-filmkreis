<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.bitomsky','docs.dokumentarfilm.kingkong')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-vondokuzuspiel"></a>
<h3>Als die Wirklichkeit laufen lernte...<br>
<small>Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm</small>
</h3>
    

<p>
<i>"Dokumentarfilme haben mich immer besonders angezogen. Sie stellen 
eine Form des Filmemachens dar, die einem erlaubt, die Dinge so zu filmen, 
wie sie passieren - ohne einzugreifen. Die Kamera liegt auf der Lauer, wie 
ein J&auml;ger, und wartet auf Bilder, die die Wirklichkeit produziert. Dann kam 
eine Zeit, in der ich das Interesse an dieser Art der Dokumentation, am 
cin&eacute;ma v&eacute;rit&eacute;, verlor, weil ich ihre Beschr&auml;nkungen erkannte. Wenn man
darauf wartet, dass etwas Wichtiges passiert, geschieht entweder gar nichts 
oder nur etwas v&ouml;llig Belangloses. Man kann zwanzig Tage lang hinter seiner 
versteckten Kamera lauern, wie ich es f&uuml;r meinen kubanischen Film 
GENTE EN LA PLAYA getan habe, und am Ende hat man lediglich die Oberfl&auml;che 
der Dinge aufgenommen.... Deshalb habe ich mich dem Spielfilm zugewandt, 
um eine Geschichte zu erz&auml;hlen und um mit Schauspielern zu arbeiten. Mit 
anderen Worten, ich habe angefangen, die Art von Filmen zu drehen, die ich 
als junger Mann verachtet habe."</i> <small>(Nestor Almendros in "A Man with a Camera")</small>
</p>


<p>Der letztes Jahr verstorbene Kameramann Nestor Almendros steht 
beispielhaft f&uuml;r eine Reihe von Filmemachern, die ihre Karriere mit 
Dokumentarfilmen begonnen haben, bevor sie mit Spielfilmen einem 
breiten Publikum bekannt wurden. Die im Umgang mit dem Dokumentarfilm 
gewonnenen F&auml;higkeiten kommen ihrer Arbeit mit fiktiven Stoffen hierbei
meist zugute. So zeichnet fast alle Filme von Almendros der sparsame 
Umgang mit kameratechnischen Mitteln aus. Wo immer es ging, versuchte er 
mit der f&uuml;r den Dokumentarfilm obligatorischen "available light photography" 
auszukommen, d.h. die Dinge nach M&ouml;glichkeit ohne Hilfe zus&auml;tzlicher 
Ausleuchtung abzubilden.</p>


<p>Seine wichtigsten Dokumentarfilme drehte Almendros mit dem iranischen 
Filmemacher Barbet Schroeder; GENERAL IDI AMIN DADA (1974) und 
KOKO, THE TALXING GORILLA (1977). Mit Roberto Rossellini (ROMA, 
CITTA APERTA, 1945) drehte er im Auftrag der franz&ouml;sischen Regierung 
eine Dokumentation &uuml;ber eines der am kontroversesten diskutierten
Geb&auml;ude in Paris: LE CENTRE GEORGES POMPIDOU (1977).</p>


<p>Almendros' Kollege, der amerikanische Kameramann Haskell Wexler, 
war auf dem Gebiet des Dokumentarfilms noch wesentlich intensiver 
t&auml;tig (auch als Regisseur), bevor er sich dem Spielfilm zuwandte. Zu seinen 
bekanntesten Dokumentationen geh&ouml;ren der halbdokumentarische 
MEDIUM COOL (1969), THE BUS (1965) und NEWSREEL (1969). Mit den Filmen 
BRAZIL: A REPORT ON TORTURE (1971) und INTERVIEWS WITH MY LAI VETERANS (1971) 
verl&auml;sst er die Position des Beobachters und bezieht gewollt Stellung.
Zu Haskell Wexlers wichtigsten Spielfilmarbeiten geh&ouml;ren 
IN THE HEAT OF THE NIGHT (1967, R.: Norman Jewison), 
THE CONVERSATION (1973, R.: Francis Ford Coppola) und
ONE FLEW OVER THE CUCKOO'S NEST (1975, R.: Milos Forman). F&uuml;r seine
Kameraarbeit zu WHO'S AFRAID OF VIRGINIA WOOLF (1966, R.: Mike Nichols) wurde
er sogar mit dem Oscar ausgezeichnet</p>

    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
