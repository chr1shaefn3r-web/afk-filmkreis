<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.ueberblick','docs.dokumentarfilm.dokuessay')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-wildenhahn"></a>
<h3>Das Abenteuer Dokumentarfilm<br>
<small>Zum 60. Geburtstag von Klaus Wildenhahn</small>
</h3>

<p>
<i>"Der eigenproduzierte lange
Dokumentarfilm im Fernsehen geht wahrscheinlich
seinem Ende entgegen."</i> So
lautete die pessimistische Prognose von 
Werner Filmer, stellvertretender Chefredakteur des
Programmbereichs Politik im WDR auf einem
Symposium &uuml;ber den "Dokumentarfilm im
Fernsehen der Bundesrepublik" im Oktober
vergangenen Jahres an der Universit&auml;t 
Marburg. Dass Ausnahmen diese Prognose in der
Tendenz best&auml;tigen, mag ein schwacher Trost
sein. Aber es gibt sie immerhin noch, auch
wenn der Dokumentarfilm heute redaktionell
weitgehend heimatlos zwischen den verschiedenen 
Programmbereichen der Kultur, dem
Fernsehspiel und der aktuellen Politik vagabundiert.</p>


<p>Der Dokumentarist Klaus Wildenhahn, der am
19. Juni seinen 60. Geburtstag feierte, ist eine
solche Ausnahmeerscheinung. Er macht seit
knapp 25 Jahren lange Dokumentarfilme f&uuml;rs
Fernsehen. Etliche davon sind zu Klassikern
des Mediums geworden und ihr Autor inzwischen 
zu einem respektablen "Fossil", dem
man gleichwohl noch viele produktive Jahre im
"&ouml;ffentlich-rechtlichen" Fernsehmedium w&uuml;nschen 
m&ouml;chte, das er als "gute Erbschaft unserer Besatzungsm&auml;chte" 
sch&auml;tzt, weil es f&uuml;r ihn <i>"das nicht-komerzielle in 
Arbeitsweise und Methode"</i> erm&ouml;glicht.</p>


<h4>SOLIDARISCH MIT DEN MENSCHEN</h4>

<p>Es sind Filme &uuml;ber die Arbeit und den Alltag,
&uuml;ber Landarbeit, Bauarbeit, Industriearbeit;
aber auch: St&uuml;cke &uuml;ber die M&uuml;hen der Kunst,
die Schwierigkeiten &auml;sthetischer Produktion.
Das klingt thematisch nach Lehrfilm und biederem 
Bildungsprogramm. Doch dem ist zum
Gl&uuml;ck nicht so, auch wenn Wildenhahn - seit
1964 als festangestellter Dokumentarfilm-Regisseur 
beim NDR - derzeit in der Redaktion
"Philosophie, Geschichte, Bildung" beheimatet
ist. Seine Filme gehen stets mit voraussetzungsloser 
Neugier an ihre Themen heran.
Dem Prinzip der teilnehmenden Beobachtung
verpflichtet, solidarisch mit den Menschen, die
er zeigt, machen sie den Zuschauer zum Augen- und 
Ohrenzeugen von Entdeckungsreisen.</p>


<p>In Wildenhahns Filmen kommen Menschen zu
Wort und ins Bild, die das Fernsehen nur allzu
gern zugunsten von Expertenbefragungen und
besserwisserischem Kommentar vernachl&auml;ssigt.</p>


<p>Sie unterscheiden sich krass von dem, was der
Fernsehprogramm-Alltag an dokumentarischen Formen 
bereith&auml;lt und haben nichts gemein mit den &uuml;blichen 
ins 45-Minuten-Zeitschema eingepassten Fernsehfeatures. 
Lange synchron gedrehte Einstellungen nehmen sich
Zeit f&uuml;r die Personen, die Wildenhahn mit einer
nur schwer beschreibbaren emotionalen Qualit&auml;t 
portr&auml;tiert. Sein sparsamer Kommentar, den
er stets selbst spricht, ist ebenso unpr&auml;tenti&ouml;s
wie die Filmbilder. Er informiert den Zuschauer 
in knappen Stichworten &uuml;ber die Vorbereitung 
und den Ablauf der Dreharbeiten,
macht ihn zusammen mit dem Originalton zum
Ohrenzeugen der Recherche.</p>


<h4>REISEN DURCH LANDSCHAFTEN</h4>


<p>Wildenhahns Filme sind auch Filme der Bewegung 
durch Landschaften: topographische und
historische Entdeckungsfahrten; Reisen, deren
Routen geographisch wie historisch aus der 
vorindustriell-agrarisch gepr&auml;gten Provinz ("In der
Fremde", 1967; "Die Liebe zum Land", 1973/74) bis 
in die ehemaligen, jetzt ausgebluteten
Metropolen der Schwerindustrie f&uuml;hren und
vor den Problemen der Zukunft einer 
Arbeitsgesellschaft, der die Arbeit ausgeht, nicht den
Blick verschlie&szlig;en ("Stillegung", 1987; "Rheinhausen Herbst 1988").
Jenseits der expliziten Inhalte bleibt ihr atmosph&auml;risches 
Gesp&uuml;r f&uuml;r die Topographie, f&uuml;r Bilder und T&ouml;ne, die die 
Lebenswelt der Protagonisten mit viel Sinn f&uuml;r scheinbar Beil&auml;ufiges
detailreich erfassen, im Ged&auml;chtnis: Eine Dorfstra&szlig;e 
in Ostfriesland abends; Arbeiter in einem Bus zur 
Fr&uuml;hschicht unterwegs ("Emden geht nach USA", 1975/76); 
der Blick aus dem Fenster einer Zechensiedlung im Ruhrgebiet,
sonntagsnachmittags ("Der Nachwelt eine Botschaft", 1979/80); 
der Blick aus einem Ballettstudio auf hochsommerliche New Yorker Stra&szlig;en 
("498, Third Avenue", 1967). Wildenhahn
hatte schon fr&uuml;h den Mut zur Entdeckung der
Langsamkeit, zu Pausen und den Erz&auml;hlduktus 
strukturierenden Leerstellen mit Sensibilit&auml;t 
f&uuml;r das Treffende im scheinbar Peripheren.</p>


<h4>PRAXIS UND THEORIE</h4>

<p>Wildenhahn ist nicht nur ein Praktiker, sondern
im Lauf der Jahre auch ein Theoretiker seines
Metiers geworden. Man hat ihm vor allem nach
der Publikation seines Buches &uuml;ber 
"Synthetischen und Dokumentarischen Film" (1972) 
Dogmatismus, Abneigung gegen experimentelle
Formen und die Propagierung einer Film&auml;sthetik, 
die durch &auml;u&szlig;erste N&auml;he zum Objekt die
Aufl&ouml;sung der Autorenposition anstrebt, vorgeworfen. 
Doch seine Dokumentarfilme waren
&auml;sthetisch immer viel reicher als seine damals
vertretene theoretische Position, waren immer
auch experimentell, ohne freilich das Experimentelle 
besonders zu betonen. Sie verleugneten in der Montage, 
der Organisation des filmischen Materials, keineswegs 
ihren Autorenstandpunkt.</p>


<p>Sein Filmhandwerk lernte Wildenhahn von
1961 bis 1964 als "Realisator" in der Redaktion
des zeitkritischen Magazins "Panorama" beim
NDR. Realisator sein hie&szlig;_ damals: die Stories
der Journalisten in Film.  umzusetzen, Texte zu
bebildern. Das Bild spielte dabei eine 
untergeordnete Rolle. Das war zu einer Zeit, als die
in den USA durch Richard Leacock, Don Allen
Pennebaker und die Gebr&uuml;der Maysles entwickelte 
neue Dokumentarfilmtechnik des "Direct
Cinema" auch in Deutschland bekannt wurde.
Die "Direct Cinema"-Technik bedeutete eine
Revolution f&uuml;r die &Auml;sthetik des Dokumentarfilms. 
Bis dahin hatten die weitgehend unbeweglichen 
35mm-Kameras wie die damaligen M&ouml;glichkeiten 
der Tonaufzeichnungen weder spontane Aufnahmen 
noch synchronen Originalton erlaubt. Man kam deshalb
nicht ohne eine Nachinszenierung aus. Mit den
neuen, ger&auml;uscharmen 16mm-Kameras
und der synchronen Tonaufzeichnung war der
journalistisch aktuelle Zugriff auf die Realit&auml;t,
die direkte Reportage m&ouml;glich geworden. Diese
neuen Kameras und Tonaufnahmeger&auml;te, lichtstarke 
Optik und hochempfindliches Filmmaterial erlaubten es,
mit minimalem technischem und
personellem Aufwand in langen Einstellungen
Abl&auml;ufe aus der Situation heraus durchg&auml;ngig zu
verfolgen. Mit Leacock Pennebaker und Maysles 
machte Wildenhahn 1964 ein Interview 
f&uuml;r die Sendung "Der Filmclub" des
dritten Fernsehprogramms des NDR. <i>"Das war
die zeitlich fixierbare Initialz&uuml;ndung"</i>, erinnert
er sich sp&auml;ter, <i>"ab dann konnte ich die &uuml;blichen
Fernsehdokumentationen nicht mehr herstellen, 
die ich gut drei Jahre lang mit gro&szlig;er Energie 
und Freude gemacht hatte"</i>.</p>


<h4>SUCHE NACH DER CHRONOLOGIE</h4>

<p>Die Beobachtung aus der Situation heraus
pr&auml;gte den Stil der Filme Wildenhahns in den
60er und 70er Jahren. Sein erster langer Film
f&uuml;r die NDR-Fernsehspielabteilung Egon
Monks, in die er 1967 hin&uuml;berwechselte, entstand 
in dreimonatiger Drehzeit mit einer zwei
Mann-Equipe. "In der Fremde" verfolgt den
Bau eines Futtermittel-Silos in der 
norddeutschen Provinz. Der dramaturgische Aufbau 
dieser _Langzeitbeobachtung verdeutlicht ein 
weiteres Arbeitsprinzip Wildenhahns: das Auffinden 
einer Chronologie in der Alltagsbeobachtung 
von Arbeitsvorg&auml;ngen, einer Chronologie,
die sich nicht nur an der Aufnahmezeit 
orientiert, sondern auch an den Beziehungen der
dargestellten Personen verfolgt wird. "In der
Fremde" zeigt die Arbeitsbedingungen der 
Betonbauer, Zimmerleute, Eisenflechter und
Hilfsarbeiter, die rund um die Uhr in zwei
Schichten arbeiten, weit weg von Heim und
Familie.</p>


<p>Die Dramaturgie ergibt sich aus der Situation
vor Ort. Sie wird nicht nachtr&auml;glich am Schneidetisch 
entwickelt. Drehzeit und Zeit der Recherche fallen 
zusammen. Die Montage, die dramaturgische Verdichtung 
der Ereignisse resultiert aus der Situationslogik am 
Drehort, sie findet hier quasi schon in der Kamera statt. 
"In der Fremde" wurde in einem Drehverh&auml;ltnis
von l:15 aufgenommen. Das war das Doppelte
der damals &uuml;blichen Fernsehnorm. Die Hauptabteilung 
Fernsehspiel des NDR verf&uuml;gte seinerzeit &uuml;ber gute 
Sendepl&auml;tze. Sie konnte mehr Geld in ein solches Filmprojekt
investieren als etwa aktuelle Redaktionen. Bis beute ist 
Wildenhahn als festangestellter Dokumentarfilmregisseur 
privilegierter als Auftragsproduzenten, f&uuml;r die l&auml;ngere 
Drehzeiten nur durch finanzielle Selbstausbeutung erm&ouml;glicht 
werden.</p>


<p>Sein zweiteiliger Film "Die Liebe zum Land"
(1973/74), dessen ironischer Titel auf die harten
Lebens- und Arbeitsbedingungen der hier portr&auml;tierten 
Bauern und Landarbeiter anspielt,
hatte bereits eine Vorf&uuml;hrdauer von zweieinhalb 
Stunden. Die Gesamtl&auml;nge des Dokumentarfilmzyklus' 
"Emden geht nach USA" betr&auml;gt f&uuml;nf_ Stunden. An vier 
einst&uuml;ndige Dokumentarteile schlie&szlig;t ein poetisches 
Portr&auml;t der Region Ostfriesland mit dem Titel "Im Norden das
Meer, im Westen der Fluss, im S&uuml;den das Moor,
im Osten Vorurteile" an. Die Dreharbeiten erstreckten 
sich &uuml;ber einen Zeitraum von sieben Monaten.</p>


<p>Der Filmzyklus beginnt mit einem Portr&auml;t des
Ferdinand Dierks, IG Metall Vertrauensk&ouml;rperleiter 
bei VW Emden, des gr&ouml;&szlig;ten Arbeitgebers der Region 
Ostfriesland. Das Portr&auml;t leitet
in die Chronologie der Ereignisse angesichts
der drohenden Produktionsverlagerung des
VW-Werkes Emden in die USA &uuml;ber. Die Vorbereitungen 
zu einer Protestkundgebung gegen
die Pl&auml;ne der Konzernspitze zeigen die 
unterschiedlichen Interessen von Gewerkschaftsbasis 
und der IG-Metall-F&uuml;hrung, die die Pl&auml;ne
des VW-Aufsichtsrats offenbar mittr&auml;gt.</p>


<p>"Emden geht nach USA" ist ein chronologisch
erz&auml;hltes Lehrst&uuml;ck &uuml;ber innergewerkschaftliche 
Machtstrukturen und den Prozess der
Willensbildung "von unten". Kommentar und
Textinserts fassen die Reportageszenen inhaltlich 
zusammen, liefern zus&auml;tzliche Informationen 
&uuml;ber den Aufbau und die Entscheidungsstrukturen 
einer Gewerkschaftsorganisation.
Diese Form wirkt heute streckenweise etwas
rigide, zu sehr vom Impetus des Didaktischen
gepr&auml;gt. Doch das intensive, in jeder Szene 
mitschwingende pers&ouml;nliche Interesse an den 
portr&auml;tierten Arbeitern, die atmosph&auml;rische 
Qualit&auml;t und Dichte der Beobachtung bebt den 
Emden-Filmzyklus &uuml;ber die exemplarische 
Innenansicht eines gewerkschaftlichen 
Willensbildungsprozesses hinaus.</p>


<h4>DER WEG IN DIE GESCHICHTE</h4>


<p>Nach dem Filmzyklus "Emden geht nach USA"
drehte Wildenhahn im Ruhrgebiet. Sein Weg
aus der norddeutschen Provinz in die ausgeblutete 
Metropole der Schwerindustrie und der Arbeiterbewegung 
war auch ein Weg aus der Aktualit&auml;t in die Geschichte, 
ein Weg von der dokumentarischen Chronologie der 
Gegenwart in die historische Spurensuche. In M&uuml;lheim an
der Ruhr lernte er den Arbeiter-Schriftsteller
G&uuml;nther Westerhoff kennen. Mit ihm zusammen 
machte er in den Jahren von 1979 bis 1981
drei Filme: "Der Nachwelt eine Botschaft",
"Bandonion I - Deutsche Tangos", "Bandonion II: Tango im Exil". 
Diese Trilogie ist stilistisch
ganz anders als der Emden-Zyklus: nachdenklicher, 
suchender, offener in der Form.
Sie beginnt mit einem Portr&auml;t des ehemaligen
Zechenschlossers Westerhoff. Er schreibt Gedichte und 
Kurzgeschichten. Gedichte, in denen er seine Erfahrungen, 
das Leben in der Zechensiedlung und am Arbeitsplatz dem 
Vergessen entreissen, wieder hervorholen und weitergeben 
will. Wildenhahn sieht und h&ouml;rt ihm zu,
zeigt seine Gedichte, liest sie vor, zeigt Westerhoff 
bei einer Lesung. Gemeinsam suchen sie
die Orte auf, auf die sich die Texte beziehen. Eine Art 
filmische Co-Lekt&uuml;re also, ein doppeltes
Erinnern. Ein Erinnern das schwer ist. Denn
nur noch Spuren verweisen auf jene Welt, die in
Westerhoffs Texten zu Sprache kommt. Zu
Bildern kommt sie nurmehr indirekt. Die Zeche, 
auf der Westerhoff gearbeitet hat, ist l&auml;ngst
abgerissen. Das gibt dem ersten Teil dieser
Film-Trilogie eine melancholische Grundstimmung. 
Die aber wird aufgehoben durch eine
Entdeckung, die Wildenhahns Portr&auml;t in eine
Recherche ausweitet: Westerhoff spielt Bandonion.</p>


<p>Wildenhahn begibt sich zusammen mit Westerhoff
in "Bandonion I" und "Bandonion II"
auf eine spannende Forschungsreise in die Geschichte 
dieses Musikinstruments, der Leute,
die es gespielt haben und noch spielen, und seiner Musik, 
dem Tango. Was Westerhoff mit
seinen Gedichten tat, greift Wildenhahn in den
beiden Bandonion-Filmen auf: er entrei&szlig;t die
Geschichte einer proletarischen Musikkultur
dem Vergessen, der kulturellen Anonymit&auml;t.
Wildenhahns Entdeckerfreude findet ihren
Ausdruck in der liebevollen Genauigkeit, mit
der er in "Bandonion II" den Komponisten
Mauricio Kagel und den ehemaligen Fliesenleger 
und examinierten Musiklehrer Klaus
Gutjahr, der in &uuml;ber 800 Stunden Arbeit selbst
ein Bandonion gebaut hat, im Gespr&auml;ch miteinander 
zeigt. Gutjahr erkl&auml;rt Kagel seinen
"Neubau": Man sieht Handwerker unter sich,
die voneinander lernen, Hier scheint eine Utopie
gegl&uuml;ckt, der "kulturelle Graben" zwischen
b&uuml;rgerlicher Avantgarde und Arbeiterkultur
&uuml;berwunden.</p>


<p>Die Besch&auml;ftigung mit dem Bandonion bringt
Wildenhahn ein Jahr sp&auml;ter, im Fr&uuml;hjahr 1982,
auf eine Kunstproduzentin, die ganz &auml;hnlich
wie G&uuml;nther Westerhoff ihre Kunst als &auml;sthetische 
Formulierung von Lebenserfahrung begreift: 
die in Wuppertal arbeitende Choreographin Pina
Bausch und ihr Tanztheater. Ihr
St&uuml;ck mit dem Titel "Bandonion" hat Wildenhahn 
neugierig gemacht, und diese Neugier annonciert 
sein Filmtitel "Was tun Pina Bausch und ihre T&auml;nzer 
in Wuppertal?" Pina Bausch
stellt Fragen an ihr Ensemble. Die T&auml;nzerinnen
und T&auml;nzer bekommen konkrete Aufgaben.
Sie sollen mit Gesten Gef&uuml;hle "erz&auml;hlen". Dieser 
Verarbeitungsprozess von gesellschaftlicher
Erfahrung in Kunstproduktion interessiert Wildenhahn. 
Er betrifft auch seine Arbeit als Dokumentarist ganz 
elementar. Denn er stellt in
diesem Film zwei Bereiche, die nichts miteinander zu 
tun haben, als Ausdruck unserer kulturellen Situation 
nebeneinander: das avantgardistische Tanztheater und die 
invalide Flie&szlig;bandarbeiterin Ruth Gr&uuml;n. Kunst und Arbeitswelt 
bleiben unvers&ouml;hnt. Aber: So wie Ruth
Gr&uuml;n in ihren Erz&auml;hlungen &uuml;ber die Flie&szlig;bandarbeit 
eine eigene Sprache f&uuml;r diese Lebenssituation 
findet, so findet sie Pina Bausch in ihren
Choreographien. Und was Bausch probiert,
probiert ebenso Wildenhahn: die Vermittlung
gesellschaftlicher Erfahrung in &auml;sthetische 
Praxis. Insofern betreibt dieser Film auch eine Art
Selbstreflexion.</p>


<p>Man kann dar&uuml;ber spekulieren, ob die Selbstreflexion, 
die Suche nach Vorbildern und
die Selbstvergewisserung dokumentarischen
Handwerks f&uuml;r Wildenhahn in_ genau dem Moment
wichtig wurde, als er feststellen musste,
dass er mit seinen bisher gedrehten Filmen &uuml;ber
die Arbeiterbewegung an einem Endpunkt 
angelangt war: den der st&auml;ndigen Reproduktion
ihres Untergangs. Seine zwei "Yorkshire"-Filme
&uuml;ber den englischen Arbeiterstreik 1984
und seine letzten beiden Ruhrgebietsfilme
"Stillegung" (&uuml;ber das Thyssen-Stahlwerk in
Oberhausen) und "Rheinhausen Herbst 1988"
vertrauen sich nicht mehr  einzelnen Protagonisten an. 
Sie ersetzen das Prinzip der Chronologie, der Identifikation mit 
den Protagonisten durch die mehrdimensionale subjektive Ann&auml;herung 
von aussen.</p>


<h4>HINWENDUNG ZU DEN PIONIEREN</h4>


<p>Die Selbstreflexion als Gespr&auml;ch &uuml;ber das
Handwerk und die Erinnerung an die Geschichte des 
Dokumentarfilms ist explizites Thema
im 1983 gedrehten "Film f&uuml;r Bossak und Leacock". 
Diese Reisereportage formuliert im Portr&auml;t dieser 
beiden Dokumentaristen seiner
"V&auml;ter", wie Wildenhahn sie nennt, so etwas
wie sein k&uuml;nstlerisches Credo. Er beschreibt
darin die Lehren, die er f&uuml;r sich selbst aus der
Arbeit des Polen Jerzy Bossak und des Amerikaners 
Richard Leacock gezogen hat.</p>


<p>Jerzy Bossak, den Pionier des polnischen Dokumentarfilms,
Mentor einer ganzen Generation polnischer Dokumentaristen, beobachtet
Wildenhahn w&auml;hrend der Oberhausener Kurzfilmtage im Gespr&auml;ch mit dem 
polnischen Experimentalfilm-Regisseur Rybczinsky. Es gebt
um die Dialektik von Form und Inhalt beim filmischen Handwerk. 
Bossak sagt zu Rybczinksy: <i>"Du musst nachdenken, den Inhalt finden,
dann werden wir &uuml;ber die Form sprechen.
Wenn Du nichts zu sagen hast, wirst Du nie
einen Film schaffen."</i> Dann Leacock, im Gespr&auml;ch mit 
Filmstudenten in seiner K&uuml;che, in Boston/Massachusetts:  
<i>"Entscheidend  ist, wollt ihr gestalten oder wollt ihr was 
entdecken?"</i> Und zu Wildenhahn: <i>"Fast alle meine
Studenten wollen letztendlich gestalten, sicher
sein, was passiert. Das ist langweilig."</i>
</p>


<p>Jerzy Bossak und Richard Leacock haben in
ihrem filmischen Handwerk eine individuelle
dokumentarische Haltung entwickelt. Das ist
bei Leacock die gleicherma&szlig;en technisch wie
journalistisch inspirierte Neugier des Entdeckers. 
F&uuml;r ihn gleicht dokumentarische Filmarbeit einer 
Entdeckungsreise: <i>"Wir waren wie
Diebe; abends f&uuml;hrten wir uns vor, was wir gestohlen hatten."</i>
So res&uuml;miert Leacock seine Art der leidenschaftlichen 
Wirklichkeitsbeobachtung bei den Dreharbeiten zu "Primary".
Einer Beobachtung, die dann in der Montage
zur spannenden Chronik gesellschaftlicher und
politischer Ereignisse verdichtet wurde.</p>


<h4>HISTORIKER UND ENTDECKER</h4>


<p>Bossaks Autorenhaltung bestimmt sich aus der
Moralit&auml;t des Historikers, der die filmisch dokumentierte 
Gegenwart (f&uuml;r ihn als Dokumentaristen waren das die letzten 
Kriegs- und ersten Nachkriegsjahre im zerst&ouml;rten Polen) dem 
kollektiven Ged&auml;chtnis der Gesellschaft aufbewahren m&ouml;chte. 
Eines Historikers freilich, den der Geschichtsprozess, 
retrospektivisch betrachtet, stets aufs Neue zum Pessimisten 
verurteilt. Er gleicht dem melancholischen "Engel der Geschichte", 
den Walter Benjamin in der neunten
These seiner Abhandlung "&Uuml;ber den Begriff der Geschichte" beschrieb. 
Jenem "Angelus Novus" des Bildes von Paul Klee, der in die Vergangenheit 
blickt und den ein Sturmwind - nach Benjamin "das was wir Fortschritt nennen" - 
aus dem Paradies in die Zukunft treibt, ohne dass er ihr je
ansichtig wird.</p>


<p>Beide Arbeitsweisen, die des Historikers wie
die des Entdeckers, der gerne auf Reisen geht,
hat Wildenhahn f&uuml;r sich n&uuml;tzlich machen k&ouml;nnen. 
Ein Pessimist ist er bis heute aber nicht
geworden.</p>


<p>Gegenw&auml;rtig arbeitet er an einem Film &uuml;ber ein 
Team von Bauarbeitern und Bauingenieuren, die in
Dresden historische Bauten restaurieren. Der
bisher volkseigene Betrieb wird derzeit unter
Beteiligung einer M&uuml;nchner Firma in eine Kapitalgesellschaft umgewandelt. 
Man darf gespannt sein auf Wildenhahns Blick in die
deutsch-deutsche Geschichte aus der Perspektive einer derzeit so 
spannenden Gegenwart. Auf dem Leipziger Dokumentarfilmfestival soll
der Film uraufgef&uuml;hrt werden.</p>


<p>Klaus Gronenborn</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
