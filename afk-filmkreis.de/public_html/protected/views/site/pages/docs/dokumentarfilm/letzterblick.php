<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.littlebird','docs.dokumentarfilm.robertkramer')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-letzterblick"></a>
<h3>Ein letzter Blick in die Runde<br>
<small>(was wir vorher vergessen haben...)</small>
</h3>


<p>Dieser Aufsatz sollte gezeigt haben, dass die Gemeinsamkeiten zwischen Dokumentar-
und Spielfilm erheblicher sind, als man auf den ersten Blick annehmen k&ouml;nnte.
Als Beweis k&ouml;nnen die Filmemacher gelten, denen eine Idee oder ein Thema wichtiger ist als
die Form. Schoedsack und der Dschungel, Bu&ntilde;uel und die Moral.</p>


<p>Weitere Beispiele dr&auml;ngen sich auf. Walt Disneys zuckerwattes&uuml;&szlig;er Stil blieb nicht auf seine
Zeichentrickfilme beschr&auml;nkt. Die Tiere in seinen Lehrfilmen (am bekanntesten wohl THE
LIVING DESERT (1953); THE VANISHING PRAIRIE, Oscar f&uuml;r den besten
Dokumentarfilm 1954) werden ebenso wie seine zahllosen Trickfiguren durch Musik und
Montage mit menschlichen Charaktereigenschaften belegt. Wahlweise denke man auch an
seinen MEIN FREUND, DAS ATOM.</p>


<p>Ernsthaftere Absichten verfolgte W.S.van Dyke, der bereits weiter oben erw&auml;hnt wurde.
Bekannt wurde er durch zahlreiche Spielfilme wie TARZAN THE APE MAN (1932, mit
Johnny Weissm&uuml;ller), THE THIN MAN (1934), SAN FRANCISCO (1936, featuring Clark
Gable, Spencer Tracy und unglaubliche Spezialeffekte) und IT'S A WONDERFUL WORLD
(1939). Weniger bekannt ist, dass er auch als Kameramann und Regisseur zu den wichtigsten
Dokumentarfilmern geh&ouml;rt. Sein Anliegen ist dabei ein soziales und kritisches. In THE CITY
(1939) besch&auml;ftigt er sich mit den Problemen der Gro&szlig;st&auml;dte. VLLEY TOWN (194O)
beschreibt die Situation von Arbeitern in einer amerikanischen Kleinstadt w&auml;hrend der
Depression. Auch in seinen Spielfilmen sind diese Elemente, wenn auch meist nur
unterschwellig, vertreten (vgl. THE THIN MAN).</p>


<p>Der Designer und Filmarchitekt Ralph McQuarrie arbeitete zun&auml;chst f&uuml;r die Boeing-Company
und die NASA, wo er Lehr- und Werbefilme &uuml;ber bemannte Raumfl&uuml;ge und
Planetenerkundung drehte. Mitte der siebziger Jahre wurde der junge Filmemacher George
Lucas auf ihn aufmerksam und verpflichtete ihn f&uuml;r sein Science-Fiction Projekt STAR WARS
(1977). McQuarrie wurde der erste feste Mitarbeiter von Lucasfilm und k&uuml;nstlerischer Leiter
der Visual-Effects Abteilung Industrial Light and Magic. Einige der innovativsten Fahrzeuge,
Raumschiffe und Roboter in der Geschichte des phantastischen Films stammen von ihm. Aus
seiner Zeit bei der NASA brachte er die Erfahrung mit, dass High-Tech, sobald sie einmal im
Gebrauch ist, nicht lange hochgl&auml;nzend und neu aussehen kann. Deshalb sehen seine
Raumschiffe in STAR WARS leicht verstaubt und abgenutzt aus. Damit setzte er den optischen
Standard f&uuml;r die meisten nachfolgenden Weltraumabenteuer.</p>


<p>Diese &Uuml;bersicht kann nat&uuml;rlich nicht vollst&auml;ndig sein, viele wichtige Namen mussten
ausgelassen werden: 
Ren&eacute; Clair (THE EIFFEL TOWER, 1927; SOUS LES TAITS DE PARIS, 1930), 
Jean Epstein (FINIS TERRAE, 194l; LA CHUTE DE LA MAISON USHER, 1928), 
Robert Siodmak (MENSCHEN AM SONNTAG, 1929; THE SPIRAL STAIRCASE, 1945), 
Leni Riefenstahl (OLYMPIA, 1938; DAS BLAUE LICHT, 1932),
Georges Franju (LE METRO, 1934; LE GRAND MELIES, 1952; LES SANG DES BETES, 1949; LES YEUX SANS VISAGE, 1959), 
Jean Renoir (LA GRANDE ILLUSION, 1937; A SALUTE TO FRANCE, 1944), 
Josef von Sternberg (THE TOWN,1944; DER BLAUE ENGEL, 1930), 
Carol Reed (THE TRUE GLORY, 1945; THE THIRD MAN, 1949),
Jonathan Demme (STOP MAKING SENSE, 1985; THE SILENCE OF THE LAMBS, 1991), 
Michael Apted (BRING ON THE NIGHT, 1985; GORKI PARK, 1982), 
Phil Janou (U2-RATTLE AND HUM, 1988; 3 O'CLOCK HIGH, 1989), 
Martha Coolidge_(DAVID OFF AND ON, 1973; AN OLD-FASHIONED WOMAN, 1974; RAMBLING ROSE, 1990), 
Alain Resnais (NUIT ET BROUILLARD, 1955; LE CHANT DU STYR&Egrave;NE, 1958; LOIN DU VIET-NAM, 1967; HIROSHIMA - MON AMOUR, 1959), 
Louis Malle (CALCUTTA, 1969; AND THE PURSUIT OF HAPPINESS, 1986; GOD'S COUNTRY, 1985; ATLANTIC CITY, USA, 1980), 
Jean-Luc Godard (LOIN DU VIET-NAM, 1967; A BOUT DE SOUFFLE, 1960), 
Bertrand Tavemier (LA GUERRE SANS NOM, 1991; ROUND MIDNIGHT, 1986), 
Michelangelo Antonioni (NETTEZZA URBANA, 1948; BLOW-UP, 1966), 
Michael Moore (ROGER &amp; ME, 1988; Arbeiten f&uuml;r Steven Spielbergs TV-Serie AMAZING STORIES), 
Jim McBride (DAVID HOLZMAN'S DIARY, 1968; THE BIG EASY, 1986), 
Wim Wenders (TOKYO-GA, 1985; AUFZEICHNUNGEN ZU KLEIDERN UND ST&Auml;DTEN, 1988; CHAMBRE 666 - N'IMPORTE QUAND..., 1983; PARIS, TEXAS, 1985), 
John Korty (LANGUAGE OF FACES, 1961; AN INTERVIEW WITH BRUCE GORDON, 1964; OLIVER'S STORY, 1978), 
Pierre Schoendorffer (ANDERSON PLATOON, 1967; A FACE OF WAR, 1968; DIEN BIEN PUH, 1991),
Harold Becker (BLIND GARY DAVIS, 1963; SEA OF LOVE, 1989), 
Brian de Palma (RESPONSIBLE EYE, 1966; DIONYSUS IN 69, 1969; CARRIE, 1976, SCARFACE, 1983), 
William Friedkin (12 Dokumentationen f&uuml;r das NBC-Bildungsprogramm; FRENCH CONNECTION, 1971; THE EXORCIST, 1973), 
et cetera, et cetera...
</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
