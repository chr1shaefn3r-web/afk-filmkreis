<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.dokuessay','docs.dokumentarfilm.bitomsky')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-drama"></a>
<h3>Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?</h3>

<h4>Die Akteure:</h4>

<p>SERGEJ M. EISENSTEIN, 95 Jahre, einer der
bedeutendsten Spielfilmregisseure aller Zeiten.
Tr&auml;ger des Titels <i>verdienter Arbeiter der Kunst</i>
(1935) und des <i>Stalin-Preises 1.Klasse</i>. Aus
seiner Theaterarbeit hatte E. eine experimentelle, 
mit vielen Zirkuselementen versehene
(also "zirzensische") Inszenierung entwickelt.
1923 entwarf er seine Theorie der <i>Montage
der Attraktionen</i>. Unter ATTRAKTIONEN_ verstand E. 
<i>"...jedes aggressive theatralische Element, 
jedes Element, das die Gedanken und die Psyche des Zuschauers 
beeinflusst"</i>; mit derartigen "mathematisch berechneten" 
Attraktionen wollte er seine Zuschauer wachr&uuml;tteln
und sie auf den Weg der Erkenntnis f&uuml;hren.
Typische Merkmale seiner Filme aus den 20er
Jahren sind das Fehlen von "b&uuml;rgerlichen"
Helden; daf&uuml;r erscheint die Masse als neuer
Held auf der Leinwand. Sein erster Langfilm STREIK (1925) 
enth&auml;lt trotz seines ernsten Themas - blutige Niederschlagung 
eines Streiks in einer vorrevolution&auml;ren
Fabrik - noch viele skurrile, sogar humorvolle Elemente; 
so werden die verschiedenen Polizeispitzel mit Tieren verglichen 
und in absonderlichen Verkleidungen vorgef&uuml;hrt, die
man sogleich durchschaut. F&uuml;r ihre Provokationen bedient sich die 
Polizei einer Armee der Unterwelt, die in Tonnen haust, 
akrobatische Kunstst&uuml;ckchen vollf&uuml;hrt und geradewegs 
der DREIGROSCHENOPER entsprungen scheint. Weitere Filme E. sind 
PANZERKREUZER POTEMKIN mit der ber&uuml;hmten
Treppenszene in Odessa, OKTOBER, ALEXANDER NEWSKIJ und 
IWAH DER SCHRECKLICHE.</p>


<p>DZIGA  WERTOW, 97 Jahre, begr&uuml;ndete
1923 die Theorie des Dokumentarfilms mit
seinem Manifest KINOKI.UMSTURZ. <i>Kinoki</i>,
w&ouml;rtlich Filmaugen, nannte sich die Herstellergruppe 
der von W. geleiteten staatlichen
Wochenschau, doch verstand sich diese Gruppe
als Vorhut einer Massenbewegung, durch die
sich das Filmwesen entsprechend den Erfordernissen 
der proletarischen Revolution neu
organisieren w&uuml;rde. Weitere Lieblingsbegriffe
dieser Gruppe sind Kinoglas = Filmauge
und Kinoprawda = Kinowahrheit, gleichzeitig
auch der Titel von Wertows dokumentarischer
Wochenschau ab 1922. Aus der Arbeit der
Wochenschauen und der Agit-Z&uuml;ge (Eisenbahnz&uuml;ge, 
die, mit einer Druckerei und kompletten Filmeinrichtungen 
versehen, an die Fronten des B&uuml;rgerkriegs entsandt wurden,
um dort unter den Truppen revolution&auml;re
Aufkl&auml;rungsarbeit zu leisten und gleichzeitig
Wochenschauszenen zu filmen) entwickelte W.
besonders die <i>formalen</i> Mittel des Dokumentarfilms, 
z.B. Gro&szlig;aufnahmen und die rhythmische Gliederung einzelner 
Sequenzen als Mittel der Aussage. Gleichzeitig wurde er ein
unerbittlicher Verfechter der Auffassung, dass
jegliche fiktive Handlung im Film prinzipiell
sch&auml;dlich und abzuschaffen sei. Neben vielen Dokumentarfilmen 
im Rahmen der Kinoprawda wurden vor allem zwei Langfilme
ber&uuml;hmt: DER MANN MIT DER KAMERA (1929) und EIN SECHSTEL DER ERDE (1926).
W. wurde 1935 der bedeutendste sowjetische
Filmorden, der <i>Rote Stern am Band</i>, verliehen.</p>


<table summary="">
	
<speaker name="Eisenstein" id="e"></speaker>
	
<speaker name="Wertow" id="w"></speaker>
	
<speaker name="Gott" id="g"></speaker>
	
<tr>
<td colspan="2"><i>Dziga Wertow und Sergej Eisenstein in der H&ouml;lle, 
wo ihnen in einer Endlosschleife der neue Wenders-Film vorgef&uuml;hrt wird. 
Anl&auml;sslich eines Hafturlaubs treffen sie sich dort zu einem
Spaziergang im Park und streiten sich, wie so oft, &uuml;ber einen alten 
Aufsatz Wertows von 1925, in dem er Eisensteins neuen Film 
STREIK kritisiert</i></td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">F&uuml;r mich ist es ziemlich egal, mit welchen 
Mitteln ein Film arbeitet, ob er ein Schauspielerfilm ist mit 
inszenierten Bildern oder ein Dokumentarfilm. In einem guten Film
geht es um die Wahrheit und nicht um die Wirklichkeit.</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top"><i>(zitiert aus seinen Werken)</i>;
Das Objektiv ist pr&auml;zis, unfehlbar und muss mitten in den Ereignissen, 
den wirklichen Geschehnissen sein: Aufnahmen ausserhalb der Ateliers, 
ohne Berufsschauspieler, Drehb&uuml;cher, Sujets und gebaute Dekorationen. 
In diesem Sinne ist dein STREIK ein Versuch, einige Konstruktionsmethoden 
der KINOPRAWDA und des KINOGLAS dem Spielfilm aufzupfropfen. 
Montageaufbau, Auswahl der aufzunehmenden Einstellungen und Komposition 
der Zwischentitel in diesem Film sind dem KINOKI verwandt. Zutiefst 
fremd ist den KINOKI die schauspielerische Darstellung, auf die sich 
dein Film gr&uuml;ndet. Zutiefst fremd ist ihnen jedes theatralisch-zirzensische 
Moment, jede artifizielle dekadente Hypertrophie...
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top"><i>(unterbricht)</i> 
Ist ja schon gut, ich kenne deine verbitterte Lamentiererei gegen alles, 
was nicht das "Leben so zeigt wie es ist", gegen die "stumme Ekstase" und 
das "Theater f&uuml;r Dummk&ouml;pfe". Aber solltest du nach 7O Jahren nicht so 
langsam gemerkt haben, dass das Objektiv niemals pr&auml;zis und unfehlbar sein 
kann. Blo&szlig;es Abfilmen der Wirklichkeit allein offenbart noch keinesfalls 
die Wahrheit in der Wirklichkeit. Schon in dem Moment, in dem du die 
Kamera auf ein Motiv h&auml;ltst, hast du eine subjektive Auswahl getroffen. 
Gerade du, Genosse Wertow, solltest wissen, dass auch bei sogenannten 
Dokumentarfilmen der Manipulation T&uuml;r und Tor ge&ouml;ffnet sind.
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Nat&uuml;rlich kann sich der Zuschauer nie vor 
gestellten Bildern und anderer Irref&uuml;hrungen sch&uuml;tzen. Aber ich bleibe dabei: 
ein Dokumentarfilm soll zun&auml;chst eine Abbildung der Wirklichkeit sein.
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">Ein Dokumentarfilm kann aber auch sehr subjektiv 
sein, ja sogar Gef&uuml;hle vermitteln. Es gibt sehr pers&ouml;nliche 
Erfahrungsberichte von Filmemachern, mit ganz klar subjektivem
Standpunkt. Und wer w&uuml;rde bei einem Dokumentarfilm &uuml;ber die wunderbaren 
Landschaften auf der Krim nicht geradezu romantisch werden?
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Das ist nat&uuml;rlich richtig - auch mir wollen jedes 
Jahr wieder die Tr&auml;nen der R&uuml;hrung kommen, wenn ich das Volk geschlossen 
an der Erntefront sehe. Aber das sind Empfindungen, die erst im Zuschauer 
ausgel&ouml;st werden. Die Bilder selbst sind gewissermassen noch neutral.
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top"><i>(Schiebt gedankenverloren einen herumstehenden 
Kinderwagen hin und her)</i> Man kann aber leicht seine Aufnahmen so 
montieren, eventuell noch mit einer geeigneten Musik unterlegt, dass schon 
der Film keineswegs mehr neutral, sondern z.B. &auml;u&szlig;erst pathetisch wirkt.
Damit habe ich Erfahrung! Und was unterscheidet dann eigentlich einen 
Dokumentarfilm noch von einem Spielfilm, wenn er sowohl subjektiv als auch 
objektiv ist, die Wirklichkeit abbildet und auch Gef&uuml;hle vermittelt?
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Gerade hier sehe ich noch wichtige Unterschiede: 
ein wesentliches Merkmal jedes Spielfilms ist die 
<i>dramatische Verdichtung</i> der Geschehnisse. Der Dokumentarfilm dagegen 
soll wirkliche Ereignisse aufzeichnen, ohne etwas wegzulassen, und das oft 
auch noch in Echtzeit.
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">Das ist wieder dein Altersstarrsinn. Nur weil 
du dich weigerst, andere als deine eigenen Dokumentarfilme anzusehen, 
ist dir wohl entgangen, dass_jeder gute Dokumentarfilmer sein
Material gliedert und eine Ordnung oder eine Tendenz in seinen Film bringt. 
Das hat schon der gute Flaherty in NANOOK gemerkt, aber auch f&uuml;r WOODSTOCK 
z.B. haben Michael Wadleigh, Martin Scorsese und Thelma Schoonmaker aus 
Tonnen von Originalmaterial einen Film nach ihren Vorstellungen mit ihrem 
Rhythmus gebaut. Nat&uuml;rlich sind auch solche Filme <i>dramatisch verdichet</i>.
Der einzige Unterschied ist der, dass beim Spielfilm die Handlung und die 
Gliederung schon vorher feststehen, wohingegen beim Dokumentarfilm
erst im Nachhinein dramatisiert wird.
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top"><i>(genervt)</i> 
Gut, ich mach's dir noch einfacher: der Spielfilm verwendet Schauspieler, 
der Dokumentarfilm nicht! Wenn ich an deine ganzen Schmachtfilme denke, 
die du mit unf&auml;higen Schauspielern f&uuml;r Stalin gedreht hast - was hat das 
noch mit Wahrheit, was hat das noch mit der Wirklichkeit zu tun?
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">Lass gef&auml;lligst mein Sp&auml;twerk aus dem Spiel! 
Aber wenn du den Einsatz von Schauspielern als Unterscheidungskriterium 
anf&uuml;hrst, beziehst du dich auf reine &Auml;u&szlig;erlichkeiten. Da kannst du ja 
gleich sagen: einen Dokumentarfilm erkennt man an der verwackelten 
Kamera, dem grobk&ouml;rnigen Film, schlechten Ton, ins Bild h&auml;ngenden 
Mikrophonen usw. Das ist nicht mehr besonders glaubw&uuml;rdig, seit in 
zahllosen Spielfilmen mit diesen Klischees munter gespielt 
wird - "pseudodokumentarische" Filme nennt man das wohl. Schau dir
dazu blo&szlig; HUSBANDS AND WIVES von Woody Allen an!
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Ich schaue mir diesen neumodischen Kram nicht an. 
Die formalen Kriterien taugen sicherlich nicht allein dazu, einen 
Dokumentarfilm zu charakterisieren. Aber es bleibt doch f&uuml;r einen 
&uuml;berwiegenden Teil der Dokumentarfilme das Kennzeichen, dass der 
Produktionsprozess des Films nicht kaschiert wird - wenn z.B. der 
Filmemacher bei Interviews die Fragen vor der Kamera stellt. Wichtiger 
noch scheint mir, dass auf Effekte weitgehend verzichtet wird, die die 
Zuschauer in die Handlung 'saugen', also etwa technische oder k&uuml;nstlerische 
Tricks, Licht- und Toneffekte oder &uuml;berhaupt Filmmusik.
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">Wenn man die Unzahl von mittelm&auml;&szlig;igen 
Interviewfilmen und Reportagen im Fernsehen betrachtet, hast du sicher recht. 
Aber die formalen Kriterien allein treffen eben noch keine klare 
Unterscheidung. Ich kann mich nur wiederholen: ein Spielfilm kann wesentlich 
"dokumentarischer" sein als mancher nach deinen Reinheitsgeboten gedrehter 
Dokumentarfilm. Ein Film ist <i>dann</i> objektiv und dokumentarisch, wenn er 
die <i>Wahrheit</i> zeigt, aber noch lange nicht, wenn er blo&szlig; 
<i>Wirklickeit</i> abbildet. Nicht umsonst finden sich Szenen aus
meinen - inszenierten - Filmen heutzutage in Geschichtsb&uuml;chern...
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top"><i>(wirft ein)</i>...und in zehn Jahren findest 
du Standbilder aus JURASSIC PARK in den Biologieb&uuml;chern...
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top"><i>(l&auml;sst sich nicht aus dem Konzept bringen)</i>
Dokumentarisch ist, was einen realen Sachverhalt ausdr&uuml;ckt. Es kommt nicht 
so sehr auf das Dokumentarische der Objekte und Menschen an - das ist 
sowieso eine Fiktion, der Manipulation sind keine Schranken 
gesetzt - sondern vielmehr auf das Dokumentarische des dargestellten 
Faktums selber, das dann eigentlich in jeder Beziehung rekonstruiert sein 
kann.</td>
</tr>

	
<tr>
<td valign="top">Gott:</td><td valign="top">... H&auml;h?</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top"><i>(l&auml;sst vor Schreck den Kinderwagen los, der eine 
in der Gegend herumstehende Treppe hinunterrollt)</i> 
Um Himmels Willen, hast du mich erschreckt... Also pass auf, ich werde dir
nun ein Gleichnis erz&auml;hlen: hast du den Film RASHOMON von Akira Kurosawa 
gesehen?  ... Wahrscheinlich nicht, schlie&szlig;lich ist der Regisseur ja Heide. 
Jedenfalls geht es in diesem Film um einen Mord, der von vier verschiedenen 
Zeugen auf jeweils verschiedene, jedoch f&uuml;r sich vollkommen schl&uuml;ssige 
und glaubw&uuml;rdige Art berichtet wird. Nur am Rande: einige Jahrzehnte sp&auml;ter 
wird der <i>Dokumentarfilm</i> THE THIN BLUE LINE dasselbe dramaturgische
Mittel verwenden. Stell dir vor, einer dreht nun einen Dokumentarfilm, in 
dem er nur einen Zeugen &uuml;ber den Mord befragt; zweifellos w&uuml;rden damit Dzigas 
Kriterien - authentische Schaupl&auml;tze, am tats&auml;chlichen Geschehen beteiligte 
Menschen - erf&uuml;llt, doch k&auml;me dabei nicht einmal die halbe Wahrheit heraus. 
Mir scheint als Maxime f&uuml;r den Dokumentarfilm die <i>Wahrheit</i> wichtiger 
als die platte Wirklichkeit, auch wenn dabei mit Schauspielern und
im Studio nachgestellten Szenen gearbeitet wird.
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Das geht mir eindeutig zu weit; es gibt einfach den 
Unterschied zwischen 'fiction' und 'nonfiction', wie unsere amerikanischen 
Kollegen sagen w&uuml;rden. Aber vielleicht kommen wir auf einem anderen Weg 
weiter, n&auml;mlich wenn wir danach fragen: "Was will der Regisseur?" und: 
"Was findet das Publikum?".
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">Ja, was will der Regisseur? Manipulation des
Publikums z.B., da sind wir uns sicher einig.</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Bei meinem <i>Roten Stern am Band</i>, das will 
ich meinen!</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top">Gut, Manipulation ist ein Punkt. Ein anderer, der 
f&uuml;r die meisten Dokumentarfilme in der Schule und im Fernsehen von gro&szlig;er 
Bedeutung ist, ist die Vermittlung yon Wissen. Wissen &uuml;ber andere L&auml;nder, 
&uuml;ber Tiere, &uuml;ber Geschichte oder politische Fragen. Viel interessanter 
scheint mir zu sein, dass gerade die guten Kino-Dokumentarfilme mehr wollen, 
als nur Wissen zu vermitteln! Genauso wie in Spielfilmen sollen in guten 
Dokumentarfilmen Gef&uuml;hle geweckt werden, Tr&auml;ume und Assoziationen und 
Ahnungen. Umgekehrt nutzen jedoch auch viele fortschrittliche Filmemacher 
den Spielfilm als Vehikel, um auf unterhaltende Weise Meinungen und Wissen 
zu transportieren.
</td>
</tr>

	
<tr>
<td valign="top">Wertow:</td><td valign="top">Fortschrittlich ist allein der Verzicht auf die 
Filmzauberei. Ich kann es nur immer wieder wiederholen: Nieder mit der 
Verszenung des Alltags! F&uuml;r eine wahrhafte Kinofizierung!
</td>
</tr>

	
<tr>
<td valign="top">Eisenstein:</td><td valign="top"><i>(beruhigt ihn)</i> 
Ja, ja, das Filmdrama ist Opium f&uuml;r das Volk... aber noch einmal zur&uuml;ck
zur Frage, warum denn die Zuschauer in Dokumentarfilme gehen.
</td>
</tr>

	
<tr>
<td valign="top">Gott:</td><td valign="top"><i>(mischt sich ein)</i> 
Jetzt seid ihr schon so lange hier unten und k&ouml;nnt in Ruhe hinter das
Geschehen blicken - und ihr habt immer noch nicht kapiert, dass die 
Menschen eben bescheuert sind! Sie gehen nur noch in Dokumentarfilme, 
wenn sie zuf&auml;llig das Thema interessiert. Fr&uuml;her war es die politische 
und soziale Situation der Massen, heute scheinen es eher Musikgruppen, 
Autorennen oder Reiseziele zu sein...
</td>
</tr>


	
<tr>
<td valign="top">Wertow:</td><td valign="top"><i>(fl&uuml;stert EISENSTEIN zu)</i> 
Der Alte ist wieder mal geladen... wahrscheinlich sind ihm gerade 
seine letzten Sch&auml;fchen davongelaufen... <i>(laut, den Blick nach oben)</i> 
jaja, dass der Dokumentarfilm auch formal brillant sein kann, attraktiv 
wie SERGEJS vielger&uuml;hmten Filme und genausowenig langweilig, das scheint 
sich leider noch nicht herumgesprochen zu haben.
</td>
</tr>

	
<tr>
<td colspan="2"><i>(...Leider werden Eisenstein und Wertow hier abberufen 
zu den Dreharbeiten an dem Passionsfilm im im Fegefeuer, PURGATORY PARK).
</i></td>
</tr>

</table>
   
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
