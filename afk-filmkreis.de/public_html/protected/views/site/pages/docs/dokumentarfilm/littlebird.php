<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.krieg','docs.dokumentarfilm.letzterblick')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-littlebird"></a>
<h3>Fly Little Bird<br>
<small>oder das Aufbegehren der n&auml;chsten Generation</small>
</h3>


<p> R&uuml;ckblickend scheinen f&uuml;r die Regisseure des New Hollywood vor allem zwei
Einfl&uuml;sse stilpr&auml;gend gewesen zu sein: die Roger Corman Factory (in der fast alle
Erfolgsregisseure der siebziger und achtziger Jahre als Drehbuchautoren, Cutter, Ausstatter
oder Kameraassistenten angefangen haben) und der Dokumentarfilm in Verbindung mit
popul&auml;rer Musik. Als bekanntestes Beispiel kann sicherlich Martin Scorsese (TAXI DRIVER,
1975; GOOD FELLAS, 1990) angesehen werden. Kaum ein anderer moderner Filmemacher 
kann so gut mit dem Genre des Dokumentarfilms umgehen und hat diesem so viel f&uuml;r seine
Spielfilme abgewonnen. Scorseses erste Begegnung mit dem Medium war Michael Wadleighs
legend&auml;rer Konzertfilm WOODSTOCK (1969). Wadleigh, der sp&auml;ter selber vom Dokumentar- 
zum Spielfilm gewechselt ist (WOLFEN, 1981), wollte dem Mammutkonzert eine Ikone
schaffen, ein Dokument, das sowohl die Ereignisse auf der B&uuml;hne als auch die Stimmung unter
den G&auml;sten einf&auml;ngt. Deshalb lie&szlig; er zwei Kamerateams gleichzeitig drehen. Heraus kam eine
unglaubliche Menge an Rohmaterial. Die eigentliche Leistung des Films bestand nun darin, das
Material zu ordnen und in einem Rhythmus zu montieren, der die Atmosph&auml;re auf dem Festival
ann&auml;hernd wiedergeben sollte. Der Verdienst f&uuml;r diese Leistung liegt bei Martin Scorsese und
Thelma Schoonmakers, die beide als Cutter angestellt waren. WOODSTOCK erwies sich als
ausgesprochen originell montiert und nimmt einiges an Schnittechniken vorweg, was f&uuml;r viele
sp&auml;tere Scorsese-Filme stilpr&auml;gend wurde; so ist das Bild in WOODSTOCK &uuml;ber weite
Strecken in zwei oder mehr Abschnitte unterteilt, um das Geschehen von mehreren
Standpunkten aus zu zeigen oder unabh&auml;ngige Handlungsstr&auml;nge nebeneinander zu stellen. In
seinem fr&uuml;hen Gangsterfilm MEAN STREETS (1974) greift Scorsese auf diese Technik zur&uuml;ck. 
In anderen Filmen, z.B. GOOD FELLAS (1990) h&auml;lt er das Bild an der
entscheidenden Stelle f&uuml;r einen kurzen Moment an.</p>


<p>Einen eigenen Musik-Dokumentarfilm konnte Scorsese 1976 drehen; THE LAST WALTZ, der
das Abschiedskonzert von Bob Dylans Rockgruppe The Band aufzeichnet. Scorsese wollte das
Konzert mit filmischen Mitteln einfangen, die in ihrer Ehrlichkeit und Geradlinigkeit der Musik
von The Band entsprechen. Schnitt und Mischung gestalteten sich &auml;u&szlig;erst schwierig, so dass
der Film erst nach 18 Monaten in die Kinos kam. Trotz aller Schwierigkeiten und
Verz&ouml;gerungen wurde THE LAST WALTZ als bester Musikfilm seit WOODSTOCK gefeiert.
Der Film wurde auf 35mm-Material gedreht, was f&uuml;r einen Dokumentarfilm damals noch recht
ungew&ouml;hnlich war, ihm jedoch eine starke optische Eindringlichkeit verleiht. Mit Hilfe eines
300 Seiten dicken Drehplans, der alle Texte und Griffwechsel auflistet, choreographiert
Scorsese seine acht Kameram&auml;nner (darunter Laszlo Kovacs, Vilmos Zsigmond und Michael
Chapman). Zum ersten Mal nahm man 24 Tonspuren auf, die dann f&uuml;r den Soundtrack auf vier
Dolby-Stereo-Spuren zusammengespielt wurden.</p>


<p>Im dokumentarischen Bereich hat sich Scorsese eine zweite Karriere aufgebaut. Nach seinem
eigenen STREET SCENES (1970), der Stimmungsbilder vom Stra&szlig;enleben in New York
zeichnet, war er als Cutter und Koproduzent an einigen Dokumentationen beteiligt. Interessant
ist au&szlig;erdem noch Scorseses Portrait seiner Eltern in ITALIANAMERICAN (1974), die
gelegentlich Kurzauftritte in seinen Spielfilmen haben. Wenn Mutter Scorsese Robert de Niro
in GOOD FELLAS begr&uuml;&szlig;t, dann begr&uuml;&szlig;en sich tats&auml;chlich zwei alte Freunde. Detailgetreue
Milieuschilderungen vom Leben der italienischen Einwanderer in Little Italy ziehen sich wie ein
roter Faden durch die Filme von Martin Scorsese.</p>


<p>Ein anderes Beispiel ist der amerikanische Filme- und Mythenmacher Francis Ford
Coppola (THE GODFATHER, 1971; APOCALYPSE NOW, 1979). Coppola hatte Ende der
siebziger Jahre den Traum, eine Gemeinschaft unabh&auml;ngiger Filmemacher jenseits von
Hollywood zu gr&uuml;nden. AMERICAN ZOETROPE nannte er seine kurzlebige
Produktionsgesellschaft, die zun&auml;chst Werbe- und Dokumentarfilme herstellte. George Lucas
(AMERICAN GRAFFITI, 1974; STAR WARS, 1977) geh&ouml;rte zu Coppolas fr&uuml;hesten
Weggef&auml;hrten und Sch&uuml;lern. Seine erste Arbeit f&uuml;r Coppola war FILMMAKER (1968), eine
Dokumentation &uuml;ber die Dreharbeiten zu dessen Road-Movie THE RAIN PEOPLE (1968).
Zuvor hatte Lucas bereits den Western MACKENNA'S GOLD (1967) dokumentiert. Auch
heute ist es noch &uuml;blich, dass junge Filmemacher als Starthilfe mit der Dokumentation von
Filmen bereits erfolgreicher Kollegen beauftragt werden; so durfte die Jungfilmerin Katja von
Garnier (ABGESCHMINKT, 1992) die Dokumentation zur Entstehung von Wolfgang Petersens
Thriller IN THE LINE OF FIRE (1993) drehen.</p>


<p>Auch zahlreiche andere Regisseure arbeiten gerne mit den Mitteln des
Dokumentarfilms. So z.B. der Brite John Schlesinger (MIDNIGHT COWBOY, 1968; THE
FALCON AND THE SNOWMAN, 1985), der selber als Dokumentarfilmer angefangen hat
(TERMINUS, 1960) und in seinen Filmen aus den siebziger Jahren gerne eine n&uuml;chterne,
unterk&uuml;hlte Filmsprache verwendet, die sich mit ihrem distanzierten Blick auf das Geschehen
deutlich an den klassischen Dokumentarfilm anlehnt. Seinen Thriller MARATHON MAN
(1976) er&ouml;ffnet er mit einer Parallelmontage: man sieht den im Central Park von New York
trainierenden Dustin Hoffman beim Laufen. Dazu werden kurze Aufnahmen eines farbigen
Langstreckenl&auml;ufers aus Ken Ichikawas Olympia-Dokumentation TOKYO OLYMPIAD (1964)
montiert. Schlesinger f&uuml;hrt uns also gleich zu Beginn zwei L&auml;ufer vor: einen wirklichen
Sportler und einen Schauspieler. Der Sportler schaut beim Laufen immer wieder direkt in die
ihn begleitende Kamera, deren Pr&auml;senz dem Zuschauer eher unbewusst bleibt, weil sie nur in
einem Ausschnitt (close-up) verharrt. Trotz der Bewegung des L&auml;ufers wirkt die Einstellung
sehr ruhig. Die Kamera im MARATHON MAN verh&auml;lt sich wie ein Begleiter. Sie folgt Dustin
Hoffman und macht seine Bewegungen mit. Hierbei liefert sie Bilder, die hektisch und trotz
steadycam leicht verwackelt wirken. Sie entsprechen also dem, was man sich normalerweise
unter dokumentarischen Aufnahmen vorstellt. Die fiktive Szene wirkt dokumentarischer als die
Szene aus dem eigentlichen Dokumentarfilm.</p>


<p>Diese Kombination gibt den Rhythmus des Films vor und schafft eine eigenartig verhaltene
Spannung, die vor allem in den Suspense- und Action-Szenen zum Tragen kommt.</p>

    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
