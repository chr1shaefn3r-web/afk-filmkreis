<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.drama','docs.dokumentarfilm.vondokuzuspiel')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-bitomsky"></a>
<h3>Spurensuche<br>
<small>Der Filmemacher Hartmut Bitomsky</small>
</h3>


<p>In den Reihen all der ungest&uuml;men (Dokumentar-)Filmer, die momentan das 
Ph&auml;nomen der deutschen Wiedervereinigung in Bildern zu begreifen versuchen,
d&uuml;rfte Hartmut Bitomsky nicht zu finden sein. Bitomsky hat stets der 
Versuchung widerstanden, dem jeweiligen "Ruf der Geschichte" nachzulaufen. 
Sein filmisches Interesse setzt vielmehr da ein, wo sich diese Ereignisse 
zu Geschichten verdichten, die sich in den unterschiedlichsten Archiven 
als Profan-Mythen ablagern. Seit nunmehr 20 Jahren versucht er in seinen 
Filmen beharrlich diese Archive, die seit Beginn dieses Jahrhunderts auch 
Archive des Kinos sind, zu entdecken, zu verstehen, wie sie aufgebaut werden, 
wie sie funktionieren, um dann unter ihren Verzerrungen und F&auml;lschungen 
Spuren einer Wirklichkeit freizulegen, der so einfach nicht beizukommen ist.
</p>


<p>l942 in Bremen geboren, studierte Harmut Bitomsky ab 1962 zun&auml;chst an der 
FU Berlin Germanistik, Theaterwissenschaft und Publizistik und wechselte 
1966 an die neugegr&uuml;ndete "Deutsche Film- und Fernsehakademie". Zwei
Jahre sp&auml;ter, auf dem H&ouml;hepunkt der Studentenrevolte, wurde er zusammen 
mit anderen Kommilitonen wegen politischer Aktivit&auml;ten
relegiert. Seitdem lebt er als Autor und freier Filmemacher in Berlin. 
1970 drehte er zusammen mit seinem Freund und langj&auml;hrigen Weggef&auml;hrten 
Harun Farocki (vgl. "Eine Anstrengung - &uuml;berfl&uuml;ssig?" in <i>"film-dienst"</i> 10/1990, Seite 14) seinen ersten Film f&uuml;r den Westdeutschen Rundfunk: 
"Die Teilung aller Tage", mehr eine gefilmte denn eine filmische 
Einf&uuml;hrung in die Grundbegriffe der marxistischen Theorie, in Anlehnung an 
Bert Brecht mit "Lehrfilm" untertitelt. F&uuml;r denselben Sender
entstanden bis heute 14 weitere Produktionen. Darunter sogenannte 
"medienkritische Filme" wie "Kressin und..." (1973) oder "Kino/Kritik"
(1974), essayistische Portr&auml;ts des englischen Dokumentarfilmers 
Humphrey Jennings (1975, bzw. 1976), eine Studie &uuml;ber John Ford (1976), 
der zweiteilige Spielfilm "Karawane der W&ouml;rter" (1977), die vierteilige 
Dokumentation einer Reise auf dem legend&auml;ren nordamerikanischen 
"Highway 40 West" (1980/81) und schlie&szlig;lich zwei Filme, die den Formen 
und der Funktionsweise  der faschistischen &Auml;sthetik nachsp&uuml;ren - nicht 
im Bereich des Milit&auml;rischen, wo sie nach landl&auml;ufiger Meinung ihre
deutlichste Auspr&auml;gung fand, sondern im vermeintlich "Zivilen", wo sie ihre 
gr&ouml;&szlig;te Effizienz hatte.</p>


<h4>SPUREN IM "KULTURFILM"</h4>


<p>In "Deutschlandbilder" (1983) sch&auml;lt Hartmut Bitomsky aus sogenannten 
"Kulturflmen", die damals zur unverf&auml;nglichen "Erbauung" regelm&auml;&szlig;ig 
im Vorprogramm der Kinovorstellungen liefen, ein in h&ouml;chstem Ma&szlig;e 
ideologisch verf&auml;rbtes Deutschland-Bild heraus, das die Nazis
dem Volk auf subtile Weise als sein eigenes pr&auml;sentierten. Bitomskys 
Fazit: <i>"Die Kulturfilme funktionierten wie ein umgekehrtes Plebiszit:
das Regime best&auml;tigt sein Volk, weil es sich so anstellig zeigt und 
schaffensfroh mitmacht."</i>
</p>


<p>"Reichsautobahn" (1986) verdankt sich urspr&uuml;nglich dem Umstand, 
dass Bitomsky bei der Sichtung jener "Kulturfilme" st&auml;ndig auf Episoden 
stie&szlig;, die sich eigentlich nur als Werbefilme f&uuml;r dieses, damals in Teilen 
fertiggestellte, Gro&szlig;projekt nationalsozialistischer Verkehrsplanung 
verstehen lie&szlig;en.</p>


<p>Wozu braucht eine Autobahn, nahezu der Inbegriff blanker Funktionalit&auml;t, 
"Werbung"? Einmal neugierig geworden, stie&szlig; Bitomsky auf andere 
Ungereimtheiten eines Bauwerkes, dessen Qualit&auml;ten eher im Mythischen, 
denn im Funktionalen lagen. Als Arbeitsbeschaffungsma&szlig;nahme blieb ihre 
Auswirkung auf den Arbeitsmarkt bescheiden. Verkehrspolitisch war
sie unsinnig, da die Nazi-Parole von der "mobilen Gesellschaft" vorerst 
nicht mehr als eine k&uuml;hne Vision war. Mit einem Belag, der selbst f&uuml;r 
LKWs zu d&uuml;nn war, konnte ihr Bau auch nicht unter milit&auml;rstrategischen 
Gesichtspunkten erfolgt sein. Wie Bitomsky mit einer F&uuml;lle von 
Archivmaterial belegt, wurde <i>"die Autobahn... gleich von Anfang an 
zu einem k&uuml;nstlichen Gegenstand erhoben. Sie wurde von Malern gemalt, 
von Photographen photographiert, von Dichtern besungen, von Romanciers 
beschrieben. (...) Die Filme, Gem&auml;lde, Photos, B&uuml;cher und Gedichte hielten 
als ihre Fassade her. Ein potemkinsches Projekt"</i>. Die betreffenden 
"Kulturfilme" waren also nicht Werbefilme f&uuml;r die neue Autobahn, sondern 
nur Teil eines gigantischen Werbefeldzuges mit dem Titel "Reichsautobahn".</p>


<p>Zumindest in thematischer Hinsicht kn&uuml;pft Hartmut Bitomsky in seinem 
bisher letzten Film "Der VW-Komplex" (1989) an seinen Vorg&auml;nger an. Es geht 
erneut um Autos und um Stra&szlig;en. Er begibt sich in jene Produktionsst&auml;tte, 
die von Hitler gegr&uuml;ndet wurde, seine Vision des "mobilen Volkes" zwar 
programmatisch im Namen tr&auml;gt, sie jedoch erst in der Nachkriegszeit zur 
Realit&auml;t werden lassen sollte. Wie die Reichsautobahn nicht darin aufging,
dass auf ihr Autos fuhren, ist "VW" nicht auf einen Ort, eine Stadt zu 
reduzieren, wo Autos hergestellt werden. Bitomsky verbindet Analyse mit 
freier Assoziation, kontrastiert die vollmundige Firmen-Philosophie der 
"Verbindung von Vergangenheit und Zukunft" mit Archivbildern, die das 
offiziell gern ausgesparte einklagen. Aber ebenso irritiert wie fasziniert 
folgt er auch der Stra&szlig;e, auf der heutzutage Autos hergestellt werden, 
bevor sie auf anderen als Schrotthaufen enden k&ouml;nnen: der gespenstische 
Prozess der vollautomatischen Produktion.</p>


<p>In "Reichsautobahn" hatte Bitomsky aus einem zeitgen&ouml;ssischen Buch 
&uuml;ber den Autobahnbau zitiert: <i>"Nirgends verdr&auml;ngt jedoch die 
Maschine die menschliche Arbeit. (...) Und wenn man in dem weitverstreuten 
Gel&auml;nde auch nur wenige Menschen zu erblicken glaubt - sie sind da."</i>
Nicht der Entwicklungsstand damaliger Technologie machte dieses Statement 
erforderlich, sondern die ideologische Parole vom Volk, das emsig seine 
Zukunft gestaltet. Auch im heutigen VW-Werk ist der Arbeiter nat&uuml;rlich noch
nicht "ausgestorben". Aber die Kamera muss ihn suchen und findet ihn als 
Appendix einer gigantischen Maschinerie.</p>


<h4>PRODUKTIONSBEDINGUNGEN DER (KINO-)WIRKLICHKEIT</h4>

<p>Bitomsky hat als Filmemacher im Laufe der Jahre eine eigenst&auml;ndige 
Filmsprache und Methodik entwickelt, in die seine gleichzeitige Arbeit 
als Filmtheoretiker eingeflossen ist. Er gab die Schriften von And&eacute;i Bazin 
und Bel&aacute; Bal&aacute;sz heraus und war langj&auml;hriger Redakteur der 1984 eingestellten 
Zeitschrift "Filmkritik". 1972 erschien sein Buch <i>"Die R&ouml;te des Rots 
von Technicolor. Kinorealit&auml;t und Produktionswirklichkeit."</i>, in dem er 
die marxistische Filmtheorie mit dem Ansatz des Strukturalismus zu verbinden 
versucht. Dort schreibt er: <i>"Eine vornehmlich ge&uuml;bte Form der 
Ideologiekritik an Kunstwerken ist puritanisches Erbe, sie sch&auml;tzt alles
Vergn&uuml;gen gering, und sie kann sich das Vergn&uuml;gen, das ein Publikum mit 
einem Film hat, &uuml;berhaupt nicht erkl&auml;ren: weil sie nur das am Film 
wahrhaben will, was man nicht sehen kann - die Ideologie. Diese 
Ideologiekritik liest den Film wie einen heimlich in den Film 
hineingesteckten Kommentar &uuml;ber die Realit&auml;t; sie liest 
den Film nicht."</i>
</p>


<p>Diese Anstrengung, sich auf die Materialit&auml;t der origin&auml;ren Filmsprache 
(oder auch die "filmische Realit&auml;t") einzulassen, hat Bitomsky seinen 
Zuschauern immer wieder abverlangt. Freilich nicht, um eine jeder 
Wirklichkeit entr&uuml;ckte Welt des sch&ouml;nen Scheins nahezubringen, sondern 
um die Tauglichkeit des Films als eine eigenst&auml;ndige Form der Erkenntnis 
und der Erfahrung zu sichern, die als solche grunds&auml;tzliche 
Konstruktionsprinzipien von "Wirklichkeit" sichtbar machen kann. In dieser
Doppelbewegung ist Bitomsky in seinen Filmen gleicherma&szlig;en am mythischen 
Gehalt des Realen wie am realen Gehalt des Mythischen interessiert. 
Dabei kann seine Arbeit nicht die eines Interpreten sein, der etwas zu 
erkl&auml;ren verm&ouml;chte, sondern nur die eines Dekonstrukteurs, der die 
materiellen Schichten scheinbar evidenter Bedeutungen freilegt. 
(<i>"Die Geschichte gegen den Strich b&uuml;rsten"</i>, hat Walter
Benjamin das einmal genannt.) In seinem einzigen f&uuml;r das Kino gedrehten 
Spielfilm, "Auf Biegen oder Brechen" (1974), hat Bitomsky versucht, 
diesen Grad der Reflexivit&auml;t mit dem subjektiven (Er-)Leben und der 
"Lust am Kino" zu verbinden. Eine geradlinige Story, die trotzdem nicht 
die trivialen Muster fiktionaler Komplexit&auml;tsreduktion wiederholt. Der 
Versuch, diesen Gordischen Knoten mit redlichen Mitteln zu l&ouml;sen, 
fiel nicht sonderlich &uuml;berzeugend aus.</p>


<h4>DIE BEWEGUNG IM STATISCHEN</h4>

<p>Das eigenwilligste Verfahren, das Hartmut Bitomsky in seinen Filmen 
entwickelt hat, scheint fast aus einem Misstrauen gegen&uuml;ber dem eigenen 
Medium, dem Film, zu resultieren. Immer wieder l&ouml;st er Sequenzen in einzelne 
Fotos (nicht Standbilder!) auf, die er dann vor seine Filmkamera h&auml;lt. Das 
"Einfrieren" der Bewegung hat jedoch erstaunlicherweise keine Verflachung, 
sondern eine Intensivierung zur Folge. In "Reichsautobahn" wird zun&auml;chst 
eine Sequenz gezeigt, die einen emsig schaufelnden Adolf Hitler beim ersten 
Spatenstich zeigt. In einzelne Fotos aufgel&ouml;st, verzerrt sich dieses
Gesicht zur grotesken Maske. Zugleich gewinnen Personen im Hintergrund 
des Bildes, die im laufenden Film nur als Staffage auszumachen waren, 
pl&ouml;tzlich Konturen, werden als Beteiligte sichtbar.</p>


<p>Was verbirgt ein Film, wenn er das zeigt, was die Kamera sieht? In 
seinem Essayfilm "Das Kino und der Tod" (1988) beschr&auml;nkt sich Bitomsky
&uuml;ber 45 Minuten auf dieses Verfahren. Zu Beginn fragt man sich irritiert, 
warum er nicht gleich die betreffenden Filmausschnitte pr&auml;sentiert. 
Am Ende hat man manch altbekannten Film nur in ein paar Fotos v&ouml;llig neu 
gesehen. Aber auch ein Vefahren, das nicht immer gleicherma&szlig;en 
effizient ist. Wenn er in "Der VW-Komplex" Fotos durchbl&auml;ttert, auf denen 
ein Fotograf Fahrzeugbauteile zu bizarren Kunstobjekten stilisiert 
hat, will sich erhellende Differenz nicht einschieben.</p>


<h4>DIE STIMME UND DIE DIFFERENZ</h4>

<p>Im Gegensatz zu Harun Farocki, der bei seinen letzten Filmen auf 
jeglichen Kommentar verzichtet hat, spielt die Sprache (seine Sprache) 
in den Filmen von Hartmut Bitomsky ein Rolle, wie sie ihr sonst wohl nur 
noch in den Filmen Alexander Kluges zukommt. Dabei ist die Art, wie er 
mit seiner sonoren Nasalstimme in einer an Monotonie nicht zu 
&uuml;berbietenden Melodik einen schlichten Hauptsatz an den n&auml;chsten
reiht, anfangs schon fast eine Beleidigung f&uuml;r jedes halbwegs geschulte 
Geh&ouml;r. Es braucht seine Zeit, bis man dahinterkommt, dass es eigentlich 
auch gar nicht um das geht, was er sagt. So funktioniert dieser Kommentar 
vielfach am besten da, wo er gar keiner ist, wo die Stimme nicht das 
Sichtbare erkl&auml;rt oder interpretiert, sondern es scheinbar nur verdoppelt.</p>


<p>Wo man bei jedem anderen Regisseur den "m&uuml;ndiger Zuschauer" einklagen 
w&uuml;rde, gelingt es Bitomsky vielfach, eine eigent&uuml;mliche Differenz zwischen 
dem Visuellen und dem Akustischen zu erzeugen, die zu einem wirksamen 
Korrektiv der immer drohenden "Objektivit&auml;t" der Bilder wird. Zugleich aber 
auch immer ein sensibler Balanceakt, der unvermittelt in manieriertes Pathos 
umkippen kann. Wo Bitomsky denselben Sprachduktus einsetzt, um die Bilder 
zu interpretieren, also von der Differenz zur Synthese wechselt, ger&auml;t das 
Ganze auch schon einmal zur Platit&uuml;de. In "Das Kino und der Tod" stellt er 
die Frage, warum der Tod bzw. das Sterben im Film eine derart gro&szlig;e Rolle 
spielt. Die Antwort: <i>"Die Gesellschaft handelt so, damit das Leben 
in ihr als lebenswert erscheint."</i> Nicht, dass die grunds&auml;tzliche 
Richtigkeit dieser These zu bestreiten w&auml;re; es die Art,
wie der Satz gesprochen wird, die hier als filmisches Mittel nicht nicht 
funktioniert, da er die Bilder nicht akzentuiert, sondern von ihnen ablenkt.
</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
