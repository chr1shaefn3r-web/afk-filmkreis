<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.sinn','docs.dokumentarfilm.wildenhahn')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-ueberblick"></a>
<h3>Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms</h3>


<h4>Filmdokumente</h4>


<p>Die Geschichte des Dokumentarfilms beginnt 
im strengen Sinn mit den ersten Filmen &uuml;berhaupt, 
die die Gebr&uuml;der Lumi&egrave;re
1895 in Paris machten. Der &uuml;berwiegende
Teil der fr&uuml;hen Filmaufnahmen besteht aus
Dokumenten allt&auml;glicher Handlungen. Um
diese aufzuzeichnen wurde die Kamera auf
einem Stativ fest montiert und die Szene
wurde in einer einzigen Einstellung abgedreht, 
bis die Filmrolle leer war. Diese Bilder 
verloren schnell den Reiz des neuen und
schon 1896 wurden die Mittel des Schnitts,
der Montage oder der Einstellungswechsel
in ersten Spielfilmen entwickelt. Ab 1908
entstanden dann die ersten dedizierten 
Dokumentarfilme  als  Vorprogramm  zum
Unterhaltungsfilm. In dieser Zeit entstanden
auch  kunstlos  gedrehte  abendf&uuml;llende
Nachrichtenfilme wie z.B. der Bericht &uuml;ber
Scotts S&uuml;dpolexpedition (H. Pontiges).</p>


<h4>Der fr&uuml;he russische Film und seine Auswirkungen</h4>


<p>Der Film wurde in der Sowjetunion schon
fr&uuml;h als Mittel zur Verbreitung von revolution&auml;rem 
Gedankengut gef&ouml;rdert. So diente
der Film w&auml;hrend der Oktoberrevolution
als Chronist und Propagandamittel. Erste
k&uuml;nstlerische Ans&auml;tze bei Dokumentationen
lassen sich bei russischen Filmen finden, die
in  der  Zeit  unmittelbar  nach  der
Oktoberrevolution gedreht wurden. Um
den Mangel an Filmmaterial zu kaschieren,
waren die Chronisten gezwungen, mit 
aufwendigen Montagetechniken und schnellen
Schnittfolgen die reinen Dokumentationen
mit bewusst eingesetzten k&uuml;nstlerischen
Mitteln aufzuwerten. Alle bisherigen 
Gesetze und Konstruktionsgewohnheiten des
Films wurden bewusst verletzt. In den
Jahren nach 1923 wurde erstmals eine
Theorie dokumentarischer Filmpraxis durch
Dsiga  Wertows  Manifest  "Kinooki"
("Filmauge" oder "Kinoglas") formuliert:
<i>"Bis auf den heutigen Tag haben wir die
Kamera vergewaltigt und sie gezwungen,
die Arbeit unseres Auges zu kopieren. [...]
Von heute an werden wir die Kamera 
befreien und werden sie in entgegengesetzter
Richtung, weit entfernt vom Kopieren, 
arbeiten lassen. Alle Schw&auml;chen des 
menschlichen Auges an den Tag bringen! Wir 
treten ein f&uuml;r Kinoglas, das  im Chaos der
Bewegungen die Resultate f&uuml;r die eigene
Bewegung aufsp&uuml;rt, wir treten ein f&uuml;r 
Kinoglas mit seiner Dimension von Zeit und
Raum [...] Befreit von zeitlichen und 
r&auml;umlichen Eingrenzungen, stelle ich beliebige
Punkte des Universums gegen&uuml;ber, unabh&auml;ngig 
davon, wo ich sie aufgenommen habe. 
Dies ist mein Weg zur Schaffung einer 
neuen Wahrnehmung der Welt."</i> <small>(1923)</small>
Hervorzuheben sind hier besonders die
Filme "Ein Sechstel der Erde" (1926)
und "Der  Mann  mit der  Kamera" (1929).</p>


<h4>Die Kamera erschlie&szlig;t die Welt</h4>


<p>Etwa zeitgleich, aber dennoch unabh&auml;ngig
von dem Stil des russischen Dokumentarfilms 
verfilmte Robert Flaherty das Leben
eines Eskimos "Nanook of the North" (1922), 
den er w&auml;hrend zwei Jahren in der
Arktis begleitete. In dem Film werden 
Naturereignisse wie Schneest&uuml;rme, aber auch
Landschaftsaufnahmen erstmals als dramaturgische 
Elemente eingesetzt. Dieser Flaherty geh&ouml;rt zu der 
gro&szlig;en Gruppe der "Abenteurer mit der Kamera", die in dieser
Zeit zu exotischen Zielen ausschw&auml;rmten
um dem heimischen Publikum Bilder von
anderen Kulturen zu pr&auml;sentieren. Der unerwartete 
Kassenerfolg f&uuml;hrte im Anschluss
dazu, dass eine Reihe kommerzieller Filme
im lyrischen Stil &uuml;ber die Lebensweise
"primitiver" St&auml;mme produziert wurde.</p>


<p>Der russische Film hatte zu dieser Zeit 
dennoch starken Einfluss auf die europ&auml;ische
Avantgarde. Mt der konsequenten Ablehnung 
der Werte und Ideen, die Hollywood
repr&auml;sentierte, entstanden Filme wie Walter
Ruttmanns  "Berlin  -  Sinfonie  der
Gro&szlig;stadt" (1927), die durch Montage
der Einzelbilder rhythmische Strukturen mit
neuer &Auml;sthetik hervorbrachten und den
Kunstcharakter des Films konsequent betonten.</p>


<p>In den ersten drei&szlig;ig Jahren der 
Dokumentarfilmgeschichte haben sich somit bereits
die grundlegenden Ans&auml;tze entwickelt, die
sich in den Dokumentarfilmschulen der
darauffolgenden Jahrzehnte in immer neuen
Variationen wiederfinden sollten:
<ul>

<li>Der  aufkl&auml;rerische,  erzieherische
und auch manipulierende Dokumentarfilm
(Propagandafilm), der das Publikum zwar
belehren, ihm vornehmlich jedoch als Mittel
der Identifikation dienen sollte. Nicht 
Abbildung der Wirklichkeit, sondern Schaffung 
eines Weltbildes ist das Ziel.</li>


<li>Der informative, zum Teil allein
durch  die  Themenwahl  spektakul&auml;re
"objektive" Dokumentarfilm. Er ist in dem
Sinn objektiv, dass das Gezeigte von au&szlig;en
betrachtet wird und keine Wertung vom
Filmemacher mitgeliefert wird.</li>

<li>Der betont subjektive, assoziative,
k&uuml;nstlerische Film, der jedoch lange Zeit
aus den gro&szlig;en Kinos verbannt war und als
avantgardistischer Kurzfilm nur von einer
"Elite" zur Kenntnis genommen wurde.</li>

</ul>
</p>


<h4>Aufkl&auml;rung aus England</h4>


<p>Eine weitere wichtige Dokumentarfilmbewegung 
begann 1929 in England mit der Auff&uuml;hrung von 
John Griersons Film "Drifters", der die Fahrt eines 
Fischdampfers schildert, der zum Heringsfang 
ausl&auml;uft. In Folge entstanden &uuml;ber 1OO Filme,
die in England zu einer sozialen Institution
mit der Funktion eines informationspolitischen 
Mediums wurden. F&uuml;r Merson war
der Film zun&auml;chst weder zur Unterhaltung
noch zur Kunst bestimmt, sondern zur Propaganda. 
Nach seiner Auffassung war es ein zeitgem&auml;&szlig;es 
Mittel der Industriegesellschaft, die Staatsb&uuml;rger 
aufzukl&auml;ren und zu erziehen.</p>


<p>In den USA der 20er und 30er Jahre waren
zwei Haupttypen des Dokumentarfilms popul&auml;r: 
Die Reisebeschreibung, die sich
meist auf die klischeehafte Beschreibung
von romantischen St&auml;tten beschr&auml;nkte, und
der kurze "interest"-Film &uuml;ber irgendein
aktuelles Thema.</p>

<p>Im Verlauf der 30er Jahre erlahmte der
russische Dokumentarfilm unter den Auflagen  
des  "sozialistischen Realismus",
w&auml;hrend in anderen L&auml;ndern das sozialistische 
Gedankengut von kleinen Gruppen
avantgardistischer Filmemacher verbreitet
wurde. Als Beispiele stehen Bu&ntilde;uels "Las
Hurdes" (Spanien 1932) und Joris Ivens
"Komsomol" (1932).</p>


<p>Im deutschen Film des Dritten Reichs
spielte der Dokumentarfilm als wesentliches
Instrument  des  staatlichen  Propagandaapparates 
eine &uuml;bergeordnete Rolle. Besonders hervorzuheben 
sind hier die sp&auml;teren Wochenschauen sowie die Filme Leni
Riefenstahls, die sich nicht nur durch die
Inszenierung spektakul&auml;rer Massenszenen
auszeichnete ("Triumph  des  Willens"
1934), sondern auch durch einen brillanten
Schnitt auszeichnen. Der im Gr&ouml;&szlig;enwahn
gedrehte Film "Olympia" (1938), der die
Olympiade von Berlin  "dokumentiert",
wurde aus 800 OOO Metern (480 Stunden)
Filmmaterial zusammengeschnitten und ist
bis heute vor allem wegen der zwiesp&auml;ltig
faszinierenden &Auml;sthetik von den unz&auml;hligen
langweiligen  Olympiadedokumentationen
noch eine der sehenswertesten.</p>


<p>Mit Beginn des Zweiten Weltkriegs entstanden 
in Deutschland die mehr auf Terror
zugeschnittenen Agitationsfilme, die die
fr&uuml;hen Siege der deutschen Armee in frappierender   
Eindringlichkeit   schildern ("Feuertaufe" 194l).</p>


<p>In den alliierten L&auml;ndern brachte der Ausbruch 
des Krieges viele Filmemacher aller
Genres zum Propagandafilm. Als Beispiele
seien hier britische Durchhaltefilme wie
Harry Watts "Target for tonight" (194O)
oder die amerikanische "Why we fight"-Serie, 
bei der prominente Hollywood-Regisseure wie 
John Ford und John Huston unter der Leitung von 
Frank Capra arbeiteten.</p>


<p>In der Nachkriegszeit nahmen Auftragsarbeiten 
f&uuml;r die Industrie zu, bei denen sich
die Filmemacher den Forderungen der
Auftraggeber beugen mussten. Auch die
stereotypen Reisefilme mussten sich den
kommerziellen Interessen der Fremdenverkehrsvereine 
beugen und verloren damit  noch mehr an 
Individualit&auml;t und Reiz.</p>


<p>Familienunterhaltung boten in den 50er
Jahren auch Disneys abendf&uuml;llende Naturfilme ("Die W&uuml;ste lebt"), 
allerdings untergrub das Fernsehen diese Art von 
popul&auml;ren  Dokumentarfilmen, so dass Bekanntheitsgrad 
und Verbreitung schlie&szlig;lich bis weit unter das 
Vorkriegsniveau zur&uuml;ckfiel.</p>


<h4>Das cin&eacute;ma v&eacute;rit&eacute;</h4>


<p>Der zunehmend unpr&auml;zise und dadurch
entwertete Begriff "dokumentarisch" f&uuml;hrte
dazu, dass neuere Gruppen ihn f&uuml;r sich ablehnten. 
Es entstanden neue Bewegungen wie das 
"free cinema" (England), das "direct cinema" (USA) 
oder das "cin&eacute;ma v&eacute;rit&eacute;" (Frankreich).</p>


<p>Das Wort vom "cin&eacute;ma v&eacute;rit&eacute;" fiel zum 
ersten Mal im Zusammenhang mit Jean
Rouchs Interviewfilm "Chronique d'un &eacute;t&eacute;"
(1961). Rouch forderte, dass der Einsatz
des Aufnahmeapparates die Objektivit&auml;t des
tats&auml;chlichen Geschehens, das registriert
werden soll, nicht beeintr&auml;chtigen d&uuml;rfe.
Das technische Problem, auf das das
Verlangen nach Authentizit&auml;t stie&szlig;, war
erst mit der Entwicklung der handlichen 16mm-Kameras 
und der Transistonechnik ann&auml;hernd zufriedenstellend 
l&ouml;sbar. Die Beobachtung mit der Kamera sollte nach
M&ouml;glichkeit so durchgef&uuml;hrt werden, dass
deren Anwesenheit auf die Protagonisten in
ihrer jeweiligen Situation und auf die 
Ereignisse, die in jedem Moment unvorhergesehen 
eintreten konnten, keinen Einfluss
haben sollte. Die Dokumentaristen sollten
demnach reale Vorg&auml;nge sichtbar machen
und ihnen den Augenblick der Wahrheit
entreissen. Der Stil und das Konzept des
cin&eacute;ma v&eacute;rit&eacute; beeinflusste in der Folge auch
die Regisseure der Nouvelle Vague, setzte
sich aber erst mit zahlreichen 
Fernsehausstrahlungen auch beim breiten Publikum
durch.</p>


<p>Auch der wohl popul&auml;rste und bestimmt
kommerziell erfolgreichste Dokumentarfilm
"Woodstock" (Mike Wadleigh, 1970)
enth&auml;lt noch Elemente der V&eacute;rit&eacute;-Fotographie, 
die er allerdings mit einer fl&uuml;ssigen
Schnittfolge verband. In der Folge entstanden 
noch zahlreiche, qualitativ zum Teil
stark umstrittene Konzertfilme, die versuchten,
an den Erfolg anzukn&uuml;pfen.</p>


<p>Das cin&eacute;ma v&eacute;rit&eacute; verliert in den Folgejahren 
dann allerdings an Stellenwert, weil
mehr und mehr erkannt wird, dass der
Wegfall des Kommentars und die neutrale
Position der Filmemacher keine absolute
Objektivit&auml;t garantieren, da die Autoren 
zumindest entscheiden m&uuml;ssen, was in den
Film mit aufgenommen wird und damit
doch unmittelbar eingreifen.</p>


<h4>Die 60er und 70er Jahre</h4>


<p>In den sp&auml;ten 60er und fr&uuml;hen 70er Jahren
wurden dann besonders in den Vereinigen
Staaten politische Dokumentarfilme gedreht, 
bei denen eine neue Form des Journalismus 
von der Objektivit&auml;t abkam und es
zulie&szlig;, dass sich die Autoren konsequent
mit einbringen. Hier seien vor allem "The
Selling of the Pentagon" (Peter Davis, 1971) 
oder die sp&auml;ten Filme von &Eacute;mile de
Antonio genannt. Er konzentrierte sich in
Filmen wie "America is Hard to See"
(1970) auf eindringliche Analysen, die in
collagenhaften Konfrontationen sich selbst
enth&uuml;llende Bilder enthalten. Auch die feministischen 
Filme der fr&uuml;hen 70 er Jahre
sind hier einzuordnen. So zeichnen sich die
Filme von Amalie Rothschild oder Donna
Deitch durch eine typische Mischung von
politischem Engagement,  Selbstreflexion
und pers&ouml;nlicher Aussage aus.</p>


<p>Neben diesen zum gro&szlig;en Teil unabh&auml;ngigen 
Produktionen existierten auch Gruppen
etablierter Dokumentarfilmer, denen das
Fernsehen mit von ihm finanzierten Projekten 
zu einer gewissen Popularit&auml;t verhalf.</p>


<p>In der Bundesrepublik fristete der 
Dokumentarfilm bis in die 60er Jahre hinein eine
recht k&uuml;mmerliche Existenz in den Reservaten des 
Industrie- und Kulturfilms. Erst
mit Peter Nestler und Klaus Wildenhahn erhielt
der deutsche Dokumentarfilm durch
geduldiges Herangehen an die Themen und
eine flexible Gestaltung ohne streng 
vorgefasstes Konzept eine neue Qualit&auml;t.</p>


<p>Auch durchaus provokante Stellungnahmen,  
wie  "Der Kampf um 11%" (Michael Busse, Thomas Mitscherlich und
J&uuml;rgen Peters, 1972), in dem der Verlauf
eines Arbeitskampfes aus der Perspektive
der Beteiligten heraus dargestellt wird, stehen 
f&uuml;r diese Entwicklung.</p>

<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
