<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.sinn','docs.dokumentarfilm.ueberblick')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-sinn"></a>
<h3>Der Sinn des Ganzen</h3>

<p>Durch die &uuml;berm&auml;ssige Flut von Lehrfilmen und uninspirierten Fernsehdokumentationen ist
der Ruf von Dokumentarfilmen ziemlich ramponiert worden. Kaum jemand erwartet von einer
Dokumentation einen unterhaltsamen Kinoabend. Dass genau das Gegenteil der Fall sein kann,
dass Dokumentarfilme nicht nur dokumentieren und informieren, sondern auch unterhalten,
mitreissen und mitf&uuml;hlen lassen k&ouml;nnen, das zeigen die Filme, die in diesem Programm laufen
und die auch in den n&auml;chsten Semestern ihren verdienten Platz auf der Kinoleinwand finden
werden.</p>


<p>In diesem Semester wird die historische Entwicklung des Dokumentarfilms mit ihren 
verschiedenen Auspr&auml;gungen (vom ethnographischen bis hin zum Agitpropfilm) ein klein wenig
angerissen: NANOOK OF THE NORTH von Robert Flaherty (1922), LAS HURDES von Luis
Bu&ntilde;uel (1932) und PAUL JACOBS UND DIE ATOMBANDE von Jack Willis und Saul Landau
(1978). W&auml;hrend man in diesen Filmen - ebenso im umstrittenen STAU - JETZT GEHT'S LOS
von Thomas Heise - in Teilen noch den Dokumentarfilm wiederfindet, wie man ihn zu kennen
glaubt, so wirken VATERS LAND (Peter Krieg), THE THIN BLUE LINE (Errol Morris) und STEP
ACROSS THE BORDER (Nicolas Humbert, Werner Penzel) eher wie Bildercollagen, vielschichtig
und emotional.</p>


<p>In den letzten Jahren hat sich ausserdem gezeigt, dass bei einer zunehmenden Anzahl von
Filmen die Unterscheidung in Dokumentar- oder Spielfilm, die zun&auml;chst so einfach erscheinen
mag, immer schwieriger wird. Auch dieses 'Grenzgebiet' zwischen Spiel- und Dokumentarfilm
soll sowohl in diesem Semester als auch in den kommenden mit herausragenden Beispielen
beleuchtet werden (in diesem Semester: L. 627 von Bertrand Tavernier und ZELIG von Woody
Allen als Spielfilme mit dokumentarisch-realistischem Hintergrund und formalen Stilmitteln
des Dokumentarfilms zur Steigerung der Authentizit&auml;t sowie Errol Morris' THE THIN BLUE
LINE als ein Dokumentarfilm mit inszenierter Wirklichkeit).</p>


<p>Die ganze Bandbreite von Filmen, die mit dem Verdikt 'Dokumentarfilm' belegt werden,
zeigt das kleine Music Special, welches drei Filme vereint, die man zun&auml;chst als 'Musikfilme'
bezeichnen w&uuml;rde.</p>


<p>Musikfilme besch&auml;ftigen sich normalerweise in einer eher unfilmischen und uninspirierten
Art und Weise mit Proben, Konzerten oder Tourneen ber&uuml;hmter Musiker. Wer sich f&uuml;r die
Musik interessiert, gar ein Fan ist, der wird diese meist bunt zusammengew&uuml;rfelten Konzertausschnitte 
goutieren. Wem die Musik nichts sagt, dem wird auch der Film nichts bringen,
aus dem einfachen Grunde weil der Film eigentlich gar keine filmische Ebene besitzt, sondern
lediglich die Musik zu den Zuschauerinnen &uuml;bertragen m&ouml;chte.</p>


<p>Ein guter Dokumentarfilm, ein Musikfilm muss daher mehr sein als eine Anh&auml;ufung von
Bildern &uuml;ber gitarrenzertr&uuml;mmernde Pop-Ikonen und wunderkerzenwinkende Massen. Michael
Wadleighs WOODSTOCK ist von den gezeigten drei Filmen dem dokumentierenden Konzertfilm
noch am n&auml;chsten. Was diesen Film allerdings aus der Konzertfilm-Dutzendware heraushebt,
sind nicht nur sein epochales Sujet und seine epische L&auml;nge, sondern auch und gerade 
Filmisches: ein intelligenter Schnitt, witzig eingesetzte splitscreen-Technik und ein Auge 
f&uuml;r die Kleinigkeiten, die Wesentliches aussagen. Alles in allem ist dieser Film nicht nur Dokument 
eines historischen Konzerts, er vermittelt auch etwas vom Flower-Power-Lebensgef&uuml;hl seiner
Entstehungszeit.</p>


<p>Stilistisch und thematisch schon wesentlich abstrakter und kaum noch als Konzertfilm zu
bezeichnen (man achte z.B. auf die Positionen, die die Kamera einnimmt) ist Jonathan Demmes
STOP MAKING SENSE. Das Konzert der Talking Heads wird in kleine St&uuml;cke geteilt und zu 
kleinen Episoden zusammengesetzt. Keine st&ouml;renden Informationen, keine peinlichen 
backstage-Interviews, keine gr&ouml;lenden Fans, daf&uuml;r aber Musik und Bilder, die eher poetischen Charakter
haben.</p>


<p>Noch ein St&uuml;ck weiter geht Humbert&amp;Penzels STEP ACROSS THE BORDER: In diesem Film
werden kaum noch Bilder aus Konzerten verwendet, sondern neue Bilder zur Musik erfunden.
Man muss die Musik nicht m&ouml;gen, die Musiker nicht kennen und wird trotzdem in den Sog
dieser Bilder geraten, die eher assoziativ die verschiedensten Gedanken oder Gef&uuml;hle wecken.
Eine perfekte Symbiose von Ton und Bild ist dieser Film von einem 'Dokument' weit entfernt
und trifft genau den Nerv der Zuschauerinnen.</p>

    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
