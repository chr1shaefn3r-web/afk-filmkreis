<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.wildenhahn','docs.dokumentarfilm.drama')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-dokuessay"></a>
<h3>Dokumentarfilme - Vom Dokument zum Essay</h3>
      

<p>Am Anfang steht das Dokument. Ein historischer Augenblick, 
eine Alltagsaufnahme, ein Landschaftsbild oder ein Gespr&auml;ch.
Im Gegensatz zu einer Inszenierung im Theater
oder im Spielfilm soll das Dokument ein m&ouml;glichst
authentisches Abbild der Wirklichkeit sein.</p>


<p>Dass diese Authentizit&auml;t einen unerreichbaren
Idealfall darstellt, war den meisten Filmemachern
bereits fr&uuml;hzeitig klar. Die Diskussion dar&uuml;ber, ob
ein Dokument die Wirklichkeit ad&auml;quat abbilden
kann oder gar <i>soll</i>, f&uuml;llt B&auml;nde. Aber es ist unmittelbar 
verst&auml;ndlich, dass z.B. ein Bosnier andere Dokumente von 
Bosnien-Herzegovina aufnehmen und auswerten w&uuml;rde als ein 
Franzose - ganz egal, wie objektiv beide sein m&ouml;chten.</p>


<h4>Dokumente &amp; Dokumentarfilme</h4>


<p>Ebenso leuchtet ein, dass ein Dokument alleine
noch keinen eigenst&auml;ndigen Dokumentarfilm ergibt. 
Aus diesem Grunde z&auml;hlen Dokumente und
ihre nicht inhaltlich motivierte Aneinanderreihung
(Wochenschauen, Nachrichten o.a.) gemeinhin
nicht zu den Dokumentar<i>filmen</i>.</p>


<p>Zu diesen mehr oder weniger sinn- und lieblos 
aneinandergereihten Dokumenten z&auml;hlen auch
viele andere nicht-fiktive Filme: Tierfilme, in denen 
Heinz Sielmann an irgendwelchen Wuscheltieren 
herumfummelt oder die in den sechziger Jahren weitverbreiteten 
Reisefilme (TRAUMSTRASSEN DER WELT, FLYING CLIPPER etc.), die
den wiederaufbauenden Deutschen an exotischere
Schaupl&auml;tze als das Deutsche Eck entf&uuml;hren sollten. 
Obwohl gerade diese klischeehaften Postkartenfilme 
als 'Dokumentarfilme' firmierten, tendiert
ihr Wirklichkeitsbezug gegen Null.</p>


<p>Die neckische postmoderne Variante der Reise-
und Fluchtfilme in eine bessere Welt sind die
diffus-esoterischen Naturfilme &agrave; la KOYANISQATSI
(1984) oder BARAKA (1992), die in elegischen
Dokumenten die urt&uuml;mliche Natur preisen und
uns mit dem Holzhammer suggerieren, dass im
Get&uuml;mmel der Gro&szlig;st&auml;dte doch irgendetwas 
verloren gegangen sein muss.</p>


<p>Ebenfalls nicht-fiktiv aber ebenfalls kein 
Dokumentarfilm im eigentlichen Sinne ist der Lehrfilm. 
Diese Art von schulischem Zeitvertreib hat
wesentlich dazu beigetragen, dass der Dokumentarfilm 
ganz allgemein als belehrend, dr&ouml;ge, statisch
und altmodisch gilt. Interessant ist &uuml;brigens, dass
diese Meisterwerke an Sachlichkeit wie z.B. 
SCHAFSCHEREN IN WESTAUSTRALIEN, DIE SEGNUNGEN DER PETROCHEMIE 
oder DAS BEWEGENDE LEBEN DES MENDELEJEW beinahe 
ausschlie&szlig;lich im Kinder- und Jugendprogramm der 
Fernsehanstalten landen, sei es in der SENDUNG MIT DER
MAUS, sei es im TELEKOLLEG CHEMIE. Dass der
belehrende und informative Aspekt von Dokumentarfilmen 
bei weitem nicht der einzige und vor allem nicht der 
dominierende ist, verliert man beim
&Auml;lterwerden leicht aus den Augen.</p>


<p>Eine finale Steigerung des Lehrfilms stellt dann
der Industriefilm dar, der letztlich nichts anderes 
ist als ein auf Pseudoobjektivit&auml;t getrimmter
Werbefilm. Da so ziemlich jeder in seinem Leben
einem Lehr- oder Industriefilm beiwohnen musste,
aber nur wenige einen 'echten' Dokumentarfilm gesehen 
haben, verwundert es nicht, wenn viele ablehnend reagieren, 
wenn von Dokumentarfilmen die Rede ist.</p>


<h4>Der beschreibende Dokumentarfilm</h4>


<p>Im Gegensatz zum Postkarten-Reisefilm setzt sich
der ethnographische Film das Ziel, das Leben 'fremder' 
Kulturen durch teilnehmende Beobachtung und unter 
Einbeziehen der Betroffenen m&ouml;glichst objektiv zu 
dokumentieren. Dass dies ein frommer Wunsch ist, 
insbesondere wenn ein Europ&auml;er mit seiner Erziehung 
und seinem Hintergrund eine 'fremde Kultur' abbildet, 
soll hier nicht weiter ausgef&uuml;hrt werden.</p>


<p>Aber immerhin, der ethnographische Film ist
ein eigenst&auml;ndiger Dokumentar<i>film</i>, nicht mehr nur
ein Dokument. Diese vielleicht spitzfindig erscheinende 
Unterscheidung wurde bereits 1923 von
Dsiga Wertow in einer Theorie dokumentarischer
Filmpraxis erl&auml;utert:</p>


<p>
<i>"Bis auf den heutigen Tag haben wir die
Kamera vergewaltigt und sie gezwungen, 
die Arbeit unseres Auges zu kopieren. 
Und je besser die Kopie war, desto h&ouml;her 
wurde die Aufnahme bewertet. Von heute an werden wir die
Kamera befreien und werden sie in entgegengesetzter Richtung, 
weit entfernt vom Kopieren, arbeiten lassen. Alle
Schw&auml;chen des menschlichen Auges an den Tag bringen!"</i>
</p>


<p>Die Wirklichkeit wird also nicht - wie im einfachen 
Dokument - durch eine simple Abbildung
dargestellt, sondern zerlegt und analysiert. Das
Auge schaut nicht nur, der Kopf verarbeitet die
Bilder auch. Oder wie es die Theoretiker des britischen 
DOCUMENTARY MOVEMENT formuliert haben:</p>


<p>
<i>"We have to interpret creatively and in
social terms the life of the people as it
exists in reality."</i>
</p>


<p>Die 'Kreativit&auml;t' der FilmemacherIn besteht in
der Verwendung von filmischen Mitteln wie Montage, 
Bildkomposition, Auslassungen, Dramatisierung oder 
Gliederung. Dadurch erh&auml;lt der Film ein
Eigenleben und wirkt &uuml;ber seine Information hinaus 
durch einen dramatischen Zusammenhang auf
die Zuschauer.  Die 'Realit&auml;t' ergibt sich daraus,
dass der beschreibende Dokumentarfilm seinen 'Objekten' 
ein Forum gibt, sie vertritt und zu Wort
kommen l&auml;sst. Diese Art von Dokumentarfilmen,
die immer noch im wesentlichen Dokumente verwendet, 
diese aber inhaltlich und dramaturgisch
anordnet, kann man <b>beschreibende Dokumentarfilme</b> nennen.</p>


<p>Ein Beispiel f&uuml;r diese Art von Filmen w&auml;re
NANOOK OF THE NORTH, einer der ersten beschreibenden Dokumentarfilme 
und der erste ethnographische Film. Regisseur Flaherty hat nicht
wahllos sch&ouml;ne Aufnahmen von Eskimos aneinandergereiht, 
sondern seine Szenen vielmehr dramaturgisch montiert, 
ja den Eskimo Nanook nahezu zum Hauptdarsteller seines 
Filmes gemacht. Die Besch&auml;ftigung mit dem 'Dokument' seines Films
und das Einbeziehen der eigenen und der abgefilmten Personen macht 
aus der Ansammlung von Dokumenten einen Dokumentarfilm. Auf der anderen
Seite macht der dramaturgische Aufbau gerade diesen 
Film - aber auch viele andere beschreibende
Dokumentarfilme - immer mehr zu einem Spielfilm. In einen guten 
beschreibenden Dokumentarfilm werden die ZuschauerInnen genauso ihre 
Hoffnungen, Tr&auml;ume und Gedanken projizieren, wie sie
dies bei einem Spielfilm tun - der ungeheure Erfolg von 
NANOOK bei Presse und Publikum und die
Art und Weise, wie der Film aufgenommen wurde,
belegen dies. Dass der 'Held' des Films in seiner
fotogenen exotischen Wirklichkeit erfroren ist, hat
an dieser Wirkung nichts ge&auml;ndert.</p>


<p>Der beschreibende Dokumentarfilm ist aber keinesfalls 
auf die Vergangenheit beschr&auml;nkt. Noch heute gibt es viele 
und auch gute Dokumentarfilme, die nichts anderes tun, als 
ein bestimmtes Ereignis oder eine bestimmte Entwicklung zu 
dokumentieren.</p>


<p>Der Niedergang der osteurop&auml;ischen Gerontokratien und 
speziell f&uuml;r die BRD die Wiedervereinigung hat eine ganze Reihe 
von Dokumentarfilmen initiiert, welche ohne essay- oder collagenhaft
zu werden (zu diesen Begriffen sp&auml;ter mehr) die
Ereignisse ihrer Zeit b&uuml;ndeln und die Stimmungen
wiedergeben (LEIPZIG IM HERBST oder DRESDEN, OKTOBER 1989 u.v.a.).</p>


<p>Im Gegensatz zu Frankreich und auch Westdeutschland hat der eher 
assoziativ-k&uuml;nstlerische Essay in der DDR, sprich bei der DEFA, keine
Tradition gehabt. Insofern verwundert es nicht,
wenn der geradlinige schn&ouml;rkellose deskriptive Dokumentarfilm 
auch heute noch von vielen Filmemachern im Osten Deutschlands bevorzugt wird.</p>


<h4>Der Agitpropfilm</h4>


<p>Am Anfang dieses Textes wurde dargelegt, dass ein
Dokumentarfilm die 'Realit&auml;t' kaum objektiv abbilden kann, 
sondern sie letztlich kreativ und in ihren sozialen und 
kulturellen Zusammenh&auml;ngen intepretiert.</p>


<p>Diese Erkenntnis wurde sehr bald in die Forderung umgesetzt, 
dass ein Dokumentarfilm, ja &uuml;berhaupt jeder Film, eine Politisierung 
des gesellschaftlichen Bewusstseins zum Ziel haben sollte. 
In den zwanziger Jahren lehnten viele Filmemacher den
damals inhaltlich und formal an den Bed&uuml;rfnissen der b&uuml;rgerlichen 
Hochkultur orientierten Spielfilm ab.  Die Aufgaben des Mediums Film seien
weder Kunst noch Unterhaltung, sondern <i>Propaganda</i>. Auf den Dokumentarfilm bezogen: 
Ein Film ist nicht nur Information und Beschreibung,
sondern auch die <i>Erzeugung einer neuen Wirklichkeit</i>.</p>


<p>Wenngleich viele dieser Illusionen heute nur noch
im Poesiealbum der Weltrevolution stehen und sich
die proletarischen Massen eher f&uuml;r Sport&uuml;bertragungen 
als f&uuml;r ihre soziale Wirklichkeit interessieren, der <b>Agitprop</b>- oder 
Propagandafilm ist ein fester Bestandteil der Filmkultur vieler L&auml;nder
geworden. Und dies nicht (nur) im negativ-manipulativen Sinne wie es das Wort 
'Propaganda' suggeriert, sondern eher im Sinn einer politisch motivierten 
(Gegen-)Aufkl&auml;rung.</p>


<p>Vom beschreibenden Dokumentarfilm unterscheidet sich der Agitpropfilm 
durch seine eindeutige Absicht, die Zuschauer aufzukl&auml;ren oder vom
eigenen Standpunkt zu &uuml;berzeugen. Diese Absicht
ist von Anfang an zu erkennen und ein guter Agitpropfilm 
ist damit allemal ehrlicher als ein seri&ouml;s
verpackter, vorgeblich objektiver Dokumentarfilm,
der seinen Standpunkt verleugnet.  Wesentliches
Stilmittel des Propagandafilms - und gutes Unterscheidungskriterium 
gegen den beobachtenden Dokumentarfilm - ist die pointierte Montage der 
Dokumente (analog zur 'Montage der Attraktionen' im Spielfilm), die z.B. darin 
bestehen kann, dass zwei vollkommen gegens&auml;tzliche Dokumente so 
gegeneinander gestellt werden, dass die Aussage des Films 
offensichtlich wird. Der gesprochene Kommentar in Agitpropfilmen 
stellt meist eine deutlich gef&auml;rbte Meinungs&auml;usserung dar (eine Analogie
w&auml;re die Unterscheidung Nachricht - Kommentar,
die Zeitungsredaktionen zu treffen k&ouml;nnen glauben).</p>


<p>Vom Essayfilm hingegen unterscheidet sich der
Agitproptilm dadurch, dass er immer noch mit 'konventionellen Dokumenten' 
arbeitet (Nachrichtenbilder, Interviews, historische Dokumente, 
Alltagsszenen etc.) und diese nicht assoziativ verschl&uuml;sselt
montiert, sondern einer klar verst&auml;ndlichen, inhaltlich motivierten 
Dramaturgie folgt.</p>


<p>Der Film PAUL JACOBSs UND DIE ATOMBANDE
von Saul Landau z.B. ist in dem Sinne ein Propagandafilm, dass er 
Gegenpropaganda ist gegen eine angeblich neutrale und staatstragende 
'Wahrheit', die von (staatlichen) Medien und verschiedenen 
Interessengruppen beeinflusst, wenn nicht sogar definiert wird.  
Gerade die Problematik der Nutzung der Kernspaltung, die auch in 
diesem Film das Thema liefert, ist ein Paradebeispiel f&uuml;r 
permanent verf&auml;lschte Staats- und Industriepropaganda,
die zu einem ganzen Wust an Filmen gef&uuml;hrt hat,
in denen nett argumentierend oder deutlich polemisierend 
Gegenpropaganda - oder je nach Standpunkt: (Gegen)Aufkl&auml;rung - betrieben wird.</p>


<p>Im Zuge der vielf&auml;ltigen B&uuml;rgerrechts- und
Emanzipationsbewegungen sind schliesslich in den
letzten zwanzig Jahren gerade auch in Deutschland
eine Unmenge von Agitpropfilmen entstanden, die
sich direkt an die entsprechende 'Bewegung' richten, zentrale Ereignisse 
dokumentieren und zu Aktionen, Protest o.a. aufrufen. Diese 
Bewegungsfilme, teilweise faszinierende Zeitdokumente mit
politischem Anspruch, teilweise alternative Familienvideos mit 
Betroffenheitsappell, gab es zu jeder besseren Sitzblockade, jeder R&auml;umung von 
Jugendzentren und zu vielen Bauplatzbesetzungen. Diese
Filme haben in summa &uuml;brigens eine ganze Menge
Menschen erreicht und den Verleih sowie die Produktion von 
Dokumentarfilmen kurzfristig boomen
lassen. Offenbar waren diese Filme aber in ihrer
Mehrzahl nicht gut genug, um dauerhaft ein gr&ouml;sseres Publikum 
halten zu k&ouml;nnen. Auch der politisch
aufgekl&auml;rte Zeitgenosse scheut offenbar das Risiko
'Dokumentarfilm' und h&auml;ngt seinen Gedanken lieber in 
neuseel&auml;ndischen Klavierfilmen nach.</p>


<p>Auch OKTOBER von S. Eisenstein, der monumentale Film 
zur gleichnamigen Revolution, gilt
vielen heute als Propagandafilm - obwohl er
mit Schauspielern teilweise in Kulissen inszeniert
wurde. Letztlich ist es ja auch egal, ob Matrosen den 
Winterpalast st&uuml;rmen oder Schauspieler
in Matrosenkost&uuml;men, das Ziel des Filmes bleibt
das Gleiche: Preisen der Notwendigkeit und der
Errungenschaften der russischen Revolution. Dieser 
Spielfilm wollte authentische Bilder liefern, Geschichte 
nachstellen, und noch heute werden die
Bilder aus diesem Film in vielen ernsthaften Publikationen 
zur Geschichte der Oktoberrevolution
(unwissentlich) als 'Dokumente' verwendet.</p>


<p>Ein Propagandafilm im Wortsinne dagegen ist
DER EWIGE JUDE. Gerade dieser Film wurde
jedoch im Dritten Reich als 'authentische Dokumentation' 
&uuml;ber das Judentum angek&uuml;ndigt. Dies sollte bei den durch 
permanente Propaganda orientierungslos gewordenen Zuschauern besondere
Seriosit&auml;t suggerieren.  Einmal abgesehen davon,
dass viele der angeblichen 'Dokumente' gestellt waren, 
ist der Film durch Auswahl und Zusammenstellung dieser Bilder nichts 
anderes als eine propagandistische Bebilderung der deutschen 
Rassenideologie.</p>


<h4>Der Essayfilm</h4>


<p>Es gibt eine grosse Anzahl von Dokumentarfilmen, die
eine Fortentwicklung des rein abbildenden Dokuments und der 
analysierenden Beschreibung darstellen. Diese Filme wollen 
nicht die Wirklichkeit abbilden, sondern Bilder im Kopf der Zuschauerin
entstehen lassen. Bilder wecken Gef&uuml;hle und Assoziationen, 
die mit dem abgefilmten Sujet unmittelbar nichts mehr zu tun haben m&uuml;ssen.</p>


<p>Ein <b>Essayfilm</b> hat zwar meistens ein Thema
und verwendet manchmal 'konventionelle' Dokumente (Filmausschnitte, 
Gespr&auml;chsfetzen, Landschaftsaufnahmen, Fotografien o.a.), 
aber er hat nicht mehr den themenorientierten Duktus eines
Faktenfilms oder einer Beschreibung.</p>


<p>Ein Essayfilm ist subjektiv und m&ouml;chte Gedanken und Gef&uuml;hle 
visualisieren. Das Dokument wird dabei den Gedanken der Autorin untergeordnet.
Viele formale Merkmale, die viele Dokumentarfilme
auf den ersten Blick erkennbar machen, fehlen oder
fallen nicht ins Gewicht: statische, realistisch wirkende Aufnahmen, 
ordnender Kommentar, lineare Abfolge zusammenh&auml;ngender Dokumente. 
Charakteristisch f&uuml;r den Essayfilm ist neben der eher assoziativen 
Verarbeitung von Thema oder Idee ein hohes Mass an &Auml;sthetik und 
Formenbewusstsein.</p>


<p>Als Beispiel mag hier STEP ACROSS THE BORDER von Mumbert/Penzel dienen. Das 'Objekt'
des Films, der britische Musiker Fred Frith, wird
dem Publikum nicht nahegebracht &uuml;ber informative biographische 
Details aus seiner Kindergartenzeit, sondern &uuml;ber seine Musik und seine 
K&ouml;rpersprache.  Die Musik wiederum wird nicht einfach abgespielt, 
sondern bebildert. Collagenhaft reiht der Film lange Kamerafahrten, 
experimentelle Stadtansichten und Statements aneinander
und unterlegt diese Bilder mit einer Musik, die die
vielf&auml;ltigen Eindr&uuml;cke noch weiter verschwimmen
l&auml;sst. Vieles wird stilisiert und in Szene gesetzt
wie in einem Spielfilm. Humbert und Penzel zeigen
nicht die 'Wirklichkeit', sondern ihre Gedanken, die
sie haben, wenn diese Wirklichkeit auf sie wirkt.
Dies macht - wie bereits gesagt - im Prinzip jeder 
Dokumentarfilm, aber im Essayfilm ist dieser
Prozess inhaltlich und formal zu Ende gebracht.</p>


<p>Man kann im Moment den Eindruck gewinnen,
dass der eher linear aufgebaute und wenig verfremdete beschreibende 
Dokumentarfilm etwas aus der Mode gekommen ist und die meisten neueren 
Kino-Dokumentarfilme essayistischen Charakter haben
oder mit den formalen Mitteln des Spielfilms arbeiten. 
Dies bedeutet allerdings nicht, dass der Essayfilm eine Erfindung 
der 'Neuzeit' ist. Schon Dsiga Wertow ordnet seine Bilder einer These 
oder einem Kommentar unter. Im Prinzip handelt es sich
dabei um Essay-Filme, die Tatsache, dass die Dokumente ihren 
eigenst&auml;ndigen Charakter verlieren und nur Versatzst&uuml;cke eines gr&ouml;&szlig;eren Gedankens
sind, hat zur Bezeichnung <b>synthetischer Film</b>
gef&uuml;hrt. Solche Filme kommen zwar ohne Schauspieler und ohne 
gestellte Szenen aus, sind also keine Spielfilme, aber sie sind mindestens genauso
weit von einer Abbildung der Wirklichkeit entfernt
wie diese.</p>


<p>Historische Marksteine dieses synthetischen
Films waren KINOGLAS von Dsiga Wertow (1923)
und THE SPANISH EARTH von Joris Ivens (1937).</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
