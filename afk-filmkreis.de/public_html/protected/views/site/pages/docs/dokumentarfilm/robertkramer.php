<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<big><b>Dokumentarfilm</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.dokumentarfilm.letzterblick','docs.dokumentarfilm.harunfarocki')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-robertkramer"></a>
<h3>Portrait: Robert Kramer in M&uuml;nchen</h3>


<p>Das M&uuml;nchner Dokumentarfilmfestival will
kein Festival der Erstentdeckungen sein. Es
bietet in seinem internationalen Programm
eine interessante Auswahl von Produktionen
des Vorjahrs, gr&ouml;&szlig;tenteils auf anderen Festivals gesichtet. 
Dazu kommt eine Regionalschau (erstaunlich gut besucht) mit neuesten
Produktionen hiesiger "DokumentaristInnen". Ereignis beim diesj&auml;hrigen Festival
waren die vier Filme von Robert Kramer, die
als Hommage ins Programm aufgenommen
waren: MILESTONES (1972-75), SCENES FROM
THE PORTUGUESE CLASS STRUGGLE (1977),
DOC'S KINGDOM (1987), ROUTE ONE/USA (1989). 
Im Berliner "Arsenal" und im Kommunalen Kino Frankfurt hatte es 
kurz vorher vollst&auml;ndige Retrospektiven des Werks von
Robert Kramer gegeben.</p>


<p>Dokumentarfilme sind ein ungeliebtes Genre.
Diese Filme scheinen uns gerade das vorenthalten zu wollen, 
was "der Film", "das Kino" uns versprechen. Truffaut mochte Dokumentarisches nicht. 
Er war der Ansicht, dass man solche Filme nur in kostenlosen Vorf&uuml;hrungen dem Publikum anbieten d&uuml;rfte: 
<i>"... das ist kein Witz, zum Beispiel waren die 
Reaktionen der Zuschauer von CHRONIQUE D'UN &Eacute;T&Eacute; einander diametral entgegengesetzt, je nachdem, ob die 
Zuschauer Eintritt zahlen mussten oder nicht"</i> <small>(aus: Lettre contre le cin&eacute;ma-v&eacute;rit&eacute;)</small>). Die besten Dokumentarfilme
sind aber gerade so beschaffen, dass sie die Genregrenzen transzendieren. Finden und
Erfinden geh&ouml;ren zusammen, bringen Kino-Wahrheit nur dann hervor, wenn sie miteinander 
ins Spiel gebracht werden.</p>


<p>In MILESTONES spielt Robert Kramer mit der Stilisierung &agrave; la cin&eacute;ma-v&eacute;rit&eacute; derart gekonnt,
dass man das durchaus Inszenierte des Films erst allm&auml;hlich, anfangs ungl&auml;ubig, wahrnimmt. 
Der Kritiker der S&uuml;ddeutschen Zeitung, der 1977 &uuml;ber den Film schrieb, hat von
dem Konstruierten, dem Erfundenen des Films nichts bemerkt. Er nannte ihn einen
Film, <i>"in dem nur Zeugnisse und Dokumente aus dem Alltag amerikanischer Minderheiten
gesammelt und kommentarlos einander gegegen&uuml;bergestellt sind (...). Die Gespr&auml;chspartner 
kommen den Filmemachern weit genug entgegen: mit einer Offenheit, die bei uns
exhibitionistisch wirken w&uuml;rde (...)"</i>. Nun
h&auml;tte der Kritiker schon anhand der credits
stutzig werden m&uuml;sen, wo von Rollen und
ihren Darstellern die Rede ist. Dass Kramers
Figuren fiktionalisierte Versionen der Leute
sind, die sie spielen, hat z.B. Vincent Canby
(New  York  Times,  2.11.75)  ausf&uuml;hrlich
beschrieben:  <i>"Grace Paley, Autorin von
Kurzgeschichten, spielt eine Filmemacherin,
die eng mit der Anti-Kriegs-Arbeit verbunden
ist (...). David C. Stone, Produzent des
Films, spielt mit kantigem Witz einen Kneipenbesitzer, 
dessen radikales Engagement mit
der Zeit eingeschlafen ist (...). John Douglas,
der Co-Regisseur des Films, hat eine Hauptrolle als 
blinder T&ouml;pfer, dessen Werkstatt vor&uuml;bergehend 
zum Refugium f&uuml;r alte Freunde
wird, die in ver&auml;nderten Zeiten nach einem
neuen Lebensstil suchen (...)"</i>.</p>


<p>Der Film hat Traumsequenzen, z.B. die Alptraumbilder 
des Vietnamheimkehrers, er hat
offenkundig spielfilmhaft inszenierte Passagen: 
den Einbruch, die Vergewaltigung. Nur
am Schluss gibt es pures cin&eacute;ma-v&eacute;rit&eacute;: wenn
die Geburt eines Kindes in einer kommuneartigen 
Gemeinschaft gezeigt wird. Tats&auml;chlich
ist dieser peinigend lange Schlussteil eine der
wenige Schwachstellen des Films. Man sieht
einerseits ein schlecht gespieltes Geb&auml;rtheater, 
andererseits l&auml;sst der reale Geburtsvorgang
keinen Raum f&uuml;r das Spiel, und so l&auml;uft
streckenweise eine Art medizinischer Lehrfilm ab.</p>


<p>MILESTONES ist ein gro&szlig;er und bewegender
Film von dreieinviertel Stunden L&auml;nge, in
dem Dutzende von Figuren (zumeist Wei&szlig;e
der gehobenen Mittelschicht), die nur teilweise 
direkt miteinander zu tun haben, auf
der Suche nach einem stimmigen Lebensstil
gezeigt werden. Nicht um "Minderheiten"
geht es in diesem Film, sondern um <i>die</i> kleine
radikale Minderheit, die in den 60er Jahren
als Aktivisten zum "Movement" (B&uuml;rgerrechtsbewegung, 
Protest gegen den Vietnamkrieg) geh&ouml;rten und die sich nun in 
den Siebzigern in einzelnen, privateren Suchbewegungen zerfasern.</p>


<p>
<i>"Es schien, als w&auml;ren wir das Zentrum der
Dinge gewesen"</i>, sagt ein junger Mann, der
sich an Demonstrationen in Washington, an
Black Panthers etc. erinnert. Nun geht er mit
einem Freund von damals durch eine Vorstadt&ouml;dnis 
und erz&auml;hlt vom Scheitern oppositioneller 
Gewerkschaftsgruppen in der Automobilfabrik, in der er arbeitet. Ein anderer
Protagonist &uuml;berlegt sich, als Eremit in den
H&ouml;hlen der Hopi-Indianer zu leben. Er sa&szlig;
im Gef&auml;ngnis, weil er Deserteuren zur Flucht
ins Ausland verhalf. Er steht stellvertretend f&uuml;r
eine Gruppe, die es nun nicht mehr gibt.
Andere haben sich aufs Land zur&uuml;ckgezogen,
erproben "nat&uuml;rliche" Lebensformen, frei
von den Zw&auml;ngen einer todess&uuml;chtigen Zivilisation, 
zugleich erleben sie "nat&uuml;rliche Bindungen" als Fesseln. 
Andere machen Sozialarbeit mit Strafgefangenen. Wieder andere
machen  B&uuml;cher  oder  Filme  &uuml;ber  ihr
Engagement.</p>


<p>Der Engagierte, dem es nicht gelingt, sein
Engagement zu professionalisieren (seinen
Lebensunterhalt damit zu verdienen), wird
Fundamentalist. Dabei sind Professionalismus 
und Fundamentalismus Schrumpfformen
des Engagements. Das Engagement aber ist
selbst schon eine Schrumpfform der Existenz, insofern dabei 
Leidenschaften auf Interessen reduziert werden. So hat Peter 
Sloterdijk die Verlaufsform von "Bewegungen"
geschrieben. Spannend an den Suchbewegungen der Figuren in MILESTONES ist nun, dass
sie meist als Scheitern, Flucht, Katastrophe
erlebt werden, zugleich aber - von au&szlig;en
gesehen - der Versuch sind, gerade vom
Reduktionismus des Engagements freizukommen, ohne dessen Impulse zu verraten. Wie
kann ein stimmiges Leben gegen die t&ouml;dliche
Zivilisation gelingen? Wie kann man Selbstfindung, 
Sinnfindung, Geldfindung harmonisieren?</p>


<p>Ein Hippie-P&auml;rchen verabschiedet sich von
der Landkommune. Mit zwei Kindern und
einem klapprigen Auto geht man auf Achse.
Eine Panne. Die Kinder stopfen Schnee in den
Benzintank. Die Frau muss sich &uuml;ber den
Motor beugen, in der Rastst&auml;tte gibt's nur
Junkfood, in der Garage fragt sie nach einem
Job. Der Mann will nicht mit ihr &uuml;ber ihr
Gef&uuml;hlswirrwarr reden. Sie f&uuml;hlt sich einer
Maschinerie ausgeliefert, der sie entkommen
wollte. Die Grundstimmung des Films ist
zuerst Scheitern, Flucht, Katastrophe. Die
Geburt am Schluss wird oft als Hoffnungssymbol 
interpretiert. Das ist nicht falsch, aber
bezeichnend ist, dass der Film sich hier auf
jenen Aspekt der Geburt konzentriert, der
Schmerz, H&ouml;llenfahrt durch den Geburtskanal 
hei&szlig;t. Dass der Film die Zeitstimmung
genauestens trifft, steht au&szlig;er Frage.</p>


<p>In einem Interview der Cahiers du Cin&eacute;ma
(Dezember 78) hat man Robert Kramer darauf 
hingewiesen, dass Straub/Huillet MILESTONES 
gar nicht m&ouml;gen und dem Film u.a.
vorwerfen, dass es der Film eines verw&ouml;hnten
amerikanischen Kindes sei. Kramer antwortet
ausweichend: da sei was dran, aber dieser
Aspekt werde im Film selbst bedacht und 
<i>"...ich verstehe die Filme von Straub nicht recht.
Ich habe MOSES UND ARON in Portugal gesehen, 
es war sehr eigenartig, ihn unter diesen
Umst&auml;nden zu sehen"</i>.</p>


<p>SCENES FROM THE PORTUGUESE CLASS STRUGGLE hei&szlig;t der Film, den Kramer 1977
(zusammen mit Philip Spinelli) in Portugal
gedreht hat. MILESSTONES war ein offenes
Geflecht  von  tagebuchartig  registrierten
Lebenswegen Einzelner, eine F&uuml;lle pr&auml;zis
geformter Gespr&auml;chssituationen, eine Reflexion 
&uuml;ber den allt&auml;glichen Lebensstil, dieser
Film nun erscheint als das krasse Gegenteil;
ein High-Speed-Agitprop-Streifen, in dem die
Ereignisse der portugiesischen Revolution
durchgeh&auml;ckselt werden. Die Bilder flitzen
vor&uuml;ber wie die Briefe in der Verteilermaschinerie 
auf dem Hauptpostamt, und der fast
unabl&auml;ssige Kommentar verteilt die Bilder (in
denen niemand &uuml;ber knappe Statements hinaus zu Wort kommt) in die K&auml;stchen und
stempelt sie ab: hier spricht eine Speerspitze
des Imperialismus, hier ein Vertreter des Volkes. 
Der Kommentar sortiert nach Kategorien
einer Klassenanalyse auf der Linie der Partei
Otelo de Cavalhos. Das Volk und seine Spontaneit&auml;t 
werden zum strahlenden Mythos, die
Revolution wird zum Volksfest, das B&ouml;sewichter st&ouml;ren wollen, und die 
Kommentarstimmen definieren, was die wahre Stimme
des Volkes ist. Manche Analysen m&ouml;gen richtig 
sein, manche Hintergrundinformationen
neu und aufschlussreich, insgesamt aber donnern 
die Ereignisse vor&uuml;ber wie ein Sturzbach. Die Filmemacher m&ouml;gen sich 
vorgekommen sein wie Fische im Wasser, aber sie
kanalisieren nur Str&ouml;me von Bildern, die von
sich her nichts sagen d&uuml;rfen. Merkw&uuml;rdigerweise 
vermittelt auch dieser Film eine panische, katastrophische Grundstimmung. 
Vom Tempo und der Begrifflichkeit des Kommentars 
abgesehen, hat der Film die konventionelle Machart eines TV-Beitrags.</p>


<p>Zehn Jahre sp&auml;ter drehte Kramer noch einen
Film in Portugal: DOC'S KINGDOM. Ein amerikanischer 
Arzt (gespielt von dem Journalisten Paul McIsaac, der im n&auml;chsten Film der
Reisebegleiter auf der <i>Route One</i> sein wird)
dreht sich in Selbstgespr&auml;chen um sich selbst.
Nicht nur die Massen sind verschwunden,
auch in der n&auml;chsten Umgebung des Doc ist
es leer. Im Hospital, wo er seinen Dienst tut,
mit R&ouml;ntgenbildern hantiert, gibt es nur mit
einem Patienten eher unverbindliche Wortwechsel. 
Auch in der Kneipe nur ein knappes
Miteinanderreden mit dem Wirt. Docs K&ouml;nigreich hei&szlig;t Einsamkeit, Isolation. Nur zwei
Ereignisse st&ouml;ren ihn darin - vor&uuml;bergehend - auf. Das Fenster wird ihm zertr&uuml;mmert
und es werden Parolen auf den Boden vor seinem Haus gemalt. Die Bedrohung bleibt
undefiniert, erh&auml;lt kein Gesicht. Vielleicht sind es ehemalige Kombattanten, die den
Gringo forthaben wollen. Zuerst wappnet er sich gegen die Unbekannten, sp&auml;ter wirft er
seine Pistole ins Meer. Dann aber erh&auml;lt er Besuch aus Amerika: der erwachsene Sohn
(eines der herangewachsenen Kinder aus MILESTONES?) will wissen, wie sein Vater
aussieht und was er treibt. Es kommt zu einem gro&szlig;en, 
sch&ouml;n in der Schwebe gehaltenen Gespr&auml;ch zwischen beiden, das kl&auml;rt,
warum ihre Leben getrennt verlaufen sind
und weiter verlaufen werden.</p>


<p>Kramer zeichnet das Bild dieser Einsamkeit
in einer ruhigen, eleganten Inszenierung. Ein
Spielfilm, in den ein Dokumentarfilm &uuml;ber
Einsamkeitsgesten integriert ist. Dramaturgisch ist das Gravitationszentrum 
das Vater-Sohn-Gespr&auml;ch, eine Art Showdown. Doc ist
ein Schiffbr&uuml;chiger, ein Gestrandeter. Unterwegs nach dem Nirgendwoland Utopia, ist er
im Niemandsland am Rande Lissabons gelandet. Was sich im Film untergr&uuml;ndig, 
andeutungsweise sp&uuml;rbar macht, ist im Zusammenhang der anderen Filme Kramers deutlich.
Der legend&auml;re Gr&uuml;nder Lissabons ist Odysseus. F&uuml;r Doc aber gibt es keine Heimkehr,
nur eine R&uuml;ckkehr in die USA im n&auml;chsten
Film, um den Stand der Dinge dort zu durchleuchten. Einsamkeit, Resignation, Verzweiflung..., 
aber in diesem Film passiert etwas
Merkw&uuml;rdiges: die katastrophische Grundstimmung ist verschwunden. Vielleicht liegt
das an der spezifisch portugiesischen Form
der Melancholie, <i>Saudade</i> genannt, die nicht
nur Trauer und Verzweiflung ist, sondern
auch - Reinhold Schneider hat das so gesagt - Sch&ouml;nheit, sogar Erl&ouml;sung, 
<i>"... saudade, deren Wesen ihre Unbegrenztheit und Undefiniertheit, 
die in Melodie sich l&ouml;sende uranf&auml;ngliche irdische Unerf&uuml;llbarkeit der Kreatur ist"</i>.</p>


<p>Eine Steigerung dieser Sch&ouml;nheit und Erl&ouml;stheit, mit der sich Kramer anscheinend in 
Portugal infiziert hat, findet sich in ROUTE ONE/USA, dem Film, der an L&auml;nge (vier
Stunden), Bedeutung und Kraft das Gegenst&uuml;ck, Weiterf&uuml;hrung der Fragen und Echo zu
MILESTONES ist. (Siehe auch epd Film 6190, S. 15).  Am deutlichsten wird das Nichtkatastrophische 
dieses Films in manchen Gespr&auml;chen, wenn sich Augenblicke einverst&auml;ndigen Stillschweigens ergeben, die sind
wie: Ein Engel geht durchs Zimmer. So im Gespr&auml;ch mit der alten Indianerin, die z&ouml;gert
und schlie&szlig;lich nicht mehr weiter aus ihrer Geschichte erz&auml;hlen will.  Doc bel&auml;sst es
dabei. So auch ein Augenblick stillen Einverst&auml;ndnisses im Weinkeller des portugiesischen 
Kaufmanns. Aber auch in den Begegnungen mit dem "gegnerischen" Amerika,
mit den fanatischen Predigern, Rassisten, militanten Patrioten etc. gibt es eine Ruhe des
Blicks, ein Standhalten des Anblicks, in dem
die Personen geschont sind und gerade so in
ihrer Merkw&uuml;rdigkeit, Verbohrtheit, ihrem
bisweilen gef&auml;hrlichen Wahn scharf gesehen
werden. Kramer ist nicht mehr der nerv&ouml;se
Chronist, eher in der Art eines Bluess&auml;ngers
berichtet er von seinen Begegnungen. Zu
Beginn der Reise zitiert Doc einige Verse
Walt Whitmans aus dem "Gesang von der
freien Stra&szlig;e". Der Klang dieses Gedichts
scheint Rhythmus und Melodie des ganzen
Films zu pr&auml;gen. <i>"Nur der Kern jedes Dings
n&auml;hrt"</i>,  hei&szlig;t  es  bei  Whitman.  Kramer
beschreibt seinen Blick auf die Dinge so:
<i>"Mit der gr&ouml;&szlig;tm&ouml;glichen Konzentration die
Anziehungskr&auml;fte sichtbar machen, die von
jedem betrachteten Ding ausgehen. Das hei&szlig;t
respektvollen Umgang mit dem Vorhandenen
zu finden. Mein Blick sch&uuml;tzt die Personen:
ich werde mich ihrer nicht bedienen, um ein
Argument zu belegen. Sie sind da, weil sie da
sind."</i>
</p>


<p>Diesen Blick gab es auch schon in fr&uuml;heren
Filmen Kramers, aber nicht in dieser vertrauensvollen Intensit&auml;t. In allen Filmen Kramers
gibt es als Hauptimpuls die Suche nach dem intensiven Augenblick. Intensit&auml;t ist aber eine
sehr ambivalente Kategorie. Sie kann einer &Auml;sthetik des Schocks, Alptraums, Erschreckens 
zugeh&ouml;ren, aber auch einer des Geltenlassens. In einem fr&uuml;hen Interview spricht
Kramer einmal davon, er wolle inmitten einer Bombe sein, wenn sie explodiert. Es gibt die
Intensit&auml;t einer Explosion oder die einer Begegnung, in der eine Gestalt sich offenbart.</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Der Sinn des Ganzen', array('site/page', 'view'=>'docs.dokumentarfilm.sinn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein &Uuml;berblick &uuml;ber die Geschichte des Dokumentarfilms', array('site/page', 'view'=>'docs.dokumentarfilm.ueberblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Das Abenteuer Dokumentarfilm: Zum 60. Geburtstag von Klaus Wildenhahn', array('site/page', 'view'=>'docs.dokumentarfilm.wildenhahn'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Dokumentarfilme - Vom Dokument zum Essay', array('site/page', 'view'=>'docs.dokumentarfilm.dokuessay'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Abbild oder Inszenierung der Wirklichkeit - Was ist ein Dokumentarfilm?', array('site/page', 'view'=>'docs.dokumentarfilm.drama'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Spurensuche: Der Filmemacher Hartmut Bitomsky', array('site/page', 'view'=>'docs.dokumentarfilm.bitomsky'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Als die Wirklichkeit laufen lernte...: Filmemacher auf dem Weg vom Dokumentarfilm zum Spielfilm', array('site/page', 'view'=>'docs.dokumentarfilm.vondokuzuspiel'));?></small>
</li>
<li>
<small><?php echo CHtml::link('King Kongs Kinderstube: Dokumentarfilm als Abenteuer', array('site/page', 'view'=>'docs.dokumentarfilm.kingkong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fliegende Pfarrer und Hollywood im Krieg', array('site/page', 'view'=>'docs.dokumentarfilm.krieg'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Fly Little Bird: oder das Aufbegehren der n&auml;chsten Generation', array('site/page', 'view'=>'docs.dokumentarfilm.littlebird'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Ein letzter Blick in die Runde: (was wir vorher vergessen haben...)', array('site/page', 'view'=>'docs.dokumentarfilm.letzterblick'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Portrait: Robert Kramer in M&uuml;nchen', array('site/page', 'view'=>'docs.dokumentarfilm.robertkramer'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Eine Anstrengung - &uuml;berfl&uuml;ssig?: Portr&auml;t des Filmemachers und Publizisten Harun Farocki', array('site/page', 'view'=>'docs.dokumentarfilm.harunfarocki'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Blicke, die nicht richten: Die Filme von Romuald Karmakar', array('site/page', 'view'=>'docs.dokumentarfilm.romualdkarmakar'));?></small>
</li>
</ul>
