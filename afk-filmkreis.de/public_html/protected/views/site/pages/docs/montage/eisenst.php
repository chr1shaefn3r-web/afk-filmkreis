<p align="right">
<small>
<?php echo CHtml::link('&gt;&gt;&gt; Begleitheft \'Die Filme von Mike Leigh\'', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Zwischen den Bildern</b></big>
<br>Aspekte der Filmmontage<br>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.montage.mtv','docs.montage.wong')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-eisenst"></a>
<h3>IA28 - Sergej Eisensteins Weg zur Intellektuellen Montage</h3>
      
<subsection title="1. Einleitung">
        
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/portrait.jpg" align="left" height="345" width="250">Sergej Eisenstein (1898-1948) konzipiert Ende der 20er Jahre sein erstes Buch. Es soll ein "kugelf&ouml;rmiges" Buch werden. Ein Buch &uuml;ber die Methode der Montage, der er bis dahin schon zahlreiche Manifeste gewidmet hatte. Nicht die Abgrenzung zu anderen Theoretikern stand dabei im Vordergrund, sondern die Darstellung eines neues Analysemodells, da&szlig; sich der g&auml;ngigen Bem&uuml;hung der Avantgarde, einer allumfassenden Theorie der Kunst zu begr&uuml;nden, entzog.</p>
        
<p>
          
<i>"Es ist sehr schwer, ein Buch zu schreiben. Weil jedes Buch zweidimensional ist. Ich wollte aber, da&szlig; sich dieses Buch durch eine Eigenschaft auszeichnet, die keinesfalls in die Zweidimensionalit&auml;t eines Druckwerkes pa&szlig;t. Diese Forderung hat zwei Seiten. Die erste besteht darin, da&szlig; das B&uuml;ndel dieser Aufs&auml;tze auf gar keinen Fall nacheinander betrachtet und rezipiert werden soll. Ich w&uuml;nschte, da&szlig; man sie alle zugleich wahrnehmen k&ouml;nne, weil sie schlie&szlig;lich eine Reihe von Sektoren darstellen, die, auf verschiedene Gebiete ausgerichtet, um einen allgemeinen, sie bestimmenden Standpunkt - die Methode - angeordnet sind. Andererseits wollte ich rein r&auml;umlich die M&ouml;glichkeit schaffen, da&szlig; jeder Beitrag unmittelbar mit einem anderen in Beziehung tritt... Solcher Synchronit&auml;t und gegenseitigen Durchdringungen der Aufs&auml;tze k&ouml;nnte ein Buch in Form... einer Kugel Rechnung tragen... Aber leider... werden B&uuml;cher nicht als Kugeln geschrieben... Mir bleibt nur die Hoffnung, da&szlig; dieses unentwegt die Methode wechselseitiger Umkehrbarkeit er&ouml;rternde Buch nach eben derselben Methode gelesen werden wird. In der Erwartung, da&szlig; wir es lernen werden, B&uuml;cher als sich drehende Kugeln zu lesen und zu schreiben. B&uuml;cher, die wie Seifenblasen sind, gibt es auch heute nicht wenige. Besonders &uuml;ber Kunst."</i> <small>(BULGAKOWA, S. 31f)</small>
        
</p>
        
<p>Dies schreibt Eisenstein am 5. August 1929 in sein Tagebuch. Bis dato hat er drei Spielfilme gedreht: STREIK (1924), PANZERKREUZER POTEMKIN (1925), OKTOBER (1927). Ausgehend von seiner Theaterarbeit (in deren Rahmen er bereits einen <i>"Kurzfilm"</i> - GLUMOWS TAGEBUCH, 1923, 120 m - aufgenommen hatte) hat er die Idee der <i>"Montage der Attraktionen"</i> bis zur <i>"Intellektuellen Montage"</i> (vor allem OKTOBER) entwickelt. In diesem Tagebuchausschnitt spiegelt sich einiges von seiner Auffassung vom Filmen wider. In deren Zentrum steht immer die Methode - die Montage. Sie entwickelt sich - dies zeigt auch der Tagebucheintrag - nicht linear. Er spricht von <i>"Synchronit&auml;t"</i>, <i>"gegenseitiger Durchdringung"</i>, der <i>"Beziehung"</i> zwischen den unterschiedlichen Artikeln, die nicht notwendigerweise aufeinader aufbauen m&uuml;ssen, inhaltlich sogar kontr&auml;r sein k&ouml;nnten.</p>
        
<p>Dies erfordert die aktive Teilnahme des Lesers / der Leserin - jedes Leseerlebnis w&auml;re individuell in der Zusammenstellung und dem Wahrnehmungsproze&szlig; - vielschichtig, komplex, aber auch mehrdeutig.</p>
        
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Sergej M. Eisenstein - Kurzbiographie</h3>
<p>1898 in Riga geboren. B&uuml;rgerliches Elternhaus, Vater Architekt. Nach dem ersten Weltkrieg Sch&uuml;ler bei Wsewolod Meyerhold. Arbeitete als Regisseur und B&uuml;hnenbildner am Proletkult-Theater. 1924 entsteht sein erster Film STREIK. 1925 schuf er das epochemachende Filmkunstwerk PANZERKREUZER POTEMKIN. Es folgten OKTOBER (Zehn Tage, die die Welt ersch&uuml;tterten) - 1927 - und DAS ALTE UND DAS NEUE (Die Generallinie) - 1929. Zahlreiche Reisen f&uuml;hrten Eisenstein nach Frankreich, Deutschland, in die Schweiz, die USA und nach Mexiko. Dort 1930-31 Dreharbeiten zu QUE VIVA MEXICO! - wird aus Geldmangel nicht vollendet. Ab 1932 war er Professor an der Filmhochschule in Moskau. Arbeitete 1935-37 an dem Projekt BESHINWIESE - wurde von der <i>"Hauptverwaltung Film"</i> eingestellt. 1938 erschien sein Film ALEXANDER NEWSKI und 1944 IWAN DER SCHRECKLICHE, I. Teil. (Der II. Teil blieb unvollendet.) Eisenstein starb 1948 in Moskau</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
        
<p>Eisenstein spricht auch von einer <i>"Reihe von Sektoren"</i>, die auf <i>"verschiedene Gebiete ausgerichtet"</i> sind - seine Ans&auml;tze gehen meist von filmfremden Disziplinen aus - Psychologie, Lyrik, Psychoanalyse usw. - immer wieder reviediert er alte Ans&auml;tze, nimmt neue Anregungen dazu. Seine Auffassung von Montage ist entsprechend keine geschlossene, sondern eine offene Theorie - immer im Wandel, gepr&auml;gt von einer st&auml;ndigen Suche.</p>
      
</subsection>
      
<subsection title="2. Gegen&uuml;berstellung: die dramatische Montage (Griffith) - die &quot;intellektuelle&quot; Montage (Eisenstein)">
        
<p>Was genau Eisensteins Methode der Montage ausmacht zeigt sich sehr sch&ouml;n in einer Gegen&uuml;berstellung mit einem kontr&auml;ren Ansatz. Der Vergleich mit dem amerikanischen Stummfilmpionier D. W. Griffith bietet sich geradezu an. Eisenstein befa&szlig;te sich ausf&uuml;hrlich mit dessen Auffassung von Montange (und deren Tradition in der Literaturgeschichte - evtl. Anm. 1) in seinem Artikel <i>"Dickens, Griffith und wir"</i> <small>(1941)</small>. Er bewunderte dessen Entdeckungen wie die Parallelmontage (Gegen&uuml;berstellung zweier Handlungsstr&auml;nge), den Wechsel der Kameradistanz (z. B. von Totaler zur Nahaufnahme) und erkannte dessen wesentlichen Einflu&szlig; auf die russischen Filmemacher an. Dennoch grenzt er sich scharf ab: Griffith	 benutzt seine Montage zur Steigerung der Dramatik, des suspense, Eisenstein will mehr:</p>
        
<p>
          
<i>"Ein Kunstwerk, wie wir es verstehen, ist (wenigstens ein den beiden Bereichen, in denen ich arbeite - Theater und Film) vor allem ein Traktor, der die Psyche des Zuschauers im Sinne des angestrebten Klassenstandpunktes umpfl&uuml;gt"</i> <small>(KAUFMANN, S. 55)</small>
        
</p>
        
<p>In BIRTH OF A NATION (Die Geburt einer Nation; 1915) und in OKTOBER (1927) beschreiben beide die Entstehungsgeschichte der eigenen Nation - Griffith <i>"dramatisch"</i>, Eisenstein <i>"intellektuell"</i>. Ein Vergleich wird die Unterschiede verdeutlichen:</p>
        
<subsubsection title="2.1 Griffith: Die dramatische Montage">
          
<p>In dem Ausschnitt wird die Ermordung von Pr&auml;sident Lincoln durch den Attent&auml;ter Booth w&auml;hrend eines Theaterbesuchs gezeigt:</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth01.jpg" align="left" height="130" width="180">
Zwischentitel: Und dann, als die fruchtbaren Tage vor&uuml;ber waren und eine segensreiche Zeit des Friedens bevorstand (...) , nahte die schicksalsvolle Nacht des 14. April 1865.</p>
          
<p>Es folgt eine kurze Szene, in der Benjamin Cameron (Henry B. Walthall) Elsie Stoneman (Lilian Gish) vom Haus der Stonemans abholt und zusammen mit ihr ins Theater geht. Sie nehmen an einer Galavorf&uuml;hrung teil, bei der auch Pr&auml;sident Lincoln anwesend sein wird. Die Vorstellung hat schon begonnen.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth02.jpg" align="left" height="130" width="180"> Zwischentitel: Zeit: 8.30 Uhr. Die Ankunft des Pr&auml;sidenten</p>
          
<p>1	Aufblende. Lincoln und seine Begleiter kommen nacheinander am Ende der Treppe im Theater an und wenden sich in Richtung Pr&auml;sidentenloge. Lincolns Leibw&auml;chter geht zuerst, Lincoln selber als letzter. (7 Sek.)</p>
          
<p>2	Die Pr&auml;sidentenloge vom Zuschauerraum des Theaters aus gesehen. Die Begleiter Lincolns erscheinen in der Loge. (4 Sek.)</p>
          
<p>3	Aufblende. Pr&auml;sident Lincoln vor der T&uuml;r zur Logen, wie er einem Diener seinen Hut &uuml;berreicht. (3 Sek)</p>
          
<p>4	Die Pr&auml;sidentenloge. Wie Einstellung 2: Lincoln erscheint in der Loge. (5 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth03.jpg" align="left" height="130" width="180">
5	Halbnah. Elsie Stoneman und Ben Cameron sitzen im Zuschauerraum. Sie blicken beide zur Pr&auml;sidentenloge hoch, beginnen dann zu klatschen und stehen auf. (7 Sek.)</p>
          
<p>6	Einstellung von hinten auf das Publikum in Richtung B&uuml;hne. Die Pr&auml;sidentenloge befindet sich auf der rechten Seite. Das Publikum mit dem R&uuml;cken zur Kamera steht im Vordergrund, klatscht und begr&uuml;&szlig;t begeistert den Pr&auml;sidenten. (3 Sek.)</p>
          
<p>8	 Wie Einstellung 6. (3 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth04.jpg" align="left" height="130" width="180">
9	 Die Pr&auml;sidentenloge. Wie Einstellung 7: Der Pr&auml;sident verneigt sich und setzt sich dann (5 Sek.)</p>
          
<p>Zwischentitel: Lincolns pers&ouml;nlicher Leibw&auml;chter bezieht seinen Posten vor der Loge.</p>
          
<p>10	Aufblende. Der Leibw&auml;chter erscheint im Gang vor der Loge und setzt sich. Er beginnt, ungeduldig seine Knie zu reiben. (10 Sek.)</p>
          
<p>11	Einstellung vom hinteren Zuschauerraum in Richtung B&uuml;hne. Die Auff&uuml;hrung ist in vollem Gange. (5 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth05.jpg" align="left" height="130" width="180">
12	Die Pr&auml;sidentenloge. Wie Einstellung 9: Lincoln ergreift die Hand seiner Frau. Aufmerksam verfolgt er das Geschehen auf der B&uuml;hne. (9 Sek.)</p>
          
<p>13	 Wie Einstellung 11: Das Klatschen des Publikums verebbt. (4 Sek.)</p>
          
<p>14	Eine n&auml;here Einstellung der B&uuml;hne. Das Spiel der Schauspieler. (10 Sek.)</p>
          
<p> Zwischentitel: Um etwas von der Auff&uuml;hrung mitzubekommen, verl&auml;&szlig;t der Leibw&auml;chter seinen Posten.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth06.jpg" align="left" height="130" width="180">
15	Aufblende: Der Leibw&auml;chter. Wie Einstellung 10: Er ist offensichtlich ungeduldig. (5 Sek.)</p>
          
<p>16	N&auml;here Einstellung der B&uuml;hne. Wie Einstellung 14. (2 Sek.)</p>
          
<p>17	Aufblende. Der Leibw&auml;chter. Wie Einstellung 15: Er steht auf und stellt seinen Stuhl hinter eine Seitent&uuml;r. (6 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth07.jpg" align="left" height="130" width="180">
18	Wie Einstellung 6: Blick auf die Loge neben der von Lincoln. Der Leibw&auml;chter erscheint und nimmt Platz. (3 Sek.)</p>
          
<p>19	In einer Kreisblende sehen wir eine n&auml;here Einstellung der Handlung von Nr. 18. Der Leibw&auml;chter nimmt in der Loge Platz. (5 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth08.jpg" align="left" height="130" width="180">
Zwischentitel: Zeit 10.30 Uhr - 3. Akt, 2. Szene</p>
          
<p>20	Eine Totale des Zuschauerraums von hinten; eine diagonale Schlitzblende gibt nur den Blick auf die Pr&auml;sidentenloge frei. (5 Sek.)</p>
          
<p>21	Halbnah. Elsie und Ben. Elsie deutet auf etwas in Richtung Pr&auml;sidentenloge. (6 Sek.)</p>
          
<p>Zwischentitel: John Wilkes Booth (gespielt von Raoul Walsh)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth09.jpg" align="left" height="130" width="180">
22	Kopf und schultern von John Wilkes Booth, durch eine Kreisblende gesehen. (3 Sek.)</p>
          
<p>23	Wie Einstellung 21: Elsie verfolgt wieder voller Aufmerksamkeit das Geschehen auf der B&uuml;hne. (6 Sek.)</p>
          
<p>24	Booth. Wie Einstellung 22. (2,5 Sek.)</p>
          
<p>25	N&auml;here Einstellung auf die Pr&auml;sidentenlogen. (5 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth10.jpg" align="left" height="130" width="180">
26	Booth. Wie Einstellung 22. (4 Sek.)</p>
          
<p>27	N&auml;here Einstellung der B&uuml;hne. Wie Einstellung 14. (4 Sek.)</p>
          
<p>28	N&auml;here Einstellung der Pr&auml;sidentenloge. Wie Einstellung 25. Lincoln l&auml;chelt zustimmend dem Geschehen auf der B&uuml;hne zu. Er bewegt seine Schultern, als ob ihm kalt w&auml;re, und beginnt, seinen Mantel &uuml;berzuziehen. (8 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth11.jpg" align="left" height="130" width="180">
29	Booth. Wie Einstellung 22: Er blickt nach oben, w&auml;hrend er sich vom Sitz erhebt. (4 Sek.)</p>
          
<p>30	Fortsetzung Einstellung 28. Lincoln beendet das &Uuml;berziehen seines Mantels. (6 Sek.)</p>
          
<p>31	Das Theater, von der R&uuml;ckseite des Publikums aus gesehen. Wie Einstellung 20: Die Schlitzmaske blendet aus und zeigt den gesamten Zuschauerraum. (4 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth12.jpg" align="left" height="130" width="180">
32	 Sehr nah. Der Leibw&auml;chter, wie er sich &uuml;ber das St&uuml;ck am&uuml;siert. Wie Einstellung 19 mit einer Kreisblende. (1,5 Sek.)</p>
          
<p>33	Aufblende. Booth. Er geht durch die T&uuml;r am Ende des Ganges vor Lincoln Loge. Er beugt sich vor, um durch das Schl&uuml;sselloch in Lincolns Loge zu blicken. Er zieht den Revolver hervor und atmet tief durch, als wolle er sich f&uuml;r die Tat Mut machen. (14 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth13.jpg" align="left" height="130" width="180">
34	 Sehr nah. Der Revolver. (3 Sek.)</p>
          
<p>35	Fortsetzung von Einstellung 33: Booth versucht, die Loge zu betreten, hat f&uuml;r einen Moment Schwierigkeiten beim &Ouml;ffnen der T&uuml;r, betritt dann die Pr&auml;sidentenloge. (8 Sek.)</p>
          
<p>36	Nah. Blick auf Lincolns Loge. Wie Einstellung 25: Booth taucht hinter Lincoln auf. (5 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth14.jpg" align="left" height="130" width="180">
37	Die B&uuml;hne. Wie Einstellung 14: Die Schauspieler agieren auf der B&uuml;hne. (4 Sek.)</p>
          
<p>38	 Wie Einstellung 36: Booth schie&szlig;t Lincoln in den R&uuml;cken. Lincoln bricht zusammen. Booth klettert seitlich &uuml;ber die Br&uuml;stung der Loge und springt auf die B&uuml;hne. (5 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth15.jpg" align="left" height="130" width="180">
39	Totale. Booth steht auf der B&uuml;hne. Er rei&szlig;t die Arme hoch und ruft:</p>
          
<p>Zwischentitel: Sic Semper Tyrannis</p>
          
<p>
            
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth16.jpg" height="130" width="180">
            <img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth17.jpg" height="130" width="180">
            <br>
            
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth18.jpg" height="130" width="180">
            <img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth19.jpg" height="130" width="180">
          </p>
          
<p>Also, was tut Griffith?</p>
          
<p>Zun&auml;chst betrachtet ist die Handlung der Szene relativ einfach: Lincoln besucht eine Theaterinszenierung, sein Leibw&auml;chter l&auml;&szlig;t sich ablenken, verl&auml;&szlig;t seinen Posten, so kann der Attent&auml;ter bis in die Loge vordringen und Lincoln erschie&szlig;en. Die eigentliche Handlung befindet sich an einem Ort und dreht sich um drei Personen: Lincoln, den Leibw&auml;chter und Booth. Griffith f&uuml;hrt jedoch noch eine zus&auml;tzliche Handlungsgruppe ein: Cameron und Stoneman, die inmitten des Publikums sitzen. Er verzichtet auf eine kontinuierliche Handlungswiedergabe, wie bis dahin &uuml;blich, und l&ouml;st die Szene auf, wechselt zwischen diesen einzelnen Gruppen, ohne da&szlig; dadurch das Geschehen wesentlichen vorangetrieben w&uuml;rde. Trotzdem sind die Wechsel f&uuml;r den Zuschauer verst&auml;ndlich, der Ablauf bleibt kontinuierlich. Die Gesamthandlung wird zerlegt, und durch Montage wieder neu zusammengef&uuml;gt. <br>
Griffith <i>"erkl&auml;rt"</i> den Ablauf, und w&auml;hlt dazu die wesentlichen Momente aus. Durch diese <i>"Anh&auml;ufung einer Serie von wirkungsvollen Details"</i> <small>(REISZ, S. 19)</small> erh&auml;lt er eine wesentlich gr&ouml;&szlig;ere Erz&auml;hldichte. In Einstellung 15 zeigt er den ungeduldig wartenden Leibw&auml;chter, statt mit ihm fortzufahren, pr&auml;sentiert er in Nr. 16 den Grund f&uuml;r seine Ungeduld: die Auff&uuml;hrung. Damit werden der folgende Orts- und Kamerapositionswechsel (von Nr. 17 zu Nr. 18: der Leibw&auml;chter betritt die Loge) eingeleitet.</p>
          
<p>Das Wechseln der Handlungsgruppen bietet Griffith noch weitere M&ouml;glichkeitet. Mit den Figuren von Cameron und Stoneman hat er gewisserma&szlig;en Beobachter des Geschehens eingef&uuml;hrt. Er kann dadurch von quasi Sehenden zu dem Gesehenen (dem Ablauf in Lincolns Loge) wechseln. Er bildet das Geschehen also nicht nur ab, sondern bietet dem Zuschauer zus&auml;tzlich die M&ouml;glichkeit der Identifikation.</p>
          
<p>Um den Gef&uuml;hlsausdruck oder die Dramatik einer Szene zu unterstreichen, &auml;ndert Griffith die Distanz zum Darsteller oder Objekt: Kurz bevor Booth die Loge betritt (Nr. 33) schneidet er auf die Nahaufnahme des Revolvers (Nr. 34), um dessen Absicht noch einmal zu verdeutlichen (unterstrichen durch den "Fokus" der Kreisblende), springt dann wieder zur&uuml;ck in die Totale (Nr. 36).Von der &Uuml;bersicht auf das Detail und zur&uuml;ck.</p>
          
<p>Griffith Auffassung von Montage ist gebunden an ein Seh- und Erz&auml;hlperspektive. Sie gibt der Kamera und deren Position eine v&ouml;llig neue Wichtigkeit. Auch die Bedeutung der Schauspieler &auml;ndert sich: Sie bekommen mehr Gewicht z. B. durch Nahaufnahmen, dies erfordert gleichzeitig eine exaktere Arbeitsweise. Die Inszenierung liegt durch die Montage jetzt v&ouml;llig in den H&auml;nden des Regisseurs. Dabei spielt die Auswahl der Einstellungen, die Reihenfolge, aber vor allem das Timig, das Tempo eine entscheidende Rolle. Die Arbeit des Cutters gewinnt an Bedeutung. </p>
        
</subsubsection>
        
<subsubsection title="2.2 Eisenstein: Die intellektuelle Montage">
          
<p>Im Vergleich dazu ein Ausschnitt aus Sergej Eisensteins OKTOBER (1927), in dem er versucht die Ereignisse, die zur Oktoberrevolution gef&uuml;hrt haben, aber vor allem ihre Bedeutung und ihren ideologischen Hintergrund, nachzuverfolgen. In diesem Ausschnitt wird die Figur des neuen Ministerpr&auml;sidenten der provisorischen Regierung; Kerenskij, portr&auml;tiert, der sich nach der Februarrevolution im Winterpalais des Zaren eingerichtet hat. </p>
          
<p>Seit 1928 spricht Eisenstein in bezug auf OKTOBER von einer <i>"intellektuellen"</i> Montage:</p>
          
<p>
            
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob01.jpg" align="left" height="130" width="180">
            <b>Sergei Eisenstein:  Oktober (1927)<br>
Aktrolle 3:  Charakterisierung von Kerenskij</b>
          
</p>
          
<p>1-17 Das Innere des Winterpalais in St. Petersburg. Kerenskij, Haupt der provisorischen Regierung, geht langsam, begleitet von zwei Leutnants, den riesigen Korridor des Palais entlang. Er steigt die Treppe hoch: dazwischen geschnitten sind Zeitlupenaufnahmen des stolz die Stufen erklimmenden Kerens-kij, sowie einzelne Zwischentitel, die Kerenskijs Titel nennen: Kommandierender General, Kriegs- und Marineminister, etc. pp.</p>
          
<p>18	Nahaufnahme. Eine Girlande in den H&auml;nden einer der Statuen im Palast.</p>
          
<p>19	Halbtotale. Die Statue in voller Gr&ouml;&szlig;e, die Girlande haltend.</p>
          
<p>20	Nahaufnahme. Girlande, wie in Nr. 18.</p>
          
<p>21	Zwischentitel: Hoffnung seines Landes und der Revolution.</p>
          
<p>22	Einstellung aus der Froschperspektive au eine andere Statue, die eine Girlande h&auml;lt. (Der Kamerastandpunkt vermittelt den Eindruck, als ob die Statue jeden Augenblick den Kopf Kerenskijs mit der Girlande schm&uuml;cken wolle.)</p>
          
<p>23	Zwischentitel: Alexandr Frjodorowitsch Kerenskij.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob02.jpg" align="left" height="130" width="180">

24	Sehr nah. Kerenskijs Gesicht, ruhig und gefa&szlig;t.</p>
          
<p>25	Nahaufnahme. Die Girlande in den H&auml;nden der Statue.</p>
          
<p>26	Sehr nah. Kerenskij, wie in Nr. 24. Der AUsdruck seines Gesichts entspannt sich, er l&auml;chelt.</p>
          
<p>27	Nahaufnahme. Die Girlande wie in Nr. 25</p>
          
<p>28-39		Kerenskij steigt weiter die Treppe empor und wird von einem der riesigen und hoch dekorierten Gardisten der zaristischen Leibwache begr&uuml;&szlig;t. Kerenskij, auf W&uuml;rde bedacht, wirkt neben dieser imposanten Figur l&auml;cherlich. Er wird einer ganzen Reihe von Leibgardisten vorgestellt und sch&uuml;ttelt jedem die Hand. Was f&uuml;r ein Demokrat!</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob03.jpg" align="left" height="130" width="180">
40-74		Kerenskij wartet vor einer reich verzierten T&uuml;r, die zu den privatgem&auml;chern des Zaren f&uuml;hrt. Wir sehen das zaristische Wappen an der T&uuml;r. Kerenskij wartet hilflos darauf, dass sich die T&uuml;r &ouml;ffnet. Zwei Gardisten grinsen. Kerenskijs Stiefel: dann, sehr nah, seine behandschuhten H&auml;nde, die sich ungeduldigbewegen. Die beiden Leutnants sind verlegen. Wir schneiden auf den Kopf eine reichornamentierten Prozellanpfaus: er sch&uuml;ttelt seinen Kopf, dann &ouml;ffnet er stolz dem Pfauenschwanz zu einem F&auml;cher: er beginnt sich zu drehen, indem er eine Art Tanz vorf&uuml;hrt, w&auml;hrend die Fl&uuml;gel im Licht glei&szlig;en. Die riesige T&uuml;r &ouml;ffnet sich. Einer der Gardisten grinst. Kerenskij &uuml;berschreitet die Schwelle; und weitere T&uuml;ren &ouml;ffnen sich. nach und nach. (Der Vorgang der sich &ouml;ffnenden T&uuml;ren wird mehrmals wiederholt, ohne da&szlig; die einzelnen Phasen des T&uuml;r&ouml;ffnens Anschlu&szlig; haben). Der Kopf des Pfaus h&auml;lt inne und blickt, anscheinend in voller Bewunderung, dem verschwindenden Kerenskij hinterher.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob04.jpg" align="left" height="130" width="180">
74-79		Schnitt auf die Soldaten, Matrosen und bolschewistischen Arbeiter, die teilnahmslos im Gef&auml;ngnis warten; dann auf Lenin, der sich in nebligen S&uuml;mpfen versteckt h&auml;lt.</p>
          
<p>80-99		Kerenskij in den Privatgem&auml;chern des Zaren. Nahaufnahmen der riesigen Nippessammlung, auf jedem Gegenstand die Initiale A des Zaren, sogar auf dem kaiserlichen Nachttopf. In den Gem&auml;chern der Zarin Alexandra Fjodorowna: noch mehr Nippes, kostbar verzierte M&ouml;bel, Borduren und Quasten, das Bett der Zarin. Kerenskij legt sich aufs Bett (gezeigt in drei aufeinaderfolgenden Einstellungen mit verschiedenen Kamerawinkeln). Alexandr Fjodorowitsch. Noch mehr Quasten und Bord&uuml;ren, etc.</p>
          
<p>100-105	In der Bibliothek Nikolaus' II. Kerenskij, neben einem Tisch in der zaristischen Bibliothek stehend, wirkt in dieser Umgebung klein und unscheinbar. Drei weitere Einstellungen von Kerenskij, die sich fortgesetzt von ihn entfernen und dadurch noch mehr die unscheinbare Gr&ouml;&szlig;e Kerenskijs in diesem riesigen Raum des Palastes verdeutlichen. Kerenskij nimmt ein St&uuml;ck Papier vom Schreibtisch.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob05.jpg" align="left" height="130" width="180">
106	Zwischentitel: Erla&szlig; zur Wiedereinf&uuml;hrung der Todesstrafe.</p>
          
<p>107	Halbtotale. Kerenskij am Schreibtisch, nachdenklich.</p>
          
<p>108	Totale. Kerenskij. Er beugt sich &uuml;ber den Schreibtisch und unterzeichnet den Erla&szlig;.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob06.jpg" align="left" height="130" width="180">
109	Einstellung vom obersten Absatz der Palasttreppe auf Kerenskij, der sich langsam der ersten Treppenstufe n&auml;hert.</p>
          
<p>110	Nahaufnahme. Ein Diener beobachtet ihn.</p>
          
<p>111-124	Kerenskij, den Kopf vorgeneigt, die Hand wie Napoleon in den Ausschnitt der Jacke geschoben, steigt bed&auml;chtig die Treppe hoch. Ein Ein Diener und einer der Leutnante beobachten ihn. Halbtotale. Kerenskij, auf den Boden blickend, die Arme vor der Brust verschr&auml;nkt. Eine Statuette von Napoleon in der gleichen Haltung. Der Diener und der Leutnant salutieren. Eine Reihe der gro&szlig;en Palast-Weingl&auml;ser. Noch eine Reihe Gl&auml;ser. Eine Reihe Zinnsoldaten, in der gleichen Anordnung wie die Weingl&auml;ser.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob07.jpg" align="left" height="130" width="180">
125	Nahaufnahme. Kerenskij am Tisch sitzend. Vor ihm stehen die vier Teile einer vierschnabeligen Karaffe. Die Teile stehen nebeneinander auf dem Tisch; Kerenskij starrt auf sie herab.</p>
          
<p>126	Nahaufnahme. Kerenskijs H&auml;nde versuchen, die vier Teile der Karaffe zusammenzuf&uuml;gen.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob08.jpg" align="left" height="130" width="180">
127	Halbtotale. Kerenskij.</p>
          
<p>128	Nahaufnahme. Kerenskijs H&auml;nde.</p>
          
<p>129	Halbtotale. Kerenskij. Er starrt auf  die Karaffe.</p>
          
<p>130	Gro&szlig;aufnahme. Kerenskijs Hand, die eine Schublade des Tisches &ouml;ffnet und einen Karaffenst&ouml;psel - in Form einer Krone - herausnimmt.</p>
          
<p>131	Halbtotale. Kerenskij; er hebt den wie eine Krone geformten St&ouml;psel in Augenh&ouml;he.</p>
          
<p>132	Sehr nahe Gro&szlig;aufnahme. Der wie eine Krone geformte St&ouml;psel.</p>
          
<p>133	Halbtotale. Kerenskij; er steckt die Krone auf die Karaffen&ouml;ffnung.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob09.jpg" align="left" height="130" width="180">
134	Sehr nahe Gro&szlig;aufnahme. Der kronenf&ouml;rmige St&ouml;psel, der nun die Karaffe verschlie&szlig;t.</p>
          
<p>135	Die Dampfpfeife einer Fabrik, die Dampf abbl&auml;st.</p>
          
<p>136	Sehr nahe Gro&szlig;aufnahme. Der kronenf&ouml;rmige St&ouml;psel.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob10.jpg" align="left" height="130" width="180">
137	Die Dampfpfeife.</p>
          
<p>138	Zwischentitel: Die Revolution in Gefahr</p>
          
<p>139	Sehr nahe Gro&szlig;aufnahme. Der kronenf&ouml;rmige St&ouml;psel.</p>
          
<p>140	Halbtotale. Kerenskij, der sich hinsetzt, um den St&ouml;psel auf der Karaffe zu bewundern.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob11.jpg" align="left" height="130" width="180">
141	Sehr nahe Gro&szlig;aufnahme. Der kronenf&ouml;rmige St&ouml;psel.</p>
          
<p>142	Die Dampfpfeife.</p>
          
<p>143-150	Zwischenschnitte der einzelnen Worte des Titels General Kornilow schreitet voran, dazwischen die Einstellungen der Dampfpfeife.</p>
          
<p>151	Zwischentitel:	Alle Kraft zur Verteidigung von Petersburg.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob12.jpg" align="left" height="130" width="180">
152	Totale einer Fabrik. M&auml;nner (Bolschewiken) mit Gewehren und Fahnen in der Hand laufen an der Kamera vorbei.</p>
          
<p>153	Die Dampfpfeife.</p>
          
<p>154	Zwischentitel: F&uuml;r Gott und Vaterland.</p>
          
<p>155	Zwischentitel: F&uuml;r </p>
          
<p>156	Zwischentitel: Gott</p>
          
<p>157	Die Kuppel einer Kirche</p>
          
<p>158	Nahaufnahme. Eine reichverzierte Ikone.</p>
          
<p>159	Eine hohe Kirchturmspitze, wobei die Kamera um 45 Grad nach links geneigt ist.</p>
          
<p>160	Die gleiche Kirchturmspitze, diesmal um 45 Grad nach rechts geneigt.</p>
          
<p>161-186	Einstellungen grotesker religi&ouml;ser Belder, Tempel, Buddhas,  afrikanischer Masken etc.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob13.jpg" align="left" height="130" width="180">
187	Zwischentitel: F&uuml;r</p>
          
<p>188	Zwischentitel: das Vaterland.</p>
          
<p>189-199	Nahaufnahmen von Orden, beschm&uuml;ckten Uniformen, Offiziers-Epauletten etc.</p>
          
<p>200	Zwischentitel: Hurra!</p>
          
<p>201	Der Sockel der Zarenstatue. (Die Statue war von Arbeitern in der ersten Aktrolle niedergerissen worden.) Die auf der Erde liegenden Einzelteile der zerst&ouml;rten Statue fliegen zur&uuml;ck in ihre urspr&uuml;ngliche Position auf dem Sockel.</p>
          
<p>202	Zwischentitel: Hurra!</p>
          
<p>203	Der gleiche Vorgang wie in Nr. 201, von einem anderen Kamerastandpunkt aus.</p>
          
<p>204	Zwischentitel: Hurra!</p>
          
<p>205-209	Kurze Einstellungen von religi&ouml;sen Abbildungen, die wir schon vorher gesehen haben. Sie scheinen alle zu l&auml;cheln.</p>
          
<p>210-219	Andere Teile der Zarenstatue setzen sich wieder zusammen. Schlie&szlig;lich fliegen Zepter und Kopf mit kurzem Gewackel in ihre Position zur&uuml;ck.</p>
          
<p>220-233	Verschiedene Kirchturmspitzen, wie zuvor, ebenfalls geneigt. Eine Kirchturmspitze auf den Kopf gestellt. Weihrauchf&auml;sser werden geschwungen. Der Kopf der Zarenstatue befindet sich wieder in seiner alten Position. Ein Priester h&auml;lt sein Kreuz hoch.</p>
          
<p>234-259	General Kornilow, Anf&uuml;hrer der konterrevolution&auml;ren Armee, mustert hoch zu Pferd seine Truppe. Die Statue Napoleons auf dem R&uuml;cken eines Pferdes, den Arm weit ausgetreckt, Eine &auml;hnliche Einstellung von Kornilow, der ebenfalls den Arm hebt. Kerenskij, immer noch im Zarenpalast, blickt mit verschr&auml;nkten Armen auf den St&ouml;psel der Karaffe.</p>
          
<p>Zwischentitel: Zwei Bonapartes.</p>
          
<p>Noch mehr Einstellungen von der Statue Napoleons. Der Kopf Napoleons, wie er nach links blickt. Der Kopf Napoleons, wie er nach rechts blickt. Die beiden K&ouml;pfe zusammen auf der Leinwand, sich gegenseitig anblickend. Zwei groteske religi&ouml;se Figuren, wie zuvor, in der gleichen Position. Weitere Einstellungen der Statue von Napoleon und eine weitere Sequenz mit religi&ouml;sen Bildnissen.</p>
          
<p>260	Kornilow erteilt zu Pferde den Marschbefehl.</p>
          
<p>261	Ein Panzer bewegt sich vorw&auml;rts, &uuml;berrollt einen Sch&uuml;tzengraben.</p>
          
<p>262	Kerenskij w&auml;lzt sich im Schlafgemach der Zarin verzweifelt auf dem Bett herum.</p>
          
<p>263	 Teile der Napoleonb&uuml;ste liegen verstreut auf dem Boden.</p>
          
<p>Eisenstein stellt im Gegensatz zu Griffith keinen dramatischen Handlungsablauf dar. Er charakterisiert Kerenskij, ironisiert (wertet) seine Person. Das bildlich dargestellte entspricht nicht unbedingt dem zu vermittelnden Inhalt (Metaphern, Wortspiele, z. B. Einstellungen 40-74 - Pfau, Einstellung 118 - Statuette von Napoleon). Es gibt keine Kontinuit&auml;t in den einzelnen Sequenzen, in dem Sinne, da&szlig; eine Einstellung an die andere "anschlie&szlig;en" w&uuml;rde. Weder r&auml;umlich noch zeitlich ist ein ersichtlicher Zusammenhang gegeben (Einstellung 108 auf 109 - Wechsel von Bibliothek zu Treppenabsatz). Eisenstein versucht nicht <i>"die Illusion einer Geschichte durch sich logisch entwicklende Sequenzen zu schaffen<unquote>, sondern die </unquote>Bedeutung des Konflikts und den ideologischen Hintergrund des Konflikts zu erkl&auml;ren"</i> <small>(REISZ, S.30)</small>
</p>
          
<h4>3. "Dickens, Griffith und wir" - Abgrenzungen / Ideologie</h4>
          
<p>Griffith hat in seinem Film INTOLERANCE (1916) versucht, den Rahmen eines rein dramatisch gepr&auml;gten Erz&auml;hlverlaufs zu verlassen, oder wie es Eisenstein selbst beschrieben hat, &uuml;ber die <i>"Grenze der Fabel hinaus in das Gebiet der Verallgemeinerung und der Allegorie vorzusto&szlig;en"</i> <small>(EISENSTEIN, S. 199))</small>
</p>
          
<p>INTOLERANCE sollte vier Epochen in vier Handlungsstr&auml;ngen miteinander verflechten, die sich zun&auml;chst unabh&auml;ngig voneinander entwickeln, allm&auml;hlich ann&auml;hern, um sich schlie&szlig;lich laut Griffith <i>"im Finale zu einem ganz m&auml;chtigen Strom von aufw&uuml;hlender Emotion"</i> zu vereinigen.
Verbindendes Element war ein Motiv nach Walt Whitman, das refrainartig zu jeder neuen Epoche wiederkehrte: Lilian Gish, die eine Wiege schaukelte. Sinnbild f&uuml;r die immer wiederkehrenden, sich wiederholenden Abl&auml;ufe der Epochen. Das Bild wurde vom Publikum weder angenommen, noch verstanden.</p>
          
<p>Eisenstein stellt in seinem Artikel <i>"Dickens, Griffith und wir"</i> <small>(1941/42)</small> ausf&uuml;hrlich dar, worin er das grunds&auml;tzliche Mi&szlig;verst&auml;ndnis in Griffith Versuch sieht, eine Allegorie zu transportieren: Nicht "darstellende Montagebilder" ("Wiege") erzeugen Metaphern, sondern <i>"allein das Bild der Montagegegen&uuml;berstellung"</i> <small>(EISENSTEIN, S. 119)</small>. <i>"Griffith bleibt &uuml;berall beim nur Darstellenden und Gegenst&auml;ndlichen stehen und bem&uuml;ht sich nirgends, durch Gegen&uuml;berstellung der Bildausschnitte zum inneren Sinn und zum Bildlichen vorzudringen"</i> <small>(EISENSTEIN, S. 118)</small>. Nach Eisenstein also eine prinzipiell falsche Herangehensweise.</p>
          
<p>
            
<i>"Die Gr&uuml;nde hierf&uuml;r sind nicht professionell-technischer, sondern ideologisch-gedanklicher Natur.
Es geht hier nicht so sehr darum, da&szlig; die Darstellung bei richtiger Anlage und Bearbeitung nicht zu bildlicher, metaphorischer Aussage gesteigert werden kann oder da&szlig; Griffith von seiner Methode oder seiner handwerklichen Meisterschaft im Stich gelassen wird.
Es geht vielmehr darum, da&szlig; Griffith trotz seiner Versuche und Bem&uuml;hungen letztlich unf&auml;hig ist, Erscheinungen wirklich sinnvoll zu abstrahieren, das hei&szlig;t aus der Mannigfaltigkeit historischer Fakten die verallgemeinerte Deutung einer historischen Erscheinung herauszuarbeiten."</i> <small>(EISENSTEIN, S. 122)</small>
          
</p>
          
<p>Die Montageauffassung ist nach Eisenstein zun&auml;chst Ausdruck der herrschenden Gesellschaftsordnung. Erst durch eine entsprechende Erkenntnismethode, die des Marxismus, die Anwendung der Dialektik (Dialektik als Gesetz der Naturrevolution laut Engels und Lenin) sei <i>"zum ersten Mal die M&ouml;glichkeit zum Abstrahieren und zum Gebrauch &uuml;bertragener Begriffe"</i> <small>(EISENSTEIN, S. 123)</small> gegeben.</p>
          
<p>Nach seiner Auffassung h&auml;t der b&uuml;rgerliche Film stark an der gegenst&auml;ndlichen Abbildung fest, und strebt (z. B. durch Allegorie oder &Uuml;bertragung) keinen Erkenntnis- oder Lernproze&szlig; an. Stattdessern wird durch die g&auml;ngigen Montageverfahren die existierende Gesellschaftsordnung visualisiert und etabliert. So z. B. durch die Parallelmontage, die von Griffith sozusagen "perfektioniert" wurde: Durch diese materialisierte Darstellung zweier sich nie ber&uuml;hrender Lebensbereiche manifestiert sich das Bild der kapitalistischen Gesellschaft. Die Parallelit&auml;t beschreibt das Oben und Unten getrennter sozialer Schichten, die Entwicklung von Gefahr und Errettung oder das Schicksal der Liebenden, die getrennt bleiben bis zur Vereinigung im <i>"transzendierenden und entr&uuml;ckten"</i> <small>(SEE&szlig;LEN, S. 24)</small> Happy End. Ein Schnittpunkt oder Aufeinandertreffen dieser Linien bleibt seinem Wesen nach f&uuml;r Eisenstein irrational und unlogisch. </p>
          
<p>Erst die Dialektik erm&ouml;glicht die <i>"Entwicklung des Widerspruchs zwischen den Klassen, der auf die revolution&auml;re Tat hinausl&auml;uft"</i> <small>(SEE&szlig;LEN, S.24)</small>. Diese Dialektik entsteht erst durch die Montage. Nicht die Handlung oder Geschichte soll bei den Zuschauern einen Lernproze&szlig; einleiten, sondern auf einer anderen Ebene das Verfahren der Montage. Mit diesem Gedanken besch&auml;ftigt sich Eisenstein in seinem kompletten theoretischen und praktischen Werk.</p>
        
</subsubsection>
      
</subsection>
      
<subsection title="4. Die Entwicklung des Montagebegriffs">
        
<p>Man kann bei Eisensteins Montageauffassung nicht von einem sich kontinuierlich entwickelnden Theoriegeb&auml;ude sprechen. Er entwickelt Begriffe, l&auml;&szlig;t sie sp&auml;ter wieder fallen oder verwendet sie in einem anderen Bedeutungskontext - ohne in seinen Schriften daraufhinzuweisen. Die Anregungen dazu erh&auml;lt er aus den verschiedensten - meist film-fremden - Bereichen: meistens der Psychologie (seine Filme sollen den Zuschauer ver&auml;ndern, entprechend mu&szlig; er dessen Wahrnehmungproze&szlig; kennen), aber auch aus der Gestalt- oder Literaturtheorie, deren Begriffskanon er oft &uuml;bernimmt. <br>
Grundlegende Begriffe erfahren ensprechend verschiedene Bedeutungsstadien, so z. B. die Attraktion - das Grundelement der Montage - durchl&auml;uft drei Phasen: Strukturelement, Stimulus (Reflexologie, Pawlow), sprachliches Paradigma zum Bild.</p>
        
<p>Seine Filme lassen sich nicht parallel zu seinen theoretischen Schriften "lesen". Oft greift er mit ihnen voraus, kann erst sp&auml;ter deren neue Ans&auml;tze beschreiben oder sie in einem erweiterten Zusammenhang sehen. Entprechend finden sich Ans&auml;tze f&uuml;r seine intellektuelle Attraktion schon sehr fr&uuml;h in seinem Werk.</p>
        
<subsubsection title="4.1  Montage- und Attraktionsbegriff im Theater">
          
<p>1920 kam Eisenstein nach einem abgebrochenen Architekturstudium an das Moskauer Proletkulttheater. (Anm. 2) Die Proletkultkonferenz verstand Theater als die <i>"Synthese aus allen anderen Arten der Kunst - als &lt;Organisation menschlichen Handelns&gt;, als &lt;wesentliches Mittel f&uuml;r den Aufbau des Lebens nach dem Willen des Proletariats&gt;"</i> <small>(WEISE, S. 21)</small>. Er geh&ouml;rte dort dem linken Fl&uuml;gel an, der entgegen dem b&uuml;rgerlich, <i>"abbildend-erz&auml;hlenden Theater<unquote>, ein </unquote>Agitationstheater der Attraktionen"</i> <small>(WEISE, S. 23)</small> vertrat. Attraktionen im Sinne von "Nummern", die aneinadergef&uuml;gt, einen Lernproze&szlig; bei den Zuschauern bewirken sollten. 1922 verlie&szlig; er das Proletkult-Theater.</p>
          
<p>Er absolvierte einen dreimonatigen Montagekurs im Schnellverfahren bei Lew Kuleschow (Konstruktivist, erster sowjetischer Montage-Experimentator und Theoretiker), war 1922 Mitarbeiter von Esfir Schub bei der "ideologisch-korrekten" Ummontage von Fritz Langs DR. MABUSE, DER SPIELER (1922).</p>
          
<p>Den wichtigsten Einflu&szlig; erfuhr er im "Theaterlaboratorium" Wsewolod E. Meyerhold (Gr&uuml;nder des staatlichen Regieinstitutes in Moskau, 1921 - GVYRM) ein Konstruktivist, radikaler Theatererneuerer und linker K&uuml;nstler. </p>
          
<p>Er lehrte die von ihm entwickelte Biomechanik, eine verfremdende Ausdrucksbewegung und trieb die "Kinofizierung" des Theaters voran: den Zerfall des traditionellen Dramas in kleine, geschlossene Einheiten / Episoden, die frei montierbar waren, ohne Bindung an eine der Chronologie oder Ort-Zeit-Einheit. Die Handlung konnte auch durch einzelne "Nummern", dem Heraustreten aus der B&uuml;hnenrealit&auml;t, der direkten Ansprachen an die Zuschauer oder aktuelle Diskussionen unterbrochen werden. </p>
          
<p>Montage wurde von den Konstruktivisten in Analogie zum konstruktiven Produktionsproze&szlig; verstanden, der aus dem Bauelement ("Attraktion") und der Konstruktion (F&uuml;gung) besteht, die sie f&uuml;r das Theater entsprechend neu zu bestimmen versuchten (z. B. im Gegensatz zur Bildenden Kunst). 1927 schrieb Eisenstein entsprechend: <i>"Ich bin Zivilingenieur und Mathematiker von Beruf. Ich gehe an die Herstellung eines Films in gleicher Weise wie an die Einrichtung einer Gefl&uuml;gelfarm oder die Installation einer Wasserleitung. Mein Standpunkt ist ein durchaus utilitaristischer, rationeller, materialistischer."</i> <small>(BULGAKOWA, S. 39)</small>
</p>
          
<p>Eisenstein, der nach dieser Methode inszenierte, erkannte bald, da&szlig; die M&ouml;glichkeiten des Theaters f&uuml;r eine derartige Attraktions- bzw. Montagemethode eingeschr&auml;nkt waren. Die Auff&uuml;hrung vom GESCHEITESTEN (1923) beendete er mit einer weiteren "Attraktion" - seinem ersten Kurzfilm GLUMOWS TAGEBUCH. </p>
        
</subsubsection>
        
<subsubsection title="4.2 &quot;Montage der Attraktionen&quot;">
          
<p>1923 verfa&szlig;t Eisenstein sein 1. Manifest "Montage der Attraktionen", das noch sehr stark von seiner Theaterarbeit und der konstruktivistischen Lehre gepr&auml;gt ist. Der von ihm hier eingef&uuml;hrte Begriff <i>"Attraktion"</i> wurde zur Ma&szlig;einheit einer Auff&uuml;hrung erkl&auml;rt. Allerdings sieht er, entgegen seinen Kollegen, die Attraktion nicht mehr als "Trick", der gleich einer "Nummer" in einem Zirkus, einer Music Hall oder einem Nummernprogramm vor allem die Aufmerksamkeit des Publikums steigern soll. Eisenstein - und das ist ein v&ouml;lliges Novum - setzt die Ma&szlig;einheit des Bauelements "Attraktion" gleich mit der Ma&szlig;einheit der Wirkung, im Sinne eines Stimulus.</p>
          
<p>Diese Auffassung ist vor allem Ausdruck der damals sehr popul&auml;ren Theorien der Reflexologie und des Behaviorismus. Das visuelle Element der Attraktion f&uuml;hrt direkt zu einem physischen Stimulus / Reflex. Die Folge - also die Montage der Attraktionen - bewirken die entsprechenden Emotionen. Dabei ist die Attraktion als Bauelement bereits ein zusammengesetzter Reiz, was zu auseinanderstrebenden Emotionen f&uuml;hrt. Der Regisseur konzipiert dabei zwar den Endeffekt, jedoch erst der Zuschauer f&uuml;gt in seinem Wahrnehmungsproze&szlig; die Reize zusammen. Entsprechend bewegte sich die Betrachtung weg vom Kunstwerk und hin zu seiner Wirkung. </p>
          
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">STREIK (1924)</h3>
<p>
              
<i>"Grundlage des Szenariums sind die realen Ereignisse um einen Streik aus dem Jahre 1902, polit&ouml;konomische Studien der Autorengruppe und deren Erfahrungen, die sie bei Fabrikbesuchen, in Diskussionen dort mit Arbeitern und alten Revolution&auml;ren sammelt: <unquote>[...]</unquote> Arbeiter einer Fabrik bereiten den Streik vor. Der Direktor setzt eine Schar von Spitzeln zu &Uuml;berwachung ein. Als sich ein zu Unrecht des Diebstahls beschuldigter Arbeiter erh&auml;ngt, ist der Anla&szlig; zum Streik gegeben. Forderungen der Arbeiter wie der Acht-Stunden-Tag und h&ouml;here L&ouml;hne werden von der Fabrikverwaltung abgelehnt. Hunger zieht in die Arbeiterviertel ein, aber der Streik wird fortgesetzt. Mit allen Mitteln versucht die Polizei, die Streikenden zu provozieren. Als sich zeigt, da&szlig; die Streikfront immer fester wird, setzten die Ortsbeh&ouml;rden Kosaken ein, die in den Mietskasenen der Arbeiter ein gro&szlig;es Blutbad anrichten. - Dem Film vorangestellt ist ein Lenin-Zitat aus dem Jahre 1907: 'Die Kraft der Arbeiterklasse ist die Organisation. Ohne Organisation der Massen ist das Proletariat nichts. Ist es organisiert, ist es alles.'"</i> <small>(WEISE, S.34)</small>
            
</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
          
<p>Durch die reflexologische Betrachtung wurde gleichzeitig die Hierarchie der Einwirkungsmitte au&szlig;er Kraft gesetzt. Ein Paukenschlag als Stimulus hat den gleichen Wert wie ein Shakespearemonolog - akustische, visuelle, taktile, olfaktorische Reize sind gleichberechtigt.
<i>"Die konsequente Schlu&szlig;folgerung aus dem Manifest lautet: Attraktionsmontage ist die Schaffung einer Kette von bedingten Reflexen. Die Kunst ist ein Training im sozialen Reagieren. Der Theater- oder Film-Ingenieur entwickelt eine Kombination von Reizen, eine klassische Konditionierung. Reize k&ouml;nnen arbitr&auml;r gekoppelt werden, um soziale Reflexe zu trainieren (wie Klassenha&szlig; und Klassensolidarit&auml;t). Attraktion tritt als ein zusammengesetzter, &lt;komplexer&gt; Reiz auf."</i> <small>(BULGAKOWA, S. 40)</small>
</p>
          
<p>Da&szlig; die Attraktion im Film wesentlich einfacher einsetzbar ist als im Theater ist die konsequente Schlu&szlig;folgerung. Die Abbildung ist per se bereits abstrahierend, d. h. nur ein Bild von der Realit&auml;t, sie erfordert bereits eine "Assoziation" und der prinzipielle Aufbau eines Filmes, die technisch erforderliche Kopplung, beinhaltet schon eine Form der Montage.haltet schon eine Form der Montage. Die Hinwendung zum Film bedeutet den Bruch mit seinem k&uuml;nstlerischen Ziehvater Meyerhold.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/massaker.jpg" align="left" height="437" width="250">Die Montage der Filmattraktion kann sich allerdings von der blo&szlig;en physiologischen Einwirkung entfernen und zu einer Abstraktion f&uuml;hren. Eisenstein belegt dies anhand eines Ausschnittes aus seinem ersten Spielfilm STREIK (1924), den er selbst als <i>"Oktober des Films."</i> <small>(KAUFMANN, S. 55)</small> bezeichnet hat.</p>
          
<p>Am Ende des Films ist zu sehen, wie die Streikenden niedergeschossen werden. Zwischen die Aufnahmen von den Demonstranten - meist in der Totalen - schneidet Eisenstein die authentische Abschlachtung eines Ochsen im Schlachthof - er geht dabei immer mehr in die Gro&szlig;aufnahme, bis am Schlu&szlig; nur noch das tote weit ge&ouml;ffnete Ochsenauge zu sehen ist.  <i>"Das physiologisch empfundene Entsetzen angesichts des realen T&ouml;ten und des Todes wird &uuml;bertragen auf die Szene des Menschenmassakers, das, wie Eisenstein meinte, kein Schauspieler so schaurig und real darstellen k&ouml;nne. Nicht der logisch nachvollzogene Vergleich (Schlachthof - Massaker) ist dabei von Bedeutung. Sondern die &Uuml;bertragung der emotionalen Ersch&uuml;tterung dient dem notwendigen ideologischen Effekt, der mit dem Zwischentitel &lt;vergi&szlig; nicht, Proletarier!&gt; einen Schlu&szlig;punkt setzt."</i> <small>(BULGAKOWA, S. 41f)</small>  D. h. Eisenstein erwartete von den Rezipienten / Zuschauern eine rationalisierte Verallgemeinerung Assoziationen.</p>
          
<p>Hier zeigt sich der Widerspruch zwischen seinem Anspruch und seiner damaligen Montagetheorie: H&auml;tte Eisenstein damals mehr &uuml;ber Pawlows Versuche gewu&szlig;t, so schreibt er sp&auml;ter, h&auml;tte er Attraktion gleich als Stimulus bezeichnet. Da wird deutlich, wie eng der Attraktionsbegriff noch an die klassische Konditionierung gebunden ist: Stimulus - Reaktion. Das Bewu&szlig;tsein, der Entscheidungsproze&szlig; bleibt ausgeschlossen. Dem K&uuml;nstler wird die Freiheit der (Massen-) Manipulation zugesprochen. Es stellt sich die Frage nach der Programmierbarkeit von Assoziationsketten. Zudem - stereotype Reaktionen auf Grund von Konditionierung beinhalten zwar einen Lernproze&szlig;, jedoch keinen (bewu&szlig;ten) Erkenntnisproze&szlig;.
In seinem Artikel "Die Inszenierungsmethode eines Arbeiterfilms" (1925) sieht Eisenstein die Grenzen, die der Assoziation bzw. der &Uuml;bertragbarkeit gesetzt sind. Sie setzten eine "Lesef&auml;higkeit" der Zuschauer voraus. Die kann, je nach Bev&ouml;lkerungsgruppe, sehr verschiedenen sein:</p>
          
<p>
            
<i>"Bei dem Arbeiterpublikum l&ouml;ste der Schlachthof durchaus keinen 'blutigen' Effekt aus, und zwar aus dem einfachen Grund, weil f&uuml;r den Arbeiter das Blut hier in erster Linie die Assoziation zu den bei Schlachth&ouml;fen &uuml;blichen Verarbeitungsfabriken herstellt. Auf den Bauern gar, der selbst gw&ouml;hnt ist, Vieh zu schlachten, wird die Wirkung gleich Null sein."</i> <small>(KAUFMANN, S. 60)</small>
          
</p>
          
<p>Und: je heterogener die Zuschauergruppe zusammengesetzt ist, um so kleiner ist ihr gemeinsamer Assoziationskanon.</p>
          
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">PANZERKREUZER POTEMKIN (1925)</h3>
<p>
              
<i>"PANZERKREUZER POTEMKIN h&auml;lt sich im wesentlichen an die historische Vorlage des Matrosenaufstandes. Nach den Niederlagen im Russisch-Japanischen Krieg versch&auml;rft sich in der Schwarzmeerflotte der Druck des Offizierkorps auf die Mannschaften. die Unruhe unter den Matrosen wird gr&ouml;&szlig;er, und bolschewistische Kader planen f&uuml;r September 1905 einen Flottenaufstand. Am 14. Juni weigert sich eine Gruppe von Matrosen auf dem Panzerkreuzer 'F&uuml;rst Potemkin von Taurien', verdorbenes Fleisch zu essen. Als die Matrosen erschossen werden sollen, meutert die gesamte Mannschaft. Die Offiziere werden verhaftet, das Schiff l&auml;uft den Hafen von Odessa an, um einen von den Offizieren ermordeten bolschewistischen Matrosen zu bestatten. Die gerade streikende Odessaer Arbeiterschaft solidarisiert sich mit den Aufst&auml;ndischen und erweist dem Toten die letzte Ehre. Drei Tage sp&auml;ter f&auml;hrt der Panzerkreuzer dem zur Unterdr&uuml;ckung der Meuterei herbeieilenden Admiralsgeschwader entgegen. Die 'Potemkin'-Matrosen fordern die Mannschaften der Flotte auf, sich der Rebellion anzuschlie&szlig;en. Die Aufst&auml;ndischen d&uuml;rfen passiern, und der Panzerkreuzer 'Georgi Pobedanosez' schlie&szlig;t sich ihnen an, jedoch nur f&uuml;r kurze Zeit. Die 'Potemkin' bleibt isoliert, und als das Schiff den Hafen von Konstanza anl&auml;uft, wird es von der rum&auml;nischen Regierung interniert und an Ru&szlig;land ausgeliefert. Die meisten der aufst&auml;ndischen Matrosen werden nach ihrer R&uuml;ckkehr in die Heimat von den zaristischen Beh&ouml;rden umgebracht."</i> <small>(WEISE, S. 46f)</small>
            
</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
        
</subsubsection>
        
<subsubsection title="4.3  Montage als Konflikt und Dominante-Oberton">
          
<p>1925 hat Eisensteins zweiter Film PANZERKREUZER POTEMKIN Premiere in Berlin. Das Publikum - das b&uuml;rgerliche! - reagiert mit Begeisterung. Auf Reize, Stimuli, die eigentlich das Proletariat aufr&uuml;tteln sollten. Der Wahrnehmungsproze&szlig; lie&szlig; sich also nicht nur auf soziale Stimuli einschr&auml;nken.<br>
Es wird deutlich, da&szlig; sein nur reflexologisches Analysemodell als eine lineare Kette von Stimuli den Wahrnehmungsproze&szlig; der Zuschauer nicht beschreiben konnte. 1929 erweiterte Eisenstein das Modell in seinem Artikel "Jenseits der Enstellung" um den Begriff des Konflikts. Angeregt dazu wurde er durch die Bekanntschaft mit dem Psychologen Luria - Eisentein &uuml;bertrug dessen Konfliktbegriff auf die leninistische Dialektik. F&uuml;r ihn liegt der Konflikt innerhalb einer Einstellung, sie <i>"akkumuliert Konflikte (des Vorder- und Hintergrunds, der Linien, Konturen, Volumen, Lichtflecken, Massen Bewegungsrichtungen, Beleuchtung, eines Vorgangs und seiner zeitlichen Darstellung in Zeitlupe oder Zeitraffer"</i> - fast alle aufgez&auml;hlten Konflikte sind visueller Natur - <i>"sie &lt;zerrei&szlig;en&gt; das Bild und werden durch die n&auml;chste Einstellung ab(auf)gel&ouml;st."</i> <small>(BULGAKOWA, S. 48)</small> Der Konflikt kann aber auch zwischen zwei Einstellungen bestehen. Gem&auml;&szlig; dem 2. Gesetz der Dialektik kann das Nebeneinander der konfliktreichen Einstellungen  (Quantit&auml;t), <i>"(potentiell) einen Sprung in die neue Qualit&auml;t bedeuten"</i> <small>(BULGAKOWA, S. 48)</small>. Oder anders: Dieser Sprung am Schnittppunkt zweier materieller Bilder kann &uuml;ber die Vorstellung / Wahrnehmung des Zuschauers / der Zuschauerin in eine nicht-materielle Ebene erfolgen - z. B. in die einer ideologischen Erkenntnis. Dadurch erh&auml;lt Eisensteins Analysemodell eine neue Dynamik im Wahrnehmungsproze&szlig;. Es geht nicht mehr um die Addition gleichwertiger Stimuli, sondern um eine strukturierte Wahrnehmungsarbeit des Zuschauers / der Zuschauerin.</p>
          
<p>Innerhalb der Wechselbeziehung zwischen den Einstellungen findet eine dynamische Indergration der einzelnen Elemente statt - die Br&uuml;che, Widerspr&uuml;che verarbeiten kann ohne die Einheitlichkeit zu verlieren. Ein linearer Wahrnehmungsproze&szlig; dagegem k&ouml;nnte nur kausale Zusammmenh&auml;nge darstellen, z. B. eine r&auml;umliche oder zeitliche Kontinuit&auml;t.</p>
          
<p>Zeitgleich zu dieser Montage als Konstruktion &uuml;ber Attraktion (bzw. Konfkikt) erwickelte er das Modell der Montage als Konstruktion &uuml;ber Dominante. Diese beschreibt er in "Die vierte Dimension im Film" (1929). Beides sind an sich kontr&auml;re Modelle. Die Dominante leitet Eisenstein aus der noch in den Kinderschuhen steckenden Gestaltpsychologie ab, die er &uuml;ber den Psychologen Wygotski kennengelernt hatte. Dessen Besch&auml;ftigung mit der Ekstase und der Natur des Reizes f&uuml;hrte ihn zu einem kathartischen Kinstmodell. Nach seinem Ansatz setzt die "Evolution" dort ein, <i>"wo der Mensch k&uuml;nstliche Zeichen einf&uuml;hrt: k&uuml;nstlich geschaffene Reize, die das Verhalten steuern und die Bildung neuer bedingter Verbindungen hervorrufen"</i> <small>(BULGAKOWA, S. 46)</small>. Die Erweiterung des Zeichenvorrats dient der Sozialisation der Pers&ouml;nlichkeit. Die Aneingnung des Zeichenvorrats ist ein bewu&szlig;ter Proze&szlig; des Rezipienten / der Rezipientin.</p>
          
<p>Eisenstein deutet diese Zeichen als visuelle Attraktions-Zeichen. Sind sie Hauptmerkmal bzw. stehen sie im Vordergrund einer Einstellungssequenz, nennt er sie Diminante. Es k&ouml;nnnte sich dabei z. B. um den Vordergrund, die wichtigste Bewegungsrichtung, das Tempo usw. Handeln.
In seiner eigenen Analyse einer Sequenz aus dem Panzerkreuzer (1934) beschreibt er die Dominate - die Bewegungsrichtung, Vertikale - Horizontale - Halbkreisen - und deren &Uuml;berg&auml;nge: <i>"In der Komposition der Einstellung dominieren Vertikalen, doch die Bewegung ist horizontal; die Vertikalten der Komposition werden von Halbkreisen verdr&auml;ngt, aber die Beibehaltung der Bewegungsrichtung sorgt f&uuml;r eine Verflechtung. Dann &auml;ndert sich die Richtung der Bewegung (von links-rechts zu unvermitteltem rechts-links) unter Beibehaltung der Dominanz vom Halbkreis in der Komposition. In der letzten, einenden Einstellung kommen die auseinaderstrebenden Tendenzen (Horizontale und Vertikale, bewegung links-rechts und rechts-links) zusammen, vorbereitet durch ausgekl&uuml;genten Kombinationswechsel davor."</i> <small>(BULGAKOWA, S. 142)</small>
</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/dominante.jpg" align="left" height="290" width="250">Die Dominante wirkt verkn&uuml;pfend. Erst in der Kombination enth&uuml;llt sie ihren Charakter - sie kann nich alleine sthe, ist relativ und ver&auml;nderlich. Die Montage nach Dominaten erm&ouml;glich dabei ein differeniertes Spektrum von &Uuml;berg&auml;ngen - vom Hin&uuml;bergleiten in die n&auml;chste Einstellung bis zu derem totalen Gegensatz. Montage bestimmt so im Konflikt die semantische Eigenart des Films.</p>
          
<p>Die in den Anf&auml;ngen formulierte Montage als Summe oder offene Reihung, versteht Eisenstein jetzt als komplexeren, hierarchischen Zusammenhang aus Einheit, Struktur und System. Von der konstruktivistischen Attraktionsmontage hin zur Montage als dynamische Integration. Jetzt ist die Dominate das strukturgebende Prinzip und der Einzelbaustein. Sie l&ouml;st dabei das konstruktive Prinzip als Verfahren ab - vergleichbar dem dynamisierten Attraktions-Konfliktbegriff. Entscheidend sind dabei die Differenzqualit&auml;ten, die die Beziehung zwischen den dominierenden und untergeordneten Elementen ausmachen. Die einstigen Au&szlig;enfaktoren (Ausrei&szlig;er aus einer Reihe) werden jetzt zu innertextlichen Parametern.
<i>"Die Wahl der Dominante ist ein Ergebnis des Kampfes zwischen verschiedenen formbildenden Elementen"</i> <small>(BULGAKOWA, S. 50)</small>. Entsprechend dynamisch ist der Aufnahmeproze&szlig; des Betrachters. Er synthetisiert die Konflikte und ordnet sie in ein hierarchisches System.
Um den Umgang mit der Dominanten, die Strukturierungsarbeit f&uuml;r den Zuschauer zu vereinfachen, bem&uuml;hten sich die Konstruktivisten und anfangs auch Eisenstein, sie herauszuarbeiten und von unwichtigeren bzw. unbwu&szlig;t wirkenden Bildreizen zu befreien. Durch Akzentuierung (Ausschnitte, Perspektivenwahl) oder Reduktion der restlichen Bildreize. Dadurch erhofften sie sich eine eindeutigere Lesbarkeit.
Eisenstein interressierte sich allerdings gerade f&uuml;r diese so schwer greifbaren psychologischen Begleiterscheinungen, Anh&auml;ngsel der Dominanten. In Anlehnung an den amerikanischen Psychologen W. James bezeichnete er sie als Oberton. In diesem Oberton schwingt das Empfinden noch &uuml;ber eine Einstellung hinaus weiter, und &uuml;berlagert diese in Form von nicht abgeklungenen Prozessen. Damit entsteht eine Spannung <i>"zwischen den pr&auml;valenten und sich verdr&auml;ngenden, oszillierenden semantischen Paradigmen"</i> <small>(BULGAKOWA, S. 52)</small>. D. h. bei bewu&szlig;t visuell wahrgenommenen Dominanten klingen immer noch unbewu&szlig;t wahrgenommene obertonale Begleiterscheinungen mit an - physiologische und strukturelle Momente verschmelzen - dadurch erh&auml;lt Eisenstein eine sinnliche Komponente in der Montage.</p>
          
<p>Allerdings f&auml;llt es Eisenstein schwer, die Komplexit&auml;t des Obertonbegriffs zu fassen. Die Bestimmung des Konflikts zwischen zwei Einstellung verliert zunehmend f&uuml;r ihn an Bedeutung, ihn interessieren st&auml;rker die Gesetze der Verbindung. Und vor allem die der anderen Stimuli, die Obert&ouml;ne, das "Unfa&szlig;bare". So l&auml;&szlig;t er nach dem Aufsatz "Die vierte Dimension im Film" (1929) den Begriff Dominante fallen und widmet sich dem Unsichtbaren, visuell nicht Greifbaren. Das f&uuml;hrt ihn schlie&szlig;lich zur intellektuellen Montage. </p>
        
</subsubsection>
        
<subsubsection title="4.4  Intellektuelle Attraktion IA-28">
          
<p>Der Begriff der Intellektuellen Attraktion taucht bei Eisenstein erstmals 1928 im  Zusammenhang mit seinem Film OKTOBER (1927) auf.</p>
          
<p>Begonnen hatte alles mit einem "harmlosen Sprachspiel" im PANZERKREUZER POTEMKIN, wo sich in der Folge  einer Montagesequenz drei verschiede Steinskulpturen der Odessaer Treppe von einem schlafenden zu einem br&uuml;llenden L&ouml;wen erhoben - als Bild des Idioms <i>"Die Steine br&uuml;llen!"</i>. Was hatte Eisenstein getan (abgesehen davon, da&szlig; diese Sequenz vielfach mi&szlig;verstanden wurde)? Eine sprachliche Metapher bildlich umgesetzt und eine Bewegungsillusion geschaffen.</p>
          
<h4>Sprachparadigmen und Bild-Wort-Spiele</h4>
          
<p>Eisenstein zielte auf die kognitive Ebene des Zuschauers ab, die Ebene der konzeptualen Erkenntnis. Er begann sich intensiver mit Sprachaufbau und Metaphorik auseinanderzusetzen,  - Ans&auml;tze, die er teilweise schon w&auml;hrend seinter Theaterarbeit verfolgt hatte. In OKTOBER findet sich dann eine "Materialisierung" bekannter Sprachmetaphern.</p>
          
<p>
<i>"Das Spiel der Syllogismen l&ouml;st das lebendige Spiel der Leidenschaften ab... hier wird erstmalig einschieden eine prinzipielle Linie zwischen Theater und Film gezogen... das Attraktionsprinzip ist unzerst&ouml;rbar. 1928 wird die Attraktion korrigiert: im Theater zielt sie auf das Gef&uuml;hl, im Film auf das Bewu&szlig;tsein."</i> <small>(BULGAKOWA, S. 55)</small>
Im Theater sieht er einen Reiz als <i>"ein Stimulus, der an den unbedingen Reflex gerichtet ist"</i>, im Film, bei der intellektuellen Attraktion handelt es sich um einen "assoziativen" Reflex, der bereits auf die Sprache und Sprachspiele ausgerichtet ist.</p>
          
<p>Zun&auml;chst untersucht und verwendet er Metaphern und Metonymien. Vor allem in OKTOBER vielfach als Scherze oder Ironisierung. Z. B. nach einem Zwischentitel <i>"Die Kosaken haben Kerenski verraten"</i> (Im Russischen gleichbedeutend mit "betrogen") schneidet er auf ein Hirschgeweih in Kerenskis Zimmer. Er stellt Kerenski einen Pfau entgegen (vgl. Bildsequenz in 3. Aktrolle OKTOBER, Kapitel 2.2), Rednern eine Lyra, dem Menschewiken eine Balalaika. Nicht immer wurden diese Wort-Bild-Spiele vom Publikum verstanden bzw. angenommen.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/loewe.jpg" align="right" height="407" width="180">
Der stark abbildend-darstellende Charakter, den Eisenstein typisch f&uuml;r die europ&auml;ische Kunst sah, schien eine Abstraktion auf einer kognitiv-sprachlichen Ebene zu erschweren. Zum einen sah er eine M&ouml;glichkeit in der Transformation der Bilder, die das Figurativ-Abbildende unterdr&uuml;cken sollte: Deformation und Verfremdung durch Licht, Optik, Aufnahmewinkel, Bildkomposition, Isolierung in Nahaufnahme, Substitution des Ganzen durch einen Teil dank der Zergliederung im Bildausschnitt usw. Z. B. in der vorgestellten Aktrolle aus OKTOBER kippt die Kamera um 45&iexcl; bei den Kirchturmaufnahmen und leitet so die G&ouml;ttersequenz ein.
Zum anderen begann er sich mit japanischer Hieroglyphik auseinanderzusetzen. Begriffe ergeben sich dort meist aus zusammengesetzten Zeichen, so zum Beispiel</p>
          
<blockquote>
Hund + Mund = bellen<br>
Auge + Wasser = weinen<br>
Messer + Herz = Trauer<br>

</blockquote>
          
<p>&Uuml;bertragen bedeutete dies f&uuml;r Eisenstein, da&szlig; das zusammengesetzte Zeichen dem Zusammenprall zweier unabh&auml;ngiger Filmeinstellungen entsprach. Dies waren nicht mehr blo&szlig;e Bildzeichen, sondern ein <i>"Symbol im Werden"</i> <small>(BULGAKOWA, S. 60)</small>. Von jetzt an betrachtete er nicht mehr die Analogie von Wort und Bild, sondern die Analogie der semantischen Prozesse, sprach von der Rekonstruktion der Denkmechanismen: logisch, intellektuell, dialektisch. Das Filmbild - Ideogramm.</p>
          
<p>
            
<i>"Zwischen den in die Montage eintretenden intellektuellen Attraktionen liegt die &lt;&Auml;hnlichkeit&gt; nicht im Sinnlichen. Also im Absoluten und Nicht-&Auml;u&szlig;erlichen... Ein Barock-Christus und ein Holzklotz sind einander absolut un&auml;hnlich, bedeuten jedoch dasselbe. Balalaika und Menschewik sind sich nicht physisch, sondern abstrakt &auml;hnlich"</i> <small>(BULGAKOWA, S. 61)</small>
          
</p>
          
<p>Eine Einstellung entspricht einem Teilbegriff, die Begriffsbildung entsteht durch die Montageoperation. Somit wird aus einem statischen Proze&szlig; (einzelne Assoziation) ein dynamischer.</p>
          
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">OKTOBER 1927</h3>
<p>
              
<i>"Der Film beginnt mit der Schilderung der 'Beseitigung' des Zarismus - eine Menschenmenge rei&szlig;t im Moskau die Statue Alexanders III., das Symbol der Selbstherrschaft, vom Sockel. an der Front verbr&uuml;dern sich die russischen Soldaten mit feindlichen Truppen. Doch die Hoffnung des Volkes auf Brot, Friede und Land wird bald entt&auml;uscht: die provisorische Regierung erledigt weiterhin die Gesch&auml;fte des Kapitals, also wird auch der Krieg fortgesetzt. Im April kehrt Lenin aus der Schweiz nach St. Petersburg zur&uuml;ck; der riesigen Menschenmenge, die ihn am Bahnhof empf&auml;ngt, ruft er zu: 'Nieder mit der provisorischen Regierung! Alle Macht den Sowjets! Es lebe die sozialistische Revolution!' Im Juli veranstalten die Bolschewiki - f&uuml;r einen Aufstand ist es noch zu fr&uuml;h - in Petersburg eine friedliche Demonstration; die Regierungstruppen schie&szlig;en auf die Menschenmenge und lassen die Klappbr&uuml;cke &uuml;ber die Newa &ouml;ffnen, um das Stadtzentrum von den Arbeitervierteln abzuschneiden, das Hauptquartier der Bolschewistischen Partei wird &uuml;berfallen und gepl&uuml;ndert und viele Bolschewiki werden verhaftet - Lenin mu&szlig; erneut in die Illegalit&auml;t, er flieht nach Finnland. Inzwischen richtet sich der neue Ministerpr&auml;sident der provisorischen Regierung im Winterpalais des Zaren ein.<br>
Im September marschiert General Kornilow mit der 'Wilden Tatarendivision' und englischen Panzern - auf Petersburg f&uuml;r die Monarchie. Die Regierung ist machtlos, doch die Bolschewiki bewaffnen sich und stellen sich den Putschisten entgegen. Die 'Wilde Division' verbr&uuml;dert sich mit den revolution&auml;ren Arbeitern und Soldaten und Kornilow wird verhaftet. Jetzt beginnt der Petersburger Sowjet mit den Vorbereitungen zum bewaffneten Aufstand. Lenin kehrt zur&uuml;ck, und unter seinem Vorsitz beschlie&szlig;t das ZK der Bolschewistischen Partei, am 25.Oktober w&auml;hrend der Tagung des Allrussischen Sowjetkongresses die Partei den Generalstab f&uuml;r den Aufstand. Am Morgen des 25. Oktober erhalten die Revolution&auml;re Verst&auml;rkung durch den Panzerkreuzer 'Aurora'. Die Newa-Br&uuml;cke wird wieder geschlossen. Kerenski bittet vergeblich um die Unterst&uuml;tzung des sich neutral verhaltenden Kosakenregiments; im Wagen der US-Botschaft flieht er. Das Winterpalais wird nun von Kadetten und einem Frauenbataillon bewacht. Auf dem Sowjetkongre&szlig; versuchen Menschewiki und Sozialrevolution&auml;re (Karenskis Partei) abzuwiegeln, doch bei der Wahl zu neuen ZK erhalten die Bolschewiki die Mehrheit, und von der Front zur&uuml;ckgekehrte Garnisonen und Bataillone schlie&szlig;en sich ihnen an. Nun besetzen die Revolution&auml;re alle strategisch wichtigen Punkte der Stadt und umstellen das Winterpalais. Die Sch&uuml;sse der 'Aurora' auf das Palais sind das Signal zum Angriff. Die Verteidiger sind schnell &uuml;berw&auml;ltigt. Die aufst&auml;ndischen Massen dringen in den Palast der 1100 R&auml;ume. Die zur&uuml;ckgebliebenen Minister werden verhaftet. Am Ende des Film zeigen verschiedene Zifferbl&auml;tter mit verschiedenen Weltzeiten die Stunde der Revolution und Lenin verk&uuml;ndet auf dem Sowjetkongre&szlig;: 'Die Arbeiter- und Bauernrevolution ist vollendet.'"</i> <small>(WEISE, S. 62ff)</small>
            
</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
          
<h4>Bewegungsillusion und Filmsemantik</h4>
          
<p>Eine andere Entdeckung aus der Steinl&ouml;wensequenz war die M&ouml;glichkeit der Bewegungsillusion: drei statische Einstellungen ergaben eine zusammenh&auml;ngende Bewegung. Die gleiche Entdeckung bei einer Gewehrschu&szlig;sequenz aus dem PANZERKREUZER: das Nacheinanderschneiden von hellen und dunklen Einstellungen eines Maschinengewehrs erwecken den Eindruck des Schie&szlig;ens (Flickering). Oder - wieder im PANZERKREUZER - die Superposition zweier Einstellungen (Kuleschow-Effekt): eine Frau mit Zwicker und gleich darauf mit zerschlagenem Zwicker erwecken den Eindruck eines Schusses. </p>
          
<p>Wie gro&szlig; kann entsprechend die Diskrepanz zwischen zwei Einstellungen sein, damit dort noch eine Bewegungsillusion entsteht? Nicht mehr das Konfliktpotential zweier gegens&auml;tzlicher Einstellungen nahmen Eisensteins Interesse ein, sondern das, was dazwischen lag, die Schnittstelle. Er bezeichnet sie mit den Begriffen Intervall oder Ri&szlig; zwischen den Bildern.</p>
          
<p>Zum einen begreift er die Bewegungsillusion als <i>"technisch-optische Grundlage"</i> des Mediums Film &uuml;berhaupt: <i>"Das Bewegungs-Ph&auml;nomen des Films liegt darin, da&szlig; zwei unbewegliche Bilder eines bewegten K&ouml;rpers in aufeinanderfolgenden Positionen bei schnellem nacheinander Zeigen in Bewegung verschmelzen."</i> <i>"Der Bewegungs-Begriff (Empfindung) entsteht im Proze&szlig; der Superposition (&Uuml;berlagerung) des behaltenen Eindruckes der ersten Position des Objektes und der sichtbar werdenden Position des Objektes."</i> <small>(BULGAKOWA, S. 68)</small> Das gleiche gilt f&uuml;r das Empfinden einer Raumtiefe im Film durch die &Uuml;berlagerung zweidimensional divergierender Einstellungen. </p>
          
<p>Ebenso betrachtet Eisenstein die Verdichtung semantischer Prozesse: <i>"Das Entstehen neuer Begriffe und Anschauungen im Konflikt zwischen &uuml;blicher Vorstellung und einzelner Darstellung betrachte ich ebenfalls als Dynamik - Dynamisierung der Anschauungstr&auml;gheit..."</i> <small>(BULGAKOWA, S. 68f)</small>
</p>
          
<p>Olga Bulgakowa beschreibt dies folgenderma&szlig;en: <i>"Intervall ist nicht nur ein Ri&szlig;, der die Rezeption der Bewegung im Film ins Bewu&szlig;tseim r&uuml;ckt, die Risse f&ouml;rdern die Semantisierung im hohen Ma&szlig;e. Denn durch das Akzentuieren des Risses - kein Zeigen eines feuernden Maschinengewehres, sondern &Uuml;berlagerung dunkler und heller Einstellungen - wird der Begriff des Schie&szlig;ens verdeutlicht, es bleibt nicht bei der Darstellung."</i> <small>(BULGAKOWA, S. 70)</small>
Das Intervall steht f&uuml;r das Spiel zwischen Anwesenheit-Abwesenheit, Vergangenem-Zuk&uuml;nftigem. Dadurch entsteht eine spannungsbildende Polarit&auml;t.</p>
          
<p>Eisenstein schafft Bewegungsillusion auch dort, wo Objekte sich nicht bewegen k&ouml;nnen. In der eingangs beschriebenen Sequenz aus OKTOBER sieht man Kerenski die Treppe hinaufsteigen. Trotzdem bleibt er selbst konstant, steigt immer wieder die Treppe hinauf, tats&auml;chlich scheinen sich nur die ihn kr&ouml;nenden Statuen zu bewegen. </p>
          
<p>Eine andere Sequenz aus OKTOBER (Aktrolle 3), die "G&ouml;tter"sequenz, zeigt noch einmal deutlich Eisensteins Anspruch, mit Bildassoziationen den Zuschauer auf einer intellektuellen Ebene zu erreichen. (Sequenz entspricht Einstellungen 234-259, laut Kapitel 2.2) - durch Bild-Wort-Spiele, Ideogramme, Bewegungsillusion.</p>
          
<p>
            
<i>"Kornilows Marsch auf Petrograd geschah unter dem Banner &lt;f&uuml;r Gott und Vaterland&gt;. Wir versuchen hier, den religi&ouml;sen Hintergrund dieser Episode rationalistisch aufzudecken. Eine Reihe von religi&ouml;sen Bildern, angefangen von einer pr&auml;chtigen Barockstatue Jesu bis hin zu einem Eskimo-Gott, wurden zusammengeschnittten. Der Widerspruch bestand in diesem Fall zwischen der Idee von Gott und seiner Symbolisierung. W&auml;hrend bei der zuerst gezeigten Statue Idee und Bild vollkommen identisch erscheinen, entfernen sich die beiden Elemente mit jedem weiteren Bild immer mehr voneinander. Indem wir die Bezeichnen &lt;gott&gt; weiter aufrechterhalten, entfernen sich die Bilder zunehmend von unserer Vorstellung von Gott, so da&szlig; dies unweigerlich zu eigenen Ruckschl&uuml;ssen &uuml;ber die wahre Natur von Gottheiten f&uuml;hren mu&szlig;. In diesem Fall versucht eine Kette von Bildern eine rein intellektuelle Schlu&szlig;folgerung zu erzeugen. Sie ergibt sich nach und nach aus dem Konflikt einer bestimmten Vorstellung (von Gott) mit ihrer allm&auml;hlichen Diskreditierung."</i> <small>(REISZ, S. 32)</small>
          
</p>
          
<p>
            
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/goetter.jpg" align="left" height="230" width="440">
          </p>
          
<p>Die "G&ouml;tter"sequenz endet damit, da&szlig; die Einstellung eines barocken Jesus (der in den kreisf&ouml;rmigen Strahlen seines Heiligenschein zu explodieren scheint) mit einer eierf&ouml;rmigen Maske von Uzume, der Gottheit von Mirth, in kurzen Abst&auml;nden unterschnitten wird. <i>"Der Widerstreit zwischen der geschlossenen Eiform und dem sehr graphischen Sternkranz erzeugte den Effekt einer sich st&auml;ndig wiederholenden Explosion - einer Bombe oder eines Schrapnells"</i> <small>(REISZ, S. 34)</small> - der &Uuml;bergang in eine Bewegungsillusion.<br>
Damit wird aber auch deutlich, wie schwierig es im Einzelfall f&uuml;r den Zuschauer sein kann, Eisensteins angedeutete Gedankeng&auml;nge nachzuvollziehen. Vieldeutige oder stark spezialisierte "Begriffe" bergen immer die Gefahr des Miss- oder Unverst&auml;ndnisses. Eisenstein setzt letztlich stets ein <i>"korrespondierendes sprachliches Paradigma"</i> voraus.</p>
        
</subsubsection>
      
</subsection>
      
<subsection title="6. Eisensteins Kategoriesierung der Montage - Zusammenfassung">
        
<p>In seinem Artikel "Die vierte Dimension im Film" fa&szlig;t Eisenstein seine Unterscheidung der verschiedenen Kategorien der Montage noch einmal zusammen. Er differenziert zwischen der metrischen, der rhythmischen, der tonalen, der obertonalen und der intellektuellen Montage (BULGAKOWA, S. 78f):</p>
        
<p>-Eine metrische Montage, die sich auf eine "primitive" Rhythmusempfindung gr&uuml;ndet. Sonst sind die Abschnitte unabh&auml;ngig, zwischen ihnen gibt es keine Verbindung, nur ihre L&auml;nge ist ausschlaggebend, die Spannung wird erzeugt durch gleichm&auml;&szlig;ige mechanische K&uuml;rzung der L&auml;ngen: doppelt, dreifach usw.).
entspr&auml;che der Kin&auml;sthesie</p>
        
<p>-Eine rhythmische Montage, der ein differenziertes Rhythmusempfinden zugrunde liegt. Die Relation zwischen der L&auml;nge des Abschnitts und dem Charakter der darin abgebildeten Bewegungen dient als Grundlage der Verbindung. Intensivierung kann durch Rhythmuswechsel der Bewegungen erreicht werden: vom (gleichm&auml;&szlig;igen) Soldatenschritt zum (Chaotischen) Kinderwagenholpern auf der Odessaer Treppe (Anm.: in PANZERKREUZER POTEMKIN)</p>
        
<p>-Eine tonale Montage. Hier ist das physiologische Empfinden verschiedener physischer Parameter des Bildes und ihr <i>"Vibrieren"</i> (wie Eisenstein schreibt) von Einstellung zu Einstellung ausschlaggebend. Beleuchtung, Optik, Konturen, Oberfl&auml;chenbeschaffenheit der Objekte werden zu physiologischen Individualisierungsmerkmalen einer Einstellung, zu Dominanten, die den <i>"affektiven emotionellen Klang"</i> bestimmen. Intensivierung wird erreicht durch die Bestimmung einer Dominante und ihrer Kondensieren aus einer Einstellung in die andere. Als Beispiel f&uuml;hrt Eisenstein die Lichtschwankungen (von hellgrau zu bleischwarz) in der Episode "Ernte" aus DAS ALTE UND DAS NEUE an oder von dunkelgrau zum nebligwei&szlig; in den D&auml;mmerungen von POTEMKIN;</p>
        
<p>-obertonale Montage (alle Erreger z&auml;hlen);
entspr&auml;chen verschiedenen Ebenen der Perzeption</p>
        
<p>-intellektuelle Montage (&Uuml;berf&uuml;hrung der physiologisch wirkenden Erreger auf die Ebene der Vorstellung und des Begriffs).
korrespondiert mit der kognitiven Ebene, der Ebene der konzeptualen Erkenntnis</p>
        
<p>Um nocheinmal auf den Ausgangspunkt, den Vergleich zwischen Griffith und Eisenstein, zur&uuml;ckzukommen: Eisenstein vertritt im Vergleich zur dramatischen Montage z. B. eines D. W. Griffith eine v&ouml;llig kontr&auml;re Auffassung von Narration, Raum- und Zeitdarstellung. </p>
        
<p>Raum und Zeit werden nicht nach ihrem Realit&auml;tsgehalt, sondern nach ihrem Bedeutungsgehalt dargestellt. Sie bleiben abstrakt und dienen nicht der handlungsgebundenen, sondern der begrifflichen Orientierung. R&auml;ume werden dekonstruiert, Zeitabl&auml;ufe gerafft oder um ein vielfaches gedehnt (z. B. durch mehrfaches Hintereinanderschneiden der gleichen oder aus verschiedenen Blickwinkeln gedrehten Einstellung). Anschl&uuml;sse im Sinne einer kausalen Kontinuit&auml;t werden negiert.</p>
        
<p>Seine Handlungstr&auml;ger sind nicht Subjekte, mit deren Sehperspektive sich die Zuschauer identifizieren k&ouml;nnten. Oft haben Eisensteins Filme einen starken Massencharakter, wie z. B. in STREIK oder OKTOBER, in denen Individuen praktisch ausgeklammert sind, treten jedoch Einzelpersonen in Aktion, sieht er sie nicht als Individuen oder Charaktere, er verwendet sie als Typen, um ihres Ausdrucks oder des Bildes wegen.    </p>
        
<p>Vor allem transportiert Eisenstein die Inhalte nicht &uuml;ber die Geschichte oder Handlung - Reiz und Empfinden bzw. Erkenntnis (man k&ouml;nnte sie auch mit Katharsis bezeichnen) entstehen aus der Methode heraus, und ergeben sich aus der Art der Verkettung von Einzelelementen. Dabei ist der Zuschauer enorm gefordert - seine Strukturierungsarbeit l&auml;&szlig;t den Film im Prinzip erst entstehen. Oder wie es Oksana Bulgakowa formuliert: <i>"Der Proze&szlig; der Erz&auml;hlung und der Bedeutungsbildung im Montagefilm ist eine dynamische, von der Rezeption nicht zu trennende Struktuierungsarbeit des Zuschauers."</i> <small>(BULGAKOWA, S. 76)</small>
</p>
      
</subsection>
      
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Sergei Eisenstein: "Dickens, Griffith und wir"</h3>
<p>In seinem Artikel DICKENS, GRIFFITH UND WIR weist Eisenstein unter anderem auf die literarische Tradition der Montage hin - und geht zur&uuml;ck bis zu Shakespeare. Griffith hatte bereits selbst formuliert, da&szlig; vor allem Dickens ihn gepr&auml;gt h&auml;tte. Bei seiner Recherche st&ouml;&szlig;t Eisenstein auf ein geradezu "originelles &lt;traktat&gt;" zu Beginn des 17. Kapitels von OLIVER TWIST <i>"&uuml;ber die Prinzipien eben jenes Motageaufbaus eines Sujets [...], der hier bei Dickens so bestrickend gehandhabt wird und von Dickens aus in Griffith's Methode Eingang fand."</i> <small>(EISENSTEIN, S. 94)</small>:</p>
<p>
          
<i>"Auf den Brettern ist es bei allen guten mord- und totschlagreichen St&uuml;cken &uuml;blich, die tragischen und komischen Szenen so regelm&auml;&szlig;ig aufeinander folgen zu lassen, wie das Rote und Wei&szlig;e in einer wohldurchwachsenen Speckseite. Der Held sinkt unter dem Gewicht seiner Ketten und seines Ungl&uuml;cks auf dem &auml;rmlichen Strohlager nieder, und in dem n&auml;chsten Auftritt erfreut sein treuer Gef&auml;hrte, der von alledem nichts wei&szlig;, die Zuschauer mit einem komischen Gesang. Mit klopfendem Herzen sehen wie die Heldin in den Krallen eines stolzen und grausamen Barons; ihre Tugend, ihr Leben - beides ist in Gefahr, und bereits hat sie den Dolch gezogen um die Tugend auf Kosten des Lebens zu retten; aber in demselben Augenblick, wo unsere Erwartungen auf das h&ouml;chste gespannt sind, geht es an ein Musizieren, und wir sind geradewegs in die weiten Hallen eines Schlosses versetzt, wo ein grauk&ouml;pfiger Kastellan einen lustigen Chor mit einer noch lustigeren Gesellschaft von Untergebenen auff&uuml;hrt; und diese leiern, ohne sich im mindesten an den Ort zu kehren, mag er nun eine Kirche oder ein Palast sein, ohne Unterla&szlig; ihre Jodelweisen herunter.
Solche Wechsel m&ouml;gen abgeschmackt erscheinen, aber sie sind gar nicht so unnat&uuml;rlich, wie man auf den ersten Blick glauben sollte. Die &Uuml;berg&auml;nge von einer wohlbesetzten Tafel	zum Sterbebette, vom Trauergewande zum Festtagsschmuck, die man im wirklichen Leben bemerken kann, sind um keinen Jora weniger auffallend, und der Unterschied besteht nur darin, da&szlig; wir im letzteren Falle Schauspieler, im ersteren Zuschauer sind. Der Mime auf dem Schauplatz seines Wirkens achtet nicht auf die gewaltsamen &Uuml;berg&auml;nge und auf das j&auml;he Erschlie&szlig;en der Gef&uuml;hle und Leidenschaften, w&auml;hrend die schaulustige Menge gar bald &uuml;ber Verletzung der inneren Wahrheit zu klagen geneigt ist.
Pl&ouml;tzlicher Szenenwechsel und rasche Ver&auml;nderung in Ort und Zeit haben bei Gebilden der Phantasie nicht nur ein langj&auml;hriges Herkommen f&uuml;r sich, sondern werden von vielen sogar als die gr&ouml;&szlig;te Kunst des Autors betrachtet, die von derartigen Kritikern haupts&auml;chlich nach den Verlegenheiten gew&uuml;rdigt wird, in denen der Held das Gedichtes am Ende eines jeden Kapitels bleibt - und so k&ouml;nnte vielleicht diese kurze Einleitung als unn&ouml;tig erscheinen."</i> <small>(EISENSTEIN, S. 94f)</small>
        
</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
      
<p>&nbsp;</p>
<hr>
<h3>Literatur / Quellenangaben</h3>
<ul>
<li>
<small>Bulgakowa, Oksana: Sergej Eisenstein - drei Utopien, Architekturentw&uuml;rfe zur Filmtheorie; PotemkinPress; Berlin; 1996</small>
</li>
<li>
<small>Eisenstein, Sergei: Gesammelte Aufs&auml;tze I; Im Verlag der Ache; Z&uuml;rich</small>
</li>
<li>
<small>Kaufmann, Lilli (Hrsg.); Sergei Eisenstein: &Uuml;ber mich und meine Filme; Henschelverlag Berlin; 1975</small>
</li>
<li>
<small>Reisz, Karel und Millar, Gavin: Geschichte und Technik der Filmmontage; Filmlandpresse; M&uuml;nchen; 1988</small>
</li>
<li>
<small>See&szlig;len, Georg: Die anderen M&ouml;glichkeiten des Kinos - Zu Sergej Eisensteins 100. Geburtstag; in: epd Film 12/97, 14. Jahrgang; 1997; Frankfurt</small>
</li>
<li>
<small>Weise, Eckhard: Sergei M. Eisenstein - in Selbstzeugnissen und Bilddokumenten; Rohwolt Taschenbuch Verlag; Hamburg; 1975</small>
</li>
</ul>
    
<br>
<hr size="1" noshade="noshade">
<h3 >Inhalt:</h3>
<ul >
<li>
<small><?php echo CHtml::link('Einführung', array('site/page', 'view'=>'docs.montage.intro'));?></small>
</li>
<li>
<small><?php echo CHtml::link('MTV-&Auml;sthetik&lt;: 128 Cuts per Minute - und kein Ende in Sicht', array('site/page', 'view'=>'docs.montage.mtv'));?></small>
</li>
<li>
<small><?php echo CHtml::link('IA28 - Sergej Eisensteins Weg zur Intellektuellen Montage', array('site/page', 'view'=>'docs.montage.eisenst'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Montageaspekte in den Filmen Wong Kar-Weis', array('site/page', 'view'=>'docs.montage.wong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Die Welt im Querschnitt: Walter Ruttmann und sein Montagefilm BERLIN - DIE SINFONIE DER GROSSTADT', array('site/page', 'view'=>'docs.montage.ruttmann'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Gewaltige Montage-Monumente: Sam Peckinpah', array('site/page', 'view'=>'docs.montage.peckinpah'));?></small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p >
<font size="-2">Impressum</font>
</p>
<p>
<font  size="-2">"Zwischen den Bildern - Aspekte der Filmmontage" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und erscheint begleitend zur Filmreihe im Wintersemester 97/98.
Vervielf&auml;ltigung und Kopieren nur mit vorheriger Genehmigung des AFK</font>
</p>
<p>
<font  size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstr.12<br>
76128 Karlsruhe</font>
</p>
<p>
<font  size="-2">Mitarbeiter an dieser Ausgabe:</font>
</p>
<p>
<font  size="-2">A. G&uuml;nter<br>
M. Hirasaka<br>
M. Nagenborg<br>
H. Stiens<br>
P. Stock</font>
</p>
<p>
<font  size="-2">HTML-Aufbereitung:  Michael Haist</font>
</p>
