<p align="right">
<small>
<?php echo CHtml::link('&gt;&gt;&gt; Begleitheft \'Die Filme von Mike Leigh\'', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
<br>
</small>
</p>
      
<p>
        
<img  alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/intro/cutterin.jpg" align="right" height="316" width="200">
        <i>"Filmmontage ist eine Sammelbezeichnung f&uuml;r einen komplexen Vorgang, der den Film in seinem Ablauf strukturiert, seine visuellen und akustischen Elemente ausw&auml;hlt, anordnet und sie organisiert, indem sie durch Schnitt gegen&uuml;bergestellt, aneinandergereiht und/oder in ihrer Dauer begrenzt werden."</i> <small>(Hans Beller in "Handbuch der Filmmontage")</small>
      
</p>
      
<p>Mit diesen Worten umschreibt Hans Beller recht treffend die technisch-ordnenden Qualit&auml;ten des Filmschnitts:
einzelne Teile erhalten durch Zusammenf&uuml;gen einen Sinn, der oftmals &uuml;ber die urspr&uuml;ngliche Bedeutung der montierten Bilder und T&ouml;ne hinausgeht. Was in Bellers Beschreibung nicht anklingt, ist der gestalterische Aspekt der Montage, die M&ouml;glichkeit, auch noch nach Beendigung der Dreharbeiten kreativ in die Entstehung eines Fims einzugreifen und seine Aussage zu formen.</p>
      
<p>Mit unserer Filmreihe "Zwischen den Bildern - Aspekte der Filmmontage" und dem begleitenden Heft wollen wir nicht den Anspruch erheben, einen umfassenden &Uuml;berblick &uuml;ber die verschiedenen Stilmittel und Techniken der Montage zu geben. Genausowenig wollen wir einen vollst&auml;ndigen geschichtlichen Abri&szlig; &uuml;ber die Entwicklung des Filmschnitts darstellen.
Interessiert hat uns vielmehr der bewu&szlig;te Einsatz der Montage als pers&ouml;nliches Ausdrucksmittel des jeweiligen Filmemachers. Individuelle Motive und Wahrnehmung spielen hierbei eine gro&szlig;e Rolle im filmischen Verarbeitungsproze&szlig;. So lassen sich aus keinem der ausgew&auml;hlten Filme die Person und der pers&ouml;nliche Hintergrund des Filmemachers wegdenken.</p>
      
<p>In einem Disput zur Bedeutung der Montage im sowjetischen Film schrieb der Regisseur und Theoretiker Sergej Eisenstein:
<i>"Es gab in unserer Filmkunst eine Periode, in der die Montage "alles" galt. Jetzt geht eine Periode ihrem Ende zu, in der die Montage "nichts" gilt. Wir h&auml;ngen keinem der beiden Extreme an und halten f&uuml;r erforderlich, jetzt daran zu erinnern, da&szlig; die Montage ein ebenso notwendiger Bestandteil eines Filmwerkes ist wie alle anderen Elemente der filmischen Einwirkung. Nach dem Feldzug "f&uuml;r die Montage" und dem Sturm "gegen die Montage" ist es an der Zeit, ganz von neuem und unvoreingenommen an die Probleme der Montage heranzugehen."</i>
</p>
    
<br >
<hr size="1" noshade="noshade">
<h3 >Inhalt:</h3>
<ul >
<li>
<small><?php echo CHtml::link('Einführung', array('site/page', 'view'=>'docs.montage.intro'));?></small>
</li>
<li>
<small><?php echo CHtml::link('MTV-&Auml;sthetik&lt;: 128 Cuts per Minute - und kein Ende in Sicht', array('site/page', 'view'=>'docs.montage.mtv'));?></small>
</li>
<li>
<small><?php echo CHtml::link('IA28 - Sergej Eisensteins Weg zur Intellektuellen Montage', array('site/page', 'view'=>'docs.montage.eisenst'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Montageaspekte in den Filmen Wong Kar-Weis', array('site/page', 'view'=>'docs.montage.wong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Die Welt im Querschnitt: Walter Ruttmann und sein Montagefilm BERLIN - DIE SINFONIE DER GROSSTADT', array('site/page', 'view'=>'docs.montage.ruttmann'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Gewaltige Montage-Monumente: Sam Peckinpah', array('site/page', 'view'=>'docs.montage.peckinpah'));?></small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p >
<font size="-2">Impressum</font>
</p>
<p>
<font  size="-2">"Zwischen den Bildern - Aspekte der Filmmontage" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und erscheint begleitend zur Filmreihe im Wintersemester 97/98.
Vervielf&auml;ltigung und Kopieren nur mit vorheriger Genehmigung des AFK</font>
</p>
<p>
<font  size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstr.12<br>
76128 Karlsruhe</font>
</p>
<p>
<font  size="-2">Mitarbeiter an dieser Ausgabe:</font>
</p>
<p>
<font  size="-2">A. G&uuml;nter<br>
M. Hirasaka<br>
M. Nagenborg<br>
H. Stiens<br>
P. Stock</font>
</p>
<p>
<font  size="-2">HTML-Aufbereitung:  Michael Haist</font>
</p>

