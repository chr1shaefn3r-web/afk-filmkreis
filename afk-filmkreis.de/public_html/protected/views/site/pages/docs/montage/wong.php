<p align="right">
<small>
<?php echo CHtml::link('&gt;&gt;&gt; Begleitheft \'Die Filme von Mike Leigh\'', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Zwischen den Bildern</b></big>
<br>Aspekte der Filmmontage<br>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.montage.eisenst','docs.montage.ruttmann')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-wong"></a>
<h3>Montageaspekte in den Filmen Wong Kar-Weis</h3>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/wong/karwai.jpg" align="left" height="168" width="180">
Wenn man die letzten drei Filme Wong Kar-Wais grob auf ihre Haupthandlungsstr&auml;nge reduziert, gibt es in CHUNGKING EXPRESS zwei, in FALLEN ANGELS mindestens drei, und in HAPPY TOGETHER eine Liebesgeschichte. So sind es nicht mal so sehr die Geschichten, die seine Filme einzigartig machen, sondern die Art und Weise wie er diese erz&auml;hlt.</p>
      
<p>Zusammen mit seinem Kameramann Christopher Doyle und seinen Cuttern William Chang und Wong Ming-Lam schafft Wong Kar-Wai einen sehr pers&ouml;nlichen, un&uuml;blichen Erz&auml;hlstil, der stark darauf bedacht ist, die inneren Seelenzust&auml;nde seiner Figuren wiederzuspiegeln. Meistens sind diese ungl&uuml;cklich verliebt, Momente der Melancholie und Sehnsucht beherschen ihr entfremdetes, einsames Gro&szlig;stadtleben, ob nun in Hong Kong oder Buenos Aires, wobei sie jedoch versuchen irgendwie ihre Individualit&auml;t zu bewahren und sich selbst nicht zu verlieren. Das Aufzeigen bzw. die filmische Umsetzung dieser Stimmungs- und Gef&uuml;hlswelten, pa&szlig;t Wong Kar-Wai so sehr der Handlung an, da&szlig; eine Unterscheidung	zwischen Form und Inhalt sich quasi ausschlie&szlig;t. Im Unterschied zu den meisten Spielfilmen, wo der schnitterz&auml;hltechnische Umgang mit strukturbildenden Pfeilern wie Zeit und Raum haupts&auml;chlich durch die fortschreitende Handlung gepr&auml;gt ist, werden diese Faktoren erst durch die subjektiven Wahrnehmungswirklichkeiten der Figuren bestimmt. Momente k&ouml;nnen zu Ewigkeiten werden, w&auml;hrend andere Situationen, die sich &uuml;ber l&auml;ngere Zeitr&auml;ume abspielen, wie in einem Rausch vorr&uuml;berziehen. Das innere Erleben kehrt sich nach au&szlig;en und umgekehrt.
In der Montage zeigt sich dies z.B im &Uuml;berlappen der Ton-und Bildspur, dem Umgang mit verschiedenen Bildgeschwindigkeiten vom Zeitraffer bis zum eingefrorenem Standbild, oder dem Wechsel von s/w und Farbe. Der Einsatz von einer oftmals sehr agilen, rastlosen Handkamera, extremer Weitwinkelobjektive, oder der bewu&szlig;te Umgang mit Unsch&auml;rfen, versetzen neben anderen Stilmerkmalen den Zuschauer  in einen tranceartigen, zeitlosen Film-Raum.
Da CHUNGKING EXPRESS anfangs auf drei statt auf zwei Episoden konzipiert war, entstand schon w&auml;hrend der Dreharbeiten zu dem Film Material zu FALLEN ANGELS. Es w&auml;re interessant zu erfahren, ob der Entschlu&szlig;, den Film anders zu strukturieren wie urspr&uuml;nglich geplant, erst am Schnitttisch entstand. Dies zeigt jedoch deutlich wie stark die Filme durch die Montage gepr&auml;gt sind.</p>
      
<p>Die Filme sind sich formal wie auch inhaltlich sehr &auml;hnlich, wobei die Grundstimmung von CHUNGKING EXPRESS viel heiterer wirkt als bei FALLEN ANGELS. In beiden F&auml;llen werden die Figuren am Anfang in den Film ohne gro&szlig;e Erkl&auml;rungen hineingeworfen. Durch schnelle Schnittfolgen von Handkamerafahrten und Schwenks, entsteht eine Stimmung der Hektik und Orientierungslosigkeit.
Die Hektik wird am Anfang von CHUNGKING EXPRESS schlagartig unterbrochen, als Polizist 223 bei seiner Verfolgung die Frau ohne Namen anrempelt. Im selben Moment wird nur f&uuml;r ein paar Bilder lang eine Uhr eingeblendet, gleich darauf: eine Halbtotale in der sich beide f&uuml;r einen Augenblick ansehen, das Bild bewegt sich nur noch langsam, einzelbildweise, w&auml;hrend die Stimme von 223 aus dem Off prophezeit: <i>"in 55 Stunden werde ich mich in diese Frau verlieben"</i>. Abblende. In der n&auml;chsten Einstellung sieht man 223 ohne Schnitt knapp eine Minute lang telefonieren.</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/wong/chungk.jpg" align="left" height="142" width="200">Solche pl&ouml;tzlichen Rhytmuswechsel, hier unter anderem durch verschiedene Bildgeschwindigkeiten und unerwarteten Einstellungseinw&uuml;rfen (die Uhr) erreicht, verleihen dem Film eine unstetige, leicht nerv&ouml;se Struktur, wie bei einem avantgardistischen Jazzst&uuml;ck.
Die Schnittfolgen sind oftmals sehr schnell und vieles verfl&uuml;chtigt sich, ohne da&szlig; es die meisten Zuschauer wohl bemerken werden. So gibt es beispielsweise in CHUNGKING EXPRESS eine Szene, wo 223 eine Rolltreppe irgendeiner Bahnstation hochl&auml;uft, worauf dann eine Totale folgt, auf der ein anderer Polizist von einem Br&uuml;ckengel&auml;nder runtersieht. Viel sp&auml;ter wird sich herausstellen, da&szlig; sich die zweite Episode des Films um diesen Polizisten handelt. Seine Freundin, der er nachtrauert, eine Stewardess, wird auf &auml;hnlich beil&auml;ufige Art in einer Szene die an einem Flughafen spielt "eingef&uuml;hrt". Manche Einstellungen erf&uuml;llen so suggestiv den Zweck zuk&uuml;nftige Geschehnisse oder Personen vorwegzunehmen, wodurch eine Gleichwertigkeit der Ereignisse erreicht wird, ob und wann sie sich auch immer abspielen m&ouml;gen. Nicht alle Schnittfolgen m&uuml;ssen jedoch auf Anhieb so rational erfa&szlig;bar sein wie hier, da diese auch meist auf einer emotionalen Ebene funktionieren.</p>
      
<p>Die Montage dient wie schon Anfangs erw&auml;hnt dazu die Gef&uuml;hls- und Stimmungslagen der Figuren auszudr&uuml;cken, was jedesmal anders geschehen kann, gleichzeitig mu&szlig; sie aber darauf achten, alles stimmig zusammenzuf&uuml;gen, egal wie unterschiedlich von Tempo und Struktur die Einzelsequenzen auch sein m&ouml;gen. Ein Gro&szlig;teil dieser geschlossenen Wirkung  wird in den Filmen durch einen stets wohlausgewogenen Rhytmus innerhalb der Gesamtstruktur erreicht.	Nach jeder rastlosen Phase folgt eine ruhige Einstellung oder Szene, die den Zuschauer Zeit zum Verweilen l&auml;sst. Mal geschieht dies durch den Wechsel von wild geschnittenen, bewegten Handkameraeinstellungen zu festen Stativaufnahmen, die zum Teil minutenlang ohne einen Schnitt auskommen, oder es werden beispielsweise verschiedene Einstellungen aus derselben Kameraperspektive oder Schu&szlig;richtung aneinandergereiht die in ihrer Gesamtheit einen "ruhigen" Block ergeben:</p>
      
<p>In einer Szene von FALLEN ANGELS sieht man den Killer, nach einem seiner Auftr&auml;ge, in einen Bus steigen, wo er &uuml;beraschenderweise einen alten Schulkameraden trifft. Die Schie&szlig;erei war gepr&auml;gt durch eine unruhige Montage, wohingegen jetzt die Kamera minutenlang in einer lediglich am Anfang kurz unterbrochenen Halbnahen, auf der beide Personen zu sehen sind, verharrt.
Die Tonspur ist ein weiteres dramaturgisches Element, welches erheblich zur Geschlossenheit beitr&auml;gt. Oftmals verbinden Musik und die als Off-Stimme eingesetzten lakonischen Selbstkommentare der Figuren mehrere Einstellungsfolgen, die zeitlich wie r&auml;umlich v&ouml;llig auseinanderliegen k&ouml;nnen.
In CHUNGKING EXPRESS gibt es eine Szene, wo Faye in die Wohnung vom Polizisten 663, in dem sie sich verliebt hat, einbricht. Ein Song von den Cranberries (auf chinesisch!) ert&ouml;nt. In rascher Abfolge und mehreren Handkameraeinstellungen sieht man nun, w&auml;hrend die ganze Zeit der Song l&auml;uft, wie sie die Sachen in seiner Wohnung ver&auml;ndert</p>
      
<p>Sie gie&szlig;t unter anderem neue Goldfische in sein Aquarium, wechselt die Ettiketten seiner Thunfischdosen usw. Am Ende tut sie Schlaftabletten in eine Wasserflasche, Schnitt, Faye steht wieder hinter ihrer Imbi&szlig;theke und schaut auf die Uhr. Schnitt, man sieht Polizist 663 zuerst von der Flasche trinken, darauf schl&auml;ft er ein. Schnitt, Faye steht wieder in der Wohnung und nimmt weitere Ver&auml;nderungen vor, ihre Kleidung wechselt dabei fast unmerklich von einer Einstellung zur anderen, in der n&auml;chsten tr&auml;gt sie wieder die gleiche Kleidung wie vorher, dann wieder nicht, womit deutlich wird, da&szlig; sich all dies in gr&ouml;&szlig;eren Zeitabschnitten abspielt. Sie verl&auml;&szlig;t die Wohnung, der Song h&ouml;rt auf.</p>
      
<p> In Szenenbl&ouml;cken wie dieser werden die Einstellungen im Rhythmus der Musik geschnitten. Im Gegensatz zu manchen Hollywoodfilmen, wo solche &auml;hnlichen, videoclipartig montierten Sequenzen eher f&uuml;r die kommerzielle Auswertung des Songs gemacht zu sein scheinen, steht in diesem Fall die dramaturgische Funktion nicht nur zweckm&auml;&szlig;ig im Vordergrund. Fast alle Aktionen die Faye innerhalb dieses Szenenblocks macht, werden sp&auml;ter in einem anderen Erz&auml;hlabschnitt nochmal aufgegriffen, wo sich Polizist 663 nach langer Zeit dar&uuml;ber wundert, da&szlig; sich vieles in seiner Wohnung ver&auml;ndert hat. Vor dem Szenenblock mit dem Cranberries-St&uuml;ck h&ouml;rte Faye permanent nur ein und denselben Song: "California Dreamin". Als Zuschauer h&ouml;rt man ihn dadurch auch st&auml;ndig. In dem Moment wo sie selbst aktiv wird, und in ihrem Leben eine &Auml;nderung herbeif&uuml;hrt, wobei sie die Lebensumst&auml;nde eines Anderen ver&auml;ndert, ert&ouml;nt auch eine andere Musik. </p>
      
<p>Diesen charakteristischen Einsatz von Musik, der sich direkt auf die Personen bezieht, f&uuml;hrt Wong Kar-Wai in FALLEN ANGELS so weit, da&szlig; fast jede Person ein oder zwei f&uuml;r ihn passende Songs  bekommt. Diese Songs begleiten die Figuren in bestimmten Situationen und kommentieren gleichzeitig deren Gef&uuml;hlslage. Jedesmal wenn der Killer in FALLEN ANGELS einen neuen Auftrag ausf&uuml;hrt, h&ouml;rt man ein bestimmtes Trip-Hop St&uuml;ck, das zusammen mit den &auml;hnlich geschnittenen Bilderfolgen als schon mal dagewesenes Motiv auftaucht. Durch diesen Wiedererkennungswert bekommt die Musik zus&auml;tzlich die Funktion von Orientierungspunkten, nach der Bilder und Szenen eingeordnet und geschnitten werden k&ouml;nnen.</p>
      
<p>Seinem unvergleichlichen Erz&auml;hlstil ist Wong Kar-Wai auch in seinem neuesten Film HAPPY TOGETHER treu geblieben. In einer Szene sinniert Lai Yiu-Fai in Buenos Aires, wie es wohl gerade in Hong Kong w&auml;re, und stellt dabei fest, da&szlig; die Stadt genau am anderen Ende der Erdkugel liegt. Schnitt, man sieht verschiedene Zeitrafferaufnahmen von Hong Kong auf dem Kopf. </p>
      
<p>Die Grundstimmung des Films ist in Vergleich zu FALLEN ANGELS noch schwerm&uuml;tiger geworden und der anarchistische Humor, der den vorrangegangenen Filmen ein Moment der Leichtigkeit verlieh, fehlt fast v&ouml;llig. Die Montage passt sich dementsprechend diesem Zustand an. Der Grundrhythmus ist langsamer geworden. Insgesamt gibt es mehr lange, wenig unterbrochene Passagen auch mehr in s/w. Die Rastlosigkeit der Figuren wird nicht mehr so sehr durch schnelle Schnittfolgen unterstrichen und statt charakterisierender Musikst&uuml;cke werden die Figuren vom Tango begleitet.</p>
    
<br>
<hr size="1" noshade="noshade">
<h3 >Inhalt:</h3>
<ul >
<li>
<small><?php echo CHtml::link('Einführung', array('site/page', 'view'=>'docs.montage.intro'));?></small>
</li>
<li>
<small><?php echo CHtml::link('MTV-&Auml;sthetik&lt;: 128 Cuts per Minute - und kein Ende in Sicht', array('site/page', 'view'=>'docs.montage.mtv'));?></small>
</li>
<li>
<small><?php echo CHtml::link('IA28 - Sergej Eisensteins Weg zur Intellektuellen Montage', array('site/page', 'view'=>'docs.montage.eisenst'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Montageaspekte in den Filmen Wong Kar-Weis', array('site/page', 'view'=>'docs.montage.wong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Die Welt im Querschnitt: Walter Ruttmann und sein Montagefilm BERLIN - DIE SINFONIE DER GROSSTADT', array('site/page', 'view'=>'docs.montage.ruttmann'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Gewaltige Montage-Monumente: Sam Peckinpah', array('site/page', 'view'=>'docs.montage.peckinpah'));?></small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p >
<font size="-2">Impressum</font>
</p>
<p>
<font  size="-2">"Zwischen den Bildern - Aspekte der Filmmontage" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und erscheint begleitend zur Filmreihe im Wintersemester 97/98.
Vervielf&auml;ltigung und Kopieren nur mit vorheriger Genehmigung des AFK</font>
</p>
<p>
<font  size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstr.12<br>
76128 Karlsruhe</font>
</p>
<p>
<font  size="-2">Mitarbeiter an dieser Ausgabe:</font>
</p>
<p>
<font  size="-2">A. G&uuml;nter<br>
M. Hirasaka<br>
M. Nagenborg<br>
H. Stiens<br>
P. Stock</font>
</p>
<p>
<font  size="-2">HTML-Aufbereitung:  Michael Haist</font>
</p>
