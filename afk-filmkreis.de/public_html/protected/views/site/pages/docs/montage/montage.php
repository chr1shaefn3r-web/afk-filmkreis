<p align="right">
<small>
<?php echo CHtml::link('&gt;&gt;&gt; Begleitheft \'Die Filme von Mike Leigh\'', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Zwischen den Bildern</b></big>
<br>Aspekte der Filmmontage<br>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-intro"></a>
<h3>Einf&uuml;hrung</h3>
      
<p>
        
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/intro/cutterin.jpg" align="right" height="316" width="200">
        <i>"Filmmontage ist eine Sammelbezeichnung f&uuml;r einen komplexen Vorgang, der den Film in seinem Ablauf strukturiert, seine visuellen und akustischen Elemente ausw&auml;hlt, anordnet und sie organisiert, indem sie durch Schnitt gegen&uuml;bergestellt, aneinandergereiht und/oder in ihrer Dauer begrenzt werden."</i> <small>(Hans Beller in "Handbuch der Filmmontage")</small>
      
</p>
      
<p>Mit diesen Worten umschreibt Hans Beller recht treffend die technisch-ordnenden Qualit&auml;ten des Filmschnitts:
einzelne Teile erhalten durch Zusammenf&uuml;gen einen Sinn, der oftmals &uuml;ber die urspr&uuml;ngliche Bedeutung der montierten Bilder und T&ouml;ne hinausgeht. Was in Bellers Beschreibung nicht anklingt, ist der gestalterische Aspekt der Montage, die M&ouml;glichkeit, auch noch nach Beendigung der Dreharbeiten kreativ in die Entstehung eines Fims einzugreifen und seine Aussage zu formen.</p>
      
<p>Mit unserer Filmreihe "Zwischen den Bildern - Aspekte der Filmmontage" und dem begleitenden Heft wollen wir nicht den Anspruch erheben, einen umfassenden &Uuml;berblick &uuml;ber die verschiedenen Stilmittel und Techniken der Montage zu geben. Genausowenig wollen wir einen vollst&auml;ndigen geschichtlichen Abri&szlig; &uuml;ber die Entwicklung des Filmschnitts darstellen.
Interessiert hat uns vielmehr der bewu&szlig;te Einsatz der Montage als pers&ouml;nliches Ausdrucksmittel des jeweiligen Filmemachers. Individuelle Motive und Wahrnehmung spielen hierbei eine gro&szlig;e Rolle im filmischen Verarbeitungsproze&szlig;. So lassen sich aus keinem der ausgew&auml;hlten Filme die Person und der pers&ouml;nliche Hintergrund des Filmemachers wegdenken.</p>
      
<p>In einem Disput zur Bedeutung der Montage im sowjetischen Film schrieb der Regisseur und Theoretiker Sergej Eisenstein:
<i>"Es gab in unserer Filmkunst eine Periode, in der die Montage "alles" galt. Jetzt geht eine Periode ihrem Ende zu, in der die Montage "nichts" gilt. Wir h&auml;ngen keinem der beiden Extreme an und halten f&uuml;r erforderlich, jetzt daran zu erinnern, da&szlig; die Montage ein ebenso notwendiger Bestandteil eines Filmwerkes ist wie alle anderen Elemente der filmischen Einwirkung. Nach dem Feldzug "f&uuml;r die Montage" und dem Sturm "gegen die Montage" ist es an der Zeit, ganz von neuem und unvoreingenommen an die Probleme der Montage heranzugehen."</i>
</p>
    
<a name="sec-mtv"></a>
<h3>&gt;MTV-&Auml;sthetik&lt;<br>
<small>128 Cuts per Minute - und kein Ende in Sicht</small>
</h3>
      
<p>
        
<i>"Get MTV off the Air!"</i> <small>(Dead Kennedys)</small>
      
</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/mtv/mtvlogo.gif" align="left" height="142" width="180">Einen Film nachzusagen, er habe eine MTV-&Auml;sthetik, ist meist ein Vorwurf, selten eine Beschreibung. Im folgenden Text soll nachgegangen werden, was das Wort eigentlich bezeichnet und warum es im Mund des Cineasten so einen seltsamen Beigeschmack hinterl&auml;&szlig;t.</p>
      
<p>Statt &gt;MTV-&Auml;sthetik&lt; sagt der Eine oder die Andere auch &gt;Videoclip-&Auml;sthetik&lt;. Die Geschichte des Videoclips ist aber die Geschichte von MTV, und darum Ehre, wem Ehre geb&uuml;hrt schreibe ich hier &uuml;ber die MTV-&Auml;sthetik.</p>
      
<p>Da es sich in dieser Aufsatzsammelung alles um das Thema &gt;Schnitt&lt; dreht, bem&uuml;ht sich der Verfasser sich auf diesen speziellen Aspekt zu beschr&auml;nken, d. h. sich auf die Schnell-Schnitt-Forderungen zu konzentrieren. Abschweifungen sind zu entschuldigen, unvermeidlich und f&uuml;hren garantiert zum Thema zur&uuml;ck. Ich entschuldige mich bei allen Maskenbildern, Farbfolienherstellern und digitalen Bildbenacharbeitern und allen, deren Handwerk nicht gen&uuml;gend gew&uuml;rdigt wird. Ich habe Euch trotzdem lieb.</p>
      
<h4>1982: Birth of a nation</h4>
      
<p>1981 erscheinen die ersten Anzeigen in fachinternen Buisness-Bl&auml;ttern. &gt;&gt;The Biggest Advertising Merger in History&lt;&lt; versprechen sie. (Fu&szlig;note: Zur Geschichte des MTV vgl. Jane &amp; Michael Stern: Encylopedia of Pop Culture. New York: Harper 1992.) MTV hat sein Versprechen gehalten; und damit sind wir bei Grund No. 1, warum das Wort &gt;&gt;MTV&lt;&lt; immer noch einen schlechten Geschmack hinterl&auml;&szlig;t. MTV ist eine Erfindung, welche die Verk&auml;ufe der Musikindustrie steigern sollte und gleichzeitig die der Televisionbranche. Oder umgekehrt. Egal: Es geht ums Verkaufen. Aber soweit (d. h.bei der Kritik) sind wir ja noch garnicht. Also, MTV war der erste Sender, der auf die Idee kam, die Visualisierung von Musik an zwei Gruppen zu verkaufen: 1.) an die K&auml;ufer die von ihrer Band mehr erwarten als nur Musik (n&auml;mlich ein Image, also Bild oder (frei &uuml;bersetzt) ein zur Schau gestellter Lebensstil) und 2.) an die Musikindustrie, die via Image einer Band, den potentiellen K&auml;ufern ein weitere Orientierungshilfe geben wollte/sollte (Image hei&szlig;t... s. oben.).</p>
      
<p>Der gro&szlig;e, kommerziell ertragreiche Witz an Musiksendern wie MTV ist, da&szlig; der Sender &gt;&gt;nur&lt;&lt; die Infrastruktur und sein eigenes Image zur Verf&uuml;gung stellt; die Kosten f&uuml;r die Erstellung der Musik-Promo-Clips ich meine nat&uuml;rlich &gt;&gt;Videoclips&lt;&lt; tr&auml;gt der Verk&auml;ufer der Musik.</p>
      
<p>Damit hatte MTV eine ganz andere Ausgangbasis als andere TV-Sender, denn die hatten nur einen Produzenten f&uuml;r 90 Minuten, MTV hatte und hat 30 Produzenten (= 90 Minuten geteilt durch drei). Das Mindeste, was ein jeder Produzent anstrebt, ist, seine Kosten zu decken. Das Maximale, was jeder Produzent anstrebt, ist, mehr als seine Kosten einzuspielen. Die Produzenten der MTV-Clips bekommen ihr Geld nat&uuml;rlich nicht von MTV, sondern von der Musikindustrie, f&uuml;r die sie arbeiten. Was liegt da n&auml;her, als die Kosten des einzelen Videoclips zu erh&ouml;hen, um die Medienwirkung des Clips garantiert zu erh&ouml;hen? Mehr Medienwirkung = mehr Geld f&uuml;r die Produzenten.</p>
      
<p>Nun kommt wieder die 30:1-Quote ins Spiel. Ein 90-min&uuml;tiger Spielfilm kostete 1980 sch&auml;tzungsweise 15 Millionen US-Dollar. Geteilt durch 30 also 50.000 US-Dollar. Erh&ouml;ht nun jeder Video-Clip-Produzent nun seine Kosten um 10%, also auf 55.000 US-Dollar, um sein Video vom Kino-Durchschnitt abzuheben, so bedeutet dies f&uuml;r MTV-&auml;hnliche Sender, da&szlig; sie mit einer Produktion von 16,5 Millionen US-Dollar gegen eine 15 Millionen-US-Dollar-Produktion antreten. Und einige Musikvideoproduzenten erh&ouml;hten um mehr als 10%, und nicht alle Filme, die im Fernsehn ausgestrahlt werden, kosten 15 Millionen. Damit aber nicht genug, MTV bediente sich auch noch eines neuen Mediums: Video.</p>
      
<h4>Exkurs: Video vs. Kino</h4>
      
<p>Bis in die 80er arbeiteten die meisten TV-Sender auf 16mm. Der Umstieg auf Video erfolgte langsam, weil die Anschaffung einer MAZ-Anlage (MAZ = Magnetaufzeichnung) hohe Kosten verursachte. Der Vorteil der neuen Technologie war jedoch die Senkung der Materialkosten. Mit Video kann man f&uuml;r das gleiche Geld eine Szene &ouml;fter drehen, oder auch die Kamera einfach w&auml;hrend eines Ereignisses mitlaufen lassen. Letzteres erzeugt beim Zuschauer ein &gt;live&lt;-feeling, das wir alle inzwischen von unseren privaten Urlaubsvideos kennen und sch&auml;tzen. Und dies ist entscheidend f&uuml;r Musikkonsumenten, die von ihrer Band Authenzit&auml;t erwarten. Und dies ist, was Kinofilme wie CHUNGKING EXPRESS heute reproduzieren. Denn es darf nicht vergessen werden, da&szlig; verschiedene Unterhaltungsmedien stets in wirtschaftlicher Konkurrenz zueinander stehen. Alles, was der Zuschauer aus dem Fernsehn kennt, erwartet er schlie&szlig;lich auch im Kino zu sehen (und wer ins Kino geht, ist f&uuml;r 90 Minuten zumindest kein TV-Konsument). Allerdings vergessen die meisten Konsumenten, da&szlig; nur weil man Kino-Filme auch im Fernsehn sehen kann, die beiden Medien dennoch verschiedenen Produktionsbedingungen unterworfen sind.</p>
      
<p>Im TV gab es in den 80er einiges zu sehen, was man im Kino nicht sehen konnte. Die &gt;&gt;Duran Duran&lt;&lt;-clips unter der Regie von Russel Mulcalhy z. B. boten eine Vielzahl von Effekten, die wir aus dem Kino noch nicht kannten. Hierbei kam der Flimmerkiste ihre kleine Projektionsfl&auml;che zur Hilfe: Effekte f&uuml;r die kleine R&ouml;hre m&uuml;ssen nun einmal nicht so &gt;&gt;gut&lt;&lt;, will sagen: aufwendig sein, wie Raumschiffmodelle in 70mm-Produktionen.</p>
      
<p>Gerade bei Spezial Effekten kommen dem Video auch noch weitere medienspezifische Eigenarten zur Hilfe. Zum einen sind f&uuml;r Videoproduktionen aufgrund der geringen optischen Aufl&ouml;sung Nah- und Detailaufnahmen der Vorzug zu geben, zum anderen erfordert ein Produkt, das aus vielen kleinen Einstellungen besteht, eine schnellere Montage, um den Zuschauer dennoch den Eindruck eines Ganzen zu vermitteln. Wenn der Zuschauer aber gelernt hat, viele Details als ein Ganzes zu begreifen, so braucht er auch nur ab und zu den Fu&szlig; eines digitalen Dinos zu sehen, um sich das Monster in seiner ganzen Pracht vorzustellen.</p>
      
<p>Da&szlig; man es sich finanziell erlauben kann, Szenen mit Video &ouml;fter zu drehen, erleichert das Kombinieren mit andersorts gedrehten/erzeugten Szenen. Die M&ouml;glichkeit der direkte Kontrolle des abgedrehten Materials erleichtert zudem die Verwendung der Montage von einzelen Einstellungen in einem Bild. Fr&uuml;he Musikvideos verwendeten deswegen z. B. gerne Blue Box-Sequenzen, weil diese f&uuml;r den TV-Bildschirm viel leichter zu realisieren waren als f&uuml;r die gro&szlig;e Leinwand. - Nebenbei erw&auml;hnt, werden Kinofilme heute oft mit Hybrid-Systemen gedreht, welche gleichzeitig die Szene auch auf Magnetband bannen.</p>
      
<h4>Birth of a nation (Fortsetzung)</h4>
      
<p>Ist das Fernsehn der Lehrmeister der Nation? Was die Sprache der bewegten Bilder angeht, so gewi&szlig;. Durch seinen populistischen (Nicht-)Anspruch gelang es Sendern wie MTV eine Bildsprache allgemein verst&auml;ndlich zu machen, die schon seit langer Zeit von sog. &gt;Experimentalfilmern&lt; beherrschaft wurde. MTV verfolgte dabei nicht das Interesse, sich zu einer kulturellen Avantgarde zu z&auml;hlen, sondern wollte sich lediglich &uuml;ber die medienspezifischen M&ouml;glichkeiten von den anderen Produkten der kommerziell erzeugten Bilderwelt abzusetzen. Die Produzenten von Darstellungen auf anderen Medien mu&szlig;ten auf diese Herausforderungen reagieren.</p>
      
<p>Im Kino hatte diese Herausforderung unterschiedlich zu beurteilende Folgen. Z. B. konnte MTV darauf verzichten, eine durchgehende story zu erz&auml;hlen. Es ging ja &gt;&gt;nur&lt;&lt; darum, die entsprechenden Bilder zum Produkt &gt;&gt;Rock'n'Roll&lt;&lt; zu liefern, damit sich dieses besser verkauft. Und, bitte, seht MTV nur als Werbeveranstaltung, um Tontr&auml;ger zu verkaufen. Und nun fragt Euch, warum die Filme von David Lynch im Kino an der Kasse honoriert werden, mit all ihren Drehbuchl&uuml;cken.</p>
      
<p>So war es fr&uuml;her: Komplizierte stories mu&szlig;ten daf&uuml;r herhalten, um viele spektakul&auml;re Szenen/Bilder zu rechtfertigen. Es galt ein Drehbuch herzustellen, da&szlig; einen hohen &gt;Schauwert&lt; des realisierten Film m&ouml;glich machte. Gro&szlig;artige Szenen und brilliante Schauspielerauftritte mu&szlig;ten im Rahmen einer story erm&ouml;glicht werden. Heute wird diese Dienstleistungen am Zuschauer erf&uuml;llt ohne story. Den &gt;&gt;Rest&lt;&lt; erzeugt er in seinem Kopf oder er akzeptiert sie einfach so, ohne das Fehlen eines &gt;&gt;Restes&lt;&lt; einzuklagen. Genau wie in einem Madonna-Videoclip.</p>
      
<p>Und es tut nichts zur Sache, da&szlig; z. B. der italienische B-Film immer schon so funktionierte, da&szlig; ein Film aus guten Szenen ohne &Uuml;berg&auml;nge bestanden hat. Der italienische Billigfilm hat nie die Massenwirkung erzielt, welche das Nationen &uuml;bergreifende MTV hat. Nicht nur da&szlig; MTV in der gesamten westlichen Welt gesehen wird, man kann MTV heute auf jeden Sender empfangen. Jede Nachrichtensendung ist heute ein Videoclip: Computererzeugte Schriftz&uuml;ge winden sich ins Bild und Steadycams jagen ihre Motive. - Ich sage nicht, da&szlig; alle Ver&auml;nderungen des TV in den 90er sich monokausal auf das Ph&auml;nomen &gt;MTV&lt; zur&uuml;ckf&uuml;hren lassen. Aber ich sage, da&szlig; die erste Ausstrahlung MTV als ein markantes Ereignis in der Geschichte der Menschheit dargestellt werden kann, weil ab diesen Zeitpunkt die Menschen eine neue Bildersprache erlernten. Der erste Videoclip, das MTV ausstrahlte war &gt;&gt;Video killed the Radiostar&lt;&lt;. Heute mu&szlig; festgestellt werden: &gt;&gt;Video overkilled the Cinema&lt;&lt;.</p>
      
<h4>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/mtv/bunuel.jpg" align="left" height="127" width="180">Film ist Rythmus. Rythmus i&szlig;t Film.</h4>
      
<p>George A. Romero schneidet seine Filme zu &gt;klassicher&lt; rockmusik, Bu-uel lies Tango und Opern zu seinen Stummfilmen laufen. Heute geben Hip Hop und Techno den Takt an, auch wenn die Tonspur anderes besagt. Der Dikatatur der story ist einer Dikatatur des Rythmus gewichen. Wenn der Inhalt keinen Zusammenhang der einzelnen Einstellungen mehr gew&auml;hrleistet, dann mu&szlig; die Form sie zusammenschwei&szlig;en.</p>
      
<p>Beispiel: Detlev Buck. KARNICKELS und WIR K...NNEN AUCH ANDERS erz&auml;hlen stories, deshalb bleibt die Kamera ab und zu auch einfach nur stehen. Dies verleiht den Filmen eine klassische Filmsprache, die George Lucas auch bei STAR WARS bewu&szlig;t einsetzte. STAR WARS sollte schon beim ersten Sehen den Eindruck eines Klassikers hinterlassen.) M&Auml;NNERPENSION ist 90er-Jahre-Kino: chic und cool. Und der n&auml;chste Schnitt mu&szlig; kommen und uns zu der n&auml;chste h&uuml;bsche Kameraeinstellung bringen. So dargestellt wirkt die am Mainstream orientierte Machart als Einschr&auml;nkung eines der begabtesten deutschen Filmemacher, und sie ist doch nur ein Versuch konkurrenzf&auml;hig zu bleiben. Oder genauer: Den Markt f&uuml;r das Produkt Buck auf die MTV-geschulten Teenager auszuweiten, ein finanzstarkes Marktsegment auch in Deutschland (und dummerdings fest in amerkanischer Hand).</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/mtv/spawn.jpg" align="left" height="126" width="180">
Beispiel: &gt;&gt;Spawn&lt;&lt;. Ein Comic wie &gt;&gt;Spawn&lt;&lt; w&auml;re ohne MTV nicht denkbar. Eine derartige Verfilmung leider auch nicht. Der Comic nutzt die Chancen der MTV-Sprache und erz&auml;hlt eine Geschichte in grellen, sprunghaften Einstellungen. Der Film f&uuml;hlt sich dieser Erz&auml;hlhaltung verpflichtet und traut sich dennoch nicht einmal, eine Geschichte zu erz&auml;hlen. Statt desen bekommen wir einen Videoclip von &gt;Marlyn Manson&lt; geboten: Sch&ouml;ne Ghetto-Bilder zu sch&ouml;ner Musik. (Und Buisness-intern hei&szlig;t das dann Cross-Marketing: Euere Musik verkauft unseren Film, und unser Film verkauft Eure Musik. Schei&szlig; Kapitalismus!)</p>
      
<p>Letztes Beispiel: NATURAL BORN KILLERS. Schnelle bewegte Bilder huschen &uuml;ber die Leinwand und begleiten die Musik von LARD &amp; Co. Selten habe ich einen so am&uuml;santen Blutrausch auf der Leinwand gesehen. Ach, das sollte medienkritisch sein? Merke: Man kann MTV nicht durch eine &uuml;bertrieben schnelle, durchgeknallte Bildsprache, die alle Stile wild durcheinanderwirft, kritisieren, denn genau so funktioniert MTV.</p>
      
<h4>Das &gt;Horror Vacui&lt; der Kritiker</h4>
      
<p>Oliver Stone mu&szlig;te nach seinem Versagen, auf die Textebene zur&uuml;ckgehen und in Interviews kundtun, was denn die Aussage von NATURAL BORN KILLERS h&auml;tte sein sollen. In seiner Geschw&auml;tzigkeit r&uuml;ckt er in die N&auml;he Godards und anderen Manifestisten, Avantgardisten und sonstigen Filmemachern, die ihren Filmen mit wichtigen Worten viel Gewicht verleihen wollen. - Auch auf der Ebene der kulturellen Rechtfertigung hat MTV viel f&uuml;r die Befreiung der bewegten Bilder getan. Heute bedeutet Video wirklich &gt;&gt;Ich fliege&lt;&lt; und nicht mehr &gt;&gt;Ich sehe&lt;&lt;, wie es Nam Jam Paik f&uuml;r seine eigenen Arbeiten formulierte.</p>
      
<p>Es ist bezeichnend, da&szlig; diejenigen, denen ihre MTV-&Auml;sthetik am ehesten verziehen wird, die &gt;&gt;jungen&lt;&lt; schwarzen Filmemacher wie Spike Lee sind. Aber hier l&auml;&szlig;t sich diese &Auml;sthetik auch wieder rechtfertigen, wenn auch allerdings nur aufgrund der rassistischen Grundannahme, da&szlig; <i>"die Schwarzen eben Rythmus im Blut haben"</i>.</p>
      
<p>Angreifbar wird die MTV-&Auml;sthetik, weil sie nur entwickelt wurde, um die Produkte der Musikindustrie zu verkaufen. Deswegen entsteht Mi&szlig;trauen, wenn z. B. Rudi Dolezal vielleicht nicht einmal zu unrecht behauptet, da&szlig; die Videoclips in den 90er Jahren dabei sind <i>"die Rolle des Avantgardefilms zu &uuml;bernehmen, ihn abzul&ouml;sen, vielleicht zu ersetzen."</i> Die Experimentalfilmer sind schlie&szlig;lich der Kunst verpflichtet und nicht dem Geld wie die Videoclips, m&ouml;chte man einwenden und dabei vergessen, da&szlig; auch Experimentalfilmer gerne mit ihren Produkten ihre Miete und ihre Br&ouml;tchen verdienen.</p>
      
<h4>Wie man das Staunen verlernt</h4>
      
<p>Vielen Cineasten (und ich schlie&szlig;e mich hier ein) bedauern, da&szlig; die MTViserung des Kinos, da&szlig; Kino plastikm&auml;&szlig;ig gemacht hat, was nicht weniger als unmenschlicher bedeutet. Erinnert Ihr Euch noch an die 70er Jahre als ein Autostunt noch nicht in zahlreiche Einzeleinstellungen zerschnitten wurde und man die Leistung des Stuntman noch richtig w&uuml;rdigen konnte. In Filmen wie THE ROCK leisten Stundmen immer noch das gleiche, vielleicht auch besseres, aber ihre Leistungen werden in viele kurze Einstellungen zerschnitten. Die Leistung des Einzelnen wird durch die Bildsprache unkenntlich gemacht und es entsteht der Eindruck, da&szlig; die Bilder der realen Leistung nicht genug Respekt zollen.</p>
      
<p>Dies ist eine seltsam romatisierende Sicht der Dinge, denn die Leistungen, die f&uuml;r die Herstellung dieser Art von Filmen erbracht werden, sind n&uuml;chtern betrachtet gr&ouml;&szlig;er als die mehr bewunderten. Man bekommt pro Film heute mehr geboten: mehr Kamerafahrten, mehr Effekte und mehr Einstellungen (= mehr Schnitte). An der Herstellung eines Films d&uuml;rften heute im Durchschnitt mehr Menschen mitarbeiten als je zuvor, insofern ist es absurd davon zu sprechen, da&szlig; die Filme unmenschlicher geworden sind. Es sind nicht die Filme, welche die Leistungen des Einzelnen nicht gen&uuml;gend respektieren, es sind wir selbst. Vor 20 Jahren konnten wir sagen: Oh, was ein herrlicher Stunt! Heute m&uuml;&szlig;ten wir sagen: Oh, eine nette Kamerabewegung! Mein Gott, wie toll die Statisten gef&uuml;hrt wurden! Was f&uuml;r ein Stunt! Tolle digitale Bildbearbeitung! (Schnitt) Oh, noch eine gute Kamerabwegung! Noch ein brillinater Stunt! Noch... Soviel Bewunderung &uuml;berfordert freilich jeden Bewunderer. Im cineastischen Overkill haben wir das Staunen verlernt; aber das ist unser ganz pers&ouml;nliches Problem.</p>
      
<h4>Das Ende der Spirale</h4>
      
<p>Wenn man aber einer dieser Romatiker ist, mu&szlig; man dann nicht f&uuml;rchten in Zukunft nur noch die guten alten Filme gucken zu k&ouml;nnen und auf gute neue garnicht mehr hoffen darf? F&uuml;rchtet Euch nicht und hofft, den die Logik des Marktes steht auf unserer Seite.</p>
      
<p>Bei MTV und &auml;hnlichen Sendern gibt es deutliche Ver&auml;nderungen zu beobachten. Die Konkurrenz der Videoclips untereinander hat dazu gef&uuml;hrt, da&szlig; die Langsamkeit neu entdeckt wurde. Um im Montage-Blitzkrieg der Konkurrenzprodukte aufzufallen, greifen inzwischen viele Clips zur Notbremse und drosseln das Tempo. Wenn wir es wirklich MTV zu als das zu verdanken haben, was ich in diesem Aufsatz beschrieben haben, dann sind die langen Einstellungen der Trip Hop-Generation unsere Hoffnungstr&auml;ger. - Allerdings wird &uuml;ber diesen Einflu&szlig; dann niemand schreiben, sondern den Sieg des guten Geschmacks &uuml;ber den b&ouml;sen Kommerz preisen.</p>
      
<p>&nbsp;</p>
<hr>
<h3>Literatur / Quellenangaben</h3>
<p>
<small>Literaturtip:
Zum Weiterlesen empfiehlt der freundliche Verfasser den Sammelband &gt;Visueller Sound - Musikvideos zwischen Avantgarde undPopul&auml;rkultur&lt; (hg. von Celilia Hausherr und Annette Sch&ouml;nholz, erschienen 1994 im Zyklop Verlag/Luzern).
</small>
</p>
    
<a name="sec-eisenst"></a>
<h3>IA28 - Sergej Eisensteins Weg zur Intellektuellen Montage</h3>
      
<subsection title="1. Einleitung">
        
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/portrait.jpg" align="left" height="345" width="250">Sergej Eisenstein (1898-1948) konzipiert Ende der 20er Jahre sein erstes Buch. Es soll ein "kugelf&ouml;rmiges" Buch werden. Ein Buch &uuml;ber die Methode der Montage, der er bis dahin schon zahlreiche Manifeste gewidmet hatte. Nicht die Abgrenzung zu anderen Theoretikern stand dabei im Vordergrund, sondern die Darstellung eines neues Analysemodells, da&szlig; sich der g&auml;ngigen Bem&uuml;hung der Avantgarde, einer allumfassenden Theorie der Kunst zu begr&uuml;nden, entzog.</p>
        
<p>
          
<i>"Es ist sehr schwer, ein Buch zu schreiben. Weil jedes Buch zweidimensional ist. Ich wollte aber, da&szlig; sich dieses Buch durch eine Eigenschaft auszeichnet, die keinesfalls in die Zweidimensionalit&auml;t eines Druckwerkes pa&szlig;t. Diese Forderung hat zwei Seiten. Die erste besteht darin, da&szlig; das B&uuml;ndel dieser Aufs&auml;tze auf gar keinen Fall nacheinander betrachtet und rezipiert werden soll. Ich w&uuml;nschte, da&szlig; man sie alle zugleich wahrnehmen k&ouml;nne, weil sie schlie&szlig;lich eine Reihe von Sektoren darstellen, die, auf verschiedene Gebiete ausgerichtet, um einen allgemeinen, sie bestimmenden Standpunkt - die Methode - angeordnet sind. Andererseits wollte ich rein r&auml;umlich die M&ouml;glichkeit schaffen, da&szlig; jeder Beitrag unmittelbar mit einem anderen in Beziehung tritt... Solcher Synchronit&auml;t und gegenseitigen Durchdringungen der Aufs&auml;tze k&ouml;nnte ein Buch in Form... einer Kugel Rechnung tragen... Aber leider... werden B&uuml;cher nicht als Kugeln geschrieben... Mir bleibt nur die Hoffnung, da&szlig; dieses unentwegt die Methode wechselseitiger Umkehrbarkeit er&ouml;rternde Buch nach eben derselben Methode gelesen werden wird. In der Erwartung, da&szlig; wir es lernen werden, B&uuml;cher als sich drehende Kugeln zu lesen und zu schreiben. B&uuml;cher, die wie Seifenblasen sind, gibt es auch heute nicht wenige. Besonders &uuml;ber Kunst."</i> <small>(BULGAKOWA, S. 31f)</small>
        
</p>
        
<p>Dies schreibt Eisenstein am 5. August 1929 in sein Tagebuch. Bis dato hat er drei Spielfilme gedreht: STREIK (1924), PANZERKREUZER POTEMKIN (1925), OKTOBER (1927). Ausgehend von seiner Theaterarbeit (in deren Rahmen er bereits einen <i>"Kurzfilm"</i> - GLUMOWS TAGEBUCH, 1923, 120 m - aufgenommen hatte) hat er die Idee der <i>"Montage der Attraktionen"</i> bis zur <i>"Intellektuellen Montage"</i> (vor allem OKTOBER) entwickelt. In diesem Tagebuchausschnitt spiegelt sich einiges von seiner Auffassung vom Filmen wider. In deren Zentrum steht immer die Methode - die Montage. Sie entwickelt sich - dies zeigt auch der Tagebucheintrag - nicht linear. Er spricht von <i>"Synchronit&auml;t"</i>, <i>"gegenseitiger Durchdringung"</i>, der <i>"Beziehung"</i> zwischen den unterschiedlichen Artikeln, die nicht notwendigerweise aufeinader aufbauen m&uuml;ssen, inhaltlich sogar kontr&auml;r sein k&ouml;nnten.</p>
        
<p>Dies erfordert die aktive Teilnahme des Lesers / der Leserin - jedes Leseerlebnis w&auml;re individuell in der Zusammenstellung und dem Wahrnehmungsproze&szlig; - vielschichtig, komplex, aber auch mehrdeutig.</p>
        
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Sergej M. Eisenstein - Kurzbiographie</h3>
<p>1898 in Riga geboren. B&uuml;rgerliches Elternhaus, Vater Architekt. Nach dem ersten Weltkrieg Sch&uuml;ler bei Wsewolod Meyerhold. Arbeitete als Regisseur und B&uuml;hnenbildner am Proletkult-Theater. 1924 entsteht sein erster Film STREIK. 1925 schuf er das epochemachende Filmkunstwerk PANZERKREUZER POTEMKIN. Es folgten OKTOBER (Zehn Tage, die die Welt ersch&uuml;tterten) - 1927 - und DAS ALTE UND DAS NEUE (Die Generallinie) - 1929. Zahlreiche Reisen f&uuml;hrten Eisenstein nach Frankreich, Deutschland, in die Schweiz, die USA und nach Mexiko. Dort 1930-31 Dreharbeiten zu QUE VIVA MEXICO! - wird aus Geldmangel nicht vollendet. Ab 1932 war er Professor an der Filmhochschule in Moskau. Arbeitete 1935-37 an dem Projekt BESHINWIESE - wurde von der <i>"Hauptverwaltung Film"</i> eingestellt. 1938 erschien sein Film ALEXANDER NEWSKI und 1944 IWAN DER SCHRECKLICHE, I. Teil. (Der II. Teil blieb unvollendet.) Eisenstein starb 1948 in Moskau</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
        
<p>Eisenstein spricht auch von einer <i>"Reihe von Sektoren"</i>, die auf <i>"verschiedene Gebiete ausgerichtet"</i> sind - seine Ans&auml;tze gehen meist von filmfremden Disziplinen aus - Psychologie, Lyrik, Psychoanalyse usw. - immer wieder reviediert er alte Ans&auml;tze, nimmt neue Anregungen dazu. Seine Auffassung von Montage ist entsprechend keine geschlossene, sondern eine offene Theorie - immer im Wandel, gepr&auml;gt von einer st&auml;ndigen Suche.</p>
      
</subsection>
      
<subsection title="2. Gegen&uuml;berstellung: die dramatische Montage (Griffith) - die &quot;intellektuelle&quot; Montage (Eisenstein)">
        
<p>Was genau Eisensteins Methode der Montage ausmacht zeigt sich sehr sch&ouml;n in einer Gegen&uuml;berstellung mit einem kontr&auml;ren Ansatz. Der Vergleich mit dem amerikanischen Stummfilmpionier D. W. Griffith bietet sich geradezu an. Eisenstein befa&szlig;te sich ausf&uuml;hrlich mit dessen Auffassung von Montange (und deren Tradition in der Literaturgeschichte - evtl. Anm. 1) in seinem Artikel <i>"Dickens, Griffith und wir"</i> <small>(1941)</small>. Er bewunderte dessen Entdeckungen wie die Parallelmontage (Gegen&uuml;berstellung zweier Handlungsstr&auml;nge), den Wechsel der Kameradistanz (z. B. von Totaler zur Nahaufnahme) und erkannte dessen wesentlichen Einflu&szlig; auf die russischen Filmemacher an. Dennoch grenzt er sich scharf ab: Griffith	 benutzt seine Montage zur Steigerung der Dramatik, des suspense, Eisenstein will mehr:</p>
        
<p>
          
<i>"Ein Kunstwerk, wie wir es verstehen, ist (wenigstens ein den beiden Bereichen, in denen ich arbeite - Theater und Film) vor allem ein Traktor, der die Psyche des Zuschauers im Sinne des angestrebten Klassenstandpunktes umpfl&uuml;gt"</i> <small>(KAUFMANN, S. 55)</small>
        
</p>
        
<p>In BIRTH OF A NATION (Die Geburt einer Nation; 1915) und in OKTOBER (1927) beschreiben beide die Entstehungsgeschichte der eigenen Nation - Griffith <i>"dramatisch"</i>, Eisenstein <i>"intellektuell"</i>. Ein Vergleich wird die Unterschiede verdeutlichen:</p>
        
<subsubsection title="2.1 Griffith: Die dramatische Montage">
          
<p>In dem Ausschnitt wird die Ermordung von Pr&auml;sident Lincoln durch den Attent&auml;ter Booth w&auml;hrend eines Theaterbesuchs gezeigt:</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth01.jpg" align="left" height="130" width="180">
Zwischentitel: Und dann, als die fruchtbaren Tage vor&uuml;ber waren und eine segensreiche Zeit des Friedens bevorstand (...) , nahte die schicksalsvolle Nacht des 14. April 1865.</p>
          
<p>Es folgt eine kurze Szene, in der Benjamin Cameron (Henry B. Walthall) Elsie Stoneman (Lilian Gish) vom Haus der Stonemans abholt und zusammen mit ihr ins Theater geht. Sie nehmen an einer Galavorf&uuml;hrung teil, bei der auch Pr&auml;sident Lincoln anwesend sein wird. Die Vorstellung hat schon begonnen.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth02.jpg" align="left" height="130" width="180"> Zwischentitel: Zeit: 8.30 Uhr. Die Ankunft des Pr&auml;sidenten</p>
          
<p>1	Aufblende. Lincoln und seine Begleiter kommen nacheinander am Ende der Treppe im Theater an und wenden sich in Richtung Pr&auml;sidentenloge. Lincolns Leibw&auml;chter geht zuerst, Lincoln selber als letzter. (7 Sek.)</p>
          
<p>2	Die Pr&auml;sidentenloge vom Zuschauerraum des Theaters aus gesehen. Die Begleiter Lincolns erscheinen in der Loge. (4 Sek.)</p>
          
<p>3	Aufblende. Pr&auml;sident Lincoln vor der T&uuml;r zur Logen, wie er einem Diener seinen Hut &uuml;berreicht. (3 Sek)</p>
          
<p>4	Die Pr&auml;sidentenloge. Wie Einstellung 2: Lincoln erscheint in der Loge. (5 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth03.jpg" align="left" height="130" width="180">
5	Halbnah. Elsie Stoneman und Ben Cameron sitzen im Zuschauerraum. Sie blicken beide zur Pr&auml;sidentenloge hoch, beginnen dann zu klatschen und stehen auf. (7 Sek.)</p>
          
<p>6	Einstellung von hinten auf das Publikum in Richtung B&uuml;hne. Die Pr&auml;sidentenloge befindet sich auf der rechten Seite. Das Publikum mit dem R&uuml;cken zur Kamera steht im Vordergrund, klatscht und begr&uuml;&szlig;t begeistert den Pr&auml;sidenten. (3 Sek.)</p>
          
<p>8	 Wie Einstellung 6. (3 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth04.jpg" align="left" height="130" width="180">
9	 Die Pr&auml;sidentenloge. Wie Einstellung 7: Der Pr&auml;sident verneigt sich und setzt sich dann (5 Sek.)</p>
          
<p>Zwischentitel: Lincolns pers&ouml;nlicher Leibw&auml;chter bezieht seinen Posten vor der Loge.</p>
          
<p>10	Aufblende. Der Leibw&auml;chter erscheint im Gang vor der Loge und setzt sich. Er beginnt, ungeduldig seine Knie zu reiben. (10 Sek.)</p>
          
<p>11	Einstellung vom hinteren Zuschauerraum in Richtung B&uuml;hne. Die Auff&uuml;hrung ist in vollem Gange. (5 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth05.jpg" align="left" height="130" width="180">
12	Die Pr&auml;sidentenloge. Wie Einstellung 9: Lincoln ergreift die Hand seiner Frau. Aufmerksam verfolgt er das Geschehen auf der B&uuml;hne. (9 Sek.)</p>
          
<p>13	 Wie Einstellung 11: Das Klatschen des Publikums verebbt. (4 Sek.)</p>
          
<p>14	Eine n&auml;here Einstellung der B&uuml;hne. Das Spiel der Schauspieler. (10 Sek.)</p>
          
<p> Zwischentitel: Um etwas von der Auff&uuml;hrung mitzubekommen, verl&auml;&szlig;t der Leibw&auml;chter seinen Posten.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth06.jpg" align="left" height="130" width="180">
15	Aufblende: Der Leibw&auml;chter. Wie Einstellung 10: Er ist offensichtlich ungeduldig. (5 Sek.)</p>
          
<p>16	N&auml;here Einstellung der B&uuml;hne. Wie Einstellung 14. (2 Sek.)</p>
          
<p>17	Aufblende. Der Leibw&auml;chter. Wie Einstellung 15: Er steht auf und stellt seinen Stuhl hinter eine Seitent&uuml;r. (6 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth07.jpg" align="left" height="130" width="180">
18	Wie Einstellung 6: Blick auf die Loge neben der von Lincoln. Der Leibw&auml;chter erscheint und nimmt Platz. (3 Sek.)</p>
          
<p>19	In einer Kreisblende sehen wir eine n&auml;here Einstellung der Handlung von Nr. 18. Der Leibw&auml;chter nimmt in der Loge Platz. (5 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth08.jpg" align="left" height="130" width="180">
Zwischentitel: Zeit 10.30 Uhr - 3. Akt, 2. Szene</p>
          
<p>20	Eine Totale des Zuschauerraums von hinten; eine diagonale Schlitzblende gibt nur den Blick auf die Pr&auml;sidentenloge frei. (5 Sek.)</p>
          
<p>21	Halbnah. Elsie und Ben. Elsie deutet auf etwas in Richtung Pr&auml;sidentenloge. (6 Sek.)</p>
          
<p>Zwischentitel: John Wilkes Booth (gespielt von Raoul Walsh)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth09.jpg" align="left" height="130" width="180">
22	Kopf und schultern von John Wilkes Booth, durch eine Kreisblende gesehen. (3 Sek.)</p>
          
<p>23	Wie Einstellung 21: Elsie verfolgt wieder voller Aufmerksamkeit das Geschehen auf der B&uuml;hne. (6 Sek.)</p>
          
<p>24	Booth. Wie Einstellung 22. (2,5 Sek.)</p>
          
<p>25	N&auml;here Einstellung auf die Pr&auml;sidentenlogen. (5 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth10.jpg" align="left" height="130" width="180">
26	Booth. Wie Einstellung 22. (4 Sek.)</p>
          
<p>27	N&auml;here Einstellung der B&uuml;hne. Wie Einstellung 14. (4 Sek.)</p>
          
<p>28	N&auml;here Einstellung der Pr&auml;sidentenloge. Wie Einstellung 25. Lincoln l&auml;chelt zustimmend dem Geschehen auf der B&uuml;hne zu. Er bewegt seine Schultern, als ob ihm kalt w&auml;re, und beginnt, seinen Mantel &uuml;berzuziehen. (8 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth11.jpg" align="left" height="130" width="180">
29	Booth. Wie Einstellung 22: Er blickt nach oben, w&auml;hrend er sich vom Sitz erhebt. (4 Sek.)</p>
          
<p>30	Fortsetzung Einstellung 28. Lincoln beendet das &Uuml;berziehen seines Mantels. (6 Sek.)</p>
          
<p>31	Das Theater, von der R&uuml;ckseite des Publikums aus gesehen. Wie Einstellung 20: Die Schlitzmaske blendet aus und zeigt den gesamten Zuschauerraum. (4 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth12.jpg" align="left" height="130" width="180">
32	 Sehr nah. Der Leibw&auml;chter, wie er sich &uuml;ber das St&uuml;ck am&uuml;siert. Wie Einstellung 19 mit einer Kreisblende. (1,5 Sek.)</p>
          
<p>33	Aufblende. Booth. Er geht durch die T&uuml;r am Ende des Ganges vor Lincoln Loge. Er beugt sich vor, um durch das Schl&uuml;sselloch in Lincolns Loge zu blicken. Er zieht den Revolver hervor und atmet tief durch, als wolle er sich f&uuml;r die Tat Mut machen. (14 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth13.jpg" align="left" height="130" width="180">
34	 Sehr nah. Der Revolver. (3 Sek.)</p>
          
<p>35	Fortsetzung von Einstellung 33: Booth versucht, die Loge zu betreten, hat f&uuml;r einen Moment Schwierigkeiten beim &Ouml;ffnen der T&uuml;r, betritt dann die Pr&auml;sidentenloge. (8 Sek.)</p>
          
<p>36	Nah. Blick auf Lincolns Loge. Wie Einstellung 25: Booth taucht hinter Lincoln auf. (5 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth14.jpg" align="left" height="130" width="180">
37	Die B&uuml;hne. Wie Einstellung 14: Die Schauspieler agieren auf der B&uuml;hne. (4 Sek.)</p>
          
<p>38	 Wie Einstellung 36: Booth schie&szlig;t Lincoln in den R&uuml;cken. Lincoln bricht zusammen. Booth klettert seitlich &uuml;ber die Br&uuml;stung der Loge und springt auf die B&uuml;hne. (5 Sek.)</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth15.jpg" align="left" height="130" width="180">
39	Totale. Booth steht auf der B&uuml;hne. Er rei&szlig;t die Arme hoch und ruft:</p>
          
<p>Zwischentitel: Sic Semper Tyrannis</p>
          
<p>
            
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth16.jpg" height="130" width="180">
            <img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth17.jpg" height="130" width="180">
            <br>
            
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth18.jpg" height="130" width="180">
            <img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/birth/birth19.jpg" height="130" width="180">
          </p>
          
<p>Also, was tut Griffith?</p>
          
<p>Zun&auml;chst betrachtet ist die Handlung der Szene relativ einfach: Lincoln besucht eine Theaterinszenierung, sein Leibw&auml;chter l&auml;&szlig;t sich ablenken, verl&auml;&szlig;t seinen Posten, so kann der Attent&auml;ter bis in die Loge vordringen und Lincoln erschie&szlig;en. Die eigentliche Handlung befindet sich an einem Ort und dreht sich um drei Personen: Lincoln, den Leibw&auml;chter und Booth. Griffith f&uuml;hrt jedoch noch eine zus&auml;tzliche Handlungsgruppe ein: Cameron und Stoneman, die inmitten des Publikums sitzen. Er verzichtet auf eine kontinuierliche Handlungswiedergabe, wie bis dahin &uuml;blich, und l&ouml;st die Szene auf, wechselt zwischen diesen einzelnen Gruppen, ohne da&szlig; dadurch das Geschehen wesentlichen vorangetrieben w&uuml;rde. Trotzdem sind die Wechsel f&uuml;r den Zuschauer verst&auml;ndlich, der Ablauf bleibt kontinuierlich. Die Gesamthandlung wird zerlegt, und durch Montage wieder neu zusammengef&uuml;gt. <br>
Griffith <i>"erkl&auml;rt"</i> den Ablauf, und w&auml;hlt dazu die wesentlichen Momente aus. Durch diese <i>"Anh&auml;ufung einer Serie von wirkungsvollen Details"</i> <small>(REISZ, S. 19)</small> erh&auml;lt er eine wesentlich gr&ouml;&szlig;ere Erz&auml;hldichte. In Einstellung 15 zeigt er den ungeduldig wartenden Leibw&auml;chter, statt mit ihm fortzufahren, pr&auml;sentiert er in Nr. 16 den Grund f&uuml;r seine Ungeduld: die Auff&uuml;hrung. Damit werden der folgende Orts- und Kamerapositionswechsel (von Nr. 17 zu Nr. 18: der Leibw&auml;chter betritt die Loge) eingeleitet.</p>
          
<p>Das Wechseln der Handlungsgruppen bietet Griffith noch weitere M&ouml;glichkeitet. Mit den Figuren von Cameron und Stoneman hat er gewisserma&szlig;en Beobachter des Geschehens eingef&uuml;hrt. Er kann dadurch von quasi Sehenden zu dem Gesehenen (dem Ablauf in Lincolns Loge) wechseln. Er bildet das Geschehen also nicht nur ab, sondern bietet dem Zuschauer zus&auml;tzlich die M&ouml;glichkeit der Identifikation.</p>
          
<p>Um den Gef&uuml;hlsausdruck oder die Dramatik einer Szene zu unterstreichen, &auml;ndert Griffith die Distanz zum Darsteller oder Objekt: Kurz bevor Booth die Loge betritt (Nr. 33) schneidet er auf die Nahaufnahme des Revolvers (Nr. 34), um dessen Absicht noch einmal zu verdeutlichen (unterstrichen durch den "Fokus" der Kreisblende), springt dann wieder zur&uuml;ck in die Totale (Nr. 36).Von der &Uuml;bersicht auf das Detail und zur&uuml;ck.</p>
          
<p>Griffith Auffassung von Montage ist gebunden an ein Seh- und Erz&auml;hlperspektive. Sie gibt der Kamera und deren Position eine v&ouml;llig neue Wichtigkeit. Auch die Bedeutung der Schauspieler &auml;ndert sich: Sie bekommen mehr Gewicht z. B. durch Nahaufnahmen, dies erfordert gleichzeitig eine exaktere Arbeitsweise. Die Inszenierung liegt durch die Montage jetzt v&ouml;llig in den H&auml;nden des Regisseurs. Dabei spielt die Auswahl der Einstellungen, die Reihenfolge, aber vor allem das Timig, das Tempo eine entscheidende Rolle. Die Arbeit des Cutters gewinnt an Bedeutung. </p>
        
</subsubsection>
        
<subsubsection title="2.2 Eisenstein: Die intellektuelle Montage">
          
<p>Im Vergleich dazu ein Ausschnitt aus Sergej Eisensteins OKTOBER (1927), in dem er versucht die Ereignisse, die zur Oktoberrevolution gef&uuml;hrt haben, aber vor allem ihre Bedeutung und ihren ideologischen Hintergrund, nachzuverfolgen. In diesem Ausschnitt wird die Figur des neuen Ministerpr&auml;sidenten der provisorischen Regierung; Kerenskij, portr&auml;tiert, der sich nach der Februarrevolution im Winterpalais des Zaren eingerichtet hat. </p>
          
<p>Seit 1928 spricht Eisenstein in bezug auf OKTOBER von einer <i>"intellektuellen"</i> Montage:</p>
          
<p>
            
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob01.jpg" align="left" height="130" width="180">
            <b>Sergei Eisenstein:  Oktober (1927)<br>
Aktrolle 3:  Charakterisierung von Kerenskij</b>
          
</p>
          
<p>1-17 Das Innere des Winterpalais in St. Petersburg. Kerenskij, Haupt der provisorischen Regierung, geht langsam, begleitet von zwei Leutnants, den riesigen Korridor des Palais entlang. Er steigt die Treppe hoch: dazwischen geschnitten sind Zeitlupenaufnahmen des stolz die Stufen erklimmenden Kerens-kij, sowie einzelne Zwischentitel, die Kerenskijs Titel nennen: Kommandierender General, Kriegs- und Marineminister, etc. pp.</p>
          
<p>18	Nahaufnahme. Eine Girlande in den H&auml;nden einer der Statuen im Palast.</p>
          
<p>19	Halbtotale. Die Statue in voller Gr&ouml;&szlig;e, die Girlande haltend.</p>
          
<p>20	Nahaufnahme. Girlande, wie in Nr. 18.</p>
          
<p>21	Zwischentitel: Hoffnung seines Landes und der Revolution.</p>
          
<p>22	Einstellung aus der Froschperspektive au eine andere Statue, die eine Girlande h&auml;lt. (Der Kamerastandpunkt vermittelt den Eindruck, als ob die Statue jeden Augenblick den Kopf Kerenskijs mit der Girlande schm&uuml;cken wolle.)</p>
          
<p>23	Zwischentitel: Alexandr Frjodorowitsch Kerenskij.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob02.jpg" align="left" height="130" width="180">

24	Sehr nah. Kerenskijs Gesicht, ruhig und gefa&szlig;t.</p>
          
<p>25	Nahaufnahme. Die Girlande in den H&auml;nden der Statue.</p>
          
<p>26	Sehr nah. Kerenskij, wie in Nr. 24. Der AUsdruck seines Gesichts entspannt sich, er l&auml;chelt.</p>
          
<p>27	Nahaufnahme. Die Girlande wie in Nr. 25</p>
          
<p>28-39		Kerenskij steigt weiter die Treppe empor und wird von einem der riesigen und hoch dekorierten Gardisten der zaristischen Leibwache begr&uuml;&szlig;t. Kerenskij, auf W&uuml;rde bedacht, wirkt neben dieser imposanten Figur l&auml;cherlich. Er wird einer ganzen Reihe von Leibgardisten vorgestellt und sch&uuml;ttelt jedem die Hand. Was f&uuml;r ein Demokrat!</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob03.jpg" align="left" height="130" width="180">
40-74		Kerenskij wartet vor einer reich verzierten T&uuml;r, die zu den privatgem&auml;chern des Zaren f&uuml;hrt. Wir sehen das zaristische Wappen an der T&uuml;r. Kerenskij wartet hilflos darauf, dass sich die T&uuml;r &ouml;ffnet. Zwei Gardisten grinsen. Kerenskijs Stiefel: dann, sehr nah, seine behandschuhten H&auml;nde, die sich ungeduldigbewegen. Die beiden Leutnants sind verlegen. Wir schneiden auf den Kopf eine reichornamentierten Prozellanpfaus: er sch&uuml;ttelt seinen Kopf, dann &ouml;ffnet er stolz dem Pfauenschwanz zu einem F&auml;cher: er beginnt sich zu drehen, indem er eine Art Tanz vorf&uuml;hrt, w&auml;hrend die Fl&uuml;gel im Licht glei&szlig;en. Die riesige T&uuml;r &ouml;ffnet sich. Einer der Gardisten grinst. Kerenskij &uuml;berschreitet die Schwelle; und weitere T&uuml;ren &ouml;ffnen sich. nach und nach. (Der Vorgang der sich &ouml;ffnenden T&uuml;ren wird mehrmals wiederholt, ohne da&szlig; die einzelnen Phasen des T&uuml;r&ouml;ffnens Anschlu&szlig; haben). Der Kopf des Pfaus h&auml;lt inne und blickt, anscheinend in voller Bewunderung, dem verschwindenden Kerenskij hinterher.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob04.jpg" align="left" height="130" width="180">
74-79		Schnitt auf die Soldaten, Matrosen und bolschewistischen Arbeiter, die teilnahmslos im Gef&auml;ngnis warten; dann auf Lenin, der sich in nebligen S&uuml;mpfen versteckt h&auml;lt.</p>
          
<p>80-99		Kerenskij in den Privatgem&auml;chern des Zaren. Nahaufnahmen der riesigen Nippessammlung, auf jedem Gegenstand die Initiale A des Zaren, sogar auf dem kaiserlichen Nachttopf. In den Gem&auml;chern der Zarin Alexandra Fjodorowna: noch mehr Nippes, kostbar verzierte M&ouml;bel, Borduren und Quasten, das Bett der Zarin. Kerenskij legt sich aufs Bett (gezeigt in drei aufeinaderfolgenden Einstellungen mit verschiedenen Kamerawinkeln). Alexandr Fjodorowitsch. Noch mehr Quasten und Bord&uuml;ren, etc.</p>
          
<p>100-105	In der Bibliothek Nikolaus' II. Kerenskij, neben einem Tisch in der zaristischen Bibliothek stehend, wirkt in dieser Umgebung klein und unscheinbar. Drei weitere Einstellungen von Kerenskij, die sich fortgesetzt von ihn entfernen und dadurch noch mehr die unscheinbare Gr&ouml;&szlig;e Kerenskijs in diesem riesigen Raum des Palastes verdeutlichen. Kerenskij nimmt ein St&uuml;ck Papier vom Schreibtisch.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob05.jpg" align="left" height="130" width="180">
106	Zwischentitel: Erla&szlig; zur Wiedereinf&uuml;hrung der Todesstrafe.</p>
          
<p>107	Halbtotale. Kerenskij am Schreibtisch, nachdenklich.</p>
          
<p>108	Totale. Kerenskij. Er beugt sich &uuml;ber den Schreibtisch und unterzeichnet den Erla&szlig;.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob06.jpg" align="left" height="130" width="180">
109	Einstellung vom obersten Absatz der Palasttreppe auf Kerenskij, der sich langsam der ersten Treppenstufe n&auml;hert.</p>
          
<p>110	Nahaufnahme. Ein Diener beobachtet ihn.</p>
          
<p>111-124	Kerenskij, den Kopf vorgeneigt, die Hand wie Napoleon in den Ausschnitt der Jacke geschoben, steigt bed&auml;chtig die Treppe hoch. Ein Ein Diener und einer der Leutnante beobachten ihn. Halbtotale. Kerenskij, auf den Boden blickend, die Arme vor der Brust verschr&auml;nkt. Eine Statuette von Napoleon in der gleichen Haltung. Der Diener und der Leutnant salutieren. Eine Reihe der gro&szlig;en Palast-Weingl&auml;ser. Noch eine Reihe Gl&auml;ser. Eine Reihe Zinnsoldaten, in der gleichen Anordnung wie die Weingl&auml;ser.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob07.jpg" align="left" height="130" width="180">
125	Nahaufnahme. Kerenskij am Tisch sitzend. Vor ihm stehen die vier Teile einer vierschnabeligen Karaffe. Die Teile stehen nebeneinander auf dem Tisch; Kerenskij starrt auf sie herab.</p>
          
<p>126	Nahaufnahme. Kerenskijs H&auml;nde versuchen, die vier Teile der Karaffe zusammenzuf&uuml;gen.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob08.jpg" align="left" height="130" width="180">
127	Halbtotale. Kerenskij.</p>
          
<p>128	Nahaufnahme. Kerenskijs H&auml;nde.</p>
          
<p>129	Halbtotale. Kerenskij. Er starrt auf  die Karaffe.</p>
          
<p>130	Gro&szlig;aufnahme. Kerenskijs Hand, die eine Schublade des Tisches &ouml;ffnet und einen Karaffenst&ouml;psel - in Form einer Krone - herausnimmt.</p>
          
<p>131	Halbtotale. Kerenskij; er hebt den wie eine Krone geformten St&ouml;psel in Augenh&ouml;he.</p>
          
<p>132	Sehr nahe Gro&szlig;aufnahme. Der wie eine Krone geformte St&ouml;psel.</p>
          
<p>133	Halbtotale. Kerenskij; er steckt die Krone auf die Karaffen&ouml;ffnung.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob09.jpg" align="left" height="130" width="180">
134	Sehr nahe Gro&szlig;aufnahme. Der kronenf&ouml;rmige St&ouml;psel, der nun die Karaffe verschlie&szlig;t.</p>
          
<p>135	Die Dampfpfeife einer Fabrik, die Dampf abbl&auml;st.</p>
          
<p>136	Sehr nahe Gro&szlig;aufnahme. Der kronenf&ouml;rmige St&ouml;psel.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob10.jpg" align="left" height="130" width="180">
137	Die Dampfpfeife.</p>
          
<p>138	Zwischentitel: Die Revolution in Gefahr</p>
          
<p>139	Sehr nahe Gro&szlig;aufnahme. Der kronenf&ouml;rmige St&ouml;psel.</p>
          
<p>140	Halbtotale. Kerenskij, der sich hinsetzt, um den St&ouml;psel auf der Karaffe zu bewundern.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob11.jpg" align="left" height="130" width="180">
141	Sehr nahe Gro&szlig;aufnahme. Der kronenf&ouml;rmige St&ouml;psel.</p>
          
<p>142	Die Dampfpfeife.</p>
          
<p>143-150	Zwischenschnitte der einzelnen Worte des Titels General Kornilow schreitet voran, dazwischen die Einstellungen der Dampfpfeife.</p>
          
<p>151	Zwischentitel:	Alle Kraft zur Verteidigung von Petersburg.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob12.jpg" align="left" height="130" width="180">
152	Totale einer Fabrik. M&auml;nner (Bolschewiken) mit Gewehren und Fahnen in der Hand laufen an der Kamera vorbei.</p>
          
<p>153	Die Dampfpfeife.</p>
          
<p>154	Zwischentitel: F&uuml;r Gott und Vaterland.</p>
          
<p>155	Zwischentitel: F&uuml;r </p>
          
<p>156	Zwischentitel: Gott</p>
          
<p>157	Die Kuppel einer Kirche</p>
          
<p>158	Nahaufnahme. Eine reichverzierte Ikone.</p>
          
<p>159	Eine hohe Kirchturmspitze, wobei die Kamera um 45 Grad nach links geneigt ist.</p>
          
<p>160	Die gleiche Kirchturmspitze, diesmal um 45 Grad nach rechts geneigt.</p>
          
<p>161-186	Einstellungen grotesker religi&ouml;ser Belder, Tempel, Buddhas,  afrikanischer Masken etc.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/oktober/oktob13.jpg" align="left" height="130" width="180">
187	Zwischentitel: F&uuml;r</p>
          
<p>188	Zwischentitel: das Vaterland.</p>
          
<p>189-199	Nahaufnahmen von Orden, beschm&uuml;ckten Uniformen, Offiziers-Epauletten etc.</p>
          
<p>200	Zwischentitel: Hurra!</p>
          
<p>201	Der Sockel der Zarenstatue. (Die Statue war von Arbeitern in der ersten Aktrolle niedergerissen worden.) Die auf der Erde liegenden Einzelteile der zerst&ouml;rten Statue fliegen zur&uuml;ck in ihre urspr&uuml;ngliche Position auf dem Sockel.</p>
          
<p>202	Zwischentitel: Hurra!</p>
          
<p>203	Der gleiche Vorgang wie in Nr. 201, von einem anderen Kamerastandpunkt aus.</p>
          
<p>204	Zwischentitel: Hurra!</p>
          
<p>205-209	Kurze Einstellungen von religi&ouml;sen Abbildungen, die wir schon vorher gesehen haben. Sie scheinen alle zu l&auml;cheln.</p>
          
<p>210-219	Andere Teile der Zarenstatue setzen sich wieder zusammen. Schlie&szlig;lich fliegen Zepter und Kopf mit kurzem Gewackel in ihre Position zur&uuml;ck.</p>
          
<p>220-233	Verschiedene Kirchturmspitzen, wie zuvor, ebenfalls geneigt. Eine Kirchturmspitze auf den Kopf gestellt. Weihrauchf&auml;sser werden geschwungen. Der Kopf der Zarenstatue befindet sich wieder in seiner alten Position. Ein Priester h&auml;lt sein Kreuz hoch.</p>
          
<p>234-259	General Kornilow, Anf&uuml;hrer der konterrevolution&auml;ren Armee, mustert hoch zu Pferd seine Truppe. Die Statue Napoleons auf dem R&uuml;cken eines Pferdes, den Arm weit ausgetreckt, Eine &auml;hnliche Einstellung von Kornilow, der ebenfalls den Arm hebt. Kerenskij, immer noch im Zarenpalast, blickt mit verschr&auml;nkten Armen auf den St&ouml;psel der Karaffe.</p>
          
<p>Zwischentitel: Zwei Bonapartes.</p>
          
<p>Noch mehr Einstellungen von der Statue Napoleons. Der Kopf Napoleons, wie er nach links blickt. Der Kopf Napoleons, wie er nach rechts blickt. Die beiden K&ouml;pfe zusammen auf der Leinwand, sich gegenseitig anblickend. Zwei groteske religi&ouml;se Figuren, wie zuvor, in der gleichen Position. Weitere Einstellungen der Statue von Napoleon und eine weitere Sequenz mit religi&ouml;sen Bildnissen.</p>
          
<p>260	Kornilow erteilt zu Pferde den Marschbefehl.</p>
          
<p>261	Ein Panzer bewegt sich vorw&auml;rts, &uuml;berrollt einen Sch&uuml;tzengraben.</p>
          
<p>262	Kerenskij w&auml;lzt sich im Schlafgemach der Zarin verzweifelt auf dem Bett herum.</p>
          
<p>263	 Teile der Napoleonb&uuml;ste liegen verstreut auf dem Boden.</p>
          
<p>Eisenstein stellt im Gegensatz zu Griffith keinen dramatischen Handlungsablauf dar. Er charakterisiert Kerenskij, ironisiert (wertet) seine Person. Das bildlich dargestellte entspricht nicht unbedingt dem zu vermittelnden Inhalt (Metaphern, Wortspiele, z. B. Einstellungen 40-74 - Pfau, Einstellung 118 - Statuette von Napoleon). Es gibt keine Kontinuit&auml;t in den einzelnen Sequenzen, in dem Sinne, da&szlig; eine Einstellung an die andere "anschlie&szlig;en" w&uuml;rde. Weder r&auml;umlich noch zeitlich ist ein ersichtlicher Zusammenhang gegeben (Einstellung 108 auf 109 - Wechsel von Bibliothek zu Treppenabsatz). Eisenstein versucht nicht <i>"die Illusion einer Geschichte durch sich logisch entwicklende Sequenzen zu schaffen<unquote>, sondern die </unquote>Bedeutung des Konflikts und den ideologischen Hintergrund des Konflikts zu erkl&auml;ren"</i> <small>(REISZ, S.30)</small>
</p>
          
<h4>3. "Dickens, Griffith und wir" - Abgrenzungen / Ideologie</h4>
          
<p>Griffith hat in seinem Film INTOLERANCE (1916) versucht, den Rahmen eines rein dramatisch gepr&auml;gten Erz&auml;hlverlaufs zu verlassen, oder wie es Eisenstein selbst beschrieben hat, &uuml;ber die <i>"Grenze der Fabel hinaus in das Gebiet der Verallgemeinerung und der Allegorie vorzusto&szlig;en"</i> <small>(EISENSTEIN, S. 199))</small>
</p>
          
<p>INTOLERANCE sollte vier Epochen in vier Handlungsstr&auml;ngen miteinander verflechten, die sich zun&auml;chst unabh&auml;ngig voneinander entwickeln, allm&auml;hlich ann&auml;hern, um sich schlie&szlig;lich laut Griffith <i>"im Finale zu einem ganz m&auml;chtigen Strom von aufw&uuml;hlender Emotion"</i> zu vereinigen.
Verbindendes Element war ein Motiv nach Walt Whitman, das refrainartig zu jeder neuen Epoche wiederkehrte: Lilian Gish, die eine Wiege schaukelte. Sinnbild f&uuml;r die immer wiederkehrenden, sich wiederholenden Abl&auml;ufe der Epochen. Das Bild wurde vom Publikum weder angenommen, noch verstanden.</p>
          
<p>Eisenstein stellt in seinem Artikel <i>"Dickens, Griffith und wir"</i> <small>(1941/42)</small> ausf&uuml;hrlich dar, worin er das grunds&auml;tzliche Mi&szlig;verst&auml;ndnis in Griffith Versuch sieht, eine Allegorie zu transportieren: Nicht "darstellende Montagebilder" ("Wiege") erzeugen Metaphern, sondern <i>"allein das Bild der Montagegegen&uuml;berstellung"</i> <small>(EISENSTEIN, S. 119)</small>. <i>"Griffith bleibt &uuml;berall beim nur Darstellenden und Gegenst&auml;ndlichen stehen und bem&uuml;ht sich nirgends, durch Gegen&uuml;berstellung der Bildausschnitte zum inneren Sinn und zum Bildlichen vorzudringen"</i> <small>(EISENSTEIN, S. 118)</small>. Nach Eisenstein also eine prinzipiell falsche Herangehensweise.</p>
          
<p>
            
<i>"Die Gr&uuml;nde hierf&uuml;r sind nicht professionell-technischer, sondern ideologisch-gedanklicher Natur.
Es geht hier nicht so sehr darum, da&szlig; die Darstellung bei richtiger Anlage und Bearbeitung nicht zu bildlicher, metaphorischer Aussage gesteigert werden kann oder da&szlig; Griffith von seiner Methode oder seiner handwerklichen Meisterschaft im Stich gelassen wird.
Es geht vielmehr darum, da&szlig; Griffith trotz seiner Versuche und Bem&uuml;hungen letztlich unf&auml;hig ist, Erscheinungen wirklich sinnvoll zu abstrahieren, das hei&szlig;t aus der Mannigfaltigkeit historischer Fakten die verallgemeinerte Deutung einer historischen Erscheinung herauszuarbeiten."</i> <small>(EISENSTEIN, S. 122)</small>
          
</p>
          
<p>Die Montageauffassung ist nach Eisenstein zun&auml;chst Ausdruck der herrschenden Gesellschaftsordnung. Erst durch eine entsprechende Erkenntnismethode, die des Marxismus, die Anwendung der Dialektik (Dialektik als Gesetz der Naturrevolution laut Engels und Lenin) sei <i>"zum ersten Mal die M&ouml;glichkeit zum Abstrahieren und zum Gebrauch &uuml;bertragener Begriffe"</i> <small>(EISENSTEIN, S. 123)</small> gegeben.</p>
          
<p>Nach seiner Auffassung h&auml;t der b&uuml;rgerliche Film stark an der gegenst&auml;ndlichen Abbildung fest, und strebt (z. B. durch Allegorie oder &Uuml;bertragung) keinen Erkenntnis- oder Lernproze&szlig; an. Stattdessern wird durch die g&auml;ngigen Montageverfahren die existierende Gesellschaftsordnung visualisiert und etabliert. So z. B. durch die Parallelmontage, die von Griffith sozusagen "perfektioniert" wurde: Durch diese materialisierte Darstellung zweier sich nie ber&uuml;hrender Lebensbereiche manifestiert sich das Bild der kapitalistischen Gesellschaft. Die Parallelit&auml;t beschreibt das Oben und Unten getrennter sozialer Schichten, die Entwicklung von Gefahr und Errettung oder das Schicksal der Liebenden, die getrennt bleiben bis zur Vereinigung im <i>"transzendierenden und entr&uuml;ckten"</i> <small>(SEE&szlig;LEN, S. 24)</small> Happy End. Ein Schnittpunkt oder Aufeinandertreffen dieser Linien bleibt seinem Wesen nach f&uuml;r Eisenstein irrational und unlogisch. </p>
          
<p>Erst die Dialektik erm&ouml;glicht die <i>"Entwicklung des Widerspruchs zwischen den Klassen, der auf die revolution&auml;re Tat hinausl&auml;uft"</i> <small>(SEE&szlig;LEN, S.24)</small>. Diese Dialektik entsteht erst durch die Montage. Nicht die Handlung oder Geschichte soll bei den Zuschauern einen Lernproze&szlig; einleiten, sondern auf einer anderen Ebene das Verfahren der Montage. Mit diesem Gedanken besch&auml;ftigt sich Eisenstein in seinem kompletten theoretischen und praktischen Werk.</p>
        
</subsubsection>
      
</subsection>
      
<subsection title="4. Die Entwicklung des Montagebegriffs">
        
<p>Man kann bei Eisensteins Montageauffassung nicht von einem sich kontinuierlich entwickelnden Theoriegeb&auml;ude sprechen. Er entwickelt Begriffe, l&auml;&szlig;t sie sp&auml;ter wieder fallen oder verwendet sie in einem anderen Bedeutungskontext - ohne in seinen Schriften daraufhinzuweisen. Die Anregungen dazu erh&auml;lt er aus den verschiedensten - meist film-fremden - Bereichen: meistens der Psychologie (seine Filme sollen den Zuschauer ver&auml;ndern, entprechend mu&szlig; er dessen Wahrnehmungproze&szlig; kennen), aber auch aus der Gestalt- oder Literaturtheorie, deren Begriffskanon er oft &uuml;bernimmt. <br>
Grundlegende Begriffe erfahren ensprechend verschiedene Bedeutungsstadien, so z. B. die Attraktion - das Grundelement der Montage - durchl&auml;uft drei Phasen: Strukturelement, Stimulus (Reflexologie, Pawlow), sprachliches Paradigma zum Bild.</p>
        
<p>Seine Filme lassen sich nicht parallel zu seinen theoretischen Schriften "lesen". Oft greift er mit ihnen voraus, kann erst sp&auml;ter deren neue Ans&auml;tze beschreiben oder sie in einem erweiterten Zusammenhang sehen. Entprechend finden sich Ans&auml;tze f&uuml;r seine intellektuelle Attraktion schon sehr fr&uuml;h in seinem Werk.</p>
        
<subsubsection title="4.1  Montage- und Attraktionsbegriff im Theater">
          
<p>1920 kam Eisenstein nach einem abgebrochenen Architekturstudium an das Moskauer Proletkulttheater. (Anm. 2) Die Proletkultkonferenz verstand Theater als die <i>"Synthese aus allen anderen Arten der Kunst - als &lt;Organisation menschlichen Handelns&gt;, als &lt;wesentliches Mittel f&uuml;r den Aufbau des Lebens nach dem Willen des Proletariats&gt;"</i> <small>(WEISE, S. 21)</small>. Er geh&ouml;rte dort dem linken Fl&uuml;gel an, der entgegen dem b&uuml;rgerlich, <i>"abbildend-erz&auml;hlenden Theater<unquote>, ein </unquote>Agitationstheater der Attraktionen"</i> <small>(WEISE, S. 23)</small> vertrat. Attraktionen im Sinne von "Nummern", die aneinadergef&uuml;gt, einen Lernproze&szlig; bei den Zuschauern bewirken sollten. 1922 verlie&szlig; er das Proletkult-Theater.</p>
          
<p>Er absolvierte einen dreimonatigen Montagekurs im Schnellverfahren bei Lew Kuleschow (Konstruktivist, erster sowjetischer Montage-Experimentator und Theoretiker), war 1922 Mitarbeiter von Esfir Schub bei der "ideologisch-korrekten" Ummontage von Fritz Langs DR. MABUSE, DER SPIELER (1922).</p>
          
<p>Den wichtigsten Einflu&szlig; erfuhr er im "Theaterlaboratorium" Wsewolod E. Meyerhold (Gr&uuml;nder des staatlichen Regieinstitutes in Moskau, 1921 - GVYRM) ein Konstruktivist, radikaler Theatererneuerer und linker K&uuml;nstler. </p>
          
<p>Er lehrte die von ihm entwickelte Biomechanik, eine verfremdende Ausdrucksbewegung und trieb die "Kinofizierung" des Theaters voran: den Zerfall des traditionellen Dramas in kleine, geschlossene Einheiten / Episoden, die frei montierbar waren, ohne Bindung an eine der Chronologie oder Ort-Zeit-Einheit. Die Handlung konnte auch durch einzelne "Nummern", dem Heraustreten aus der B&uuml;hnenrealit&auml;t, der direkten Ansprachen an die Zuschauer oder aktuelle Diskussionen unterbrochen werden. </p>
          
<p>Montage wurde von den Konstruktivisten in Analogie zum konstruktiven Produktionsproze&szlig; verstanden, der aus dem Bauelement ("Attraktion") und der Konstruktion (F&uuml;gung) besteht, die sie f&uuml;r das Theater entsprechend neu zu bestimmen versuchten (z. B. im Gegensatz zur Bildenden Kunst). 1927 schrieb Eisenstein entsprechend: <i>"Ich bin Zivilingenieur und Mathematiker von Beruf. Ich gehe an die Herstellung eines Films in gleicher Weise wie an die Einrichtung einer Gefl&uuml;gelfarm oder die Installation einer Wasserleitung. Mein Standpunkt ist ein durchaus utilitaristischer, rationeller, materialistischer."</i> <small>(BULGAKOWA, S. 39)</small>
</p>
          
<p>Eisenstein, der nach dieser Methode inszenierte, erkannte bald, da&szlig; die M&ouml;glichkeiten des Theaters f&uuml;r eine derartige Attraktions- bzw. Montagemethode eingeschr&auml;nkt waren. Die Auff&uuml;hrung vom GESCHEITESTEN (1923) beendete er mit einer weiteren "Attraktion" - seinem ersten Kurzfilm GLUMOWS TAGEBUCH. </p>
        
</subsubsection>
        
<subsubsection title="4.2 &quot;Montage der Attraktionen&quot;">
          
<p>1923 verfa&szlig;t Eisenstein sein 1. Manifest "Montage der Attraktionen", das noch sehr stark von seiner Theaterarbeit und der konstruktivistischen Lehre gepr&auml;gt ist. Der von ihm hier eingef&uuml;hrte Begriff <i>"Attraktion"</i> wurde zur Ma&szlig;einheit einer Auff&uuml;hrung erkl&auml;rt. Allerdings sieht er, entgegen seinen Kollegen, die Attraktion nicht mehr als "Trick", der gleich einer "Nummer" in einem Zirkus, einer Music Hall oder einem Nummernprogramm vor allem die Aufmerksamkeit des Publikums steigern soll. Eisenstein - und das ist ein v&ouml;lliges Novum - setzt die Ma&szlig;einheit des Bauelements "Attraktion" gleich mit der Ma&szlig;einheit der Wirkung, im Sinne eines Stimulus.</p>
          
<p>Diese Auffassung ist vor allem Ausdruck der damals sehr popul&auml;ren Theorien der Reflexologie und des Behaviorismus. Das visuelle Element der Attraktion f&uuml;hrt direkt zu einem physischen Stimulus / Reflex. Die Folge - also die Montage der Attraktionen - bewirken die entsprechenden Emotionen. Dabei ist die Attraktion als Bauelement bereits ein zusammengesetzter Reiz, was zu auseinanderstrebenden Emotionen f&uuml;hrt. Der Regisseur konzipiert dabei zwar den Endeffekt, jedoch erst der Zuschauer f&uuml;gt in seinem Wahrnehmungsproze&szlig; die Reize zusammen. Entsprechend bewegte sich die Betrachtung weg vom Kunstwerk und hin zu seiner Wirkung. </p>
          
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">STREIK (1924)</h3>
<p>
              
<i>"Grundlage des Szenariums sind die realen Ereignisse um einen Streik aus dem Jahre 1902, polit&ouml;konomische Studien der Autorengruppe und deren Erfahrungen, die sie bei Fabrikbesuchen, in Diskussionen dort mit Arbeitern und alten Revolution&auml;ren sammelt: <unquote>[...]</unquote> Arbeiter einer Fabrik bereiten den Streik vor. Der Direktor setzt eine Schar von Spitzeln zu &Uuml;berwachung ein. Als sich ein zu Unrecht des Diebstahls beschuldigter Arbeiter erh&auml;ngt, ist der Anla&szlig; zum Streik gegeben. Forderungen der Arbeiter wie der Acht-Stunden-Tag und h&ouml;here L&ouml;hne werden von der Fabrikverwaltung abgelehnt. Hunger zieht in die Arbeiterviertel ein, aber der Streik wird fortgesetzt. Mit allen Mitteln versucht die Polizei, die Streikenden zu provozieren. Als sich zeigt, da&szlig; die Streikfront immer fester wird, setzten die Ortsbeh&ouml;rden Kosaken ein, die in den Mietskasenen der Arbeiter ein gro&szlig;es Blutbad anrichten. - Dem Film vorangestellt ist ein Lenin-Zitat aus dem Jahre 1907: 'Die Kraft der Arbeiterklasse ist die Organisation. Ohne Organisation der Massen ist das Proletariat nichts. Ist es organisiert, ist es alles.'"</i> <small>(WEISE, S.34)</small>
            
</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
          
<p>Durch die reflexologische Betrachtung wurde gleichzeitig die Hierarchie der Einwirkungsmitte au&szlig;er Kraft gesetzt. Ein Paukenschlag als Stimulus hat den gleichen Wert wie ein Shakespearemonolog - akustische, visuelle, taktile, olfaktorische Reize sind gleichberechtigt.
<i>"Die konsequente Schlu&szlig;folgerung aus dem Manifest lautet: Attraktionsmontage ist die Schaffung einer Kette von bedingten Reflexen. Die Kunst ist ein Training im sozialen Reagieren. Der Theater- oder Film-Ingenieur entwickelt eine Kombination von Reizen, eine klassische Konditionierung. Reize k&ouml;nnen arbitr&auml;r gekoppelt werden, um soziale Reflexe zu trainieren (wie Klassenha&szlig; und Klassensolidarit&auml;t). Attraktion tritt als ein zusammengesetzter, &lt;komplexer&gt; Reiz auf."</i> <small>(BULGAKOWA, S. 40)</small>
</p>
          
<p>Da&szlig; die Attraktion im Film wesentlich einfacher einsetzbar ist als im Theater ist die konsequente Schlu&szlig;folgerung. Die Abbildung ist per se bereits abstrahierend, d. h. nur ein Bild von der Realit&auml;t, sie erfordert bereits eine "Assoziation" und der prinzipielle Aufbau eines Filmes, die technisch erforderliche Kopplung, beinhaltet schon eine Form der Montage.haltet schon eine Form der Montage. Die Hinwendung zum Film bedeutet den Bruch mit seinem k&uuml;nstlerischen Ziehvater Meyerhold.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/massaker.jpg" align="left" height="437" width="250">Die Montage der Filmattraktion kann sich allerdings von der blo&szlig;en physiologischen Einwirkung entfernen und zu einer Abstraktion f&uuml;hren. Eisenstein belegt dies anhand eines Ausschnittes aus seinem ersten Spielfilm STREIK (1924), den er selbst als <i>"Oktober des Films."</i> <small>(KAUFMANN, S. 55)</small> bezeichnet hat.</p>
          
<p>Am Ende des Films ist zu sehen, wie die Streikenden niedergeschossen werden. Zwischen die Aufnahmen von den Demonstranten - meist in der Totalen - schneidet Eisenstein die authentische Abschlachtung eines Ochsen im Schlachthof - er geht dabei immer mehr in die Gro&szlig;aufnahme, bis am Schlu&szlig; nur noch das tote weit ge&ouml;ffnete Ochsenauge zu sehen ist.  <i>"Das physiologisch empfundene Entsetzen angesichts des realen T&ouml;ten und des Todes wird &uuml;bertragen auf die Szene des Menschenmassakers, das, wie Eisenstein meinte, kein Schauspieler so schaurig und real darstellen k&ouml;nne. Nicht der logisch nachvollzogene Vergleich (Schlachthof - Massaker) ist dabei von Bedeutung. Sondern die &Uuml;bertragung der emotionalen Ersch&uuml;tterung dient dem notwendigen ideologischen Effekt, der mit dem Zwischentitel &lt;vergi&szlig; nicht, Proletarier!&gt; einen Schlu&szlig;punkt setzt."</i> <small>(BULGAKOWA, S. 41f)</small>  D. h. Eisenstein erwartete von den Rezipienten / Zuschauern eine rationalisierte Verallgemeinerung Assoziationen.</p>
          
<p>Hier zeigt sich der Widerspruch zwischen seinem Anspruch und seiner damaligen Montagetheorie: H&auml;tte Eisenstein damals mehr &uuml;ber Pawlows Versuche gewu&szlig;t, so schreibt er sp&auml;ter, h&auml;tte er Attraktion gleich als Stimulus bezeichnet. Da wird deutlich, wie eng der Attraktionsbegriff noch an die klassische Konditionierung gebunden ist: Stimulus - Reaktion. Das Bewu&szlig;tsein, der Entscheidungsproze&szlig; bleibt ausgeschlossen. Dem K&uuml;nstler wird die Freiheit der (Massen-) Manipulation zugesprochen. Es stellt sich die Frage nach der Programmierbarkeit von Assoziationsketten. Zudem - stereotype Reaktionen auf Grund von Konditionierung beinhalten zwar einen Lernproze&szlig;, jedoch keinen (bewu&szlig;ten) Erkenntnisproze&szlig;.
In seinem Artikel "Die Inszenierungsmethode eines Arbeiterfilms" (1925) sieht Eisenstein die Grenzen, die der Assoziation bzw. der &Uuml;bertragbarkeit gesetzt sind. Sie setzten eine "Lesef&auml;higkeit" der Zuschauer voraus. Die kann, je nach Bev&ouml;lkerungsgruppe, sehr verschiedenen sein:</p>
          
<p>
            
<i>"Bei dem Arbeiterpublikum l&ouml;ste der Schlachthof durchaus keinen 'blutigen' Effekt aus, und zwar aus dem einfachen Grund, weil f&uuml;r den Arbeiter das Blut hier in erster Linie die Assoziation zu den bei Schlachth&ouml;fen &uuml;blichen Verarbeitungsfabriken herstellt. Auf den Bauern gar, der selbst gw&ouml;hnt ist, Vieh zu schlachten, wird die Wirkung gleich Null sein."</i> <small>(KAUFMANN, S. 60)</small>
          
</p>
          
<p>Und: je heterogener die Zuschauergruppe zusammengesetzt ist, um so kleiner ist ihr gemeinsamer Assoziationskanon.</p>
          
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">PANZERKREUZER POTEMKIN (1925)</h3>
<p>
              
<i>"PANZERKREUZER POTEMKIN h&auml;lt sich im wesentlichen an die historische Vorlage des Matrosenaufstandes. Nach den Niederlagen im Russisch-Japanischen Krieg versch&auml;rft sich in der Schwarzmeerflotte der Druck des Offizierkorps auf die Mannschaften. die Unruhe unter den Matrosen wird gr&ouml;&szlig;er, und bolschewistische Kader planen f&uuml;r September 1905 einen Flottenaufstand. Am 14. Juni weigert sich eine Gruppe von Matrosen auf dem Panzerkreuzer 'F&uuml;rst Potemkin von Taurien', verdorbenes Fleisch zu essen. Als die Matrosen erschossen werden sollen, meutert die gesamte Mannschaft. Die Offiziere werden verhaftet, das Schiff l&auml;uft den Hafen von Odessa an, um einen von den Offizieren ermordeten bolschewistischen Matrosen zu bestatten. Die gerade streikende Odessaer Arbeiterschaft solidarisiert sich mit den Aufst&auml;ndischen und erweist dem Toten die letzte Ehre. Drei Tage sp&auml;ter f&auml;hrt der Panzerkreuzer dem zur Unterdr&uuml;ckung der Meuterei herbeieilenden Admiralsgeschwader entgegen. Die 'Potemkin'-Matrosen fordern die Mannschaften der Flotte auf, sich der Rebellion anzuschlie&szlig;en. Die Aufst&auml;ndischen d&uuml;rfen passiern, und der Panzerkreuzer 'Georgi Pobedanosez' schlie&szlig;t sich ihnen an, jedoch nur f&uuml;r kurze Zeit. Die 'Potemkin' bleibt isoliert, und als das Schiff den Hafen von Konstanza anl&auml;uft, wird es von der rum&auml;nischen Regierung interniert und an Ru&szlig;land ausgeliefert. Die meisten der aufst&auml;ndischen Matrosen werden nach ihrer R&uuml;ckkehr in die Heimat von den zaristischen Beh&ouml;rden umgebracht."</i> <small>(WEISE, S. 46f)</small>
            
</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
        
</subsubsection>
        
<subsubsection title="4.3  Montage als Konflikt und Dominante-Oberton">
          
<p>1925 hat Eisensteins zweiter Film PANZERKREUZER POTEMKIN Premiere in Berlin. Das Publikum - das b&uuml;rgerliche! - reagiert mit Begeisterung. Auf Reize, Stimuli, die eigentlich das Proletariat aufr&uuml;tteln sollten. Der Wahrnehmungsproze&szlig; lie&szlig; sich also nicht nur auf soziale Stimuli einschr&auml;nken.<br>
Es wird deutlich, da&szlig; sein nur reflexologisches Analysemodell als eine lineare Kette von Stimuli den Wahrnehmungsproze&szlig; der Zuschauer nicht beschreiben konnte. 1929 erweiterte Eisenstein das Modell in seinem Artikel "Jenseits der Enstellung" um den Begriff des Konflikts. Angeregt dazu wurde er durch die Bekanntschaft mit dem Psychologen Luria - Eisentein &uuml;bertrug dessen Konfliktbegriff auf die leninistische Dialektik. F&uuml;r ihn liegt der Konflikt innerhalb einer Einstellung, sie <i>"akkumuliert Konflikte (des Vorder- und Hintergrunds, der Linien, Konturen, Volumen, Lichtflecken, Massen Bewegungsrichtungen, Beleuchtung, eines Vorgangs und seiner zeitlichen Darstellung in Zeitlupe oder Zeitraffer"</i> - fast alle aufgez&auml;hlten Konflikte sind visueller Natur - <i>"sie &lt;zerrei&szlig;en&gt; das Bild und werden durch die n&auml;chste Einstellung ab(auf)gel&ouml;st."</i> <small>(BULGAKOWA, S. 48)</small> Der Konflikt kann aber auch zwischen zwei Einstellungen bestehen. Gem&auml;&szlig; dem 2. Gesetz der Dialektik kann das Nebeneinander der konfliktreichen Einstellungen  (Quantit&auml;t), <i>"(potentiell) einen Sprung in die neue Qualit&auml;t bedeuten"</i> <small>(BULGAKOWA, S. 48)</small>. Oder anders: Dieser Sprung am Schnittppunkt zweier materieller Bilder kann &uuml;ber die Vorstellung / Wahrnehmung des Zuschauers / der Zuschauerin in eine nicht-materielle Ebene erfolgen - z. B. in die einer ideologischen Erkenntnis. Dadurch erh&auml;lt Eisensteins Analysemodell eine neue Dynamik im Wahrnehmungsproze&szlig;. Es geht nicht mehr um die Addition gleichwertiger Stimuli, sondern um eine strukturierte Wahrnehmungsarbeit des Zuschauers / der Zuschauerin.</p>
          
<p>Innerhalb der Wechselbeziehung zwischen den Einstellungen findet eine dynamische Indergration der einzelnen Elemente statt - die Br&uuml;che, Widerspr&uuml;che verarbeiten kann ohne die Einheitlichkeit zu verlieren. Ein linearer Wahrnehmungsproze&szlig; dagegem k&ouml;nnte nur kausale Zusammmenh&auml;nge darstellen, z. B. eine r&auml;umliche oder zeitliche Kontinuit&auml;t.</p>
          
<p>Zeitgleich zu dieser Montage als Konstruktion &uuml;ber Attraktion (bzw. Konfkikt) erwickelte er das Modell der Montage als Konstruktion &uuml;ber Dominante. Diese beschreibt er in "Die vierte Dimension im Film" (1929). Beides sind an sich kontr&auml;re Modelle. Die Dominante leitet Eisenstein aus der noch in den Kinderschuhen steckenden Gestaltpsychologie ab, die er &uuml;ber den Psychologen Wygotski kennengelernt hatte. Dessen Besch&auml;ftigung mit der Ekstase und der Natur des Reizes f&uuml;hrte ihn zu einem kathartischen Kinstmodell. Nach seinem Ansatz setzt die "Evolution" dort ein, <i>"wo der Mensch k&uuml;nstliche Zeichen einf&uuml;hrt: k&uuml;nstlich geschaffene Reize, die das Verhalten steuern und die Bildung neuer bedingter Verbindungen hervorrufen"</i> <small>(BULGAKOWA, S. 46)</small>. Die Erweiterung des Zeichenvorrats dient der Sozialisation der Pers&ouml;nlichkeit. Die Aneingnung des Zeichenvorrats ist ein bewu&szlig;ter Proze&szlig; des Rezipienten / der Rezipientin.</p>
          
<p>Eisenstein deutet diese Zeichen als visuelle Attraktions-Zeichen. Sind sie Hauptmerkmal bzw. stehen sie im Vordergrund einer Einstellungssequenz, nennt er sie Diminante. Es k&ouml;nnnte sich dabei z. B. um den Vordergrund, die wichtigste Bewegungsrichtung, das Tempo usw. Handeln.
In seiner eigenen Analyse einer Sequenz aus dem Panzerkreuzer (1934) beschreibt er die Dominate - die Bewegungsrichtung, Vertikale - Horizontale - Halbkreisen - und deren &Uuml;berg&auml;nge: <i>"In der Komposition der Einstellung dominieren Vertikalen, doch die Bewegung ist horizontal; die Vertikalten der Komposition werden von Halbkreisen verdr&auml;ngt, aber die Beibehaltung der Bewegungsrichtung sorgt f&uuml;r eine Verflechtung. Dann &auml;ndert sich die Richtung der Bewegung (von links-rechts zu unvermitteltem rechts-links) unter Beibehaltung der Dominanz vom Halbkreis in der Komposition. In der letzten, einenden Einstellung kommen die auseinaderstrebenden Tendenzen (Horizontale und Vertikale, bewegung links-rechts und rechts-links) zusammen, vorbereitet durch ausgekl&uuml;genten Kombinationswechsel davor."</i> <small>(BULGAKOWA, S. 142)</small>
</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/dominante.jpg" align="left" height="290" width="250">Die Dominante wirkt verkn&uuml;pfend. Erst in der Kombination enth&uuml;llt sie ihren Charakter - sie kann nich alleine sthe, ist relativ und ver&auml;nderlich. Die Montage nach Dominaten erm&ouml;glich dabei ein differeniertes Spektrum von &Uuml;berg&auml;ngen - vom Hin&uuml;bergleiten in die n&auml;chste Einstellung bis zu derem totalen Gegensatz. Montage bestimmt so im Konflikt die semantische Eigenart des Films.</p>
          
<p>Die in den Anf&auml;ngen formulierte Montage als Summe oder offene Reihung, versteht Eisenstein jetzt als komplexeren, hierarchischen Zusammenhang aus Einheit, Struktur und System. Von der konstruktivistischen Attraktionsmontage hin zur Montage als dynamische Integration. Jetzt ist die Dominate das strukturgebende Prinzip und der Einzelbaustein. Sie l&ouml;st dabei das konstruktive Prinzip als Verfahren ab - vergleichbar dem dynamisierten Attraktions-Konfliktbegriff. Entscheidend sind dabei die Differenzqualit&auml;ten, die die Beziehung zwischen den dominierenden und untergeordneten Elementen ausmachen. Die einstigen Au&szlig;enfaktoren (Ausrei&szlig;er aus einer Reihe) werden jetzt zu innertextlichen Parametern.
<i>"Die Wahl der Dominante ist ein Ergebnis des Kampfes zwischen verschiedenen formbildenden Elementen"</i> <small>(BULGAKOWA, S. 50)</small>. Entsprechend dynamisch ist der Aufnahmeproze&szlig; des Betrachters. Er synthetisiert die Konflikte und ordnet sie in ein hierarchisches System.
Um den Umgang mit der Dominanten, die Strukturierungsarbeit f&uuml;r den Zuschauer zu vereinfachen, bem&uuml;hten sich die Konstruktivisten und anfangs auch Eisenstein, sie herauszuarbeiten und von unwichtigeren bzw. unbwu&szlig;t wirkenden Bildreizen zu befreien. Durch Akzentuierung (Ausschnitte, Perspektivenwahl) oder Reduktion der restlichen Bildreize. Dadurch erhofften sie sich eine eindeutigere Lesbarkeit.
Eisenstein interressierte sich allerdings gerade f&uuml;r diese so schwer greifbaren psychologischen Begleiterscheinungen, Anh&auml;ngsel der Dominanten. In Anlehnung an den amerikanischen Psychologen W. James bezeichnete er sie als Oberton. In diesem Oberton schwingt das Empfinden noch &uuml;ber eine Einstellung hinaus weiter, und &uuml;berlagert diese in Form von nicht abgeklungenen Prozessen. Damit entsteht eine Spannung <i>"zwischen den pr&auml;valenten und sich verdr&auml;ngenden, oszillierenden semantischen Paradigmen"</i> <small>(BULGAKOWA, S. 52)</small>. D. h. bei bewu&szlig;t visuell wahrgenommenen Dominanten klingen immer noch unbewu&szlig;t wahrgenommene obertonale Begleiterscheinungen mit an - physiologische und strukturelle Momente verschmelzen - dadurch erh&auml;lt Eisenstein eine sinnliche Komponente in der Montage.</p>
          
<p>Allerdings f&auml;llt es Eisenstein schwer, die Komplexit&auml;t des Obertonbegriffs zu fassen. Die Bestimmung des Konflikts zwischen zwei Einstellung verliert zunehmend f&uuml;r ihn an Bedeutung, ihn interessieren st&auml;rker die Gesetze der Verbindung. Und vor allem die der anderen Stimuli, die Obert&ouml;ne, das "Unfa&szlig;bare". So l&auml;&szlig;t er nach dem Aufsatz "Die vierte Dimension im Film" (1929) den Begriff Dominante fallen und widmet sich dem Unsichtbaren, visuell nicht Greifbaren. Das f&uuml;hrt ihn schlie&szlig;lich zur intellektuellen Montage. </p>
        
</subsubsection>
        
<subsubsection title="4.4  Intellektuelle Attraktion IA-28">
          
<p>Der Begriff der Intellektuellen Attraktion taucht bei Eisenstein erstmals 1928 im  Zusammenhang mit seinem Film OKTOBER (1927) auf.</p>
          
<p>Begonnen hatte alles mit einem "harmlosen Sprachspiel" im PANZERKREUZER POTEMKIN, wo sich in der Folge  einer Montagesequenz drei verschiede Steinskulpturen der Odessaer Treppe von einem schlafenden zu einem br&uuml;llenden L&ouml;wen erhoben - als Bild des Idioms <i>"Die Steine br&uuml;llen!"</i>. Was hatte Eisenstein getan (abgesehen davon, da&szlig; diese Sequenz vielfach mi&szlig;verstanden wurde)? Eine sprachliche Metapher bildlich umgesetzt und eine Bewegungsillusion geschaffen.</p>
          
<h4>Sprachparadigmen und Bild-Wort-Spiele</h4>
          
<p>Eisenstein zielte auf die kognitive Ebene des Zuschauers ab, die Ebene der konzeptualen Erkenntnis. Er begann sich intensiver mit Sprachaufbau und Metaphorik auseinanderzusetzen,  - Ans&auml;tze, die er teilweise schon w&auml;hrend seinter Theaterarbeit verfolgt hatte. In OKTOBER findet sich dann eine "Materialisierung" bekannter Sprachmetaphern.</p>
          
<p>
<i>"Das Spiel der Syllogismen l&ouml;st das lebendige Spiel der Leidenschaften ab... hier wird erstmalig einschieden eine prinzipielle Linie zwischen Theater und Film gezogen... das Attraktionsprinzip ist unzerst&ouml;rbar. 1928 wird die Attraktion korrigiert: im Theater zielt sie auf das Gef&uuml;hl, im Film auf das Bewu&szlig;tsein."</i> <small>(BULGAKOWA, S. 55)</small>
Im Theater sieht er einen Reiz als <i>"ein Stimulus, der an den unbedingen Reflex gerichtet ist"</i>, im Film, bei der intellektuellen Attraktion handelt es sich um einen "assoziativen" Reflex, der bereits auf die Sprache und Sprachspiele ausgerichtet ist.</p>
          
<p>Zun&auml;chst untersucht und verwendet er Metaphern und Metonymien. Vor allem in OKTOBER vielfach als Scherze oder Ironisierung. Z. B. nach einem Zwischentitel <i>"Die Kosaken haben Kerenski verraten"</i> (Im Russischen gleichbedeutend mit "betrogen") schneidet er auf ein Hirschgeweih in Kerenskis Zimmer. Er stellt Kerenski einen Pfau entgegen (vgl. Bildsequenz in 3. Aktrolle OKTOBER, Kapitel 2.2), Rednern eine Lyra, dem Menschewiken eine Balalaika. Nicht immer wurden diese Wort-Bild-Spiele vom Publikum verstanden bzw. angenommen.</p>
          
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/loewe.jpg" align="right" height="407" width="180">
Der stark abbildend-darstellende Charakter, den Eisenstein typisch f&uuml;r die europ&auml;ische Kunst sah, schien eine Abstraktion auf einer kognitiv-sprachlichen Ebene zu erschweren. Zum einen sah er eine M&ouml;glichkeit in der Transformation der Bilder, die das Figurativ-Abbildende unterdr&uuml;cken sollte: Deformation und Verfremdung durch Licht, Optik, Aufnahmewinkel, Bildkomposition, Isolierung in Nahaufnahme, Substitution des Ganzen durch einen Teil dank der Zergliederung im Bildausschnitt usw. Z. B. in der vorgestellten Aktrolle aus OKTOBER kippt die Kamera um 45&iexcl; bei den Kirchturmaufnahmen und leitet so die G&ouml;ttersequenz ein.
Zum anderen begann er sich mit japanischer Hieroglyphik auseinanderzusetzen. Begriffe ergeben sich dort meist aus zusammengesetzten Zeichen, so zum Beispiel</p>
          
<blockquote>
Hund + Mund = bellen<br>
Auge + Wasser = weinen<br>
Messer + Herz = Trauer<br>

</blockquote>
          
<p>&Uuml;bertragen bedeutete dies f&uuml;r Eisenstein, da&szlig; das zusammengesetzte Zeichen dem Zusammenprall zweier unabh&auml;ngiger Filmeinstellungen entsprach. Dies waren nicht mehr blo&szlig;e Bildzeichen, sondern ein <i>"Symbol im Werden"</i> <small>(BULGAKOWA, S. 60)</small>. Von jetzt an betrachtete er nicht mehr die Analogie von Wort und Bild, sondern die Analogie der semantischen Prozesse, sprach von der Rekonstruktion der Denkmechanismen: logisch, intellektuell, dialektisch. Das Filmbild - Ideogramm.</p>
          
<p>
            
<i>"Zwischen den in die Montage eintretenden intellektuellen Attraktionen liegt die &lt;&Auml;hnlichkeit&gt; nicht im Sinnlichen. Also im Absoluten und Nicht-&Auml;u&szlig;erlichen... Ein Barock-Christus und ein Holzklotz sind einander absolut un&auml;hnlich, bedeuten jedoch dasselbe. Balalaika und Menschewik sind sich nicht physisch, sondern abstrakt &auml;hnlich"</i> <small>(BULGAKOWA, S. 61)</small>
          
</p>
          
<p>Eine Einstellung entspricht einem Teilbegriff, die Begriffsbildung entsteht durch die Montageoperation. Somit wird aus einem statischen Proze&szlig; (einzelne Assoziation) ein dynamischer.</p>
          
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">OKTOBER 1927</h3>
<p>
              
<i>"Der Film beginnt mit der Schilderung der 'Beseitigung' des Zarismus - eine Menschenmenge rei&szlig;t im Moskau die Statue Alexanders III., das Symbol der Selbstherrschaft, vom Sockel. an der Front verbr&uuml;dern sich die russischen Soldaten mit feindlichen Truppen. Doch die Hoffnung des Volkes auf Brot, Friede und Land wird bald entt&auml;uscht: die provisorische Regierung erledigt weiterhin die Gesch&auml;fte des Kapitals, also wird auch der Krieg fortgesetzt. Im April kehrt Lenin aus der Schweiz nach St. Petersburg zur&uuml;ck; der riesigen Menschenmenge, die ihn am Bahnhof empf&auml;ngt, ruft er zu: 'Nieder mit der provisorischen Regierung! Alle Macht den Sowjets! Es lebe die sozialistische Revolution!' Im Juli veranstalten die Bolschewiki - f&uuml;r einen Aufstand ist es noch zu fr&uuml;h - in Petersburg eine friedliche Demonstration; die Regierungstruppen schie&szlig;en auf die Menschenmenge und lassen die Klappbr&uuml;cke &uuml;ber die Newa &ouml;ffnen, um das Stadtzentrum von den Arbeitervierteln abzuschneiden, das Hauptquartier der Bolschewistischen Partei wird &uuml;berfallen und gepl&uuml;ndert und viele Bolschewiki werden verhaftet - Lenin mu&szlig; erneut in die Illegalit&auml;t, er flieht nach Finnland. Inzwischen richtet sich der neue Ministerpr&auml;sident der provisorischen Regierung im Winterpalais des Zaren ein.<br>
Im September marschiert General Kornilow mit der 'Wilden Tatarendivision' und englischen Panzern - auf Petersburg f&uuml;r die Monarchie. Die Regierung ist machtlos, doch die Bolschewiki bewaffnen sich und stellen sich den Putschisten entgegen. Die 'Wilde Division' verbr&uuml;dert sich mit den revolution&auml;ren Arbeitern und Soldaten und Kornilow wird verhaftet. Jetzt beginnt der Petersburger Sowjet mit den Vorbereitungen zum bewaffneten Aufstand. Lenin kehrt zur&uuml;ck, und unter seinem Vorsitz beschlie&szlig;t das ZK der Bolschewistischen Partei, am 25.Oktober w&auml;hrend der Tagung des Allrussischen Sowjetkongresses die Partei den Generalstab f&uuml;r den Aufstand. Am Morgen des 25. Oktober erhalten die Revolution&auml;re Verst&auml;rkung durch den Panzerkreuzer 'Aurora'. Die Newa-Br&uuml;cke wird wieder geschlossen. Kerenski bittet vergeblich um die Unterst&uuml;tzung des sich neutral verhaltenden Kosakenregiments; im Wagen der US-Botschaft flieht er. Das Winterpalais wird nun von Kadetten und einem Frauenbataillon bewacht. Auf dem Sowjetkongre&szlig; versuchen Menschewiki und Sozialrevolution&auml;re (Karenskis Partei) abzuwiegeln, doch bei der Wahl zu neuen ZK erhalten die Bolschewiki die Mehrheit, und von der Front zur&uuml;ckgekehrte Garnisonen und Bataillone schlie&szlig;en sich ihnen an. Nun besetzen die Revolution&auml;re alle strategisch wichtigen Punkte der Stadt und umstellen das Winterpalais. Die Sch&uuml;sse der 'Aurora' auf das Palais sind das Signal zum Angriff. Die Verteidiger sind schnell &uuml;berw&auml;ltigt. Die aufst&auml;ndischen Massen dringen in den Palast der 1100 R&auml;ume. Die zur&uuml;ckgebliebenen Minister werden verhaftet. Am Ende des Film zeigen verschiedene Zifferbl&auml;tter mit verschiedenen Weltzeiten die Stunde der Revolution und Lenin verk&uuml;ndet auf dem Sowjetkongre&szlig;: 'Die Arbeiter- und Bauernrevolution ist vollendet.'"</i> <small>(WEISE, S. 62ff)</small>
            
</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
          
<h4>Bewegungsillusion und Filmsemantik</h4>
          
<p>Eine andere Entdeckung aus der Steinl&ouml;wensequenz war die M&ouml;glichkeit der Bewegungsillusion: drei statische Einstellungen ergaben eine zusammenh&auml;ngende Bewegung. Die gleiche Entdeckung bei einer Gewehrschu&szlig;sequenz aus dem PANZERKREUZER: das Nacheinanderschneiden von hellen und dunklen Einstellungen eines Maschinengewehrs erwecken den Eindruck des Schie&szlig;ens (Flickering). Oder - wieder im PANZERKREUZER - die Superposition zweier Einstellungen (Kuleschow-Effekt): eine Frau mit Zwicker und gleich darauf mit zerschlagenem Zwicker erwecken den Eindruck eines Schusses. </p>
          
<p>Wie gro&szlig; kann entsprechend die Diskrepanz zwischen zwei Einstellungen sein, damit dort noch eine Bewegungsillusion entsteht? Nicht mehr das Konfliktpotential zweier gegens&auml;tzlicher Einstellungen nahmen Eisensteins Interesse ein, sondern das, was dazwischen lag, die Schnittstelle. Er bezeichnet sie mit den Begriffen Intervall oder Ri&szlig; zwischen den Bildern.</p>
          
<p>Zum einen begreift er die Bewegungsillusion als <i>"technisch-optische Grundlage"</i> des Mediums Film &uuml;berhaupt: <i>"Das Bewegungs-Ph&auml;nomen des Films liegt darin, da&szlig; zwei unbewegliche Bilder eines bewegten K&ouml;rpers in aufeinanderfolgenden Positionen bei schnellem nacheinander Zeigen in Bewegung verschmelzen."</i> <i>"Der Bewegungs-Begriff (Empfindung) entsteht im Proze&szlig; der Superposition (&Uuml;berlagerung) des behaltenen Eindruckes der ersten Position des Objektes und der sichtbar werdenden Position des Objektes."</i> <small>(BULGAKOWA, S. 68)</small> Das gleiche gilt f&uuml;r das Empfinden einer Raumtiefe im Film durch die &Uuml;berlagerung zweidimensional divergierender Einstellungen. </p>
          
<p>Ebenso betrachtet Eisenstein die Verdichtung semantischer Prozesse: <i>"Das Entstehen neuer Begriffe und Anschauungen im Konflikt zwischen &uuml;blicher Vorstellung und einzelner Darstellung betrachte ich ebenfalls als Dynamik - Dynamisierung der Anschauungstr&auml;gheit..."</i> <small>(BULGAKOWA, S. 68f)</small>
</p>
          
<p>Olga Bulgakowa beschreibt dies folgenderma&szlig;en: <i>"Intervall ist nicht nur ein Ri&szlig;, der die Rezeption der Bewegung im Film ins Bewu&szlig;tseim r&uuml;ckt, die Risse f&ouml;rdern die Semantisierung im hohen Ma&szlig;e. Denn durch das Akzentuieren des Risses - kein Zeigen eines feuernden Maschinengewehres, sondern &Uuml;berlagerung dunkler und heller Einstellungen - wird der Begriff des Schie&szlig;ens verdeutlicht, es bleibt nicht bei der Darstellung."</i> <small>(BULGAKOWA, S. 70)</small>
Das Intervall steht f&uuml;r das Spiel zwischen Anwesenheit-Abwesenheit, Vergangenem-Zuk&uuml;nftigem. Dadurch entsteht eine spannungsbildende Polarit&auml;t.</p>
          
<p>Eisenstein schafft Bewegungsillusion auch dort, wo Objekte sich nicht bewegen k&ouml;nnen. In der eingangs beschriebenen Sequenz aus OKTOBER sieht man Kerenski die Treppe hinaufsteigen. Trotzdem bleibt er selbst konstant, steigt immer wieder die Treppe hinauf, tats&auml;chlich scheinen sich nur die ihn kr&ouml;nenden Statuen zu bewegen. </p>
          
<p>Eine andere Sequenz aus OKTOBER (Aktrolle 3), die "G&ouml;tter"sequenz, zeigt noch einmal deutlich Eisensteins Anspruch, mit Bildassoziationen den Zuschauer auf einer intellektuellen Ebene zu erreichen. (Sequenz entspricht Einstellungen 234-259, laut Kapitel 2.2) - durch Bild-Wort-Spiele, Ideogramme, Bewegungsillusion.</p>
          
<p>
            
<i>"Kornilows Marsch auf Petrograd geschah unter dem Banner &lt;f&uuml;r Gott und Vaterland&gt;. Wir versuchen hier, den religi&ouml;sen Hintergrund dieser Episode rationalistisch aufzudecken. Eine Reihe von religi&ouml;sen Bildern, angefangen von einer pr&auml;chtigen Barockstatue Jesu bis hin zu einem Eskimo-Gott, wurden zusammengeschnittten. Der Widerspruch bestand in diesem Fall zwischen der Idee von Gott und seiner Symbolisierung. W&auml;hrend bei der zuerst gezeigten Statue Idee und Bild vollkommen identisch erscheinen, entfernen sich die beiden Elemente mit jedem weiteren Bild immer mehr voneinander. Indem wir die Bezeichnen &lt;gott&gt; weiter aufrechterhalten, entfernen sich die Bilder zunehmend von unserer Vorstellung von Gott, so da&szlig; dies unweigerlich zu eigenen Ruckschl&uuml;ssen &uuml;ber die wahre Natur von Gottheiten f&uuml;hren mu&szlig;. In diesem Fall versucht eine Kette von Bildern eine rein intellektuelle Schlu&szlig;folgerung zu erzeugen. Sie ergibt sich nach und nach aus dem Konflikt einer bestimmten Vorstellung (von Gott) mit ihrer allm&auml;hlichen Diskreditierung."</i> <small>(REISZ, S. 32)</small>
          
</p>
          
<p>
            
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/eisenst/goetter.jpg" align="left" height="230" width="440">
          </p>
          
<p>Die "G&ouml;tter"sequenz endet damit, da&szlig; die Einstellung eines barocken Jesus (der in den kreisf&ouml;rmigen Strahlen seines Heiligenschein zu explodieren scheint) mit einer eierf&ouml;rmigen Maske von Uzume, der Gottheit von Mirth, in kurzen Abst&auml;nden unterschnitten wird. <i>"Der Widerstreit zwischen der geschlossenen Eiform und dem sehr graphischen Sternkranz erzeugte den Effekt einer sich st&auml;ndig wiederholenden Explosion - einer Bombe oder eines Schrapnells"</i> <small>(REISZ, S. 34)</small> - der &Uuml;bergang in eine Bewegungsillusion.<br>
Damit wird aber auch deutlich, wie schwierig es im Einzelfall f&uuml;r den Zuschauer sein kann, Eisensteins angedeutete Gedankeng&auml;nge nachzuvollziehen. Vieldeutige oder stark spezialisierte "Begriffe" bergen immer die Gefahr des Miss- oder Unverst&auml;ndnisses. Eisenstein setzt letztlich stets ein <i>"korrespondierendes sprachliches Paradigma"</i> voraus.</p>
        
</subsubsection>
      
</subsection>
      
<subsection title="6. Eisensteins Kategoriesierung der Montage - Zusammenfassung">
        
<p>In seinem Artikel "Die vierte Dimension im Film" fa&szlig;t Eisenstein seine Unterscheidung der verschiedenen Kategorien der Montage noch einmal zusammen. Er differenziert zwischen der metrischen, der rhythmischen, der tonalen, der obertonalen und der intellektuellen Montage (BULGAKOWA, S. 78f):</p>
        
<p>-Eine metrische Montage, die sich auf eine "primitive" Rhythmusempfindung gr&uuml;ndet. Sonst sind die Abschnitte unabh&auml;ngig, zwischen ihnen gibt es keine Verbindung, nur ihre L&auml;nge ist ausschlaggebend, die Spannung wird erzeugt durch gleichm&auml;&szlig;ige mechanische K&uuml;rzung der L&auml;ngen: doppelt, dreifach usw.).
entspr&auml;che der Kin&auml;sthesie</p>
        
<p>-Eine rhythmische Montage, der ein differenziertes Rhythmusempfinden zugrunde liegt. Die Relation zwischen der L&auml;nge des Abschnitts und dem Charakter der darin abgebildeten Bewegungen dient als Grundlage der Verbindung. Intensivierung kann durch Rhythmuswechsel der Bewegungen erreicht werden: vom (gleichm&auml;&szlig;igen) Soldatenschritt zum (Chaotischen) Kinderwagenholpern auf der Odessaer Treppe (Anm.: in PANZERKREUZER POTEMKIN)</p>
        
<p>-Eine tonale Montage. Hier ist das physiologische Empfinden verschiedener physischer Parameter des Bildes und ihr <i>"Vibrieren"</i> (wie Eisenstein schreibt) von Einstellung zu Einstellung ausschlaggebend. Beleuchtung, Optik, Konturen, Oberfl&auml;chenbeschaffenheit der Objekte werden zu physiologischen Individualisierungsmerkmalen einer Einstellung, zu Dominanten, die den <i>"affektiven emotionellen Klang"</i> bestimmen. Intensivierung wird erreicht durch die Bestimmung einer Dominante und ihrer Kondensieren aus einer Einstellung in die andere. Als Beispiel f&uuml;hrt Eisenstein die Lichtschwankungen (von hellgrau zu bleischwarz) in der Episode "Ernte" aus DAS ALTE UND DAS NEUE an oder von dunkelgrau zum nebligwei&szlig; in den D&auml;mmerungen von POTEMKIN;</p>
        
<p>-obertonale Montage (alle Erreger z&auml;hlen);
entspr&auml;chen verschiedenen Ebenen der Perzeption</p>
        
<p>-intellektuelle Montage (&Uuml;berf&uuml;hrung der physiologisch wirkenden Erreger auf die Ebene der Vorstellung und des Begriffs).
korrespondiert mit der kognitiven Ebene, der Ebene der konzeptualen Erkenntnis</p>
        
<p>Um nocheinmal auf den Ausgangspunkt, den Vergleich zwischen Griffith und Eisenstein, zur&uuml;ckzukommen: Eisenstein vertritt im Vergleich zur dramatischen Montage z. B. eines D. W. Griffith eine v&ouml;llig kontr&auml;re Auffassung von Narration, Raum- und Zeitdarstellung. </p>
        
<p>Raum und Zeit werden nicht nach ihrem Realit&auml;tsgehalt, sondern nach ihrem Bedeutungsgehalt dargestellt. Sie bleiben abstrakt und dienen nicht der handlungsgebundenen, sondern der begrifflichen Orientierung. R&auml;ume werden dekonstruiert, Zeitabl&auml;ufe gerafft oder um ein vielfaches gedehnt (z. B. durch mehrfaches Hintereinanderschneiden der gleichen oder aus verschiedenen Blickwinkeln gedrehten Einstellung). Anschl&uuml;sse im Sinne einer kausalen Kontinuit&auml;t werden negiert.</p>
        
<p>Seine Handlungstr&auml;ger sind nicht Subjekte, mit deren Sehperspektive sich die Zuschauer identifizieren k&ouml;nnten. Oft haben Eisensteins Filme einen starken Massencharakter, wie z. B. in STREIK oder OKTOBER, in denen Individuen praktisch ausgeklammert sind, treten jedoch Einzelpersonen in Aktion, sieht er sie nicht als Individuen oder Charaktere, er verwendet sie als Typen, um ihres Ausdrucks oder des Bildes wegen.    </p>
        
<p>Vor allem transportiert Eisenstein die Inhalte nicht &uuml;ber die Geschichte oder Handlung - Reiz und Empfinden bzw. Erkenntnis (man k&ouml;nnte sie auch mit Katharsis bezeichnen) entstehen aus der Methode heraus, und ergeben sich aus der Art der Verkettung von Einzelelementen. Dabei ist der Zuschauer enorm gefordert - seine Strukturierungsarbeit l&auml;&szlig;t den Film im Prinzip erst entstehen. Oder wie es Oksana Bulgakowa formuliert: <i>"Der Proze&szlig; der Erz&auml;hlung und der Bedeutungsbildung im Montagefilm ist eine dynamische, von der Rezeption nicht zu trennende Struktuierungsarbeit des Zuschauers."</i> <small>(BULGAKOWA, S. 76)</small>
</p>
      
</subsection>
      
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Sergei Eisenstein: "Dickens, Griffith und wir"</h3>
<p>In seinem Artikel DICKENS, GRIFFITH UND WIR weist Eisenstein unter anderem auf die literarische Tradition der Montage hin - und geht zur&uuml;ck bis zu Shakespeare. Griffith hatte bereits selbst formuliert, da&szlig; vor allem Dickens ihn gepr&auml;gt h&auml;tte. Bei seiner Recherche st&ouml;&szlig;t Eisenstein auf ein geradezu "originelles &lt;traktat&gt;" zu Beginn des 17. Kapitels von OLIVER TWIST <i>"&uuml;ber die Prinzipien eben jenes Motageaufbaus eines Sujets [...], der hier bei Dickens so bestrickend gehandhabt wird und von Dickens aus in Griffith's Methode Eingang fand."</i> <small>(EISENSTEIN, S. 94)</small>:</p>
<p>
          
<i>"Auf den Brettern ist es bei allen guten mord- und totschlagreichen St&uuml;cken &uuml;blich, die tragischen und komischen Szenen so regelm&auml;&szlig;ig aufeinander folgen zu lassen, wie das Rote und Wei&szlig;e in einer wohldurchwachsenen Speckseite. Der Held sinkt unter dem Gewicht seiner Ketten und seines Ungl&uuml;cks auf dem &auml;rmlichen Strohlager nieder, und in dem n&auml;chsten Auftritt erfreut sein treuer Gef&auml;hrte, der von alledem nichts wei&szlig;, die Zuschauer mit einem komischen Gesang. Mit klopfendem Herzen sehen wie die Heldin in den Krallen eines stolzen und grausamen Barons; ihre Tugend, ihr Leben - beides ist in Gefahr, und bereits hat sie den Dolch gezogen um die Tugend auf Kosten des Lebens zu retten; aber in demselben Augenblick, wo unsere Erwartungen auf das h&ouml;chste gespannt sind, geht es an ein Musizieren, und wir sind geradewegs in die weiten Hallen eines Schlosses versetzt, wo ein grauk&ouml;pfiger Kastellan einen lustigen Chor mit einer noch lustigeren Gesellschaft von Untergebenen auff&uuml;hrt; und diese leiern, ohne sich im mindesten an den Ort zu kehren, mag er nun eine Kirche oder ein Palast sein, ohne Unterla&szlig; ihre Jodelweisen herunter.
Solche Wechsel m&ouml;gen abgeschmackt erscheinen, aber sie sind gar nicht so unnat&uuml;rlich, wie man auf den ersten Blick glauben sollte. Die &Uuml;berg&auml;nge von einer wohlbesetzten Tafel	zum Sterbebette, vom Trauergewande zum Festtagsschmuck, die man im wirklichen Leben bemerken kann, sind um keinen Jora weniger auffallend, und der Unterschied besteht nur darin, da&szlig; wir im letzteren Falle Schauspieler, im ersteren Zuschauer sind. Der Mime auf dem Schauplatz seines Wirkens achtet nicht auf die gewaltsamen &Uuml;berg&auml;nge und auf das j&auml;he Erschlie&szlig;en der Gef&uuml;hle und Leidenschaften, w&auml;hrend die schaulustige Menge gar bald &uuml;ber Verletzung der inneren Wahrheit zu klagen geneigt ist.
Pl&ouml;tzlicher Szenenwechsel und rasche Ver&auml;nderung in Ort und Zeit haben bei Gebilden der Phantasie nicht nur ein langj&auml;hriges Herkommen f&uuml;r sich, sondern werden von vielen sogar als die gr&ouml;&szlig;te Kunst des Autors betrachtet, die von derartigen Kritikern haupts&auml;chlich nach den Verlegenheiten gew&uuml;rdigt wird, in denen der Held das Gedichtes am Ende eines jeden Kapitels bleibt - und so k&ouml;nnte vielleicht diese kurze Einleitung als unn&ouml;tig erscheinen."</i> <small>(EISENSTEIN, S. 94f)</small>
        
</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
      
<p>&nbsp;</p>
<hr>
<h3>Literatur / Quellenangaben</h3>
<ul>
<li>
<small>Bulgakowa, Oksana: Sergej Eisenstein - drei Utopien, Architekturentw&uuml;rfe zur Filmtheorie; PotemkinPress; Berlin; 1996</small>
</li>
<li>
<small>Eisenstein, Sergei: Gesammelte Aufs&auml;tze I; Im Verlag der Ache; Z&uuml;rich</small>
</li>
<li>
<small>Kaufmann, Lilli (Hrsg.); Sergei Eisenstein: &Uuml;ber mich und meine Filme; Henschelverlag Berlin; 1975</small>
</li>
<li>
<small>Reisz, Karel und Millar, Gavin: Geschichte und Technik der Filmmontage; Filmlandpresse; M&uuml;nchen; 1988</small>
</li>
<li>
<small>See&szlig;len, Georg: Die anderen M&ouml;glichkeiten des Kinos - Zu Sergej Eisensteins 100. Geburtstag; in: epd Film 12/97, 14. Jahrgang; 1997; Frankfurt</small>
</li>
<li>
<small>Weise, Eckhard: Sergei M. Eisenstein - in Selbstzeugnissen und Bilddokumenten; Rohwolt Taschenbuch Verlag; Hamburg; 1975</small>
</li>
</ul>
    
<a name="sec-wong"></a>
<h3>Montageaspekte in den Filmen Wong Kar-Weis</h3>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/wong/karwai.jpg" align="left" height="168" width="180">
Wenn man die letzten drei Filme Wong Kar-Wais grob auf ihre Haupthandlungsstr&auml;nge reduziert, gibt es in CHUNGKING EXPRESS zwei, in FALLEN ANGELS mindestens drei, und in HAPPY TOGETHER eine Liebesgeschichte. So sind es nicht mal so sehr die Geschichten, die seine Filme einzigartig machen, sondern die Art und Weise wie er diese erz&auml;hlt.</p>
      
<p>Zusammen mit seinem Kameramann Christopher Doyle und seinen Cuttern William Chang und Wong Ming-Lam schafft Wong Kar-Wai einen sehr pers&ouml;nlichen, un&uuml;blichen Erz&auml;hlstil, der stark darauf bedacht ist, die inneren Seelenzust&auml;nde seiner Figuren wiederzuspiegeln. Meistens sind diese ungl&uuml;cklich verliebt, Momente der Melancholie und Sehnsucht beherschen ihr entfremdetes, einsames Gro&szlig;stadtleben, ob nun in Hong Kong oder Buenos Aires, wobei sie jedoch versuchen irgendwie ihre Individualit&auml;t zu bewahren und sich selbst nicht zu verlieren. Das Aufzeigen bzw. die filmische Umsetzung dieser Stimmungs- und Gef&uuml;hlswelten, pa&szlig;t Wong Kar-Wai so sehr der Handlung an, da&szlig; eine Unterscheidung	zwischen Form und Inhalt sich quasi ausschlie&szlig;t. Im Unterschied zu den meisten Spielfilmen, wo der schnitterz&auml;hltechnische Umgang mit strukturbildenden Pfeilern wie Zeit und Raum haupts&auml;chlich durch die fortschreitende Handlung gepr&auml;gt ist, werden diese Faktoren erst durch die subjektiven Wahrnehmungswirklichkeiten der Figuren bestimmt. Momente k&ouml;nnen zu Ewigkeiten werden, w&auml;hrend andere Situationen, die sich &uuml;ber l&auml;ngere Zeitr&auml;ume abspielen, wie in einem Rausch vorr&uuml;berziehen. Das innere Erleben kehrt sich nach au&szlig;en und umgekehrt.
In der Montage zeigt sich dies z.B im &Uuml;berlappen der Ton-und Bildspur, dem Umgang mit verschiedenen Bildgeschwindigkeiten vom Zeitraffer bis zum eingefrorenem Standbild, oder dem Wechsel von s/w und Farbe. Der Einsatz von einer oftmals sehr agilen, rastlosen Handkamera, extremer Weitwinkelobjektive, oder der bewu&szlig;te Umgang mit Unsch&auml;rfen, versetzen neben anderen Stilmerkmalen den Zuschauer  in einen tranceartigen, zeitlosen Film-Raum.
Da CHUNGKING EXPRESS anfangs auf drei statt auf zwei Episoden konzipiert war, entstand schon w&auml;hrend der Dreharbeiten zu dem Film Material zu FALLEN ANGELS. Es w&auml;re interessant zu erfahren, ob der Entschlu&szlig;, den Film anders zu strukturieren wie urspr&uuml;nglich geplant, erst am Schnitttisch entstand. Dies zeigt jedoch deutlich wie stark die Filme durch die Montage gepr&auml;gt sind.</p>
      
<p>Die Filme sind sich formal wie auch inhaltlich sehr &auml;hnlich, wobei die Grundstimmung von CHUNGKING EXPRESS viel heiterer wirkt als bei FALLEN ANGELS. In beiden F&auml;llen werden die Figuren am Anfang in den Film ohne gro&szlig;e Erkl&auml;rungen hineingeworfen. Durch schnelle Schnittfolgen von Handkamerafahrten und Schwenks, entsteht eine Stimmung der Hektik und Orientierungslosigkeit.
Die Hektik wird am Anfang von CHUNGKING EXPRESS schlagartig unterbrochen, als Polizist 223 bei seiner Verfolgung die Frau ohne Namen anrempelt. Im selben Moment wird nur f&uuml;r ein paar Bilder lang eine Uhr eingeblendet, gleich darauf: eine Halbtotale in der sich beide f&uuml;r einen Augenblick ansehen, das Bild bewegt sich nur noch langsam, einzelbildweise, w&auml;hrend die Stimme von 223 aus dem Off prophezeit: <i>"in 55 Stunden werde ich mich in diese Frau verlieben"</i>. Abblende. In der n&auml;chsten Einstellung sieht man 223 ohne Schnitt knapp eine Minute lang telefonieren.</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/wong/chungk.jpg" align="left" height="142" width="200">Solche pl&ouml;tzlichen Rhytmuswechsel, hier unter anderem durch verschiedene Bildgeschwindigkeiten und unerwarteten Einstellungseinw&uuml;rfen (die Uhr) erreicht, verleihen dem Film eine unstetige, leicht nerv&ouml;se Struktur, wie bei einem avantgardistischen Jazzst&uuml;ck.
Die Schnittfolgen sind oftmals sehr schnell und vieles verfl&uuml;chtigt sich, ohne da&szlig; es die meisten Zuschauer wohl bemerken werden. So gibt es beispielsweise in CHUNGKING EXPRESS eine Szene, wo 223 eine Rolltreppe irgendeiner Bahnstation hochl&auml;uft, worauf dann eine Totale folgt, auf der ein anderer Polizist von einem Br&uuml;ckengel&auml;nder runtersieht. Viel sp&auml;ter wird sich herausstellen, da&szlig; sich die zweite Episode des Films um diesen Polizisten handelt. Seine Freundin, der er nachtrauert, eine Stewardess, wird auf &auml;hnlich beil&auml;ufige Art in einer Szene die an einem Flughafen spielt "eingef&uuml;hrt". Manche Einstellungen erf&uuml;llen so suggestiv den Zweck zuk&uuml;nftige Geschehnisse oder Personen vorwegzunehmen, wodurch eine Gleichwertigkeit der Ereignisse erreicht wird, ob und wann sie sich auch immer abspielen m&ouml;gen. Nicht alle Schnittfolgen m&uuml;ssen jedoch auf Anhieb so rational erfa&szlig;bar sein wie hier, da diese auch meist auf einer emotionalen Ebene funktionieren.</p>
      
<p>Die Montage dient wie schon Anfangs erw&auml;hnt dazu die Gef&uuml;hls- und Stimmungslagen der Figuren auszudr&uuml;cken, was jedesmal anders geschehen kann, gleichzeitig mu&szlig; sie aber darauf achten, alles stimmig zusammenzuf&uuml;gen, egal wie unterschiedlich von Tempo und Struktur die Einzelsequenzen auch sein m&ouml;gen. Ein Gro&szlig;teil dieser geschlossenen Wirkung  wird in den Filmen durch einen stets wohlausgewogenen Rhytmus innerhalb der Gesamtstruktur erreicht.	Nach jeder rastlosen Phase folgt eine ruhige Einstellung oder Szene, die den Zuschauer Zeit zum Verweilen l&auml;sst. Mal geschieht dies durch den Wechsel von wild geschnittenen, bewegten Handkameraeinstellungen zu festen Stativaufnahmen, die zum Teil minutenlang ohne einen Schnitt auskommen, oder es werden beispielsweise verschiedene Einstellungen aus derselben Kameraperspektive oder Schu&szlig;richtung aneinandergereiht die in ihrer Gesamtheit einen "ruhigen" Block ergeben:</p>
      
<p>In einer Szene von FALLEN ANGELS sieht man den Killer, nach einem seiner Auftr&auml;ge, in einen Bus steigen, wo er &uuml;beraschenderweise einen alten Schulkameraden trifft. Die Schie&szlig;erei war gepr&auml;gt durch eine unruhige Montage, wohingegen jetzt die Kamera minutenlang in einer lediglich am Anfang kurz unterbrochenen Halbnahen, auf der beide Personen zu sehen sind, verharrt.
Die Tonspur ist ein weiteres dramaturgisches Element, welches erheblich zur Geschlossenheit beitr&auml;gt. Oftmals verbinden Musik und die als Off-Stimme eingesetzten lakonischen Selbstkommentare der Figuren mehrere Einstellungsfolgen, die zeitlich wie r&auml;umlich v&ouml;llig auseinanderliegen k&ouml;nnen.
In CHUNGKING EXPRESS gibt es eine Szene, wo Faye in die Wohnung vom Polizisten 663, in dem sie sich verliebt hat, einbricht. Ein Song von den Cranberries (auf chinesisch!) ert&ouml;nt. In rascher Abfolge und mehreren Handkameraeinstellungen sieht man nun, w&auml;hrend die ganze Zeit der Song l&auml;uft, wie sie die Sachen in seiner Wohnung ver&auml;ndert</p>
      
<p>Sie gie&szlig;t unter anderem neue Goldfische in sein Aquarium, wechselt die Ettiketten seiner Thunfischdosen usw. Am Ende tut sie Schlaftabletten in eine Wasserflasche, Schnitt, Faye steht wieder hinter ihrer Imbi&szlig;theke und schaut auf die Uhr. Schnitt, man sieht Polizist 663 zuerst von der Flasche trinken, darauf schl&auml;ft er ein. Schnitt, Faye steht wieder in der Wohnung und nimmt weitere Ver&auml;nderungen vor, ihre Kleidung wechselt dabei fast unmerklich von einer Einstellung zur anderen, in der n&auml;chsten tr&auml;gt sie wieder die gleiche Kleidung wie vorher, dann wieder nicht, womit deutlich wird, da&szlig; sich all dies in gr&ouml;&szlig;eren Zeitabschnitten abspielt. Sie verl&auml;&szlig;t die Wohnung, der Song h&ouml;rt auf.</p>
      
<p> In Szenenbl&ouml;cken wie dieser werden die Einstellungen im Rhythmus der Musik geschnitten. Im Gegensatz zu manchen Hollywoodfilmen, wo solche &auml;hnlichen, videoclipartig montierten Sequenzen eher f&uuml;r die kommerzielle Auswertung des Songs gemacht zu sein scheinen, steht in diesem Fall die dramaturgische Funktion nicht nur zweckm&auml;&szlig;ig im Vordergrund. Fast alle Aktionen die Faye innerhalb dieses Szenenblocks macht, werden sp&auml;ter in einem anderen Erz&auml;hlabschnitt nochmal aufgegriffen, wo sich Polizist 663 nach langer Zeit dar&uuml;ber wundert, da&szlig; sich vieles in seiner Wohnung ver&auml;ndert hat. Vor dem Szenenblock mit dem Cranberries-St&uuml;ck h&ouml;rte Faye permanent nur ein und denselben Song: "California Dreamin". Als Zuschauer h&ouml;rt man ihn dadurch auch st&auml;ndig. In dem Moment wo sie selbst aktiv wird, und in ihrem Leben eine &Auml;nderung herbeif&uuml;hrt, wobei sie die Lebensumst&auml;nde eines Anderen ver&auml;ndert, ert&ouml;nt auch eine andere Musik. </p>
      
<p>Diesen charakteristischen Einsatz von Musik, der sich direkt auf die Personen bezieht, f&uuml;hrt Wong Kar-Wai in FALLEN ANGELS so weit, da&szlig; fast jede Person ein oder zwei f&uuml;r ihn passende Songs  bekommt. Diese Songs begleiten die Figuren in bestimmten Situationen und kommentieren gleichzeitig deren Gef&uuml;hlslage. Jedesmal wenn der Killer in FALLEN ANGELS einen neuen Auftrag ausf&uuml;hrt, h&ouml;rt man ein bestimmtes Trip-Hop St&uuml;ck, das zusammen mit den &auml;hnlich geschnittenen Bilderfolgen als schon mal dagewesenes Motiv auftaucht. Durch diesen Wiedererkennungswert bekommt die Musik zus&auml;tzlich die Funktion von Orientierungspunkten, nach der Bilder und Szenen eingeordnet und geschnitten werden k&ouml;nnen.</p>
      
<p>Seinem unvergleichlichen Erz&auml;hlstil ist Wong Kar-Wai auch in seinem neuesten Film HAPPY TOGETHER treu geblieben. In einer Szene sinniert Lai Yiu-Fai in Buenos Aires, wie es wohl gerade in Hong Kong w&auml;re, und stellt dabei fest, da&szlig; die Stadt genau am anderen Ende der Erdkugel liegt. Schnitt, man sieht verschiedene Zeitrafferaufnahmen von Hong Kong auf dem Kopf. </p>
      
<p>Die Grundstimmung des Films ist in Vergleich zu FALLEN ANGELS noch schwerm&uuml;tiger geworden und der anarchistische Humor, der den vorrangegangenen Filmen ein Moment der Leichtigkeit verlieh, fehlt fast v&ouml;llig. Die Montage passt sich dementsprechend diesem Zustand an. Der Grundrhythmus ist langsamer geworden. Insgesamt gibt es mehr lange, wenig unterbrochene Passagen auch mehr in s/w. Die Rastlosigkeit der Figuren wird nicht mehr so sehr durch schnelle Schnittfolgen unterstrichen und statt charakterisierender Musikst&uuml;cke werden die Figuren vom Tango begleitet.</p>
    
<a name="sec-ruttmann"></a>
<h3>Die Welt im Querschnitt<br>
<small>Walter Ruttmann und sein Montagefilm BERLIN - DIE SINFONIE DER GROSSTADT</small>
</h3>
      
<p>
        
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/collage.jpg" align="left" height="212" width="180">
        <i>"Die Neue Sachlichkeit ist die allgemeine Str&ouml;mung, die gegenw&auml;rtig in Deutschland herrscht, eine Str&ouml;mung des Zynismus und der Resignation auf entt&auml;uschte Hoffnungen. Der Zynismus und die Resignation stellen die negative Seite der Neuen Sachlichkeit dar, ihre positive Seite ist die Begeisterung f&uuml;r die Sachlichkeit, der Wunsch, die Dinge objektiv zu behandeln, so wie sie sind, ohne in ihnen eine ideelle Bedeutung zu suchen."</i>
      
</p>
      
<p>Mit diesen Worten schilderte Gustav Friedrich Hartlaub, Direktor des Mannheimer Kunstmuseums und Sch&ouml;pfer des Begriffs "Neue Sachlichkeit", die Grundvoraussetzungen f&uuml;r die Neuorientierung in den bildenden K&uuml;nsten seit Beginn der zwanziger Jahre und die daraus resultierenden neuen k&uuml;nstlerischen Konzeptionen.
Die K&uuml;nstler haben nach Hartlaubs Meinung genug vom <i>"Hin und Her, von den H&ouml;henfl&uuml;gen und Ausbr&uuml;chen, die doch unweigerlich mit einer Niederlage enden. Es war an der Zeit, die Welt so zu sehen, wie sie ist. Und es gibt viele Dinge in der Welt, die an sich schon sch&ouml;n, wertvoll und n&uuml;tzlich sind. Die Neue Sachlichkeit regt nicht zur Analyse der Zusammenh&auml;nge zwischen den Dingen und den Ereignissen an, sondern sie empfiehlt, die Wirklichkeit in ihrer vorhandenen Form zu billigen. Das ist eine Philosophie der "vollendeten Tatsachen", eine Philosophie des Zynismus und der Kapitulation."</i>
</p>
      
<h4>Deutschland im Zeichen der Neuen Sachlichkeit</h4>
      
<p>In der Malerei der Neuen Sachlichkeit werden Aspekte wie die Anbetung der Technik und die zynische Entlarvung der Lebensweise in einer gro&szlig;kapitalistischen Metropole deutlich, ebenso wie ein Gef&uuml;hl der Resignation: <i>"es hat keinen Zweck, die gesellschaftlichen Widerspr&uuml;che zu beseitigen."</i>
<br>
Trotz dieser gemeinsamen inhaltlichen Tendenzen war die Neue Sachlichkeit keine einheitliche k&uuml;nstlerische Richtung. Bereits Hartlaub unterschied zwei Fl&uuml;gel: einen rechten, die romantische Str&ouml;mung, und einen linken, der eine sozialdemokratische Ausrichtung hatte: <i>"Die Rechten &uuml;berwanden die Entt&auml;uschung durch den Traum, durch eine Flucht aus dem Leben. Die Linken klammerten sich an die Entwicklung der Technik und sehen in den Errungenschaften der Technik eine bessere Zukunft. Aber die einen wie die anderen bem&uuml;hten sich nicht, eine Antwort auf die Frage zu geben: auf welche Weise kann man die Welt verbessern, wenn man seine Aufgabe auf die Registrierung der Fakten und die fotografische Wirklichkeit beschr&auml;nkt?"</i>
</p>
      
<p>Diese n&uuml;chterne, eher beobachtende als bewertende Haltung l&auml;&szlig;t sich nicht nur in der Malerei finden - auch im Bereich der Filmproduktion kam es ab der ersten H&auml;lfte der zwanziger Jahre zu einer Neuorientierung. Diese war zwar nicht so stark und umfassend wie ihr Pendant auf dem Gebiet der Malerei, daf&uuml;r aber ebenso wichtig und innovativ.<br>
Im Gegensatz zur Malerei setzte sich im neusachlichen Film jedoch recht bald eine sozialkritische Haltung durch, die, vor allem unter dem Einflu&szlig; der Weltwirtschaftskrise 1929 - 1933, den Film zu einen gesellschaftsbestimmenden Instrument machte. Aus dem Neusachlichen Film, den man in seiner reinsten Form auch als Absoluten Film bezeichnen k&ouml;nnte, wurde der zun&auml;chst sozialkritische, dann agitatorisch-proletarische Spielfilm.</p>
      
<h4>Die zeitgeschichtlichen Voraussetzungen</h4>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/nosferatu.jpg" align="right" height="181" width="180">Die zeitgeschichtlichen Voraussetzungen f&uuml;r den Neusachlichen Film sind in den Jahren der Stabilisierung von 1924 bis 1929 zu suchen: ab 1924 begann sich das normale Leben wieder einzustellen; die Mark war stabilisiert und Deutschland &uuml;bernahm den Dawes-Plan, der den Modus der Reparationsleistungen festlegte und Deutschlands Eingliederung in das Finanzsystem der Allierten bewirkte. Die Inflation von 1922/23 erschien bald als ferner Alptraum. Die Drohung einer sozialen Revolution begann sich aufzul&ouml;sen, wie zwei Jahre vorher der Vampir in Murnaus NOSFERATU und viele andere phantastische Gestalten des Nachkriegsfilms.</p>
      
<p>
        
<i>"Die Filme der Stabilisierungszeit wandten sich nun verst&auml;rkt der Au&szlig;enwelt zu und verlegten die Betonung von Geistererscheinungen auf Zeiterscheinungen, von imagin&auml;ren auf nat&uuml;rliche Landschaften. Im Wesentlichen waren diese Filme realistisch."</i> <small>(Siegfried Kracauer)</small>
      
</p>
      
<p>Der &Uuml;bergang vom Expressionismus zur Neuen Sachlichkeit darf jedoch nicht als die Abl&ouml;sung der Vertreter einer Kunstrichtung durch die Propheten einer neuen Schule gesehen werden; vielmehr handelte es sich um einen Wandel des Sehens bei den gleichen Leuten.
1923 hatte in Berlin das B&uuml;hnenst&uuml;ck "Nebeneinander" Premiere, dessen Autor Georg Kaiser bis dahin als f&uuml;hrender Dramatiker des Expressionismus galt. Begl&uuml;ckt schrieb der Kritiker Siegfried Jacobson, der den Expressionismus nicht mochte, in der Weltb&uuml;hne:<br>

<i>"Georg Kaiser ist aus der Wolke, die ihn bisher umnebelte, mit beiden F&uuml;&szlig;en auf die Erde gestiegen... Fr&uuml;her bestand seine Welt aus Wesen, die verm&ouml;ge ihrer Hysterie jedem Ereignis wie einer Katastrophe wehrlos ausgeliefert waren... Was davon da war, ist einer Nervigkeit gewichen, die Kaisers endlich errungener kalter und heiterer &Uuml;berlegenheit &uuml;ber den turbulenten Irrsinn unserer Tage durchaus angemessen ist."</i>
<br>
Das war die Neue Sachlichkeit avant la lettre. "Nebeneinander" wurde ein Schl&uuml;sselwort der Neuen Sachlichkeit. Statt nach dem "Absoluten des Isolierten" zu suchen, strebte man nun nach dem "Relativen des Nebeneinander".</p>
      
<p>Ein anderes Schl&uuml;sselwort der Neuen Sachlichkeit wurde "Querschnitt". Der Querschnitt hie&szlig; die ber&uuml;hmteste Kulturzeitschrift der zwanziger Jahre, bis 1926 herausgegeben vom Kunsth&auml;ndler Alfred Flechtheim und ungeheuer einflu&szlig;reich bei der Durchsetzung dieser neuen Richtung. Als "Life-Style-Magazin" der zwanziger Jahre bezeichnet, zog er immer wieder neusachliche Bilder zur Illustrierung seiner Artikel heran: modische Caf&eacute;s und Lokale, kurzlebige Gr&ouml;&szlig;en des Gesellschaftslebens, wie T&auml;nzerinnen und Boxer, aber auch ungew&ouml;hnliche Themen wie die eine zeitlang sehr popul&auml;ren T&auml;towierungen - alles fand Zugang zu den Seiten des Querschnitt, der somit ein visuelles Barometer f&uuml;r die Moden, Lebensstile und wechselnde Objekte der Allt&auml;glichkeit in der Weimarer Republik wurde.</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/skizze.jpg" align="right" height="256" width="160">Auch in der Malerei fand diese Idee Zugang: "Querschnitt" nannte George Grosz, der neben seiner Arbeit f&uuml;r verschiedene kommunistische Publikationen auch Textbeitr&auml;ge f&uuml;r den Querschnitt schrieb, eine Zeichnung von 1919/20, mit der er collagenhaft einen Angriff gegen die &ouml;ffentliche Moral der beginnenden "roaring twenties" ausf&uuml;hrt.
Dargestellt ist das Nebeneinander von B&uuml;rgertum und Proletariat in einer von den deutlich sichtbaren Spuren des gerade &uuml;berstandenen Ersten Weltkrieges gezeichneten Gro&szlig;stadtkulisse: Kriegskr&uuml;ppel, Arbeiter, Polizisten, die Reichswehr, die gerade Aufst&auml;ndische exekutiert, Bauchladenverk&auml;ufer und wohlhabende Herren mit ihren k&auml;uflichen Damen. Gro&szlig;stadtleben, Prostitution, Elend und soziale Spannungen; dies alles wird nebeneinander und in mehrfacher &Uuml;berlagerung gesehen.</p>
      
<p>Die Zeichnung gibt recht eindrucksvoll Groszs&laquo; Charakterisierung der "roaring twenties" wieder: <i>"&Uuml;berall erschollen Ha&szlig;ges&auml;nge. Alle wurden geha&szlig;t: die Juden, die Kapitalisten, die Junker, die Kommunisten, das Milit&auml;r, die Hausbesitzer, die Arbeiter, die Arbeitslosen, die Schwarze Reichswehr, die Kontrollkommissionen, die Politiker, die Warenh&auml;user und nochmals die Juden. Es war eine Orgie der Verhetzung, und die Republik war schwach, kaum wahrnehmbar... Es war eine v&ouml;llig negative Welt, mit buntem Schaum obendrauf, den viele f&uuml;r das wahre, das gl&uuml;ckliche Deutschland vor dem Anbruch der neuen Barbarei hielten."</i>
</p>
      
<h4>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/abenteuer.jpg" align="left" height="150" width="180">Die Querschnittfilme</h4>
      
<p>Ihren fr&uuml;hesten filmischen Ausdruck fand die Neue Sachlichkeit in den sogenannten Querschnittfilmen , die jeweils ein bestimmtes Detail aus dem Alltag zum Thema erhoben. "Im breiten Querschnitt" das typische Leben der Gro&szlig;stadtmenschen zu zeigen war auch die erkl&auml;rte Absicht von Bela Balazs bei der Konzeption des Films K 13513 - DIE ABENTEUER EINES ZEHNMARKSCHEINS (1926, Regie Berthold Viertel), der zum programmatischen Pionierwerk dieses neuen Genres wurde. Die Hauptrolle wurde passenderweise mit einer Sache, einem Geldschein, besetzt. Anhand dieses Zehnmarkscheines, der durch die H&auml;nde der verschiedensten Bewohner Berlins aus den unterschiedlichsten Milieus wandert, wird in mehreren Episoden das Leben w&auml;hrend der Inflation geschildert.<br>

<i>"Die Akrobatik eines ins Bild gewehten Papierstreifens, sein Fahren, Flattern und Streifen durch die Lebensbezirke der Gro&szlig;stadt..."</i> bildet <i>"...die rein optisch empfundene Bildidee"</i>, beschreibt der Regisseur die Grundstruktur seines Erstlingswerkes.</p>
      
<p>Aus dem gezeigten Querschnitt ergibt sich, abgesehen von der Mannigfaltigkeit der Ereignisse, eigentlich nur, da&szlig; das Leben unaufh&ouml;rlich flie&szlig;t, keinen Anfang und kein Ende hat. Der Film spiegelt es wieder wie in einem Kaffeehausfenster, in dem man immer neue Passanten, neue Autos und Autobusse sieht. Balazs gab f&uuml;r diese Erz&auml;hlweise auch die theoretische Begr&uuml;ndung: Das Leben sollte nicht <i>"unter dem Blickwinkel nur einer einzigen Person und innerhalb der Grenzen ihrer Erlebnisf&auml;higkeit"</i> dargestellt werden, sondern <i>"im breiten Querschnitt: nicht eines Menschen zuf&auml;lliges Leben, sondern das typische Leben schlechthin"</i>. Auch in diesen Theorien spukt wieder die Idee von der Abbildung des allt&auml;glichen Lebens, <i>"so wie es ist"</i>.<br>
Da viele Querschnittsfilme auf Archivmaterial zur&uuml;ckgriffen, boten sie die M&ouml;glichkeit, billig produziert zu werden und zugleich viel zu zeigen ohne etwas zu enth&uuml;llen.</p>
      
<p>Ebenfalls zu den Querschnittsfilmen geh&ouml;rt MENSCHEN UNTEREINANDER - ACHT AKTE AUS EINEM INTERESSANTEN HAUSE (1926) von Gerhard Lamprecht, eine filmische Recherche, die an einem "locus classicus" festmacht ist, und somit zum Modell f&uuml;r das spezifisch berlinerische Genre der Pensions-Filme  wird. </p>
      
<p>In einem ehemaligen Herrschaftshaus in Berlin, das jetzt in Appartements aufgeteilt ist, zeigt der Film einige Tage aus dem Leben der unterschiedlichsten Mietparteien: im Erdgescho&szlig; wohnen ein Anwalt und ein Juwelier, im 1. Obergescho&szlig; ein Beamter, dessen Frau wegen Totschlags im Gef&auml;ngnis sitzt und eine appetitliche Witwe, die einem Heiratsschwindler zum Opfer f&auml;llt; im 2. Obergescho&szlig; befinden sich eine Tanzschule und eine Heiratsvermittlung; im Dachgescho&szlig; wohnen ein alter, halbblinder Klavierlehrer und ein Luftballonverk&auml;ufer.</p>
      
<p>Obwohl sich in beiden Filmen das "typische Leben" noch hinter einer stark kolportagehaften Handlung versteckt, verweisen sie bereits auf einen programmatischen Aspekt der Neuen Sachlichkeit: Die Aufwertung eines Gegenstandes oder eines Ortes zum "Handelnden" oder handlungsbestimmenden Element.</p>
      
<h4>BERLIN - DIE SINFONIE DER GROSSTADT</h4>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/berlin1.jpg" align="left" height="150" width="200">Die Querschnittskonzeption lag auch Walter Ruttmanns Reportagefilm BERLIN - DIE SINFONIE DER GROSSTADT (1927) zugrunde. Der Film wirkt wie die Realisierung und zugleich Vollendung des Programms, das der zweite Herausgeber des Querschnitt, Hermann von Wedderkopp, schon 1926 f&uuml;r das Kino, in dem er - neben der Zeitung - <i>"das ideale Kunstprodukt dieser Zeit"</i> sah, aufstellte: <i>"Spielerischer Ausdruck einer idealen Wirklichkeit, die frisch geschehene, unverf&auml;lschte, unverkl&auml;rte Wirklichkeit, mit gro&szlig;em Stadtausschnitt, B&auml;umerauschen, Brandung, Eleganz, Technik, Sport, bekannten und anonymen Gesichtern und dem ganzen wunderbaren Durcheinander... Wirklichkeit ist heute Qualit&auml;tsprobe, Voraussetzung f&uuml;r das Wort "neu". Da&szlig; man sie selten findet, beweist, wie schwierig es ist, sie einzufangen und k&uuml;nstlerisch zu pr&auml;sentieren. Dazu geh&ouml;rt Phantasie, denn im Grunde genommen ist Wirklichkeit stets das Phantastische. Phantasie ist nicht ins Wesenlose schweifende Erfindung, sondern Kombinationsgabe. Sehen des Wesentlichen, Bestimmenden und Aussondern des Toten. Wirklichkeit ist Synonym f&uuml;r Echtheit, Erlebnis, Lebendigkeit."</i>
</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/berlin2.jpg" align="left" height="148" width="400">Berlin wird mit den Augen eines Reisenden gesehen, der im noch schwachen Licht des erwachenden Morgens im Schnellzug - Richtung Berlin - durch die Vorst&auml;dte zum Anhalter Bahnhof f&auml;hrt. Der Tag d&auml;mmert. Je heller es wird, desto mehr Menschen f&uuml;llen die stillen Stra&szlig;en. Die Stadt erwacht: man sieht Polizeistreifen, versp&auml;tete Nachtbummler und morgendliche Plakatkleber. Dann machen sich die Menschen auf den Weg zur Arbeit: mit Ringbahn, Autobus, Untergrund-, Stra&szlig;enbahn und Auto fahren Arbeiter und kleine Angestellte in ihre Fabriken und B&uuml;ros. In den Fabriken h&auml;mmert der Rhythmus der Maschinen. Die Gesch&auml;fte &ouml;ffnen, Kinder gehen zur Schule, Hausfrauen gehen ihrer Arbeit nach. Je weiter der Tag fortschreitet, desto lebendiger wird die Stadt. Um 12 Uhr ist Mittagspause: die Arbeiter essen, im Restaurant wird gegessen, die Zootiere werden gef&uuml;ttert. Nach der Mittagsruhe geht die Arbeit weiter. Zeitungen berichten von Krisen, Mord und B&ouml;rsengesch&auml;ften. Auf den Stra&szlig;en herrscht allgemeine Hektik. Die Beine der Passanten eilen vorbei. Der Verkehr wird immer dichter. Bettler und Steichholzverk&auml;ufer stehen vor einem Juweliergesch&auml;ft. Dann bricht der Abend &uuml;ber Berlin herein. Werkst&auml;tten und B&uuml;ros schlie&szlig;en; die Maschinen werden abgestellt. Die R&uuml;ckwanderung der Massen beginnt. Leuchtreklamen und die Lichter der Vergn&uuml;gungsst&auml;tten flammen auf. Leute stehen vor Schaufenstern und vor einem Kino. Die Menschen vergn&uuml;gen sich im Variet&eacute;, im Kino (man sieht Chaplins Beine) und im Spielklub. In einem Tanzlokal spielt eine Jazzband. Immer noch herrscht Hektik: die Beine von Revuegirls, Theaterschauspielern und Passanten dominieren den Bildausschnitt. Regen setzt ein und auf den n&auml;chtlichen Stra&szlig;en beginnen Gleisarbeiten. Das Nachtleben verlagert sich in Kneipen und Hinterzimmer, wo Kartenspieler ihr Gl&uuml;ck versuchen. Noch sp&auml;ter zieht sich das Leben in der Stadt  zur&uuml;ck und die n&auml;chtlichen Stra&szlig;en leeren sich.</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/reklame.jpg" align="right" height="258" width="180">Uraufgef&uuml;hrt wurde BERLIN - DIE SINFONIE DER GROSSTADT am 23. September 1927 im Berliner Tauentzien-Palast, begleitet von einem gro&szlig;en Live-Orchester unter Leitung des Komponisten Edmund Meisel, der auch die vielbeachtete Musik f&uuml;r Eisensteins PANZERKREUZER POTEMKIN geschrieben hatte. Die Orchesterpartitur ist nicht mehr vollst&auml;ndig erhalten, ein Klavierauszug belegt jedoch, da&szlig; es sich um eine "funktionale" Musik gehandelt haben mu&szlig;, d. h. um eine unmittelbar auf die Bildrhythmik bezogene Komposition als Teil des "Gesamtkunstwerkes".</p>
      
<p>Das Premierenpublikum erwartete laut Vorank&uuml;n-digung ein Film, der <i>"mit allem bricht, was der Film bisher gezeigt hatte. Es spielen keine Schau-spieler, und doch handeln Hunderttausende. Es gibt keine Spielhand-lung, und es erschlie&szlig;en sich doch ungez&auml;hlte Dramen des Lebens. Es gibt keine Kulissen und keine Ausstattung, und man schaut doch in der hundertpferdigen Flucht die unz&auml;hligen Gesichter der Millionenstadt. Pal&auml;ste, H&auml;userschluchten, rasende Eisenbahnen, donnernde Maschinen, das Flammenmeer der Gro&szlig;stadtn&auml;chte - Schulkinder, Arbeitermassen, brausender Verkehr, Naturseligkeiten, Gro&szlig;stadtsumpf, das Luxushotel und die Branntweindestille ... Der m&auml;chtige Rhytmus der Arbeit, der rauschende Hymnus des Vergn&uuml;gens, der Verzweiflungsschrei des Elends und das Donnern der steinernen Stra&szlig;en - alles wurde vereinigt zur Sinfonie der Gro&szlig;stadt."</i>
</p>
      
<p>Was Regisseur Walter Ruttmann seinem Publikum anbietet, war als Idee nicht mehr ganz neu: Alberto Cavalcantis "Stadtfilm" &uuml;ber Paris RIEN QUE LES HEURES (NICHTS ALS STUNDEN, F 1926) d&uuml;rfte Ruttmann zumindest bekannt gewesen sein, ebenso der Film MOSKAU (1927) von Mikhail Kaufmann und Ilya Kopalin. In Anbetracht der &uuml;berm&auml;chtigen Konkurrenz durch zeitgen&ouml;ssische und historische Melodramen, Kom&ouml;dien und R&uuml;hrst&uuml;cke war sein Konzept jedoch umso k&uuml;hner: ein abendf&uuml;llender Dokumentarfilm tritt in Konkurrenz zum sonstigen Spielfilmangebot und wird wider Erwarten ein aufsehenerregender Premierenerfolg, was umso erstaunlicher ist, als er auf alle kassenwirksamen Elemente des Spielfilms verzichtet und trotz seiner L&auml;nge von &uuml;ber 60 Minuten ohne die im Stummfilm &uuml;blichen Zwischentitel auskommt - ein Kunstgriff, der Ruttmanns dokumentarischem Konzept entgegenkommt.</p>
      
<h4>Malerei mit Licht - Walter Ruttmann und die Avantgarde</h4>
      
<p>Wie bereits eingangs erw&auml;hnt, lassen sich die Wurzeln der Neuen Sachlichkeit oft im Expressionismus finden. So waren auch die Gestalter von BERLIN - DIE SINFONIE DER GROSSTADT zuvor prominente Exponeten des deutschen Filmexpressionismus: Carl Mayer, der CALIGARI-Autor, hat die Idee geliefert, Karl Freund, der Kameramann von Murnau und Lang, hat an der Konzeption mitgewirkt und den Film produziert; Regisseur Walter Ruttmann, der hier im Wesentlich ein Regisseur der Montage ist, war zun&auml;chst Maler und bildete dann zusammen mit Viking Eggeling und Hans Richter die Avantgarde des deutschen Experimentalfilms.</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/entwurf.jpg" align="left" height="225" width="200">Ruttmann hatte in Z&uuml;rich Architektur studiert und sich bereits vor dem Ersten Weltkrieg als Maler und Grafiker etabliert. Seine Portraitzeichnungen verkauften sich gut und bei Plakatwettbewerben gewann er mehrere erste Preise. Nach dem Krieg wandte er sich von einem von Corinth beeinflu&szlig;ten Impressionismus ab und begann abstrakt zu malen.</p>
      
<p>Seine gro&szlig;e Leidenschaft war jedoch schon immer die "neue Kunstform" gewesen, das Lichtspiel, von Ruttmann auch als "Malerei mit Zeit" bezeichnet.
<i>"Ruttmann war stets ein eifriger Besucher des Kinos gewesen, und alle Bewegungsvorg&auml;nge im Kino hatten ihn in der Evolution der Linie stark gefesselt. Er erkannte auch die ungeheuere Bedeutung des Kinos f&uuml;r das allgemeine Publikum; lehnte den Film als Roman ab und &uuml;berlegt immer wieder, wie ein Film als Kunstwerk gestaltet sein m&uuml;&szlig;te. So kam er, der den Film optisch auffa&szlig;te, dazu, ein "bewegtes" Bild schaffen zu wollen."</i> <small>(Max Butting, Musiker und enger Freund Ruttmanns)</small>
</p>
      
<p>In einem 1917 entstandenen Aufsatz benennt Ruttmann die Ausdrucksmittel der Kinematographie, deren Gesetze f&uuml;r ihn am n&auml;chsten mit denen der Malerei und des Tanzes verwandt sind: <i>"Formen, Fl&auml;chen, Helligkeiten und Dunkelheiten mit all den ihnen innewohnenden Stimmungsgehalt, vor allem aber die Bewegung dieser optischen Ph&auml;momene, die zeitliche Entwicklung einer Form aus einer anderen. 
[...]
 Zum Beispiel: Die bewu&szlig;te Vermittlung eines bestimmten Ausdrucks durch die Folge eines dunklen Filmabschnitts durch einen hellen, ein Crescendo der Handlung durch einen sich steigernden Rhythmus in der Aufeinanderfolge einzelner Filmabschnitte, durch eine fortschreitende Verk&uuml;rzung, Verl&auml;ngerung, Verdunklung oder Aufhellung dieser Teile, durch die pl&ouml;tzliche Einf&uuml;hrung eines Vorgangs mit ganz andersartigem optischen Charakter, durch die Entwicklung unruhiger, wilder Bewegtheit aus einer ruhenden Masse, durch bildm&auml;&szlig;ig komponierte Einheit zwischen den Menschen und den sie umgebenden Naturformen - und durch tausend andere und damit zusammenh&auml;ngende M&ouml;glichkeiten des Wechsels von Hell und Dunkel, von Ruhe und Bewegung ..."</i>
</p>
      
<p>1919 gr&uuml;ndete er seine eigene Filmgesellschaft, die Ruttmann-Film GmbH mit Sitz in M&uuml;nchen. 1920 lie&szlig; er sich einen selbst konstruierten Tricktisch patentieren. Auf diesem entstand in monatelanger Arbeit sein erster Film, der aus 10.000 einzeln eingef&auml;rbten Bildern bestehenden Trickfilm LICHTSPIEL OPUS 1, dessen optisches Konzept am treffendsten Alfred Kerr im "Berliner Tageblatt" charakterisiert hat:<br>

<i>"Man denkt an Expressionistenbilder. Die sind aber unbewegsam. Chagalls Leuchtparadiese bleiben doch starr. Die Funkelfuturismen der neuesten Pariser versteinern reglos - im Rahmen. Hier aber flitzen Dinge, rudern, brennen, steigen, sto&szlig;en, quellen, gleiten, schreiten, welken, flie&szlig;en, schwellen, d&auml;mmen; entfalten sich, w&ouml;lben sich, breiten sich, verringern sich, kugeln sich, engen sich, sch&auml;rfen sich, teilen sich, kr&uuml;mmen sich, heben sich, f&uuml;llen sich, leeren sich, bl&auml;hen sich, ducken sich; bl&uuml;meln und verkr&uuml;meln sich. Kurz: Expressionismus in Bewegtheit. Ein Rausch f&uuml;r die Pupille ... Aber kein Menschenfilm."</i>
</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/schere.jpg" align="left" height="140" width="200">1921 konnte Walter Ruttmann seinen Erstling der &Ouml;ffentlichkeit vorf&uuml;hren, bereits ein Jahr sp&auml;ter folgte OPUS 2, der im Gegensatz zu seinem Vorg&auml;nger in der <i>"rhythmischen Gliederung der Erscheinungsgruppen &uuml;bersichtlicher"</i> und <i>"klar periodisiert"</i> wirkt. (Bernhard Diebold, Kritiker der "Frankfurter Zeitung" und ein Verfechter des gemalten Films als "neuer Augenkunst".)</p>
      
<p>Praktisch &uuml;ber Nacht war Walter Ruttmann zu einem Pionier der deutschen Film-Avantgarde geworden. Als Erster war er in der Lage, abstrakte farbige Filme in &uuml;berzeugender Qualit&auml;t herzustellen, was ihm die sofortige Aufmerksamkeit der Werbebranche sicherte: ab 1922 betrat er mit seinen kolorierten und viragierten abstrakten Werbefilmen im Stil der beiden OPUS-Filme ebenfalls Neuland.</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/falken.jpg" align="left" height="135" width="200">Um die Jahreswende 1922/23 zog er von M&uuml;nchen nach Berlin, also in das Zentrum der deutschen Filmindustrie, wo er zun&auml;chst an Lotte Reinigers abendf&uuml;llenden Scherenschnittfilm DIE ABENTEUER DES PRINZEN ACHMED arbeitete. (Von Ruttmann stammen abstrakte Muster, Wolkenhintergr&uuml;nde, Blitze , Feuerschwaden und Geistererscheinungen, die er zu den Zauberszenen beisteuerte.) Auf die Vermittlung von Lotte Reiniger engagierte ihn Fritz Lang, um die "Falkentraum"-Tricksequenz f&uuml;r den ersten Teil der NIBELUNGEN zu gestalten.</p>
      
<p>W&auml;hrend dieser Zeit arbeitete Ruttmann eng mit der Bauhaus-Sch&uuml;lerin Lore Leudesdorff zusammen. Unter ihrem Einflu&szlig; vollzog sich bei ihm eine Abwendung von mehr naturhaften Formungen hin zu sachlich-geometrischen Motiven, wie sie in OPUS 3 und OPUS 4 (1924/25) zu finden sind; eine Entwicklung, die schlie&szlig;lich in der Komposition seines Berlin-Films ihren H&ouml;hepunkt fand.</p>
      
<p>Interessanterweise l&auml;&szlig;t sich in BERLIN eine kurze Sequenz finden, die eine tats&auml;chliche Schnittstelle zwischen dem absoluten und dem neusachlichen Film markiert: wenn gleich zu Beginn des Films der Zug in Richtung Berlin f&auml;hrt, heben sich entlang der Gleise Schlagb&auml;ume, deren Diagonalen gleich darauf in einer sehr kurzen Tricksequenz mit abstrakten, die Geschwindigkeit des Zuges verdeutlichenden horizontalen und diagonalen Linien aufgenommen werden.</p>
      
<h4>Bewegung als optisches Prinzip</h4>
      
<p>Die vier wichtigsten Faktoren bei der Herstellung seines Berlin-Films fa&szlig;t Walter Ruttmenn folgenderma&szlig;en zusammen:<br>

<i>"1. Konsequente Durchf&uuml;hrung der musikalisch-rhythmischen Forderungen des Films, denn Film ist rhythmische Organisation der Zeit durch optische Mittel.<br>
2. Konsequente Abwehr von gefilmtem Theater.<br>
3. Keine gestellten Szenen. Menschliche Vorg&auml;nge und Menschen wurden "beschlichen". Durch dieses "Sich-unbeobachtet-glauben" entstand Unmittelbarkeit des Ausdrucks.<br>
4. Jeder Vorgang spricht durch sich selbst - also: keine Titel."</i>
</p>
      
<p>Die geforderte "Unmittelbarkeit des Ausdrucks" verpflichtete die Macher des Films dazu, sich ausschlie&szlig;lich auf authentisches, nicht inszeniertes Material zu st&uuml;tzen. Kameramann Karl Freund &uuml;ber diese neue Art der Gestaltung: <i>"Ich wollte alles aufnehmen. Menschen, die aufstehen, um zur Arbeit zu gehen, die fr&uuml;hst&uuml;cken, Stra&szlig;enbahn fahren oder laufen. Meine Charaktere stammen aus allen Lebensbereichen. Vom niedrigsten Arbeiter bis zum Bankpr&auml;sidenten."</i> <small>("Karl Freund, Candid Cinematographer"; in Popular Photography, Februar 1939)</small>
<br>
Freund wu&szlig;te, da&szlig; er f&uuml;r solche Zwecke auf die Methoden des Dokumentarfilms zur&uuml;ckgreifen mu&szlig;te. Als begabter Techniker machte er das damals erh&auml;ltliche Filmmaterial hochempfindlich, um so auch unter schlechten Lichtbedingungen filmen zu k&ouml;nnen. Au&szlig;erdem dachte er sich Tricks aus, um seine Kamera w&auml;hrend des Drehens zu verstecken: er fuhr z. B. in einem halboffenen Lastwagen mit Schlitzen f&uuml;r das Objektiv in den Seitenplanen oder versteckte seine Kamera in einem Kasten, der wie ein ganz harmloser Koffer aussah. Auf die Frage, ob er die dokumentarische Fotografie als Kunst betrachte, antwortete Freund au&szlig;er sich vor Begeisterung: <i>"Es ist die einzige Art der Photographie, die wirklich Kunst ist. Warum? Weil man mit ihr das Leben portr&auml;tieren kann. Diese gro&szlig;en Photographien, auf denen die Leute geziert l&auml;cheln und grimassieren und sich ins Bild setzen... Bah! Das ist keine Photographie. Ein leicht handhabbares Objektiv, Aufnahmen vom Leben, Realismus dagegen: das ist Photographie in ihrer reinsten Form."</i>
</p>
      
<p>Um allerdings dem Leben der Gro&szlig;stadt in Rhythmus und Tempo gerecht zu werden, war die Montage des Bilder mindestens so wichtig wie die Authentizit&auml;t des gefilmten Materials. Zu den dominierenden optischen Motiven geh&ouml;ren der Verkehr und die Beine der Menschen, die im neusachlichen Film die Allt&auml;glichkeit des Geschehens verdeutlichen - sei es auf der Stra&szlig;e, in der Revue oder auf einer Demonstration. St&auml;ndige Bewegung ist der beherrschende Eindruck und die &Uuml;bersetzung in einen ad&auml;quaten Filmrhythmus das angestrebte Ziel.<br>
Ruttmann beschreibt die hierbei auftretenden Probleme: <i>"Beim Schneiden zeigte sich, wie schwer die Sichtbarmachung der sinfonischen Kurve war, die mir vor Augen stand. Viele der sch&ouml;nsten Aufnahmen mu&szlig;ten fallen, weil hier kein Bilderbuch entstehen durfte, sondern so etwas wie das Gef&uuml;ge einer komplizierten Maschine, die nur in Schwung geraten kann, wenn jedes kleinste Teilchen mit genauester Pr&auml;zision in das andere greift. (...) Nach jedem Schnittversuch sah ich, was mir noch fehlte, dort ein Bild f&uuml;r ein zartes Crescendo, hier ein Andante, ein blecherner Klang oder ein Fl&ouml;tenton und danach bestimmte ich immer von neuem, was aufzunehmen und was f&uuml;r Motive zu suchen waren."</i>
</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/tanzen.jpg" align="left" height="248" width="180">Was Ruttmann als "sinfonische Kurve" bezeichnet, sollte sich im Wesentlichen aus der Montage der Bilder ergeben. So kommt es, da&szlig; BERLIN bei einer Gesamtdauer von 61 Minuten aus der beachtlichen Anzahl von 1009 Einstellungen zusammengesetzt ist. Die durchschnittliche Einstellungsl&auml;nge liegt also bei 3,6 Sekunden - und somit erheblich unter der entsprechender Spielfilme der damaligen Zeit - was jedoch nicht bedeuten soll, da&szlig; der Film dieses Tempo &uuml;ber seine ganze L&auml;nge durchh&auml;lt. Die Dauer der einzelnen Einstellungen schwankt zwischen 45,5 Sek. als Maximum und 0,2 Sek. als Minimalwert, wobei die durchschnittliche Einstellungsdauer, bezogen auf die f&uuml;nf inhaltlichen Abschnitte des Films (Nacht, Morgen, Vormittag, Mittag, Abend), relativ einheitlich ist. Aus dieser inhaltlichen Gliederung folgt eine Strukturierung des Tagesablaufs in Aktivit&auml;tsphasen, die vom Schnittempo entsprechend begleitet werden: Erwachen der Stadt und Arbeitsbeginn, Mittagspause und Nachmittagstempo, Feierabend und Gro&szlig;stadtnacht.</p>
      
<p>Interessant sind also die extremen Schwankungen innerhalb der thematischen Abschnitte, die sowohl schnelle Schnittfolgen als auch l&auml;ngere Einstellungen enthalten, was sich gleich an der Exposition (Morgengrauen, der Zug f&auml;hrt Richtung Berlin) gut zeigen l&auml;&szlig;t: die anf&auml;nglichen Einstellungen werden zun&auml;chst mit &Uuml;berblendungen verbunden, was aufgrund des zeitintensiven "weichen" &Uuml;bergangs Blende kein hohes Schnittempo zul&auml;&szlig;t. Doch schon in der zweiten Minute erh&ouml;ht sich dieses um ein vielfaches, auf &uuml;ber 50 Schnitte in einer einzigen Minute, was innerhalb des gesamten Films einen absoluten H&ouml;hepunkt darstellt. Wenn sich der Zug in der dritten und vierten Minute Berlin n&auml;hert, verlangsamt sich seine Fahrt und somit auch der Wechsel der Einstellungen, bis hin zum Stillstand des Zuges. Der Wechsel der Einstellungen n&auml;hert sich ebenfalls dem absoluten Tiefpunkt vom zwei Schnitten pro Minute, was die Atmosph&auml;re der noch schlafenden Stadt verdeutlicht. Erst ab der elften Minute nimmt das Tempo wieder zu (die Stadt erwacht) und steigert sich bis zur 14. Minute (die Arbeit in den Fabriken beginnt). Dann f&auml;llt das Tempo wieder etwas ab (Gesch&auml;fte &ouml;ffnen, Berufsverkehr) und steigert sich bis zur 23. und 24. Minute (Hektik im B&uuml;robetrieb) zu einem neuen H&ouml;hepunkt; usw ...</p>
      
<p>Ein wesentlicher Gesichtspunkt der Montage war die <i>"straffe Organisation des Zeitlichen nach streng musikalischen Prinzipien"</i> <small>(Ruttmann)</small>. So wird auch in der Literatur immer wieder der Versuch unternommen, anhand der beschriebenen Tempowechsel - sowohl innerhalb einzelner Abschnitte, als auch auf den gesamten Film bezogen - ein musikalisches Prinzip abzulesen, etwa die Satzfolge der Sinfonie: schnell (Allegro), langsam (Andante), m&auml;&szlig;iges Tempo (Menuett), schnell (Allegro). <i>"Das kurze einleitende "Andante" der stillen Morgenstunden, in denen Berlin erwacht; das alsbald einsetzende st&uuml;rmische "Allegro con fuoco" des Verkehrs (...); das gewaltig aufklingende "Adagio" eines Intermezzos (...); das frohe "Allegretto" der Sport gewidmeten Freistunden und endlich das gl&auml;nzende, rauschende "Finale" des abendlichen Berlins."</i> <small>(Jeanpaul Goergen in (7))</small>
</p>
      
<p>Allerdings darf man aus der Anlehnung an eine musikalische Ordnung kein stringentes Strukturprinzip ableiten; vielmehr bildet die Orientierung an der sinfonischen Satzfolge in Film allenfalls den Ausgangspunkt f&uuml;r eine rhythmische Untergliederung im Sinne "optischer Musik".
Eine thematische Orientierung kommt der Intention des wirklichkeitsgetreuen Stadtportraits wesentlich n&auml;her: <i>"die Rhythmik der Montage ... ist im wesentlichen eine analoge Umsetzung der Rhythmik des st&auml;dtischen Tagesablaufs: nach den Strapazen der Anreise die Ruhe am fr&uuml;hen Morgen, die Hektik bei Arbeitsbeginn, der Mittagstrubel, die zwischenzeitliche Erholung am fr&uuml;hen Nachmittag, die wieder aufbrechende Betriebsamkeit des Feierabendverkehrs, die Abendvergn&uuml;gen."</i> <small>(Helmut Korte in (8))</small>
</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/schreibzug.jpg" align="left" height="143" width="419">Im Sinne der rhythmischen Unterst&uuml;tzung der Bilder werden beispielsweise auch die bewegungsintensiven Motive (flie&szlig;ender Verkehr, laufende Menschenmassen, Beine, rasende Maschinen ...) durch ein besonders hohes Schnittempo hervorgehoben. Der hierbei erreichte Geschwindigkeitseindruck wird durch die kontrastierend eingesetzten unterschiedlichen Bewegungsrichtungen sogar noch verst&auml;rkt.</p>
      
<h4>Im "Querschnitt" der Kritiken</h4>
      
<p>Die zeitgen&ouml;ssische Kritik nahm den Film im Gro&szlig;en und Ganzen zustimmend bis enthusiastisch auf: <i>"...was der Film an Bildern bringt, ist so unerh&ouml;rt, so neuartig, so packend gesehen, da&szlig; kleine, kritische Einw&auml;nde ohne Bedeutung f&uuml;r den Gesamteindruck bleiben. Nie vorher sah man das Problem der Maschine auf &auml;hnlich phantastische Art gemeistert und symbolisch gesteigert. Ruttmann verschmilzt Mensch und Stadt. Er besch&auml;ftigt keine Schauspieler, er kennt nicht einmal Gesichter. Er gibt von dem Menschen die Geb&auml;rde, die Figur, das Tempo. Er ist sachlich und n&uuml;chtern. Aber gerade deshalb bezaubert der Film. Eine Meisterleistung."</i>, konnte man am 24.09.1927 im Kinematograph  lesen.<br>
Weiter: <i>"Ein einzigartiger, ein erregender, ein &uuml;berw&auml;ltigender Film! Eine ganz gro&szlig;e Leistung! M&ouml;glich freilich nur durch die Erfindung der Hypersensibilisierung des Negativs durch Kuntze-Safra, deren Verdienste der geschw&auml;tzige Vorspann nicht gen&uuml;gend unterstreicht. Um die Schaffung dieser 1466 Filmmeter bem&uuml;hen sich Carl Mayer, Karl Freund, Reimar Kuntze, Robert Baberske, Laszlo Sch&auml;ffer und Walter Ruttmann, welcher letztlich als Leiter des Ganzen anzusehen ist."</i>
</p>
      
<p>Der russische Regisseur Wsewolod Pudovkin, selbst ein gro&szlig;er Meister der Bildmontage und ein Bewunderer Ruttmanns, schrieb &uuml;ber BERLIN: <i>"In seinem Film "stellte" Ruttmann niemals etwas nur "vor", immer "wirkt" er &uuml;ber die Montage des Materials auf den Zuschauer "ein" ... Ruttmann gibt dem Zuschauer so lange keine Ruhe, bis er ihn vollends beherrscht, bis er ihn hypnotisiert, dabei ist es keineswegs sein Ziel, die Gegenst&auml;nde, die er dreht, dem Zuschauer vorzustellen, sondern er will &uuml;ber den Montagerhythmus auf ihn einwirken."</i>
</p>
      
<p>Lediglich die marxistischen Filmkritiker waren bei aller Anerkennung der formalen Qualit&auml;ten nicht einverstanden mit dem Film, weil sie nicht akzeptieren konnten, da&szlig; Ruttmann nicht auf die soziale Analyse aus war, sondern auf eine visuelle Symphonie. Der Filmhistoriker Jerzy Toeplitz hat dies Jahre sp&auml;ter in seiner mehrb&auml;ndigen "Geschichte des Films" richtiggestellt:<br>

<i>"Die Verbindung der Aufnahmen wurde nicht von dem Inhalt der Szenen, sondern ausschlie&szlig;lich von der formalen &Auml;hnlichkeit oder vom Kontrast bestimmt. Die lebendige Stadt wurde nach Ruttmanns Konzeption zu einem seltsamen, kompilzierten, ewig funktionierenden Mechanismus. Ruttmann erkl&auml;rt jedoch nicht die Ursachen und den Zweck dieses Funktionierens. Die Konzeption der Neuen Sachlichkeit fand in BERLIN ihre beste filmische Verwirklichung."</i>
</p>
      
<h4>Epilog: MELODIE DER WELT, DEUTSCHE WAFFENSCHMIEDE</h4>
      
<p>Mit der Zwangsl&auml;ufigkeit, die allem Innovativen den Status der Einmaligkeit nimmt, fand auch Ruttmanns filmisches Konzept schnell Nachahmer. &Uuml;ber die Vorbildfunktion von BERLIN schreibt der amerikanische Filmhistoriker John Grierson: <i>"Kein Film hat mehr Einflu&szlig; gehabt, kein Film ist mehr imitiert worden. Er initiierte die modernere Art, das Dokumentarfilmmaterial vor der eigenen T&uuml;r zu finden: Begebenheiten, die sich weder durch den Reiz der Neuheit, noch durch die Romantik edler Wilder in exotischer Kulisse empfehlen. Der Film stellt f&uuml;r den Dokumentarfilm die R&uuml;ckkehr vom Romantischen zum Realistischen dar."</i> <small>(in Films and Filming, August 1961)</small>
</p>
      
<p>Der gro&szlig;e Erfolg seines Berlin-Films brachte Walter Ruttmann auch im Ausland Anerkennung ein. Um die Jahrreswende 1928/29 reiste er nach Moskau, um einen Roman von Jack London zu verfilmen. Das Projekt kam nicht zustande, doch noch im selben Jahr konnte er - zur&uuml;ck in Deutschland - seinen n&auml;chsten Montagefilm drehen: MELODIE DER WELT, ein Werbefilm f&uuml;r Weltreisen mit den Dampfern der Hamburg-Amerika-Linie und zugleich sein wichtigster Tonfilm. Wieder dominieren die symphonische Weltsicht und der Querschnittsgedanke: Stra&szlig;enszenen, Kriegsschaupl&auml;tze, religi&ouml;se Zeremonien, Essensrituale, usw. werden nach analogen bzw. kontrastierenden Gesichtspunkten in Gruppen geordnet und verdichten sich auf diese Weise zu dem Bild einer einzigen Welt.<br>
Die M&ouml;glichkeiten des Tonfilms nutzend, erweitert Ruttmann sein Montageprinzip um Aufnahmen von Stra&szlig;en- und Arbeitsger&auml;uschen, die sich gegenseitig &uuml;berlagern. Schiffssirenen ert&ouml;nen als akustische H&ouml;hepunkte des Films. Sogar die Stille wird gekonnt als neues dramaturgisches Mittel eingesetzt: <i>"Auf Bilder von Soldaten, Panzern und Explosionen folgt ein Frauengesicht, das einen Schrei des Entsetzens ausst&ouml;&szlig;t - dann liegen die Gr&auml;ber in vollkommener Stille."</i> <small>(Jeanpaul Goergen in (7))</small>
</p>
      
<p>Seine Tonexperimente konnte Ruttmann in dem f&uuml;r die Reichsrundfunkgesellschaft produzierten H&ouml;rspiel "Weekend" sogar noch fortsetzen: Alltagsger&auml;usche, Sprachfetzen, Lieder, Kinderverse, usw. charakterisieren in knapp 12 Minuten die Stationen eines typischen Wochenendes (Feierabend, Fahrt ins Freie, Wiederbeginn der Arbeit, ...). Auch hier bleibt Ruttmann der Wirklichkeit verpflichtet: die "Dialoge" wurden von Laien gesprochen und die Ger&auml;usche mit einem fahrbaren Aufnahmenwagen direkt vor Ort aufgenommen, in Fabriken, auf Bahnh&ouml;fen und Hochbahntreppen.</p>
      
<p>Bis in seine sp&auml;teren (Auftrags)Arbeiten sp&uuml;rt Walter Ruttmann, der <i>"dem Dritten Reich wohl seine Mitarbeit, nicht jedoch sein Talent zur Verf&uuml;gung gestellt hat"</i>, den Dingen und der ihnen eigenen Dynamik nach. Hier offenbart sich allerdings das gro&szlig;e Manko in seinem Ansatz, ausschlie&szlig;lich in visuellen Kategorien zu denken: seine Propagandafilme wie METALL DES HIMMELS sind zwar abermals eine <i>"technisch perfekte"</i> ... <i>"Flucht in die Form"</i>, unternehmen jedoch nicht den <i>"Versuch der Sublimation ins Kritische."</i> <small>(Hilmar Hoffmann, "Und die Fahne f&uuml;hrt uns in die Ewigkeit")</small> Im 1940 produzierten DEUTSCHE WAFFENSCHMIEDEN interessiert ihn wieder die Bewegung an sich, diesmal jedoch die Bewegungen von Maschinen, die Granaten und Munition herstellen. Dieses Zugest&auml;ndnis an ein formales Prinzip ist umso erstaunlicher, wenn man bedenkt, da&szlig; Ruttmann, den sein Dienst als Gasschutz-Offizier im Ersten Weltkrieg seelisch schwere belastet hatte, als entschiedener Kriegsgegner aus dem Feld zur&uuml;ckgekehrt war.</p>
      
<p>Die Filme von Walter Ruttmann waren, sofern sie sich ausschlie&szlig;lich f&uuml;r den Rhythmus und den Klang einer Sequenz interessierten, also offen f&uuml;r die unterschiedlichsten Inhalte, sogar f&uuml;r Propaganda.</p>
      
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Filmographie:</h3>
<p>1921 DIE T...NENDE WELLE<br>
1921	OPUS 1<br>
1922	OPUS 2,	 verschiedene abstrakte Werbefilme<br>
1923	Mitarbeit an Lotte Reinigers DIE ABENTEUER DES PRINZEN ACHMED<br>
1924	OPUS 3; "Falkentraum-Sequenz" f&uuml;r Fritz Langs DIE NIBELUNGEN, TEIL 1<br>
1925	OPUS 4<br>
1927	BERLIN - DUE SINFONIE DER GROSSTADT<br>
1929	MELODIE DER WELT<br>
1930	WEEKEND<br>
1931	FEIND IM BLUT<br>
1932	ACCIAIO (ARBEIT MACHT GL&Uuml;CKLICH) <br>
1934	ALTGERMANISCHE BAUERNKULTUR<br>
1935	METALL DES HIMMELS<br>
	KLEINER FILM &Uuml;BER EINE GROSSE STADT,	 D&Uuml;SSELDORF<br>
	STUTTGART, GROSSTADT ZWISCHEN WALD UND	REBEN <br>
1936	SCHIFF IN NOT<br>
1937	MANNESMANN<br>
1938	HAMBURG, WELTSTRASSE, WELTHAFEN<br>
	IM ZEICHEN DES VERTRAUENS<br>
	IM DIENSTE DER MENSCHHEIT<br>
	HENKEL, EIN DEUTSCHES WERK IN SEINER  ARBEIT<br>
1940	DIE DEUTSCHE WAFFENSCHMIEDE<br>
	DEUTSCHE PANZER; ABERGLAUBE<br>
1941	KREBS</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
      
<p>&nbsp;</p>
<hr>
<h3>Literatur / Quellenangaben</h3>
<ul>
<li>
<small>(1) Arnheim, Rudolf: Kritiken und Aufs&auml;tze zum Film; Frankfurt am Main, 1979</small>
</li>
<li>
<small>(2) Brennicke, Ilona; Hembus, Joe: Klassiker des deutschen  Stummfilms 1910-1930; M&uuml;nchen, 1983</small>
</li>
<li>
<small>(3) Buderer, Hans-J&uuml;rgen; Fath, Manfred: Neue Sachlichkeit - Bilder auf der Suche nach der Wirklichkeit; M&uuml;nchen,  1994</small>
</li>
<li>
<small>(4) Eisner, Lotte H.: Die d&auml;monische Leinwand; Frankfurt am  Main, 1980</small>
</li>
<li>
<small>(5) Faulstisch, Werner; Korte, Helmut (Hrsg.): Fischer Filmgeschichte Band 2: 1925-1944; Frankfurt am Main, 1991</small>
</li>
<li>
<small>(6) Goergen, Jeanpaul: Musik des Lichts - Zum 100. Geburtstag Walter Ruttmanns; in epd Film 12/87</small>
</li>
<li>
<small>(7) Goergen, Jeanpaul: Walter Ruttmann - Eine Dokumentation; Freunde der Deutschen Kinemathek (Hrsg.); Berlin, 1990</small>
</li>
<li>
<small>(8) Korte, Helmut (Hrsg.): Film und Realit&auml;t in der Weimarer Republik; Frankfurt am Main, 1980</small>
</li>
<li>
<small>(9) Kracauer, Siegfried: Von Caligari zu Hitler - Eine psychologische Geschichte des deutschen Films; Frankfurt am Main, 2. Auflage 1993</small>
</li>
<li>
<small>(10) Kranzfelder, Ivo: George Grosz; K&ouml;ln, 1993</small>
</li>
<li>
<small>(11) Metken, G&uuml;nter (Hrsg.): Realismus zwischen Revolution und Reaktion 1919-1939; M&uuml;nchen, 1981</small>
</li>
<li>
<small>(12) Michalski, Sergiusz: Neue Sachlichkeit; K&ouml;ln, 1994</small>
</li>
<li>
<small>(13) Toeplitz, Jerzy: Geschichte des Films, Band 1, 1895-1927, Berlin 1985</small>
</li>
<li>
<small>(14) Toeplitz, Jerzy: Geschichte des Films, Band 2, 1928-1933, Berlin 1985</small>
</li>
<li>
<small>(15) Werner, Paul: Die Skandalchronik des deutschen Films von 1900 bis 1945; Frankfurt am Main, 1990</small>
</li>
</ul>
    
<a name="sec-peckinpah"></a>
<h3>Gewaltige Montage-Monumente: Sam Peckinpah</h3>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/peckinpah/pkportrait.jpg" align="left" height="155" width="200">Seinen legend&auml;ren Ruf als notorischer Querulant und Anh&auml;nger brutaler Gewalt hat Sam Peckinpah nicht unwesentlich dem verschwenderischen und innovativen Gebrauch verschiedenster Montagetechniken zu verdanken.<br>
N&auml;heren Zugang zur Kunst der Montage bekam Peckinpah, der auf einer Ranch in Kalifornien aufwuchs, nach Theaterinszenierungen in seiner Studienzeit sowie anf&auml;nglicher Arbeit als Fernsehregisseur und Drehbuchautor (u. a. GUNSMOKE, dt. Titel Rauchende Colts). Mitte der f&uuml;nfziger Jahre als Assistent des Regisseurs Don Siegel, der zu jener Zeit Leiter der Montage-Abteilung bei Warner-Bros. war.</p>
      
<p>Gewaltdarstellungen spielen in vielen Filmen Peckinpahs eine zentrale Rolle, oft sind sie die Eckpfeiler der Geschichte. Dabei setzt er bewu&szlig;t augekl&uuml;gelte Montagetechniken ein, die er in vorher nie dagewesener Weise kombiniert. Zu seinen auff&auml;lligsten Stilmerkmalen geh&ouml;rt die Manipulation der Bildgeschwindigkeit, vor allem der virtuose Einsatz der Zeitlupe. Durch die Kontrastwirkung von normalemTempo und Zeitlupe werden Einzelheiten, die sonst im unkontrollierten Bilderrausch einer Actionszene untergehen w&uuml;rden, erst f&uuml;r den Zuschauer wahrnehmbar. Peckinpah erreicht eine Intensivierung der Gewalteindr&uuml;cke durch Verlangsamung, Reduktion und prismatische Perspektivwechsel.<br>
Die meist obligatorische Eskalation der Gewalt in seinen Filmen und die Inszenierung  von Hyper-Gewalt-Orgien, wie sie extrem im  ber&uuml;hmt-ber&uuml;chtigten Schlu&szlig;massaker von THE WILD BUNCH-Sie kannten kein Gesetz zum Ausdruck kam (Cutter Robert L. Wolfe dazu: '...wir beschlossen zu versuchen, daraus eine Art Ballett [..] aus fallenden K&ouml;rpern und abgefeuerten Waffen [...] zu machen.'), brachte Peckinpah vielfach den Vorwuf der Gewaltverherrlichung ein. Der Kritiker Joseph Morgenstern lastet ihm zum Beispiel eine Stilisierung von Gewaltt&auml;tigkeit durch Zeitlupe an. Leslie Fiedler, ein renommierter amerikanischer Kulturkritiker spricht dagegen neutraler von <i>"Reflexion der Gewaltt&auml;tigkeit [...] durch die filmischen Techniken der verlangsamten, beschleunigten und festgefrorenen Bilder"</i>.</p>
      
<p>In Wahrheit geht es Peckinpah auch keineswegs um eine Zelebrierung oder &Auml;sthetisierung von Gewalt an und f&uuml;r sich oder um  Schockierung oder Ekelerregung beim Zuschauer (wie im Splatter-Genre) , sondern prim&auml;r einfach um die realistische Darstellung. Hierbei darf man auch den Erz&auml;hlzusammenhang der betroffenen Filme nicht au&szlig;er Acht lassen (haupts&auml;chlich Western, die zu einer Zeit spielen, als die Zeit des goldenen Westens selbst schon lange Legende war). Diese sind &uuml;berwiegend in Zeiten des Umbruchs angesiedelt, wenn die alte Gesellschaftsform am zerbr&ouml;ckeln ist und eine neue erst in undeutlichen Konturen sichtbar, wenn sich vertraute Werte aufl&ouml;sen, wenn Tradition, Ordnung und Vernunft zusammenbrechen. St&auml;ndiger Verlust an Bezugspunkten f&uuml;hrt bei Peckinpahs Protagonisten, die sich gew&ouml;hnlich auf der Flucht, auf Verfolgungsjagden, Rachefeldz&uuml;gen oder Strafexpeditionen befinden, zu Perspektivverlust, und Prinzipienlosigkeit, zu Vereinsamung und zu Zerst&ouml;rungswut aufgrund ungebundener Antriebe. Mit hektischen Schnittfolgen versucht Peckinpah das Chaos einer Welt f&uuml;hlbar zu machen, welches die Figuren in seinen Filmen aus ihrem inneren Gleichgewicht wirft.</p>
      
<p>
<br clear="all">
Schnitt war f&uuml;r Peckinpah  unverzichtbares Instrument der Gestaltung und Strukturierung eines Films. Nach Abschlu&szlig; der Dreharbeiten begann f&uuml;r Peckinpah erst der Hauptteil seines kreativen Schaffens. Die Montage-Sessions im Schneideraum nahmen Monate in Anspruch. Das abgefilmte Material wurde zerst&uuml;ckelt, seziert, sortiert und entsprechend der Filmdramaturgie neu angeordnet , wobei die drei- bis vierst&uuml;ndigen Rohfassungen auf kinogerechte Zeitformate von etwa zwei Stunden zusammenschnurrten. Cutter Lou Lombardo, der zwei Filme Peckinpahs schnitt, beschrieb dessen Methode bei der Herstellung k&uuml;rzerer Fassungen so: 'Er nimmt nur Teile der Sequenzen heraus, so da&szlig; ihre Essenz intakt bleibt'.</p>
      
<p>
<br clear="all">
Neben der f&uuml;r ihn typischen Zeitlupe arbeitete Peckinpah mit Parallelmontage und Gegenschnitt-Folgen (vielfach zur Polarisierung von gegens&auml;tzlichen Charakteren), mit Zeitraffer, mit Wiederholungen und mit der Montage von Standfotos. Charakteristisch sind auch Sequenzen, die aus extrem vielen sehr kurzen Einstellungen zusammengesetzt sind. Auf die Spitze getrieben wird dies im Todesballett des Schlu&szlig;massakers von THE WILD BUNCH mit 352 Einstellungen in nur sechs Minuten und sieben Sekunden. Der ganze Film hatte schlie&szlig;lich die ungew&ouml;hnlich hohe Zahl von mehr als 3600 Einstellungen, 'mehr als jeder andere Farbfilm, der je gedreht wurde' (Lombardo).</p>
      
<p>
<br clear="all">
Kaum jemand kennt jedoch Peckinpahs Filme in ihrer urspr&uuml;nglich vorgesehenen Fassung. Sie wurden verst&uuml;mmelt und entstellt, gek&uuml;rzt und aufgrund halbherziger Verleihstrategien der ...ffentlichkeit kaum zug&auml;nglich gemacht (meist wohl mehr aus kommerziellen als aus ethisch-moralischen Gr&uuml;nden). So geh&ouml;rt es zur ureigenen Tragik seines Schicksals, da&szlig; er, der wie kaum ein anderer die sch&ouml;pferischen M&ouml;glichkeiten von Schnitt nutzte, wie kein anderer unter den restriktiven Folgen von Schnitt zu leiden hatte.</p>
      
<p>So lag Peckinpah in st&auml;ndigem Clinch mit Studios und Produzenten, was sich in der turbulenten Entstehungsgeschichte vieler seiner Filme wiederspiegelt. Angefangen beim mehrfachen Umschreiben der Drehb&uuml;cher &uuml;ber Hangreiflichkeiten am Set und gefeuerte Mitarbeiter zog sich die Kette der Konflikte bis zu den folgenschweren Gefechten im Schneideraum, denen ganze Sequenzen zum Opfer fielen. Die Tatsache, zum Beispiel, da&szlig; bei Peckinpahs PAT GARRETT JAGT BILLY THE KID sechs(!) Cutter im Titelvorspann genannt werden, sagt da alles.
Peckinpah verlor im Kampf gegen das konformistische, an kurzfristigem Profit orientierte System der Filmproduktion Hollywoods die entscheidenden Schlachten um die Endmontage.
In seiner chaotischen Lebensweise, die durch wenig Schlaf einhergehend mit exzessivem Tabletten-, Alkohol- und Zigarettenkonsum gekennzeichnet war, &auml;hnelte er zuletzt mehr und mehr den Outlaws seiner Filme und hauste die letzten Jahre nur noch in Wohnmobilen. Sam Peckinpah starb am 28. Dezember 1984 in Inglewood, Kalifornien.</p>
      
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Filmographie</h3>
<p>
	1961  THE DEADLY COMPANIONS (Gef&auml;hrten des Todes) mit Maureen O'Hara, Brian Keith<br>
	1961/62	 RIDE THE HIGH COUNTRY (Sacramento) mit Joel McCrea, Randolph Scott	<br>
	1964/65	 MAJOR DUNDEE (Sierra Charriba)	 mit Charlton Heston, Richard Harris, James Coburn 1964/65<br>
	1968/69  THE WILD BUNCH (Sie kannten kein Gesetz)	mit William Holden, Ernest Borgnine, Robert Ryan,Warren Oates, Ben Johnson<br>
	1969/70	 THE BALLAD OF CABLE HOGUE (Abgerechnet wird zum Schluss) mit Jason Robards Jr, Stella Stevens <br>
	1971  STRAW DOGS  (Wer Gewalt s&auml;t) mit Dustin Hoffman, Susan George <br>
	1971/72	 JUNIOR BONNER mit Steve McQueen, Robert Preston<br>
	1972	THE GETAWAY mit Steve McQueen, Ali MacGraw<br>
	1972/73	 PAT GARRETT AND BILLY THE KID (Pat Garrett jagt Billy the Kid) mit James Coburn, Kris Kristofferson, Bob Dylan, Jason Robards Jr<br>
	1973/74	 BRING ME THE HEAD OF ALFREDO GARCIA (Bring mir den Kopf von Alfredo Garcia) mit Warren Oates, Isela Vega, Kris Kristofferson<br>
	1975  THE KILLER-ELITE (Die Killer-Elite)  mit James Caan, Robert Duvall<br>
	1976/77	 CROSS OF IRON (Steiner-Das Eiserne Kreuz) mit James Coburn, Maximilian Schell, James Mason  <br>
	1977/78	 CONVOY mit Kris Kristofferson, Ali MacGraw, Ernest Borgnine<br>
	1982/83	 THE OSTERMAN WEEKEND mit Rutger Hauer, John Hurt, Dennis Hopper, Burt Lancaster</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
      
<p>&nbsp;</p>
<hr>
<h3>Literatur / Quellenangaben</h3>
<ul>
<li>
<small>Frank Arnold, Ulrich von Berg: Sam Peckinpah-Ein Outlaw in Hollywood, Ullstein 1987</small>
</li>
<li>
<small><a href="http://www.geocities.com/Hollywood/Academy/1912">http://www.geocities.com/Hollywood/Academy/1912</a></small>
</li>
<li>
<small><a href="http://www.dreamagic.com/roger/peck.html">http://www.dreamagic.com/roger/peck.html</a></small>
</li>
<li>
<small><a href="http://www.film100.com/cgi/direct.cgi?v.peck">http://www.film100.com/cgi/direct.cgi?v.peck</a></small>
</li>
</ul>
    
<br>
<hr size="1" noshade="noshade">
<h3 >Inhalt:</h3>
<ul >
<li>
<small><?php echo CHtml::link('Einführung', array('site/page', 'view'=>'docs.montage.intro'));?></small>
</li>
<li>
<small><?php echo CHtml::link('MTV-&Auml;sthetik&lt;: 128 Cuts per Minute - und kein Ende in Sicht', array('site/page', 'view'=>'docs.montage.mtv'));?></small>
</li>
<li>
<small><?php echo CHtml::link('IA28 - Sergej Eisensteins Weg zur Intellektuellen Montage', array('site/page', 'view'=>'docs.montage.eisenst'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Montageaspekte in den Filmen Wong Kar-Weis', array('site/page', 'view'=>'docs.montage.wong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Die Welt im Querschnitt: Walter Ruttmann und sein Montagefilm BERLIN - DIE SINFONIE DER GROSSTADT', array('site/page', 'view'=>'docs.montage.ruttmann'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Gewaltige Montage-Monumente: Sam Peckinpah', array('site/page', 'view'=>'docs.montage.peckinpah'));?></small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p >
<font size="-2">Impressum</font>
</p>
<p>
<font  size="-2">"Zwischen den Bildern - Aspekte der Filmmontage" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und erscheint begleitend zur Filmreihe im Wintersemester 97/98.
Vervielf&auml;ltigung und Kopieren nur mit vorheriger Genehmigung des AFK</font>
</p>
<p>
<font  size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstr.12<br>
76128 Karlsruhe</font>
</p>
<p>
<font  size="-2">Mitarbeiter an dieser Ausgabe:</font>
</p>
<p>
<font  size="-2">A. G&uuml;nter<br>
M. Hirasaka<br>
M. Nagenborg<br>
H. Stiens<br>
P. Stock</font>
</p>
<p>
<font  size="-2">HTML-Aufbereitung:  Michael Haist</font>
</p>
