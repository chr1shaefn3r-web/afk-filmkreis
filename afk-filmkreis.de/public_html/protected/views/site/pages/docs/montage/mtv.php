<p align="right">
<small>
<?php echo CHtml::link('&gt;&gt;&gt; Begleitheft \'Die Filme von Mike Leigh\'', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Zwischen den Bildern</b></big>
<br>Aspekte der Filmmontage<br>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.montage.intro','docs.montage.eisenst')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-mtv"></a>
<h3>&gt;MTV-&Auml;sthetik&lt;<br>
<small>128 Cuts per Minute - und kein Ende in Sicht</small>
</h3>
      
<p>
        
<i>"Get MTV off the Air!"</i> <small>(Dead Kennedys)</small>
      
</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/mtv/mtvlogo.gif" align="left" height="142" width="180">Einen Film nachzusagen, er habe eine MTV-&Auml;sthetik, ist meist ein Vorwurf, selten eine Beschreibung. Im folgenden Text soll nachgegangen werden, was das Wort eigentlich bezeichnet und warum es im Mund des Cineasten so einen seltsamen Beigeschmack hinterl&auml;&szlig;t.</p>
      
<p>Statt &gt;MTV-&Auml;sthetik&lt; sagt der Eine oder die Andere auch &gt;Videoclip-&Auml;sthetik&lt;. Die Geschichte des Videoclips ist aber die Geschichte von MTV, und darum Ehre, wem Ehre geb&uuml;hrt schreibe ich hier &uuml;ber die MTV-&Auml;sthetik.</p>
      
<p>Da es sich in dieser Aufsatzsammelung alles um das Thema &gt;Schnitt&lt; dreht, bem&uuml;ht sich der Verfasser sich auf diesen speziellen Aspekt zu beschr&auml;nken, d. h. sich auf die Schnell-Schnitt-Forderungen zu konzentrieren. Abschweifungen sind zu entschuldigen, unvermeidlich und f&uuml;hren garantiert zum Thema zur&uuml;ck. Ich entschuldige mich bei allen Maskenbildern, Farbfolienherstellern und digitalen Bildbenacharbeitern und allen, deren Handwerk nicht gen&uuml;gend gew&uuml;rdigt wird. Ich habe Euch trotzdem lieb.</p>
      
<h4>1982: Birth of a nation</h4>
      
<p>1981 erscheinen die ersten Anzeigen in fachinternen Buisness-Bl&auml;ttern. &gt;&gt;The Biggest Advertising Merger in History&lt;&lt; versprechen sie. (Fu&szlig;note: Zur Geschichte des MTV vgl. Jane &amp; Michael Stern: Encylopedia of Pop Culture. New York: Harper 1992.) MTV hat sein Versprechen gehalten; und damit sind wir bei Grund No. 1, warum das Wort &gt;&gt;MTV&lt;&lt; immer noch einen schlechten Geschmack hinterl&auml;&szlig;t. MTV ist eine Erfindung, welche die Verk&auml;ufe der Musikindustrie steigern sollte und gleichzeitig die der Televisionbranche. Oder umgekehrt. Egal: Es geht ums Verkaufen. Aber soweit (d. h.bei der Kritik) sind wir ja noch garnicht. Also, MTV war der erste Sender, der auf die Idee kam, die Visualisierung von Musik an zwei Gruppen zu verkaufen: 1.) an die K&auml;ufer die von ihrer Band mehr erwarten als nur Musik (n&auml;mlich ein Image, also Bild oder (frei &uuml;bersetzt) ein zur Schau gestellter Lebensstil) und 2.) an die Musikindustrie, die via Image einer Band, den potentiellen K&auml;ufern ein weitere Orientierungshilfe geben wollte/sollte (Image hei&szlig;t... s. oben.).</p>
      
<p>Der gro&szlig;e, kommerziell ertragreiche Witz an Musiksendern wie MTV ist, da&szlig; der Sender &gt;&gt;nur&lt;&lt; die Infrastruktur und sein eigenes Image zur Verf&uuml;gung stellt; die Kosten f&uuml;r die Erstellung der Musik-Promo-Clips ich meine nat&uuml;rlich &gt;&gt;Videoclips&lt;&lt; tr&auml;gt der Verk&auml;ufer der Musik.</p>
      
<p>Damit hatte MTV eine ganz andere Ausgangbasis als andere TV-Sender, denn die hatten nur einen Produzenten f&uuml;r 90 Minuten, MTV hatte und hat 30 Produzenten (= 90 Minuten geteilt durch drei). Das Mindeste, was ein jeder Produzent anstrebt, ist, seine Kosten zu decken. Das Maximale, was jeder Produzent anstrebt, ist, mehr als seine Kosten einzuspielen. Die Produzenten der MTV-Clips bekommen ihr Geld nat&uuml;rlich nicht von MTV, sondern von der Musikindustrie, f&uuml;r die sie arbeiten. Was liegt da n&auml;her, als die Kosten des einzelen Videoclips zu erh&ouml;hen, um die Medienwirkung des Clips garantiert zu erh&ouml;hen? Mehr Medienwirkung = mehr Geld f&uuml;r die Produzenten.</p>
      
<p>Nun kommt wieder die 30:1-Quote ins Spiel. Ein 90-min&uuml;tiger Spielfilm kostete 1980 sch&auml;tzungsweise 15 Millionen US-Dollar. Geteilt durch 30 also 50.000 US-Dollar. Erh&ouml;ht nun jeder Video-Clip-Produzent nun seine Kosten um 10%, also auf 55.000 US-Dollar, um sein Video vom Kino-Durchschnitt abzuheben, so bedeutet dies f&uuml;r MTV-&auml;hnliche Sender, da&szlig; sie mit einer Produktion von 16,5 Millionen US-Dollar gegen eine 15 Millionen-US-Dollar-Produktion antreten. Und einige Musikvideoproduzenten erh&ouml;hten um mehr als 10%, und nicht alle Filme, die im Fernsehn ausgestrahlt werden, kosten 15 Millionen. Damit aber nicht genug, MTV bediente sich auch noch eines neuen Mediums: Video.</p>
      
<h4>Exkurs: Video vs. Kino</h4>
      
<p>Bis in die 80er arbeiteten die meisten TV-Sender auf 16mm. Der Umstieg auf Video erfolgte langsam, weil die Anschaffung einer MAZ-Anlage (MAZ = Magnetaufzeichnung) hohe Kosten verursachte. Der Vorteil der neuen Technologie war jedoch die Senkung der Materialkosten. Mit Video kann man f&uuml;r das gleiche Geld eine Szene &ouml;fter drehen, oder auch die Kamera einfach w&auml;hrend eines Ereignisses mitlaufen lassen. Letzteres erzeugt beim Zuschauer ein &gt;live&lt;-feeling, das wir alle inzwischen von unseren privaten Urlaubsvideos kennen und sch&auml;tzen. Und dies ist entscheidend f&uuml;r Musikkonsumenten, die von ihrer Band Authenzit&auml;t erwarten. Und dies ist, was Kinofilme wie CHUNGKING EXPRESS heute reproduzieren. Denn es darf nicht vergessen werden, da&szlig; verschiedene Unterhaltungsmedien stets in wirtschaftlicher Konkurrenz zueinander stehen. Alles, was der Zuschauer aus dem Fernsehn kennt, erwartet er schlie&szlig;lich auch im Kino zu sehen (und wer ins Kino geht, ist f&uuml;r 90 Minuten zumindest kein TV-Konsument). Allerdings vergessen die meisten Konsumenten, da&szlig; nur weil man Kino-Filme auch im Fernsehn sehen kann, die beiden Medien dennoch verschiedenen Produktionsbedingungen unterworfen sind.</p>
      
<p>Im TV gab es in den 80er einiges zu sehen, was man im Kino nicht sehen konnte. Die &gt;&gt;Duran Duran&lt;&lt;-clips unter der Regie von Russel Mulcalhy z. B. boten eine Vielzahl von Effekten, die wir aus dem Kino noch nicht kannten. Hierbei kam der Flimmerkiste ihre kleine Projektionsfl&auml;che zur Hilfe: Effekte f&uuml;r die kleine R&ouml;hre m&uuml;ssen nun einmal nicht so &gt;&gt;gut&lt;&lt;, will sagen: aufwendig sein, wie Raumschiffmodelle in 70mm-Produktionen.</p>
      
<p>Gerade bei Spezial Effekten kommen dem Video auch noch weitere medienspezifische Eigenarten zur Hilfe. Zum einen sind f&uuml;r Videoproduktionen aufgrund der geringen optischen Aufl&ouml;sung Nah- und Detailaufnahmen der Vorzug zu geben, zum anderen erfordert ein Produkt, das aus vielen kleinen Einstellungen besteht, eine schnellere Montage, um den Zuschauer dennoch den Eindruck eines Ganzen zu vermitteln. Wenn der Zuschauer aber gelernt hat, viele Details als ein Ganzes zu begreifen, so braucht er auch nur ab und zu den Fu&szlig; eines digitalen Dinos zu sehen, um sich das Monster in seiner ganzen Pracht vorzustellen.</p>
      
<p>Da&szlig; man es sich finanziell erlauben kann, Szenen mit Video &ouml;fter zu drehen, erleichert das Kombinieren mit andersorts gedrehten/erzeugten Szenen. Die M&ouml;glichkeit der direkte Kontrolle des abgedrehten Materials erleichtert zudem die Verwendung der Montage von einzelen Einstellungen in einem Bild. Fr&uuml;he Musikvideos verwendeten deswegen z. B. gerne Blue Box-Sequenzen, weil diese f&uuml;r den TV-Bildschirm viel leichter zu realisieren waren als f&uuml;r die gro&szlig;e Leinwand. - Nebenbei erw&auml;hnt, werden Kinofilme heute oft mit Hybrid-Systemen gedreht, welche gleichzeitig die Szene auch auf Magnetband bannen.</p>
      
<h4>Birth of a nation (Fortsetzung)</h4>
      
<p>Ist das Fernsehn der Lehrmeister der Nation? Was die Sprache der bewegten Bilder angeht, so gewi&szlig;. Durch seinen populistischen (Nicht-)Anspruch gelang es Sendern wie MTV eine Bildsprache allgemein verst&auml;ndlich zu machen, die schon seit langer Zeit von sog. &gt;Experimentalfilmern&lt; beherrschaft wurde. MTV verfolgte dabei nicht das Interesse, sich zu einer kulturellen Avantgarde zu z&auml;hlen, sondern wollte sich lediglich &uuml;ber die medienspezifischen M&ouml;glichkeiten von den anderen Produkten der kommerziell erzeugten Bilderwelt abzusetzen. Die Produzenten von Darstellungen auf anderen Medien mu&szlig;ten auf diese Herausforderungen reagieren.</p>
      
<p>Im Kino hatte diese Herausforderung unterschiedlich zu beurteilende Folgen. Z. B. konnte MTV darauf verzichten, eine durchgehende story zu erz&auml;hlen. Es ging ja &gt;&gt;nur&lt;&lt; darum, die entsprechenden Bilder zum Produkt &gt;&gt;Rock'n'Roll&lt;&lt; zu liefern, damit sich dieses besser verkauft. Und, bitte, seht MTV nur als Werbeveranstaltung, um Tontr&auml;ger zu verkaufen. Und nun fragt Euch, warum die Filme von David Lynch im Kino an der Kasse honoriert werden, mit all ihren Drehbuchl&uuml;cken.</p>
      
<p>So war es fr&uuml;her: Komplizierte stories mu&szlig;ten daf&uuml;r herhalten, um viele spektakul&auml;re Szenen/Bilder zu rechtfertigen. Es galt ein Drehbuch herzustellen, da&szlig; einen hohen &gt;Schauwert&lt; des realisierten Film m&ouml;glich machte. Gro&szlig;artige Szenen und brilliante Schauspielerauftritte mu&szlig;ten im Rahmen einer story erm&ouml;glicht werden. Heute wird diese Dienstleistungen am Zuschauer erf&uuml;llt ohne story. Den &gt;&gt;Rest&lt;&lt; erzeugt er in seinem Kopf oder er akzeptiert sie einfach so, ohne das Fehlen eines &gt;&gt;Restes&lt;&lt; einzuklagen. Genau wie in einem Madonna-Videoclip.</p>
      
<p>Und es tut nichts zur Sache, da&szlig; z. B. der italienische B-Film immer schon so funktionierte, da&szlig; ein Film aus guten Szenen ohne &Uuml;berg&auml;nge bestanden hat. Der italienische Billigfilm hat nie die Massenwirkung erzielt, welche das Nationen &uuml;bergreifende MTV hat. Nicht nur da&szlig; MTV in der gesamten westlichen Welt gesehen wird, man kann MTV heute auf jeden Sender empfangen. Jede Nachrichtensendung ist heute ein Videoclip: Computererzeugte Schriftz&uuml;ge winden sich ins Bild und Steadycams jagen ihre Motive. - Ich sage nicht, da&szlig; alle Ver&auml;nderungen des TV in den 90er sich monokausal auf das Ph&auml;nomen &gt;MTV&lt; zur&uuml;ckf&uuml;hren lassen. Aber ich sage, da&szlig; die erste Ausstrahlung MTV als ein markantes Ereignis in der Geschichte der Menschheit dargestellt werden kann, weil ab diesen Zeitpunkt die Menschen eine neue Bildersprache erlernten. Der erste Videoclip, das MTV ausstrahlte war &gt;&gt;Video killed the Radiostar&lt;&lt;. Heute mu&szlig; festgestellt werden: &gt;&gt;Video overkilled the Cinema&lt;&lt;.</p>
      
<h4>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/mtv/bunuel.jpg" align="left" height="127" width="180">Film ist Rythmus. Rythmus i&szlig;t Film.</h4>
      
<p>George A. Romero schneidet seine Filme zu &gt;klassicher&lt; rockmusik, Bu-uel lies Tango und Opern zu seinen Stummfilmen laufen. Heute geben Hip Hop und Techno den Takt an, auch wenn die Tonspur anderes besagt. Der Dikatatur der story ist einer Dikatatur des Rythmus gewichen. Wenn der Inhalt keinen Zusammenhang der einzelnen Einstellungen mehr gew&auml;hrleistet, dann mu&szlig; die Form sie zusammenschwei&szlig;en.</p>
      
<p>Beispiel: Detlev Buck. KARNICKELS und WIR K...NNEN AUCH ANDERS erz&auml;hlen stories, deshalb bleibt die Kamera ab und zu auch einfach nur stehen. Dies verleiht den Filmen eine klassische Filmsprache, die George Lucas auch bei STAR WARS bewu&szlig;t einsetzte. STAR WARS sollte schon beim ersten Sehen den Eindruck eines Klassikers hinterlassen.) M&Auml;NNERPENSION ist 90er-Jahre-Kino: chic und cool. Und der n&auml;chste Schnitt mu&szlig; kommen und uns zu der n&auml;chste h&uuml;bsche Kameraeinstellung bringen. So dargestellt wirkt die am Mainstream orientierte Machart als Einschr&auml;nkung eines der begabtesten deutschen Filmemacher, und sie ist doch nur ein Versuch konkurrenzf&auml;hig zu bleiben. Oder genauer: Den Markt f&uuml;r das Produkt Buck auf die MTV-geschulten Teenager auszuweiten, ein finanzstarkes Marktsegment auch in Deutschland (und dummerdings fest in amerkanischer Hand).</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/mtv/spawn.jpg" align="left" height="126" width="180">
Beispiel: &gt;&gt;Spawn&lt;&lt;. Ein Comic wie &gt;&gt;Spawn&lt;&lt; w&auml;re ohne MTV nicht denkbar. Eine derartige Verfilmung leider auch nicht. Der Comic nutzt die Chancen der MTV-Sprache und erz&auml;hlt eine Geschichte in grellen, sprunghaften Einstellungen. Der Film f&uuml;hlt sich dieser Erz&auml;hlhaltung verpflichtet und traut sich dennoch nicht einmal, eine Geschichte zu erz&auml;hlen. Statt desen bekommen wir einen Videoclip von &gt;Marlyn Manson&lt; geboten: Sch&ouml;ne Ghetto-Bilder zu sch&ouml;ner Musik. (Und Buisness-intern hei&szlig;t das dann Cross-Marketing: Euere Musik verkauft unseren Film, und unser Film verkauft Eure Musik. Schei&szlig; Kapitalismus!)</p>
      
<p>Letztes Beispiel: NATURAL BORN KILLERS. Schnelle bewegte Bilder huschen &uuml;ber die Leinwand und begleiten die Musik von LARD &amp; Co. Selten habe ich einen so am&uuml;santen Blutrausch auf der Leinwand gesehen. Ach, das sollte medienkritisch sein? Merke: Man kann MTV nicht durch eine &uuml;bertrieben schnelle, durchgeknallte Bildsprache, die alle Stile wild durcheinanderwirft, kritisieren, denn genau so funktioniert MTV.</p>
      
<h4>Das &gt;Horror Vacui&lt; der Kritiker</h4>
      
<p>Oliver Stone mu&szlig;te nach seinem Versagen, auf die Textebene zur&uuml;ckgehen und in Interviews kundtun, was denn die Aussage von NATURAL BORN KILLERS h&auml;tte sein sollen. In seiner Geschw&auml;tzigkeit r&uuml;ckt er in die N&auml;he Godards und anderen Manifestisten, Avantgardisten und sonstigen Filmemachern, die ihren Filmen mit wichtigen Worten viel Gewicht verleihen wollen. - Auch auf der Ebene der kulturellen Rechtfertigung hat MTV viel f&uuml;r die Befreiung der bewegten Bilder getan. Heute bedeutet Video wirklich &gt;&gt;Ich fliege&lt;&lt; und nicht mehr &gt;&gt;Ich sehe&lt;&lt;, wie es Nam Jam Paik f&uuml;r seine eigenen Arbeiten formulierte.</p>
      
<p>Es ist bezeichnend, da&szlig; diejenigen, denen ihre MTV-&Auml;sthetik am ehesten verziehen wird, die &gt;&gt;jungen&lt;&lt; schwarzen Filmemacher wie Spike Lee sind. Aber hier l&auml;&szlig;t sich diese &Auml;sthetik auch wieder rechtfertigen, wenn auch allerdings nur aufgrund der rassistischen Grundannahme, da&szlig; <i>"die Schwarzen eben Rythmus im Blut haben"</i>.</p>
      
<p>Angreifbar wird die MTV-&Auml;sthetik, weil sie nur entwickelt wurde, um die Produkte der Musikindustrie zu verkaufen. Deswegen entsteht Mi&szlig;trauen, wenn z. B. Rudi Dolezal vielleicht nicht einmal zu unrecht behauptet, da&szlig; die Videoclips in den 90er Jahren dabei sind <i>"die Rolle des Avantgardefilms zu &uuml;bernehmen, ihn abzul&ouml;sen, vielleicht zu ersetzen."</i> Die Experimentalfilmer sind schlie&szlig;lich der Kunst verpflichtet und nicht dem Geld wie die Videoclips, m&ouml;chte man einwenden und dabei vergessen, da&szlig; auch Experimentalfilmer gerne mit ihren Produkten ihre Miete und ihre Br&ouml;tchen verdienen.</p>
      
<h4>Wie man das Staunen verlernt</h4>
      
<p>Vielen Cineasten (und ich schlie&szlig;e mich hier ein) bedauern, da&szlig; die MTViserung des Kinos, da&szlig; Kino plastikm&auml;&szlig;ig gemacht hat, was nicht weniger als unmenschlicher bedeutet. Erinnert Ihr Euch noch an die 70er Jahre als ein Autostunt noch nicht in zahlreiche Einzeleinstellungen zerschnitten wurde und man die Leistung des Stuntman noch richtig w&uuml;rdigen konnte. In Filmen wie THE ROCK leisten Stundmen immer noch das gleiche, vielleicht auch besseres, aber ihre Leistungen werden in viele kurze Einstellungen zerschnitten. Die Leistung des Einzelnen wird durch die Bildsprache unkenntlich gemacht und es entsteht der Eindruck, da&szlig; die Bilder der realen Leistung nicht genug Respekt zollen.</p>
      
<p>Dies ist eine seltsam romatisierende Sicht der Dinge, denn die Leistungen, die f&uuml;r die Herstellung dieser Art von Filmen erbracht werden, sind n&uuml;chtern betrachtet gr&ouml;&szlig;er als die mehr bewunderten. Man bekommt pro Film heute mehr geboten: mehr Kamerafahrten, mehr Effekte und mehr Einstellungen (= mehr Schnitte). An der Herstellung eines Films d&uuml;rften heute im Durchschnitt mehr Menschen mitarbeiten als je zuvor, insofern ist es absurd davon zu sprechen, da&szlig; die Filme unmenschlicher geworden sind. Es sind nicht die Filme, welche die Leistungen des Einzelnen nicht gen&uuml;gend respektieren, es sind wir selbst. Vor 20 Jahren konnten wir sagen: Oh, was ein herrlicher Stunt! Heute m&uuml;&szlig;ten wir sagen: Oh, eine nette Kamerabewegung! Mein Gott, wie toll die Statisten gef&uuml;hrt wurden! Was f&uuml;r ein Stunt! Tolle digitale Bildbearbeitung! (Schnitt) Oh, noch eine gute Kamerabwegung! Noch ein brillinater Stunt! Noch... Soviel Bewunderung &uuml;berfordert freilich jeden Bewunderer. Im cineastischen Overkill haben wir das Staunen verlernt; aber das ist unser ganz pers&ouml;nliches Problem.</p>
      
<h4>Das Ende der Spirale</h4>
      
<p>Wenn man aber einer dieser Romatiker ist, mu&szlig; man dann nicht f&uuml;rchten in Zukunft nur noch die guten alten Filme gucken zu k&ouml;nnen und auf gute neue garnicht mehr hoffen darf? F&uuml;rchtet Euch nicht und hofft, den die Logik des Marktes steht auf unserer Seite.</p>
      
<p>Bei MTV und &auml;hnlichen Sendern gibt es deutliche Ver&auml;nderungen zu beobachten. Die Konkurrenz der Videoclips untereinander hat dazu gef&uuml;hrt, da&szlig; die Langsamkeit neu entdeckt wurde. Um im Montage-Blitzkrieg der Konkurrenzprodukte aufzufallen, greifen inzwischen viele Clips zur Notbremse und drosseln das Tempo. Wenn wir es wirklich MTV zu als das zu verdanken haben, was ich in diesem Aufsatz beschrieben haben, dann sind die langen Einstellungen der Trip Hop-Generation unsere Hoffnungstr&auml;ger. - Allerdings wird &uuml;ber diesen Einflu&szlig; dann niemand schreiben, sondern den Sieg des guten Geschmacks &uuml;ber den b&ouml;sen Kommerz preisen.</p>
      
<p>&nbsp;</p>
<hr>
<h3>Literatur / Quellenangaben</h3>
<p>
<small>Literaturtip:
Zum Weiterlesen empfiehlt der freundliche Verfasser den Sammelband &gt;Visueller Sound - Musikvideos zwischen Avantgarde undPopul&auml;rkultur&lt; (hg. von Celilia Hausherr und Annette Sch&ouml;nholz, erschienen 1994 im Zyklop Verlag/Luzern).
</small>
</p>
    
<br>
<hr size="1" noshade="noshade">
<h3 >Inhalt:</h3>
<ul >
<li>
<small><?php echo CHtml::link('Einführung', array('site/page', 'view'=>'docs.montage.intro'));?></small>
</li>
<li>
<small><?php echo CHtml::link('MTV-&Auml;sthetik&lt;: 128 Cuts per Minute - und kein Ende in Sicht', array('site/page', 'view'=>'docs.montage.mtv'));?></small>
</li>
<li>
<small><?php echo CHtml::link('IA28 - Sergej Eisensteins Weg zur Intellektuellen Montage', array('site/page', 'view'=>'docs.montage.eisenst'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Montageaspekte in den Filmen Wong Kar-Weis', array('site/page', 'view'=>'docs.montage.wong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Die Welt im Querschnitt: Walter Ruttmann und sein Montagefilm BERLIN - DIE SINFONIE DER GROSSTADT', array('site/page', 'view'=>'docs.montage.ruttmann'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Gewaltige Montage-Monumente: Sam Peckinpah', array('site/page', 'view'=>'docs.montage.peckinpah'));?></small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p >
<font size="-2">Impressum</font>
</p>
<p>
<font  size="-2">"Zwischen den Bildern - Aspekte der Filmmontage" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und erscheint begleitend zur Filmreihe im Wintersemester 97/98.
Vervielf&auml;ltigung und Kopieren nur mit vorheriger Genehmigung des AFK</font>
</p>
<p>
<font  size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstr.12<br>
76128 Karlsruhe</font>
</p>
<p>
<font  size="-2">Mitarbeiter an dieser Ausgabe:</font>
</p>
<p>
<font  size="-2">A. G&uuml;nter<br>
M. Hirasaka<br>
M. Nagenborg<br>
H. Stiens<br>
P. Stock</font>
</p>
<p>
<font  size="-2">HTML-Aufbereitung:  Michael Haist</font>
</p>
