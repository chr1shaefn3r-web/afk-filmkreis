<p align="right">
<small>
<?php echo CHtml::link('&gt;&gt;&gt; Begleitheft \'Die Filme von Mike Leigh\'', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Zwischen den Bildern</b></big>
<br>Aspekte der Filmmontage<br>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.montage.ruttmann','')?>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-peckinpah"></a>
<h3>Gewaltige Montage-Monumente: Sam Peckinpah</h3>
      
<p>
<br clear="all">
Seinen legend&auml;ren Ruf als notorischer Querulant und Anh&auml;nger brutaler Gewalt hat Sam Peckinpah nicht unwesentlich dem verschwenderischen und innovativen Gebrauch verschiedenster Montagetechniken zu verdanken.<br>
N&auml;heren Zugang zur Kunst der Montage bekam Peckinpah, der auf einer Ranch in Kalifornien aufwuchs, nach Theaterinszenierungen in seiner Studienzeit sowie anf&auml;nglicher Arbeit als Fernsehregisseur und Drehbuchautor (u. a. GUNSMOKE, dt. Titel Rauchende Colts). Mitte der f&uuml;nfziger Jahre als Assistent des Regisseurs Don Siegel, der zu jener Zeit Leiter der Montage-Abteilung bei Warner-Bros. war.</p>
      
<p>Gewaltdarstellungen spielen in vielen Filmen Peckinpahs eine zentrale Rolle, oft sind sie die Eckpfeiler der Geschichte. Dabei setzt er bewu&szlig;t augekl&uuml;gelte Montagetechniken ein, die er in vorher nie dagewesener Weise kombiniert. Zu seinen auff&auml;lligsten Stilmerkmalen geh&ouml;rt die Manipulation der Bildgeschwindigkeit, vor allem der virtuose Einsatz der Zeitlupe. Durch die Kontrastwirkung von normalemTempo und Zeitlupe werden Einzelheiten, die sonst im unkontrollierten Bilderrausch einer Actionszene untergehen w&uuml;rden, erst f&uuml;r den Zuschauer wahrnehmbar. Peckinpah erreicht eine Intensivierung der Gewalteindr&uuml;cke durch Verlangsamung, Reduktion und prismatische Perspektivwechsel.<br>
Die meist obligatorische Eskalation der Gewalt in seinen Filmen und die Inszenierung  von Hyper-Gewalt-Orgien, wie sie extrem im  ber&uuml;hmt-ber&uuml;chtigten Schlu&szlig;massaker von THE WILD BUNCH-Sie kannten kein Gesetz zum Ausdruck kam (Cutter Robert L. Wolfe dazu: '...wir beschlossen zu versuchen, daraus eine Art Ballett [..] aus fallenden K&ouml;rpern und abgefeuerten Waffen [...] zu machen.'), brachte Peckinpah vielfach den Vorwuf der Gewaltverherrlichung ein. Der Kritiker Joseph Morgenstern lastet ihm zum Beispiel eine Stilisierung von Gewaltt&auml;tigkeit durch Zeitlupe an. Leslie Fiedler, ein renommierter amerikanischer Kulturkritiker spricht dagegen neutraler von <i>"Reflexion der Gewaltt&auml;tigkeit [...] durch die filmischen Techniken der verlangsamten, beschleunigten und festgefrorenen Bilder"</i>.</p>
      
<p>In Wahrheit geht es Peckinpah auch keineswegs um eine Zelebrierung oder &Auml;sthetisierung von Gewalt an und f&uuml;r sich oder um  Schockierung oder Ekelerregung beim Zuschauer (wie im Splatter-Genre) , sondern prim&auml;r einfach um die realistische Darstellung. Hierbei darf man auch den Erz&auml;hlzusammenhang der betroffenen Filme nicht au&szlig;er Acht lassen (haupts&auml;chlich Western, die zu einer Zeit spielen, als die Zeit des goldenen Westens selbst schon lange Legende war). Diese sind &uuml;berwiegend in Zeiten des Umbruchs angesiedelt, wenn die alte Gesellschaftsform am zerbr&ouml;ckeln ist und eine neue erst in undeutlichen Konturen sichtbar, wenn sich vertraute Werte aufl&ouml;sen, wenn Tradition, Ordnung und Vernunft zusammenbrechen. St&auml;ndiger Verlust an Bezugspunkten f&uuml;hrt bei Peckinpahs Protagonisten, die sich gew&ouml;hnlich auf der Flucht, auf Verfolgungsjagden, Rachefeldz&uuml;gen oder Strafexpeditionen befinden, zu Perspektivverlust, und Prinzipienlosigkeit, zu Vereinsamung und zu Zerst&ouml;rungswut aufgrund ungebundener Antriebe. Mit hektischen Schnittfolgen versucht Peckinpah das Chaos einer Welt f&uuml;hlbar zu machen, welches die Figuren in seinen Filmen aus ihrem inneren Gleichgewicht wirft.</p>
      
<p>
<br clear="all">
Schnitt war f&uuml;r Peckinpah  unverzichtbares Instrument der Gestaltung und Strukturierung eines Films. Nach Abschlu&szlig; der Dreharbeiten begann f&uuml;r Peckinpah erst der Hauptteil seines kreativen Schaffens. Die Montage-Sessions im Schneideraum nahmen Monate in Anspruch. Das abgefilmte Material wurde zerst&uuml;ckelt, seziert, sortiert und entsprechend der Filmdramaturgie neu angeordnet , wobei die drei- bis vierst&uuml;ndigen Rohfassungen auf kinogerechte Zeitformate von etwa zwei Stunden zusammenschnurrten. Cutter Lou Lombardo, der zwei Filme Peckinpahs schnitt, beschrieb dessen Methode bei der Herstellung k&uuml;rzerer Fassungen so: 'Er nimmt nur Teile der Sequenzen heraus, so da&szlig; ihre Essenz intakt bleibt'.</p>
      
<p>
<br clear="all">
Neben der f&uuml;r ihn typischen Zeitlupe arbeitete Peckinpah mit Parallelmontage und Gegenschnitt-Folgen (vielfach zur Polarisierung von gegens&auml;tzlichen Charakteren), mit Zeitraffer, mit Wiederholungen und mit der Montage von Standfotos. Charakteristisch sind auch Sequenzen, die aus extrem vielen sehr kurzen Einstellungen zusammengesetzt sind. Auf die Spitze getrieben wird dies im Todesballett des Schlu&szlig;massakers von THE WILD BUNCH mit 352 Einstellungen in nur sechs Minuten und sieben Sekunden. Der ganze Film hatte schlie&szlig;lich die ungew&ouml;hnlich hohe Zahl von mehr als 3600 Einstellungen, 'mehr als jeder andere Farbfilm, der je gedreht wurde' (Lombardo).</p>
      
<p>
<br clear="all">
Kaum jemand kennt jedoch Peckinpahs Filme in ihrer urspr&uuml;nglich vorgesehenen Fassung. Sie wurden verst&uuml;mmelt und entstellt, gek&uuml;rzt und aufgrund halbherziger Verleihstrategien der ...ffentlichkeit kaum zug&auml;nglich gemacht (meist wohl mehr aus kommerziellen als aus ethisch-moralischen Gr&uuml;nden). So geh&ouml;rt es zur ureigenen Tragik seines Schicksals, da&szlig; er, der wie kaum ein anderer die sch&ouml;pferischen M&ouml;glichkeiten von Schnitt nutzte, wie kein anderer unter den restriktiven Folgen von Schnitt zu leiden hatte.</p>
      
<p>So lag Peckinpah in st&auml;ndigem Clinch mit Studios und Produzenten, was sich in der turbulenten Entstehungsgeschichte vieler seiner Filme wiederspiegelt. Angefangen beim mehrfachen Umschreiben der Drehb&uuml;cher &uuml;ber Hangreiflichkeiten am Set und gefeuerte Mitarbeiter zog sich die Kette der Konflikte bis zu den folgenschweren Gefechten im Schneideraum, denen ganze Sequenzen zum Opfer fielen. Die Tatsache, zum Beispiel, da&szlig; bei Peckinpahs PAT GARRETT JAGT BILLY THE KID sechs(!) Cutter im Titelvorspann genannt werden, sagt da alles.
Peckinpah verlor im Kampf gegen das konformistische, an kurzfristigem Profit orientierte System der Filmproduktion Hollywoods die entscheidenden Schlachten um die Endmontage.
In seiner chaotischen Lebensweise, die durch wenig Schlaf einhergehend mit exzessivem Tabletten-, Alkohol- und Zigarettenkonsum gekennzeichnet war, &auml;hnelte er zuletzt mehr und mehr den Outlaws seiner Filme und hauste die letzten Jahre nur noch in Wohnmobilen. Sam Peckinpah starb am 28. Dezember 1984 in Inglewood, Kalifornien.</p>
      
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Filmographie</h3>
<p>
	1961  THE DEADLY COMPANIONS (Gef&auml;hrten des Todes) mit Maureen O'Hara, Brian Keith<br>
	1961/62	 RIDE THE HIGH COUNTRY (Sacramento) mit Joel McCrea, Randolph Scott	<br>
	1964/65	 MAJOR DUNDEE (Sierra Charriba)	 mit Charlton Heston, Richard Harris, James Coburn 1964/65<br>
	1968/69  THE WILD BUNCH (Sie kannten kein Gesetz)	mit William Holden, Ernest Borgnine, Robert Ryan,Warren Oates, Ben Johnson<br>
	1969/70	 THE BALLAD OF CABLE HOGUE (Abgerechnet wird zum Schluss) mit Jason Robards Jr, Stella Stevens <br>
	1971  STRAW DOGS  (Wer Gewalt s&auml;t) mit Dustin Hoffman, Susan George <br>
	1971/72	 JUNIOR BONNER mit Steve McQueen, Robert Preston<br>
	1972	THE GETAWAY mit Steve McQueen, Ali MacGraw<br>
	1972/73	 PAT GARRETT AND BILLY THE KID (Pat Garrett jagt Billy the Kid) mit James Coburn, Kris Kristofferson, Bob Dylan, Jason Robards Jr<br>
	1973/74	 BRING ME THE HEAD OF ALFREDO GARCIA (Bring mir den Kopf von Alfredo Garcia) mit Warren Oates, Isela Vega, Kris Kristofferson<br>
	1975  THE KILLER-ELITE (Die Killer-Elite)  mit James Caan, Robert Duvall<br>
	1976/77	 CROSS OF IRON (Steiner-Das Eiserne Kreuz) mit James Coburn, Maximilian Schell, James Mason  <br>
	1977/78	 CONVOY mit Kris Kristofferson, Ali MacGraw, Ernest Borgnine<br>
	1982/83	 THE OSTERMAN WEEKEND mit Rutger Hauer, John Hurt, Dennis Hopper, Burt Lancaster</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
      
<p>&nbsp;</p>
<hr>
<h3>Literatur / Quellenangaben</h3>
<ul>
<li>
<small>Frank Arnold, Ulrich von Berg: Sam Peckinpah-Ein Outlaw in Hollywood, Ullstein 1987</small>
</li>
<li>
<small><a href="http://www.geocities.com/Hollywood/Academy/1912">http://www.geocities.com/Hollywood/Academy/1912</a></small>
</li>
<li>
<small><a href="http://www.dreamagic.com/roger/peck.html">http://www.dreamagic.com/roger/peck.html</a></small>
</li>
<li>
<small><a href="http://www.film100.com/cgi/direct.cgi?v.peck">http://www.film100.com/cgi/direct.cgi?v.peck</a></small>
</li>
</ul>
    
<br>
<hr size="1" noshade="noshade">
<h3 >Inhalt:</h3>
<ul >
<li>
<small><?php echo CHtml::link('Einführung', array('site/page', 'view'=>'docs.montage.intro'));?></small>
</li>
<li>
<small><?php echo CHtml::link('MTV-&Auml;sthetik&lt;: 128 Cuts per Minute - und kein Ende in Sicht', array('site/page', 'view'=>'docs.montage.mtv'));?></small>
</li>
<li>
<small><?php echo CHtml::link('IA28 - Sergej Eisensteins Weg zur Intellektuellen Montage', array('site/page', 'view'=>'docs.montage.eisenst'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Montageaspekte in den Filmen Wong Kar-Weis', array('site/page', 'view'=>'docs.montage.wong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Die Welt im Querschnitt: Walter Ruttmann und sein Montagefilm BERLIN - DIE SINFONIE DER GROSSTADT', array('site/page', 'view'=>'docs.montage.ruttmann'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Gewaltige Montage-Monumente: Sam Peckinpah', array('site/page', 'view'=>'docs.montage.peckinpah'));?></small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p >
<font size="-2">Impressum</font>
</p>
<p>
<font  size="-2">"Zwischen den Bildern - Aspekte der Filmmontage" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und erscheint begleitend zur Filmreihe im Wintersemester 97/98.
Vervielf&auml;ltigung und Kopieren nur mit vorheriger Genehmigung des AFK</font>
</p>
<p>
<font  size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstr.12<br>
76128 Karlsruhe</font>
</p>
<p>
<font  size="-2">Mitarbeiter an dieser Ausgabe:</font>
</p>
<p>
<font  size="-2">A. G&uuml;nter<br>
M. Hirasaka<br>
M. Nagenborg<br>
H. Stiens<br>
P. Stock</font>
</p>
<p>
<font  size="-2">HTML-Aufbereitung:  Michael Haist</font>
</p>
