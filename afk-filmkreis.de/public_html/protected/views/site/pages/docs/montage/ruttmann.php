<p align="right">
<small>
<?php echo CHtml::link('&gt;&gt;&gt; Begleitheft \'Die Filme von Mike Leigh\'', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Zwischen den Bildern</b></big>
<br>Aspekte der Filmmontage<br>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.montage.wong','docs.montage.peckinpah')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-ruttmann"></a>
<h3>Die Welt im Querschnitt<br>
<small>Walter Ruttmann und sein Montagefilm BERLIN - DIE SINFONIE DER GROSSTADT</small>
</h3>
      
<p>
        
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/collage.jpg" align="left" height="212" width="180">
        <i>"Die Neue Sachlichkeit ist die allgemeine Str&ouml;mung, die gegenw&auml;rtig in Deutschland herrscht, eine Str&ouml;mung des Zynismus und der Resignation auf entt&auml;uschte Hoffnungen. Der Zynismus und die Resignation stellen die negative Seite der Neuen Sachlichkeit dar, ihre positive Seite ist die Begeisterung f&uuml;r die Sachlichkeit, der Wunsch, die Dinge objektiv zu behandeln, so wie sie sind, ohne in ihnen eine ideelle Bedeutung zu suchen."</i>
      
</p>
      
<p>Mit diesen Worten schilderte Gustav Friedrich Hartlaub, Direktor des Mannheimer Kunstmuseums und Sch&ouml;pfer des Begriffs "Neue Sachlichkeit", die Grundvoraussetzungen f&uuml;r die Neuorientierung in den bildenden K&uuml;nsten seit Beginn der zwanziger Jahre und die daraus resultierenden neuen k&uuml;nstlerischen Konzeptionen.
Die K&uuml;nstler haben nach Hartlaubs Meinung genug vom <i>"Hin und Her, von den H&ouml;henfl&uuml;gen und Ausbr&uuml;chen, die doch unweigerlich mit einer Niederlage enden. Es war an der Zeit, die Welt so zu sehen, wie sie ist. Und es gibt viele Dinge in der Welt, die an sich schon sch&ouml;n, wertvoll und n&uuml;tzlich sind. Die Neue Sachlichkeit regt nicht zur Analyse der Zusammenh&auml;nge zwischen den Dingen und den Ereignissen an, sondern sie empfiehlt, die Wirklichkeit in ihrer vorhandenen Form zu billigen. Das ist eine Philosophie der "vollendeten Tatsachen", eine Philosophie des Zynismus und der Kapitulation."</i>
</p>
      
<h4>Deutschland im Zeichen der Neuen Sachlichkeit</h4>
      
<p>In der Malerei der Neuen Sachlichkeit werden Aspekte wie die Anbetung der Technik und die zynische Entlarvung der Lebensweise in einer gro&szlig;kapitalistischen Metropole deutlich, ebenso wie ein Gef&uuml;hl der Resignation: <i>"es hat keinen Zweck, die gesellschaftlichen Widerspr&uuml;che zu beseitigen."</i>
<br>
Trotz dieser gemeinsamen inhaltlichen Tendenzen war die Neue Sachlichkeit keine einheitliche k&uuml;nstlerische Richtung. Bereits Hartlaub unterschied zwei Fl&uuml;gel: einen rechten, die romantische Str&ouml;mung, und einen linken, der eine sozialdemokratische Ausrichtung hatte: <i>"Die Rechten &uuml;berwanden die Entt&auml;uschung durch den Traum, durch eine Flucht aus dem Leben. Die Linken klammerten sich an die Entwicklung der Technik und sehen in den Errungenschaften der Technik eine bessere Zukunft. Aber die einen wie die anderen bem&uuml;hten sich nicht, eine Antwort auf die Frage zu geben: auf welche Weise kann man die Welt verbessern, wenn man seine Aufgabe auf die Registrierung der Fakten und die fotografische Wirklichkeit beschr&auml;nkt?"</i>
</p>
      
<p>Diese n&uuml;chterne, eher beobachtende als bewertende Haltung l&auml;&szlig;t sich nicht nur in der Malerei finden - auch im Bereich der Filmproduktion kam es ab der ersten H&auml;lfte der zwanziger Jahre zu einer Neuorientierung. Diese war zwar nicht so stark und umfassend wie ihr Pendant auf dem Gebiet der Malerei, daf&uuml;r aber ebenso wichtig und innovativ.<br>
Im Gegensatz zur Malerei setzte sich im neusachlichen Film jedoch recht bald eine sozialkritische Haltung durch, die, vor allem unter dem Einflu&szlig; der Weltwirtschaftskrise 1929 - 1933, den Film zu einen gesellschaftsbestimmenden Instrument machte. Aus dem Neusachlichen Film, den man in seiner reinsten Form auch als Absoluten Film bezeichnen k&ouml;nnte, wurde der zun&auml;chst sozialkritische, dann agitatorisch-proletarische Spielfilm.</p>
      
<h4>Die zeitgeschichtlichen Voraussetzungen</h4>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/nosferatu.jpg" align="right" height="181" width="180">Die zeitgeschichtlichen Voraussetzungen f&uuml;r den Neusachlichen Film sind in den Jahren der Stabilisierung von 1924 bis 1929 zu suchen: ab 1924 begann sich das normale Leben wieder einzustellen; die Mark war stabilisiert und Deutschland &uuml;bernahm den Dawes-Plan, der den Modus der Reparationsleistungen festlegte und Deutschlands Eingliederung in das Finanzsystem der Allierten bewirkte. Die Inflation von 1922/23 erschien bald als ferner Alptraum. Die Drohung einer sozialen Revolution begann sich aufzul&ouml;sen, wie zwei Jahre vorher der Vampir in Murnaus NOSFERATU und viele andere phantastische Gestalten des Nachkriegsfilms.</p>
      
<p>
        
<i>"Die Filme der Stabilisierungszeit wandten sich nun verst&auml;rkt der Au&szlig;enwelt zu und verlegten die Betonung von Geistererscheinungen auf Zeiterscheinungen, von imagin&auml;ren auf nat&uuml;rliche Landschaften. Im Wesentlichen waren diese Filme realistisch."</i> <small>(Siegfried Kracauer)</small>
      
</p>
      
<p>Der &Uuml;bergang vom Expressionismus zur Neuen Sachlichkeit darf jedoch nicht als die Abl&ouml;sung der Vertreter einer Kunstrichtung durch die Propheten einer neuen Schule gesehen werden; vielmehr handelte es sich um einen Wandel des Sehens bei den gleichen Leuten.
1923 hatte in Berlin das B&uuml;hnenst&uuml;ck "Nebeneinander" Premiere, dessen Autor Georg Kaiser bis dahin als f&uuml;hrender Dramatiker des Expressionismus galt. Begl&uuml;ckt schrieb der Kritiker Siegfried Jacobson, der den Expressionismus nicht mochte, in der Weltb&uuml;hne:<br>

<i>"Georg Kaiser ist aus der Wolke, die ihn bisher umnebelte, mit beiden F&uuml;&szlig;en auf die Erde gestiegen... Fr&uuml;her bestand seine Welt aus Wesen, die verm&ouml;ge ihrer Hysterie jedem Ereignis wie einer Katastrophe wehrlos ausgeliefert waren... Was davon da war, ist einer Nervigkeit gewichen, die Kaisers endlich errungener kalter und heiterer &Uuml;berlegenheit &uuml;ber den turbulenten Irrsinn unserer Tage durchaus angemessen ist."</i>
<br>
Das war die Neue Sachlichkeit avant la lettre. "Nebeneinander" wurde ein Schl&uuml;sselwort der Neuen Sachlichkeit. Statt nach dem "Absoluten des Isolierten" zu suchen, strebte man nun nach dem "Relativen des Nebeneinander".</p>
      
<p>Ein anderes Schl&uuml;sselwort der Neuen Sachlichkeit wurde "Querschnitt". Der Querschnitt hie&szlig; die ber&uuml;hmteste Kulturzeitschrift der zwanziger Jahre, bis 1926 herausgegeben vom Kunsth&auml;ndler Alfred Flechtheim und ungeheuer einflu&szlig;reich bei der Durchsetzung dieser neuen Richtung. Als "Life-Style-Magazin" der zwanziger Jahre bezeichnet, zog er immer wieder neusachliche Bilder zur Illustrierung seiner Artikel heran: modische Caf&eacute;s und Lokale, kurzlebige Gr&ouml;&szlig;en des Gesellschaftslebens, wie T&auml;nzerinnen und Boxer, aber auch ungew&ouml;hnliche Themen wie die eine zeitlang sehr popul&auml;ren T&auml;towierungen - alles fand Zugang zu den Seiten des Querschnitt, der somit ein visuelles Barometer f&uuml;r die Moden, Lebensstile und wechselnde Objekte der Allt&auml;glichkeit in der Weimarer Republik wurde.</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/skizze.jpg" align="right" height="256" width="160">Auch in der Malerei fand diese Idee Zugang: "Querschnitt" nannte George Grosz, der neben seiner Arbeit f&uuml;r verschiedene kommunistische Publikationen auch Textbeitr&auml;ge f&uuml;r den Querschnitt schrieb, eine Zeichnung von 1919/20, mit der er collagenhaft einen Angriff gegen die &ouml;ffentliche Moral der beginnenden "roaring twenties" ausf&uuml;hrt.
Dargestellt ist das Nebeneinander von B&uuml;rgertum und Proletariat in einer von den deutlich sichtbaren Spuren des gerade &uuml;berstandenen Ersten Weltkrieges gezeichneten Gro&szlig;stadtkulisse: Kriegskr&uuml;ppel, Arbeiter, Polizisten, die Reichswehr, die gerade Aufst&auml;ndische exekutiert, Bauchladenverk&auml;ufer und wohlhabende Herren mit ihren k&auml;uflichen Damen. Gro&szlig;stadtleben, Prostitution, Elend und soziale Spannungen; dies alles wird nebeneinander und in mehrfacher &Uuml;berlagerung gesehen.</p>
      
<p>Die Zeichnung gibt recht eindrucksvoll Groszs&laquo; Charakterisierung der "roaring twenties" wieder: <i>"&Uuml;berall erschollen Ha&szlig;ges&auml;nge. Alle wurden geha&szlig;t: die Juden, die Kapitalisten, die Junker, die Kommunisten, das Milit&auml;r, die Hausbesitzer, die Arbeiter, die Arbeitslosen, die Schwarze Reichswehr, die Kontrollkommissionen, die Politiker, die Warenh&auml;user und nochmals die Juden. Es war eine Orgie der Verhetzung, und die Republik war schwach, kaum wahrnehmbar... Es war eine v&ouml;llig negative Welt, mit buntem Schaum obendrauf, den viele f&uuml;r das wahre, das gl&uuml;ckliche Deutschland vor dem Anbruch der neuen Barbarei hielten."</i>
</p>
      
<h4>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/abenteuer.jpg" align="left" height="150" width="180">Die Querschnittfilme</h4>
      
<p>Ihren fr&uuml;hesten filmischen Ausdruck fand die Neue Sachlichkeit in den sogenannten Querschnittfilmen , die jeweils ein bestimmtes Detail aus dem Alltag zum Thema erhoben. "Im breiten Querschnitt" das typische Leben der Gro&szlig;stadtmenschen zu zeigen war auch die erkl&auml;rte Absicht von Bela Balazs bei der Konzeption des Films K 13513 - DIE ABENTEUER EINES ZEHNMARKSCHEINS (1926, Regie Berthold Viertel), der zum programmatischen Pionierwerk dieses neuen Genres wurde. Die Hauptrolle wurde passenderweise mit einer Sache, einem Geldschein, besetzt. Anhand dieses Zehnmarkscheines, der durch die H&auml;nde der verschiedensten Bewohner Berlins aus den unterschiedlichsten Milieus wandert, wird in mehreren Episoden das Leben w&auml;hrend der Inflation geschildert.<br>

<i>"Die Akrobatik eines ins Bild gewehten Papierstreifens, sein Fahren, Flattern und Streifen durch die Lebensbezirke der Gro&szlig;stadt..."</i> bildet <i>"...die rein optisch empfundene Bildidee"</i>, beschreibt der Regisseur die Grundstruktur seines Erstlingswerkes.</p>
      
<p>Aus dem gezeigten Querschnitt ergibt sich, abgesehen von der Mannigfaltigkeit der Ereignisse, eigentlich nur, da&szlig; das Leben unaufh&ouml;rlich flie&szlig;t, keinen Anfang und kein Ende hat. Der Film spiegelt es wieder wie in einem Kaffeehausfenster, in dem man immer neue Passanten, neue Autos und Autobusse sieht. Balazs gab f&uuml;r diese Erz&auml;hlweise auch die theoretische Begr&uuml;ndung: Das Leben sollte nicht <i>"unter dem Blickwinkel nur einer einzigen Person und innerhalb der Grenzen ihrer Erlebnisf&auml;higkeit"</i> dargestellt werden, sondern <i>"im breiten Querschnitt: nicht eines Menschen zuf&auml;lliges Leben, sondern das typische Leben schlechthin"</i>. Auch in diesen Theorien spukt wieder die Idee von der Abbildung des allt&auml;glichen Lebens, <i>"so wie es ist"</i>.<br>
Da viele Querschnittsfilme auf Archivmaterial zur&uuml;ckgriffen, boten sie die M&ouml;glichkeit, billig produziert zu werden und zugleich viel zu zeigen ohne etwas zu enth&uuml;llen.</p>
      
<p>Ebenfalls zu den Querschnittsfilmen geh&ouml;rt MENSCHEN UNTEREINANDER - ACHT AKTE AUS EINEM INTERESSANTEN HAUSE (1926) von Gerhard Lamprecht, eine filmische Recherche, die an einem "locus classicus" festmacht ist, und somit zum Modell f&uuml;r das spezifisch berlinerische Genre der Pensions-Filme  wird. </p>
      
<p>In einem ehemaligen Herrschaftshaus in Berlin, das jetzt in Appartements aufgeteilt ist, zeigt der Film einige Tage aus dem Leben der unterschiedlichsten Mietparteien: im Erdgescho&szlig; wohnen ein Anwalt und ein Juwelier, im 1. Obergescho&szlig; ein Beamter, dessen Frau wegen Totschlags im Gef&auml;ngnis sitzt und eine appetitliche Witwe, die einem Heiratsschwindler zum Opfer f&auml;llt; im 2. Obergescho&szlig; befinden sich eine Tanzschule und eine Heiratsvermittlung; im Dachgescho&szlig; wohnen ein alter, halbblinder Klavierlehrer und ein Luftballonverk&auml;ufer.</p>
      
<p>Obwohl sich in beiden Filmen das "typische Leben" noch hinter einer stark kolportagehaften Handlung versteckt, verweisen sie bereits auf einen programmatischen Aspekt der Neuen Sachlichkeit: Die Aufwertung eines Gegenstandes oder eines Ortes zum "Handelnden" oder handlungsbestimmenden Element.</p>
      
<h4>BERLIN - DIE SINFONIE DER GROSSTADT</h4>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/berlin1.jpg" align="left" height="150" width="200">Die Querschnittskonzeption lag auch Walter Ruttmanns Reportagefilm BERLIN - DIE SINFONIE DER GROSSTADT (1927) zugrunde. Der Film wirkt wie die Realisierung und zugleich Vollendung des Programms, das der zweite Herausgeber des Querschnitt, Hermann von Wedderkopp, schon 1926 f&uuml;r das Kino, in dem er - neben der Zeitung - <i>"das ideale Kunstprodukt dieser Zeit"</i> sah, aufstellte: <i>"Spielerischer Ausdruck einer idealen Wirklichkeit, die frisch geschehene, unverf&auml;lschte, unverkl&auml;rte Wirklichkeit, mit gro&szlig;em Stadtausschnitt, B&auml;umerauschen, Brandung, Eleganz, Technik, Sport, bekannten und anonymen Gesichtern und dem ganzen wunderbaren Durcheinander... Wirklichkeit ist heute Qualit&auml;tsprobe, Voraussetzung f&uuml;r das Wort "neu". Da&szlig; man sie selten findet, beweist, wie schwierig es ist, sie einzufangen und k&uuml;nstlerisch zu pr&auml;sentieren. Dazu geh&ouml;rt Phantasie, denn im Grunde genommen ist Wirklichkeit stets das Phantastische. Phantasie ist nicht ins Wesenlose schweifende Erfindung, sondern Kombinationsgabe. Sehen des Wesentlichen, Bestimmenden und Aussondern des Toten. Wirklichkeit ist Synonym f&uuml;r Echtheit, Erlebnis, Lebendigkeit."</i>
</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/berlin2.jpg" align="left" height="148" width="400">Berlin wird mit den Augen eines Reisenden gesehen, der im noch schwachen Licht des erwachenden Morgens im Schnellzug - Richtung Berlin - durch die Vorst&auml;dte zum Anhalter Bahnhof f&auml;hrt. Der Tag d&auml;mmert. Je heller es wird, desto mehr Menschen f&uuml;llen die stillen Stra&szlig;en. Die Stadt erwacht: man sieht Polizeistreifen, versp&auml;tete Nachtbummler und morgendliche Plakatkleber. Dann machen sich die Menschen auf den Weg zur Arbeit: mit Ringbahn, Autobus, Untergrund-, Stra&szlig;enbahn und Auto fahren Arbeiter und kleine Angestellte in ihre Fabriken und B&uuml;ros. In den Fabriken h&auml;mmert der Rhythmus der Maschinen. Die Gesch&auml;fte &ouml;ffnen, Kinder gehen zur Schule, Hausfrauen gehen ihrer Arbeit nach. Je weiter der Tag fortschreitet, desto lebendiger wird die Stadt. Um 12 Uhr ist Mittagspause: die Arbeiter essen, im Restaurant wird gegessen, die Zootiere werden gef&uuml;ttert. Nach der Mittagsruhe geht die Arbeit weiter. Zeitungen berichten von Krisen, Mord und B&ouml;rsengesch&auml;ften. Auf den Stra&szlig;en herrscht allgemeine Hektik. Die Beine der Passanten eilen vorbei. Der Verkehr wird immer dichter. Bettler und Steichholzverk&auml;ufer stehen vor einem Juweliergesch&auml;ft. Dann bricht der Abend &uuml;ber Berlin herein. Werkst&auml;tten und B&uuml;ros schlie&szlig;en; die Maschinen werden abgestellt. Die R&uuml;ckwanderung der Massen beginnt. Leuchtreklamen und die Lichter der Vergn&uuml;gungsst&auml;tten flammen auf. Leute stehen vor Schaufenstern und vor einem Kino. Die Menschen vergn&uuml;gen sich im Variet&eacute;, im Kino (man sieht Chaplins Beine) und im Spielklub. In einem Tanzlokal spielt eine Jazzband. Immer noch herrscht Hektik: die Beine von Revuegirls, Theaterschauspielern und Passanten dominieren den Bildausschnitt. Regen setzt ein und auf den n&auml;chtlichen Stra&szlig;en beginnen Gleisarbeiten. Das Nachtleben verlagert sich in Kneipen und Hinterzimmer, wo Kartenspieler ihr Gl&uuml;ck versuchen. Noch sp&auml;ter zieht sich das Leben in der Stadt  zur&uuml;ck und die n&auml;chtlichen Stra&szlig;en leeren sich.</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/reklame.jpg" align="right" height="258" width="180">Uraufgef&uuml;hrt wurde BERLIN - DIE SINFONIE DER GROSSTADT am 23. September 1927 im Berliner Tauentzien-Palast, begleitet von einem gro&szlig;en Live-Orchester unter Leitung des Komponisten Edmund Meisel, der auch die vielbeachtete Musik f&uuml;r Eisensteins PANZERKREUZER POTEMKIN geschrieben hatte. Die Orchesterpartitur ist nicht mehr vollst&auml;ndig erhalten, ein Klavierauszug belegt jedoch, da&szlig; es sich um eine "funktionale" Musik gehandelt haben mu&szlig;, d. h. um eine unmittelbar auf die Bildrhythmik bezogene Komposition als Teil des "Gesamtkunstwerkes".</p>
      
<p>Das Premierenpublikum erwartete laut Vorank&uuml;n-digung ein Film, der <i>"mit allem bricht, was der Film bisher gezeigt hatte. Es spielen keine Schau-spieler, und doch handeln Hunderttausende. Es gibt keine Spielhand-lung, und es erschlie&szlig;en sich doch ungez&auml;hlte Dramen des Lebens. Es gibt keine Kulissen und keine Ausstattung, und man schaut doch in der hundertpferdigen Flucht die unz&auml;hligen Gesichter der Millionenstadt. Pal&auml;ste, H&auml;userschluchten, rasende Eisenbahnen, donnernde Maschinen, das Flammenmeer der Gro&szlig;stadtn&auml;chte - Schulkinder, Arbeitermassen, brausender Verkehr, Naturseligkeiten, Gro&szlig;stadtsumpf, das Luxushotel und die Branntweindestille ... Der m&auml;chtige Rhytmus der Arbeit, der rauschende Hymnus des Vergn&uuml;gens, der Verzweiflungsschrei des Elends und das Donnern der steinernen Stra&szlig;en - alles wurde vereinigt zur Sinfonie der Gro&szlig;stadt."</i>
</p>
      
<p>Was Regisseur Walter Ruttmann seinem Publikum anbietet, war als Idee nicht mehr ganz neu: Alberto Cavalcantis "Stadtfilm" &uuml;ber Paris RIEN QUE LES HEURES (NICHTS ALS STUNDEN, F 1926) d&uuml;rfte Ruttmann zumindest bekannt gewesen sein, ebenso der Film MOSKAU (1927) von Mikhail Kaufmann und Ilya Kopalin. In Anbetracht der &uuml;berm&auml;chtigen Konkurrenz durch zeitgen&ouml;ssische und historische Melodramen, Kom&ouml;dien und R&uuml;hrst&uuml;cke war sein Konzept jedoch umso k&uuml;hner: ein abendf&uuml;llender Dokumentarfilm tritt in Konkurrenz zum sonstigen Spielfilmangebot und wird wider Erwarten ein aufsehenerregender Premierenerfolg, was umso erstaunlicher ist, als er auf alle kassenwirksamen Elemente des Spielfilms verzichtet und trotz seiner L&auml;nge von &uuml;ber 60 Minuten ohne die im Stummfilm &uuml;blichen Zwischentitel auskommt - ein Kunstgriff, der Ruttmanns dokumentarischem Konzept entgegenkommt.</p>
      
<h4>Malerei mit Licht - Walter Ruttmann und die Avantgarde</h4>
      
<p>Wie bereits eingangs erw&auml;hnt, lassen sich die Wurzeln der Neuen Sachlichkeit oft im Expressionismus finden. So waren auch die Gestalter von BERLIN - DIE SINFONIE DER GROSSTADT zuvor prominente Exponeten des deutschen Filmexpressionismus: Carl Mayer, der CALIGARI-Autor, hat die Idee geliefert, Karl Freund, der Kameramann von Murnau und Lang, hat an der Konzeption mitgewirkt und den Film produziert; Regisseur Walter Ruttmann, der hier im Wesentlich ein Regisseur der Montage ist, war zun&auml;chst Maler und bildete dann zusammen mit Viking Eggeling und Hans Richter die Avantgarde des deutschen Experimentalfilms.</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/entwurf.jpg" align="left" height="225" width="200">Ruttmann hatte in Z&uuml;rich Architektur studiert und sich bereits vor dem Ersten Weltkrieg als Maler und Grafiker etabliert. Seine Portraitzeichnungen verkauften sich gut und bei Plakatwettbewerben gewann er mehrere erste Preise. Nach dem Krieg wandte er sich von einem von Corinth beeinflu&szlig;ten Impressionismus ab und begann abstrakt zu malen.</p>
      
<p>Seine gro&szlig;e Leidenschaft war jedoch schon immer die "neue Kunstform" gewesen, das Lichtspiel, von Ruttmann auch als "Malerei mit Zeit" bezeichnet.
<i>"Ruttmann war stets ein eifriger Besucher des Kinos gewesen, und alle Bewegungsvorg&auml;nge im Kino hatten ihn in der Evolution der Linie stark gefesselt. Er erkannte auch die ungeheuere Bedeutung des Kinos f&uuml;r das allgemeine Publikum; lehnte den Film als Roman ab und &uuml;berlegt immer wieder, wie ein Film als Kunstwerk gestaltet sein m&uuml;&szlig;te. So kam er, der den Film optisch auffa&szlig;te, dazu, ein "bewegtes" Bild schaffen zu wollen."</i> <small>(Max Butting, Musiker und enger Freund Ruttmanns)</small>
</p>
      
<p>In einem 1917 entstandenen Aufsatz benennt Ruttmann die Ausdrucksmittel der Kinematographie, deren Gesetze f&uuml;r ihn am n&auml;chsten mit denen der Malerei und des Tanzes verwandt sind: <i>"Formen, Fl&auml;chen, Helligkeiten und Dunkelheiten mit all den ihnen innewohnenden Stimmungsgehalt, vor allem aber die Bewegung dieser optischen Ph&auml;momene, die zeitliche Entwicklung einer Form aus einer anderen. 
[...]
 Zum Beispiel: Die bewu&szlig;te Vermittlung eines bestimmten Ausdrucks durch die Folge eines dunklen Filmabschnitts durch einen hellen, ein Crescendo der Handlung durch einen sich steigernden Rhythmus in der Aufeinanderfolge einzelner Filmabschnitte, durch eine fortschreitende Verk&uuml;rzung, Verl&auml;ngerung, Verdunklung oder Aufhellung dieser Teile, durch die pl&ouml;tzliche Einf&uuml;hrung eines Vorgangs mit ganz andersartigem optischen Charakter, durch die Entwicklung unruhiger, wilder Bewegtheit aus einer ruhenden Masse, durch bildm&auml;&szlig;ig komponierte Einheit zwischen den Menschen und den sie umgebenden Naturformen - und durch tausend andere und damit zusammenh&auml;ngende M&ouml;glichkeiten des Wechsels von Hell und Dunkel, von Ruhe und Bewegung ..."</i>
</p>
      
<p>1919 gr&uuml;ndete er seine eigene Filmgesellschaft, die Ruttmann-Film GmbH mit Sitz in M&uuml;nchen. 1920 lie&szlig; er sich einen selbst konstruierten Tricktisch patentieren. Auf diesem entstand in monatelanger Arbeit sein erster Film, der aus 10.000 einzeln eingef&auml;rbten Bildern bestehenden Trickfilm LICHTSPIEL OPUS 1, dessen optisches Konzept am treffendsten Alfred Kerr im "Berliner Tageblatt" charakterisiert hat:<br>

<i>"Man denkt an Expressionistenbilder. Die sind aber unbewegsam. Chagalls Leuchtparadiese bleiben doch starr. Die Funkelfuturismen der neuesten Pariser versteinern reglos - im Rahmen. Hier aber flitzen Dinge, rudern, brennen, steigen, sto&szlig;en, quellen, gleiten, schreiten, welken, flie&szlig;en, schwellen, d&auml;mmen; entfalten sich, w&ouml;lben sich, breiten sich, verringern sich, kugeln sich, engen sich, sch&auml;rfen sich, teilen sich, kr&uuml;mmen sich, heben sich, f&uuml;llen sich, leeren sich, bl&auml;hen sich, ducken sich; bl&uuml;meln und verkr&uuml;meln sich. Kurz: Expressionismus in Bewegtheit. Ein Rausch f&uuml;r die Pupille ... Aber kein Menschenfilm."</i>
</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/schere.jpg" align="left" height="140" width="200">1921 konnte Walter Ruttmann seinen Erstling der &Ouml;ffentlichkeit vorf&uuml;hren, bereits ein Jahr sp&auml;ter folgte OPUS 2, der im Gegensatz zu seinem Vorg&auml;nger in der <i>"rhythmischen Gliederung der Erscheinungsgruppen &uuml;bersichtlicher"</i> und <i>"klar periodisiert"</i> wirkt. (Bernhard Diebold, Kritiker der "Frankfurter Zeitung" und ein Verfechter des gemalten Films als "neuer Augenkunst".)</p>
      
<p>Praktisch &uuml;ber Nacht war Walter Ruttmann zu einem Pionier der deutschen Film-Avantgarde geworden. Als Erster war er in der Lage, abstrakte farbige Filme in &uuml;berzeugender Qualit&auml;t herzustellen, was ihm die sofortige Aufmerksamkeit der Werbebranche sicherte: ab 1922 betrat er mit seinen kolorierten und viragierten abstrakten Werbefilmen im Stil der beiden OPUS-Filme ebenfalls Neuland.</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/falken.jpg" align="left" height="135" width="200">Um die Jahreswende 1922/23 zog er von M&uuml;nchen nach Berlin, also in das Zentrum der deutschen Filmindustrie, wo er zun&auml;chst an Lotte Reinigers abendf&uuml;llenden Scherenschnittfilm DIE ABENTEUER DES PRINZEN ACHMED arbeitete. (Von Ruttmann stammen abstrakte Muster, Wolkenhintergr&uuml;nde, Blitze , Feuerschwaden und Geistererscheinungen, die er zu den Zauberszenen beisteuerte.) Auf die Vermittlung von Lotte Reiniger engagierte ihn Fritz Lang, um die "Falkentraum"-Tricksequenz f&uuml;r den ersten Teil der NIBELUNGEN zu gestalten.</p>
      
<p>W&auml;hrend dieser Zeit arbeitete Ruttmann eng mit der Bauhaus-Sch&uuml;lerin Lore Leudesdorff zusammen. Unter ihrem Einflu&szlig; vollzog sich bei ihm eine Abwendung von mehr naturhaften Formungen hin zu sachlich-geometrischen Motiven, wie sie in OPUS 3 und OPUS 4 (1924/25) zu finden sind; eine Entwicklung, die schlie&szlig;lich in der Komposition seines Berlin-Films ihren H&ouml;hepunkt fand.</p>
      
<p>Interessanterweise l&auml;&szlig;t sich in BERLIN eine kurze Sequenz finden, die eine tats&auml;chliche Schnittstelle zwischen dem absoluten und dem neusachlichen Film markiert: wenn gleich zu Beginn des Films der Zug in Richtung Berlin f&auml;hrt, heben sich entlang der Gleise Schlagb&auml;ume, deren Diagonalen gleich darauf in einer sehr kurzen Tricksequenz mit abstrakten, die Geschwindigkeit des Zuges verdeutlichenden horizontalen und diagonalen Linien aufgenommen werden.</p>
      
<h4>Bewegung als optisches Prinzip</h4>
      
<p>Die vier wichtigsten Faktoren bei der Herstellung seines Berlin-Films fa&szlig;t Walter Ruttmenn folgenderma&szlig;en zusammen:<br>

<i>"1. Konsequente Durchf&uuml;hrung der musikalisch-rhythmischen Forderungen des Films, denn Film ist rhythmische Organisation der Zeit durch optische Mittel.<br>
2. Konsequente Abwehr von gefilmtem Theater.<br>
3. Keine gestellten Szenen. Menschliche Vorg&auml;nge und Menschen wurden "beschlichen". Durch dieses "Sich-unbeobachtet-glauben" entstand Unmittelbarkeit des Ausdrucks.<br>
4. Jeder Vorgang spricht durch sich selbst - also: keine Titel."</i>
</p>
      
<p>Die geforderte "Unmittelbarkeit des Ausdrucks" verpflichtete die Macher des Films dazu, sich ausschlie&szlig;lich auf authentisches, nicht inszeniertes Material zu st&uuml;tzen. Kameramann Karl Freund &uuml;ber diese neue Art der Gestaltung: <i>"Ich wollte alles aufnehmen. Menschen, die aufstehen, um zur Arbeit zu gehen, die fr&uuml;hst&uuml;cken, Stra&szlig;enbahn fahren oder laufen. Meine Charaktere stammen aus allen Lebensbereichen. Vom niedrigsten Arbeiter bis zum Bankpr&auml;sidenten."</i> <small>("Karl Freund, Candid Cinematographer"; in Popular Photography, Februar 1939)</small>
<br>
Freund wu&szlig;te, da&szlig; er f&uuml;r solche Zwecke auf die Methoden des Dokumentarfilms zur&uuml;ckgreifen mu&szlig;te. Als begabter Techniker machte er das damals erh&auml;ltliche Filmmaterial hochempfindlich, um so auch unter schlechten Lichtbedingungen filmen zu k&ouml;nnen. Au&szlig;erdem dachte er sich Tricks aus, um seine Kamera w&auml;hrend des Drehens zu verstecken: er fuhr z. B. in einem halboffenen Lastwagen mit Schlitzen f&uuml;r das Objektiv in den Seitenplanen oder versteckte seine Kamera in einem Kasten, der wie ein ganz harmloser Koffer aussah. Auf die Frage, ob er die dokumentarische Fotografie als Kunst betrachte, antwortete Freund au&szlig;er sich vor Begeisterung: <i>"Es ist die einzige Art der Photographie, die wirklich Kunst ist. Warum? Weil man mit ihr das Leben portr&auml;tieren kann. Diese gro&szlig;en Photographien, auf denen die Leute geziert l&auml;cheln und grimassieren und sich ins Bild setzen... Bah! Das ist keine Photographie. Ein leicht handhabbares Objektiv, Aufnahmen vom Leben, Realismus dagegen: das ist Photographie in ihrer reinsten Form."</i>
</p>
      
<p>Um allerdings dem Leben der Gro&szlig;stadt in Rhythmus und Tempo gerecht zu werden, war die Montage des Bilder mindestens so wichtig wie die Authentizit&auml;t des gefilmten Materials. Zu den dominierenden optischen Motiven geh&ouml;ren der Verkehr und die Beine der Menschen, die im neusachlichen Film die Allt&auml;glichkeit des Geschehens verdeutlichen - sei es auf der Stra&szlig;e, in der Revue oder auf einer Demonstration. St&auml;ndige Bewegung ist der beherrschende Eindruck und die &Uuml;bersetzung in einen ad&auml;quaten Filmrhythmus das angestrebte Ziel.<br>
Ruttmann beschreibt die hierbei auftretenden Probleme: <i>"Beim Schneiden zeigte sich, wie schwer die Sichtbarmachung der sinfonischen Kurve war, die mir vor Augen stand. Viele der sch&ouml;nsten Aufnahmen mu&szlig;ten fallen, weil hier kein Bilderbuch entstehen durfte, sondern so etwas wie das Gef&uuml;ge einer komplizierten Maschine, die nur in Schwung geraten kann, wenn jedes kleinste Teilchen mit genauester Pr&auml;zision in das andere greift. (...) Nach jedem Schnittversuch sah ich, was mir noch fehlte, dort ein Bild f&uuml;r ein zartes Crescendo, hier ein Andante, ein blecherner Klang oder ein Fl&ouml;tenton und danach bestimmte ich immer von neuem, was aufzunehmen und was f&uuml;r Motive zu suchen waren."</i>
</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/tanzen.jpg" align="left" height="248" width="180">Was Ruttmann als "sinfonische Kurve" bezeichnet, sollte sich im Wesentlichen aus der Montage der Bilder ergeben. So kommt es, da&szlig; BERLIN bei einer Gesamtdauer von 61 Minuten aus der beachtlichen Anzahl von 1009 Einstellungen zusammengesetzt ist. Die durchschnittliche Einstellungsl&auml;nge liegt also bei 3,6 Sekunden - und somit erheblich unter der entsprechender Spielfilme der damaligen Zeit - was jedoch nicht bedeuten soll, da&szlig; der Film dieses Tempo &uuml;ber seine ganze L&auml;nge durchh&auml;lt. Die Dauer der einzelnen Einstellungen schwankt zwischen 45,5 Sek. als Maximum und 0,2 Sek. als Minimalwert, wobei die durchschnittliche Einstellungsdauer, bezogen auf die f&uuml;nf inhaltlichen Abschnitte des Films (Nacht, Morgen, Vormittag, Mittag, Abend), relativ einheitlich ist. Aus dieser inhaltlichen Gliederung folgt eine Strukturierung des Tagesablaufs in Aktivit&auml;tsphasen, die vom Schnittempo entsprechend begleitet werden: Erwachen der Stadt und Arbeitsbeginn, Mittagspause und Nachmittagstempo, Feierabend und Gro&szlig;stadtnacht.</p>
      
<p>Interessant sind also die extremen Schwankungen innerhalb der thematischen Abschnitte, die sowohl schnelle Schnittfolgen als auch l&auml;ngere Einstellungen enthalten, was sich gleich an der Exposition (Morgengrauen, der Zug f&auml;hrt Richtung Berlin) gut zeigen l&auml;&szlig;t: die anf&auml;nglichen Einstellungen werden zun&auml;chst mit &Uuml;berblendungen verbunden, was aufgrund des zeitintensiven "weichen" &Uuml;bergangs Blende kein hohes Schnittempo zul&auml;&szlig;t. Doch schon in der zweiten Minute erh&ouml;ht sich dieses um ein vielfaches, auf &uuml;ber 50 Schnitte in einer einzigen Minute, was innerhalb des gesamten Films einen absoluten H&ouml;hepunkt darstellt. Wenn sich der Zug in der dritten und vierten Minute Berlin n&auml;hert, verlangsamt sich seine Fahrt und somit auch der Wechsel der Einstellungen, bis hin zum Stillstand des Zuges. Der Wechsel der Einstellungen n&auml;hert sich ebenfalls dem absoluten Tiefpunkt vom zwei Schnitten pro Minute, was die Atmosph&auml;re der noch schlafenden Stadt verdeutlicht. Erst ab der elften Minute nimmt das Tempo wieder zu (die Stadt erwacht) und steigert sich bis zur 14. Minute (die Arbeit in den Fabriken beginnt). Dann f&auml;llt das Tempo wieder etwas ab (Gesch&auml;fte &ouml;ffnen, Berufsverkehr) und steigert sich bis zur 23. und 24. Minute (Hektik im B&uuml;robetrieb) zu einem neuen H&ouml;hepunkt; usw ...</p>
      
<p>Ein wesentlicher Gesichtspunkt der Montage war die <i>"straffe Organisation des Zeitlichen nach streng musikalischen Prinzipien"</i> <small>(Ruttmann)</small>. So wird auch in der Literatur immer wieder der Versuch unternommen, anhand der beschriebenen Tempowechsel - sowohl innerhalb einzelner Abschnitte, als auch auf den gesamten Film bezogen - ein musikalisches Prinzip abzulesen, etwa die Satzfolge der Sinfonie: schnell (Allegro), langsam (Andante), m&auml;&szlig;iges Tempo (Menuett), schnell (Allegro). <i>"Das kurze einleitende "Andante" der stillen Morgenstunden, in denen Berlin erwacht; das alsbald einsetzende st&uuml;rmische "Allegro con fuoco" des Verkehrs (...); das gewaltig aufklingende "Adagio" eines Intermezzos (...); das frohe "Allegretto" der Sport gewidmeten Freistunden und endlich das gl&auml;nzende, rauschende "Finale" des abendlichen Berlins."</i> <small>(Jeanpaul Goergen in (7))</small>
</p>
      
<p>Allerdings darf man aus der Anlehnung an eine musikalische Ordnung kein stringentes Strukturprinzip ableiten; vielmehr bildet die Orientierung an der sinfonischen Satzfolge in Film allenfalls den Ausgangspunkt f&uuml;r eine rhythmische Untergliederung im Sinne "optischer Musik".
Eine thematische Orientierung kommt der Intention des wirklichkeitsgetreuen Stadtportraits wesentlich n&auml;her: <i>"die Rhythmik der Montage ... ist im wesentlichen eine analoge Umsetzung der Rhythmik des st&auml;dtischen Tagesablaufs: nach den Strapazen der Anreise die Ruhe am fr&uuml;hen Morgen, die Hektik bei Arbeitsbeginn, der Mittagstrubel, die zwischenzeitliche Erholung am fr&uuml;hen Nachmittag, die wieder aufbrechende Betriebsamkeit des Feierabendverkehrs, die Abendvergn&uuml;gen."</i> <small>(Helmut Korte in (8))</small>
</p>
      
<p>
<br clear="all">
<img alt="" vspace="10" hspace="10" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/montage/ruttmann/schreibzug.jpg" align="left" height="143" width="419">Im Sinne der rhythmischen Unterst&uuml;tzung der Bilder werden beispielsweise auch die bewegungsintensiven Motive (flie&szlig;ender Verkehr, laufende Menschenmassen, Beine, rasende Maschinen ...) durch ein besonders hohes Schnittempo hervorgehoben. Der hierbei erreichte Geschwindigkeitseindruck wird durch die kontrastierend eingesetzten unterschiedlichen Bewegungsrichtungen sogar noch verst&auml;rkt.</p>
      
<h4>Im "Querschnitt" der Kritiken</h4>
      
<p>Die zeitgen&ouml;ssische Kritik nahm den Film im Gro&szlig;en und Ganzen zustimmend bis enthusiastisch auf: <i>"...was der Film an Bildern bringt, ist so unerh&ouml;rt, so neuartig, so packend gesehen, da&szlig; kleine, kritische Einw&auml;nde ohne Bedeutung f&uuml;r den Gesamteindruck bleiben. Nie vorher sah man das Problem der Maschine auf &auml;hnlich phantastische Art gemeistert und symbolisch gesteigert. Ruttmann verschmilzt Mensch und Stadt. Er besch&auml;ftigt keine Schauspieler, er kennt nicht einmal Gesichter. Er gibt von dem Menschen die Geb&auml;rde, die Figur, das Tempo. Er ist sachlich und n&uuml;chtern. Aber gerade deshalb bezaubert der Film. Eine Meisterleistung."</i>, konnte man am 24.09.1927 im Kinematograph  lesen.<br>
Weiter: <i>"Ein einzigartiger, ein erregender, ein &uuml;berw&auml;ltigender Film! Eine ganz gro&szlig;e Leistung! M&ouml;glich freilich nur durch die Erfindung der Hypersensibilisierung des Negativs durch Kuntze-Safra, deren Verdienste der geschw&auml;tzige Vorspann nicht gen&uuml;gend unterstreicht. Um die Schaffung dieser 1466 Filmmeter bem&uuml;hen sich Carl Mayer, Karl Freund, Reimar Kuntze, Robert Baberske, Laszlo Sch&auml;ffer und Walter Ruttmann, welcher letztlich als Leiter des Ganzen anzusehen ist."</i>
</p>
      
<p>Der russische Regisseur Wsewolod Pudovkin, selbst ein gro&szlig;er Meister der Bildmontage und ein Bewunderer Ruttmanns, schrieb &uuml;ber BERLIN: <i>"In seinem Film "stellte" Ruttmann niemals etwas nur "vor", immer "wirkt" er &uuml;ber die Montage des Materials auf den Zuschauer "ein" ... Ruttmann gibt dem Zuschauer so lange keine Ruhe, bis er ihn vollends beherrscht, bis er ihn hypnotisiert, dabei ist es keineswegs sein Ziel, die Gegenst&auml;nde, die er dreht, dem Zuschauer vorzustellen, sondern er will &uuml;ber den Montagerhythmus auf ihn einwirken."</i>
</p>
      
<p>Lediglich die marxistischen Filmkritiker waren bei aller Anerkennung der formalen Qualit&auml;ten nicht einverstanden mit dem Film, weil sie nicht akzeptieren konnten, da&szlig; Ruttmann nicht auf die soziale Analyse aus war, sondern auf eine visuelle Symphonie. Der Filmhistoriker Jerzy Toeplitz hat dies Jahre sp&auml;ter in seiner mehrb&auml;ndigen "Geschichte des Films" richtiggestellt:<br>

<i>"Die Verbindung der Aufnahmen wurde nicht von dem Inhalt der Szenen, sondern ausschlie&szlig;lich von der formalen &Auml;hnlichkeit oder vom Kontrast bestimmt. Die lebendige Stadt wurde nach Ruttmanns Konzeption zu einem seltsamen, kompilzierten, ewig funktionierenden Mechanismus. Ruttmann erkl&auml;rt jedoch nicht die Ursachen und den Zweck dieses Funktionierens. Die Konzeption der Neuen Sachlichkeit fand in BERLIN ihre beste filmische Verwirklichung."</i>
</p>
      
<h4>Epilog: MELODIE DER WELT, DEUTSCHE WAFFENSCHMIEDE</h4>
      
<p>Mit der Zwangsl&auml;ufigkeit, die allem Innovativen den Status der Einmaligkeit nimmt, fand auch Ruttmanns filmisches Konzept schnell Nachahmer. &Uuml;ber die Vorbildfunktion von BERLIN schreibt der amerikanische Filmhistoriker John Grierson: <i>"Kein Film hat mehr Einflu&szlig; gehabt, kein Film ist mehr imitiert worden. Er initiierte die modernere Art, das Dokumentarfilmmaterial vor der eigenen T&uuml;r zu finden: Begebenheiten, die sich weder durch den Reiz der Neuheit, noch durch die Romantik edler Wilder in exotischer Kulisse empfehlen. Der Film stellt f&uuml;r den Dokumentarfilm die R&uuml;ckkehr vom Romantischen zum Realistischen dar."</i> <small>(in Films and Filming, August 1961)</small>
</p>
      
<p>Der gro&szlig;e Erfolg seines Berlin-Films brachte Walter Ruttmann auch im Ausland Anerkennung ein. Um die Jahrreswende 1928/29 reiste er nach Moskau, um einen Roman von Jack London zu verfilmen. Das Projekt kam nicht zustande, doch noch im selben Jahr konnte er - zur&uuml;ck in Deutschland - seinen n&auml;chsten Montagefilm drehen: MELODIE DER WELT, ein Werbefilm f&uuml;r Weltreisen mit den Dampfern der Hamburg-Amerika-Linie und zugleich sein wichtigster Tonfilm. Wieder dominieren die symphonische Weltsicht und der Querschnittsgedanke: Stra&szlig;enszenen, Kriegsschaupl&auml;tze, religi&ouml;se Zeremonien, Essensrituale, usw. werden nach analogen bzw. kontrastierenden Gesichtspunkten in Gruppen geordnet und verdichten sich auf diese Weise zu dem Bild einer einzigen Welt.<br>
Die M&ouml;glichkeiten des Tonfilms nutzend, erweitert Ruttmann sein Montageprinzip um Aufnahmen von Stra&szlig;en- und Arbeitsger&auml;uschen, die sich gegenseitig &uuml;berlagern. Schiffssirenen ert&ouml;nen als akustische H&ouml;hepunkte des Films. Sogar die Stille wird gekonnt als neues dramaturgisches Mittel eingesetzt: <i>"Auf Bilder von Soldaten, Panzern und Explosionen folgt ein Frauengesicht, das einen Schrei des Entsetzens ausst&ouml;&szlig;t - dann liegen die Gr&auml;ber in vollkommener Stille."</i> <small>(Jeanpaul Goergen in (7))</small>
</p>
      
<p>Seine Tonexperimente konnte Ruttmann in dem f&uuml;r die Reichsrundfunkgesellschaft produzierten H&ouml;rspiel "Weekend" sogar noch fortsetzen: Alltagsger&auml;usche, Sprachfetzen, Lieder, Kinderverse, usw. charakterisieren in knapp 12 Minuten die Stationen eines typischen Wochenendes (Feierabend, Fahrt ins Freie, Wiederbeginn der Arbeit, ...). Auch hier bleibt Ruttmann der Wirklichkeit verpflichtet: die "Dialoge" wurden von Laien gesprochen und die Ger&auml;usche mit einem fahrbaren Aufnahmenwagen direkt vor Ort aufgenommen, in Fabriken, auf Bahnh&ouml;fen und Hochbahntreppen.</p>
      
<p>Bis in seine sp&auml;teren (Auftrags)Arbeiten sp&uuml;rt Walter Ruttmann, der <i>"dem Dritten Reich wohl seine Mitarbeit, nicht jedoch sein Talent zur Verf&uuml;gung gestellt hat"</i>, den Dingen und der ihnen eigenen Dynamik nach. Hier offenbart sich allerdings das gro&szlig;e Manko in seinem Ansatz, ausschlie&szlig;lich in visuellen Kategorien zu denken: seine Propagandafilme wie METALL DES HIMMELS sind zwar abermals eine <i>"technisch perfekte"</i> ... <i>"Flucht in die Form"</i>, unternehmen jedoch nicht den <i>"Versuch der Sublimation ins Kritische."</i> <small>(Hilmar Hoffmann, "Und die Fahne f&uuml;hrt uns in die Ewigkeit")</small> Im 1940 produzierten DEUTSCHE WAFFENSCHMIEDEN interessiert ihn wieder die Bewegung an sich, diesmal jedoch die Bewegungen von Maschinen, die Granaten und Munition herstellen. Dieses Zugest&auml;ndnis an ein formales Prinzip ist umso erstaunlicher, wenn man bedenkt, da&szlig; Ruttmann, den sein Dienst als Gasschutz-Offizier im Ersten Weltkrieg seelisch schwere belastet hatte, als entschiedener Kriegsgegner aus dem Feld zur&uuml;ckgekehrt war.</p>
      
<p>Die Filme von Walter Ruttmann waren, sofern sie sich ausschlie&szlig;lich f&uuml;r den Rhythmus und den Klang einer Sequenz interessierten, also offen f&uuml;r die unterschiedlichsten Inhalte, sogar f&uuml;r Propaganda.</p>
      
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Filmographie:</h3>
<p>1921 DIE T...NENDE WELLE<br>
1921	OPUS 1<br>
1922	OPUS 2,	 verschiedene abstrakte Werbefilme<br>
1923	Mitarbeit an Lotte Reinigers DIE ABENTEUER DES PRINZEN ACHMED<br>
1924	OPUS 3; "Falkentraum-Sequenz" f&uuml;r Fritz Langs DIE NIBELUNGEN, TEIL 1<br>
1925	OPUS 4<br>
1927	BERLIN - DUE SINFONIE DER GROSSTADT<br>
1929	MELODIE DER WELT<br>
1930	WEEKEND<br>
1931	FEIND IM BLUT<br>
1932	ACCIAIO (ARBEIT MACHT GL&Uuml;CKLICH) <br>
1934	ALTGERMANISCHE BAUERNKULTUR<br>
1935	METALL DES HIMMELS<br>
	KLEINER FILM &Uuml;BER EINE GROSSE STADT,	 D&Uuml;SSELDORF<br>
	STUTTGART, GROSSTADT ZWISCHEN WALD UND	REBEN <br>
1936	SCHIFF IN NOT<br>
1937	MANNESMANN<br>
1938	HAMBURG, WELTSTRASSE, WELTHAFEN<br>
	IM ZEICHEN DES VERTRAUENS<br>
	IM DIENSTE DER MENSCHHEIT<br>
	HENKEL, EIN DEUTSCHES WERK IN SEINER  ARBEIT<br>
1940	DIE DEUTSCHE WAFFENSCHMIEDE<br>
	DEUTSCHE PANZER; ABERGLAUBE<br>
1941	KREBS</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>
      
<p>&nbsp;</p>
<hr>
<h3>Literatur / Quellenangaben</h3>
<ul>
<li>
<small>(1) Arnheim, Rudolf: Kritiken und Aufs&auml;tze zum Film; Frankfurt am Main, 1979</small>
</li>
<li>
<small>(2) Brennicke, Ilona; Hembus, Joe: Klassiker des deutschen  Stummfilms 1910-1930; M&uuml;nchen, 1983</small>
</li>
<li>
<small>(3) Buderer, Hans-J&uuml;rgen; Fath, Manfred: Neue Sachlichkeit - Bilder auf der Suche nach der Wirklichkeit; M&uuml;nchen,  1994</small>
</li>
<li>
<small>(4) Eisner, Lotte H.: Die d&auml;monische Leinwand; Frankfurt am  Main, 1980</small>
</li>
<li>
<small>(5) Faulstisch, Werner; Korte, Helmut (Hrsg.): Fischer Filmgeschichte Band 2: 1925-1944; Frankfurt am Main, 1991</small>
</li>
<li>
<small>(6) Goergen, Jeanpaul: Musik des Lichts - Zum 100. Geburtstag Walter Ruttmanns; in epd Film 12/87</small>
</li>
<li>
<small>(7) Goergen, Jeanpaul: Walter Ruttmann - Eine Dokumentation; Freunde der Deutschen Kinemathek (Hrsg.); Berlin, 1990</small>
</li>
<li>
<small>(8) Korte, Helmut (Hrsg.): Film und Realit&auml;t in der Weimarer Republik; Frankfurt am Main, 1980</small>
</li>
<li>
<small>(9) Kracauer, Siegfried: Von Caligari zu Hitler - Eine psychologische Geschichte des deutschen Films; Frankfurt am Main, 2. Auflage 1993</small>
</li>
<li>
<small>(10) Kranzfelder, Ivo: George Grosz; K&ouml;ln, 1993</small>
</li>
<li>
<small>(11) Metken, G&uuml;nter (Hrsg.): Realismus zwischen Revolution und Reaktion 1919-1939; M&uuml;nchen, 1981</small>
</li>
<li>
<small>(12) Michalski, Sergiusz: Neue Sachlichkeit; K&ouml;ln, 1994</small>
</li>
<li>
<small>(13) Toeplitz, Jerzy: Geschichte des Films, Band 1, 1895-1927, Berlin 1985</small>
</li>
<li>
<small>(14) Toeplitz, Jerzy: Geschichte des Films, Band 2, 1928-1933, Berlin 1985</small>
</li>
<li>
<small>(15) Werner, Paul: Die Skandalchronik des deutschen Films von 1900 bis 1945; Frankfurt am Main, 1990</small>
</li>
</ul>
    
<br>
<hr size="1" noshade="noshade">
<h3 >Inhalt:</h3>
<ul >
<li>
<small><?php echo CHtml::link('Einführung', array('site/page', 'view'=>'docs.montage.intro'));?></small>
</li>
<li>
<small><?php echo CHtml::link('MTV-&Auml;sthetik&lt;: 128 Cuts per Minute - und kein Ende in Sicht', array('site/page', 'view'=>'docs.montage.mtv'));?></small>
</li>
<li>
<small><?php echo CHtml::link('IA28 - Sergej Eisensteins Weg zur Intellektuellen Montage', array('site/page', 'view'=>'docs.montage.eisenst'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Montageaspekte in den Filmen Wong Kar-Weis', array('site/page', 'view'=>'docs.montage.wong'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Die Welt im Querschnitt: Walter Ruttmann und sein Montagefilm BERLIN - DIE SINFONIE DER GROSSTADT', array('site/page', 'view'=>'docs.montage.ruttmann'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Gewaltige Montage-Monumente: Sam Peckinpah', array('site/page', 'view'=>'docs.montage.peckinpah'));?></small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p >
<font size="-2">Impressum</font>
</p>
<p>
<font  size="-2">"Zwischen den Bildern - Aspekte der Filmmontage" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und erscheint begleitend zur Filmreihe im Wintersemester 97/98.
Vervielf&auml;ltigung und Kopieren nur mit vorheriger Genehmigung des AFK</font>
</p>
<p>
<font  size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstr.12<br>
76128 Karlsruhe</font>
</p>
<p>
<font  size="-2">Mitarbeiter an dieser Ausgabe:</font>
</p>
<p>
<font  size="-2">A. G&uuml;nter<br>
M. Hirasaka<br>
M. Nagenborg<br>
H. Stiens<br>
P. Stock</font>
</p>
<p>
<font  size="-2">HTML-Aufbereitung:  Michael Haist</font>
</p>