<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighfilms','docs.mikeleigh.leighhardlabour')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighbleakmoments"></a>
<h3>Bleak Moments</h3>

<p>
Eine Frau, zwei M&auml;nner: miteinander reden, ohne sich anzusehen, miteinander schweigen, Tee trinken.
<i>"Ein programmatischer Titel; Leighs erster Film ist bis heute sein qu&auml;lendster geblieben - mitzuerleben,
wie Personen um Worte ringen und nicht f&auml;hig sind, sich zu &auml;ussern, kann den Zuschauer geradezu
aggressiv machen."</i> <small>(FILMBULLETIN 1.94)</small>

</p>


<h4>Mike Leigh:</h4>
      
<p>

<i>"Mein erster Film und mein letzter 35mm-Spielfilm f&uuml;r die n&auml;chsten 17 Jahre. Es geht um
 meine Einsamkeit, mein Bedauern angesichts verpasster sexueller Chancen, einen Toast auf
 die alternative Subkultur, eine Erinnerung an reaktion&auml;re Lehrer, unh&ouml;fliche chinesische
 Kellner und versteckten Humor. Und der Film beschw&ouml;rt auch diese stillen Nachmittage in
 Vorst&auml;dten herauf, denen ich entronnen bin und zu denen ich mich in diesem Film
 unwiderstehlich hingezogen f&uuml;hle. Achten Sie vor allem auf Barham Manoochehris
 wundersch&ouml;ne Bilder und auf Joolia Cappleman, wenn sie sagt 'I like Chinese food.'
"</i>

</p>


<h4>Zum Film:</h4>


<p>

<i>"BLEAK MOMENTS (der Titel ist ausserordentlich passend) ist nicht auf herk&ouml;mmliche
Weise unterhaltsam. Das soll aber keineswegs heissen, dass er langweilig oder schwer
anzusehen ist; ganz im Gegenteil, es ist unm&ouml;glich, ihn nicht anzusehen. Nach einer
Pressevorf&uuml;hrung meinte einer meiner Kollegen - und wahrscheinlich ist seine Meinung
weitverbreitet -: "Ich klebte f&ouml;rmlich an der Leinwand. Ich konnte einfach den Blick nicht
abwenden. Aber ich k&ouml;nnte ihn mir nie ein zweites Mal ansehen."<br>

Ich glaube, ich hingegen k&ouml;nnte ihn mir immer wieder ansehen, aber ich kann das Gef&uuml;hl
meines Kollegen verstehen. Dieser Film behandelt den Schmerz und die unendliche
Frustration des Lebens auf eine so grundlegende Weise, dass es m&ouml;glicherweise doch zu
schwer verdaulich sein kann."</i> <small>(Roger Ebert in Chicago Sun Times)</small>

</p>


<h4>Preise</h4>

<p>
<ul>
	  
<li>Filmfestival von Chicago: Bester Film</li>
	  
<li>Internationale Filmfestspiele von Locarno: Bester Film</li>

</ul>
</p>

      
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Bleak Moments</h3>
<p>
Gro&szlig;britannien 1971, 35mm Farbe, 110 min., OF
</p>
<p>

<table summary="">
	    
<tr>
<td>Buch und Regie:</td><td>Mike Leigh</td>
</tr>
            
<tr>
<td>Kamera:</td><td>Bahram Manoochehri</td>
</tr>
            
<tr>
<td>Schnitt:</td><td>Leslie Blair</td>
</tr>
	    
<tr>
<td>Art Director:</td><td>Richard Rambant</td>
</tr>
	    
<tr>
<td>Ton:</td><td>Bob Withey</td>
</tr>
	    
<tr>
<td>Musik:</td><td>Mike Bradwell</td>
</tr> 
	  
</table>

</p>
<p>Eine Produktion von AUTUMN PRODUCTIONS, MEMORIAL ENTERPRISES und dem BFI PRODUCTION BOARD</p>
<p>DARSTELLER:<table summary="">
	    
<tr>
<td>Sylvia</td><td>ANNE RAITT</td>
</tr>
	    
<tr>
<td>Hilda</td><td>SARAH STEPHENSON</td>
</tr>
	    
<tr>
<td>Peter</td><td>ERIC ALLAN</td>
</tr>
	    
<tr>
<td>Pat</td><td>JOOLIA CAPPLEMAN</td>
</tr>
	    
<tr>
<td>Norman</td><td>MIKE BRADWELL</td>
</tr>
	    
<tr>
<td>Pats Mutter</td><td>LIZ SMITH</td>
</tr>
	    
<tr>
<td>Normans Freunde</td><td>MALCOLM SMITH</td>
</tr>
	    
<tr>
<td></td><td>DONALD SUMPTER</td>
</tr>
	    
<tr>
<td>Sylvias Boss</td><td>CHRISTOPHER MARTIN</td>
</tr>
	    
<tr>
<td>Krankengymnastin</td><td>LINDA BECKETT</td>
</tr>
	    
<tr>
<td></td><td>SANDRA BOLTON</td>
</tr>
	    
<tr>
<td></td><td>STEPHEN CHURCHETT</td>
</tr>
	    
<tr>
<td>Supervisor</td><td>UNA BRANDON-JONES</td>
</tr>
	    
</table>
</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>


    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>