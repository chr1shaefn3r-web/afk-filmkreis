<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighnaked','docs.mikeleigh.leighcareergirls')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighsecrets"></a>
<h3>Secrets And Lies<br>
<small>Lustspiel vom geteilten Leid</small>
</h3>


<p>
Eine Standardgeschichte, maulten in Cannes
ein paar Siebengscheite bei "salade ni&ccedil;oise", eine seifige soap. 
Fast wie in einem Roman von Dickens
werde in SECRETS AND LIES eine Adoptivwaise auf
der Suche nach der leiblichen Mutter f&uuml;ndig und
gl&uuml;cklich. Dem Zwang zum Happy-End sei da bis
zur Ersch&ouml;pfung nachgelebt. Offenbar gehe jetzt
die Vereinheitlichung der Geschm&auml;cker bereits von
den Autoren aus; das geschehe obendrein auf freiwilliger 
Basis, auch ohne den alten Erfolgsdruck
von Investorenseite.</p>

<p>
Wer es so sieht, konnte sich auf Robert Altman berufen, 
der an der C&ocirc;te d'Azur ausrief, nicht l&auml;nger
die Autoren machten die Filme, sondern die Banken. 
Dustin Hoffman, Al Pacino und Francis Coppola
pflichteten bei. Wie nach Absprache geb&auml;rdeten
sich bew&auml;hrte Systemsupporter als die h&auml;rtesten
Kritiker &uuml;berbordender Gesch&auml;ftemacherei. Und
es ist ja wahr, vor lauter Zahlen sehen die 
Umsatzgewaltigen des Weltkinos keinen einzigen realen
Bilanzwert mehr, und ihr asthmatisches Chaotentum
l&auml;hmt alle Kreativit&auml;t. Allm&auml;hlich wird es
ruchbar, vor lauter Erfolgshetze k&ouml;nnte (mindestens) 
der amerikanische Film in die tiefste gestalterische 
Krise seit den Sechzigern gleiten.</p>


<p>
Der neue Film von Mike Leigh (einem Engl&auml;nder) 
l&auml;sst sich als &laquo;pitching&raquo; leicht in h&ouml;chstens 25
W&ouml;rter fassen. So erz&auml;hlt, setzt er tats&auml;chlich eine
Adoptivwaise (sie ist schwarz) in Szene, die auf
der Suche nach der leiblichen Mutter (sie ist weiss)
f&uuml;ndig und gl&uuml;cklich wird. Die Geschichte ist eine
wirkliche Story - simpel, am&uuml;sant und komisch -
und malt in nichts schwarz. Das Publikum verl&auml;sst
den Saal frohgemut. LIFE IS SWEET, das Leben ist
s&uuml;ss. So hiess 1991 eine Kom&ouml;die Leighs, mit der
sich diese neueste vorteilhaft zusammenbringen l&auml;sst.</p>


<p>
<b>Sich f&uuml;hlen wie Prinzessin Margret</b>
</p>

<p>
Doch wird da kein wohlfeiler, unglaubw&uuml;rdiger Trost 
ausgeteilt. Noch steht zu bef&uuml;rchten, es
ziehe jetzt den Autor unter die Stimmungsdiscounter. 
Man braucht sich bloss zu erinnern, was
f&uuml;r einen scharfen Kontrast NAKED 1993 zum 
vorausgegangenen LIFE IS SWEET bildete, und man
muss ausserdem sehen, wie das gleiche jetzt wieder 
geschieht, im Verh&auml;ltnis zu SECRETS AND LIES.
Die Protagonisten jenes herben derben Zeitst&uuml;cks
von 1993 waren in jeder Hinsicht aus- und abgebrannt. 
Sie f&uuml;hlten sich samt und sonders &laquo;wie Prinzessin Margret&raquo;. 
Es war so sehr mit grimmigem Pessimismus unterlegt, wie das Dasein der
Unterklasse jetzt wieder betont en rose, als fraglos
lebenswert erscheint.</p>

<p>
Schliesslich ist im wirklichen Leben auch niemand 
nur immer zutodebetr&uuml;bt, oder er jauchzt
ohne Unterlass himmelhoch auf. Anlass zur
Selbstaufgabe wie in NAKED findet sich jederzeit,
desgleichen aber Grund zu neuer Hoffnung wie in
SECRETS AND LIES oder LIFE IS SWEET. Sichtlich
blickt Leigh bewusst &uuml;ber den einzelnen Titel hinaus 
und thematisiert von einem Mal zum folgenden, 
wie wir alle schwanken zwischen Zuversicht
und Verzweiflung, Tatkraft und Depression. Seine
Filme erfassen eine Stimmungslage bis ins feinste,
doch unterlassen sie es tunlichst, sie zur jeweils
umfassenden, einzig g&uuml;ltigen Atmosph&auml;re zu erheben.
Genommen wird die Gef&uuml;hlstemperatur nur einer einzelnen
Gruppe von Individuen aufs
Mal. Aber der Wert muss aufs Zehntelsgrad genau
stimmen.</p>

<p>In LIFE IS SWEET und SECRETS AND LIES bieten
Familien Behausung und Behaustheit und schaffen 
damit Raum f&uuml;r die unerl&auml;sslichen Sch&uuml;be von
Optimismus zwischen den unvermeidlichen Anwandlungen 
von Melancholie. Bei einiger Skepsis
bejahen beide Filme, auf die Dauer sei es im
Dunstkreis der N&auml;chsten m&ouml;glich, mehr Probleme
zu l&ouml;sen, als darin erzeugt w&uuml;rden. Mindestens
wird solange, als der konkrete Einzelfall nicht das
Gegenteil beweist, angenommen, eine derartige
Chance sei gegeben. Nichts von all dem kommt in
NAKED auch nur vor. Dort wird der Held gerade in
dem &uuml;bertragenen Sinn als "nackt" hingestellt, als
er allein ist und draussen auf der Strasse.
</p>


<p>
<b>Aufgefangene Abst&uuml;rze</b>
</p>


<p>Nicht, dass alles zum Besten st&uuml;nde im engen Zirkel 
der Angeh&ouml;rigen. Doch ist draussen auf
der Strasse kaum viel Besseres anzutreffen. 
SECRETS AND LIES f&uuml;hrt vor, wie sich Einsamkeit und
Verzweiflung mitunter &uuml;berwinden lassen (freilich
ohne Geew&auml;hr). Cynthia hat reichlich Anlass, sich
&laquo;wie Prinzessin Margret&raquo; zu f&uuml;hlen, wo doch die
Umst&auml;nde sie pl&ouml;tzlich dr&auml;ngen, ihrer lange 
verneinten, verheimlichten, verdr&auml;ngten Tochter 
Hortense sp&auml;t noch eine Mutter zu werden. 
Erschwerend kommt hinzu, dass es den f&auml;lligen Schritt
&uuml;ber jedermanns milde, aber sp&uuml;rbare Rassenvorurteile 
hinweg zu tun gilt; und das heisst, dass
auch die Heldin selbst &uuml;ber ihren Schatten springen muss.</p>


<p>
Dabei w&auml;re es leicht, sich die Schwierigkeit
einfach vom Leih zu halten. Das ist ja schon einmal 
geschehen, als Cynthia ihr Kind zur Adoption
freigab. Nur mit M&uuml;he entscheidet sie sich diesmal
f&uuml;r die kollektive L&ouml;sung und unterbreitet das
Problem ihren Lieben. Die (zun&auml;chst) verheerenden 
Folgen quittiert ihr Bruder Maurice mit dem
Kernsatz: &laquo;We all suffer, why don't we share the suffering&raquo;. 
Ja, dazu sind Familien wohl da, um Abst&uuml;rze aufzufangen. 
Leigh's Figuren halten's, wie's Familien immer tun, 
wenn ihnen etwas gelingt, zum Beispiel die Eingliederung 
der bis dahin v&ouml;llig fremden und obendrein verkehrt gef&auml;rbten 
Hortense.</p>

<p>Sie geben sich der Heiterkeit und der allseitigen 
Sympathie hin, nicht sofort, etwas sp&auml;ter.
SECRETS AND LIES ist ein Lustspiel mit tieferer Bedeutung, 
und ein solches bildet einen graden engen Pfad. Bis zuletzt 
weicht der Film kein Fingerbreit von ihm ab.</p>


<p>Pierre Lachat</p>

    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>
