<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighbleakmoments','docs.mikeleigh.leighnutsmay')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighhardlabour"></a>
<h3>Hard Labour</h3>


<p>
 Eine &auml;ltere Hausfrau aus der Arbeiterklasse, deren Ehemann fortw&auml;hrend rumn&ouml;rgelt und die auch von
 anderen f&uuml;r ihre Plackerei keine Anerkennung oder Zuwendung er&auml;hrt. <i>"Mike Leighs d&uuml;sterster Film
 ohne jeden Moment von Komik (wie sie selbst BLEAK MOMENTS in Momenten wie der Szene im
 chinesischen Restaurant besitzt)"</i> <small>(FILMBULLETIN 1.94)</small>

</p>


<h4>Mike Leigh:</h4>

<p>

<i>"Ich wuchs in North Salford, einem Arbeiterviertel &uuml;ber der Arztpraxis meines Vaters auf.
 Als ich dreizehn war, zogen wir um die Ecke in ein Vorot-Reihenhaus. "Hard Labour" wurde
 genau dort, in dieser Gegend von Salford gedreht und beschw&ouml;rt diese beiden
 gesellschaftlichen Welten herauf, die in meinen Filmen immer wieder aufeinandeerstossen.
 Der Film ist zwar nicht ausgeprochen biographisch, doch es tauchen einige biographische_
 Charaktere auf, insbesondere Mrs. Thornley, deren Vorbild eine "wirkliche" Putzfrau war.<br>
 Besonderes Augenmerk verdienen Clifford Kershaw und Alison Steadman bei ihrem
 denkw&uuml;rdigen ersten Auftritt."</i>

</p>



<h4>Zum Film:</h4>


<p>

<i>"Einer der &uuml;ber HARD LABOUR abgegebenen Kommentare, gegen die ich nichts einzuwenden
 habe, ist, dass er von allen Filmen, die ich gemacht habe, derjenige ist, in den die allermeiste
 Arbeit investiert wurde, und bei dem es jemanden gab - das hei&szlig;t Tony Garnett -, der keine
 M&uuml;he scheute, um uns die M&ouml;glichkeit zu geben, den Film genau richtig zu machen. Acht
 Wochen Proben, f&uuml;nf Wochen Drehzeit, die gr&ouml;&szlig;te Besetzung, die ich je hatte - insgesamt 26
 Rollen -; und wir konnten alles machen, was wir wollten. Schwachstellen hat der Film nur
 da, wo meine geringe Erfahrung nicht mit unseren ehrgeizigen Pl&auml;nen Schritt halten konnte.<br>
 Wahrscheinlich konnte der Film genau deswegen inhaltlich so gr&uuml;ndlich und unnachgiebig
 sein, weil es bei den Arbeitsbedinungen keine Kompromisse gab."</i> <small>(Auszug aus einem Interview mit Mike Leigh)</small>

</p>

      
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Hard Labour</h3>
<p>GB 1972, 16 mm, Farbe, 75 min.<br>Eine Produktion von BBC TELEVISION</p>
<p>

<table summary="">
	    
<tr>
<td>Buch und Regie:</td><td>Mike Leigh</td>
</tr>
            
<tr>
<td>Kamera:</td><td>Clifford Kershaw</td>
</tr>
            
<tr>
<td>Schnitt:</td><td>Christopher Rowlands</td>
</tr>
	    
<tr>
<td>Art Director:</td><td>Paul Munting</td>
</tr>
	    
<tr>
<td>Kost&uuml;me:</td><td>Sally Nieper</td>
</tr>
	    
<tr>
<td>Ton:</td><td>Dick Manton</td>
</tr>
	    
<tr>
<td>Produktions-Team:</td><td>Roger Bamford</td>
</tr> 
	    
<tr>
<td></td><td>Roy Baker</td>
</tr> 
	    
<tr>
<td></td><td>Spike Hughes</td>
</tr> 
	    
<tr>
<td></td><td>Irene East</td>
</tr> 
	    
<tr>
<td>Produzent:</td><td>Tony Garnett</td>
</tr> 
	  
</table>

</p>
<p>DARSTELLER:
<table summary="">
	    
<tr>
<td>Mrs. Thornley</td><td>Liz Smith</td>
</tr>
	    
<tr>
<td>Jim Thornley</td><td>Clifford Kershaw</td>
</tr>
	    
<tr>
<td>Ann</td><td>Polly Hemingway</td>
</tr>
	    
<tr>
<td>Edward</td><td>Bernard Hill</td>
</tr>
	    
<tr>
<td>Veronica</td><td>Alison Steadman</td>
</tr>
	    
<tr>
<td>Mrs. Stone</td><td>Vanessa Harris</td>
</tr>
	    
<tr>
<td>Mr. Stone</td><td>Cyril Varley</td>
</tr>
	    
<tr>
<td>Julie</td><td>Linda Beckett</td>
</tr>
	    
<tr>
<td>Naseem</td><td>Ben Kingsley</td>
</tr>
	    
<tr>
<td>Barry</td><td>Alan Erasmus</td>
</tr>
	    
<tr>
<td>June</td><td>Rowena Parr</td>
</tr>
	    
<tr>
<td>Mrs. Rigby</td><td>June Whitaker</td>
</tr>
	    
<tr>
<td>Mrs. Thornleys Freundin</td><td>Paula Tilbrook</td>
</tr>
	    
<tr>
<td>Mr. Shore</td><td>Keith Washington</td>
</tr>
	    
<tr>
<td>Kontrolleur</td><td>Lois Raynes</td>
</tr>
	  
</table>

</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>


    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>