<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.intro','docs.mikeleigh.leighbleakmoments')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighfilms"></a>
<h3>Mike Leigh und seine Filme</h3>

<h4>Das Allt&auml;gliche und Extreme</h4>


<p>
<i>"Meine Zielsetzung ist es, das Gew&ouml;hnliche au&szlig;ergew&ouml;hnlich zu machen, den Zuschauer mit
Figuren zu konfrontieren, die er extrem findet, denn Menschen sind extrem. Letztendlich ist jeder
ein Individuum."</i> So &auml;u&szlig;erte sich Leigh zu NAKED, seinem mit Sicherheit extremsten und 
verst&ouml;rendsten Film - der ihn allerdings auch bekannt gemacht hat (Goldene Palme in Cannes 1993). 
Johnny, die Hauptfigur, ist ein nihilistischer Gassenphilosoph, respektlos und menschenverachtend, 
brutal und exzessiv. Ein Mensch, der nicht in der Lage ist, menschliche W&auml;rme
anzunehmen oder weiterzugeben, der sich das, was er braucht oder zu brauchen glaubt, mit
Gewalt nimmt. Eine haltlose, ortslose Figur - und vor allem einsam.</p>


<p>
Und hier ist auch die Schnittstelle zu Leighs
restlichen Filmen, die so viel harmloser, 
allt&auml;glicher erscheinen.  Leigh befindet sich
st&auml;ndig auf der Suche nach dem, was sich
zwischen seinen Figuren abspielt, was sie
verbindet oder trennt.  Ihre Beziehungen
zueinander, ihre F&auml;higheit bzw. Unf&auml;higkeit
zur Kommunikation, ihre soziale Einbindung
oder Ausgrenzung,  ihn  interessiert  die
Normalit&auml;t, die kann mitunter auch extrem
sein. Dazu braucht Leigh nicht unbedingt
Figuren wie Johnny. Meistens sind seine
Charaktere eher "Durchschnittsmenschen",
die zur Arbeiterklasse geh&ouml;ren, Leigh zeich-
net sie mit Vorliebe in ihrer allt&auml;glichen
Umgebung - Familie, Arbeit, Nachbarschaft,
soziales  Umfeld, was Frank Arnold dazu
bewegt hat, seine Filme als "Ethnologie der
britischen Klassengesellschaft" zu bezeichnen.
</p>


<p>
Die Begeisterung f&uuml;r das Kino begann bei Leigh sehr fr&uuml;h: <i>"Ich w&uuml;rde sagen, es gibt keinen Film
zwischen 1949 und 1960, den ich nicht gesehen habe."</i> - <i>"Mir ist nichts entgangen, und ich fand
alles gro&szlig;artig. Aber trotzdem ging mir durch den Kopf, da&szlig; es doch wunderbar sein m&uuml;sste,
einen Film zu sehen, in dem die Leute sich so benehmen, wie die Menschen auf der Stra&szlig;e und
nicht wie Kinofiguren."</i>

</p>


<p>
Da es Leigh weniger um die Geschichte geht, sondern um das Verhalten und Verh&auml;ltnis seiner
Charaktere zueinander, sind die Handlungen seiner Filme stark reduziert. Es gibt selten viel
Aktion oder extreme Wendungen. Es geht vor allem darum, die Figuren zusammenzubringen.
</p>


<p>
Alfred Hitchcock hat sich dazu einmal folgenderma&szlig;en ge&auml;u&szlig;ert: <i>"Welche Frau, die den ganzen
Tag am Sp&uuml;lstein gestanden und den Abwasch gemacht hat, will f&uuml;nf Dollar an der Kinokasse
bezahlen, nur um auf der Leinwand eine Frau zu sehen, die am Sp&uuml;lstein steht und den
Abwasch macht?"</i> Mike Leigh dazu: <i>"Was Hirchcock sagt, ist mir wurscht. Ich m&ouml;chte wetten,
da&szlig; Hitchcock in seinem Leben nicht ein einziges Mal in der K&uuml;che gestanden und Geschirr
gesp&uuml;lt hat! Was redet er also? BLEAK MOMENTS startete in London in derselben Woche wie
Hitchcocks FRENZY. Einer der gr&ouml;&szlig;ren Gl&uuml;cksmomente in meinem Leben war der, als ich im
"Observer" eine Gegen&uuml;berstellung der beiden Filme las. Darin stand sinngem&auml;&szlig;: "Auf der einen
Seite haben wir Hitchcock, der daf&uuml;r ber&uuml;hmt ist, seine Schauspieler zu verachten, und auf der
andere Seite ist da Mike Leigh, der seinen Akteuren offenkundig gr&ouml;&szlig;ten Respekt entgegenbringt."
Das gedruckt zu sehen, hat mir sehr gefallen."</i>

</p>


<p>
In der Tat hat Mike Leigh eine ganz besondere Beziehung zu seinen Schauspielern.
</p>


<h4>Devised and Directed by Mike Leigh</h4>


<p>

<i>"Devised and Directed by Mike Leigh"</i> - mit diesem Satz beginnen bzw. enden viele seiner Filme.
er steht vor allem f&uuml;r eine etwas andere Auffassung vom Prozess des Filmemachen, und - damit
verbunden - f&uuml;r seinen unverkennbaren pers&ouml;nlichen Stil.
</p>


<p>
Das betrifft vor allem die Beziehung zu seinen Schauspielern: Er gibt ihnen viel Raum in der
Interpretation ihrer Rollen und beteiligt sie am kompletten Entstehungsprozess des Films.
Entsprechend sorgsam triftt er seine Auswahl. Darsteller wie Ben Kingsley, Gary Oldman, Tim
Roth usw. haben in seinen Filmen ihre Karriere begonnen, andere wie Timothy Spall, Alison
Steadman, Philip Davis, David Thewlis, Claire Skinner, Katrin Cartlidge, Brenda Blethyn usw.
tauchen immer wieder in den unterschiedlichsten Rollen auf.
</p>


<p>
Mike Leigh "beginnt" einen Film nicht mit einem fertigen Script und ausgearbeiteten Figuren.
Ausgangspunkt ist Idee, eine Vorstellung von Figuren oder ein abstrakter Handlungsgang.
Bei SECRETS AND LIES waren es die adoptionsbedingten Erfahrungen eines befreundeten
Ehepaares, der Wunsch, sich mit einer Generation von Schwarzen zu besch&auml;ftigen, die erwachsen 
wird und aufbricht; au&szlig;erdem hatte er &uuml;ber schwarze Kinder recherchiert, die von wei&szlig;en
Frauen in den 50er und 60er Jahren geboren worden waren. <i>"Aber kannte ich die Geschichte?
Nein. Das sind die Dinge, die man beim Machen eines Films entdeckt."</i>

</p>


<p>
Bereits zu diesem Zeitpunkt w&auml;hlt er die Schauspieler aus. (Ihre Anzahl bestimmt sich nicht 
selten durch die Gr&ouml;&szlig;e des Budgets). Er hat ungef&auml;hre Vorstellungen von ihren Rollen innerhalb
des Films - einen Entwurf. Pr&auml;zisiert wird die Figur aber erst gemeinsam mit dem Darsteller.
<i>"... Ja, wie sympathisch ein Charakter wird, entwickelt sich w&auml;hrend der Proben. Nein, ich weiss
vorher schon, wie etwas aussehen soll. Der K&uuml;nstler erschafft das Material; alles Material kommt
zu einer Existenz als Resultat der Kombination von Improvisation und Ordnung."</i> Konkret 
funktioniert das so: <i>"Ich arbeite sehr eng mit jedem einzelnen Schauspieler zusammen, um eine
Figur zu erschaffen. St&uuml;ck f&uuml;r St&uuml;ck entwickeln wir die ganze Geschichte dieser Figur, ihre
ganze Welt mit all den Beziehungen. Auch die Zeit ist sehr wichtig, die chronologische Zeit des
Lebens einer Figur, die Jahre, die sie bereits gelebt hat. Dabei geht es nicht nur um
Improvisation, sondern auch um Recherche. Aber das Wichtigste ist dabei nicht. was der
Schauspieler individuell macht, sondern was die Darsteller zusammen in den Beziehungen
machen."</i>

</p>


<p>
Sind die Hintergr&uuml;nde entwickelt, l&auml;sst Leigh seine Darsteller erstmals aufeinandertreffen. Dann
beginnt der eigentliche "workshop process". Gemeinsam werden Szenarien entworfen, die 
verschiedenen Figuren begegnen einander - in Improvisationen werden M&ouml;glichkeiten ihrer 
gegenseitigen Beziehungen untersucht, bis sich daraus konkrete Strukturen und ein Handlungsablauf
ergeben.
</p>


<p>
Katrin Cartlidge (NAKED, CAREER GIRLS) hat diesen Entwicklungsprozess in einem Interview
folgenderma&szlig;en beschrieben: <i>"Man arbeitet f&uuml;r Monate - wie ich es bezeichnen w&uuml;rde - in einer
Art von organischem Chaos. Jede einzelne Situation wird geformt und pr&auml;zisiert - das beginnt
mit Improvisationen, wird dann zu etwas Wiederholbarem und wird dann festgehalten, fixiert...
Mike startet im Chaos und kommt an bei einer strukturierten Form."</i>

</p>


<p>
Diese "strukturierte Form", die Essenz aus den gemeinsamen Vorarbeiten, verarbeitet Leigh
dann zu dem endg&uuml;ltigen Script. <i>"Dieser ganze Vorbereitungsprozess - &uuml;ber den ich nur sehr
z&ouml;gernd spreche, wie Sie gemerkt haben: weil das privat ist; wichtig ist der Film, der am Ende 
dabei herauskommt - dient nur dazu, uns darauf vorzubereiten, den Film w&auml;hrend der 
Dreharbeiten zu erschaffen, St&uuml;ck f&uuml;r St&uuml;ck. Ohne diese Vorbereitung k&ouml;nnte ich nie mit den 
Schauspielern on location gehen."</i> -  Leigh dreht zu  einem Zeitpunkt,  an  dem  der 
Entwicklungsprozess zu einem Ende gekommen ist, diesen Punkt will er festhalten. Improvisiert
wird bei den Aufnamen daher nicht mehr.
</p>


<p>
Diese komplette Vorlaufphase dauerte bei NAKED vier und bei SECRETS AND LIES sogar f&uuml;nf
Monate. (Zum Vergleich: die Dreharbeiten zu NAKED waren nach 14 Wochen abgeschlossen.)
</p>


<p>
Diese Form von Vorbereitung verlangt den Darstellern einiges ab. Bei SECRETS AND LIES z.B. 
hat Leigh Brenda Blethyn, "Cynthia", nicht dr&uuml;ber informiert, dass ihre zur Adoption 
freigegebene Filmtochter schwarz sein w&uuml;rde:
</p>


<p>
<i>"
lch hatte Marianne Jean-Baptistes Name auf der Darstellerliste gesehen, aber ich hatte sie
davor nie getroffen. Ich ging zu der Station an diesem Tag, sah Marianne und wusste, sie war
nicht die Richtige. Als sie mich fragte, "Sind Sie Cynthia?", dachte ich wirklich, sie geh&ouml;re zur
Crew! Also wenn Cynthia sagt, es handele sich um einen Fehler, war das meine ehrliche
Reaktion. Ich habe nicht gespielt."</i>

</p>


<p>
Nicht anders erging es &uuml;brigens den restlichen Darstellern bei der "ersten gemeinsamen"
Familienfeier...
</p>



<p>
W&auml;hrend Leigh Umfeld und Interieurs in seinen Filmen mit akribischer Detailtreue inszeniert,
ob das nun die Bl&uuml;mchentapete im Wohnzimmer oder das immer wiederkehrende Tee-Ritual
(mit Keksen) ist, legt er das Schauspiel aus auf &Uuml;berzeichnung und Expressivit&auml;t. Regelrecht
schonungslos zeigt er seine Figuren mit ihren Eigenheiten, Verklemmungen und Komplexen -
jedoch nie ohne Sympathie.
</p>


<p>
Dies hat ihm schon oft den Vorwurf eingehandelt, blosse Karikaturen zu entwerfen, bzw. von
Seiten der britischen Linken, sich &uuml;ber die englische Arbeiterklasse lustig zu machen. Leigh
dazu:
</p>


<p>
<i>"
Wenn  Sie  unter  einem  Karikaturisten
George Grosz oder Otto Dix verstehen, ist
das  wunderbar,  dann  freue  ich  mich,
Karikaturist genannt zu werden, jemand der
die grotesken Charakterz&uuml;ge des Menschen
hervorhebt. Aber ich sehe mich selbst nicht
als Karikaturisten. Ich arbeite mit den Mitteln
der &Uuml;berh&ouml;hung, um an die Essenz der
Dinge zu gelangen."</i>

</p>


<p>
Es geht ihm  nicht um eine realistische,
unmittelbare Abbildung von  Realit&auml;t.  Die
Genauigkeit im Einfangen der allt&auml;glichen
Rituale erweist sich als Ergebnis pr&auml;ziser
Stilisierung. Genauso die Zeichnung der
Figuren: nichts bleibt dem Zuschauer verborgen, 
jeder Spleen, jede Eigenheit tritt
&uuml;berdeutlich zum Vorschein. Leighs Filme
erhalten ihre Spannung gerade aus dieser
"Verdichtung von Realit&auml;t". Dann, wenn man
glaubt, diese "Typen" eingeordnet zu haben,
bricht Leigh das vorgefertigte Bild, l&auml;sst ihre
Motivationen  und  Verletzlichkeiten  zum
Vorschein hommen, <i>"und dadurch verm&ouml;gen
die Figuren den Zuschauer in einer Weise zu
ber&uuml;hren, wie man sie aus anderen Filmen
nicht gewohnt ist."</i>

</p>


<p>
Weniger verst&auml;ndnisvoll ist Leigh dagegen
in seiner Schilderung der "upper class" -
ihnen verzeiht er so gut wie nie.
</p>


<p>

<i>"Ich treffe in meinen Filmen keine moralischen Urteile, ich ziehe keine Schl&uuml;sse. Ich stelle
Fragen, ich beunruhige den Zuschauer, ich mache ihm ein schlechtes Gewissen, lege Bomben,
aber ich liefere keine Antworten. Ich weigere mich, Antworten zu geben, denn ich kenne die
Antworten nicht."</i> <small>(Mike Leigh in einem Interview zu NAKED)</small>

</p>


<h4>Biographisches</h4>



<p>
Leigh wird immer wieder nach autobiographischen Aspekten seiner Filmen befragt. Man sucht
nach den Motiven, wieso er sich so gerne und intensiv mit der britischen Arbeiterklasse 
besch&auml;ftigt. Eigentlich ist seine Herkunft eher b&uuml;rgerlich, trotzdem ist ihm die 
Gesellschaftsschicht, die er mit Vorliebe zeigt, nicht fremd. Er wurde 1943 in Lancashire geboren.
</p>


<p>

<i>"Mein Vater war Arzt in einem Arbeiterviertel von Manchester, meine Mutter Krankenschwester.
Ich hatte mein Zimmer &uuml;ber der Praxis meines Vaters. Ich wuchs also als Arztsohn im 
proletarischen Milieu auf und besuchte die dortige Schule. Von daher gibt es sicher eine Art Spannung
  in meiner Biographie, einen Einfluss von zwei
  Seiten, zwei verschiedenen gesellschaftlichen
  Schichten.  Ich  geh&ouml;re  zu  der
  Generation, die w&auml;hrend des Krieges geboren
  wurde und die f&uuml;nfziger Jahre als aus-
  gesprochen langweilig empfand."</i>

</p>


<p>
  Die Flucht gelang ihm zun&auml;chst in exzessiven
  Kinobesuchen. Wie schon eingangs
  erw&auml;hnt, gibt es keinen Film zwischen 1949
  und 1960, den er nicht gesehen hat. 1960
  erhielt er ein Stipendium f&uuml;r die  Royal
  Academy for Dramatic Arts (RADA), das war
  sozusagen sein Aufbruch in die Gro&szlig;stadt.
  Dort studierte er Schauspiel und f&uuml;hrte
  w&auml;hrend seines zweij&auml;hrigen Aufenthaltes
  Regie bei einer Studentenproduktion von
  Harold  Pinters  HAUSMEISTER.  Seine
  Ausbildung setzte er fort an der Camberwell
  Art   School,   der   Abteilung   f&uuml;r
  B&uuml;hnenentw&uuml;rfe der Central School of Arts
  and Crafts und an der London Film School.
  1967 arbeitet er als Regieassistent bei der
  Royal Shakespeare Company. Bevor er mit
  dem Filmen begann, schrieb und arbeitete er
  f&uuml;rs Theater. 1965 wurde in Birmingham sein
  erstes St&uuml;ck aufgef&uuml;hrt.
</p>


<p>
  Sein erster Spielfilm, BLEAK MOMENTS
  (1971) basierte auf seinem gleichnamigen
  B&uuml;hnenst&uuml;ck. Der Film war frei finanziert,_
  konnte noch auf 35mm gedreht werden. Auf
  den  Filmfestspielen  in  Chicago sowie
  Locarno erzielt er die Hauptpreise. Danach
  folgt die Arbeit f&uuml;r das BBC.
</p>


<p>
  Er   verfilmt   zwei   weitere   seiner
  Theaterst&uuml;cke, NUTS IN MAY (1975), wo ein
  Teil der Figuren aufgegriffen wurde, allerdings 
  mit einer v&ouml;llig neue Rahmenhandlung,
  und ABIGAIL'S PARTY (1977), das sich sehr
  eng an die B&uuml;hneninszenierung h&auml;lt - eine
  Arbeit, von der er sich jetzt eher distanziert,
  da die f&uuml;r ihn so wichtige Adaption an das
  andere Medium (Film) nicht stattgefunden
  hat.
</p>


<p>
  1983 entsteht MEANTIME mitproduziert von
  Channel Four - damals leider noch auf 16mm, 
  der Film l&auml;uft im Internationalen Forum
  der Berlinale.
</p>


<p>
  In den 80er Jahren arbeitet Leigh kaum noch
  f&uuml;rs Theater. <i>"Ich mache diese Arbeit nur
  sehr  z&ouml;gernd,  es  ist  nicht  meine
  Leidenschaft, meine Liebe. Ich habe keinen
  gro&szlig;en Enthusiasmus daf&uuml;r. Die Arbeit selber
  ist o.k., aber man muss derartig viel an
  Vorbereitung investieren, und dann l&ouml;st es
  sich einfach in Luft auf."</i>

</p>


<p>Mit HIGH HOPES (1988),  wieder eine
 Koproduktion von Channel Four, kann er
 nach  17  Jahren  wieder  einen  35mm
 Spielfilm drehen. Er wird in Venedig mit dem
 Preis der Internationalen Filmkritik 
 ausgezeichnet und erh&auml;lt bei der Verleihung des
 Europ&auml;ischen  Filmpreises  "Felix"  drei
 Troph&auml;en. Endlich wird auch das 
 internationale Publikum auf Leigh aufmerksam.
 1990 folgt LIFE IS SWEET, ebenfalls mehrfach
 ausgezeichnet.  Der  "Durchbruch" kommt
 dann mit NAKED (1993): Leigh erh&auml;lt
 den Preis f&uuml;r die Beste Regie in Cannes,
  sein Haupdarsteller David Thewlis wird als
  Bester Schauspieler ausgezeichnet.
  Sein n&auml;chster Film SECRETS AND LIES
  (1995) gewinnt in Cannes sogar die Goldene
  Palme als bester Film, Brenda Blethyn wird
  Beste Schauspielerin.  Und ... SECRETS
  AND LIES wird ein internationaler Erfolg an
  den Kinokassen.<br>
  1997 entsteht CAREER GIRLS.
</p>


<p>
  Seit  1965  hat  Leigh  22  B&uuml;hnenst&uuml;cke
  geschrieben und inszeniert. 1989 gr&uuml;ndete
  er zusammen mit dem Produzenten Simon
  Channing       Williams       die
  Produktionsgesellschaft "Thin Man Films",
  die sich ausschlie&szlig;lich um seine eigenen
  Filme k&uuml;mmert.
</p>


<p>
 Leighs Filme insgesamt einzuordnen, Phasen zu unterscheiden, w&uuml;rde ihm selbst widerstreben.
 Gerne wird von einer zunehmenden Politisierung in seiner Arbeit gesprochen, die in NAKED
 ihren H&ouml;hepunkt gefunden h&auml;tte. Aber letztlich kreist Leigh immer wieder um pers&ouml;nliche,
 zwischenmenschliche Themen, und die sind zu universell, um sie auf eine "Ethnologie der britischen
 Arbeiterklasse" zu beschr&auml;nken.
</p>


<p>
  Leigh selbst unterscheidet seine Filme nur in die Zeit Vor, W&auml;hrend und Nach Thatcher.
  Vielleicht ist das ein Grund, dass er mit SECRETS AND LIES und CAREER GIRLS einen
  w&auml;rmeren und vers&ouml;nlicheren Ton angeschlagen hat - mit politisch oder unpolitisch hat das jedoch
  nichts zu tun.
</p>


<p>
  Oder anders ausgedr&uuml;ckt: Zwar hat er 1960 dem "Kleinstadtmief" endlich den R&uuml;cken gekehrt,
  thematisch hat er seine "Heimat" jedoch nie richtig verlassen. <i>"Auf die eine oder andere Art
  handeln meine Filme von Wurzeln und von Familie"</i>, sagt Leigh. <i>"Einer meiner Cousins sah sich
  SECRETS AND LIES an. Und sein Kommentar: 'Oh, there we are all again.'"</i>

</p>

      
<p>&nbsp;</p>
<hr>
<h3>Literatur / Quellenangaben</h3>
<p>
<small>Zitate aus:</small>
</p>
<ul>
<li>
<small>epd Film 2/94: Robert Fischer; "Ein filmisches Universum aus Wut und Armut"</small>
</li>
<li>
<small>FILMBULETTIN 1.94: "l wouldn't be joking if I wasn't serious" - eine Ann&auml;hrung an Mike Leigh und seine Filme</small>
</li>
<li>
<small>TIME Magazine 30.09.96: Richard Corliss: "Family Values"</small>
</li>
<li>
<small>DIE ZEIT 29.09.96: Willi Winkler: "Drastisch ausgeleuchtet - Ein Portr&auml;t des Regisseurs Mike Leigh"</small>
</li>
<li>
<small>Festivalkatalog der Hofer Filmtage, 1993</small>
</li>
</ul>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>