<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighwhoswho','docs.mikeleigh.leighhome')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighgrownups"></a>
<h3>Grown-Ups</h3>

<p>
<i>"Ein junges Paar bezieht eine neue Wohnung (und stellt fest, dass im Haus nebenan ihr ehemaliger
Religionslehrer wohnt), deren h&auml;uslicher Friede alsbald durch die Schwester der Frau (die noch bei ihrer
Mutter lebt) gest&ouml;rt wird, welche fortw&auml;hrend vorbeikommt, sich einmischt und ihre Aufenthalte immer
mehr ausdehnt, bis die Katastrophe unvermeidlich wird. So gradlinig wie (tragi-)komisch.
"</i> <small>(Filmbulletin 1.94)</small>
</p>


<h4>Mike Leigh:</h4>

<p>
<i>"Wieder Chaos in der Vorstadt. Der Film entstand ohne lange Vorbereitung, um eine L&uuml;cke
bei der BBC zu schlie&szlig;en; deswegen gibt's nur sechs Schauspieler und einen Drehort. Das
f&uuml;hrt zu einer positiven Abkehr vom eher nachl&auml;ssigen Aufbau meiner Filme der siebziger
Jahre zu der dichten, straffen Dramatik, die mir bis dato nur im Theater gelungen war.
<br>
Wunderbares Zusammenspiel des Ensembles! Und achten Sie auf Remi Adefarasins
&uuml;berw&auml;ltigendes Deb&uuml;t mit der Kamera."</i>
</p>


<h4>Zum Film:</h4>

<p>
Der Film ist derartig wirklichkeitsgetreu, da&szlig; es schmerzlich und peinlich ist, zu
beobachten, wie Mike Leigh den Snobismus, der so oft implizit verstanden, aber nie offen
angesprochen wird, vorf&uuml;hrt und zum Ausdruck bingt. Indem Leigh diese verbotenen
Gedanken aufzeigt, hat er einen Film geschaffen, den man nicht ansehen kann, ohne dass
einem die sozialen Umst&auml;nde peinlich w&auml;ren. Er bewirkt aber, dass man sich schuldig f&uuml;hlt,
wenn man &uuml;ber diejenigen lacht, die sich nicht anders verhalten k&ouml;nnen. Oder genauer
gesagt, sie bringen einen zum Lachen, denn Mike Leigh versteht sich nicht als Regisseur im
herk&ouml;mmlichen Sinn. Zwar entwirft er die Handlung, l&auml;sst seinen Schauspielern aber bei der
Interpretation sehr viel Freiraum. Seine Filme sind das Ergebnis stundenlanger Proben, die
auf Improvisation aufbauen. Es ist eine einzigartige Teamarbeit, die zu einer Darstellung
f&uuml;hrt, die der Realit&auml;t beunruhigend nahekommt.
</p>

<p>Nicholas Wapshoott in The Scotsman</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>