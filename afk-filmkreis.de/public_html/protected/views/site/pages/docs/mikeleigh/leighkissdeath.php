<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighnutsmay','docs.mikeleigh.leighwhoswho')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighkissdeath"></a>
<h3>The Kiss Of Death</h3>
      
<p>
<i>"Vier junge Leute, zwei P&auml;rchen: Trevor, der tags&uuml;ber als Assistent eines Beerdigungsunternehmers
arbeitet, l&auml;&szlig;t sich von Linda "einfangen", auch wenn es dann Schwierigkeiten mit dem ersten Ku&szlig; und
Eifers&uuml;chteleien gibt. Liebe und Tod und zum ersten Mal die siebziger Jahre in voller Bl&uuml;te
(Plateausohlen!)."</i> <small>(Filmbulletin 1.94)</small>

</p>


<h4>Mike Leigh:</h4>

<p>
<i>"Wieder oben im Norden - diesmal in Oldham. F&uuml;r diesen Film habe ich eine besondere
Schw&auml;che. Jugend, Wahrheit, L&uuml;ge, Liebe, Leben und Tod. Trevor, gl&auml;nzend gespielt von
David Threlfall, besitzt gewisse Z&uuml;ge von mir selbst. Achtung bei der Szene mit dem toten
Baby. Sie ist nicht als Affront gedacht, sondern soll nur den Teil der Arbeit zeigen, der
Leichenbestattern an die Nieren geht. Besondere Aufmerksamkeit verdienen Carl Davis
fr&ouml;hliche Musik (es ist mein erster Film mit Musik) und die k&ouml;stliche Angela Curran, die wie
Betty Boop aussieht."</i>
</p>


<h4>Zum Film:</h4>

<p>Viele Leute schrieben mir, THE KISS OF DEATH sei ihnen wegen der Szene mit dem Tod
des Babys sehr nahe gegangen. In dieser Hinsicht war der Film kontrovers. Und einige
beschwerten sich auch, weil sie nicht glauben konnten, da&szlig; Leichenbestatter wirklich Witze
rei&szlig;en, w&auml;hrend sie die Leichen herrichten; aber nat&uuml;rlich habe ich die Szene eben deswegen
in den Film aufgenommen. Als ich bei meinen Recherchen zum erstenmal Bestatter
aufsuchte, machten die die entsetzlichsten Sachen, um mich zu schockieren. Die Leiche eines
alten Mannes und einer alten Frau... den Rest &uuml;berlasse ich Ihrer Phantasie.</p>


<p>Auszug aus einem Interview mit Mike Leigh</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>
