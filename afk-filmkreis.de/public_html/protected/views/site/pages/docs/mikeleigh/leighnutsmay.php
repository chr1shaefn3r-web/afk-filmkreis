<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighhardlabour','docs.mikeleigh.leighkissdeath')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighnutsmay"></a>
<h3>Nuts in May</h3>
      
<p>

<i>"Ein junges &Ouml;ko-Ehepaar will sich beim Campingurlaub erholen, sieht sich jedoch in seinem Bed&uuml;rfnis
nach Ruhe in der unber&uuml;hrten Natur von anderen Menschen gest&ouml;rt, was besonders den Sozialarbeiter-
Ehemann, einen rechthaberischen Prinzipienreiter ersten Ranges, zur Wei&szlig;glut reizt. Ausschlie&szlig;lich
drau&szlig;en gedreht, erweist sich in NUTS IN MAY die Natur blo&szlig; als Projektionsfl&auml;che der St&auml;dter, wobei
die erstmalige Kombination von tragischen und komischen Momenten hier stark zu letzteren tendiert."</i> <small>(FILMBULLETIN 1.94)</small>

</p>



<h4>Mike Leigh:</h4>

<p>
<i>"Der Produzent David Rose forderte mich auf, einen l&auml;ndlichen Film in seiner Heimat Dorset
zu machen, aber irgendwie befinden wir uns hier doch wieder mitten im Kampf der lebhaften
Wohnviertel gegen die selbstgerechten Vororte, auch wenn Eric Allans Steinbrecher die
ausgewogene Einstellung der Bewohner von Purbeck Island gegen&uuml;ber den St&auml;dtern gut
einf&auml;ngt. Besonders bemerkenswert sind Richard Ireson als schlagfertiger Polizist und Oliver
Whites wohl&uuml;berlegter Schnitt."</i>

</p>


<h4>Zum Film:</h4>


<p>
Die Gestalten sind alle verbunden durch die Vorstellung, sie st&uuml;nden miteinander in
Kontakt, aber im Grunde kommunizieren sie &uuml;berhaupt nicht. Sie gehen v&ouml;llig auf in ihrer
Selbstgerechtigkeit und der scheinheiligen &Uuml;berzeugung, sie w&uuml;rden alles verstehen und mit
den anderen Kontakt aufnehmen...<br>
Wahrscheinlich ist NUTS IN MAY deswegen so ansprechend, weil es der einzige Film ist, der
drau&szlig;en spielt und in dem man die Landschaft sieht. Er entstand, weil David Rose zu mir
sagte: "Wenn du einen Film machen willst, fahr nach Dorset und schau dir die Gegend an. Ich
komme von dort..." Also fuhr ich dort hin, aber interessanterweise handelt der Film gar nicht
von Dorset. Was mich faszinierte war, da&szlig; man diese phantastische Landschaft sieht, aber
nur im Kontext mit diesen beschr&auml;nkten, scheinheiligen, engstirnigen St&auml;dtern. Die
Hauptgestalten sind alle St&auml;dter, und wenn sie mit dem "Landvolk" in Kontakt kommen,
machen die Einheimischen sie nur l&auml;cherlich.
</p>

<p>
(Auszug aus einem Interview mit Mike Leigh)
</p>

    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>