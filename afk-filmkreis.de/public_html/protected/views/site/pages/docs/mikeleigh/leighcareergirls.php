<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighsecrets','docs.mikeleigh.leighfilmography')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighcareergirls"></a>
<h3>Career Girls<br>
<small>In die Jahre kommen</small>
</h3>


<p>&Auml;lter werden, was bedeutet das genau? Wie
ver&auml;ndert sich dar&uuml;ber das Verh&auml;ltnis zur Welt?
Soll man von einem Reifeprozess sprechen oder
eher von einer Ein&uuml;bung des Realit&auml;tsprinzips?
Oder trifft es weder das eine noch das andere? Auf
diese grossen Fragen findet Mike Leigh in seinem
neuesten Film eine Antwort, die sich kleinteilig
aus einer Reihe pr&auml;zis beobachteter Details zusammensetzt 
und einem doch viel eher eine Vorstellung von Geschichte
vermittelt als die opulenten
&laquo;period pictures&raquo; mit ihrem retro-chicen Rekonstruktionswahn.</p>


<p>Hannnah und Annie, die sich in ihrer Londoner Studienzeit gemeinsam mit
einer weiteren Kommilitonin eine Wohnung teilten, sehen sich
nach sechs Jahren zum ersten Mal wieder. &laquo;London hat sich sehr ver&auml;ndert, 
in mancherlei Hinsicht aber auch gar nicht&raquo;, fasst Annie, 
die die Stadt seit dem Ende ihres Studiums nicht mehr 
besucht hat, sinngem&auml;ss ihre ersten Eindr&uuml;cke 
zusammen. Und aus einem Mischungsverh&auml;ltnis von
Vergehendem und Bleibendem bildet sich im weiteren 
auch die Geschichtsphilosophie des Films heraus. 
Hinter den Gespr&auml;chen der beiden circa
dreissigj&auml;hrigen Frauen, den R&uuml;ckblenden und den
Wiederbegegnungen mit alten St&auml;tten und Bekannten treten zwei Biographien hervor, die zwar
einigen Ballast, aber auch Kostbares hinter sich gelassen haben und mit manchen Gespenstern der
Vergangenheit immer noch k&auml;mpfen. Man ist jetzt
sittsamer gekleidet, hat ganz ordentliche Jobs, liest
etwas weniger, und Annie pr&auml;sentiert sich im Vergleich 
zu fr&uuml;her auch als deutlich gefestigtere Pers&ouml;nlichkeit. Aber ihren endg&uuml;ltigen Platz im
Leben hat bisher keine der Frauen gefunden. Vieles
ist noch Provisorium: Annie will ihren Job wechseln, Hannah beizeiten die Wohnung, und dem
Mann f&uuml;rs Leben ist keine der beiden bisher begegnet, so dass in wichtigen Zukunftsfragen
nach wie vor Emily Bronte als Orakel konsultiert werden muss.</p>


<p>Der grossen Geste des Bilanzziehens enth&auml;lt
sich der Film. Hochfahrende Pl&auml;ne, &uuml;ber deren erfolgte 
oder nicht erfolgte Realisierung man sich
jetzt Rechenschaft ablegen m&uuml;sste, haben Annie
und Hannah nie gehabt. Dass ihr Studium (Psy-
chologie beziehungsweise Anglistik) nichts mit
dem sp&auml;teren Beruf zu tun haben w&uuml;rde, scheint
nach den englischen Verh&auml;ltnissen von vornherein
klar gewesen zu sein. Daher beschw&ouml;ren sie weder in 
Katerstimmung das "Paradies der Jugend",
noch k&ouml;nnen sie erhaben vom Standpunkt des
&laquo;Es-Geschafft-Habens&raquo; auf ihre bescheidenen Anf&auml;nge 
zur&uuml;ckblicken. Ihr Leben ist irgendwo dazwischen verlaufen.</p>

<p>
Nachdem Mike Leigh mit der Hauptfigur
von NAKED eine Art zynischen Antichristen geschaffen 
hatte und die damit verbundene Katharsis ihn dazu brachte, in SECRETS AND LIES 
auf etwas zu anr&uuml;hrende Weise auf Identifikation zu
setzen, w&auml;hlt er in CAREER CIRLS die Halbdistanz
als Erz&auml;hlhaltung. Was als Mittelwert zu verstehen
ist, denn die Abst&auml;nde variieren zwischen &laquo;ganz
nah&raquo; und &laquo;ganz weit weg&raquo;.</p>

<p>
Leigh h&auml;lt seine Schauspieler dazu an, die
Charaktere grotesk zu &uuml;berzeichnen. Annie, von
einem handgrossen Gesichtsekzem heimgesucht,
taumelt als schwer angeschlagenes Psycho-B&uuml;ndel
durch die R&uuml;ckblenden. Ihre Gesten sind fahrig
und die Bewegungen linkisch, es scheint immer,
als ob der geringste Anlass gen&uuml;gte, sie v&ouml;llig aus
der Bahn zu werfen. Hannah steht permanent unter Strom 
und redet pausenlos. Und wenn dann noch das stotternde, 
st&auml;ndig mit den Augen zwinkernde Riesenbaby Ricky zu dem Duo st&ouml;sst, ist
die Kakaphonie komplett.</p>

<p>
Aber diesen V-Effekt holt der Film wieder
ein, indem er bis ins tiefste Innere seiner Protagonisten vordringt. 
Und vielleicht besteht in dieser Form des Kompensationsgesch&auml;fts zurzeit auch
die einzige M&ouml;glichkeit, die Psychologie cineastisch zu rehabilitieren. In der eindrucksvollsten
Szene von CAREER GIRLS k&auml;mpft Annie gleichzeitig
mit ihrer Scham, ihrem Bekenntnisdrang, ihrem
feministischen &Uuml;ber-Ich und der Indifferenz ihres
Partners. Die an einem Vater-Komplex Leidende beginnt z&ouml;gerlich von ihren
Vergewaltigungsphantasien zu berichten, in denen sie sich von
einer Schar m&auml;nnlicher Voyeure umringt sieht. Dabei
unterbricht sie sich selbst und wird zu einem 
Kommentator in eigener Sache, wohl wissend,
dass sie, wenn auch nur imaginativ, an einer
Grundfeste des Feminismus r&uuml;hrt: dem &laquo;No
Means No&raquo; als eindeutige Grenzziehung gegen&uuml;ber 
m&auml;nnlichen Begehrlichkeiten. Vergeblich
bem&uuml;ht sich Annie, ihre verschiedenen Ichs unter
einen Hut zu bringen. Und als ihrem Freund auf 
ihre Offenbarung nichts anderes einf&auml;llt als lapidar 
etwas in der Art von "Wenn dir damit geholfen ist, 
frage ich meine Kumpels mal, ob sie uns
beim Sex zuschauen w&uuml;rden" zu entgegnen, und
er damit zus&auml;tzlich zu der Ebene der Imagination
und der des Politisch-Moralischen noch die des
Realen einbringt, verkompliziert sich die Lage
hoffnungslos.</p>

<p>
In solch eine Krisensituation wird die Figur
der Hannah nicht gebracht. Ihre sprachm&auml;chtige
Souver&auml;nit&auml;t, die sie jeder Lage gewachsen erscheinen 
l&auml;sst, bleibt im Kern unangetastet. Lediglich in einer 
Dialog-Passage gesteht sie einmal ein,
dass sie es als eine St&auml;rke Annies ansieht, verletzbar zu sein, 
immer wieder ohne Vorbehalt in neue
Beziehungen zu gehen, w&auml;hrend ihre eigene vermeintliche St&auml;rke 
nur der Panzer ist, der nach jeder Entt&auml;uschung undurchdringlicher wird.</p>

<p>
&laquo;Beste Nebenrollen&raquo; dieses Typs hat Katrin
Cartlidge schon in BEFORE THE RAIN und BREAKING
THE WAVES gespielt - nur als Drogens&uuml;chtige in
Mike Leighs NAKED durfte sie bisher die Fassung
verlieren -, und es w&auml;re schade, wenn sie auf das
Rollenfach der beherrschten, leidenschaftslosen
Frau festgelegt w&uuml;rde.</p>


<p>Jan Pehrke</p>

    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>