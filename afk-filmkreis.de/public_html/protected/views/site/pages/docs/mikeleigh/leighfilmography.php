<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighcareergirls')?>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighfilmography"></a>
<h3>Filmographie</h3>
      

<p>
<table summary="">

<tr>
<td>1971</td><td>BLEAK MOMENTS</td>
</tr>

<tr>
<td>1972</td><td>THE MUG'S GAME (BBC)</td>
</tr>

<tr>
<td></td>    <td>HARD LABOUR (BBC-Play for Today)</td><td>Mike Leigh-WEEKEND</td>
</tr>

<tr>
<td>1975</td><td>NUTS IN MAY (BBC-Play for Today)</td><td>Mike Leigh-WEEKEND</td>
</tr>

<tr>
<td></td>    <td>THE PERMISSIVE SOCIETY</td>
</tr>

<tr>
<td></td>    <td>THE BIRTH OF THE 2001 F.A. CUP FINAL GOALIE (Kurzfilm, BBC)</td>
</tr>

<tr>
<td></td>    <td>OLD CHUMS (Kurzfilm, BBC)</td>
</tr>

<tr>
<td></td>    <td>PROBATION (Kurzfilm, BBC)</td>
</tr>

<tr>
<td></td>    <td>A LIGHT SNACK (Kurzfilm, BBC)</td>
</tr>

<tr>
<td></td>    <td>AFTERNOON (Kurzfilm, BBC)</td>
</tr>

<tr>
<td>1976</td><td>KNOCK FOR KNOCK (BBC)</td>
</tr>

<tr>
<td>1977</td><td>THE KISS OF DEATH (BBC-Play for Today)</td><td>Mike Leigh-WEEKEND</td><td>Mike Leigh-WEEKEND</td>
</tr>

<tr>
<td></td>    <td>ABIGAIL'S PARTY (BBC)</td>
</tr>

<tr>
<td>1978</td><td>WHO'S WHO (BBC-Play for Today)</td>
</tr>

<tr>
<td>1980</td><td>GROWN-UPS (BBC)</td><td>Mike Leigh-WEEKEND</td>
</tr>

<tr>
<td>1982</td><td>HOME SWEET HOME (BBC-Play for Today)</td><td>Mike Leigh-WEEKEND</td>
</tr>

<tr>
<td>1983</td><td>MEANTIME (Channel Four)</td>
</tr>

<tr>
<td>1987</td><td>THE SHORT AND CURLIES (Kurzfilm)</td>
</tr>

<tr>
<td></td>    <td>FOUR DAYS IN JULY (BBC)</td><td>Mike Leigh-WEEKEND</td>
</tr>

<tr>
<td>1988</td><td>HIGH HOPES</td><td>Mike Leigh-REIHE</td>
</tr>

<tr>
<td>1990</td><td>LIFE IS SWEET</td><td>Mike Leigh-REIHE</td>
</tr>

<tr>
<td>1991</td><td>THE LONDON FILM FESTIVAL TRAILER</td>
</tr>

<tr>
<td>1992</td><td>A SENSE OF HISTORY</td>
</tr>

<tr>
<td>1993</td><td>NAKED</td>
</tr>

<tr>
<td>1995</td><td>SECRETS AND LIES</td><td>Mike Leigh-REIHE</td>
</tr>

<tr>
<td>1997</td><td>CAREER GIRLS</td><td>Mike Leigh-REIHE</td>
</tr>

</table>

</p>


<h4>Theater (in Auswahl)</h4>

<p>
<table summary="">

<tr>
<td>1965</td><td>THE BOX PLAY</td>
</tr>

<tr>
<td>1966</td><td>MY PARENTS HAVE GONE TO CARLISLE</td>
</tr>

<tr>
<td></td><td>THE LAST CRUSADE OF THE FIVE LITTLE NUNS</td>
</tr>

<tr>
<td>1967</td><td>NENAA</td>
</tr>

<tr>
<td>1968</td><td>INDIVIDUAL FRUIT PIES</td>
</tr>

<tr>
<td></td><td>DOWN HERE &amp; UP THERE</td>
</tr>

<tr>
<td>1969</td><td>BIG BASIL</td>
</tr>

<tr>
<td></td><td>GLUM VICTORIA AND THE LAD WITH SPECS</td>
</tr>

<tr>
<td></td><td>EPILOGUE</td>
</tr>

<tr>
<td>1970</td><td>BLEAK MOMENTS</td>
</tr>

<tr>
<td>1971</td><td>A RANCID PONG</td>
</tr>

<tr>
<td>1973</td><td>WHOLESOME GLORY</td>
</tr>

<tr>
<td></td><td>THE JAWS OF DEATH</td>
</tr>

<tr>
<td></td><td>DICK WHITTIGTON AND HIS CAT</td>
</tr>

<tr>
<td>1974</td><td>BABES GROW OLD</td>
</tr>

<tr>
<td></td><td>THE SILENT MAYORITY</td>
</tr>

<tr>
<td>1977</td><td>ABIGAIL'S PARTY</td>
</tr>

<tr>
<td>1979</td><td>ECSTASY</td>
</tr>

<tr>
<td>1981</td><td>GOOSE PIMPLES</td>
</tr>

<tr>
<td>1988</td><td>SMELLING A RAT</td>
</tr>

<tr>
<td>1989</td><td>GREEK TRAGEDY</td>
</tr>

</table>
</p>

    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>