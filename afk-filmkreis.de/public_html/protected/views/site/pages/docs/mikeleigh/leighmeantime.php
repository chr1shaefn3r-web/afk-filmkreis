<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighhome','docs.mikeleigh.leighjuly')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighmeantime"></a>
<h3>Meantime</h3>
      
<p>
<i>"Eine arbeitslose Familie in einem zu kleinen Apartment im Londoner East End: aus dem t&auml;glichen
Zeittotschlagen fl&uuml;chtet Vater Frank in die Resignation, sein Sohn Mark in den Zynismus, w&auml;hrend
dessen etwas zur&uuml;ckgebliebener j&uuml;ngerer Bruder Colin wenig erfolgreich versucht, eine Beziehung zu
einem M&auml;dchen anzukn&uuml;pfen, und mit einem Skinhead herumh&auml;ngt. Nach elf Jahren ausschlie&szlig;licher
Arbeit f&uuml;r die BBC Leighs erste unabh&auml;ngige Produktion, entstanden f&uuml;r Channel Four, ein Film &uuml;ber
die problematischen Konsequenzen des Gutgemeinten."</i> <small>(Filmbulletin 1.94)</small>
</p>

      
<h4>Mike Leigh:</h4>
      
<p>
<i>"Leider entstand der Film zu fr&uuml;h in der Geschichte von Channel Four, um als 
35mm-Spielfilm gedreht werden zu k&ouml;nnen. Allerdings kommt er 1993 in die Kinos. Er handelt von
Arbeitslosigkeit und ist der erste von drei eher eindeutig "politischen" Filmen. Barbaras
Projekt, H&auml;user zu streichen, steht f&uuml;r die Sinnlosigkeit aller kurzfristigen
Arbeitsbeschaffungsma&szlig;nahmen. Achten Sie auf Roger Pratts scharfe Bilder, Gary Oldman
im Fass und Peter Wights Satz: "Windows are good"."</i>
</p>

      
<h4>Zum Film:</h4>
      
<p>Die Schauspieler in MEANTIME spielen nicht sich selbst. Dies ist Charakterdarstellung im
besten Sinne des Wortes. Die F&auml;higkeit der Schauspieler, aus dem Charakter heraus zu
entstehen, ist wesentlich bei der improvisierten Evolution eines Filmes, dessen Bestandteile
auch Leidenschaft und Gewalt sind, wie dies beim MEANTIME manchmal der Fall ist. Aber
der Film selbst ist nat&uuml;rlich nicht "improvisiert". Er ist extrem strukturiert und ebenso
gr&uuml;ndlich durchdacht wie alle Theaterst&uuml;cke von Mike Leigh.
</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>