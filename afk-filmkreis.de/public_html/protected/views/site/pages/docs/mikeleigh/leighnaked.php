<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighlifesweet','docs.mikeleigh.leighsecrets')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighnaked"></a>
<h3>Naked</h3>
      
<p>
<i>"Eine Nacht lang streunt Johnny, ein Gossenphilosoph, durch London, erlebt verschiedene
bizarre Begegnungen und traktiert andere mit seinem wortgewaltingen Nihilismus. Seit
ABIGAIL'S PARTY wurde kein Leigh-Film so sehr von einer einzigen Figur dominiert: eine neue
Form und Stufe der Stilisierung mit einem "Helden", der keinen Zuschauer gleichg&uuml;ltig l&auml;sst."</i> <small>(Filmbulletin 1.94)</small>

</p>

      
<h4>Mike Leigh:</h4>
      
<p>
<i>"Meine Gef&uuml;hle gegen&uuml;ber NAKED sind genauso ambivalent wie meine Gef&uuml;hle gegen&uuml;ber
unserer chaotischen Welt im ausgehenden 20. Jahrhundert und vermutlich so ambivalent
wie der Film selbst. Ich hoffe, der Film ist ebenso komisch wie traurig, ebenso sch&ouml;n wie
h&auml;sslich, so mitf&uuml;hlend wie verabscheuungsw&uuml;rdig, so verantwortungsvoll wie arnachisch."</i>
</p>

      
<h4>Inhalt:</h4>
      
<p>Der Film befasst sich mit einer Seite Londons, die die meisten Leute nicht wahrhaben wollen.
Johnny ist der Inbegriff des Antihelden unseres Jahrzehnts - kalt, zynisch und unmoralisch,
dann wieder besorgt und leidenschaftlich. Gewaltt&auml;tig und sanft, komisch und tragisch
zugleich bewegt er sich durch sein Leben.
</p>

      
<h4>Preise:</h4>
      
<p>
<ul>
	  
<li>1993: Internationale Filmfestspiele Cannes: Beste Regie (Mike Leigh), Bester Schauspieler
(David Thewlis)</li>
	
</ul>

</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>