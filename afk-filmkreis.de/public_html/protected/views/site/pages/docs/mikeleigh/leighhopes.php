<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighjuly','docs.mikeleigh.leighlifesweet')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighhopes"></a>
<h3>High Hopes</h3>
      
<p>
<i>"Drei Paare im Zentrum von London: Cyril und Shirley, die "Alternativen", die f&uuml;r sich eine Nische in
Thatchers England gefunden haben, Cyrils Schwester Valerie, deren Verf&uuml;hrungs- und Kochk&uuml;nste bei
ihrem Ehemann auf wenig Reaktion sto&szlig;en und die gerne so w&auml;re, wie die Yuppies Boothe-Braine, die
Nachbarn ihrer alten Mutter. Leighs erster 35 mm-Spielfilm seit BLEAK MOMENTS: das Dumpfe
absch&uuml;ttelnd, pr&auml;sentiert er mit Cyril und Shirley zwei Figuren, die der nahezu uneingeschr&auml;nkten
Sympathie des Zuschauers sicher sein d&uuml;rfen.
"</i> <small>(Filmbulletin 1.94)</small>
</p>

      
<h4>Mike Leigh:</h4>
      
<p>
<i>"Endlich ein Spielfilm! Nach "Four Days in July" der dritte politische Film, obwohl hier &uuml;ber
die Politik nicht wirklich geredet wird. Dann geht es auch um alternde Eltern, mein Vater
war kurz zuvor gestorben. Und noch immer liegen die fr&ouml;hlichen Aussteiger im Zwist mit den
neurotischen Vorst&auml;dtern. Achten Sie auf Lesley Manvilles Satz "Do you have all your
original features?", auf Lindy Hemmings genau beobachtete Kost&uuml;me und auf Andrew
Dixons Musik, die ihm zwei Preise eintrug. (Und dazu ein weiterer Auftritt von Thatcher!)"</i>
</p>

      
<h4>Inhalt:</h4>
      
<p>HIGH HOPES spielt 1988 um und in King's Cross, dem Zentrum Londons und den Vororten.
Der Film verfolgt das Leben einiger nicht miteinander zu vereinbarender Personen, die im
Leben der anderen auftauchen und wieder verschwinden.<br>
Wayne hat sein Zuhause nach einem Streit verlassen. Cyril w&uuml;rde gerne der k&ouml;niglichen
Familie mit einem Maschinengewehr begegnen. Rupert und Laetitia Boothe-Brain
bevorzugen Yuppie-Sexspiele. Valerie gelingt es nicht, ihren Ehemann mit der Vorstellung
zu reizen, er sei Michael Douglas und sie eine Jungfrau. Mrs. Bender wird aus ihrem Haus
ausgeschlossen. Die Nachbarn kritisieren sie, weil sie ein ganzes Haus f&uuml;r sich allein
beansprucht. Cyrils Freundin Shirley m&ouml;chte eine Familie gr&uuml;nden, wird aber darin von
Cyril nicht ermutigt, denn der meinte, die Welt m&uuml;sse von weiteren Babies verschont
bleiben, bis jeder seinen Job gefunden habe, einen Platz zum Leben und genug zu essen.
</p>

      
<h4>Preise:</h4>
      
<p>
<ul>
	  
<li>1988: Filmfestival von Venedig: Preis der internationalen Filmkritik</li>
	  
<li>1989: European Film Awards: Beste Schauspielerin (Ruth Sheen)</li>
	  
<li>British Academy of Film and Television Arts (BAFTA): (Edna Dor&eacute;)</li>
	
</ul>

</p>

    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>