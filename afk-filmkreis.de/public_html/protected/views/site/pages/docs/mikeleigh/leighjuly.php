<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighmeantime','docs.mikeleigh.leighhopes')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighjuly"></a>
<h3>Four Days in July</h3>
      
<p>
<i>"Vier Tage in Nordirland, Alltagsszenen aus dem Leben eines katholischen (der Mann wurde von
Soldaten-Kugeln verkr&uuml;ppelt) und eines protestantischen Paares (der Mann ist bei der Armee) - am Ende
des Films bringen die beiden Frauen im selben Krankenhauszimmer ihre Babies zur Welt. Leighs
"normalste" Protagonisten - ohne die sonst kennzeichnenden Spleens, allerdings auch nicht geeignet,
dieselben Reaktionen beim Publikum auszul&ouml;sen, so da&szlig; Stephen Rea mit seinem Monolog Ober den
"Ulster Man" am st&auml;rksten im Ged&auml;chtnis bleibt - auch weil es der einzige komische Moment im Film
ist.
"</i> <small>(Filmbulletin 1.94)</small>
</p>

      
<h4>Mike Leigh:</h4>
      
<p>
<i>"Mein letzter BBC-Film. Und mein erster in einer Fremdsprache - erg&ouml;tzen Sie sich an dem
wunderbar undurchdringlichen David Coyle als Mickey! Alles gedreht auf den Stra&szlig;en von
East und West Belfast! Mein erster Versuch, das Wesen jeder Seite so einzufangen, wie ich es
empfand. Und ein Ausdruck meiner Hoffnungslosigkeit angesichts der unvermeidbaren
Endlosigkeit der Schwierigkeiten. Besondere Aufmerksamkeit verdienen: der Long Kesh
Double-act von Stephen Rea, der mich &uuml;berhaupt dazu anregte, den Film zu machen, und
Shane Connaughton, die sp&auml;ter "My Left Foot" schrieb. Und Rachel Portmans bewegende
Hurdy-Gurdy-Musik."</i>
</p>

      
<h4>Zum Film:</h4>
      
<p>1983 schickte der Produzent Kenith Trodd Mike Leigh nach Ulster, um dort etwas auf die
Beine zu stellen...<br>
Wie gehabt passiert wenig bei Mike Leigh. Zwei Frauen stehen kurz vor der Niederkunft:
Colette, eine Katholikin, die in der Falls Road lebt, und Lorraine, die Gattin eines Loyalisten
in der britischen Armee. Im Verlauf der vier Tage w&auml;hrenden Veranstaltungen der
protestantischen Boyne-Schlacht sehen wir einzelne Episoden aus ihrem Leben... Nie spricht
jemand &uuml;ber Politik: Das Leben der einzelnen Figuren ist derma&szlig;en mit den "Problemen"
verwoben, da&szlig; das gar nicht n&ouml;tig ist.
</p>
<p>
Sean French in "The Sunday Times"
</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>