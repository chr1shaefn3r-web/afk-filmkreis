<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighhopes','docs.mikeleigh.leighnaked')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighlifesweet"></a>
<h3>Life is Sweet</h3>
      
<p>
<i>"Essen als zentrale Metapher bei der Schilderung des Versuchs einer Arbeiterfamilie, im 
Thatcher-England zu &uuml;berleben. Die St&auml;rke der Charaktere nimmt zu, die Karikatur ab.
"</i> <small>(Filmbulletin 1.94)</small>
</p>

      
<h4>Inhalt:</h4>
      
<p>Natalie und Nicola sind Zwillinge. Natalie ist
Installateurin, Nicola ist arbeitslos. Die heitere,
tatkr&auml;ftige Natalie geht oft aus, w&auml;hrend die
magers&uuml;chtige, aggressive und verwirrte Nicola
das Haus nie verl&auml;&szlig;t.<br>
Ihr Vater Andy tr&auml;umt davon, ein eigenes Gesch&auml;ft
aufzubauen, und l&auml;sst sich von seinem hinterlistigen 
Trinkkumpanen Patsy dazu &uuml;berreden,
eine verfallene Imbissbude auf R&auml;dern zu kaufen.
Die Mutter Wendy arbeitet Teilzeit in einem
Bekleidungsgesch&auml;ft f&uuml;r Kinder. Als Aubrey, ein
exzentrischer Freund der Familie, ein Restaurant
er&ouml;ffnet, eilt Wendy ihm zu Hilfe. Aber am ersten
Abend im Lokal betrinkt sich Aubrey sinnlos.
Tags&uuml;ber erh&auml;lt Nicola heimlich Besuch von ihrem
Freund. Er hat es satt, ihre perversen sexuellen
Gel&uuml;ste zu befriedigen, und sehnt sich nach
intelligenter Unterhaltung, die aber Nicola
&uuml;berfordert.<br>
W&auml;hrend Nicola auf den v&ouml;lligen Zusammenbruch
zusteuert und Andy bei der Arbeit verletzt wird,
erweist sich Wendy als Anker im Sturm...
</p>

      
<h4>Zum Film:</h4>
      
<p>Nat&uuml;rlich ist LIFE IS SWEET kein apolitischer Film - es geht um die Politik des Privaten.
Wenn ich hier nicht &uuml;ber Mrs. Thatcher spreche, dann deswegen, weil ich eine andere
Perspektive gew&auml;hlt habe. Das heisst nicht, dass das Thatcher-Thema begraben ist. Es w&auml;re
aber langweilig, in jedem Film wieder neu dar&uuml;ber zu reden... Ich arbeite sehr intuitiv...
Bevor wir zu drehen anfangen, arbeite ich zuerst einmal ganz intuitiv mit den Schauspielern
- was bei "Life is Sweet" ungef&auml;hr drei Monate in Anspruch nahm. In dieser
Vorbereitungszeit entstehen die Personen, ihre Beziehung zueinander und alles andere; sie
entwickeln sich aus endlosen Nachforschungen, Diskussionen und vor allem
Improvisationen. Die Beziehungen der Familienmitglieder wurden tats&auml;chlich durchlebt. Es
ist schon etwas dran an Ihrer Feststellung, da&szlig; keine objektiven Erkl&auml;rungen daf&uuml;r
abgegeben werden, warum eigentlich alles so ist, wie es ist. Trotzdem w&uuml;rde ich doch sehr
hoffen, dass das, was sich vor Ihren Augen abspielt, als wirklich erscheint. Die Schauspieler
sprechen nicht einfach nur einen Text. W&auml;hrend der langen Probezeit etablieren sich
zwischen den Personen, die spielen, tats&auml;chliche Beziehungen.
</p>
<p>
(Auszug aus einem Interview mit Mike Leigh im Berlinaletip)
</p>

      
<h4>Preise:</h4>
      
<p>
<ul>
	  
<li>American Critics Association: Bester Film, Beste Schauspielerin (Alison Steadman), Beste Nebendarstellerin (Jane Horrocks)</li>
	  
<li>Los Angeles Film Critics' Award: Beste Schauspielerin (Jane Horrocks)</li>
	
</ul>

</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>