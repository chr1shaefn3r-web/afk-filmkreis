<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-intro"></a>
<h3>Vorwort<br>
<small>Mike Leigh - Zwischen Kino und Fernsehen</small>
</h3>

<p>   Seit NAKED (1993, Internationale Filmfestspiele Cannes: Beste Regie; Mike Leigh, Bester
   Darsteller: David Thewlis) und sp&auml;testens seit SECRETS AND LIES / L&Uuml;GEN UND GEHElMNISSE 
(1996 Internationale Filmfestspiele Cannes: Bester Film, Beste Darstellerin: Brenda
   Blethyn) ist Mike Leigh auch dem breiten Kinopublikum ein fester Begriff geworden.
   Dass dem inzwischen 54j&auml;hrigen der internationale "Durchbruch" erst nach &uuml;ber 20 Jahren
   Filmarbeit gelang, hat vor allem mit der desolaten Situation der englischen Filmindustrie seit den
   70er Jahren zu tun.<br>
   Nach seinem ausgezeichneten und vielversprechenden Erstling BLEAK MOMENTS (1971,
   Internationale Filmfestspiele Locarno: Goldener Leopard, Bester Film) erging es ihm nicht
   anders als anderen Regisseuren; er konnte f&uuml;r eine lange Zeit ausschlie&szlig;lich f&uuml;r das Fernsehen
   drehen. Weder ein breites Publihum noch die Filmkritik waren so erreichbar - das Aus f&uuml;r eine
   internationale Karriere.
</p>


<p>
<i>"Wenn mir nach BLEAK MOMENTS jemand
   gesagt h&auml;tte, dass ich dreizehn Jahre lang
   nicht f&uuml;rs Kino arbeiten w&uuml;rde, w&auml;re ich sehr
   deprimiert gewesen. Aber niemand hat in
   den siebziger Jahren in England unabh&auml;ngige Filme gemacht. Das &auml;nderte sich erst
   Anfang der achtziger Jahre, als Channel Four
   ins Leben grufen wurde: In der selben Zeit
   wie  BLEAK MOMENTS drehte Stephen
   Frears seinen ersten Kinofilm,  GUMSHOOE, und Ken Loach machte KES. Beide
   arbeiteten in den n&auml;chsten Jahren auch nur
   f&uuml;r das Fernsehen. Und f&uuml;r mich war es
   sicherlich noch schwieriger wegen meiner
   Arbeitsweise. Wenn ich jetzt zur&uuml;ckblicke,
   finde ich es geradezu erstaunlich, da&szlig; nach
   anderthalb Jahrzehnten, wo keine unabh&auml;ngigen Spielfilme m&ouml;glich waren, ich seit
   1987 kontinuierlich Filme drehen konnte. Bis
   1980  weigerte  sich  das  London  Film
   Festival, Fernsehproduktionen zu zeigen.
   Ich erwog damals ernsthaft, w&auml;hrend des
   Festivals in einen Hungerstreik zu treten und
   mich anzuketten, so verzweifelt war ich. Die
   Leiter erkl&auml;rten mich f&uuml;r verr&uuml;ckt; ich k&ouml;nne
   doch Filme machen, das Fernsehen w&uuml;rde
   sie zeigen, viele Menschen w&uuml;rden sie dort
   sehen - warum m&uuml;sse es denn unbedingt ein
   Spielfilm sein? Das war eine sehr frustrierende Zeit.
"</i>
</p>


<p>   Channel Four wurde 1982 gegr&uuml;ndet. In seinen Statuten festgelegt auf eine "innovative, originelle" 
   Programmgestaltung nahm dieser Fernsehsender gro&szlig;en Einflu&szlig; auf die britische
   Fernseh- und Kinolandschaft und half ganz wesentlich durch sein Filmf&ouml;rderungsprogramm das
   New British Cinema der 80er Jahre loszutreten (Mein wunderbarer Waschsalon, Sammy und
   Rosie tun es, London kills me, ...).
   Auch Mike Leigh bekam so die M&ouml;glichkeit, endlich wieder f&uuml;r das Kino zu drehen. HIGH HOPES
   (1988) war sein erster 35mm-Film seit 17 Jahren.
</p>


<p>   Seine lange T&auml;tigkeit f&uuml;r das Britische Fernsehen wurde erstmals w&auml;hrend den Hofer Filmtagen
    1993 mit einer umfangreichen Werkschau gew&uuml;rdigt. Das BBC hat jetzt zusammen mit Mike
   Leighs Produktionsgesellschaft und dem BFI (British Film Institute) eine Auswahl seiner
    Fernsehfilme f&uuml;r das Kino zusammengestellt. Anlass f&uuml;r uns zu einer kleinen Mike Leigh
   Werkschau mit einem Teil seiner Kinofilme und BBC-Produktionen, die hier bislang noch weitgehend unbekannt sind.
</p>


<p>Viel Spa&szlig; mit Mike Leigh!</p>
    
<a name="sec-leighfilms"></a>
<h3>Mike Leigh und seine Filme</h3>

<h4>Das Allt&auml;gliche und Extreme</h4>


<p>
<i>"Meine Zielsetzung ist es, das Gew&ouml;hnliche au&szlig;ergew&ouml;hnlich zu machen, den Zuschauer mit
Figuren zu konfrontieren, die er extrem findet, denn Menschen sind extrem. Letztendlich ist jeder
ein Individuum."</i> So &auml;u&szlig;erte sich Leigh zu NAKED, seinem mit Sicherheit extremsten und 
verst&ouml;rendsten Film - der ihn allerdings auch bekannt gemacht hat (Goldene Palme in Cannes 1993). 
Johnny, die Hauptfigur, ist ein nihilistischer Gassenphilosoph, respektlos und menschenverachtend, 
brutal und exzessiv. Ein Mensch, der nicht in der Lage ist, menschliche W&auml;rme
anzunehmen oder weiterzugeben, der sich das, was er braucht oder zu brauchen glaubt, mit
Gewalt nimmt. Eine haltlose, ortslose Figur - und vor allem einsam.</p>


<p>
Und hier ist auch die Schnittstelle zu Leighs
restlichen Filmen, die so viel harmloser, 
allt&auml;glicher erscheinen.  Leigh befindet sich
st&auml;ndig auf der Suche nach dem, was sich
zwischen seinen Figuren abspielt, was sie
verbindet oder trennt.  Ihre Beziehungen
zueinander, ihre F&auml;higheit bzw. Unf&auml;higkeit
zur Kommunikation, ihre soziale Einbindung
oder Ausgrenzung,  ihn  interessiert  die
Normalit&auml;t, die kann mitunter auch extrem
sein. Dazu braucht Leigh nicht unbedingt
Figuren wie Johnny. Meistens sind seine
Charaktere eher "Durchschnittsmenschen",
die zur Arbeiterklasse geh&ouml;ren, Leigh zeich-
net sie mit Vorliebe in ihrer allt&auml;glichen
Umgebung - Familie, Arbeit, Nachbarschaft,
soziales  Umfeld, was Frank Arnold dazu
bewegt hat, seine Filme als "Ethnologie der
britischen Klassengesellschaft" zu bezeichnen.
</p>


<p>
Die Begeisterung f&uuml;r das Kino begann bei Leigh sehr fr&uuml;h: <i>"Ich w&uuml;rde sagen, es gibt keinen Film
zwischen 1949 und 1960, den ich nicht gesehen habe."</i> - <i>"Mir ist nichts entgangen, und ich fand
alles gro&szlig;artig. Aber trotzdem ging mir durch den Kopf, da&szlig; es doch wunderbar sein m&uuml;sste,
einen Film zu sehen, in dem die Leute sich so benehmen, wie die Menschen auf der Stra&szlig;e und
nicht wie Kinofiguren."</i>

</p>


<p>
Da es Leigh weniger um die Geschichte geht, sondern um das Verhalten und Verh&auml;ltnis seiner
Charaktere zueinander, sind die Handlungen seiner Filme stark reduziert. Es gibt selten viel
Aktion oder extreme Wendungen. Es geht vor allem darum, die Figuren zusammenzubringen.
</p>


<p>
Alfred Hitchcock hat sich dazu einmal folgenderma&szlig;en ge&auml;u&szlig;ert: <i>"Welche Frau, die den ganzen
Tag am Sp&uuml;lstein gestanden und den Abwasch gemacht hat, will f&uuml;nf Dollar an der Kinokasse
bezahlen, nur um auf der Leinwand eine Frau zu sehen, die am Sp&uuml;lstein steht und den
Abwasch macht?"</i> Mike Leigh dazu: <i>"Was Hirchcock sagt, ist mir wurscht. Ich m&ouml;chte wetten,
da&szlig; Hitchcock in seinem Leben nicht ein einziges Mal in der K&uuml;che gestanden und Geschirr
gesp&uuml;lt hat! Was redet er also? BLEAK MOMENTS startete in London in derselben Woche wie
Hitchcocks FRENZY. Einer der gr&ouml;&szlig;ren Gl&uuml;cksmomente in meinem Leben war der, als ich im
"Observer" eine Gegen&uuml;berstellung der beiden Filme las. Darin stand sinngem&auml;&szlig;: "Auf der einen
Seite haben wir Hitchcock, der daf&uuml;r ber&uuml;hmt ist, seine Schauspieler zu verachten, und auf der
andere Seite ist da Mike Leigh, der seinen Akteuren offenkundig gr&ouml;&szlig;ten Respekt entgegenbringt."
Das gedruckt zu sehen, hat mir sehr gefallen."</i>

</p>


<p>
In der Tat hat Mike Leigh eine ganz besondere Beziehung zu seinen Schauspielern.
</p>


<h4>Devised and Directed by Mike Leigh</h4>


<p>

<i>"Devised and Directed by Mike Leigh"</i> - mit diesem Satz beginnen bzw. enden viele seiner Filme.
er steht vor allem f&uuml;r eine etwas andere Auffassung vom Prozess des Filmemachen, und - damit
verbunden - f&uuml;r seinen unverkennbaren pers&ouml;nlichen Stil.
</p>


<p>
Das betrifft vor allem die Beziehung zu seinen Schauspielern: Er gibt ihnen viel Raum in der
Interpretation ihrer Rollen und beteiligt sie am kompletten Entstehungsprozess des Films.
Entsprechend sorgsam triftt er seine Auswahl. Darsteller wie Ben Kingsley, Gary Oldman, Tim
Roth usw. haben in seinen Filmen ihre Karriere begonnen, andere wie Timothy Spall, Alison
Steadman, Philip Davis, David Thewlis, Claire Skinner, Katrin Cartlidge, Brenda Blethyn usw.
tauchen immer wieder in den unterschiedlichsten Rollen auf.
</p>


<p>
Mike Leigh "beginnt" einen Film nicht mit einem fertigen Script und ausgearbeiteten Figuren.
Ausgangspunkt ist Idee, eine Vorstellung von Figuren oder ein abstrakter Handlungsgang.
Bei SECRETS AND LIES waren es die adoptionsbedingten Erfahrungen eines befreundeten
Ehepaares, der Wunsch, sich mit einer Generation von Schwarzen zu besch&auml;ftigen, die erwachsen 
wird und aufbricht; au&szlig;erdem hatte er &uuml;ber schwarze Kinder recherchiert, die von wei&szlig;en
Frauen in den 50er und 60er Jahren geboren worden waren. <i>"Aber kannte ich die Geschichte?
Nein. Das sind die Dinge, die man beim Machen eines Films entdeckt."</i>

</p>


<p>
Bereits zu diesem Zeitpunkt w&auml;hlt er die Schauspieler aus. (Ihre Anzahl bestimmt sich nicht 
selten durch die Gr&ouml;&szlig;e des Budgets). Er hat ungef&auml;hre Vorstellungen von ihren Rollen innerhalb
des Films - einen Entwurf. Pr&auml;zisiert wird die Figur aber erst gemeinsam mit dem Darsteller.
<i>"... Ja, wie sympathisch ein Charakter wird, entwickelt sich w&auml;hrend der Proben. Nein, ich weiss
vorher schon, wie etwas aussehen soll. Der K&uuml;nstler erschafft das Material; alles Material kommt
zu einer Existenz als Resultat der Kombination von Improvisation und Ordnung."</i> Konkret 
funktioniert das so: <i>"Ich arbeite sehr eng mit jedem einzelnen Schauspieler zusammen, um eine
Figur zu erschaffen. St&uuml;ck f&uuml;r St&uuml;ck entwickeln wir die ganze Geschichte dieser Figur, ihre
ganze Welt mit all den Beziehungen. Auch die Zeit ist sehr wichtig, die chronologische Zeit des
Lebens einer Figur, die Jahre, die sie bereits gelebt hat. Dabei geht es nicht nur um
Improvisation, sondern auch um Recherche. Aber das Wichtigste ist dabei nicht. was der
Schauspieler individuell macht, sondern was die Darsteller zusammen in den Beziehungen
machen."</i>

</p>


<p>
Sind die Hintergr&uuml;nde entwickelt, l&auml;sst Leigh seine Darsteller erstmals aufeinandertreffen. Dann
beginnt der eigentliche "workshop process". Gemeinsam werden Szenarien entworfen, die 
verschiedenen Figuren begegnen einander - in Improvisationen werden M&ouml;glichkeiten ihrer 
gegenseitigen Beziehungen untersucht, bis sich daraus konkrete Strukturen und ein Handlungsablauf
ergeben.
</p>


<p>
Katrin Cartlidge (NAKED, CAREER GIRLS) hat diesen Entwicklungsprozess in einem Interview
folgenderma&szlig;en beschrieben: <i>"Man arbeitet f&uuml;r Monate - wie ich es bezeichnen w&uuml;rde - in einer
Art von organischem Chaos. Jede einzelne Situation wird geformt und pr&auml;zisiert - das beginnt
mit Improvisationen, wird dann zu etwas Wiederholbarem und wird dann festgehalten, fixiert...
Mike startet im Chaos und kommt an bei einer strukturierten Form."</i>

</p>


<p>
Diese "strukturierte Form", die Essenz aus den gemeinsamen Vorarbeiten, verarbeitet Leigh
dann zu dem endg&uuml;ltigen Script. <i>"Dieser ganze Vorbereitungsprozess - &uuml;ber den ich nur sehr
z&ouml;gernd spreche, wie Sie gemerkt haben: weil das privat ist; wichtig ist der Film, der am Ende 
dabei herauskommt - dient nur dazu, uns darauf vorzubereiten, den Film w&auml;hrend der 
Dreharbeiten zu erschaffen, St&uuml;ck f&uuml;r St&uuml;ck. Ohne diese Vorbereitung k&ouml;nnte ich nie mit den 
Schauspielern on location gehen."</i> -  Leigh dreht zu  einem Zeitpunkt,  an  dem  der 
Entwicklungsprozess zu einem Ende gekommen ist, diesen Punkt will er festhalten. Improvisiert
wird bei den Aufnamen daher nicht mehr.
</p>


<p>
Diese komplette Vorlaufphase dauerte bei NAKED vier und bei SECRETS AND LIES sogar f&uuml;nf
Monate. (Zum Vergleich: die Dreharbeiten zu NAKED waren nach 14 Wochen abgeschlossen.)
</p>


<p>
Diese Form von Vorbereitung verlangt den Darstellern einiges ab. Bei SECRETS AND LIES z.B. 
hat Leigh Brenda Blethyn, "Cynthia", nicht dr&uuml;ber informiert, dass ihre zur Adoption 
freigegebene Filmtochter schwarz sein w&uuml;rde:
</p>


<p>
<i>"
lch hatte Marianne Jean-Baptistes Name auf der Darstellerliste gesehen, aber ich hatte sie
davor nie getroffen. Ich ging zu der Station an diesem Tag, sah Marianne und wusste, sie war
nicht die Richtige. Als sie mich fragte, "Sind Sie Cynthia?", dachte ich wirklich, sie geh&ouml;re zur
Crew! Also wenn Cynthia sagt, es handele sich um einen Fehler, war das meine ehrliche
Reaktion. Ich habe nicht gespielt."</i>

</p>


<p>
Nicht anders erging es &uuml;brigens den restlichen Darstellern bei der "ersten gemeinsamen"
Familienfeier...
</p>



<p>
W&auml;hrend Leigh Umfeld und Interieurs in seinen Filmen mit akribischer Detailtreue inszeniert,
ob das nun die Bl&uuml;mchentapete im Wohnzimmer oder das immer wiederkehrende Tee-Ritual
(mit Keksen) ist, legt er das Schauspiel aus auf &Uuml;berzeichnung und Expressivit&auml;t. Regelrecht
schonungslos zeigt er seine Figuren mit ihren Eigenheiten, Verklemmungen und Komplexen -
jedoch nie ohne Sympathie.
</p>


<p>
Dies hat ihm schon oft den Vorwurf eingehandelt, blosse Karikaturen zu entwerfen, bzw. von
Seiten der britischen Linken, sich &uuml;ber die englische Arbeiterklasse lustig zu machen. Leigh
dazu:
</p>


<p>
<i>"
Wenn  Sie  unter  einem  Karikaturisten
George Grosz oder Otto Dix verstehen, ist
das  wunderbar,  dann  freue  ich  mich,
Karikaturist genannt zu werden, jemand der
die grotesken Charakterz&uuml;ge des Menschen
hervorhebt. Aber ich sehe mich selbst nicht
als Karikaturisten. Ich arbeite mit den Mitteln
der &Uuml;berh&ouml;hung, um an die Essenz der
Dinge zu gelangen."</i>

</p>


<p>
Es geht ihm  nicht um eine realistische,
unmittelbare Abbildung von  Realit&auml;t.  Die
Genauigkeit im Einfangen der allt&auml;glichen
Rituale erweist sich als Ergebnis pr&auml;ziser
Stilisierung. Genauso die Zeichnung der
Figuren: nichts bleibt dem Zuschauer verborgen, 
jeder Spleen, jede Eigenheit tritt
&uuml;berdeutlich zum Vorschein. Leighs Filme
erhalten ihre Spannung gerade aus dieser
"Verdichtung von Realit&auml;t". Dann, wenn man
glaubt, diese "Typen" eingeordnet zu haben,
bricht Leigh das vorgefertigte Bild, l&auml;sst ihre
Motivationen  und  Verletzlichkeiten  zum
Vorschein hommen, <i>"und dadurch verm&ouml;gen
die Figuren den Zuschauer in einer Weise zu
ber&uuml;hren, wie man sie aus anderen Filmen
nicht gewohnt ist."</i>

</p>


<p>
Weniger verst&auml;ndnisvoll ist Leigh dagegen
in seiner Schilderung der "upper class" -
ihnen verzeiht er so gut wie nie.
</p>


<p>

<i>"Ich treffe in meinen Filmen keine moralischen Urteile, ich ziehe keine Schl&uuml;sse. Ich stelle
Fragen, ich beunruhige den Zuschauer, ich mache ihm ein schlechtes Gewissen, lege Bomben,
aber ich liefere keine Antworten. Ich weigere mich, Antworten zu geben, denn ich kenne die
Antworten nicht."</i> <small>(Mike Leigh in einem Interview zu NAKED)</small>

</p>


<h4>Biographisches</h4>



<p>
Leigh wird immer wieder nach autobiographischen Aspekten seiner Filmen befragt. Man sucht
nach den Motiven, wieso er sich so gerne und intensiv mit der britischen Arbeiterklasse 
besch&auml;ftigt. Eigentlich ist seine Herkunft eher b&uuml;rgerlich, trotzdem ist ihm die 
Gesellschaftsschicht, die er mit Vorliebe zeigt, nicht fremd. Er wurde 1943 in Lancashire geboren.
</p>


<p>

<i>"Mein Vater war Arzt in einem Arbeiterviertel von Manchester, meine Mutter Krankenschwester.
Ich hatte mein Zimmer &uuml;ber der Praxis meines Vaters. Ich wuchs also als Arztsohn im 
proletarischen Milieu auf und besuchte die dortige Schule. Von daher gibt es sicher eine Art Spannung
  in meiner Biographie, einen Einfluss von zwei
  Seiten, zwei verschiedenen gesellschaftlichen
  Schichten.  Ich  geh&ouml;re  zu  der
  Generation, die w&auml;hrend des Krieges geboren
  wurde und die f&uuml;nfziger Jahre als aus-
  gesprochen langweilig empfand."</i>

</p>


<p>
  Die Flucht gelang ihm zun&auml;chst in exzessiven
  Kinobesuchen. Wie schon eingangs
  erw&auml;hnt, gibt es keinen Film zwischen 1949
  und 1960, den er nicht gesehen hat. 1960
  erhielt er ein Stipendium f&uuml;r die  Royal
  Academy for Dramatic Arts (RADA), das war
  sozusagen sein Aufbruch in die Gro&szlig;stadt.
  Dort studierte er Schauspiel und f&uuml;hrte
  w&auml;hrend seines zweij&auml;hrigen Aufenthaltes
  Regie bei einer Studentenproduktion von
  Harold  Pinters  HAUSMEISTER.  Seine
  Ausbildung setzte er fort an der Camberwell
  Art   School,   der   Abteilung   f&uuml;r
  B&uuml;hnenentw&uuml;rfe der Central School of Arts
  and Crafts und an der London Film School.
  1967 arbeitet er als Regieassistent bei der
  Royal Shakespeare Company. Bevor er mit
  dem Filmen begann, schrieb und arbeitete er
  f&uuml;rs Theater. 1965 wurde in Birmingham sein
  erstes St&uuml;ck aufgef&uuml;hrt.
</p>


<p>
  Sein erster Spielfilm, BLEAK MOMENTS
  (1971) basierte auf seinem gleichnamigen
  B&uuml;hnenst&uuml;ck. Der Film war frei finanziert,_
  konnte noch auf 35mm gedreht werden. Auf
  den  Filmfestspielen  in  Chicago sowie
  Locarno erzielt er die Hauptpreise. Danach
  folgt die Arbeit f&uuml;r das BBC.
</p>


<p>
  Er   verfilmt   zwei   weitere   seiner
  Theaterst&uuml;cke, NUTS IN MAY (1975), wo ein
  Teil der Figuren aufgegriffen wurde, allerdings 
  mit einer v&ouml;llig neue Rahmenhandlung,
  und ABIGAIL'S PARTY (1977), das sich sehr
  eng an die B&uuml;hneninszenierung h&auml;lt - eine
  Arbeit, von der er sich jetzt eher distanziert,
  da die f&uuml;r ihn so wichtige Adaption an das
  andere Medium (Film) nicht stattgefunden
  hat.
</p>


<p>
  1983 entsteht MEANTIME mitproduziert von
  Channel Four - damals leider noch auf 16mm, 
  der Film l&auml;uft im Internationalen Forum
  der Berlinale.
</p>


<p>
  In den 80er Jahren arbeitet Leigh kaum noch
  f&uuml;rs Theater. <i>"Ich mache diese Arbeit nur
  sehr  z&ouml;gernd,  es  ist  nicht  meine
  Leidenschaft, meine Liebe. Ich habe keinen
  gro&szlig;en Enthusiasmus daf&uuml;r. Die Arbeit selber
  ist o.k., aber man muss derartig viel an
  Vorbereitung investieren, und dann l&ouml;st es
  sich einfach in Luft auf."</i>

</p>


<p>Mit HIGH HOPES (1988),  wieder eine
 Koproduktion von Channel Four, kann er
 nach  17  Jahren  wieder  einen  35mm
 Spielfilm drehen. Er wird in Venedig mit dem
 Preis der Internationalen Filmkritik 
 ausgezeichnet und erh&auml;lt bei der Verleihung des
 Europ&auml;ischen  Filmpreises  "Felix"  drei
 Troph&auml;en. Endlich wird auch das 
 internationale Publikum auf Leigh aufmerksam.
 1990 folgt LIFE IS SWEET, ebenfalls mehrfach
 ausgezeichnet.  Der  "Durchbruch" kommt
 dann mit NAKED (1993): Leigh erh&auml;lt
 den Preis f&uuml;r die Beste Regie in Cannes,
  sein Haupdarsteller David Thewlis wird als
  Bester Schauspieler ausgezeichnet.
  Sein n&auml;chster Film SECRETS AND LIES
  (1995) gewinnt in Cannes sogar die Goldene
  Palme als bester Film, Brenda Blethyn wird
  Beste Schauspielerin.  Und ... SECRETS
  AND LIES wird ein internationaler Erfolg an
  den Kinokassen.<br>
  1997 entsteht CAREER GIRLS.
</p>


<p>
  Seit  1965  hat  Leigh  22  B&uuml;hnenst&uuml;cke
  geschrieben und inszeniert. 1989 gr&uuml;ndete
  er zusammen mit dem Produzenten Simon
  Channing       Williams       die
  Produktionsgesellschaft "Thin Man Films",
  die sich ausschlie&szlig;lich um seine eigenen
  Filme k&uuml;mmert.
</p>


<p>
 Leighs Filme insgesamt einzuordnen, Phasen zu unterscheiden, w&uuml;rde ihm selbst widerstreben.
 Gerne wird von einer zunehmenden Politisierung in seiner Arbeit gesprochen, die in NAKED
 ihren H&ouml;hepunkt gefunden h&auml;tte. Aber letztlich kreist Leigh immer wieder um pers&ouml;nliche,
 zwischenmenschliche Themen, und die sind zu universell, um sie auf eine "Ethnologie der britischen
 Arbeiterklasse" zu beschr&auml;nken.
</p>


<p>
  Leigh selbst unterscheidet seine Filme nur in die Zeit Vor, W&auml;hrend und Nach Thatcher.
  Vielleicht ist das ein Grund, dass er mit SECRETS AND LIES und CAREER GIRLS einen
  w&auml;rmeren und vers&ouml;nlicheren Ton angeschlagen hat - mit politisch oder unpolitisch hat das jedoch
  nichts zu tun.
</p>


<p>
  Oder anders ausgedr&uuml;ckt: Zwar hat er 1960 dem "Kleinstadtmief" endlich den R&uuml;cken gekehrt,
  thematisch hat er seine "Heimat" jedoch nie richtig verlassen. <i>"Auf die eine oder andere Art
  handeln meine Filme von Wurzeln und von Familie"</i>, sagt Leigh. <i>"Einer meiner Cousins sah sich
  SECRETS AND LIES an. Und sein Kommentar: 'Oh, there we are all again.'"</i>

</p>

      
<p>&nbsp;</p>
<hr>
<h3>Literatur / Quellenangaben</h3>
<p>
<small>Zitate aus:</small>
</p>
<ul>
<li>
<small>epd Film 2/94: Robert Fischer; "Ein filmisches Universum aus Wut und Armut"</small>
</li>
<li>
<small>FILMBULETTIN 1.94: "l wouldn't be joking if I wasn't serious" - eine Ann&auml;hrung an Mike Leigh und seine Filme</small>
</li>
<li>
<small>TIME Magazine 30.09.96: Richard Corliss: "Family Values"</small>
</li>
<li>
<small>DIE ZEIT 29.09.96: Willi Winkler: "Drastisch ausgeleuchtet - Ein Portr&auml;t des Regisseurs Mike Leigh"</small>
</li>
<li>
<small>Festivalkatalog der Hofer Filmtage, 1993</small>
</li>
</ul>
    
<a name="sec-leighbleakmoments"></a>
<h3>Bleak Moments</h3>

<p>
Eine Frau, zwei M&auml;nner: miteinander reden, ohne sich anzusehen, miteinander schweigen, Tee trinken.
<i>"Ein programmatischer Titel; Leighs erster Film ist bis heute sein qu&auml;lendster geblieben - mitzuerleben,
wie Personen um Worte ringen und nicht f&auml;hig sind, sich zu &auml;ussern, kann den Zuschauer geradezu
aggressiv machen."</i> <small>(FILMBULLETIN 1.94)</small>

</p>


<h4>Mike Leigh:</h4>
      
<p>

<i>"Mein erster Film und mein letzter 35mm-Spielfilm f&uuml;r die n&auml;chsten 17 Jahre. Es geht um
 meine Einsamkeit, mein Bedauern angesichts verpasster sexueller Chancen, einen Toast auf
 die alternative Subkultur, eine Erinnerung an reaktion&auml;re Lehrer, unh&ouml;fliche chinesische
 Kellner und versteckten Humor. Und der Film beschw&ouml;rt auch diese stillen Nachmittage in
 Vorst&auml;dten herauf, denen ich entronnen bin und zu denen ich mich in diesem Film
 unwiderstehlich hingezogen f&uuml;hle. Achten Sie vor allem auf Barham Manoochehris
 wundersch&ouml;ne Bilder und auf Joolia Cappleman, wenn sie sagt 'I like Chinese food.'
"</i>

</p>


<h4>Zum Film:</h4>


<p>

<i>"BLEAK MOMENTS (der Titel ist ausserordentlich passend) ist nicht auf herk&ouml;mmliche
Weise unterhaltsam. Das soll aber keineswegs heissen, dass er langweilig oder schwer
anzusehen ist; ganz im Gegenteil, es ist unm&ouml;glich, ihn nicht anzusehen. Nach einer
Pressevorf&uuml;hrung meinte einer meiner Kollegen - und wahrscheinlich ist seine Meinung
weitverbreitet -: "Ich klebte f&ouml;rmlich an der Leinwand. Ich konnte einfach den Blick nicht
abwenden. Aber ich k&ouml;nnte ihn mir nie ein zweites Mal ansehen."<br>

Ich glaube, ich hingegen k&ouml;nnte ihn mir immer wieder ansehen, aber ich kann das Gef&uuml;hl
meines Kollegen verstehen. Dieser Film behandelt den Schmerz und die unendliche
Frustration des Lebens auf eine so grundlegende Weise, dass es m&ouml;glicherweise doch zu
schwer verdaulich sein kann."</i> <small>(Roger Ebert in Chicago Sun Times)</small>

</p>


<h4>Preise</h4>

<p>
<ul>
	  
<li>Filmfestival von Chicago: Bester Film</li>
	  
<li>Internationale Filmfestspiele von Locarno: Bester Film</li>

</ul>
</p>

      
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Bleak Moments</h3>
<p>
Gro&szlig;britannien 1971, 35mm Farbe, 110 min., OF
</p>
<p>

<table summary="">
	    
<tr>
<td>Buch und Regie:</td><td>Mike Leigh</td>
</tr>
            
<tr>
<td>Kamera:</td><td>Bahram Manoochehri</td>
</tr>
            
<tr>
<td>Schnitt:</td><td>Leslie Blair</td>
</tr>
	    
<tr>
<td>Art Director:</td><td>Richard Rambant</td>
</tr>
	    
<tr>
<td>Ton:</td><td>Bob Withey</td>
</tr>
	    
<tr>
<td>Musik:</td><td>Mike Bradwell</td>
</tr> 
	  
</table>

</p>
<p>Eine Produktion von AUTUMN PRODUCTIONS, MEMORIAL ENTERPRISES und dem BFI PRODUCTION BOARD</p>
<p>DARSTELLER:<table summary="">
	    
<tr>
<td>Sylvia</td><td>ANNE RAITT</td>
</tr>
	    
<tr>
<td>Hilda</td><td>SARAH STEPHENSON</td>
</tr>
	    
<tr>
<td>Peter</td><td>ERIC ALLAN</td>
</tr>
	    
<tr>
<td>Pat</td><td>JOOLIA CAPPLEMAN</td>
</tr>
	    
<tr>
<td>Norman</td><td>MIKE BRADWELL</td>
</tr>
	    
<tr>
<td>Pats Mutter</td><td>LIZ SMITH</td>
</tr>
	    
<tr>
<td>Normans Freunde</td><td>MALCOLM SMITH</td>
</tr>
	    
<tr>
<td></td><td>DONALD SUMPTER</td>
</tr>
	    
<tr>
<td>Sylvias Boss</td><td>CHRISTOPHER MARTIN</td>
</tr>
	    
<tr>
<td>Krankengymnastin</td><td>LINDA BECKETT</td>
</tr>
	    
<tr>
<td></td><td>SANDRA BOLTON</td>
</tr>
	    
<tr>
<td></td><td>STEPHEN CHURCHETT</td>
</tr>
	    
<tr>
<td>Supervisor</td><td>UNA BRANDON-JONES</td>
</tr>
	    
</table>
</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>


    
<a name="sec-leighhardlabour"></a>
<h3>Hard Labour</h3>


<p>
 Eine &auml;ltere Hausfrau aus der Arbeiterklasse, deren Ehemann fortw&auml;hrend rumn&ouml;rgelt und die auch von
 anderen f&uuml;r ihre Plackerei keine Anerkennung oder Zuwendung er&auml;hrt. <i>"Mike Leighs d&uuml;sterster Film
 ohne jeden Moment von Komik (wie sie selbst BLEAK MOMENTS in Momenten wie der Szene im
 chinesischen Restaurant besitzt)"</i> <small>(FILMBULLETIN 1.94)</small>

</p>


<h4>Mike Leigh:</h4>

<p>

<i>"Ich wuchs in North Salford, einem Arbeiterviertel &uuml;ber der Arztpraxis meines Vaters auf.
 Als ich dreizehn war, zogen wir um die Ecke in ein Vorot-Reihenhaus. "Hard Labour" wurde
 genau dort, in dieser Gegend von Salford gedreht und beschw&ouml;rt diese beiden
 gesellschaftlichen Welten herauf, die in meinen Filmen immer wieder aufeinandeerstossen.
 Der Film ist zwar nicht ausgeprochen biographisch, doch es tauchen einige biographische_
 Charaktere auf, insbesondere Mrs. Thornley, deren Vorbild eine "wirkliche" Putzfrau war.<br>
 Besonderes Augenmerk verdienen Clifford Kershaw und Alison Steadman bei ihrem
 denkw&uuml;rdigen ersten Auftritt."</i>

</p>



<h4>Zum Film:</h4>


<p>

<i>"Einer der &uuml;ber HARD LABOUR abgegebenen Kommentare, gegen die ich nichts einzuwenden
 habe, ist, dass er von allen Filmen, die ich gemacht habe, derjenige ist, in den die allermeiste
 Arbeit investiert wurde, und bei dem es jemanden gab - das hei&szlig;t Tony Garnett -, der keine
 M&uuml;he scheute, um uns die M&ouml;glichkeit zu geben, den Film genau richtig zu machen. Acht
 Wochen Proben, f&uuml;nf Wochen Drehzeit, die gr&ouml;&szlig;te Besetzung, die ich je hatte - insgesamt 26
 Rollen -; und wir konnten alles machen, was wir wollten. Schwachstellen hat der Film nur
 da, wo meine geringe Erfahrung nicht mit unseren ehrgeizigen Pl&auml;nen Schritt halten konnte.<br>
 Wahrscheinlich konnte der Film genau deswegen inhaltlich so gr&uuml;ndlich und unnachgiebig
 sein, weil es bei den Arbeitsbedinungen keine Kompromisse gab."</i> <small>(Auszug aus einem Interview mit Mike Leigh)</small>

</p>

      
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Hard Labour</h3>
<p>GB 1972, 16 mm, Farbe, 75 min.<br>Eine Produktion von BBC TELEVISION</p>
<p>

<table summary="">
	    
<tr>
<td>Buch und Regie:</td><td>Mike Leigh</td>
</tr>
            
<tr>
<td>Kamera:</td><td>Clifford Kershaw</td>
</tr>
            
<tr>
<td>Schnitt:</td><td>Christopher Rowlands</td>
</tr>
	    
<tr>
<td>Art Director:</td><td>Paul Munting</td>
</tr>
	    
<tr>
<td>Kost&uuml;me:</td><td>Sally Nieper</td>
</tr>
	    
<tr>
<td>Ton:</td><td>Dick Manton</td>
</tr>
	    
<tr>
<td>Produktions-Team:</td><td>Roger Bamford</td>
</tr> 
	    
<tr>
<td></td><td>Roy Baker</td>
</tr> 
	    
<tr>
<td></td><td>Spike Hughes</td>
</tr> 
	    
<tr>
<td></td><td>Irene East</td>
</tr> 
	    
<tr>
<td>Produzent:</td><td>Tony Garnett</td>
</tr> 
	  
</table>

</p>
<p>DARSTELLER:
<table summary="">
	    
<tr>
<td>Mrs. Thornley</td><td>Liz Smith</td>
</tr>
	    
<tr>
<td>Jim Thornley</td><td>Clifford Kershaw</td>
</tr>
	    
<tr>
<td>Ann</td><td>Polly Hemingway</td>
</tr>
	    
<tr>
<td>Edward</td><td>Bernard Hill</td>
</tr>
	    
<tr>
<td>Veronica</td><td>Alison Steadman</td>
</tr>
	    
<tr>
<td>Mrs. Stone</td><td>Vanessa Harris</td>
</tr>
	    
<tr>
<td>Mr. Stone</td><td>Cyril Varley</td>
</tr>
	    
<tr>
<td>Julie</td><td>Linda Beckett</td>
</tr>
	    
<tr>
<td>Naseem</td><td>Ben Kingsley</td>
</tr>
	    
<tr>
<td>Barry</td><td>Alan Erasmus</td>
</tr>
	    
<tr>
<td>June</td><td>Rowena Parr</td>
</tr>
	    
<tr>
<td>Mrs. Rigby</td><td>June Whitaker</td>
</tr>
	    
<tr>
<td>Mrs. Thornleys Freundin</td><td>Paula Tilbrook</td>
</tr>
	    
<tr>
<td>Mr. Shore</td><td>Keith Washington</td>
</tr>
	    
<tr>
<td>Kontrolleur</td><td>Lois Raynes</td>
</tr>
	  
</table>

</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>


    
<a name="sec-leighnutsmay"></a>
<h3>Nuts in May</h3>
      
<p>

<i>"Ein junges &Ouml;ko-Ehepaar will sich beim Campingurlaub erholen, sieht sich jedoch in seinem Bed&uuml;rfnis
nach Ruhe in der unber&uuml;hrten Natur von anderen Menschen gest&ouml;rt, was besonders den Sozialarbeiter-
Ehemann, einen rechthaberischen Prinzipienreiter ersten Ranges, zur Wei&szlig;glut reizt. Ausschlie&szlig;lich
drau&szlig;en gedreht, erweist sich in NUTS IN MAY die Natur blo&szlig; als Projektionsfl&auml;che der St&auml;dter, wobei
die erstmalige Kombination von tragischen und komischen Momenten hier stark zu letzteren tendiert."</i> <small>(FILMBULLETIN 1.94)</small>

</p>



<h4>Mike Leigh:</h4>

<p>
<i>"Der Produzent David Rose forderte mich auf, einen l&auml;ndlichen Film in seiner Heimat Dorset
zu machen, aber irgendwie befinden wir uns hier doch wieder mitten im Kampf der lebhaften
Wohnviertel gegen die selbstgerechten Vororte, auch wenn Eric Allans Steinbrecher die
ausgewogene Einstellung der Bewohner von Purbeck Island gegen&uuml;ber den St&auml;dtern gut
einf&auml;ngt. Besonders bemerkenswert sind Richard Ireson als schlagfertiger Polizist und Oliver
Whites wohl&uuml;berlegter Schnitt."</i>

</p>


<h4>Zum Film:</h4>


<p>
Die Gestalten sind alle verbunden durch die Vorstellung, sie st&uuml;nden miteinander in
Kontakt, aber im Grunde kommunizieren sie &uuml;berhaupt nicht. Sie gehen v&ouml;llig auf in ihrer
Selbstgerechtigkeit und der scheinheiligen &Uuml;berzeugung, sie w&uuml;rden alles verstehen und mit
den anderen Kontakt aufnehmen...<br>
Wahrscheinlich ist NUTS IN MAY deswegen so ansprechend, weil es der einzige Film ist, der
drau&szlig;en spielt und in dem man die Landschaft sieht. Er entstand, weil David Rose zu mir
sagte: "Wenn du einen Film machen willst, fahr nach Dorset und schau dir die Gegend an. Ich
komme von dort..." Also fuhr ich dort hin, aber interessanterweise handelt der Film gar nicht
von Dorset. Was mich faszinierte war, da&szlig; man diese phantastische Landschaft sieht, aber
nur im Kontext mit diesen beschr&auml;nkten, scheinheiligen, engstirnigen St&auml;dtern. Die
Hauptgestalten sind alle St&auml;dter, und wenn sie mit dem "Landvolk" in Kontakt kommen,
machen die Einheimischen sie nur l&auml;cherlich.
</p>

<p>
(Auszug aus einem Interview mit Mike Leigh)
</p>

    
<a name="sec-leighkissdeath"></a>
<h3>The Kiss Of Death</h3>
      
<p>
<i>"Vier junge Leute, zwei P&auml;rchen: Trevor, der tags&uuml;ber als Assistent eines Beerdigungsunternehmers
arbeitet, l&auml;&szlig;t sich von Linda "einfangen", auch wenn es dann Schwierigkeiten mit dem ersten Ku&szlig; und
Eifers&uuml;chteleien gibt. Liebe und Tod und zum ersten Mal die siebziger Jahre in voller Bl&uuml;te
(Plateausohlen!)."</i> <small>(Filmbulletin 1.94)</small>

</p>


<h4>Mike Leigh:</h4>

<p>
<i>"Wieder oben im Norden - diesmal in Oldham. F&uuml;r diesen Film habe ich eine besondere
Schw&auml;che. Jugend, Wahrheit, L&uuml;ge, Liebe, Leben und Tod. Trevor, gl&auml;nzend gespielt von
David Threlfall, besitzt gewisse Z&uuml;ge von mir selbst. Achtung bei der Szene mit dem toten
Baby. Sie ist nicht als Affront gedacht, sondern soll nur den Teil der Arbeit zeigen, der
Leichenbestattern an die Nieren geht. Besondere Aufmerksamkeit verdienen Carl Davis
fr&ouml;hliche Musik (es ist mein erster Film mit Musik) und die k&ouml;stliche Angela Curran, die wie
Betty Boop aussieht."</i>
</p>


<h4>Zum Film:</h4>

<p>Viele Leute schrieben mir, THE KISS OF DEATH sei ihnen wegen der Szene mit dem Tod
des Babys sehr nahe gegangen. In dieser Hinsicht war der Film kontrovers. Und einige
beschwerten sich auch, weil sie nicht glauben konnten, da&szlig; Leichenbestatter wirklich Witze
rei&szlig;en, w&auml;hrend sie die Leichen herrichten; aber nat&uuml;rlich habe ich die Szene eben deswegen
in den Film aufgenommen. Als ich bei meinen Recherchen zum erstenmal Bestatter
aufsuchte, machten die die entsetzlichsten Sachen, um mich zu schockieren. Die Leiche eines
alten Mannes und einer alten Frau... den Rest &uuml;berlasse ich Ihrer Phantasie.</p>


<p>Auszug aus einem Interview mit Mike Leigh</p>
    
<a name="sec-leighwhoswho"></a>
<h3>Who's Who</h3>

<p>
<i>"Zwei junge B&ouml;rsenmakler, Snobs par exellence, laden zu einem exquisiten Dinner, w&auml;hrend ein &auml;lterer
Mitarbeiter derselben Firma sein Steckenpferd pflegt, das Sammeln von Schreiben und Autogrammen
der Prominenz, und schlie&szlig;lich h&auml;usliches Chaos erlebt, als ein Mr. Shakespeare die Katzen seiner Frau
fotografieren will. Ein Ausflug in eine andere Gesellschaftsschicht, der entsprechend satirisch ausgefallen
ist."</i> <small>(Filmbulletin 1.94)</small>
</p>


<h4>Mike Leigh:</h4>

<p>
<i>"Die Vorgabe: Ein Film &uuml;ber das Leben der Ober- und der oberen Mittelklasse. Margareth
Matheson, die gerade die Fernsehfassung meines St&uuml;cks "Abigail s Party" produziert hatte,
wollte, da&szlig; ich mich zur Abwechsluing einmal mit ihrer gesellschaftlichen Herkunft befasse.
Das Ergebis: Ein kleiner Angestellter eines B&ouml;rsenmaklers aus der unteren Mittelschicht,
der eine ungesunde Vorliebe f&uuml;r Ruhm hat. Der Inbegriff eines Snobs, eine meisterhafte
Darbietung von Richard Kane. Achten Sie besonders auf Lavinia Bertrams wunderbar
untertriebenes Kinderm&auml;dchen und den Sonderbeitrag einer gewissen Margaret Thatcher..."</i>
</p>
    
<a name="sec-leighgrownups"></a>
<h3>Grown-Ups</h3>

<p>
<i>"Ein junges Paar bezieht eine neue Wohnung (und stellt fest, dass im Haus nebenan ihr ehemaliger
Religionslehrer wohnt), deren h&auml;uslicher Friede alsbald durch die Schwester der Frau (die noch bei ihrer
Mutter lebt) gest&ouml;rt wird, welche fortw&auml;hrend vorbeikommt, sich einmischt und ihre Aufenthalte immer
mehr ausdehnt, bis die Katastrophe unvermeidlich wird. So gradlinig wie (tragi-)komisch.
"</i> <small>(Filmbulletin 1.94)</small>
</p>


<h4>Mike Leigh:</h4>

<p>
<i>"Wieder Chaos in der Vorstadt. Der Film entstand ohne lange Vorbereitung, um eine L&uuml;cke
bei der BBC zu schlie&szlig;en; deswegen gibt's nur sechs Schauspieler und einen Drehort. Das
f&uuml;hrt zu einer positiven Abkehr vom eher nachl&auml;ssigen Aufbau meiner Filme der siebziger
Jahre zu der dichten, straffen Dramatik, die mir bis dato nur im Theater gelungen war.
<br>
Wunderbares Zusammenspiel des Ensembles! Und achten Sie auf Remi Adefarasins
&uuml;berw&auml;ltigendes Deb&uuml;t mit der Kamera."</i>
</p>


<h4>Zum Film:</h4>

<p>
Der Film ist derartig wirklichkeitsgetreu, da&szlig; es schmerzlich und peinlich ist, zu
beobachten, wie Mike Leigh den Snobismus, der so oft implizit verstanden, aber nie offen
angesprochen wird, vorf&uuml;hrt und zum Ausdruck bingt. Indem Leigh diese verbotenen
Gedanken aufzeigt, hat er einen Film geschaffen, den man nicht ansehen kann, ohne dass
einem die sozialen Umst&auml;nde peinlich w&auml;ren. Er bewirkt aber, dass man sich schuldig f&uuml;hlt,
wenn man &uuml;ber diejenigen lacht, die sich nicht anders verhalten k&ouml;nnen. Oder genauer
gesagt, sie bringen einen zum Lachen, denn Mike Leigh versteht sich nicht als Regisseur im
herk&ouml;mmlichen Sinn. Zwar entwirft er die Handlung, l&auml;sst seinen Schauspielern aber bei der
Interpretation sehr viel Freiraum. Seine Filme sind das Ergebnis stundenlanger Proben, die
auf Improvisation aufbauen. Es ist eine einzigartige Teamarbeit, die zu einer Darstellung
f&uuml;hrt, die der Realit&auml;t beunruhigend nahekommt.
</p>

<p>Nicholas Wapshoott in The Scotsman</p>
    
<a name="sec-leighhome"></a>
<h3>Home Sweet Home</h3>
      
<p>
<i>"Szenen aus dem Leben von drei Postboten: von der Entfremdung der beiden Verheirateten (h&auml;usliche
Zankereien sind an der Tagesordnung) und dem lachenden Dritten, ihrem Kollegen, der mit der einen
Ehefrau ein Verh&auml;ltnis hat, zu der anderen vielleicht bald eines haben wird und in seinem Stoizismus
auch alle Sozialarbeiter (mitsamt ihren ungl&uuml;ckseligen Ideen) &uuml;berstehen wird, die ihn wegen seiner
behinderten Tochter aufsuchen. Einer der komischsten Leigh-Filme, besonders durch das Paar June, die
leidenschaftlich Kitschromane liest, und Harold, der &uuml;berhaupt nichts begreift.
"</i> <small>(Filmbulletin 1.94)</small>
</p>

      
<h4>Mike Leigh:</h4>
      
<p>
<i>"&Uuml;ber das Leben als Eltern (unser zweiter Sohn kam w&auml;hrend der Dreharbeiten zur Welt),
Postboten, Sozialarbeiter - und Sex: Su Elliotts leidenschaftliche June liest die Kitschromane
von Milles &amp; Boon (und redet auch so), die der Film ein wenig auf die Schippe nimmt. Achten
Sie auf Carl Davis' Musik - das London Double Bass Ensemble - wunderbar. Und Sheila
Kelley (die Honky in "Nuts in May"): bedr&uuml;ckend, komisch, tragisch. Als Michael Powell den
Film sah, sagte er mir, ich h&auml;tte jede Falle, die ich mir selbst gestellt hatte, vermieden!"</i>
</p>

      
<h4>Zum Film:</h4>
      
<p>
In HOME SWEET HOME, der faszinierenden Story von drei Postboten, mit denen praktisch
nichts passiert, funktioniert jede geschraubte Wortlosigkeit in einem be&auml;ngstigend
symmetrischen Aufbau. Es gibt Arien von Einsamkeit und bem&uuml;hter Anma&szlig;ung, in denen
tagtr&auml;umerische Ehefrauen stockend ihr Leid klagen. <i>"Es ist wie ein Band aus Stahl, das
sich fest um meine Schl&auml;fen legt"</i>. Es gibt leidenschaftslose Duette, in denen pantoffel-
heldische Ehem&auml;nner noch weiter unter den Daumen geraten. <i>"H&ouml;r auf, auf dem Teppich
herumzutrampeln. Du zerdr&uuml;ckst ihn."</i> Es gibt lange mozartsche Ensembles, in denen fast
jeder nichts sagt.<br>
Wenn Sie sich Cosi fan tutte vorstellen k&ouml;nnen, jedoch ohne Musik und ohne Worte, daf&uuml;r
aber mit einer Ausstattung und Kost&uuml;men, die aus den spie&szlig;igsten Stoffen und M&ouml;beln
besteht, die der Mensch nur produzieren kann, dann haben Sie einen Film von Mike Leigh.
</p>

<p>Clive James in "The Observer"</p>
    
<a name="sec-leighmeantime"></a>
<h3>Meantime</h3>
      
<p>
<i>"Eine arbeitslose Familie in einem zu kleinen Apartment im Londoner East End: aus dem t&auml;glichen
Zeittotschlagen fl&uuml;chtet Vater Frank in die Resignation, sein Sohn Mark in den Zynismus, w&auml;hrend
dessen etwas zur&uuml;ckgebliebener j&uuml;ngerer Bruder Colin wenig erfolgreich versucht, eine Beziehung zu
einem M&auml;dchen anzukn&uuml;pfen, und mit einem Skinhead herumh&auml;ngt. Nach elf Jahren ausschlie&szlig;licher
Arbeit f&uuml;r die BBC Leighs erste unabh&auml;ngige Produktion, entstanden f&uuml;r Channel Four, ein Film &uuml;ber
die problematischen Konsequenzen des Gutgemeinten."</i> <small>(Filmbulletin 1.94)</small>
</p>

      
<h4>Mike Leigh:</h4>
      
<p>
<i>"Leider entstand der Film zu fr&uuml;h in der Geschichte von Channel Four, um als 
35mm-Spielfilm gedreht werden zu k&ouml;nnen. Allerdings kommt er 1993 in die Kinos. Er handelt von
Arbeitslosigkeit und ist der erste von drei eher eindeutig "politischen" Filmen. Barbaras
Projekt, H&auml;user zu streichen, steht f&uuml;r die Sinnlosigkeit aller kurzfristigen
Arbeitsbeschaffungsma&szlig;nahmen. Achten Sie auf Roger Pratts scharfe Bilder, Gary Oldman
im Fass und Peter Wights Satz: "Windows are good"."</i>
</p>

      
<h4>Zum Film:</h4>
      
<p>Die Schauspieler in MEANTIME spielen nicht sich selbst. Dies ist Charakterdarstellung im
besten Sinne des Wortes. Die F&auml;higkeit der Schauspieler, aus dem Charakter heraus zu
entstehen, ist wesentlich bei der improvisierten Evolution eines Filmes, dessen Bestandteile
auch Leidenschaft und Gewalt sind, wie dies beim MEANTIME manchmal der Fall ist. Aber
der Film selbst ist nat&uuml;rlich nicht "improvisiert". Er ist extrem strukturiert und ebenso
gr&uuml;ndlich durchdacht wie alle Theaterst&uuml;cke von Mike Leigh.
</p>
    
<a name="sec-leighjuly"></a>
<h3>Four Days in July</h3>
      
<p>
<i>"Vier Tage in Nordirland, Alltagsszenen aus dem Leben eines katholischen (der Mann wurde von
Soldaten-Kugeln verkr&uuml;ppelt) und eines protestantischen Paares (der Mann ist bei der Armee) - am Ende
des Films bringen die beiden Frauen im selben Krankenhauszimmer ihre Babies zur Welt. Leighs
"normalste" Protagonisten - ohne die sonst kennzeichnenden Spleens, allerdings auch nicht geeignet,
dieselben Reaktionen beim Publikum auszul&ouml;sen, so da&szlig; Stephen Rea mit seinem Monolog Ober den
"Ulster Man" am st&auml;rksten im Ged&auml;chtnis bleibt - auch weil es der einzige komische Moment im Film
ist.
"</i> <small>(Filmbulletin 1.94)</small>
</p>

      
<h4>Mike Leigh:</h4>
      
<p>
<i>"Mein letzter BBC-Film. Und mein erster in einer Fremdsprache - erg&ouml;tzen Sie sich an dem
wunderbar undurchdringlichen David Coyle als Mickey! Alles gedreht auf den Stra&szlig;en von
East und West Belfast! Mein erster Versuch, das Wesen jeder Seite so einzufangen, wie ich es
empfand. Und ein Ausdruck meiner Hoffnungslosigkeit angesichts der unvermeidbaren
Endlosigkeit der Schwierigkeiten. Besondere Aufmerksamkeit verdienen: der Long Kesh
Double-act von Stephen Rea, der mich &uuml;berhaupt dazu anregte, den Film zu machen, und
Shane Connaughton, die sp&auml;ter "My Left Foot" schrieb. Und Rachel Portmans bewegende
Hurdy-Gurdy-Musik."</i>
</p>

      
<h4>Zum Film:</h4>
      
<p>1983 schickte der Produzent Kenith Trodd Mike Leigh nach Ulster, um dort etwas auf die
Beine zu stellen...<br>
Wie gehabt passiert wenig bei Mike Leigh. Zwei Frauen stehen kurz vor der Niederkunft:
Colette, eine Katholikin, die in der Falls Road lebt, und Lorraine, die Gattin eines Loyalisten
in der britischen Armee. Im Verlauf der vier Tage w&auml;hrenden Veranstaltungen der
protestantischen Boyne-Schlacht sehen wir einzelne Episoden aus ihrem Leben... Nie spricht
jemand &uuml;ber Politik: Das Leben der einzelnen Figuren ist derma&szlig;en mit den "Problemen"
verwoben, da&szlig; das gar nicht n&ouml;tig ist.
</p>
<p>
Sean French in "The Sunday Times"
</p>
    
<a name="sec-leighhopes"></a>
<h3>High Hopes</h3>
      
<p>
<i>"Drei Paare im Zentrum von London: Cyril und Shirley, die "Alternativen", die f&uuml;r sich eine Nische in
Thatchers England gefunden haben, Cyrils Schwester Valerie, deren Verf&uuml;hrungs- und Kochk&uuml;nste bei
ihrem Ehemann auf wenig Reaktion sto&szlig;en und die gerne so w&auml;re, wie die Yuppies Boothe-Braine, die
Nachbarn ihrer alten Mutter. Leighs erster 35 mm-Spielfilm seit BLEAK MOMENTS: das Dumpfe
absch&uuml;ttelnd, pr&auml;sentiert er mit Cyril und Shirley zwei Figuren, die der nahezu uneingeschr&auml;nkten
Sympathie des Zuschauers sicher sein d&uuml;rfen.
"</i> <small>(Filmbulletin 1.94)</small>
</p>

      
<h4>Mike Leigh:</h4>
      
<p>
<i>"Endlich ein Spielfilm! Nach "Four Days in July" der dritte politische Film, obwohl hier &uuml;ber
die Politik nicht wirklich geredet wird. Dann geht es auch um alternde Eltern, mein Vater
war kurz zuvor gestorben. Und noch immer liegen die fr&ouml;hlichen Aussteiger im Zwist mit den
neurotischen Vorst&auml;dtern. Achten Sie auf Lesley Manvilles Satz "Do you have all your
original features?", auf Lindy Hemmings genau beobachtete Kost&uuml;me und auf Andrew
Dixons Musik, die ihm zwei Preise eintrug. (Und dazu ein weiterer Auftritt von Thatcher!)"</i>
</p>

      
<h4>Inhalt:</h4>
      
<p>HIGH HOPES spielt 1988 um und in King's Cross, dem Zentrum Londons und den Vororten.
Der Film verfolgt das Leben einiger nicht miteinander zu vereinbarender Personen, die im
Leben der anderen auftauchen und wieder verschwinden.<br>
Wayne hat sein Zuhause nach einem Streit verlassen. Cyril w&uuml;rde gerne der k&ouml;niglichen
Familie mit einem Maschinengewehr begegnen. Rupert und Laetitia Boothe-Brain
bevorzugen Yuppie-Sexspiele. Valerie gelingt es nicht, ihren Ehemann mit der Vorstellung
zu reizen, er sei Michael Douglas und sie eine Jungfrau. Mrs. Bender wird aus ihrem Haus
ausgeschlossen. Die Nachbarn kritisieren sie, weil sie ein ganzes Haus f&uuml;r sich allein
beansprucht. Cyrils Freundin Shirley m&ouml;chte eine Familie gr&uuml;nden, wird aber darin von
Cyril nicht ermutigt, denn der meinte, die Welt m&uuml;sse von weiteren Babies verschont
bleiben, bis jeder seinen Job gefunden habe, einen Platz zum Leben und genug zu essen.
</p>

      
<h4>Preise:</h4>
      
<p>
<ul>
	  
<li>1988: Filmfestival von Venedig: Preis der internationalen Filmkritik</li>
	  
<li>1989: European Film Awards: Beste Schauspielerin (Ruth Sheen)</li>
	  
<li>British Academy of Film and Television Arts (BAFTA): (Edna Dor&eacute;)</li>
	
</ul>

</p>

    
<a name="sec-leighlifesweet"></a>
<h3>Life is Sweet</h3>
      
<p>
<i>"Essen als zentrale Metapher bei der Schilderung des Versuchs einer Arbeiterfamilie, im 
Thatcher-England zu &uuml;berleben. Die St&auml;rke der Charaktere nimmt zu, die Karikatur ab.
"</i> <small>(Filmbulletin 1.94)</small>
</p>

      
<h4>Inhalt:</h4>
      
<p>Natalie und Nicola sind Zwillinge. Natalie ist
Installateurin, Nicola ist arbeitslos. Die heitere,
tatkr&auml;ftige Natalie geht oft aus, w&auml;hrend die
magers&uuml;chtige, aggressive und verwirrte Nicola
das Haus nie verl&auml;&szlig;t.<br>
Ihr Vater Andy tr&auml;umt davon, ein eigenes Gesch&auml;ft
aufzubauen, und l&auml;sst sich von seinem hinterlistigen 
Trinkkumpanen Patsy dazu &uuml;berreden,
eine verfallene Imbissbude auf R&auml;dern zu kaufen.
Die Mutter Wendy arbeitet Teilzeit in einem
Bekleidungsgesch&auml;ft f&uuml;r Kinder. Als Aubrey, ein
exzentrischer Freund der Familie, ein Restaurant
er&ouml;ffnet, eilt Wendy ihm zu Hilfe. Aber am ersten
Abend im Lokal betrinkt sich Aubrey sinnlos.
Tags&uuml;ber erh&auml;lt Nicola heimlich Besuch von ihrem
Freund. Er hat es satt, ihre perversen sexuellen
Gel&uuml;ste zu befriedigen, und sehnt sich nach
intelligenter Unterhaltung, die aber Nicola
&uuml;berfordert.<br>
W&auml;hrend Nicola auf den v&ouml;lligen Zusammenbruch
zusteuert und Andy bei der Arbeit verletzt wird,
erweist sich Wendy als Anker im Sturm...
</p>

      
<h4>Zum Film:</h4>
      
<p>Nat&uuml;rlich ist LIFE IS SWEET kein apolitischer Film - es geht um die Politik des Privaten.
Wenn ich hier nicht &uuml;ber Mrs. Thatcher spreche, dann deswegen, weil ich eine andere
Perspektive gew&auml;hlt habe. Das heisst nicht, dass das Thatcher-Thema begraben ist. Es w&auml;re
aber langweilig, in jedem Film wieder neu dar&uuml;ber zu reden... Ich arbeite sehr intuitiv...
Bevor wir zu drehen anfangen, arbeite ich zuerst einmal ganz intuitiv mit den Schauspielern
- was bei "Life is Sweet" ungef&auml;hr drei Monate in Anspruch nahm. In dieser
Vorbereitungszeit entstehen die Personen, ihre Beziehung zueinander und alles andere; sie
entwickeln sich aus endlosen Nachforschungen, Diskussionen und vor allem
Improvisationen. Die Beziehungen der Familienmitglieder wurden tats&auml;chlich durchlebt. Es
ist schon etwas dran an Ihrer Feststellung, da&szlig; keine objektiven Erkl&auml;rungen daf&uuml;r
abgegeben werden, warum eigentlich alles so ist, wie es ist. Trotzdem w&uuml;rde ich doch sehr
hoffen, dass das, was sich vor Ihren Augen abspielt, als wirklich erscheint. Die Schauspieler
sprechen nicht einfach nur einen Text. W&auml;hrend der langen Probezeit etablieren sich
zwischen den Personen, die spielen, tats&auml;chliche Beziehungen.
</p>
<p>
(Auszug aus einem Interview mit Mike Leigh im Berlinaletip)
</p>

      
<h4>Preise:</h4>
      
<p>
<ul>
	  
<li>American Critics Association: Bester Film, Beste Schauspielerin (Alison Steadman), Beste Nebendarstellerin (Jane Horrocks)</li>
	  
<li>Los Angeles Film Critics' Award: Beste Schauspielerin (Jane Horrocks)</li>
	
</ul>

</p>
    
<a name="sec-leighnaked"></a>
<h3>Naked</h3>
      
<p>
<i>"Eine Nacht lang streunt Johnny, ein Gossenphilosoph, durch London, erlebt verschiedene
bizarre Begegnungen und traktiert andere mit seinem wortgewaltingen Nihilismus. Seit
ABIGAIL'S PARTY wurde kein Leigh-Film so sehr von einer einzigen Figur dominiert: eine neue
Form und Stufe der Stilisierung mit einem "Helden", der keinen Zuschauer gleichg&uuml;ltig l&auml;sst."</i> <small>(Filmbulletin 1.94)</small>

</p>

      
<h4>Mike Leigh:</h4>
      
<p>
<i>"Meine Gef&uuml;hle gegen&uuml;ber NAKED sind genauso ambivalent wie meine Gef&uuml;hle gegen&uuml;ber
unserer chaotischen Welt im ausgehenden 20. Jahrhundert und vermutlich so ambivalent
wie der Film selbst. Ich hoffe, der Film ist ebenso komisch wie traurig, ebenso sch&ouml;n wie
h&auml;sslich, so mitf&uuml;hlend wie verabscheuungsw&uuml;rdig, so verantwortungsvoll wie arnachisch."</i>
</p>

      
<h4>Inhalt:</h4>
      
<p>Der Film befasst sich mit einer Seite Londons, die die meisten Leute nicht wahrhaben wollen.
Johnny ist der Inbegriff des Antihelden unseres Jahrzehnts - kalt, zynisch und unmoralisch,
dann wieder besorgt und leidenschaftlich. Gewaltt&auml;tig und sanft, komisch und tragisch
zugleich bewegt er sich durch sein Leben.
</p>

      
<h4>Preise:</h4>
      
<p>
<ul>
	  
<li>1993: Internationale Filmfestspiele Cannes: Beste Regie (Mike Leigh), Bester Schauspieler
(David Thewlis)</li>
	
</ul>

</p>
    
<a name="sec-leighsecrets"></a>
<h3>Secrets And Lies<br>
<small>Lustspiel vom geteilten Leid</small>
</h3>


<p>
Eine Standardgeschichte, maulten in Cannes
ein paar Siebengscheite bei "salade ni&ccedil;oise", eine seifige soap. 
Fast wie in einem Roman von Dickens
werde in SECRETS AND LIES eine Adoptivwaise auf
der Suche nach der leiblichen Mutter f&uuml;ndig und
gl&uuml;cklich. Dem Zwang zum Happy-End sei da bis
zur Ersch&ouml;pfung nachgelebt. Offenbar gehe jetzt
die Vereinheitlichung der Geschm&auml;cker bereits von
den Autoren aus; das geschehe obendrein auf freiwilliger 
Basis, auch ohne den alten Erfolgsdruck
von Investorenseite.</p>

<p>
Wer es so sieht, konnte sich auf Robert Altman berufen, 
der an der C&ocirc;te d'Azur ausrief, nicht l&auml;nger
die Autoren machten die Filme, sondern die Banken. 
Dustin Hoffman, Al Pacino und Francis Coppola
pflichteten bei. Wie nach Absprache geb&auml;rdeten
sich bew&auml;hrte Systemsupporter als die h&auml;rtesten
Kritiker &uuml;berbordender Gesch&auml;ftemacherei. Und
es ist ja wahr, vor lauter Zahlen sehen die 
Umsatzgewaltigen des Weltkinos keinen einzigen realen
Bilanzwert mehr, und ihr asthmatisches Chaotentum
l&auml;hmt alle Kreativit&auml;t. Allm&auml;hlich wird es
ruchbar, vor lauter Erfolgshetze k&ouml;nnte (mindestens) 
der amerikanische Film in die tiefste gestalterische 
Krise seit den Sechzigern gleiten.</p>


<p>
Der neue Film von Mike Leigh (einem Engl&auml;nder) 
l&auml;sst sich als &laquo;pitching&raquo; leicht in h&ouml;chstens 25
W&ouml;rter fassen. So erz&auml;hlt, setzt er tats&auml;chlich eine
Adoptivwaise (sie ist schwarz) in Szene, die auf
der Suche nach der leiblichen Mutter (sie ist weiss)
f&uuml;ndig und gl&uuml;cklich wird. Die Geschichte ist eine
wirkliche Story - simpel, am&uuml;sant und komisch -
und malt in nichts schwarz. Das Publikum verl&auml;sst
den Saal frohgemut. LIFE IS SWEET, das Leben ist
s&uuml;ss. So hiess 1991 eine Kom&ouml;die Leighs, mit der
sich diese neueste vorteilhaft zusammenbringen l&auml;sst.</p>


<p>
<b>Sich f&uuml;hlen wie Prinzessin Margret</b>
</p>

<p>
Doch wird da kein wohlfeiler, unglaubw&uuml;rdiger Trost 
ausgeteilt. Noch steht zu bef&uuml;rchten, es
ziehe jetzt den Autor unter die Stimmungsdiscounter. 
Man braucht sich bloss zu erinnern, was
f&uuml;r einen scharfen Kontrast NAKED 1993 zum 
vorausgegangenen LIFE IS SWEET bildete, und man
muss ausserdem sehen, wie das gleiche jetzt wieder 
geschieht, im Verh&auml;ltnis zu SECRETS AND LIES.
Die Protagonisten jenes herben derben Zeitst&uuml;cks
von 1993 waren in jeder Hinsicht aus- und abgebrannt. 
Sie f&uuml;hlten sich samt und sonders &laquo;wie Prinzessin Margret&raquo;. 
Es war so sehr mit grimmigem Pessimismus unterlegt, wie das Dasein der
Unterklasse jetzt wieder betont en rose, als fraglos
lebenswert erscheint.</p>

<p>
Schliesslich ist im wirklichen Leben auch niemand 
nur immer zutodebetr&uuml;bt, oder er jauchzt
ohne Unterlass himmelhoch auf. Anlass zur
Selbstaufgabe wie in NAKED findet sich jederzeit,
desgleichen aber Grund zu neuer Hoffnung wie in
SECRETS AND LIES oder LIFE IS SWEET. Sichtlich
blickt Leigh bewusst &uuml;ber den einzelnen Titel hinaus 
und thematisiert von einem Mal zum folgenden, 
wie wir alle schwanken zwischen Zuversicht
und Verzweiflung, Tatkraft und Depression. Seine
Filme erfassen eine Stimmungslage bis ins feinste,
doch unterlassen sie es tunlichst, sie zur jeweils
umfassenden, einzig g&uuml;ltigen Atmosph&auml;re zu erheben.
Genommen wird die Gef&uuml;hlstemperatur nur einer einzelnen
Gruppe von Individuen aufs
Mal. Aber der Wert muss aufs Zehntelsgrad genau
stimmen.</p>

<p>In LIFE IS SWEET und SECRETS AND LIES bieten
Familien Behausung und Behaustheit und schaffen 
damit Raum f&uuml;r die unerl&auml;sslichen Sch&uuml;be von
Optimismus zwischen den unvermeidlichen Anwandlungen 
von Melancholie. Bei einiger Skepsis
bejahen beide Filme, auf die Dauer sei es im
Dunstkreis der N&auml;chsten m&ouml;glich, mehr Probleme
zu l&ouml;sen, als darin erzeugt w&uuml;rden. Mindestens
wird solange, als der konkrete Einzelfall nicht das
Gegenteil beweist, angenommen, eine derartige
Chance sei gegeben. Nichts von all dem kommt in
NAKED auch nur vor. Dort wird der Held gerade in
dem &uuml;bertragenen Sinn als "nackt" hingestellt, als
er allein ist und draussen auf der Strasse.
</p>


<p>
<b>Aufgefangene Abst&uuml;rze</b>
</p>


<p>Nicht, dass alles zum Besten st&uuml;nde im engen Zirkel 
der Angeh&ouml;rigen. Doch ist draussen auf
der Strasse kaum viel Besseres anzutreffen. 
SECRETS AND LIES f&uuml;hrt vor, wie sich Einsamkeit und
Verzweiflung mitunter &uuml;berwinden lassen (freilich
ohne Geew&auml;hr). Cynthia hat reichlich Anlass, sich
&laquo;wie Prinzessin Margret&raquo; zu f&uuml;hlen, wo doch die
Umst&auml;nde sie pl&ouml;tzlich dr&auml;ngen, ihrer lange 
verneinten, verheimlichten, verdr&auml;ngten Tochter 
Hortense sp&auml;t noch eine Mutter zu werden. 
Erschwerend kommt hinzu, dass es den f&auml;lligen Schritt
&uuml;ber jedermanns milde, aber sp&uuml;rbare Rassenvorurteile 
hinweg zu tun gilt; und das heisst, dass
auch die Heldin selbst &uuml;ber ihren Schatten springen muss.</p>


<p>
Dabei w&auml;re es leicht, sich die Schwierigkeit
einfach vom Leih zu halten. Das ist ja schon einmal 
geschehen, als Cynthia ihr Kind zur Adoption
freigab. Nur mit M&uuml;he entscheidet sie sich diesmal
f&uuml;r die kollektive L&ouml;sung und unterbreitet das
Problem ihren Lieben. Die (zun&auml;chst) verheerenden 
Folgen quittiert ihr Bruder Maurice mit dem
Kernsatz: &laquo;We all suffer, why don't we share the suffering&raquo;. 
Ja, dazu sind Familien wohl da, um Abst&uuml;rze aufzufangen. 
Leigh's Figuren halten's, wie's Familien immer tun, 
wenn ihnen etwas gelingt, zum Beispiel die Eingliederung 
der bis dahin v&ouml;llig fremden und obendrein verkehrt gef&auml;rbten 
Hortense.</p>

<p>Sie geben sich der Heiterkeit und der allseitigen 
Sympathie hin, nicht sofort, etwas sp&auml;ter.
SECRETS AND LIES ist ein Lustspiel mit tieferer Bedeutung, 
und ein solches bildet einen graden engen Pfad. Bis zuletzt 
weicht der Film kein Fingerbreit von ihm ab.</p>


<p>Pierre Lachat</p>

    
<a name="sec-leighcareergirls"></a>
<h3>Career Girls<br>
<small>In die Jahre kommen</small>
</h3>


<p>&Auml;lter werden, was bedeutet das genau? Wie
ver&auml;ndert sich dar&uuml;ber das Verh&auml;ltnis zur Welt?
Soll man von einem Reifeprozess sprechen oder
eher von einer Ein&uuml;bung des Realit&auml;tsprinzips?
Oder trifft es weder das eine noch das andere? Auf
diese grossen Fragen findet Mike Leigh in seinem
neuesten Film eine Antwort, die sich kleinteilig
aus einer Reihe pr&auml;zis beobachteter Details zusammensetzt 
und einem doch viel eher eine Vorstellung von Geschichte
vermittelt als die opulenten
&laquo;period pictures&raquo; mit ihrem retro-chicen Rekonstruktionswahn.</p>


<p>Hannnah und Annie, die sich in ihrer Londoner Studienzeit gemeinsam mit
einer weiteren Kommilitonin eine Wohnung teilten, sehen sich
nach sechs Jahren zum ersten Mal wieder. &laquo;London hat sich sehr ver&auml;ndert, 
in mancherlei Hinsicht aber auch gar nicht&raquo;, fasst Annie, 
die die Stadt seit dem Ende ihres Studiums nicht mehr 
besucht hat, sinngem&auml;ss ihre ersten Eindr&uuml;cke 
zusammen. Und aus einem Mischungsverh&auml;ltnis von
Vergehendem und Bleibendem bildet sich im weiteren 
auch die Geschichtsphilosophie des Films heraus. 
Hinter den Gespr&auml;chen der beiden circa
dreissigj&auml;hrigen Frauen, den R&uuml;ckblenden und den
Wiederbegegnungen mit alten St&auml;tten und Bekannten treten zwei Biographien hervor, die zwar
einigen Ballast, aber auch Kostbares hinter sich gelassen haben und mit manchen Gespenstern der
Vergangenheit immer noch k&auml;mpfen. Man ist jetzt
sittsamer gekleidet, hat ganz ordentliche Jobs, liest
etwas weniger, und Annie pr&auml;sentiert sich im Vergleich 
zu fr&uuml;her auch als deutlich gefestigtere Pers&ouml;nlichkeit. Aber ihren endg&uuml;ltigen Platz im
Leben hat bisher keine der Frauen gefunden. Vieles
ist noch Provisorium: Annie will ihren Job wechseln, Hannah beizeiten die Wohnung, und dem
Mann f&uuml;rs Leben ist keine der beiden bisher begegnet, so dass in wichtigen Zukunftsfragen
nach wie vor Emily Bronte als Orakel konsultiert werden muss.</p>


<p>Der grossen Geste des Bilanzziehens enth&auml;lt
sich der Film. Hochfahrende Pl&auml;ne, &uuml;ber deren erfolgte 
oder nicht erfolgte Realisierung man sich
jetzt Rechenschaft ablegen m&uuml;sste, haben Annie
und Hannah nie gehabt. Dass ihr Studium (Psy-
chologie beziehungsweise Anglistik) nichts mit
dem sp&auml;teren Beruf zu tun haben w&uuml;rde, scheint
nach den englischen Verh&auml;ltnissen von vornherein
klar gewesen zu sein. Daher beschw&ouml;ren sie weder in 
Katerstimmung das "Paradies der Jugend",
noch k&ouml;nnen sie erhaben vom Standpunkt des
&laquo;Es-Geschafft-Habens&raquo; auf ihre bescheidenen Anf&auml;nge 
zur&uuml;ckblicken. Ihr Leben ist irgendwo dazwischen verlaufen.</p>

<p>
Nachdem Mike Leigh mit der Hauptfigur
von NAKED eine Art zynischen Antichristen geschaffen 
hatte und die damit verbundene Katharsis ihn dazu brachte, in SECRETS AND LIES 
auf etwas zu anr&uuml;hrende Weise auf Identifikation zu
setzen, w&auml;hlt er in CAREER CIRLS die Halbdistanz
als Erz&auml;hlhaltung. Was als Mittelwert zu verstehen
ist, denn die Abst&auml;nde variieren zwischen &laquo;ganz
nah&raquo; und &laquo;ganz weit weg&raquo;.</p>

<p>
Leigh h&auml;lt seine Schauspieler dazu an, die
Charaktere grotesk zu &uuml;berzeichnen. Annie, von
einem handgrossen Gesichtsekzem heimgesucht,
taumelt als schwer angeschlagenes Psycho-B&uuml;ndel
durch die R&uuml;ckblenden. Ihre Gesten sind fahrig
und die Bewegungen linkisch, es scheint immer,
als ob der geringste Anlass gen&uuml;gte, sie v&ouml;llig aus
der Bahn zu werfen. Hannah steht permanent unter Strom 
und redet pausenlos. Und wenn dann noch das stotternde, 
st&auml;ndig mit den Augen zwinkernde Riesenbaby Ricky zu dem Duo st&ouml;sst, ist
die Kakaphonie komplett.</p>

<p>
Aber diesen V-Effekt holt der Film wieder
ein, indem er bis ins tiefste Innere seiner Protagonisten vordringt. 
Und vielleicht besteht in dieser Form des Kompensationsgesch&auml;fts zurzeit auch
die einzige M&ouml;glichkeit, die Psychologie cineastisch zu rehabilitieren. In der eindrucksvollsten
Szene von CAREER GIRLS k&auml;mpft Annie gleichzeitig
mit ihrer Scham, ihrem Bekenntnisdrang, ihrem
feministischen &Uuml;ber-Ich und der Indifferenz ihres
Partners. Die an einem Vater-Komplex Leidende beginnt z&ouml;gerlich von ihren
Vergewaltigungsphantasien zu berichten, in denen sie sich von
einer Schar m&auml;nnlicher Voyeure umringt sieht. Dabei
unterbricht sie sich selbst und wird zu einem 
Kommentator in eigener Sache, wohl wissend,
dass sie, wenn auch nur imaginativ, an einer
Grundfeste des Feminismus r&uuml;hrt: dem &laquo;No
Means No&raquo; als eindeutige Grenzziehung gegen&uuml;ber 
m&auml;nnlichen Begehrlichkeiten. Vergeblich
bem&uuml;ht sich Annie, ihre verschiedenen Ichs unter
einen Hut zu bringen. Und als ihrem Freund auf 
ihre Offenbarung nichts anderes einf&auml;llt als lapidar 
etwas in der Art von "Wenn dir damit geholfen ist, 
frage ich meine Kumpels mal, ob sie uns
beim Sex zuschauen w&uuml;rden" zu entgegnen, und
er damit zus&auml;tzlich zu der Ebene der Imagination
und der des Politisch-Moralischen noch die des
Realen einbringt, verkompliziert sich die Lage
hoffnungslos.</p>

<p>
In solch eine Krisensituation wird die Figur
der Hannah nicht gebracht. Ihre sprachm&auml;chtige
Souver&auml;nit&auml;t, die sie jeder Lage gewachsen erscheinen 
l&auml;sst, bleibt im Kern unangetastet. Lediglich in einer 
Dialog-Passage gesteht sie einmal ein,
dass sie es als eine St&auml;rke Annies ansieht, verletzbar zu sein, 
immer wieder ohne Vorbehalt in neue
Beziehungen zu gehen, w&auml;hrend ihre eigene vermeintliche St&auml;rke 
nur der Panzer ist, der nach jeder Entt&auml;uschung undurchdringlicher wird.</p>

<p>
&laquo;Beste Nebenrollen&raquo; dieses Typs hat Katrin
Cartlidge schon in BEFORE THE RAIN und BREAKING
THE WAVES gespielt - nur als Drogens&uuml;chtige in
Mike Leighs NAKED durfte sie bisher die Fassung
verlieren -, und es w&auml;re schade, wenn sie auf das
Rollenfach der beherrschten, leidenschaftslosen
Frau festgelegt w&uuml;rde.</p>


<p>Jan Pehrke</p>

    
<a name="sec-leighfilmography"></a>
<h3>Filmographie</h3>
      

<p>
<table summary="">

<tr>
<td>1971</td><td>BLEAK MOMENTS</td>
</tr>

<tr>
<td>1972</td><td>THE MUG'S GAME (BBC)</td>
</tr>

<tr>
<td></td>    <td>HARD LABOUR (BBC-Play for Today)</td><td>Mike Leigh-WEEKEND</td>
</tr>

<tr>
<td>1975</td><td>NUTS IN MAY (BBC-Play for Today)</td><td>Mike Leigh-WEEKEND</td>
</tr>

<tr>
<td></td>    <td>THE PERMISSIVE SOCIETY</td>
</tr>

<tr>
<td></td>    <td>THE BIRTH OF THE 2001 F.A. CUP FINAL GOALIE (Kurzfilm, BBC)</td>
</tr>

<tr>
<td></td>    <td>OLD CHUMS (Kurzfilm, BBC)</td>
</tr>

<tr>
<td></td>    <td>PROBATION (Kurzfilm, BBC)</td>
</tr>

<tr>
<td></td>    <td>A LIGHT SNACK (Kurzfilm, BBC)</td>
</tr>

<tr>
<td></td>    <td>AFTERNOON (Kurzfilm, BBC)</td>
</tr>

<tr>
<td>1976</td><td>KNOCK FOR KNOCK (BBC)</td>
</tr>

<tr>
<td>1977</td><td>THE KISS OF DEATH (BBC-Play for Today)</td><td>Mike Leigh-WEEKEND</td><td>Mike Leigh-WEEKEND</td>
</tr>

<tr>
<td></td>    <td>ABIGAIL'S PARTY (BBC)</td>
</tr>

<tr>
<td>1978</td><td>WHO'S WHO (BBC-Play for Today)</td>
</tr>

<tr>
<td>1980</td><td>GROWN-UPS (BBC)</td><td>Mike Leigh-WEEKEND</td>
</tr>

<tr>
<td>1982</td><td>HOME SWEET HOME (BBC-Play for Today)</td><td>Mike Leigh-WEEKEND</td>
</tr>

<tr>
<td>1983</td><td>MEANTIME (Channel Four)</td>
</tr>

<tr>
<td>1987</td><td>THE SHORT AND CURLIES (Kurzfilm)</td>
</tr>

<tr>
<td></td>    <td>FOUR DAYS IN JULY (BBC)</td><td>Mike Leigh-WEEKEND</td>
</tr>

<tr>
<td>1988</td><td>HIGH HOPES</td><td>Mike Leigh-REIHE</td>
</tr>

<tr>
<td>1990</td><td>LIFE IS SWEET</td><td>Mike Leigh-REIHE</td>
</tr>

<tr>
<td>1991</td><td>THE LONDON FILM FESTIVAL TRAILER</td>
</tr>

<tr>
<td>1992</td><td>A SENSE OF HISTORY</td>
</tr>

<tr>
<td>1993</td><td>NAKED</td>
</tr>

<tr>
<td>1995</td><td>SECRETS AND LIES</td><td>Mike Leigh-REIHE</td>
</tr>

<tr>
<td>1997</td><td>CAREER GIRLS</td><td>Mike Leigh-REIHE</td>
</tr>

</table>

</p>


<h4>Theater (in Auswahl)</h4>

<p>
<table summary="">

<tr>
<td>1965</td><td>THE BOX PLAY</td>
</tr>

<tr>
<td>1966</td><td>MY PARENTS HAVE GONE TO CARLISLE</td>
</tr>

<tr>
<td></td><td>THE LAST CRUSADE OF THE FIVE LITTLE NUNS</td>
</tr>

<tr>
<td>1967</td><td>NENAA</td>
</tr>

<tr>
<td>1968</td><td>INDIVIDUAL FRUIT PIES</td>
</tr>

<tr>
<td></td><td>DOWN HERE &amp; UP THERE</td>
</tr>

<tr>
<td>1969</td><td>BIG BASIL</td>
</tr>

<tr>
<td></td><td>GLUM VICTORIA AND THE LAD WITH SPECS</td>
</tr>

<tr>
<td></td><td>EPILOGUE</td>
</tr>

<tr>
<td>1970</td><td>BLEAK MOMENTS</td>
</tr>

<tr>
<td>1971</td><td>A RANCID PONG</td>
</tr>

<tr>
<td>1973</td><td>WHOLESOME GLORY</td>
</tr>

<tr>
<td></td><td>THE JAWS OF DEATH</td>
</tr>

<tr>
<td></td><td>DICK WHITTIGTON AND HIS CAT</td>
</tr>

<tr>
<td>1974</td><td>BABES GROW OLD</td>
</tr>

<tr>
<td></td><td>THE SILENT MAYORITY</td>
</tr>

<tr>
<td>1977</td><td>ABIGAIL'S PARTY</td>
</tr>

<tr>
<td>1979</td><td>ECSTASY</td>
</tr>

<tr>
<td>1981</td><td>GOOSE PIMPLES</td>
</tr>

<tr>
<td>1988</td><td>SMELLING A RAT</td>
</tr>

<tr>
<td>1989</td><td>GREEK TRAGEDY</td>
</tr>

</table>
</p>

    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><a href="#sec-intro">Vorwort: Mike Leigh - Zwischen Kino und Fernsehen</a></small>
</li>
<li>
<small><a href="#sec-leighfilms">Mike Leigh und seine Filme</a></small>
</li>
<li>
<small><a href="#sec-leighbleakmoments">Bleak Moments</a></small>
</li>
<li>
<small><a href="#sec-leighhardlabour">Hard Labour</a></small>
</li>
<li>
<small><a href="#sec-leighnutsmay">Nuts in May</a></small>
</li>
<li>
<small><a href="#sec-leighkissdeath">The Kiss Of Death</a></small>
</li>
<li>
<small><a href="#sec-leighwhoswho">Who's Who</a></small>
</li>
<li>
<small><a href="#sec-leighgrownups">Grown-Ups</a></small>
</li>
<li>
<small><a href="#sec-leighhome">Home Sweet Home</a></small>
</li>
<li>
<small><a href="#sec-leighmeantime">Meantime</a></small>
</li>
<li>
<small><a href="#sec-leighjuly">Four Days in July</a></small>
</li>
<li>
<small><a href="#sec-leighhopes">High Hopes</a></small>
</li>
<li>
<small><a href="#sec-leighlifesweet">Life is Sweet</a></small>
</li>
<li>
<small><a href="#sec-leighnaked">Naked</a></small>
</li>
<li>
<small><a href="#sec-leighsecrets">Secrets And Lies: Lustspiel vom geteilten Leid</a></small>
</li>
<li>
<small><a href="#sec-leighcareergirls">Career Girls: In die Jahre kommen</a></small>
</li>
<li>
<small><a href="#sec-leighfilmography">Filmographie</a></small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>
