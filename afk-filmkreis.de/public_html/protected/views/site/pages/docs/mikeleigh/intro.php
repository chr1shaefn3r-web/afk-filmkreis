<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small><?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.intro','docs.mikeleigh.leighfilms')?>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-intro"></a>
<h3>Vorwort<br>
<small>Mike Leigh - Zwischen Kino und Fernsehen</small>
</h3>

<p>   Seit NAKED (1993, Internationale Filmfestspiele Cannes: Beste Regie; Mike Leigh, Bester
   Darsteller: David Thewlis) und sp&auml;testens seit SECRETS AND LIES / L&Uuml;GEN UND GEHElMNISSE 
(1996 Internationale Filmfestspiele Cannes: Bester Film, Beste Darstellerin: Brenda
   Blethyn) ist Mike Leigh auch dem breiten Kinopublikum ein fester Begriff geworden.
   Dass dem inzwischen 54j&auml;hrigen der internationale "Durchbruch" erst nach &uuml;ber 20 Jahren
   Filmarbeit gelang, hat vor allem mit der desolaten Situation der englischen Filmindustrie seit den
   70er Jahren zu tun.<br>
   Nach seinem ausgezeichneten und vielversprechenden Erstling BLEAK MOMENTS (1971,
   Internationale Filmfestspiele Locarno: Goldener Leopard, Bester Film) erging es ihm nicht
   anders als anderen Regisseuren; er konnte f&uuml;r eine lange Zeit ausschlie&szlig;lich f&uuml;r das Fernsehen
   drehen. Weder ein breites Publihum noch die Filmkritik waren so erreichbar - das Aus f&uuml;r eine
   internationale Karriere.
</p>


<p>
<i>"Wenn mir nach BLEAK MOMENTS jemand
   gesagt h&auml;tte, dass ich dreizehn Jahre lang
   nicht f&uuml;rs Kino arbeiten w&uuml;rde, w&auml;re ich sehr
   deprimiert gewesen. Aber niemand hat in
   den siebziger Jahren in England unabh&auml;ngige Filme gemacht. Das &auml;nderte sich erst
   Anfang der achtziger Jahre, als Channel Four
   ins Leben grufen wurde: In der selben Zeit
   wie  BLEAK MOMENTS drehte Stephen
   Frears seinen ersten Kinofilm,  GUMSHOOE, und Ken Loach machte KES. Beide
   arbeiteten in den n&auml;chsten Jahren auch nur
   f&uuml;r das Fernsehen. Und f&uuml;r mich war es
   sicherlich noch schwieriger wegen meiner
   Arbeitsweise. Wenn ich jetzt zur&uuml;ckblicke,
   finde ich es geradezu erstaunlich, da&szlig; nach
   anderthalb Jahrzehnten, wo keine unabh&auml;ngigen Spielfilme m&ouml;glich waren, ich seit
   1987 kontinuierlich Filme drehen konnte. Bis
   1980  weigerte  sich  das  London  Film
   Festival, Fernsehproduktionen zu zeigen.
   Ich erwog damals ernsthaft, w&auml;hrend des
   Festivals in einen Hungerstreik zu treten und
   mich anzuketten, so verzweifelt war ich. Die
   Leiter erkl&auml;rten mich f&uuml;r verr&uuml;ckt; ich k&ouml;nne
   doch Filme machen, das Fernsehen w&uuml;rde
   sie zeigen, viele Menschen w&uuml;rden sie dort
   sehen - warum m&uuml;sse es denn unbedingt ein
   Spielfilm sein? Das war eine sehr frustrierende Zeit.
"</i>
</p>


<p>   Channel Four wurde 1982 gegr&uuml;ndet. In seinen Statuten festgelegt auf eine "innovative, originelle" 
   Programmgestaltung nahm dieser Fernsehsender gro&szlig;en Einflu&szlig; auf die britische
   Fernseh- und Kinolandschaft und half ganz wesentlich durch sein Filmf&ouml;rderungsprogramm das
   New British Cinema der 80er Jahre loszutreten (Mein wunderbarer Waschsalon, Sammy und
   Rosie tun es, London kills me, ...).
   Auch Mike Leigh bekam so die M&ouml;glichkeit, endlich wieder f&uuml;r das Kino zu drehen. HIGH HOPES
   (1988) war sein erster 35mm-Film seit 17 Jahren.
</p>


<p>   Seine lange T&auml;tigkeit f&uuml;r das Britische Fernsehen wurde erstmals w&auml;hrend den Hofer Filmtagen
    1993 mit einer umfangreichen Werkschau gew&uuml;rdigt. Das BBC hat jetzt zusammen mit Mike
   Leighs Produktionsgesellschaft und dem BFI (British Film Institute) eine Auswahl seiner
    Fernsehfilme f&uuml;r das Kino zusammengestellt. Anlass f&uuml;r uns zu einer kleinen Mike Leigh
   Werkschau mit einem Teil seiner Kinofilme und BBC-Produktionen, die hier bislang noch weitgehend unbekannt sind.
</p>


<p>Viel Spa&szlig; mit Mike Leigh!</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>
