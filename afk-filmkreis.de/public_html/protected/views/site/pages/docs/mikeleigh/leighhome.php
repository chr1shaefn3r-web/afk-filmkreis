<p align="right">
<small>

<?php echo CHtml::link('&lt;&lt;&lt; Begleitheft zum Thema Montage', array('site/page', 'view'=>'docs.montage.intro'));?>
<br>
</small>
</p>
<hr size="1" noshade="noshade">
<big><b>Die Filme von Mike Leigh</b></big>
<br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.mikeleigh.leighgrownups','docs.mikeleigh.leighmeantime')?>
<br>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-leighhome"></a>
<h3>Home Sweet Home</h3>
      
<p>
<i>"Szenen aus dem Leben von drei Postboten: von der Entfremdung der beiden Verheirateten (h&auml;usliche
Zankereien sind an der Tagesordnung) und dem lachenden Dritten, ihrem Kollegen, der mit der einen
Ehefrau ein Verh&auml;ltnis hat, zu der anderen vielleicht bald eines haben wird und in seinem Stoizismus
auch alle Sozialarbeiter (mitsamt ihren ungl&uuml;ckseligen Ideen) &uuml;berstehen wird, die ihn wegen seiner
behinderten Tochter aufsuchen. Einer der komischsten Leigh-Filme, besonders durch das Paar June, die
leidenschaftlich Kitschromane liest, und Harold, der &uuml;berhaupt nichts begreift.
"</i> <small>(Filmbulletin 1.94)</small>
</p>

      
<h4>Mike Leigh:</h4>
      
<p>
<i>"&Uuml;ber das Leben als Eltern (unser zweiter Sohn kam w&auml;hrend der Dreharbeiten zur Welt),
Postboten, Sozialarbeiter - und Sex: Su Elliotts leidenschaftliche June liest die Kitschromane
von Milles &amp; Boon (und redet auch so), die der Film ein wenig auf die Schippe nimmt. Achten
Sie auf Carl Davis' Musik - das London Double Bass Ensemble - wunderbar. Und Sheila
Kelley (die Honky in "Nuts in May"): bedr&uuml;ckend, komisch, tragisch. Als Michael Powell den
Film sah, sagte er mir, ich h&auml;tte jede Falle, die ich mir selbst gestellt hatte, vermieden!"</i>
</p>

      
<h4>Zum Film:</h4>
      
<p>
In HOME SWEET HOME, der faszinierenden Story von drei Postboten, mit denen praktisch
nichts passiert, funktioniert jede geschraubte Wortlosigkeit in einem be&auml;ngstigend
symmetrischen Aufbau. Es gibt Arien von Einsamkeit und bem&uuml;hter Anma&szlig;ung, in denen
tagtr&auml;umerische Ehefrauen stockend ihr Leid klagen. <i>"Es ist wie ein Band aus Stahl, das
sich fest um meine Schl&auml;fen legt"</i>. Es gibt leidenschaftslose Duette, in denen pantoffel-
heldische Ehem&auml;nner noch weiter unter den Daumen geraten. <i>"H&ouml;r auf, auf dem Teppich
herumzutrampeln. Du zerdr&uuml;ckst ihn."</i> Es gibt lange mozartsche Ensembles, in denen fast
jeder nichts sagt.<br>
Wenn Sie sich Cosi fan tutte vorstellen k&ouml;nnen, jedoch ohne Musik und ohne Worte, daf&uuml;r
aber mit einer Ausstattung und Kost&uuml;men, die aus den spie&szlig;igsten Stoffen und M&ouml;beln
besteht, die der Mensch nur produzieren kann, dann haben Sie einen Film von Mike Leigh.
</p>

<p>Clive James in "The Observer"</p>
    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Vorwort: Mike Leigh - Zwischen Kino und Fernsehen', array('site/page', 'view'=>'docs.mikeleigh.intro'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Mike Leigh und seine Filme', array('site/page', 'view'=>'docs.mikeleigh.leighfilms'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Bleak Moments', array('site/page', 'view'=>'docs.mikeleigh.leighbleakmoments'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Hard Labour', array('site/page', 'view'=>'docs.mikeleigh.leighhardlabour'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Nuts in May', array('site/page', 'view'=>'docs.mikeleigh.leighnutsmay'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('The Kiss Of Death', array('site/page', 'view'=>'docs.mikeleigh.leighkissdeath'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Who\'s Who', array('site/page', 'view'=>'docs.mikeleigh.leighwhoswho'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Grown-Ups', array('site/page', 'view'=>'docs.mikeleigh.leighgrownups'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Home Sweet Home', array('site/page', 'view'=>'docs.mikeleigh.leighhome'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Meantime', array('site/page', 'view'=>'docs.mikeleigh.leighmeantime'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Four Days in July', array('site/page', 'view'=>'docs.mikeleigh.leighjuly'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('High Hopes', array('site/page', 'view'=>'docs.mikeleigh.leighhopes'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Life is Sweet', array('site/page', 'view'=>'docs.mikeleigh.leighlifesweet'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Naked', array('site/page', 'view'=>'docs.mikeleigh.leighnaked'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Secrets And Lies: Lustspiel vom geteilten Leid', array('site/page', 'view'=>'docs.mikeleigh.leighsecrets'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Career Girls: In die Jahre kommen', array('site/page', 'view'=>'docs.mikeleigh.leighcareergirls'));?>
</small>
</li>
<li>
<small><?php echo CHtml::link('Filmographie', array('site/page', 'view'=>'docs.mikeleigh.leighfilmography'));?>
</small>
</li>
</ul>
<hr size="1" noshade="noshade">
<p>
<font size="-2">Impressum</font>
</p>
<p>
<font size="-2">"Die Filme von Mike Leigh" ist eine Sonderausgabe der Filmzeitung des AFK-Filmstudios und
erscheint begleitend zur kleinen Mike Leigh Retrospektive im Wintersemester 1997/98.</font>
</p>
<p>
<font size="-2">V.i.S.d.P.<br>
Akademischer Filmkreis Karlsruhe e.V.<br>
Kaiserstrasse 12<br>
76128 Karlsruhe<br>
</font>
</p>
<p>
<font size="-2">Mitarbeiter an dieser Ausgabe:<br>
A. G&uuml;nter, M. Pliefke</font>
</p>