<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<img alt="New Hollywood" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/newhollywood/newholly.gif"><br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.newhollywood.coppola','docs.newhollywood.nhbobby')?>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-bbsfilms"></a>
<h3>Die BBS-Gruppe und ihre Filme</h3>


<p>Einer  der  Schl&uuml;sselfilme  des  New
Hollywood und  der erste  Film,  der
von  der  BBS-Gruppe  produziert
wurde, war EASY  RIDER - offiziell
eine  Rybert-Produktion.  Die  
Rybert-Produktionsgruppe war eine Gr&uuml;ndung
von Bob Rafelson und Bert Schneider,
der  Sohn  des  langj&auml;hrigen  Vorsitzenden
der  Columbia-Pictures Abraham  Schneider.  
Rafelson  und Schneider  produzierten  seit  Mitte
der 60er Jahre die Fernsehserie THE
MONKEYS,  eine  Beatles-Parodie,
sowie nach deren Absetzung den Film
HEAD  (1968), ebenfalls  &uuml;ber  die
Monkeys.</p>


<p>Nach dem gro&szlig;en Erfolg von EASY
RIDER gab die Columbia Bob Rafelson,
Bert  Schneider  und  Steve  Blauner
(die Vornamen ergaben den  Namen
der  BBS-Produktionsgruppe)  praktisch  
einen  Freibrief zur  Produktion
weiterer  Filme  -  ohne  Auflagen
bez&uuml;glich Form und Inhalt. Daraufhin
entstanden zwischen 1970 und 1972
f&uuml;nf Filme; FIVE EASY PEACES (Ein
Mann  sucht sich  selbst,  1910)  und
THE KING OF MARVIN GARDENS (1972)
von Bob Rafelson , DRIVE, HE SAID
(1971) von Jack Nicholson, A SAFE
PLACE (1971) von Henry Jaglom und
THE LAST PICTURE SHOW (Die letzte
Vorstellung,  1971)  von  Peter
Bogdanovich. Letzterer war innerhalb
der  BBS  eher eine  Randfigur -  er
wurde  aufgrund  seines  Erstlingsfilmes  
TARGETS  (eine  Corman-Produktion, s. dort)
eingeladen, einen
Film f&uuml;r die BBS zu drehen.</p>


<p>Viel  st&auml;rker  als  die  zahlreichen
<i>youth movies</i>, die als Nachzieher von
EASY RIDER in der Hoffnung &auml;hnlicher
Erfolge  entstanden,  besch&auml;ftigen
sich  die von der  BBS-Gruppe
produzierten  Filme  mit dem
Versagen des amerikanischen
Traumes, mit dem ersch&uuml;tterten
Vertrauen in die amerikanischen
Ideale. Und dabei verlieren sie sich
nicht in modischen Aspekten, bleiben
nicht an der Oberfl&auml;che, sondern sie
versuchen, das  Innenleben der
amerikanischen Gesellschaft, die
psychischen  Defekte,  die  <i>"Anatomie
der Entfremdung"</i> <small>((1))</small> zum Ausdruck
zu bringen.  Dabei entstand eine Art
gemeinsamer Stil, eine f&uuml;r die BBS-Gruppe   
typische   Handschrift,
wodurch  sie  sich  z.B.  f&uuml;r  den
Filmkritiker H.-C. Blumenberg als die
<i>"wahre  Avantgarde  des  
amerikanischen  Films"</i> <small>((1))</small> dieser  Zeit
erwiesen.  Gro&szlig;e  Vorbilder  waren
dabei  europ&auml;ische  Regisseure  wie
Antonioni, Bergmann und die
Regisseure der franz&ouml;sischen
<i>nouvelle vague</i>.</p>


<p>FIVE EASY PIECES war neben THE
LAST  PICTURE SHOW die erfolgreichste
aller  BBS-Produktionen  (es
waren  auch  die  einzigen,  die
seinerzeit  den  Sprung  nach
Deutschland  schafften).   Jack
Nicholson  spielt  darin  einen
Pianisten,  Robert Dupea,  den Sohn
einer Musiker-Familie.  Er f&uuml;hlt sich
in seiner Umgebung  und in seinem
Beruf nicht zuhause und durch den
R&uuml;ckzug in seine famili&auml;re Umgebung
versucht  er,  sich  selbst  zu  finden.
Wie viele der Figuren in den BBS-Filmen 
scheitert er aber auf seiner
Suche.</p>


<p>DRIVE, HE SAID ist ein episodenhaftes Werk &uuml;ber eine Gruppe
Studenten, die gegen die Sportbesessenheit ihrer Universit&auml;t
revoltieren. Der m&ouml;rderische Druck
der gesellschaftlichen Zust&auml;nde
treiben den Anf&uuml;hrer in den Irrsinn.<br>
Die f&uuml;r das damalige Zielpublikum zu
realistische und zuwenig romantische 
Darstellung des Universit&auml;tslebens 
war wohl mit ein Grund f&uuml;r
den finanziellen Reinfall des Films.</p>


<p>A SAFE PLACE - der Titel des Filmes
von Henry Jaglom benennt eines der
zentralen Motive der BBS-Filme.<br>
Auch hier zieht sich die Heldin an
einen f&uuml;r sie "sicheren Ort" zur&uuml;ck:
sie flieht vor der Realit&auml;t in eine
Phantasiewelt (mit Orson Welles als
Zauberer). Der Film springt zwischen
Vergangenheit, Gegenwart und Traum
hin und her und entfernt sich dabei
sehr weit vom traditionellen Erz&auml;hl-Kino. Laut J. Monaco, einem amerikanischen 
Filmkritiker, resultierte
dabei ein <i>"pubert&auml;rer Film voll
derber Symbolik"</i>, der f&uuml;r die BBS das
Erreichen des "Tiefpunktes" bedeutete (2). 
Diese Kritik zeigt das
Unverst&auml;ndnis, auf das die Filme der
BBS selbst bei der amerikanischen
Kritik stie&szlig;en (s.u.).</p>


<p>Im Zentrum der in den 50er Jahren in
Anarene spielenden Handlung von THE
LAST PICTURE SHOW stehen drei
Jugendliche auf der Schwelle zum
Erwachsensein: der orientierungslose
Sonny, den der Zufall in die Arme der
verh&auml;rmten Frau seines Footballtrainers 
treibt, sein Freund Duane
und die von ihm geliebte Jacy, die in
ihrer Lebensgier unf&auml;hig ist, eine
echte Beziehung einzugehen. Der
Ruhepunkt f&uuml;r alle ist das Kino der
Stadt und sein Betreiber Sam. Aber
mit der Schlie&szlig;ung des Kinos werden
auch die gescheiterten Tr&auml;ume der
Jugendlichen deutlich. Es bleibt am
Ende nur die Anpassung, die Faust in
der Tasche.</p>


<p>THE KING OF MARVIN GARDENS
beginnt mit einer vier Minuten langen
Gro&szlig;aufnahme, in der Jack Nicholson
(er spielt einen Discjockey) eine
traumatische Erfahrung aus seiner
Kindheit erz&auml;hlt. Sp&auml;ter sitzt er mit
seinem Bruder im winterlich
verlassenen Atlantic City und beide
tr&auml;umen von einem fernen Insel-Paradies, 
ihrem "sicheren Platz". Bob
Rafelson selbst bezeichnet seinen
Film als "kafkaesken Alptraum".</p>


<p>Die Filme der BBS zeichnen sich
durch einen ungew&ouml;hnlichen Realismus 
der Figuren, durch einen sehr
pers&ouml;nlichen, z.T. fast 
autobiographischen Inhalt und durch die
Aufl&ouml;sung klassischer 
Erz&auml;hlstrukturen aus. Gerade die
Realit&auml;tsn&auml;he der Filme (abgesehen
von dem z.T. eher experimentellen
Stil der Filme) bereitete sowohl dem
breiten (Durchschnitts-)Publikum als
auch den Filmkritikern zuweilen
Probleme, der Misserfolg von DRIVE,
HE SAID wird zum Teil auch darauf
zur&uuml;ckgef&uuml;hrt. Selbst Kritiker, die
ansonsten die Regisseure des New
Hollywood unterst&uuml;tzten, lie&szlig;en
unmissverst&auml;ndlich Kritik laut
werden: Pauline Kael z.B. sprach von
den <i>"talentierten Leuten, deren Filme
chaotische Desaster"</i> seien (sie
spielte damit vor allem auf DRIVE,
HE SAID an) und bezeichnete die 
BBS-Produktionen als <i>"unordentliche,
halb-langweilige, verhei&szlig;ungsvolle
Fehlschl&auml;ge"</i> (1).</p>


<p>Ein weiterer Grund f&uuml;r das Aus der
BBS-Gruppe im Jahr 1972 war jedoch
auch die ungeschickte Vermarktung
der Filme. So wurde zum Beispiel
DRIVE, HE SAID, der im Uni-Milieu
spielt, in den Semesterferien in
Hollywood in die Kinos gebracht und
nicht w&auml;hrend des Semesters in
einem Kino eines Studentenviertels.
Und f&uuml;r A SAFE PLACE wurde so
schlecht geworben (er wurde als
Antwort auf LOVE STORY angepriesen), 
dass die Werbung das
eigentliche Zielpublikum des Films
nicht erreichte. So waren bis auf
FIVE EASY PIECES und THE LAST
PICTURE SHOW alle Filme ein
finanzielles Fiasko und die Columbia
zog sich aus der Finanzierung zur&uuml;ck.
Danach hatten die Regisseure
Schwierigkeiten, neue Projekte zu
finanzieren - Jack Nicholson (der
h&auml;ufig als die zentrale Figur der BBS
gilt) zog sich f&uuml;r lange Zeit auf die
Schauspielerei zur&uuml;ck und auch bei
Henry Jaglom und Bob Rafelson
dauerte es ein paar Jahre, bis sie
sich wieder an die Regie neuer Filme
wagten. Nur Peter Bogdanovich
drehte zun&auml;chst weiter neue Filme.</p>


<h4>Peter Bogdanovich - ein kurzes Portrait</h4>


<p>Bogdanovich wurde 1939 als Sohn
jugoslawischer Einwanderer in New
York geboren. Nach einem Schauspielstudium 
in New York inszenierte
er ab 1958 verschiedene St&uuml;cke u.a.
von Tennessee Williams an 
Off-Broadway-Theatern. Schon fr&uuml;h
begann er, f&uuml;r Kinos Filmtexte zu
schreiben - zun&auml;chst gegen freien
Eintritt. Dies erm&ouml;glichte ihm, vor
allem &auml;ltere Filme umsonst zu sehen.
Von 1958 bis 1968 arbeitete er als
Filmkritiker vor allem f&uuml;r den
Esquire und ver&ouml;ffentlichte
verschiedene Regisseurmonographien.<br>
Nach seinem Umzug Anfang der 60er
Jahre nach Los Angeles begann er
eifrig Beziehungen zu kn&uuml;pfen. Er
hoffte, als Schauspieler entdeckt zu
werden (er spielte nur in einem Film
mit und zwar in Orson Welles' THE
OTHER SIDE OF THE WIND, der noch
heute in einem Pariser Tresor liegt)
oder eines seiner gro&szlig;en Idole
kennenzulernen. Dabei trug er sich
schnell den Ruf ein, sich etwas zu
vehement bei ihnen einzuschmeicheln.</p>


<p>F&uuml;r Roger Corman arbeitete er
schlie&szlig;lich u.a. als Kameramann f&uuml;r
Spezialaufnahmen sowie als Autor
und drehte f&uuml;r ihn 1967 TARGETS,
seinen ersten und f&uuml;r viele auch
besten Film. Nach seiner Arbeit f&uuml;r
die BBS-Gruppe gr&uuml;ndete er
zusammen mit Coppola (der zuvor
mit seiner Produktionsfirma
American Zoetrope gescheitert war)
und Friedkin die Director's Company.</p>


<p>Schon in THE LAST PICTURE SHOW
zeigt sich Bogdanovichs Vorliebe f&uuml;r
Genres und Zeitstile. Dies resultiert
sicherlich aus seiner Verehrung der
"alten Meister" wie Howard Hawks
und John Ford und nat&uuml;rlich aus
seiner Vorliebe f&uuml;r Hollywood-Filme
schlechthin. Angeblich will er 6000
der 20000 Produktionen bis Ende der
60er Jahre gesehen haben. Das
Ergebnis:</p>


<p>WHAT'S UP DOC (1971) ist der
Versuch, eine <i>screwball</i>-Kom&ouml;die
wie BRINGING UP BABY von Howard
Hawks zu inszenieren. PAPER MOON
(1972) ist ein klassisches <i>period
picture</i>, das in den 30er Jahren
spielt, DAISY MILLER (1973) ein
Kost&uuml;mfilm und schlie&szlig;lich AT LONG
LAST LOVE (1974), ein richtiges
Musical im Stil der 30er und 40er
Jahre (ein geplanter Western mit
John Wayne, Henry Fonda und James
Stewart kam nie zustande, weil
Wayne die Zusammenarbeit mit Fonda
ablehnte).<br>
Er selbst sagt: <i>"Ich richte mich
niemals nach meinen Zeitgenossen,
sondern nach den 20000 Filmen, die
bislang gemacht worden sind und von
denen ich 6000 gesehen habe. Ob das,
was ich mache, gut ist, w&auml;ge ich ab
gegen die Regisseure, die ich
bewundere..."</i> (3).<br>
Nach dem Ende der BBS-Gruppe war
Bogdanovich der einzige der
Regisseure, der - zun&auml;chst - ohne
Schwierigkeiten weitermachen
konnte. Er hoffte, mit
konventionellen Filmen Erfolg zu
haben, indem er seinen gro&szlig;en
Vorbildern nacheiferte. Dadurch ging
jedoch die Originalit&auml;t seines ersten
Filmes immer mehr verloren. WHAT'S
UP DOC und PAPER MOON blieben bis
heute seine erfolgreichsten Filme.</p>

      
<p>&nbsp;</p>
<hr>
<h3>Literatur / Quellenangaben</h3>
<ul>
<li>
<small>(1) New Hollywood, Hanser Verlag, Reihe Film 10, 1974</small>
</li>
<li>
<small>(2) James Monaco: American Film Now, Hanser Verlag, 1985</small>
</li>
<li>
<small>(3) filmkritik Heft 1/1973, S.18</small>
</li>
</ul>
      

<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Coppola', array('site/page', 'view'=>'docs.newhollywood.coppola'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Die BBS-Gruppe und ihre Filme', array('site/page', 'view'=>'docs.newhollywood.bbsfilms'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Meister der Verwandlung: Robert de Niro', array('site/page', 'view'=>'docs.newhollywood.nhbobby'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Martin Scorsese: Die Zeit bis Taxi Driver', array('site/page', 'view'=>'docs.newhollywood.nhscorsese'));?></small>
</li>
<li>
<small><?php echo CHtml::link('John Cassavetes', array('site/page', 'view'=>'docs.newhollywood.nhcassavetes'));?></small>
</li>
</ul>
