<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<img alt="New Hollywood" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/newhollywood/newholly.gif"><hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-coppola"></a>
<h3>Coppola</h3>


<p>
YOU'RE A BIG BOY NOW erz&auml;hlt die am&uuml;sante Geschichte
von Bernard (Peter Kastner), einem Mutters&ouml;hnchen, das
auf Wunsch seines Vaters nach New York ziehen soll, um
sich dort <i>"die H&ouml;rner abzusto&szlig;en und ein Mann zu werden"</i>.
In New York wird Bernard durch ein &Uuml;berma&szlig; an (sexuellen)
M&ouml;glichkeiten angeregt. Er verschm&auml;ht die offensichtlichen
Angebote Amys (Karen Black) und verf&auml;llt der m&auml;nnerhassenden 
Schauspielerin Barbara Darling (Elizabeth
Hartman). Schon die erste Nacht endet im Totalfrust.
Als seine Mutter und Amy nach dem verlorenen Sohn
suchen, sieht er die drohende Gefahr der Bevormundung
und nimmt Rei&szlig;aus ...</p>


<p>
Nach der Premiere von BIG BOY sah man in
Hollywood ein neues Genie in der Nachfolge von
Orson Welles heraufziehen.
Coppolas  <i>"eindeutige Parteinahme f&uuml;r den
Emanzipationsdrang einer aufbrechenden jungen
Generation gegen die verkrusteten
Verhaltensma&szlig;regeln gutb&uuml;rgerlichen Benehmens
hatte zwar nicht die Radikalit&auml;t von Dennis
Hoppers EASY RIDER, der den Glauben an den
amerikanischen Traum gleich begrub, aber er
feierte Bernards Ausbruch aus den Klauen
elterlicher Bevormundung als Punktsieg des
"poppigen feelings" &uuml;ber neurotisches
Spie&szlig;b&uuml;rgertum."</i> <small>(Ulli Weis in "Das Neue Hollywood")</small>

</p>


<p>
BIG BOY wurde zum Ausl&ouml;ser einer Reihe von
Filmen, die sich mit sp&ouml;ttischem Anstand &uuml;ber
biederm&auml;nnische Augenwischerei hermachten; ein
Trend, von dem auch Mike Nichols' THE
GRADUATE mit Dustin Hoffman profitierte.
Wenn sich BIG BOY auch nicht explizit auf Filme
wie A HARD DAY'S NIGHT oder THE KNACK
(beide von R. Lester) st&uuml;tzt, so sind zumindest
gewisse <i>"Affinit&auml;ten zur Neuen Welle, zu Richard
Lester und John Cassavetes so offensichtlich,
dass zumindest von einem Gleichklang im
Zeitgef&uuml;hl und von einer gewissen Revolte gegen
die Standards einer l&auml;ngst industrialisierten
Filmsprache auszugehen ist. Es ist eine
Bewegung, an der Coppola teilhat."</i> <small>(Peter W. Jansen in "Francis Ford Coppola" -- Reihe Film 33, Carl Hanser Verlag)</small>

</p>


<h4>Die Ern&uuml;chterung - FINIAN'S RAINBOW</h4>


<p>
YOU'RE A BIG BOY NOW war erfolgreich
gelaufen und Coppola arbeitete gerade an einem
Drehbuch, aus dem sp&auml;ter THE
CONVERSATION werden sollte, als er von
Warner-Seven Arts das &uuml;berraschende Angebot
erhielt, das popul&auml;re Musical "Finian's Rainbow"
f&uuml;r die Leinwand zu adaptieren. Coppola willigte
ein, obwohl er sich vorgenommen hatte, nicht zum
Studioregisseur zu werden und in die F&auml;nge der
gro&szlig;en Produktionsgesellschaften zu geraten.
(Die Aussicht auf ein sicheres Studiogehalt nach
dem Reinfall an der B&ouml;rse mu&szlig; ausschlaggebend
gewesen sein; in Zukunft w&uuml;rde Coppola noch
&ouml;fter nach einem finanziellen Reinfall einen
Studiojob annehmen m&uuml;ssen.)
</p>


<p>
Zum Zeitpunkt der Adaption war das Musical
hinsichtlich seiner gesellschafts- und ideologiekritischen Aspekte schon recht veraltet (es hatte
seine Urauff&uuml;hrung bereits im Januar 1947 erlebt);
dennoch glaubte Coppola, die Oberfl&auml;chlichkeit
und Spannungslosigkeit des Skripts durch eigene
Arbeit wieder ausgleichen zu k&ouml;nnen - eine
Rechnung, die letztendlich nicht aufging. Die Dreharbeiten waren von Kompromissen und
Auseinandersetzungen gepr&auml;gt. So hatte
Coppola beispielsweise eigene 
Choreografievorstellunqen, die nicht mit denen von Fred
Astaire &uuml;bereinstimmten, worauf dessen
Choreograph die Produktion verlie&szlig;.<br>
Hinzu kamen finanzielle Probleme: Coppola wollte
den Film on location drehen, um der Geschichte
einen authentischeren Rahmen zu geben. Die
Studiobosse verlangten jedoch, dass aus
Kostengr&uuml;nden in den noch stehenden Kulissen
von CAMELOT gedreht wurde. F&uuml;r 
Au&szlig;enaufnahmen standen nur acht Tage zur Verf&uuml;gung.
Das gewonnene Material aus diesen acht Tagen
versuchte Coppola krampfhaft am Schneidetisch
in den Film zu integrieren, um ihm doch noch
etwas Realismus zu geben. Zahlreiche
Anschlussfehler waren das Ergebnis.<br>
Auch nach den Dreharbeiten waren die
Ent&auml;uschungen nicht vorbei: das Studio blies
den fertigen Film von 35 auf 70 mm auf, um ihn an
den Kinokassen besser verkaufen zu k&ouml;nnen. Bei
diesem Vorgang hatte wohl niemand an die 
unterschiedlichen Bildformate gedacht, so dass bei allen
Tanznummern die F&uuml;&szlig;e von Fred Astaire 
abgeschnitten waren - f&uuml;r ein Musical unverzeihlich.
</p>


<h4>Auf dem Weg In die Unabh&auml;ngigkeit: THE RAIN PEOPLE (1969)</h4>


<p>
Mit FINIAN'S RAINBOW hatte Coppola die
unangenehme Erfahrung der st&auml;ndigen
Bevormundung durch ein gro&szlig;es Studio gemacht.
Vom Aufbegehren gegen Bevormundung hatte
auch sein vorheriger Film BIG BOY gehandelt; f&uuml;r
die Zukunft nahm sich Coppola nun vor, sich nie
wieder von jemandem bestimmen zu lassen.
Seinen n&auml;chsten Film - THE RAIN PEOPLE -
wollte er nun endlich on location drehen,
ausserhalb der k&uuml;nstlerisch einengenden
Bedingungen der Studios.
</p>


<p>

<i>"Coppola hatte THE RAIN PEOPLE aus einem
Skript entwickelt, das er 1960 f&uuml;r eine "creative
writing class" am College unter dem Titel "Echoes"
begonnen hatte. Es war die Geschichte von drei
Frauen, die ihre M&auml;nner verlassen."</i> <small>(Peter W. Jansen in "Francis Ford Coppola", Reihe Film 33, Hanser Verlag)</small>
Ihren Ursprung hatte die Idee in einem Ereignis in
Coppolas Kindheit: seine Mutter hatte nach einem
heftigen Streit mit ihrem Mann einmal f&uuml;r zwei
Tage das Haus verlassen. <i>"Sie fuhr zum Haus
ihrer Schwester und blieb dort f&uuml;r zwei Tage,
ohne uns zu sagen, wo sie war. Sie sagte, sie
h&auml;tte in einem Hotel &uuml;bernachtet. Da hat
irgendetwas in mir reagiert, wissen Sie, die
Vorstellung, dass eine Frau fortgeht und in einem
Hotel &uuml;bernachtet. Ich stellte mir vor, wie sie in
diesem Hotel sa&szlig; und Angst hatte. Ich glaube,
dass die Ideen, die man hat und aus denen sp&auml;ter
etwas wird, so etwas &auml;hnliches sind wie zuf&auml;llige
Schnappsch&uuml;sse, wie eine zuf&auml;llige Stimmung."</i>

</p>


<p>
Mit etwas finanziellem R&uuml;ckhalt von Warner und
seinem Honorar von FINIAN'S RAINBOW stellte
er ein Team von rund zwanzig Mitarbeitern auf,
viele von ihnen seine Freunde, und kaufte einen
Kleinbus, in dem er ein bescheidenes Filmstudio
installierte. Dieses sog. Cinemobile sollte ihm
technische und r&auml;umliche Unabh&auml;ngigkeit von
Hollywood garantieren.
</p>


<p>
Natalie (Shirley Knight) ist eine Frau, in deren Leben alles
nach Plan verlaufen ist. Sie hat einen verst&auml;ndnisvollen
Mann, von dem sie ein Kind erwartet. Dennoch verl&auml;lsst sie
ihn eines Morgens. Einmal im Leben m&ouml;chte sie frei sein,
f&uuml;nf Minuten, eine halbe Stunde, einen halben Tag, ... Sie
will ihren Platz alleine finden. Ihre Schwangerschaft macht
alles noch komplizierter, f&uuml;rchtet sie sich doch vor der
gro&szlig;en Verantwortung. Mit ihrem Auto f&auml;hrt sie durch
Amerika, die diffusen Spuren von Freiheit und
Unabh&auml;ngigkeit verfolgend. Unterwegs nimmt sie einen
Anhalter mit: Jimmie Kilgannon (James Caan), den
ehemaligen College-Footballstar, genannt Killer. Killer ist in
Folge einer Sportverletzung geistig behindert und verh&auml;lt
sich wie ein zur&uuml;ckgebliebenes Kind. Zuerst m&ouml;chte Natalie
Killer wieder loswerden - zu sehr verlangt er ihr die
Verantwortung ab, vor der sie eigentlich hatte fliehen
wollen. Doch allm&auml;hlich entwickelt sich eine Beziehung, die
jedoch mit einer Katastrophe endet, als sich ein Fremder -
der Polizist Gordon (Robert Duvall) - zwischen die beiden
dr&auml;ngt.
</p>


<p>
Im Mittelpunkt des Films steht die von Killer
erz&auml;hlte Fabel von den Regenmenschen, <i>"who
are made of rain. When they cry, they disappear
altogether because they cry themselves away."</i>

</p>


<p>
In kaum einem anderen Film arbeitet Coppola so
deutlich seine pers&ouml;nlichen ikonografischen
Notate heraus: das Wasser (der Regen), die Zeit
und das Geld.<br>
Die Zeit spielt eine wichtige Rolle: f&uuml;r Killer gibt es
keine messbare Zeit mehr, nur noch die Zeit, die
ihm andere Menschen zuwenden. F&uuml;r Natalie
bedeutet die Zeit, die sie f&uuml;r sich selbst gewinnt,
so etwas wie Freiheit.<br>
Die Zeit bestimmt auch den Erz&auml;hlrhythmus des
Films: THE RAIN PEOPLE ist ein road movie mit
langen Einstellungen. Coppola nimmt sich viel
Zeit, erz&auml;hlt langsam. Am Anfang zeigt er den
Regen und f&uuml;hrt ihn als Handlungselement ein.
Der Regen wird Natalie auf ihrer Suche begleiten
und mit dem str&ouml;menden Regen verstr&ouml;mt auch
die Zeit; der Regen l&auml;sst ein Gef&uuml;hl f&uuml;r die Zeit
entstehen.
</p>


<p>
Die Dreharbeiten fanden - gem&auml;&szlig; der Vorlage
eines road-movies - fast ausschlie&szlig;lich <i>on the
road</i> statt. Zusammen mit seinem Team fuhr
Coppola vier Monate lang durch insgesamt
achtzehn Bundesstaaten, eine Reise, die &uuml;ber
weite Strecken der von Natalie und Killer
entspricht. Diese &Uuml;bereinstimmung war Coppola
wichtig: f&uuml;r Natalie ist der Ausbruch aus dem
Alltag eine Losl&ouml;sung, der Versuch einer 
Selbstfindung. F&uuml;r das Filmteam sollte es eine &auml;hnliche
Erfahrung werden; deshalb durfte auch niemand
seine Familie mitnehmen. (F&uuml;r sich selber machte
Coppola eine kleine Ausnahme...)<br>
Kameramann Bill Butler erinnert sich: <i>"Die Idee
war, dass wir in New York starteten und so weit
fuhren, wie wir wollten. Coppola hatte einen
gro&szlig;en Campingbus, in dem wir einen Schneidetisch eingerichtet hatten und au&szlig;erdem gab es
noch mehrere Wohnmobile."</i> Coppolas Frau
Eleanor fuhr zusammen mit den Kindern in einem
VW-Bus hinterher.
</p>


<p>
Im Team befand sich auch George Lucas, der
schon bei FINIAN'S RAINBOW Coppolas
Assistent gewesen war. Diesmal war Lucas
zweiter Kameramann, Art Director, 
Produktionsleiter und Tontechniker in einer Person. Au&szlig;erdem
entschloss sich Lucas, begleitend zu den
Dreharbeiten ein filmisches Tagebuch &agrave; la <i>cinema
verite</i> zu drehen. Das Ergebnis war FILMMAKER,
ein etwa halbst&uuml;ndiger Dokumentarfilm, ein
pers&ouml;nliches Statement &uuml;ber die t&auml;glichen
Spannungen und den Stress einer Filmproduktion,
die st&auml;ndig auf Achse ist.
</p>


<p>
Coppola war nat&uuml;rlich begeistert, zumal er
nicht nur gerne hinter der Kamera, sondern auch
gerne vor der Kamera steht. In FILMMAKER zeigt
er sich als der b&auml;rtige Prophet, der dem veralteten
Studiosystem den Untergang vorhersagt.<br>
In einem w&uuml;sten Telefongespr&auml;ch mit einem
Produktionsleiter von Warner Bros. tobt er: <i>"Das
System wird unter seinem eigenen Gewicht
zusammenbrechen. Es kann gar nicht anders."</i>
FILMMAKER ist eine der besten
Dokumentationen, die je &uuml;ber die Entstehung
eines Films gedreht wurden und wird noch heute
an den Filmklassen der UCLA als Beispiel f&uuml;r
einen hervorragenden Dokumentarfilm gezeigt.
</p>


<p>
Die letzte Szene zeigt alle Schauspieler und das
Kamerateam vor ihrer Karawane von
Ausstattungs- und Wohnmobilen. Das ganze
macht den Eindruck einer Theatergruppe aus dem
19. Jahrhundert, die von einer Tournee aus der
Provinz zur&uuml;ckkehrt.
</p>


<p>
Die Arbeit mit dem Unfertigen scheint einen
besonderen Reiz auf Coppola auszu&uuml;ben.
Improvisation war w&auml;hrend der Dreharbeiten ein
wichtiger Faktor; jedoch nicht hinsichtlich Dialogen
und Plotentwicklung (hier war das Buch bereits
pr&auml;zise ausgearbeitet; pr&auml;ziser zumindest als das
zu sp&auml;teren Filmen, wie z.B. zu THE
CONVERSATION). Wichtig war vielmehr, da&szlig;
die von Coppola gew&auml;hlte Drehform (<i>on the road</i>)
ihm viel Spielraum bei der Auswahl der Drehorte
lie&szlig;. Au&szlig;erdem hatte er die Freiheit, Ereignisse der
Reise spontan als Hintergrundelemente in seine
Geschichte einzubauen (z. B. die Parade, in die
sich Killer kurzfristig einreiht.)<br>
Was in seinen vorherigen Filmen FINIAN'S
RAINBOW und BIG BOY - wo sich Coppola
nicht gut genug vorbereitet hatte - noch ein Manko
gewesen war, erwies sich hier als Kunstgriff, um
der Geschichte mehr Wirklichkeitsn&auml;he zu geben.
</p>


<p>Abgesehen davon, dass Coppola beim
Filmemachen st&auml;ndig in Zeit- und Geldnot ist (in
seinen Filmen sind Zeit und Geld oft
gleichbedeutend), scheint er aus der Arbeit mit
dem Unfertigen seine Kreativit&auml;t zu beziehen.
(Die Schnittfassung von APOCALYPSE NOW,
die er 1979 in Cannes vorstellte, pr&auml;sentierte er
als <i>work in progress</i>.)<br>
Ein &auml;hnlicher Rhythmus pr&auml;gt auch seine rastlose
Lebensweise. Georges Lucas' Ehefrau Marcia
(Cutterin von TAXI DRIVER und ALICE
DOESN'T LIVE HERE ANYMORE) &uuml;ber
Coppola: <i>"Es ist nie langweilig mit ihm. Es sind
immer zehn oder zwanzig oder drei&szlig;ig Leute da,
irgendeiner setzt sich ans Klavier und klimpert, ein
paar Kinder tanzen herum und die Intellektuellen
sitzen da und sind in ein Gespr&auml;ch &uuml;ber Kunst
vertieft. Sein Leben befindet sich in einem
konstanten Zustand von Wandel."</i>
</p>


<h4>Der Traum von American Zoetrope</h4>


<p>

<i>"Das wesentliche Ziel der Gesellschaft ist es, sich
in den verschiedensten Bereichen des
Filmemachens zu engagieren und dabei die
begabtesten jungen Talente und die modernste
Technik und Ausr&uuml;stung einzusetzen, die auf dem
Markt zu haben sind."</i> <small>(aus einer Ank&uuml;ndigung zur Gr&uuml;ndung von American Zoetrope)</small>
</p>


<p>
W&auml;hrend der Dreharbeiten zu THE RAIN
PEOPLE hatten Francis Coppola und George
Lucas die Vision einer <i>"Filmkommune, deren
Mitglieder bis zu drei&szlig;ig Jahre alt waren, Ideen
und Ausr&uuml;stung in der gr&uuml;nen Einsamkeit von
Marin County (nahe San Francisco) miteinander
teilten und damit eine friedliche Alternative zu der
sinnlosen Verschwendung von Hollywood
demonstrierten. Das Ziel bestand darin, einen
Hafen zu schaffen, in dem Vertr&auml;ge unmoralisch
und Agenten nebens&auml;chlich waren, eine l&auml;ndliche
Basis statt einer sterilen Reflexion der Leere von
Los Angeles, eine Zuflucht, in der junge und
unerfahrene Filmemacher der Philosophie
Coppolas folgen konnten: "Film ist die endg&uuml;ltige
Form des Ausdrucks.""</i>
</p>


<p>
Nachdem sie THE RAIN PEOPLE bei Warner
abgeliefert hatten, reisten Coppola und sein
Produzent Ron Colby nach D&auml;nemark und
besuchten Lanterna Films, ein kleines Filmstudio,
das eine der vollst&auml;ndigsten Sammlungen von
Laternae Magicae und anderen fr&uuml;hen
Filmprojektoren beherbergte. Das Studio lag etwa
f&uuml;nfzig Meilen au&szlig;erhalb von Kopenhagen in
einem wundersch&ouml;nen Landhaus mitten im
Gr&uuml;nen. Die einzelnen Wohnr&auml;ume waren zu
Schneidekabinen umgebaut und mit der
modernsten Ausr&uuml;stung ausgestattet. In einer
Scheune war die Tonmischanlage untergebracht.
Coppola war begeistert; Lanterna Films war
genau das, was ihm vorschwebte. Er und Colby
beschlossen, dieses Environment nach Marin
County zu transferieren. <i>"Ich glaubte, dass die
neue Technologie genau die magische Zutat sein
w&uuml;rde, die uns den Erfolg garantieren w&uuml;rde. Wir
hatten die naive Vorstellung, dass es die
Ausr&uuml;stung war, die uns die Produktionsmittel
verschaffen w&uuml;rde. Nat&uuml;rlich lernten wir mit der
Zeit, dass es nicht die Ausr&uuml;stung, sondern das
Geld war."</i>
</p>


<p>
Auf der Heimreise machte Coppola 
Zwischenstation auf der Fotokina in K&ouml;ln, wo er aus einer
Laune heraus ein komplettes Tonmischstudio f&uuml;r
80.000 Dollar bestellte - ohne das Geld, einen
Platz f&uuml;r die Anlage oder Filme zu haben, die er
h&auml;tte nachvertonen k&ouml;nnen.<br>
Dieser sehr intuitive und spontane Umgang mit
Produktionsmitteln w&uuml;rde ihn bei zuk&uuml;nftigen
Projekten immer wieder in Schwierigkeiten bringen.
Eine treffende Umschreibung der Coppolaschen
Produktionspolitik lieferte sein alter Freund und
Kollege John Milius (Drehbuchautor von
APOCALYPSE NOW): <i>"Wie schon Talleyrand
&uuml;ber Napoleon sagte: er ist eben so gro&szlig;, wie
man ohne Tugend gro&szlig; sein kann."</i>
</p>


<p>
Im Juni 1969 begannen sich Coppola, Lucas und
Mona Skager (Produktionsassistentin bei RAIN
PEOPLE) im Raum San Francisco nach einem
gro&szlig;en viktorianischen Haus umzusehen, das als
Kopie des Lanterna-Geb&auml;udes in Betracht kam.
Als passenden Angebote ausblieben und die Zeit
immer mehr dr&auml;ngte (die teure Tonanlage w&uuml;rde
bald eintreffen), unterschrieb Coppola ohne
Z&ouml;gern einen langfristigen Mietvertrag f&uuml;r ein
freigewordenes Lagerhaus in der Innenstadt von
San Francisco. Um das n&ouml;tige Geld zusammenzukratzen, musste er sein eigenes Haus
verkaufen.<br>
American Zoetrope war geboren, aber Lanterna
Films war es nicht. (Einer Kopie der Lanterna-Studios kam etliche Jahre sp&auml;ter
George Lucas mit seiner in den Weinbergen von Lucas-Valley
gelegenen Skywalker-Ranch viel n&auml;her.... Lucas-Valley hie&szlig; &uuml;brigens
schon vor George Lucas so ...)<br>
Den Einfluss Lanternas reflektierte jedoch der
Name der neuen Company: ein Zoetrope ist ein
stroboskopischer Zylinder, der bewegte Bilder
entstehen l&auml;sst, w&auml;hrend er sich um seine eigene
Achse dreht. Der griechische Ursprung von
Zoetrope bedeutet au&szlig;erdem "lebendige
Bewegung", nach Coppolas Ansicht genau das
richtige Symbol f&uuml;r seine dynamische junge
Filmgesellschaft.</p>


<p>
Coppola war &uuml;berzeugt, dass Zoetrope die neue
Elite des Filmgesch&auml;fts werden w&uuml;rde. Lucas sah
darin eher eine Neugruppierung des Dreckigen
Dutzend von der Filmklasse der UCLA; er lud
John Milius, Matthew Robbins, Hal Barwood,
Willard Huyck, Gloria Katz und Walter Murch ein,
sich dem Abenteuer anzuschlie&szlig;en. Die Idee einer
verschworenen Gemeinde von Filmemachern, die
den moralischen Abgrund von Los Angeles hinter
sich lie&szlig;, faszinierte ihn.</p>


<p>
Lucas erinnert sich an die ersten chaotischen
Tage von Zoetrope: <i>"Es war, als wollte man den
Kindern zu Weihnachten ein Fahrrad zusammenbauen 
und w&uuml;sste, dass sie in einer Stunde
aufwachen. Man mu&szlig; sich unheimlich beeilen,
alles geht schief und die Gebrauchsanweisung ist
auch noch verschwunden."</i>
</p>


<p>
Doch Coppola hatte gro&szlig;e Pl&auml;ne f&uuml;r sein Studio.
Am 4. November 1969 wurde American Zoetrope
ins Handelsregister eingetragen. Der einzige
Aktion&auml;r der neuen Gesellschaft war Coppola
selber; gleichzeitig war er Pr&auml;sident. Lucas war
Vizepr&auml;sident und Mona Skager Schatzmeisterin.
Zusammen bildeten die drei ein ungew&ouml;hnliches
Direktorium: Coppola sch&auml;umte &uuml;ber vor
Begeisterung, Skager blieb cool und skeptisch
und Lucas war laut ihrer Einsch&auml;tzung ein
<i>"sch&uuml;chterner, ernsthafter Mensch, eher
unauff&auml;llig."</i>
</p>


<p>
&Uuml;ber Coppolas Firmenpolitik, junge Filmemacher -
h&auml;ufig Absolventen der Filmschule - unentgeltlich
f&uuml;r sich arbeiten zu lassen, kam es zur
Auseinandersetzung: nach Ansicht Coppolas
hatte Roger Corman einen Fehler gemacht, weil er
begabte junge Filmemacher nicht unter Vertrag
genommen und an sich gebunden hatte, so dass
viele gute Regisseure letztendlich umsonst f&uuml;r ihn
gearbeitet h&auml;tten. Diesen Fehler wollte er nicht
machen.<br>
Coppolas Wertesystem schien sich gewandelt
zu haben: war "Vertrag" noch vor einigen Jahren
ein schmutziges Wort f&uuml;r ihn gewesen, wollte er
nun <i>"eine Menge begabter junger Filmemacher
umsonst verpflichten, hoffen, dass einer ihrer Filme
ein Hit wird und das ganze Studio auf dieser
Basis aufbauen."</i> <small>(Lucas)</small>
</p>


<p>
Trotzdem wirkte Coppolas Enthusiasmus
ansteckend. Ger&uuml;chte an den Filmschulen der
UCS und der UCLA besagten, da&szlig; sich die
Zukunft der Filmindustrie bei Zoetrope abspiele.
Coppolas kleines Studio war ein Sammelbecken
neuer Ideen, mit denen er zu Warner Bros. ging
und sich um eine Finanzierung bem&uuml;hte.</p>


<p>
Eines der neuen Filmprojekte war THX 1138, die
Spielfilmfassung des Studentenfilms THX 1138:
4EB (ELECTRONIC LABYRINTH), den George
Lucas 1967 gedreht hatte und der den ersten
Preis beim Third Student Film Festival gewonnen
hatte.</p>


<p>THX ist ein in klinisches Wei&szlig; getauchter Science-Fiction 
Film, der in einem unterirdischen &Uuml;berwachungsstaat 
spielt, dessen Bewohner ihre Gef&uuml;hle durch die
Einnahme von Drogen regeln und keinen Sex mehr haben.
Als THX 1138 (Robert Duvall) und seine Frau LUH 3417
(Maggie McOmie) eines Tages ihre Drogen nicht mehr
nehmen, entdecken sie ihre Sexualit&auml;t. LUH wird schwanger
und, als dies herauskommt, get&ouml;tet. THX wird ins Gef&auml;ngnis
geworfen, den <i>white limbo</i>, einen unendlich weiten,
perspektivelosen wei&szlig;en Raum, dessen Bewohner wirr vor
sich hinreden. Als THX die Flucht gelingt, startet das
System eine gnadenlose Jagd auf ihn.</p>


<p>
Die Themen von Lucas' Deb&uuml;tfilm sind
altbekannt: das Verh&auml;ltnis zwischen Mensch und
Maschine, die Notwendigkeit, einer bedr&uuml;ckenden
Umgebung zu entfliehen und Verantwortung zu
&uuml;bernehmen, nicht nur in zwischenmenschlichen
Beziehungen, sondern auch f&uuml;r moralische
Konsequenzen. Lucas portr&auml;tiert eine repressive
Gesellschaft, die ihre B&uuml;rger einlullt, ihre
zuk&uuml;nftigen Generationen im Labor z&uuml;chtet und
Minderheiten auf den Stand von Untermenschen
reduziert. Hauptmotiv des Films ist der <i>white
limbo</i>, der keine anderen Schranken hat als die
&Auml;ngste seiner Insassen.</p>


<p>
THX 1138 war Coppolas Vorzeigeobjekt, sein
Beweis, dass das Studio funktionierte. Er trug den
Film zu Warner Bros. und f&uuml;hrte ihn dem
Verantwortlichen f&uuml;r Filmdeals - Ted Ashley - vor.
In seiner Tasche hatte er gleich noch sieben
weitere Projekte, f&uuml;r die er sich eine Finanzierung
erhoffte. Doch als Ashley den Film gesehen hatte,
wurde er so w&uuml;tend, dass er den ganzen
Zoetrope-Handel platzen lie&szlig; und sich weigerte,
auch nur einen Blick in die anderen Drehb&uuml;cher zu
werfen (Zwei von ihnen verfilmte Coppola sp&auml;ter als THE
CONVERSATION und APOCALYPSE NOW).<br>
Dieser Tag ging als Schwarzer Donnerstag in die
Geschichte von <i>American Zoetrope</i> ein. Es war
ein Schlag, von dem sich die kleine Gesellschaft
nie wieder ganz erholte. Lucas war verzweifelt:
man hatte ihm seinen Film weggenommen und ihn
verspottet (Warner nahm THX - nachdem man ihn
schon finanziert hatte - nat&uuml;rlich doch noch in den
Verleih, allerdings nicht, ohne vorher an dem Film
herumzuschneiden; dieser Schlag gegen seine
kreative Freiheit verletzte ihn mehr, als er zugeben
wollte). Au&szlig;erdem f&uuml;hlte er sich f&uuml;r das Schicksal
von Zoetrope mitverantwortlich.<br>
Coppola floh nach Europa, um seine Wunden zu
lecken.</p>


<p>
THX lief zun&auml;chst gar nicht so schlecht, reduzierte
sich dann aber auf den Status eines Kultfilms, der
h&ouml;chstens noch in den Mitternachtsvorstellungen
der Programmkinos gezeigt wurde. <i>American
Zoetrope</i> produzierte noch f&uuml;r eine kurze Zeit
Commercials, Pornos, Lehr- und Dokumentarfilme,
bevor das Studio 1970 geschlossen wurde.<br>
In dieser pers&ouml;nlichen und finanziellen Krise
erreichte Coppola das rettende Angebot: er sollte
im Auftrag von Paramount Mario Puzos Bestseller
THE GODFATHER verfilmen.</p>



<h4>Die Zerst&ouml;rung der Pers&ouml;nlichkeit:<br>
THE CONVERSATION (1973)</h4>


<p>
Nach dem gescheiterten Experiment mit American
Zoetrope und dem kassensprengenden Erfolg
des PATEN unternahm Francis Coppola einen
zweiten Versuch, sich (wenigstens teilweise) vom
etablierten Studiosystem zu l&ouml;sen. 1972 gr&uuml;ndete
er zusammen mit seinen Regiekollegen William
Friedkin und Peter Bogdanovich die <i>Director's
Company</i>. Paramount beteiligte sich mit 31,5
Millionen Dollar und sicherte sich damit die H&auml;lfte
der erwarteten Box-office Gewinne.<br>
Daf&uuml;r verpflichtete sich das damals 
hoffnungsvollste Triumvirat des Neuen Hollywood, in den
kommenden zw&ouml;lf jahren insgesamt zw&ouml;lf Filme
zu drehen. Die ersten Produkte der Company
waren PAPER MOON von Bogdanovich und
THE BUNKER HILL BOYS von Friedkin.
Coppolas Beitrag war THE CONVERSATION,
ein pers&ouml;nlicher Film, an dessen Drehbuch er
bereits seit 1966 gearbeitet hatte.</p>


<p>Mittagspause auf dem Union Square in San Francisco. Ein
Paar schlendert &uuml;ber den Platz, vertieft in ein Gespr&auml;ch.
Anonym und gesch&uuml;tzt inmitten der vielen Menschen
glauben sie sich an einem Ort der gr&ouml;&szlig;tm&ouml;glichen
&Ouml;ffentlichkeit und zugleich der gr&ouml;&szlig;tm&ouml;glichen Intimit&auml;t.
Doch gerade diese Intimit&auml;t wird systematisch zerst&ouml;rt: das
Gespr&auml;ch des Paares wird abgeh&ouml;rt, von allen Seiten sind
Mikrofone auf die beiden gerichtet. Leiter dieser Aktion ist
Harry Gaul (Gene Hackman), ein gefragter Abh&ouml;rspezialist
und Meister seines Fachs. In seinem Tonstudio f&auml;hrt Harry
mehrere Aufnahmen des Gespr&auml;chs zu einem Band
zusammen. Dieses bringt er seinem Auftraggeber. Als
dieser jedoch nicht da ist, nimmt er das Band wieder mit. In
seiner Werkstatt &uuml;berpr&uuml;ft Harry noch einmal die
Tonb&auml;nder. Aus dem bisher vom Stra&szlig;enl&auml;rm &uuml;bert&ouml;nten
Satz <i>"He'd kill us if he got the chance."</i> 
glaubt Harry die Anzeichen einer bevorstehenden 
Gewalttat herauszuh&ouml;ren.
In ihm w&auml;chst der Verdacht, durch seine Arbeit einem
Verbrechen Vorschub zu leisten. Allm&auml;hlich gibt er seine
passive Betrachterrolle auf und verstrickt sich immer mehr
in einem Klima aus Misstrauen und Bedrohung.</p>


<p>THE CONVERSATION ist ein Film &uuml;ber den
Verlust von Intimit&auml;t und die Zerst&ouml;rung der
Pers&ouml;nlichkeit, also ein Film &uuml;ber Gewalt.<br>
Die Gewalt ist zun&auml;chst nur ein Verdacht, eine
Bef&uuml;rchtung, die aus dem Satz <i>"He'd kill us if he
got the chance."</i> erw&auml;chst und den ganzen Film
&uuml;ber pr&auml;sent ist (durch das wiederholte Abh&ouml;ren
des Bandes).</p>


<p>Harrys Aufgabe ist es, das Leben anderer
Menschen offenzulegen. Dass er es dabei sogar
zerst&ouml;ren kann - bei einer fr&uuml;heren Abh&ouml;raktion
waren aufgrund seiner Arbeit drei Menschen
ermordet worden - verdr&auml;ngt er. Zweifel an seiner
T&auml;tigkeit darf er sich nicht leisten. Sein 
wirkungsvollstes Schutzschild ist die Anonymit&auml;t. Die
Menschen, die er im Auftrag anderer belauscht,
bleiben f&uuml;r ihn Objekte; er will und darf sie nicht
kennen.<br>
Die Angst vor Kontakten pr&auml;gt auch sein
Privatleben: niemand kennt seine Telefonnummer,
niemand hat einen Zweitschl&uuml;ssel zu seiner
Wohnung; als einmal beide Tabus verletzt
werden, reagiert er mit Panik.</p>


<p>Coppola hat THE CONVERSATION zwischen
den beiden PATEN Filmen gedreht. In allen dreien
arbeitet er mit seinen bevorzugten Ikonen.<br>
In THE CONVERSATION geht es um die
zerrissene Familie. Im ersten Teil des PATEN hatte
sich die Zerst&ouml;rung der Familie (von innen heraus)
bereits angedeutet: Michael Corleone hat im
Machtkampf um die Stellung der Corleones den
Rat seines Vaters, die Familie als h&ouml;chstes Gut zu
besch&uuml;tzen, missachtet und seinen Schwager
ermorden lassen (in DER PATE. TEIL II wird er
sogar seinen Bruder erschie&szlig;en lassen).<br>
Harry Caul geht sogar noch weiter: aus Angst vor
Verwundbarkeit hat er erst gar keine Familie
gegr&uuml;ndet. Er hat jegliche Privatsph&auml;re und
Intimit&auml;t aus seinem Leben verbannt. Als 
Abh&ouml;rspezialist wei&szlig; er, wie angreifbar und 
verwundbar sie den einzelnen machen kann.<br>
Wie Natalie in THE RAIN PEOPLE und Michael
Corleone ist auch Harry Caul eine isolierte Figur.</p>


<p>In THE CONVERSATION geht es au&szlig;erdem um
Zeit (die Zeit, die gegen Harry arbeitet) und um
Geld (das als Motiv f&uuml;r die Gewalttat angesehen
werden kann).</p>


<p>Auch in den PATEN Filmen geht es um Zeit (die
Sehnsucht nach einer vergangenen, besseren
Zeit) und um Geld (wobei Geld vom umfassenderen Begriff Macht abgel&ouml;st wird).<br>
Das Wasser spielt ebenfalls eine Rolle und wird,
wie in THE RAIN PEOPLE und THE CONVERSATION, 
mit Gewalt in Verbindung
gebracht: als Michael Corleone am Ende von THE
GODFATHER. PART I seinen Sohn taufen l&auml;&szlig;t,
und gleichzeitig die Feinde der Familie liquidiert
werden, wird die Verbindung von Wasser und
Blut zun&auml;chst mittels einer Parallelmontage
hergestellt.<br>
Als Harry Gaul in THE CONVERSATION in
einem Hotelzimmer die Wassersp&uuml;lung bet&auml;tigt,
quillt ihm Blut entgegen und &uuml;bersp&uuml;lt den Boden.
Hier sind Wasser und Blut eine direkte Verbindung
eingegangen.</p>


<p>Der Regen hat noch eine andere Bedeutung:
Er ist standig pr&auml;sent, auch wenn es eigentlich nie
regnet. Trotzdem tr&auml;gt Harry Caul fast den
ganzen Film &uuml;ber einen Regenmantel. Der Sinn
dieses Regenmantels erschlie&szlig;t sich am ehesten
&uuml;ber Harrys Nachnamen: "caul" ist das englische
Wort f&uuml;r "Gl&uuml;ckshaube". Eine Gl&uuml;ckshaube ist
<i>"die unverletzte Eihauth&uuml;lle, in der das Kind bei
ausgebliebenem Blasensprung geboren wird; die
Gl&uuml;ckshaube muss sofort zerrissen werden, um
ein Ersticken zu verhindern... der Volksglaube
spricht so geborene oder mit Resten der
Eihauth&uuml;lle behaftete Kinder als Gl&uuml;ckskinder an,
zumal wenn sie im Besitz der Gl&uuml;ckshaut bleiben
oder Teile in die Kleider vern&auml;ht bekommen."</i> <small>(aus: Brockhaus Enzklop&auml;die in zwanzig B&auml;nden. Band 7)</small>
</p>


<p>Ausgehend von diesem Gedanken ergibt sich
eine interessante M&ouml;glichkeit, den Charakter
Harry Caul zu verstehen: Harry ist das
Gl&uuml;ckskind, das seine Gl&uuml;ckshaube noch tr&auml;gt, in
Gestalt seines Regenmantels aus Plastik.<br>
Der amerikanische Filmkritiker James W. Palmer
baut auf dieser Verbindung des Namens "Caul"
mit dem Plastikmantel eine ganze Interpretation
von THE CONVERSATION auf: er sieht den Film
als <i>"die Biographie eines ungeborenen Mannes,
eine Studie &uuml;ber Harrys erfolglosen Kampf, sich
selbst als einen moralischen Menschen zu
geb&auml;ren."</i> <small>(James W. Palmer: THE CONVERSATION: Coppola's Biography of an Unborn Man. in: Film Heritage, Vol. 12, Nr.1)</small>
</p>


<p>
<i>"Dieser Abh&ouml;rspezialist Harry Gaul, der von
einem Agenten des Systems schliesslich zu
dessen Opfer wird, der die kompliziertesten
Apparate bedient und gleichwohl in einem Vakuum
lebt, gepeinigt von untergr&uuml;ndigen &Auml;ngsten, vom
Zweifel und vom Verdacht, geh&ouml;rt zu den
interessantesten Figuren des neuen
amerikanischen Kinos."</i> <small>(Ulrich Gregor in "Geschichte des Films ab 1960", Band 4)</small>
</p>


<h4>Der letzte Tycoon</h4>


<p>Eingerahmt wird THE CONVERSATION von
THE GODFATHER, PART I und PART II - ein
pers&ouml;nlicher Film zwischen zwei Auftragsfilmen,
mit denen er so etwas wie eine Symbiose
eingeht: nur durch die Arbeit f&uuml;r die gro&szlig;en
Studios kann sich Coppola immer wieder seine
pers&ouml;nlichen Projekte verwirklichen; gleichzeitig
entwickelt er auch in seinen Auftragsfilmen seine
pers&ouml;nlichen Themen weiter.<br>
Alle Figuren Coppolas haben ein besonderes
Verh&auml;ltnis zur Ikonografie seiner Filme; sie
definieren sich durch sie. So werden auch seine
Auftragsfilme beinahe zu pers&ouml;nlichen Filmen.</p>


<p>Coppola ist sich der M&ouml;glichkeiten und Gefahren
seiner Zugest&auml;ndnisse durchaus bewusst.
W&auml;hrend andere Filmemacher der selben &Auml;ra den
Konsens scheuten und zu Hollywood-Au&szlig;enseitern 
wurden (Monte Hellman, Sam Peckinpah), 
oder einfach von den Bedingungen
der Unterhaltungsindustrie aufgefressen wurden
(Matthew Robbins, John Korty) hat sich Coppola
als letzter Tycoon der Filmgeschichte etabliert.
Er hat es seiner Originalit&auml;t hinsichtlich seiner
Themen und Stoffe, aber auch seinem Kalk&uuml;l zu
verdanken, dass er nie zu einem reinen 
Auftragsregisseur, zu einer <i>hired gun</i> wurde.</p>


<a name="sec-bbsfilms"></a>
<h3>Die BBS-Gruppe und ihre Filme</h3>


<p>Einer  der  Schl&uuml;sselfilme  des  New
Hollywood und  der erste  Film,  der
von  der  BBS-Gruppe  produziert
wurde, war EASY  RIDER - offiziell
eine  Rybert-Produktion.  Die  
Rybert-Produktionsgruppe war eine Gr&uuml;ndung
von Bob Rafelson und Bert Schneider,
der  Sohn  des  langj&auml;hrigen  Vorsitzenden
der  Columbia-Pictures Abraham  Schneider.  
Rafelson  und Schneider  produzierten  seit  Mitte
der 60er Jahre die Fernsehserie THE
MONKEYS,  eine  Beatles-Parodie,
sowie nach deren Absetzung den Film
HEAD  (1968), ebenfalls  &uuml;ber  die
Monkeys.</p>


<p>Nach dem gro&szlig;en Erfolg von EASY
RIDER gab die Columbia Bob Rafelson,
Bert  Schneider  und  Steve  Blauner
(die Vornamen ergaben den  Namen
der  BBS-Produktionsgruppe)  praktisch  
einen  Freibrief zur  Produktion
weiterer  Filme  -  ohne  Auflagen
bez&uuml;glich Form und Inhalt. Daraufhin
entstanden zwischen 1970 und 1972
f&uuml;nf Filme; FIVE EASY PEACES (Ein
Mann  sucht sich  selbst,  1910)  und
THE KING OF MARVIN GARDENS (1972)
von Bob Rafelson , DRIVE, HE SAID
(1971) von Jack Nicholson, A SAFE
PLACE (1971) von Henry Jaglom und
THE LAST PICTURE SHOW (Die letzte
Vorstellung,  1971)  von  Peter
Bogdanovich. Letzterer war innerhalb
der  BBS  eher eine  Randfigur -  er
wurde  aufgrund  seines  Erstlingsfilmes  
TARGETS  (eine  Corman-Produktion, s. dort)
eingeladen, einen
Film f&uuml;r die BBS zu drehen.</p>


<p>Viel  st&auml;rker  als  die  zahlreichen
<i>youth movies</i>, die als Nachzieher von
EASY RIDER in der Hoffnung &auml;hnlicher
Erfolge  entstanden,  besch&auml;ftigen
sich  die von der  BBS-Gruppe
produzierten  Filme  mit dem
Versagen des amerikanischen
Traumes, mit dem ersch&uuml;tterten
Vertrauen in die amerikanischen
Ideale. Und dabei verlieren sie sich
nicht in modischen Aspekten, bleiben
nicht an der Oberfl&auml;che, sondern sie
versuchen, das  Innenleben der
amerikanischen Gesellschaft, die
psychischen  Defekte,  die  <i>"Anatomie
der Entfremdung"</i> <small>((1))</small> zum Ausdruck
zu bringen.  Dabei entstand eine Art
gemeinsamer Stil, eine f&uuml;r die BBS-Gruppe   
typische   Handschrift,
wodurch  sie  sich  z.B.  f&uuml;r  den
Filmkritiker H.-C. Blumenberg als die
<i>"wahre  Avantgarde  des  
amerikanischen  Films"</i> <small>((1))</small> dieser  Zeit
erwiesen.  Gro&szlig;e  Vorbilder  waren
dabei  europ&auml;ische  Regisseure  wie
Antonioni, Bergmann und die
Regisseure der franz&ouml;sischen
<i>nouvelle vague</i>.</p>


<p>FIVE EASY PIECES war neben THE
LAST  PICTURE SHOW die erfolgreichste
aller  BBS-Produktionen  (es
waren  auch  die  einzigen,  die
seinerzeit  den  Sprung  nach
Deutschland  schafften).   Jack
Nicholson  spielt  darin  einen
Pianisten,  Robert Dupea,  den Sohn
einer Musiker-Familie.  Er f&uuml;hlt sich
in seiner Umgebung  und in seinem
Beruf nicht zuhause und durch den
R&uuml;ckzug in seine famili&auml;re Umgebung
versucht  er,  sich  selbst  zu  finden.
Wie viele der Figuren in den BBS-Filmen 
scheitert er aber auf seiner
Suche.</p>


<p>DRIVE, HE SAID ist ein episodenhaftes Werk &uuml;ber eine Gruppe
Studenten, die gegen die Sportbesessenheit ihrer Universit&auml;t
revoltieren. Der m&ouml;rderische Druck
der gesellschaftlichen Zust&auml;nde
treiben den Anf&uuml;hrer in den Irrsinn.<br>
Die f&uuml;r das damalige Zielpublikum zu
realistische und zuwenig romantische 
Darstellung des Universit&auml;tslebens 
war wohl mit ein Grund f&uuml;r
den finanziellen Reinfall des Films.</p>


<p>A SAFE PLACE - der Titel des Filmes
von Henry Jaglom benennt eines der
zentralen Motive der BBS-Filme.<br>
Auch hier zieht sich die Heldin an
einen f&uuml;r sie "sicheren Ort" zur&uuml;ck:
sie flieht vor der Realit&auml;t in eine
Phantasiewelt (mit Orson Welles als
Zauberer). Der Film springt zwischen
Vergangenheit, Gegenwart und Traum
hin und her und entfernt sich dabei
sehr weit vom traditionellen Erz&auml;hl-Kino. Laut J. Monaco, einem amerikanischen 
Filmkritiker, resultierte
dabei ein <i>"pubert&auml;rer Film voll
derber Symbolik"</i>, der f&uuml;r die BBS das
Erreichen des "Tiefpunktes" bedeutete (2). 
Diese Kritik zeigt das
Unverst&auml;ndnis, auf das die Filme der
BBS selbst bei der amerikanischen
Kritik stie&szlig;en (s.u.).</p>


<p>Im Zentrum der in den 50er Jahren in
Anarene spielenden Handlung von THE
LAST PICTURE SHOW stehen drei
Jugendliche auf der Schwelle zum
Erwachsensein: der orientierungslose
Sonny, den der Zufall in die Arme der
verh&auml;rmten Frau seines Footballtrainers 
treibt, sein Freund Duane
und die von ihm geliebte Jacy, die in
ihrer Lebensgier unf&auml;hig ist, eine
echte Beziehung einzugehen. Der
Ruhepunkt f&uuml;r alle ist das Kino der
Stadt und sein Betreiber Sam. Aber
mit der Schlie&szlig;ung des Kinos werden
auch die gescheiterten Tr&auml;ume der
Jugendlichen deutlich. Es bleibt am
Ende nur die Anpassung, die Faust in
der Tasche.</p>


<p>THE KING OF MARVIN GARDENS
beginnt mit einer vier Minuten langen
Gro&szlig;aufnahme, in der Jack Nicholson
(er spielt einen Discjockey) eine
traumatische Erfahrung aus seiner
Kindheit erz&auml;hlt. Sp&auml;ter sitzt er mit
seinem Bruder im winterlich
verlassenen Atlantic City und beide
tr&auml;umen von einem fernen Insel-Paradies, 
ihrem "sicheren Platz". Bob
Rafelson selbst bezeichnet seinen
Film als "kafkaesken Alptraum".</p>


<p>Die Filme der BBS zeichnen sich
durch einen ungew&ouml;hnlichen Realismus 
der Figuren, durch einen sehr
pers&ouml;nlichen, z.T. fast 
autobiographischen Inhalt und durch die
Aufl&ouml;sung klassischer 
Erz&auml;hlstrukturen aus. Gerade die
Realit&auml;tsn&auml;he der Filme (abgesehen
von dem z.T. eher experimentellen
Stil der Filme) bereitete sowohl dem
breiten (Durchschnitts-)Publikum als
auch den Filmkritikern zuweilen
Probleme, der Misserfolg von DRIVE,
HE SAID wird zum Teil auch darauf
zur&uuml;ckgef&uuml;hrt. Selbst Kritiker, die
ansonsten die Regisseure des New
Hollywood unterst&uuml;tzten, lie&szlig;en
unmissverst&auml;ndlich Kritik laut
werden: Pauline Kael z.B. sprach von
den <i>"talentierten Leuten, deren Filme
chaotische Desaster"</i> seien (sie
spielte damit vor allem auf DRIVE,
HE SAID an) und bezeichnete die 
BBS-Produktionen als <i>"unordentliche,
halb-langweilige, verhei&szlig;ungsvolle
Fehlschl&auml;ge"</i> (1).</p>


<p>Ein weiterer Grund f&uuml;r das Aus der
BBS-Gruppe im Jahr 1972 war jedoch
auch die ungeschickte Vermarktung
der Filme. So wurde zum Beispiel
DRIVE, HE SAID, der im Uni-Milieu
spielt, in den Semesterferien in
Hollywood in die Kinos gebracht und
nicht w&auml;hrend des Semesters in
einem Kino eines Studentenviertels.
Und f&uuml;r A SAFE PLACE wurde so
schlecht geworben (er wurde als
Antwort auf LOVE STORY angepriesen), 
dass die Werbung das
eigentliche Zielpublikum des Films
nicht erreichte. So waren bis auf
FIVE EASY PIECES und THE LAST
PICTURE SHOW alle Filme ein
finanzielles Fiasko und die Columbia
zog sich aus der Finanzierung zur&uuml;ck.
Danach hatten die Regisseure
Schwierigkeiten, neue Projekte zu
finanzieren - Jack Nicholson (der
h&auml;ufig als die zentrale Figur der BBS
gilt) zog sich f&uuml;r lange Zeit auf die
Schauspielerei zur&uuml;ck und auch bei
Henry Jaglom und Bob Rafelson
dauerte es ein paar Jahre, bis sie
sich wieder an die Regie neuer Filme
wagten. Nur Peter Bogdanovich
drehte zun&auml;chst weiter neue Filme.</p>


<h4>Peter Bogdanovich - ein kurzes Portrait</h4>


<p>Bogdanovich wurde 1939 als Sohn
jugoslawischer Einwanderer in New
York geboren. Nach einem Schauspielstudium 
in New York inszenierte
er ab 1958 verschiedene St&uuml;cke u.a.
von Tennessee Williams an 
Off-Broadway-Theatern. Schon fr&uuml;h
begann er, f&uuml;r Kinos Filmtexte zu
schreiben - zun&auml;chst gegen freien
Eintritt. Dies erm&ouml;glichte ihm, vor
allem &auml;ltere Filme umsonst zu sehen.
Von 1958 bis 1968 arbeitete er als
Filmkritiker vor allem f&uuml;r den
Esquire und ver&ouml;ffentlichte
verschiedene Regisseurmonographien.<br>
Nach seinem Umzug Anfang der 60er
Jahre nach Los Angeles begann er
eifrig Beziehungen zu kn&uuml;pfen. Er
hoffte, als Schauspieler entdeckt zu
werden (er spielte nur in einem Film
mit und zwar in Orson Welles' THE
OTHER SIDE OF THE WIND, der noch
heute in einem Pariser Tresor liegt)
oder eines seiner gro&szlig;en Idole
kennenzulernen. Dabei trug er sich
schnell den Ruf ein, sich etwas zu
vehement bei ihnen einzuschmeicheln.</p>


<p>F&uuml;r Roger Corman arbeitete er
schlie&szlig;lich u.a. als Kameramann f&uuml;r
Spezialaufnahmen sowie als Autor
und drehte f&uuml;r ihn 1967 TARGETS,
seinen ersten und f&uuml;r viele auch
besten Film. Nach seiner Arbeit f&uuml;r
die BBS-Gruppe gr&uuml;ndete er
zusammen mit Coppola (der zuvor
mit seiner Produktionsfirma
American Zoetrope gescheitert war)
und Friedkin die Director's Company.</p>


<p>Schon in THE LAST PICTURE SHOW
zeigt sich Bogdanovichs Vorliebe f&uuml;r
Genres und Zeitstile. Dies resultiert
sicherlich aus seiner Verehrung der
"alten Meister" wie Howard Hawks
und John Ford und nat&uuml;rlich aus
seiner Vorliebe f&uuml;r Hollywood-Filme
schlechthin. Angeblich will er 6000
der 20000 Produktionen bis Ende der
60er Jahre gesehen haben. Das
Ergebnis:</p>


<p>WHAT'S UP DOC (1971) ist der
Versuch, eine <i>screwball</i>-Kom&ouml;die
wie BRINGING UP BABY von Howard
Hawks zu inszenieren. PAPER MOON
(1972) ist ein klassisches <i>period
picture</i>, das in den 30er Jahren
spielt, DAISY MILLER (1973) ein
Kost&uuml;mfilm und schlie&szlig;lich AT LONG
LAST LOVE (1974), ein richtiges
Musical im Stil der 30er und 40er
Jahre (ein geplanter Western mit
John Wayne, Henry Fonda und James
Stewart kam nie zustande, weil
Wayne die Zusammenarbeit mit Fonda
ablehnte).<br>
Er selbst sagt: <i>"Ich richte mich
niemals nach meinen Zeitgenossen,
sondern nach den 20000 Filmen, die
bislang gemacht worden sind und von
denen ich 6000 gesehen habe. Ob das,
was ich mache, gut ist, w&auml;ge ich ab
gegen die Regisseure, die ich
bewundere..."</i> (3).<br>
Nach dem Ende der BBS-Gruppe war
Bogdanovich der einzige der
Regisseure, der - zun&auml;chst - ohne
Schwierigkeiten weitermachen
konnte. Er hoffte, mit
konventionellen Filmen Erfolg zu
haben, indem er seinen gro&szlig;en
Vorbildern nacheiferte. Dadurch ging
jedoch die Originalit&auml;t seines ersten
Filmes immer mehr verloren. WHAT'S
UP DOC und PAPER MOON blieben bis
heute seine erfolgreichsten Filme.</p>

      
<p>&nbsp;</p>
<hr>
<h3>Literatur / Quellenangaben</h3>
<ul>
<li>
<small>(1) New Hollywood, Hanser Verlag, Reihe Film 10, 1974</small>
</li>
<li>
<small>(2) James Monaco: American Film Now, Hanser Verlag, 1985</small>
</li>
<li>
<small>(3) filmkritik Heft 1/1973, S.18</small>
</li>
</ul>
      

<a name="sec-nhbobby"></a>
<h3>Meister der Verwandlung: Robert de Niro</h3>


<p>Der Mann ist ein Ph&auml;nomen. 
<i>"In Gegenwart
einer Jazzband verwandelt er sich in einen
schwarzen Jazz-Trompeter. Trifft er einen
Indianer, wird er zur Rothaut mit einer Feder
am Hinterkopf. Kommt er mit dicken Leuten in
Kontakt, steigert er prompt sein K&ouml;rpergewicht auf
250 Pfund. In Gesellschaft von Politikern sieht er aus
wie ein Politiker....."</i> <small>((Kath. Filmdienst))</small>
<br>
Der Schauspieler Robert de Niro macht im wahrsten
Sinne des Wortes Metamorphosen durch, um eine
Rolle zu verk&ouml;rpern. Dazu greift er auf eigene
Erfahrungen zur&uuml;ck bzw. verschafft sich diese, indem
er z.B. f&uuml;r seine Rolle in
DER PATE, TEIL II nach
Sizilien f&auml;hrt, um sich dort
den sizilianischen Dialekt
anzueignen. Diese Liebe
zum Detail, die De Niro
einbringt, verleihen den
von ihm verk&ouml;rperten
Charakteren Tiefe und
Glaubw&uuml;rdigkeit.</p>


<p>1943 in New York,
Little Italy, geboren, wuchs
De Niro in zwar
wirtschaftlich mageren,
kulturell jedoch um so
reicheren Verh&auml;ltnissen auf.
Seine Mutter, Herausgeberin einer
literarischen Zeitschrift und
Malerin, sein Vater,
ebenfalls Maler und
Bildhauer, trennten sich
jedoch schon bald nach der 
Geburt des Sohnes, und
"Bobby" wuchs allein bei
seiner Mutter in Little Italy
auf. Der etwas scheue
Junge verbrachte seine
Freizeit mit Freunden aus einer "Stra&szlig;enbande". Mit
ca. 10 Jahren entdeckte De Niro dann seine
Leidenschaft f&uuml;r die Schauspielerei, als er n&auml;mlich im
Schultheater in dem St&uuml;ck "Der Zauberer von Oz"
auftrat. Dadurch kam er auch in Kontakt zu Stella
Adler und Erwin Piscator die eine Gruppe namens
"The Dramatic Workshop" leiteten. Besonders Stella
Adler begleitete ihn auf seinem weiteren Weg und
f&ouml;rderte seine Karriere.<br>
Sechs Jahre sp&auml;ter, De Niro hatte mittlerweile etliche
Male die Schule gewechselt, um sich seine
<i>"Ausbildung so zusammenzusuchen, wie er sie zu
ben&ouml;tigten glaubte"</i>, verlie&szlig; er die Schule ganz, um
eine Karriere als Schauspieler zu beginnen. Mit
kleinen Engagements jenseits des Broadways an
noch kleineren Dinner-Theatern hielt er sich die
ersten Jahre &uuml;ber Wasser - h&auml;ufig nicht einmal als
Schauspieler, sondern nur als Handlanger, was laut
seiner eigenen Aussage eine wichtige Erfahrung war:
<i>"Eine Menge Darsteller fanden es unter ihrer W&uuml;rde.
Aber ich machte Erfahrungen, w&auml;hrend ich wartete
und nicht spielte. Ich lernte genau dabei viel &uuml;ber
den Zauber des Spielens und die Profanit&auml;t des
Nicht-Spielens. Ich begriff, was es hei&szlig;t, endlich mit
der Darstellung zu beginnen."</i>
<br>
Seit 1960 besuchte der leidenschaftliche
Amateur-Mime regelm&auml;&szlig;ig Schauspielkurse am
renommierten New Yorker "Actor's Studio" und an
Stella Adlers "Conservatory of Acting" wo er viel &uuml;ber
die Stanislawski-Methode lernte: Nach Stanislawski
muss der Schauspieler in die Gef&uuml;hle und Haltungen
seiner Charaktere eindringen und verwandte
Emotionen und Reaktionen aus seinem eigenen Leben
einbringen. De Niro ist daf&uuml;r ein Paradebeispiel.</p>


<p>Auch De Niro wurde sozusagen entdeckt,
n&auml;mlich von der altgedienten Hollywood-Schauspielerin Shelley
Winters, als er an einem der Off-Broadway Theater
arbeitete. 1963 kam er dann zum Film: Ebenfalls
ein Deb&uuml;tant in seinem Gebiet, engagierte der
junge Regisseur Brian de Palma De Niro f&uuml;r eine
Rolle in THE WEDDING PARTY, die ihm zwar nicht
zum gro&szlig;en Durchbruch verhalf, doch seine
Bekanntheit steigerte. THE WEDDING PARTY war
auch der Beginn einer langj&auml;hrigen Zusammenarbeit
zwischen De Palma und De Niro, denn schon 1966
folgte GREETINGS. Hier spielt De Niro den jungen
New Yorker Jon Rubin, der, wie seine Freunde, mit
allerlei Tricks versucht, dem Wehrdienst zu
entkommen. Bis es soweit ist, schl&auml;gt er die Zeit tot,
u.a. indem er Pornos dreht. Doch auch Jon wird
schlie&szlig;lich nach Vietnam eingezogen. GREETINGS
war so erfolgreich, - er wurde z.B. mit einem
"Silbernen B&auml;ren" ausgezeichnet - dass sich De
Palma zu einer Fortsetzung entschloss: In HI MOM!
ist Jon Rubin aus Vietnam zur&uuml;ckgekehrt und beginnt
seinen Nachkriegsalltag als Filmemacher; sein
berufliches als auch privates Scheitern veranlassen
ihn am Ende zum Amoklauf.<br>
Obwohl er auch am Off-Broadway von den Kritikern
einhellig gelobt wurde, zog es De Niro immer wieder
zum Film zur&uuml;ck und mit der Rolle des Jon Rubin
erhielt er seine erste Hauptrolle, die ihm gleichzeitig
auch zum Durchbruch verhalf. HI MOM! wirkte wie
ein zeitgen&ouml;ssischer Kommentar zu der
Desorientierung der Jugend in der
nordamerikanischen Gesellschaft w&auml;hrend des
Vietnamkrieges. Der Film stellte eine scharfe, mit
radikalen Ansichten vorgebrachte Kritik an der
geistigen und moralischen Verfassung dieser
Gesellschaft dar. (Vip-Cinema, R. De Niro).</p>


<p>Nach HI MOM! folgten BLOODY MAMA (1969) von
Roger Corman und f&uuml;nf weitere Filme, in denen De
Niro mit Brillianz Nebenrollen verk&ouml;rperte. 1973 dann
ein weiterer gro&szlig;er Erfolg: Unter der Regie von
Martin Scorsese spielte De Niro an der Seite von
Harvey Keitel den Kleingangster
Johnny Boy in HEXENKESSEL.
Der Film spielt im New Yorker
Stadtteil Little Italy
und ist somit ein sehr pers&ouml;nliches
Werk, sowohl f&uuml;r De Niro als auch
f&uuml;r Martin Scorsese, die
beide aus diesem Stadtteil stammen.
Beide hatten sich schon Ende der
f&uuml;nziger Jahre in NewYork
kennengelernt, doch erst jetzt
entwickelte sich neben der
beruflichen Beziehung eine tiefe Freundschaft.
Durch HEXENKESSEL war Francis Ford Coppola
auf De Niro aufmerksam geworden und bot ihm die
Rolle des jungen Don Vito Corleone in DER PATE,
TEIL II (1974) an, die ihm einen Oscar einbrachte.
Die Story des Filmes ist allgemeinhin bekannt: Vito
Corleone alias Robert de Niro kommt als armer
sizilianischer Einwanderer nach New York. Schon
gleich zu Beginn hebt er sich von den <i>"glamour&ouml;sen,
geheimnisvoll ausgeleuchteten Gestalten ab, von
denen der Film bis dahin erz&auml;hlt hat"</i>. Seine Gestik
verr&auml;t die Entwicklung vom proletarischen Underdog
zum neuen Paten: Die w&uuml;rdevolle Haltung, <i>"ein
Heben des Zeigefingers, eine Wegwerfbewegung mit
der ganzen Hand, ..., der Zeigefinger an die Schl&auml;fe
gelegt"</i>. Mit der kaltbl&uuml;tigen Ermordung des
erpresserischen Patrons des Viertels macht er einen
gro&szlig;en Schritt in seiner Entwicklung zum "Paten".
Wie schon eingangs erw&auml;hnt geh&ouml;rte zu De Niros
Vorbereitung auf die Rolle eine Reise nach Sizilien,
wo er sich sowohl mit Dialekt als auch mit der
landes&uuml;blichen Gestik vertraut machte. Das
Ergebnis ist verbl&uuml;ffend!</p>


<p>In den darauffolgenden zwei Jahren drehte De Niro
unter der Regie von Bernado Bertolucci "1900"
(1975/76) und zusammen mit Freund Scorcese TAXI
DRIVER (1976). Trotz mehrerer eintr&auml;glicherer
Angebote entschied sich De Niro f&uuml;r TAXI DRIVER,
zum einen um Freund Scorcese einen Gefallen zu
tun, jedoch haupts&auml;chlich, weil ihn die Story
faszinierte. Um sich mit der Rolle des Travis Bickle
vertraut zu machen, erwarb De Niro extra den
Taxischein und verbrachte zahllose N&auml;chte hinter
dem Steuer in den Stra&szlig;en New Yorks.<br>
Es folgten die Filme DER LETZTE TYCOON
(1976), NEW YORK, NEW YORK (1977) von M.
Scorcese, THE DEER HUNTER (1978) von Michael
Cimino, RAGING BULL (1980) ebenfalls von M.
Scorcese. In THE DEER HUNTER spielt De Niro
den Stahlkocher Michael Vronsky. Zusammen mit
einigen Freunden geht Michael zu Beginn des Filmes
auf Hirschjagd und der Zuschauer erkennt sofort den
geborenen J&auml;ger in ihm. Die Jagd ist eine
M&ouml;glichkeit seiner <i>"kleinen, mausgrauen,
sozialgebundenen Existenz zu entkommen"</i>.
Schon bald ist nicht mehr der Hirsch das
alleinige Opfer und Michael und seine Freunde
erfahren die H&ouml;lle auf Erden, den Vietnamkrieg
(dt. Titel: DIE DURCH DIE H&Ouml;LLE GEHEN).
Regisseur Michael Cimino
wurde h&auml;ufig wegen der Brutalit&auml;t und Gewalt des
Filmes kritisiert, doch Ziel Ciminos ist nicht deren
Verherrlichung. Beim Anblick des T&ouml;tens und
Mordens kommen beim Zuschauer keine
Siegesgef&uuml;hle auf, denn man erkennt, dass in diesem
Krieg eigentlich alle Verlierer sind. Aus Vietnam
zur&uuml;ckgekehrt wird Michael als Held empfangen.
Doch der Mensch Michael Vronsky hat sich ver&auml;ndert
und die schreckliche Erfahrung des Krieges noch
lange nicht &uuml;berwunden. Er geht zwar wieder auf
Hirschjagd, doch die Jagd ist f&uuml;r ihn schon seit
langem zu Ende. Er will nicht mehr t&ouml;ten: Der Kreis
schlie&szlig;t sich.<br>
1984 folgte ONCE UPON A TIME IN AMERICA
unter der Regie von Sergio Leone. Mit viel Liebe zum
Detail erz&auml;hlt Leone die Geschichte einer
Freundschaft, der Freundschaft zwischen Noodles
(Robert De Niro) und Max (James Woods). &Uuml;ber fast
ein halbes Jahrhundert hinweg verfolgt er den
Aufstieg der beiden Freunde vom kleinen
Stra&szlig;enganoven bis hin zum Politiker. Auch in
diesem Werk macht De Niro eine Metamorphose
durch, vom jungen Stra&szlig;enganoven bis hin zum
alten, gebrochenen Mann und er wirkt in jeder dieser
Rollen glaubw&uuml;rdig.<br>
Im Privatleben geh&ouml;rt Robert de Niro zu den
&ouml;ffentlichkeitsscheuen Stars, und so verlief auch
seine Hochzeit 1976 mit der schwarzen S&auml;ngerin und
Musikerin Diahnne Abbot fast unbemerkt von der
&Ouml;ffentlichkeit. Trotz vieler gegenteiliger Ger&uuml;chte
hielt die Ehe bis Anfang der 90er Jahre. Aus der Ehe
ging ein Sohn hervor.</p>


<p>New Hollywood 1967-1976, in dieser &Auml;ra
liegen auch die Anf&auml;nge Robert de Niros. An
der Seite so gro&szlig;er Regisseure wie Brian de
Palma, Martin Scorsese, Roger Corman,
Francis Ford Coppola wurde auch De Niro zum
Star. Was sowohl ihn als auch Schauspieler wie Al
Pacino und Harvey Keitel, und damit die ganze &Auml;ra
"New Hollywood" von den altbekannten Filmen bzw.
Rollen der Schauspieler der Major Studios absetzt ist,
dass sie in den meisten F&auml;llen eine eher
unscheinbare Story erz&auml;hlen, in denen es eigentlich
keine Helden gibt. Menschen wie Jon Rubin, Travis
Bickle,... sind keine Gewinner, sie sind sehr einsame
Menschen - Verlierer? Nur indem De Niro, Keitel,
Pacino nicht auf einen Rollentyp festgelegt sind,
k&ouml;nnen sie wirklich jede Rolle glaubw&uuml;rdig verk&ouml;rpern
- anders als viele ihrer Vorg&auml;nger wie z.B. John
Wayne als Westernheld. Zu dieser F&auml;higkeit geh&ouml;rt
wahrscheinlich auch eine gewisse Unscheinbarkeit
und Bescheidenheit. Nur indem De Niro, wenn er
spielt, seine eigene Pers&ouml;nlichkeit aufgeben kann,
wirkt er &uuml;berzeugend.<br>
"New Hollywood" bezieht die Glaubw&uuml;rdigkeit seiner
Filme nicht nur aus deren Story, die nichts besch&ouml;nigt
und wahrheitsgetreu die Fakten pr&auml;sentiert, sondern
auch durch die einzelnen Schauspieler.
Die Karriere De Niros ist nicht zuletzt
deshalb so faszinierend, weil sie durchweg von
Erfolgen gepr&auml;gt ist. Auch in den 80er und 90er
Jahren war und ist Robert de Niro ein gefragter
Mann: Filme wie DIE UNBESTECHLICHEN (1987),
MIDNIGHT RUN (1987), GOOD FELLAS (1990)
und ZEIT DES ERWACHENS (1990) zeugen von
der schauspielerischen Vielf&auml;ltigkeit De Niros.</p>

      
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Kleine Filmographie 1963 - 1984</h3>
<p>(Reihenfolge entspricht Zeitpunkt der Fertigstellung!)</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>

<a name="sec-nhscorsese"></a>
<h3>Martin Scorsese<br>
<small>Die Zeit bis Taxi Driver</small>
</h3>


<h4>New York - Little Italy</h4>


<p>Martin Scorsese wurde am 17.11.1942
als zweites Kind einer Arbeiterfamilie in
Flushing, Long Island, geboren. Seine
Gro&szlig;eltern waren 191O aus Sizilien nach
Amerika  eingewandert,  seine  Eltern
zogen 1950 in den New Yorker Stadtteil
"Little Italy", wo "Marty" die katholische
St. Patrick's School, und ab September
1956 das Cathedral College besuchte.
Bis heute ist dieses italo-amerikanische
Ghetto  eine jener  Sammelst&auml;tten,  in
denen  sich  die  aus  verschiedenen
Nationen  stammenden  Immigranten
einer  Eingliederung  entziehen.  Eine
Subkultur,  in  der  das  Festhalten  an
althergebrachten  Werten  die  einzige
M&ouml;glichkeit ist, das Versinken in der
Masse des Schmelztiegels zu verhindern.
Uber allem  Leben  in  diesen  Vierteln
h&auml;ngt eine Wolke der Tradition, die den
Menschen  ihre  Identit&auml;t  verleiht.
Eingeengt in ein von geographischen,
sozialen,  psychischen wie ethnischen
Grenzen  abgeschlossenes  Gebiet
werden aber auch individuelle Freiheiten
f&uuml;r gesellschaftliche  Erwartungen und
Ideale untergeordnet. Scorsese selbst
hat diese  Lebensverh&auml;ltnisse der via
veccia  einmal  als  "mittelalterliche
Festung" beschrieben.  Eine Festung, in
der Scorsese sein Zuhause hat, in der
seine Familie, Nachbarn und Freunde die
Spielregeln vorleben, in der sich ein Mann
auf der Stra&szlig;e beweisen muss, und in der
eine katholisch-pr&uuml;de und gewaltt&auml;tig-korrupte  
Moral die Existenzen pr&auml;gen. <i>"Little Italy ist seine Welt, da w&uuml;tet er
wie ein Metzger"</i> <small>(Michael Ballhaus, Kameramann, epd 4/94)</small>
</p>


<p>Da  Scorseses  Kindheit  von  vielen
Krankenbettaufenthalten, bedingt durch
ein Asthma-Leiden, begleitet wurde, fiel
ihm eine Eingliederung in Freundeskreise
schwer,  und er galt als Au&szlig;enseiter.
Daf&uuml;r wurde er von seinem Vater &ouml;fters
mit ins Kino genommen, welches ihn
nachhaltig besch&auml;ftigte.</p>


<p>Seine jugendliche Filmleidenschaft ging
&uuml;ber  die  Einpr&auml;gung  von  Kino-  und
Personendaten aller Arten so weit, dass
er began, Storyboards von gesehenen
Filmszenen zu zeichnen, die er, ganz in
Anlehnung an die konsumierte f&uuml;nfziger
Jahre  Kinokultur, "MarScorScope-Production" 
betitelte. Eine interessante
Parallele dazu bildet F. F. Coppolas Spiel
mit  dem  Puppentheater ,  das  sein
Interesse fand, als er f&uuml;r ein Jahr wegen
Kinderl&auml;hmung im Bett lag.<br>
Zun&auml;chst blieb der Film nur ein Ausblick,
der  Scorsese  besch&auml;ftigte  und
entf&uuml;hrte,  aber  sein  eigentlich
vorgesehenes Berufsziel, das
Priesteramt,  f&uuml;hrte  ihn  in  die
Aufnahmepr&uuml;fung  f&uuml;r  das  "Divinity
Programme" des jesuitischen Fordham
College,  die  er  (zum  Gl&uuml;ck)  nicht
bestehen sollte. Desshalb schrieb er sich
an  der New York University f&uuml;r das
Lehramt in Englisch ein, wechselte aber,
nachdem er die Filmabteilung entdeckt
hatte, prompt das Hauptfach.<br>
In  den  nun  folgenden  Lehrjahren
arbeitete er an mehreren
Studentenfilmen  mit und  sammelte
Erfahrungen  im  Kamera-,  Regie-,
Beleuchtungs-  und  besonders  im
Schnittbereich.<br>

<i>"Der Grund, warum seine  Filme vom
Schnitt her so bemerkenswert sind, ist,
dass er von Beginn an denkt wie ein
Cutter;  schon  wenn  er schreibt  und
wenn er dreht."</i> <small>(Thelma Schooonmaker, Cutterin, epd 7/94)</small>
</p>


<p>&Uuml;ber  die,  zweifellos  bedeutende,
Erlangung  von   grunds&auml;tzlichen
handwerklichen  Fertigkeiten  hinaus
machte  seine  Arbeit  als  Autor  die
Studienzeit besonders wichtig. W&auml;hrend
ihr  entstanden  unter  anderem  die
B&uuml;cher zu "Who's that knocking at my door?" und,
sieben Jahre vor dem Dreh,
von "Mean Streets". An dieser Stelle und
in diesem Heft sei auch seine T&auml;tigkeit
im studentischen Filmclub erw&auml;hnt. Die
F&auml;higkeit, seine pers&ouml;nlichen Erfahrungen
und Empfindungen in autobiographisch
gepr&auml;gten  Geschichten  und  Bildern
ausdr&uuml;cken zu k&ouml;nnen, ist es, die seine
Filme  erst  erm&ouml;glichten.  Den  daf&uuml;r
notwendigen Unterricht erteilte er sich
privat,  indem  er,  &auml;hnlich  anderen
Regiekollegen der <i>New Hollywood &Auml;ra</i>
(z.B. Bogdanovich) so viel Zeit im Kino
verbrachte als nur m&ouml;glich.<br>
Von seinen ersten beiden Arbeiten, zwei
Kurzfilmen, wurde der zweite, "It's not just
You Murray!" (1964), &uuml;berraschenderweise
von  Paramount,  ins,  heute  leider
wegrationalisierte,   Vorprogramm
aufgenommen. Ein anderer Kurzfilm, "The big
shave" (1967), l&auml;&szlig;t den Namen Scorsese
zum ersten Mal in Europa auftauchen,
nachdem er bei einigen Festivals, u.a. 1968
in Oberhausen, gezeigt wird. Zur gleichen
Zeit arbeitete Scorsese schon seit &uuml;ber zwei
Jahren an seinem ersten Spielfilm.</p>


<h4>Who's that knocking at my door?</h4>


<p>"Who s that knocking at my door" (1965-1968)
war eigentlich als zweiter Teil einer
"Trilogie about the Neighborhood" geplant.
Der erste Film, der unter dem Titel "Jerusalem,
Jerusalem" gedreht werden sollte, ist, da kein
Produzent f&uuml;r das Projekt zu gewinnen war,
nie realisiert worden. Er sollte, das Drehbuch
handelte  von  zwei  18j&auml;hrigen
Priesteramtsanw&auml;rtern, das Thema Religion
anschneiden, das durchgehend in Scorseses
Filmen ein Hauptmotiv ist.<br>
Die Dreharbeiten zu "Who's that knocking at
my door" gestalteten  sich  bez&uuml;glich
Finanzierung und Produktion als &auml;u&szlig;erst
schwierig, und dass der Film schlie&szlig;lich doch
in amerikanischen Kinos zu sehen war, ist nur
mit dem ungeheuren Willen und Engagement
zu erkl&auml;ren, mit dem Scorsese das Projekt
verfolgte.<br>
Der Film entstand in drei Abschnitten. Die
erste Fassung, schon mit Harvey Keitel in der
Hauptrolle, wurde 1965 unter dem Titel "Bring
on the dancing girls", in einer 58 Minuten
Version, bis auf eine Szene in 35mm gedreht,
beim Filmfestival der N.Y.U. gezeigt, fiel
jedoch bei Kritik und Publikum durch. Zwei
Jahre sp&auml;ter, als endlich Geld aufgetrieben
war, erschien "I call first", in 16mm, mit Zina
Bethune als neuer weiblicher
Hauptdarstellerin. Vom alten Material von
"Bring on the dancing girls" wurde nur wenig
&uuml;bernommen. Auf dem Chicago Filmfestival
erntete "I call first" einige begeisterte Kritiken.
Trotzdem fand sich zun&auml;chst kein Verleiher.
Erst durch das Zugest&auml;ndnis an den auf
Sexfilme spezialisierten Joseph Brenner, der
auf den Einbau einer Bettszene bestand, f&uuml;r
die der Hauptdarsteller Harvey Keitel
kurzerhand nach Amsterdam geschickt wurde,
konnte "Who's that knocking at my door?"
endlich 1968 uraufgef&uuml;hrt werden. Keitel: <i>"Wir
holten uns eine Handvoll Girls und drehten
diese verr&uuml;ckten Nacktszenen, das war
unglaublich. Wir hatten eine Menge Spa&szlig;
damit ..."</i>.</p>


<p>Scorsese selbst hatte sicher keinen Spa&szlig; an
der Einmischung in den Inhalt des Films, aber
da ansonsten keine Eingriffe von au&szlig;en
erfolgten, war ihm eine Entfaltungsfreiheit
m&ouml;glich, die einzig vom finanziellen Etat
beschr&auml;nkt wurde.<br>
Somit konnte der erste Akt zu Scorseses
gro&szlig;em Film, (<i>"Ein guter Regisseur macht nur
einen Film"</i>), zum wahrscheinlich
pers&ouml;nlichsten Dokument &uuml;ber die damalige
Welt seines Regisseurs werden. Die Cutterin
Thelma Schoonmaker, eine Studienfreundin
Scorseses, die seine Werke bis heute durch
den Schneideraum begleitet, hatte an der
Bildsprache, die oft mehr und st&auml;rker spricht
als der Dialog, auch einen wichtigen Anteil.<br>
Der Film erz&auml;hlt die Geschichte von J.R.
(Harvey Keitel), einem "Schwarzen Engel",
der, eingeengt im Kreis von alptraumhaften
&Auml;ngsten von Gewalt auf New Yorker
Stra&szlig;en und vor allem vom Drama von
Schuld und Erl&ouml;sung, dem zentralen Thema,
das sich durch alle Filme Scorseses zieht,
versucht, sein Leben zu bestreiten. Eines
Tages trifft er im F&auml;hrhafen ein M&auml;dchen (sie
bleibt im ganzen Film namenlos). &Uuml;ber seine
Leidenschaft f&uuml;r Kinofilme kommen sie ins
Gespr&auml;ch. Sp&auml;ter, nachdem Sie sich
gemeinsam "Rio Bravo" angesehen haben:
Sie: Was meinst Du mit Mieze(broad)?
Er: (...)Da sind eben M&auml;dchen und dann sind
da Miezen. - Eine Mieze ist das Gegenteil
von einer Jungfrau. Man macht mit ihr rum.
Miezen heiratet man nicht.</p>


<p>Als er sp&auml;ter mit ihr im Bett liegt, kann er nicht
mit ihr schlafen, da er sie sonst, nach seinen
Moralvorstellungen, von der Heiligen, als die
er sie liebt, zur Hure machen w&uuml;rde. Beim
Zuschauer werden die Eindr&uuml;cke durch
wechselnde Schnitte, die immer wieder eine
Madonnenfigur zeigen, verdichtet. Als das
M&auml;dchen ihm von einer Vergewaltigung durch
ihren fr&uuml;heren Freund erz&auml;hlt, ist J.R.s Bild
von ihr als Heiliger (Jungfrau) besch&auml;digt,
und  er  gibt  ihr  die  Schuld  an  der
Vergewaltigung.<br>
J.R. :<i>"Ich kann das nicht verstehen (...) wie
kann ich glauben da&szlig; das wahr ist?"</i>
</p>


<p>Nachdem Scorsese 1969 mit "Woodstock"
einen erfolgreichen, und f&uuml;r viele bis heute
den besten, Musikfilm geschnitten hatte,
was in diesem Fall wohl die gr&ouml;&szlig;ere Leistung
bedeutete,  als  die  Durchf&uuml;hrung  der
Dreharbeiten, f&uuml;r die er Michael Wadleigh
assistierte,  galt  es  doch  aus  einem
un&uuml;berschaubaren Haufen mit konfusestem
Material  (&uuml;ber  1OO  Stunden)  eine
harmonische Einheit zusammenzuf&uuml;gen,
bekam er 1911  von Roger Corman den
Auftrag, den Folgefilm zu "Bloody Mama" (1970)
zu drehen. Das Ergebnis, "Boxcar Bertha",
ist  ein  f&uuml;r  Scorsese  eher
unpers&ouml;nliches Werk (<i>"entworfen f&uuml;r die
Jungs der 42.Stra&szlig;e"</i>), das die Handschrift
des Regisseurs vermissend, gleichwohl aber
unter dem Blickwinkel betrachtet werden
sollte, dass mit dem gleichen Team und bei
dem gleichen Studio ein Jahr sp&auml;ter "Mean
Streets" entstehen sollte.</p>


<h4>Mean Streets</h4>


<p> Mit einigem Recht kann man "Mean Streets"
in gewisser Weise als Fortsetzung von
"Who's  that  knocking  at  my  door?"
bezeichnen. Denn wenngleich die &auml;u&szlig;eren
Umst&auml;nde der Handlung die Hauptperson
Charlie (wieder gespielt von Harvey Keitel)
in eine etwas andere Perspektive Little Italys
r&uuml;cken,  so werden doch  grunds&auml;tzlich
Probleme, ebenso wie deren L&ouml;sungen,
wieder von den gleichen Ursachen und
Wirkungen hervorgerufen, und von den
gleichen Hintergr&uuml;nden beeinflusst, wie schon
bei  J. R..  Little  Italy,  das  Abbild  der
sch&uuml;tzenden und fordernden Familie, der
anheimelnden Nachbarschaft ebenso wie
der Schauplatz von Stra&szlig;engewalt, Tempel
religi&ouml;ser Uberzeugung und erstickender
Moral.<br>
"Mean Streets" ist Martin Scorseses erster
Film mit Robert de Niro, der nach Harvey
Keitel der zweite Schauspieler werden sollte,
mit dem Scorsese seinen Figuren eine H&uuml;lle
verleihen konnte, die in der Lage war, jene
komplexen Figuren zu verk&ouml;rpern, die auf
der Suche nach Erl&ouml;sung viele S&uuml;hnen auf
sich  nehmend, Konfrontationen  und
Schmerzen suchen, weil ihnen die einfache
Vergebung der S&uuml;nden durch Kirche und
Gesellschaft  nicht  (mehr)  ausreicht.
Charaktere, die, konfrontiert mit Liebe, in ihre
Festen der Moral zur&uuml;ckfallen, und mit Gewalt
und Hilflosigkeit auf ihre eigenen Sehns&uuml;chte
reagieren. Diesmal spielt de Niro jedoch noch
Johnny  Boy,  den  Widerpart  Charlies
(Keitel). Einen verspielten, schie&szlig;w&uuml;tigen
Kindskopf,   der  sich  in  seiner
Anpassungsunf&auml;higkeit dauernd neue Feinde
macht und dadurch des Schutzes von
Charlie bedarf. Charlie nimmt mit dieser
Aufgabe die Bu&szlig;e auf sich, die er zu leisten er
als sein Schicksal ansieht.<br>
Die Kritiken feierten mit "Mean Streets" vor
allem eine neue Wahrhaftigkeit des Films (<i>"ein
wahres Werk unserer Zeit, ein Triumph des
individuellen Filmemachens"</i>). Scorsese
feierte mit ihm seinen ersten gro&szlig;en Erfolg,
denn, wenngleich der Verleiher Warner
Brothers angeblich kein Geld verdiente, war
Hollywood nun interessiert.<br>
"Mean Streets" wurde zum gr&ouml;&szlig;ten Teil in
den Studios von Roger Corman gedreht. Nur
f&uuml;r die relativ sp&auml;rlichen Au&szlig;enaufnahmen
flog man nach New York, um dort, bei
schlechtem Wetter, sparsame Stadtansichten
einzufangen, u.a. die Atmosph&auml;re des 
San-Gennaro-Festes, die die labyrinthartige Enge
der  R&auml;ume,  die den  Studioaufnahmen
anhaftet,  zu  dem  Eindruck  des
abgeschlossenen Ghettos erg&auml;nzen.</p>


<h4>Alice doesn't live here anymore</h4>


<p>F&uuml;r seinen ersten Major Studio Film, "Alice
doesn't live here anymore" (1974) bekam
Scorsese von Warner Brothers ein Budget
von 1,6  Mio Dollar,  was  zwar  nicht
umwerfend viel ist, aber doch das dreifache
des Etats von "Mean Streets" und eine
Unmenge Geld im Vergleich zu den 35000 $,
die f&uuml;r die Produktion von "Who's that..."
ausgegeben wurden.  Im Mittelpunkt steht,
ungew&ouml;hnlich f&uuml;r einen Studiofilm, eine Frau,
Alice (Ellen Burstyn), die versucht sich einen
Platz  im  Leben  einzurichten.  Das  ist
zwischen  Kind,  Arbeit und oft
selbstbezogenen M&auml;nnern eine Aufgabe
ohne wirklich m&ouml;gliche L&ouml;sung, also eigentlich
kein Thema f&uuml;r das alte Hollywood.
Nach  dieser Abgrenzung gegen  die
Festlegung auf ein festes Genre (nach "Mean
Streets" waren  Scorsese  massenweise
Drehb&uuml;cher f&uuml;r Gangsterfilme angeboten
worden) begannen 1975 die Vorbereitungen
f&uuml;r Taxi Driver.</p>


<h4>Taxi Driver</h4>


<p>In diesem Film, der in enger Zusammenarbeit
mit dem Autor Paul Schrader entstand, findet
sich Scorseses Typus des "Schwarzen Engels"
in der Gestalt von Travis Bickle (Robert de Niro) wieder.<br>

<i>"Eine  Figur,  die der unaufhebbaren
Gleichzeitigkeit von Gut und B&ouml;se entspricht,
(...) in der das Gute das B&ouml;se und das B&ouml;se
das Gute nicht ausschlie&szlig;t, in der es die
dialektische  Utopie, die Aufhebung der
Gegens&auml;tze in einem Dritten nicht gibt."</i>
<br>
Travis, der durch den Sumpf der New Yorker
Stra&szlig;en, bevorzugt bei Nacht, f&auml;hrt, der sich
selbst vor dem Spiegel beim Revolverziehen
im Selbstgespr&auml;ch (<i>"Sprichst Du mit mir?"</i>)
sucht, er ist eine m&ouml;gliche Entwicklungsstufe
von J.R. und Charlie. Eine Fortsetzung, in
der sich Travis selbst als Erl&ouml;ser einsetzt, in
der er die S&uuml;ndhaftigkeit der Welt nicht mehr
nur durch Schmerzen und Opfer abgilt,
sondern durch Gewalt vertreiben will.
Die Variationsbreite, in der "Taxi Driver"
interpretiert wurde, legt das Potential an
Mi&szlig;verst&auml;ndnissen  deutlich  mit der
Scorseses Filme oft angegangen wurden,
reichen doch beispielsweise die Deutungen
der exzessiven Gewaltszene zum Schluss
des  Films von  der  Rechtfertigung der
Selbstjustiz  bis  zur  Auslegung  als
faschistoide Verherrlichung einer Asthetik des
T&ouml;tens.<br>
Doch <i>"im Grunde geht es um Einsamkeit,
sexuelle Frustration und verdr&auml;ngte Gewalt."</i> <small>(Scorsese)</small>
</p>

<a name="sec-nhcassavetes"></a>
<h3>John Cassavetes</h3>


<p>John Cassavetes wurde 1929 in New
York City geboren. Er war der Sohn eines
griechischen Immigranten, der als 
wagemutiger  und  sehr  gesch&auml;ftst&uuml;chtiger
Kaufmann bekannt war.</p>


<p>Nach Abschluss der "public school" 
besuchte  er,  noch  unentschlossen,  
verschiedene Colleges, bevor er sich f&uuml;r 6
Jahre zur US Army verpflichtete.</p>


<p>Nach der Entlassung aus der Armee 
gelang es ihm, an der New York Academy of
Dramatic Arts angenommen zu werden,
von wo er 1952 graduierte.
Daran anschlie&szlig;end folgten verschiedene
Engagements an einem kleinen Rhode
Island Theater.</p>


<p>Mit dem Medium Film kam Cassavetes
zuerst durch eine Rolle in TAXI (Gregoy
Ratoff,  1953) intensiver in Ber&uuml;hrung.
Durch weitere Rollen in Filmen und 
Fernsehshows konnte er sich als Schauspieler
einen Namen machen.</p>


<p>Besonders die regelm&auml;&szlig;igen Auftritte in
der Fernsehreihe PASO DOUBLE (Budd Schuldberg) 
und den Filmen CITY (Martin Ritt,  1956)  sowie  
CRIME  IN  THE STREETS (Don Siegel, 1951), machten
ihn in den USA einem breiteren Publikum
bekannt.</p>


<p>Nebenher gr&uuml;ndete er mit arbeitslosen
Schauspielerkollegen und Freunden einen
Method-Workshop. Dort sollte jeder, der
wollte, die M&ouml;glichkeit haben, 
Schauspielunterricht zu nehmen und Kontakte zur
Filmindustrie zu kn&uuml;pfen. Unter der als
Method-Acting  bekannten  
Unterrichts-/Schauspielmethode verstanden sie das
damals im Film neue Konzept, die 
Charakterisierung und Ausarbeitung einer 
Figur dem jeweiligen Schauspieler 
m&ouml;glichst vollst&auml;ndig zu &uuml;berlassen. Die 
Kamera sollte nur aufzeichnen, ohne durch
Einstellung, Zoom o. &auml;. in die Handlung
und das Spiel einzugreifen. Die freie 
Improvisation  des  Schauspielers  verleiht
dem Film Glaubw&uuml;rdigkeit und Leben.</p>


<p>Als &Uuml;bungsbeispiel  f&uuml;r  eine  Gruppe
Schauspielsch&uuml;ler sollte  in  diesem
Workshop ein Film gedreht werden, 
anhand dessen die Sch&uuml;ler erlernen 
konnten, vor laufender Kamera zu 
improvisieren.  Zun&auml;chst  konnte  sich  aber  der
Workshop nicht leisten, einen Film zu drehen.</p>


<h4>SHADOWS</h4>


<p>Ins Rollen kam dieses Filmprojekt erst, als
Cassavetes die M&ouml;glichkeit hatte, in einer
Radiosendung von seinem Projeht zu berichten 
und dabei zu Spenden aufzurufen.
Mit den dadurch zur Verf&uuml;gung stehenden
ca. $20.OOO - $40.OO0 Dollar (nach dem Radioaufruf gingen wohl um die $20.000 ein, eine &auml;hnliche Summe konnte Cassavetes aus privaten Ressourcen aufbringen) konnten die
Dreharbeiten zu SHADOWS beginnen.</p>


<p>Ohne die geringste Ahnung von 
technischen  Details  des   Filmemachens
(Cassavetes lernte den v&ouml;llig unerfahrenen 
Al Ruben, der sp&auml;ter Kameramann
sein sollte, beim Baseballspielen kennen)
verliefen die Dreharbeiten entsprechend
chaotisch. Es wurde im Verlauf von ca. 2
Jahren eine Unmenge unsynchronisiertes
16mm Material produziert, das sp&auml;ter nur
unter Schwierigkeiten synchronisiert werden konnte. SHADOWS geriet zum Prototyp des 
improvisierten Filmes.</p>


<p>Es existierte au&szlig;er einer Idee kein Skript.
Der Plot entstand w&auml;hrend des Drehens.
Passend dazu wurde die Filmmusik von
Charlie Mingus und Shafi Hadi 
improvisiert, die wesentlich zum Flair des Films
beitr&auml;gt.</p>


<p>Die Geschichte um die Probleme dreier
Geschwister, die mit ihrer Herkunft aus
einer Mischlingsehe k&auml;mpfen, spielt nur
eine untergeordnete Rolle. Das Wesentliche 
sind die Gef&uuml;hle und Beziehungen
der Protagonisten, die durch das 
feinf&uuml;hlige Spiel der Schauspieler glaubhaft 
wiedergegeben und dargestellt werden.</p>


<p>Als SHADOWS nach vier Jahren auf die
Leinwand kam, l&ouml;ste er sofort heftige 
Reaktionen bei der Kritik aus, wurde aber in
seiner Bedeutung  noch  nicht wahrgenommen.</p>


<p>Gro&szlig;er Erfolg bei Publikum und Kritik
stellte sich zuerst in Europa ein, wo Parallelen 
(Kameraf&uuml;hrung, Regie) zum <i>Free Cinema</i>
 in England und der <i>Nouvelle Vague</i> in 
Frankreich gezogen werden konnten.  
Daran  anschlie&szlig;end  fanden  sich
dann auch in den USA Verleiher und ein
immer gr&ouml;&szlig;eres, begeistertes Publikum.
SHADOWS wurde zu einem der wichtigsten  
Filme,  die  das  amerikanische
"independent" Kino begr&uuml;ndeten.</p>


<p>Cassavetes erhielt als Regisseur &uuml;ber
Nacht eine Reputation und hatte Gelegenheit, 
Aufs&auml;tze in Filmzeitschriften zu
ver&ouml;ffentlichen. Unter anderem sprach er
sich in einem vielbeachteten Aufsatz von
1959 explizit gegen das bis dahin in den
USA vorherschende Studiosystem aus
und  betonte  die  Unvereinbarkeit  von
k&uuml;nstlerischem und kommerziellem 
Anspruch des Filmemachens.</p>


<h4>TOO LATE BLUES und A CHILD IS WAITING: Zwei kommerzielle Versuche</h4>


<p>Trotzdem geht er daraufhin auf ein 
Angebot der Paramount ein und dreht f&uuml;r diese
TOO LATE BLUES (1961), mit dem er
seine eigenen Thesen best&auml;tigt. TOO
LATE BLUES stellt genau den misslungenen 
Kompromiss von Kunst und Kommerz
dar, dessen Unm&ouml;glichkeit er kurz vorher
so vehement postulierte.</p>


<p>Nach dieser negativen Erfahrungen l&auml;sst
er sich anschlie&szlig;end gleich nochmal auf
ein kommerzielles Abenteuer ein und f&uuml;hrt
f&uuml;r den Produzenten Stanley Kramer (u. a. 
HIGH NOON) Regie in A CHILD IS WAITING 
(1962, mit Judy Garland und Burt Lancaster). 
Der durchaus sehenswerte Film &uuml;ber Probleme 
in der Kinderpsychiatrie ist deutlich besser gelungen als
TOO LATE BLUES. Wohl vor allem weil
Kramer ihm beim Schnitt nicht freie Hand
lie&szlig;, war Cassavetes &auml;usserst unzufrieden
mit dem  Ergebnis  und versuchte 
anschlie&szlig;end wieder unabh&auml;ngig zu produzieren.</p>


<p>Bezeichnend f&uuml;r diese beiden kommerziellen 
Filme ist auch, dass mit Cassavetes
befreundete  Schauspieler (Peter  Falk,
Ben Gauara, Gena Rowlands, die seit
1954 seine Frau ist) nur in Nebenrollen
vorkommen.  Die  Hauptdarsteller  sind
Hollywoodschauspieler, die sich pers&ouml;nlich nicht kennen.</p>


<p>Im  Zusammenhang  mit  Method-Acting
und der freien Improvisation w&auml;hrend des
Drehens ist es f&uuml;r Cassavetes absolut
notwendig, dass die Schauspieler, die sich
im Idealfall v&ouml;llig mit ihrer Rolle 
identifizieren, auch privat miteinander vertraut sind.
Nur so k&ouml;nnen sie aufeinander eingehen
und aufeinander reagieren. Somit ist auch
logisch, dass in allen "echten" Cassavetes
Filmen immer wieder dieselben Namen
auftauchen. Neben Peter Falk, Ben Gazzara, 
Gena Rowlands, Seymour Cassel
und Al Ruben &uuml;bemehmen h&auml;ufig auch
Familienmitglieder kleinere Rollen. Das
Set ger&auml;t regelm&auml;&szlig;ig zu einer Familienfeier. 
Erst durch diese enge Vertrautheit
wird die intime Atmosph&auml;re seiner Filme
glaubw&uuml;rdig.</p>


<h4>FACES: Zur&uuml;ck zu den Wurzeln</h4>


<p>Geheilt von den Studioerfahrungen 
beginnt Cassavetes 1965 mit den Dreharbeiten  
zu  FACES  (wieder  mit  Gena Rowlands, 
Seymour Cassel u. a., Al Ruban an der Kamera).</p>


<p>Zu diesem Film &uuml;ber das Ende einer 
bereits  seit  l&auml;ngerem  kaputten  Ehe
(Seitenspr&uuml;nge,  Beichten,...) existierte
anders als bei SHADOWS ein regelrechter 
Plot, da Cassavetes die Geschichte
urspr&uuml;nglich als B&uuml;hnest&uuml;ck geplant hatte.  
Dennoch  l&auml;sst er,  ungeachtet des
Skripts, die Schauspieler ihre Rollen und
streckenweise die Geschichte v&ouml;llig ab&auml;ndern.</p>


<p>Die Kamera (Al Ruban dreht &uuml;berwiegend
aus der Hand) tritt noch st&auml;rker in den
Hintergrund als bei SHADOWS. Rigoros
wird auf Kameraeinstellungen verzichtet.
Die Kamera soll einfach all das festhalten,
was irgendwo geschieht:</p>


<p>
<i>"Wir drehten  immer,  wenn  die Schauspieler 
dazu bereit waren. Wir waren ihre Sklaven. Alles 
wozu wir da waren, war aufzuzeichnen was geschah"</i> <small>(J. Cassavetes in: Joseph Gelmis: The Film Director as Superstarm Gordon City, N.Y. Doubleday, 1979, S. 83)</small>
</p>


<p>Dieser  dokumentarische  Charakter  ist
auch, was Cassavetes - unter Vorbehalten - 
mit dem <i>Direct Cinema</i>, mit J. L.
Godard, mit Andy Warhol und vor allem
mit Jaques Rivette (L'AMOUR FOU, 1968) verbindet.</p>


<p>Mit dieser Arbeit hatte Cassavetes wieder
zu seinem Stil gefunden. Seine Freunde
und seine Familie w&uuml;rden auch weiterhin
in seinen Filmen alle tragenden Rollen
spielen. Seine Regiearbeit hatte sich als
gut erwiesen und konnte nun noch 
ausgefeilt und perfektioniert werden.</p>


<h4>Die Zeit des "New Hollywood"</h4>


<p>Der erste Film der 7Oer;  HUSBANDS (1970) 
zeigt drei M&auml;nner inmitten ihrer
Midlife-Crisis, ausgel&ouml;st durch den Tod
eines gemeinsamen Freundes. Ben Gazzara, 
John Cassavetes und Peter Falk
spielen die Hauptrollen. HUSBANDS entstand 
im Auftrag der Columbia und wurde
dadurch wieder zu einem Spagat zwischen 
k&uuml;nstlerischer Absicht und den 
Interessen des Studios. Anderes als bei
seinen ersten Studio-Abenteuern  hatte
Cassavetes diesesmal jedoch schon 
gen&uuml;gend Renomee und Selbstvertrauen,
um sich die k&uuml;nstlerische Kontrolle zu 
sichern. Er konnte das Set und die Darsteller 
frei bestimmen. Allerdings hatte sich
Method-Acting mittlerweile etabliert, 
weswegen die Improvisationen der Darsteller
in HUSBANDS nicht mehr so &uuml;berzeugend 
wirken wie in SHADOWS oder FACES.</p>


<p>Die  Kritik reagierte teils mit 
uneingeschr&auml;nktem Lob, teils auch mit v&ouml;lliger
Ablehnung dieses ambivalenten Films.
Eine versteckte Weiterentwicklung (oder
auch Anpassung) findet von nun an in
den Titeln in Cassavetes Filmen statt.
W&auml;hlte er in "seinen" Filmen bisher sehr
verallgemeinernde  Titel  (SHADOWS,
FACES, HUSBANDS), so konzentriert er
zumindest die Titel seiner nachfolgenden
Filme wesentlich mehr auf einzelne Personen  
(bis  hin  zu  dem  Eigennamen GLORIA).</p>


<p>Sicherlich ist diese Entwicklung insofern
ein Zugest&auml;ndnis an die Geldgeber, als
die  Entwicklung  allgemeiner Aussagen
aus Einzelschicksalen eher dem gewohnten 
Hollywood-Schema entspricht als die
Behandlung abstrakterer Schicksale 
sozialer Gruppen (wie z. B. gemischtrassige
Beziehungen in SHADOWS).  Fast alle
seine folgenden Filme werden - in sehr
begrenztem Ausma&szlig; - von gro&szlig;en Studios 
teilfinanziert.</p>


<p>Auch MINNIE AND MOSKOWITZ (1971)
wird finanziell von Universal unterst&uuml;tzt,
allerdings hat hier das Studio keinerlei
Einfluss mehr auf den Drehverlauf, sondern 
gibt allein eine Verleihgarantie f&uuml;r
das fertige Produkt.</p>


<p>In  dieser  Kom&ouml;die  wirken  auch  die
Schauspieler wieder glaubw&uuml;rdiger und
passender als in HUSBANDS. Aus den
Fehlern bei Husbands hatte Cassavetes
gelernt, so dass das sehr lockere Drehbuch 
wieder mehr durch die Schauspieler
ver&auml;ndert und interpretiert wird. Die 
einzelnen Filmcharaktere entsprechen stark
denen ihrer Darsteller.</p>


<h4>A WOMAN  UNDER THE  INFLUENCE (1974)</h4>


<p>stellt f&uuml;r Cassavetes ein gro&szlig;es 
finanzielles und filmisches Abenteuer dar.
Unter Einsatz privater Mittel verfilmt er die
Geschichte  einer  manisch-depressiven
Frau, die von ihrer Umwelt und von den
&Auml;rzten alleingelassen wird.</p>


<p>Die Kritik in den USA lehnt den Film 
zun&auml;chst v&ouml;llig ab, was sich erst &auml;ndert, als
A WOMAN UNDER THE lNFLUENCE in
Europa begeistert gefeiert wird. 
Gleichzeitig wird dieser Film zu Cassavetes 
bisher gr&ouml;&szlig;tem finanziellen Erfolg. Die 
Diskussion um alternative Therapien in den
psychiatrischen Kliniken der USA erh&auml;lt
durch den Erfolg des Filmes neue Impulse.</p>


<p>Mit den Gewinnen aus A WOMAN... ist
es  Cassavetes  m&ouml;glich, 1975  THE
KILLING OF A CHINESE BOOKIE (nach
einem Skript von Cassavetes und Scorsese) 
zu finanzieren.</p>


<p>Mit diesem <i>film noir</i> bedient sich 
Cassavetes nach MINNIE AND MOSKOWITZ zum
zweiten Mal eines Genres. Er greift auf
klassische Thriller-Themen (Mafia, Spielschulden...) 
zur&uuml;ck, um die Geschichte eines verzweifelten 
Nachtclubbesitzers zu erz&auml;hlen, der sich immer 
tiefer in zwielichtige Gesch&auml;fte verstrickt.</p>


<p>Im Unterschied zu seinen Zeitgenossen
wie Coppola, Scorsese und Altman, die
teilweise versuchen die Genres zu 
analysieren und zu parodieren, ist das Genre
selbst f&uuml;r Cassavetes jedoch v&ouml;llig 
unwichtig. Ihm dient das Genre einzig als
Mittel, um seinen Film zu transportieren.</p>


<p>Dies wird auch daran sichtbar, dass er mit
seinem n&auml;chsten Film OPENING NIGHT (1977) 
v&ouml;llig von allen Genreformen abweicht.</p>


<p>OPENING NIGHT ist Cassavetes private
Analyse des Theaters und der Regie, die
er unabh&auml;ngig produziert und umsetzt.
Durch die starke pers&ouml;nliche Pr&auml;gung des
Films bleibt er umst&auml;ndlich und wird zu
einem finanziellen Misserfolg.</p>


<p>Mit GLORIA (198O) kehrt Cassavetes
wieder  zum  Thriller  zur&uuml;ck.  Die  
Kameraarbeit von Fred Schuler stellt das
Maximum an dem dar, was Cassavetes
fordert. Sie wird zum rein beobachtenden
Auge, mit einer Perfektion, wie er es in
keinem fr&uuml;heren Film erreichen konnte.</p>


<h4>Sp&auml;tere Produktionen</h4>


<p>Der 1984 entstandene LOVE STREAMS
bildet den eigentlichen Abschluss seines
k&uuml;nstlerischen Schaffens. Noch einmal
erz&auml;hlt er anr&uuml;hrend und glaubhaft eine
Geschichte, diesmal &uuml;ber zwei Geschwister 
in einer Identit&auml;tskrise.</p>


<p>Sowohl von Seiten des Publikums als
auch von der Kritik wird LOVE STREAMS
oft als letzter Film John Cassavetes bezeichnet, 
da der danach noch folgende BIG TROUBLE (1985) 
kaum noch die Z&uuml;ge eines 
Cassavetes-Films tr&auml;gt. Cassavetes war mit 
dem Ergebnis von "Big Trouble" &auml;u&szlig;erst 
unzufrieden und hat daran anschlie&szlig;end 
keine weiteren Filme mehr gedreht.</p>


<p>John Cassavetes verstarb am 3. Februar
1989 in Los Angeles.</p>


<h4>Schlussbemerkung</h4>


<p>John Cassavetes war nie wirklich ein 
typischer Vertreter des <i>New Hollywood</i>.</p>


<p>Themenwahl, Erz&auml;hlweise und Regiestil
erinnern stark an zeitgleiche Bewegungen
in Europa (Nouvelle Vague, Cinema Direct, 
New Cinema). Ebenso verbindet ihn
mehr mit den europ&auml;ischen Autoren 
Godard, Bresson und Bergmann als mit
Scorsese, Coppola und Bogdanovich.</p>


<p>Als <i>der</i> Independent-Filmemacher der
60er wuchs er in die Zeit des New Hollywood 
hinein, hat aber nie dessen Mittel
und Stile konsequent angewandt.</p>


<p>Vielmehr hat er versucht vor allem 
m&ouml;glichst unabh&auml;ngig zu bleiben, um seine
Art der Regief&uuml;hrung durchzusetzen.</p>


<p>Diese Unabh&auml;ngigkeit konnte er nur 
dadurch erreichen, dass er seine privaten
Eink&uuml;nfte als Schauspieler (THE DIRTY
DOZEN, ROSEMARlES BABY) verwandte, 
um Filme zu finanzieren. Trotzdem
konnte er oft nicht auf Unterst&uuml;tzung
durch die groDen Studios vezichten, was
f&uuml;r ihn erst dann unkritisch wurde, als er
sich  mit seinen  Ansichten  behaupten
konnte.</p>


<p>Dennoch verbindet ihn mit dem <i>New Hollywood</i>
die Auflehnung gegen das &uuml;bliche
Studiosystem und die Suche nach neuen
Themen.  Besonders  zum  Ausdruck
kommt die Verwandschaft zu anderen
Regisseuren der Zeit durch seine 
Bekanntschaft  und  Zusammenarbeit  mit
Scorsese, deren H&ouml;hepunkt wohl das
gemeinsam verfasste Drehbuch zu THE
KILLLNG OF A CHINESE BOOKIE war.</p>


<p>In jedem Fall hat er ein Werk hinterlassen,
das ganz unverwechselbar seine Handschrift 
tr&auml;gt und zum Intimsten und Interessantesten 
z&auml;hlt, was in den 60ern und 70ern produziert wurde.</p>


    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><a href="#sec-coppola">Coppola</a></small>
</li>
<li>
<small><a href="#sec-bbsfilms">Die BBS-Gruppe und ihre Filme</a></small>
</li>
<li>
<small><a href="#sec-nhbobby">Meister der Verwandlung: Robert de Niro</a></small>
</li>
<li>
<small><a href="#sec-nhscorsese">Martin Scorsese: Die Zeit bis Taxi Driver</a></small>
</li>
<li>
<small><a href="#sec-nhcassavetes">John Cassavetes</a></small>
</li>
</ul>