<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<img alt="New Hollywood" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/newhollywood/newholly.gif"><br>
<small><?php echo ContentHelper::createKapitelJumper('docs.newhollywood.nhscorsese')?>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-nhcassavetes"></a>
<h3>John Cassavetes</h3>


<p>John Cassavetes wurde 1929 in New
York City geboren. Er war der Sohn eines
griechischen Immigranten, der als 
wagemutiger  und  sehr  gesch&auml;ftst&uuml;chtiger
Kaufmann bekannt war.</p>


<p>Nach Abschluss der "public school" 
besuchte  er,  noch  unentschlossen,  
verschiedene Colleges, bevor er sich f&uuml;r 6
Jahre zur US Army verpflichtete.</p>


<p>Nach der Entlassung aus der Armee 
gelang es ihm, an der New York Academy of
Dramatic Arts angenommen zu werden,
von wo er 1952 graduierte.
Daran anschlie&szlig;end folgten verschiedene
Engagements an einem kleinen Rhode
Island Theater.</p>


<p>Mit dem Medium Film kam Cassavetes
zuerst durch eine Rolle in TAXI (Gregoy
Ratoff,  1953) intensiver in Ber&uuml;hrung.
Durch weitere Rollen in Filmen und 
Fernsehshows konnte er sich als Schauspieler
einen Namen machen.</p>


<p>Besonders die regelm&auml;&szlig;igen Auftritte in
der Fernsehreihe PASO DOUBLE (Budd Schuldberg) 
und den Filmen CITY (Martin Ritt,  1956)  sowie  
CRIME  IN  THE STREETS (Don Siegel, 1951), machten
ihn in den USA einem breiteren Publikum
bekannt.</p>


<p>Nebenher gr&uuml;ndete er mit arbeitslosen
Schauspielerkollegen und Freunden einen
Method-Workshop. Dort sollte jeder, der
wollte, die M&ouml;glichkeit haben, 
Schauspielunterricht zu nehmen und Kontakte zur
Filmindustrie zu kn&uuml;pfen. Unter der als
Method-Acting  bekannten  
Unterrichts-/Schauspielmethode verstanden sie das
damals im Film neue Konzept, die 
Charakterisierung und Ausarbeitung einer 
Figur dem jeweiligen Schauspieler 
m&ouml;glichst vollst&auml;ndig zu &uuml;berlassen. Die 
Kamera sollte nur aufzeichnen, ohne durch
Einstellung, Zoom o. &auml;. in die Handlung
und das Spiel einzugreifen. Die freie 
Improvisation  des  Schauspielers  verleiht
dem Film Glaubw&uuml;rdigkeit und Leben.</p>


<p>Als &Uuml;bungsbeispiel  f&uuml;r  eine  Gruppe
Schauspielsch&uuml;ler sollte  in  diesem
Workshop ein Film gedreht werden, 
anhand dessen die Sch&uuml;ler erlernen 
konnten, vor laufender Kamera zu 
improvisieren.  Zun&auml;chst  konnte  sich  aber  der
Workshop nicht leisten, einen Film zu drehen.</p>


<h4>SHADOWS</h4>


<p>Ins Rollen kam dieses Filmprojekt erst, als
Cassavetes die M&ouml;glichkeit hatte, in einer
Radiosendung von seinem Projeht zu berichten 
und dabei zu Spenden aufzurufen.
Mit den dadurch zur Verf&uuml;gung stehenden
ca. $20.OOO - $40.OO0 Dollar (nach dem Radioaufruf gingen wohl um die $20.000 ein, eine &auml;hnliche Summe konnte Cassavetes aus privaten Ressourcen aufbringen) konnten die
Dreharbeiten zu SHADOWS beginnen.</p>


<p>Ohne die geringste Ahnung von 
technischen  Details  des   Filmemachens
(Cassavetes lernte den v&ouml;llig unerfahrenen 
Al Ruben, der sp&auml;ter Kameramann
sein sollte, beim Baseballspielen kennen)
verliefen die Dreharbeiten entsprechend
chaotisch. Es wurde im Verlauf von ca. 2
Jahren eine Unmenge unsynchronisiertes
16mm Material produziert, das sp&auml;ter nur
unter Schwierigkeiten synchronisiert werden konnte. SHADOWS geriet zum Prototyp des 
improvisierten Filmes.</p>


<p>Es existierte au&szlig;er einer Idee kein Skript.
Der Plot entstand w&auml;hrend des Drehens.
Passend dazu wurde die Filmmusik von
Charlie Mingus und Shafi Hadi 
improvisiert, die wesentlich zum Flair des Films
beitr&auml;gt.</p>


<p>Die Geschichte um die Probleme dreier
Geschwister, die mit ihrer Herkunft aus
einer Mischlingsehe k&auml;mpfen, spielt nur
eine untergeordnete Rolle. Das Wesentliche 
sind die Gef&uuml;hle und Beziehungen
der Protagonisten, die durch das 
feinf&uuml;hlige Spiel der Schauspieler glaubhaft 
wiedergegeben und dargestellt werden.</p>


<p>Als SHADOWS nach vier Jahren auf die
Leinwand kam, l&ouml;ste er sofort heftige 
Reaktionen bei der Kritik aus, wurde aber in
seiner Bedeutung  noch  nicht wahrgenommen.</p>


<p>Gro&szlig;er Erfolg bei Publikum und Kritik
stellte sich zuerst in Europa ein, wo Parallelen 
(Kameraf&uuml;hrung, Regie) zum <i>Free Cinema</i>
 in England und der <i>Nouvelle Vague</i> in 
Frankreich gezogen werden konnten.  
Daran  anschlie&szlig;end  fanden  sich
dann auch in den USA Verleiher und ein
immer gr&ouml;&szlig;eres, begeistertes Publikum.
SHADOWS wurde zu einem der wichtigsten  
Filme,  die  das  amerikanische
"independent" Kino begr&uuml;ndeten.</p>


<p>Cassavetes erhielt als Regisseur &uuml;ber
Nacht eine Reputation und hatte Gelegenheit, 
Aufs&auml;tze in Filmzeitschriften zu
ver&ouml;ffentlichen. Unter anderem sprach er
sich in einem vielbeachteten Aufsatz von
1959 explizit gegen das bis dahin in den
USA vorherschende Studiosystem aus
und  betonte  die  Unvereinbarkeit  von
k&uuml;nstlerischem und kommerziellem 
Anspruch des Filmemachens.</p>


<h4>TOO LATE BLUES und A CHILD IS WAITING: Zwei kommerzielle Versuche</h4>


<p>Trotzdem geht er daraufhin auf ein 
Angebot der Paramount ein und dreht f&uuml;r diese
TOO LATE BLUES (1961), mit dem er
seine eigenen Thesen best&auml;tigt. TOO
LATE BLUES stellt genau den misslungenen 
Kompromiss von Kunst und Kommerz
dar, dessen Unm&ouml;glichkeit er kurz vorher
so vehement postulierte.</p>


<p>Nach dieser negativen Erfahrungen l&auml;sst
er sich anschlie&szlig;end gleich nochmal auf
ein kommerzielles Abenteuer ein und f&uuml;hrt
f&uuml;r den Produzenten Stanley Kramer (u. a. 
HIGH NOON) Regie in A CHILD IS WAITING 
(1962, mit Judy Garland und Burt Lancaster). 
Der durchaus sehenswerte Film &uuml;ber Probleme 
in der Kinderpsychiatrie ist deutlich besser gelungen als
TOO LATE BLUES. Wohl vor allem weil
Kramer ihm beim Schnitt nicht freie Hand
lie&szlig;, war Cassavetes &auml;usserst unzufrieden
mit dem  Ergebnis  und versuchte 
anschlie&szlig;end wieder unabh&auml;ngig zu produzieren.</p>


<p>Bezeichnend f&uuml;r diese beiden kommerziellen 
Filme ist auch, dass mit Cassavetes
befreundete  Schauspieler (Peter  Falk,
Ben Gauara, Gena Rowlands, die seit
1954 seine Frau ist) nur in Nebenrollen
vorkommen.  Die  Hauptdarsteller  sind
Hollywoodschauspieler, die sich pers&ouml;nlich nicht kennen.</p>


<p>Im  Zusammenhang  mit  Method-Acting
und der freien Improvisation w&auml;hrend des
Drehens ist es f&uuml;r Cassavetes absolut
notwendig, dass die Schauspieler, die sich
im Idealfall v&ouml;llig mit ihrer Rolle 
identifizieren, auch privat miteinander vertraut sind.
Nur so k&ouml;nnen sie aufeinander eingehen
und aufeinander reagieren. Somit ist auch
logisch, dass in allen "echten" Cassavetes
Filmen immer wieder dieselben Namen
auftauchen. Neben Peter Falk, Ben Gazzara, 
Gena Rowlands, Seymour Cassel
und Al Ruben &uuml;bemehmen h&auml;ufig auch
Familienmitglieder kleinere Rollen. Das
Set ger&auml;t regelm&auml;&szlig;ig zu einer Familienfeier. 
Erst durch diese enge Vertrautheit
wird die intime Atmosph&auml;re seiner Filme
glaubw&uuml;rdig.</p>


<h4>FACES: Zur&uuml;ck zu den Wurzeln</h4>


<p>Geheilt von den Studioerfahrungen 
beginnt Cassavetes 1965 mit den Dreharbeiten  
zu  FACES  (wieder  mit  Gena Rowlands, 
Seymour Cassel u. a., Al Ruban an der Kamera).</p>


<p>Zu diesem Film &uuml;ber das Ende einer 
bereits  seit  l&auml;ngerem  kaputten  Ehe
(Seitenspr&uuml;nge,  Beichten,...) existierte
anders als bei SHADOWS ein regelrechter 
Plot, da Cassavetes die Geschichte
urspr&uuml;nglich als B&uuml;hnest&uuml;ck geplant hatte.  
Dennoch  l&auml;sst er,  ungeachtet des
Skripts, die Schauspieler ihre Rollen und
streckenweise die Geschichte v&ouml;llig ab&auml;ndern.</p>


<p>Die Kamera (Al Ruban dreht &uuml;berwiegend
aus der Hand) tritt noch st&auml;rker in den
Hintergrund als bei SHADOWS. Rigoros
wird auf Kameraeinstellungen verzichtet.
Die Kamera soll einfach all das festhalten,
was irgendwo geschieht:</p>


<p>
<i>"Wir drehten  immer,  wenn  die Schauspieler 
dazu bereit waren. Wir waren ihre Sklaven. Alles 
wozu wir da waren, war aufzuzeichnen was geschah"</i> <small>(J. Cassavetes in: Joseph Gelmis: The Film Director as Superstarm Gordon City, N.Y. Doubleday, 1979, S. 83)</small>
</p>


<p>Dieser  dokumentarische  Charakter  ist
auch, was Cassavetes - unter Vorbehalten - 
mit dem <i>Direct Cinema</i>, mit J. L.
Godard, mit Andy Warhol und vor allem
mit Jaques Rivette (L'AMOUR FOU, 1968) verbindet.</p>


<p>Mit dieser Arbeit hatte Cassavetes wieder
zu seinem Stil gefunden. Seine Freunde
und seine Familie w&uuml;rden auch weiterhin
in seinen Filmen alle tragenden Rollen
spielen. Seine Regiearbeit hatte sich als
gut erwiesen und konnte nun noch 
ausgefeilt und perfektioniert werden.</p>


<h4>Die Zeit des "New Hollywood"</h4>


<p>Der erste Film der 7Oer;  HUSBANDS (1970) 
zeigt drei M&auml;nner inmitten ihrer
Midlife-Crisis, ausgel&ouml;st durch den Tod
eines gemeinsamen Freundes. Ben Gazzara, 
John Cassavetes und Peter Falk
spielen die Hauptrollen. HUSBANDS entstand 
im Auftrag der Columbia und wurde
dadurch wieder zu einem Spagat zwischen 
k&uuml;nstlerischer Absicht und den 
Interessen des Studios. Anderes als bei
seinen ersten Studio-Abenteuern  hatte
Cassavetes diesesmal jedoch schon 
gen&uuml;gend Renomee und Selbstvertrauen,
um sich die k&uuml;nstlerische Kontrolle zu 
sichern. Er konnte das Set und die Darsteller 
frei bestimmen. Allerdings hatte sich
Method-Acting mittlerweile etabliert, 
weswegen die Improvisationen der Darsteller
in HUSBANDS nicht mehr so &uuml;berzeugend 
wirken wie in SHADOWS oder FACES.</p>


<p>Die  Kritik reagierte teils mit 
uneingeschr&auml;nktem Lob, teils auch mit v&ouml;lliger
Ablehnung dieses ambivalenten Films.
Eine versteckte Weiterentwicklung (oder
auch Anpassung) findet von nun an in
den Titeln in Cassavetes Filmen statt.
W&auml;hlte er in "seinen" Filmen bisher sehr
verallgemeinernde  Titel  (SHADOWS,
FACES, HUSBANDS), so konzentriert er
zumindest die Titel seiner nachfolgenden
Filme wesentlich mehr auf einzelne Personen  
(bis  hin  zu  dem  Eigennamen GLORIA).</p>


<p>Sicherlich ist diese Entwicklung insofern
ein Zugest&auml;ndnis an die Geldgeber, als
die  Entwicklung  allgemeiner Aussagen
aus Einzelschicksalen eher dem gewohnten 
Hollywood-Schema entspricht als die
Behandlung abstrakterer Schicksale 
sozialer Gruppen (wie z. B. gemischtrassige
Beziehungen in SHADOWS).  Fast alle
seine folgenden Filme werden - in sehr
begrenztem Ausma&szlig; - von gro&szlig;en Studios 
teilfinanziert.</p>


<p>Auch MINNIE AND MOSKOWITZ (1971)
wird finanziell von Universal unterst&uuml;tzt,
allerdings hat hier das Studio keinerlei
Einfluss mehr auf den Drehverlauf, sondern 
gibt allein eine Verleihgarantie f&uuml;r
das fertige Produkt.</p>


<p>In  dieser  Kom&ouml;die  wirken  auch  die
Schauspieler wieder glaubw&uuml;rdiger und
passender als in HUSBANDS. Aus den
Fehlern bei Husbands hatte Cassavetes
gelernt, so dass das sehr lockere Drehbuch 
wieder mehr durch die Schauspieler
ver&auml;ndert und interpretiert wird. Die 
einzelnen Filmcharaktere entsprechen stark
denen ihrer Darsteller.</p>


<h4>A WOMAN  UNDER THE  INFLUENCE (1974)</h4>


<p>stellt f&uuml;r Cassavetes ein gro&szlig;es 
finanzielles und filmisches Abenteuer dar.
Unter Einsatz privater Mittel verfilmt er die
Geschichte  einer  manisch-depressiven
Frau, die von ihrer Umwelt und von den
&Auml;rzten alleingelassen wird.</p>


<p>Die Kritik in den USA lehnt den Film 
zun&auml;chst v&ouml;llig ab, was sich erst &auml;ndert, als
A WOMAN UNDER THE lNFLUENCE in
Europa begeistert gefeiert wird. 
Gleichzeitig wird dieser Film zu Cassavetes 
bisher gr&ouml;&szlig;tem finanziellen Erfolg. Die 
Diskussion um alternative Therapien in den
psychiatrischen Kliniken der USA erh&auml;lt
durch den Erfolg des Filmes neue Impulse.</p>


<p>Mit den Gewinnen aus A WOMAN... ist
es  Cassavetes  m&ouml;glich, 1975  THE
KILLING OF A CHINESE BOOKIE (nach
einem Skript von Cassavetes und Scorsese) 
zu finanzieren.</p>


<p>Mit diesem <i>film noir</i> bedient sich 
Cassavetes nach MINNIE AND MOSKOWITZ zum
zweiten Mal eines Genres. Er greift auf
klassische Thriller-Themen (Mafia, Spielschulden...) 
zur&uuml;ck, um die Geschichte eines verzweifelten 
Nachtclubbesitzers zu erz&auml;hlen, der sich immer 
tiefer in zwielichtige Gesch&auml;fte verstrickt.</p>


<p>Im Unterschied zu seinen Zeitgenossen
wie Coppola, Scorsese und Altman, die
teilweise versuchen die Genres zu 
analysieren und zu parodieren, ist das Genre
selbst f&uuml;r Cassavetes jedoch v&ouml;llig 
unwichtig. Ihm dient das Genre einzig als
Mittel, um seinen Film zu transportieren.</p>


<p>Dies wird auch daran sichtbar, dass er mit
seinem n&auml;chsten Film OPENING NIGHT (1977) 
v&ouml;llig von allen Genreformen abweicht.</p>


<p>OPENING NIGHT ist Cassavetes private
Analyse des Theaters und der Regie, die
er unabh&auml;ngig produziert und umsetzt.
Durch die starke pers&ouml;nliche Pr&auml;gung des
Films bleibt er umst&auml;ndlich und wird zu
einem finanziellen Misserfolg.</p>


<p>Mit GLORIA (198O) kehrt Cassavetes
wieder  zum  Thriller  zur&uuml;ck.  Die  
Kameraarbeit von Fred Schuler stellt das
Maximum an dem dar, was Cassavetes
fordert. Sie wird zum rein beobachtenden
Auge, mit einer Perfektion, wie er es in
keinem fr&uuml;heren Film erreichen konnte.</p>


<h4>Sp&auml;tere Produktionen</h4>


<p>Der 1984 entstandene LOVE STREAMS
bildet den eigentlichen Abschluss seines
k&uuml;nstlerischen Schaffens. Noch einmal
erz&auml;hlt er anr&uuml;hrend und glaubhaft eine
Geschichte, diesmal &uuml;ber zwei Geschwister 
in einer Identit&auml;tskrise.</p>


<p>Sowohl von Seiten des Publikums als
auch von der Kritik wird LOVE STREAMS
oft als letzter Film John Cassavetes bezeichnet, 
da der danach noch folgende BIG TROUBLE (1985) 
kaum noch die Z&uuml;ge eines 
Cassavetes-Films tr&auml;gt. Cassavetes war mit 
dem Ergebnis von "Big Trouble" &auml;u&szlig;erst 
unzufrieden und hat daran anschlie&szlig;end 
keine weiteren Filme mehr gedreht.</p>


<p>John Cassavetes verstarb am 3. Februar
1989 in Los Angeles.</p>


<h4>Schlussbemerkung</h4>


<p>John Cassavetes war nie wirklich ein 
typischer Vertreter des <i>New Hollywood</i>.</p>


<p>Themenwahl, Erz&auml;hlweise und Regiestil
erinnern stark an zeitgleiche Bewegungen
in Europa (Nouvelle Vague, Cinema Direct, 
New Cinema). Ebenso verbindet ihn
mehr mit den europ&auml;ischen Autoren 
Godard, Bresson und Bergmann als mit
Scorsese, Coppola und Bogdanovich.</p>


<p>Als <i>der</i> Independent-Filmemacher der
60er wuchs er in die Zeit des New Hollywood 
hinein, hat aber nie dessen Mittel
und Stile konsequent angewandt.</p>


<p>Vielmehr hat er versucht vor allem 
m&ouml;glichst unabh&auml;ngig zu bleiben, um seine
Art der Regief&uuml;hrung durchzusetzen.</p>


<p>Diese Unabh&auml;ngigkeit konnte er nur 
dadurch erreichen, dass er seine privaten
Eink&uuml;nfte als Schauspieler (THE DIRTY
DOZEN, ROSEMARlES BABY) verwandte, 
um Filme zu finanzieren. Trotzdem
konnte er oft nicht auf Unterst&uuml;tzung
durch die groDen Studios vezichten, was
f&uuml;r ihn erst dann unkritisch wurde, als er
sich  mit seinen  Ansichten  behaupten
konnte.</p>


<p>Dennoch verbindet ihn mit dem <i>New Hollywood</i>
die Auflehnung gegen das &uuml;bliche
Studiosystem und die Suche nach neuen
Themen.  Besonders  zum  Ausdruck
kommt die Verwandschaft zu anderen
Regisseuren der Zeit durch seine 
Bekanntschaft  und  Zusammenarbeit  mit
Scorsese, deren H&ouml;hepunkt wohl das
gemeinsam verfasste Drehbuch zu THE
KILLLNG OF A CHINESE BOOKIE war.</p>


<p>In jedem Fall hat er ein Werk hinterlassen,
das ganz unverwechselbar seine Handschrift 
tr&auml;gt und zum Intimsten und Interessantesten 
z&auml;hlt, was in den 60ern und 70ern produziert wurde.</p>


    
<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Coppola', array('site/page', 'view'=>'docs.newhollywood.coppola'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Die BBS-Gruppe und ihre Filme', array('site/page', 'view'=>'docs.newhollywood.bbsfilms'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Meister der Verwandlung: Robert de Niro', array('site/page', 'view'=>'docs.newhollywood.nhbobby'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Martin Scorsese: Die Zeit bis Taxi Driver', array('site/page', 'view'=>'docs.newhollywood.nhscorsese'));?></small>
</li>
<li>
<small><?php echo CHtml::link('John Cassavetes', array('site/page', 'view'=>'docs.newhollywood.nhcassavetes'));?></small>
</li>
</ul>
