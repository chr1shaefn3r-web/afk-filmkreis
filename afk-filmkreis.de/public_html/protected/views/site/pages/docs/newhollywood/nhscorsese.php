<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<img alt="New Hollywood" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/newhollywood/newholly.gif"><br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.newhollywood.nhbobby','docs.newhollywood.nhcassavetes')?>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-nhscorsese"></a>
<h3>Martin Scorsese<br>
<small>Die Zeit bis Taxi Driver</small>
</h3>


<h4>New York - Little Italy</h4>


<p>Martin Scorsese wurde am 17.11.1942
als zweites Kind einer Arbeiterfamilie in
Flushing, Long Island, geboren. Seine
Gro&szlig;eltern waren 191O aus Sizilien nach
Amerika  eingewandert,  seine  Eltern
zogen 1950 in den New Yorker Stadtteil
"Little Italy", wo "Marty" die katholische
St. Patrick's School, und ab September
1956 das Cathedral College besuchte.
Bis heute ist dieses italo-amerikanische
Ghetto  eine jener  Sammelst&auml;tten,  in
denen  sich  die  aus  verschiedenen
Nationen  stammenden  Immigranten
einer  Eingliederung  entziehen.  Eine
Subkultur,  in  der  das  Festhalten  an
althergebrachten  Werten  die  einzige
M&ouml;glichkeit ist, das Versinken in der
Masse des Schmelztiegels zu verhindern.
Uber allem  Leben  in  diesen  Vierteln
h&auml;ngt eine Wolke der Tradition, die den
Menschen  ihre  Identit&auml;t  verleiht.
Eingeengt in ein von geographischen,
sozialen,  psychischen wie ethnischen
Grenzen  abgeschlossenes  Gebiet
werden aber auch individuelle Freiheiten
f&uuml;r gesellschaftliche  Erwartungen und
Ideale untergeordnet. Scorsese selbst
hat diese  Lebensverh&auml;ltnisse der via
veccia  einmal  als  "mittelalterliche
Festung" beschrieben.  Eine Festung, in
der Scorsese sein Zuhause hat, in der
seine Familie, Nachbarn und Freunde die
Spielregeln vorleben, in der sich ein Mann
auf der Stra&szlig;e beweisen muss, und in der
eine katholisch-pr&uuml;de und gewaltt&auml;tig-korrupte  
Moral die Existenzen pr&auml;gen. <i>"Little Italy ist seine Welt, da w&uuml;tet er
wie ein Metzger"</i> <small>(Michael Ballhaus, Kameramann, epd 4/94)</small>
</p>


<p>Da  Scorseses  Kindheit  von  vielen
Krankenbettaufenthalten, bedingt durch
ein Asthma-Leiden, begleitet wurde, fiel
ihm eine Eingliederung in Freundeskreise
schwer,  und er galt als Au&szlig;enseiter.
Daf&uuml;r wurde er von seinem Vater &ouml;fters
mit ins Kino genommen, welches ihn
nachhaltig besch&auml;ftigte.</p>


<p>Seine jugendliche Filmleidenschaft ging
&uuml;ber  die  Einpr&auml;gung  von  Kino-  und
Personendaten aller Arten so weit, dass
er began, Storyboards von gesehenen
Filmszenen zu zeichnen, die er, ganz in
Anlehnung an die konsumierte f&uuml;nfziger
Jahre  Kinokultur, "MarScorScope-Production" 
betitelte. Eine interessante
Parallele dazu bildet F. F. Coppolas Spiel
mit  dem  Puppentheater ,  das  sein
Interesse fand, als er f&uuml;r ein Jahr wegen
Kinderl&auml;hmung im Bett lag.<br>
Zun&auml;chst blieb der Film nur ein Ausblick,
der  Scorsese  besch&auml;ftigte  und
entf&uuml;hrte,  aber  sein  eigentlich
vorgesehenes Berufsziel, das
Priesteramt,  f&uuml;hrte  ihn  in  die
Aufnahmepr&uuml;fung  f&uuml;r  das  "Divinity
Programme" des jesuitischen Fordham
College,  die  er  (zum  Gl&uuml;ck)  nicht
bestehen sollte. Desshalb schrieb er sich
an  der New York University f&uuml;r das
Lehramt in Englisch ein, wechselte aber,
nachdem er die Filmabteilung entdeckt
hatte, prompt das Hauptfach.<br>
In  den  nun  folgenden  Lehrjahren
arbeitete er an mehreren
Studentenfilmen  mit und  sammelte
Erfahrungen  im  Kamera-,  Regie-,
Beleuchtungs-  und  besonders  im
Schnittbereich.<br>

<i>"Der Grund, warum seine  Filme vom
Schnitt her so bemerkenswert sind, ist,
dass er von Beginn an denkt wie ein
Cutter;  schon  wenn  er schreibt  und
wenn er dreht."</i> <small>(Thelma Schooonmaker, Cutterin, epd 7/94)</small>
</p>


<p>&Uuml;ber  die,  zweifellos  bedeutende,
Erlangung  von   grunds&auml;tzlichen
handwerklichen  Fertigkeiten  hinaus
machte  seine  Arbeit  als  Autor  die
Studienzeit besonders wichtig. W&auml;hrend
ihr  entstanden  unter  anderem  die
B&uuml;cher zu "Who's that knocking at my door?" und,
sieben Jahre vor dem Dreh,
von "Mean Streets". An dieser Stelle und
in diesem Heft sei auch seine T&auml;tigkeit
im studentischen Filmclub erw&auml;hnt. Die
F&auml;higkeit, seine pers&ouml;nlichen Erfahrungen
und Empfindungen in autobiographisch
gepr&auml;gten  Geschichten  und  Bildern
ausdr&uuml;cken zu k&ouml;nnen, ist es, die seine
Filme  erst  erm&ouml;glichten.  Den  daf&uuml;r
notwendigen Unterricht erteilte er sich
privat,  indem  er,  &auml;hnlich  anderen
Regiekollegen der <i>New Hollywood &Auml;ra</i>
(z.B. Bogdanovich) so viel Zeit im Kino
verbrachte als nur m&ouml;glich.<br>
Von seinen ersten beiden Arbeiten, zwei
Kurzfilmen, wurde der zweite, "It's not just
You Murray!" (1964), &uuml;berraschenderweise
von  Paramount,  ins,  heute  leider
wegrationalisierte,   Vorprogramm
aufgenommen. Ein anderer Kurzfilm, "The big
shave" (1967), l&auml;&szlig;t den Namen Scorsese
zum ersten Mal in Europa auftauchen,
nachdem er bei einigen Festivals, u.a. 1968
in Oberhausen, gezeigt wird. Zur gleichen
Zeit arbeitete Scorsese schon seit &uuml;ber zwei
Jahren an seinem ersten Spielfilm.</p>


<h4>Who's that knocking at my door?</h4>


<p>"Who s that knocking at my door" (1965-1968)
war eigentlich als zweiter Teil einer
"Trilogie about the Neighborhood" geplant.
Der erste Film, der unter dem Titel "Jerusalem,
Jerusalem" gedreht werden sollte, ist, da kein
Produzent f&uuml;r das Projekt zu gewinnen war,
nie realisiert worden. Er sollte, das Drehbuch
handelte  von  zwei  18j&auml;hrigen
Priesteramtsanw&auml;rtern, das Thema Religion
anschneiden, das durchgehend in Scorseses
Filmen ein Hauptmotiv ist.<br>
Die Dreharbeiten zu "Who's that knocking at
my door" gestalteten  sich  bez&uuml;glich
Finanzierung und Produktion als &auml;u&szlig;erst
schwierig, und dass der Film schlie&szlig;lich doch
in amerikanischen Kinos zu sehen war, ist nur
mit dem ungeheuren Willen und Engagement
zu erkl&auml;ren, mit dem Scorsese das Projekt
verfolgte.<br>
Der Film entstand in drei Abschnitten. Die
erste Fassung, schon mit Harvey Keitel in der
Hauptrolle, wurde 1965 unter dem Titel "Bring
on the dancing girls", in einer 58 Minuten
Version, bis auf eine Szene in 35mm gedreht,
beim Filmfestival der N.Y.U. gezeigt, fiel
jedoch bei Kritik und Publikum durch. Zwei
Jahre sp&auml;ter, als endlich Geld aufgetrieben
war, erschien "I call first", in 16mm, mit Zina
Bethune als neuer weiblicher
Hauptdarstellerin. Vom alten Material von
"Bring on the dancing girls" wurde nur wenig
&uuml;bernommen. Auf dem Chicago Filmfestival
erntete "I call first" einige begeisterte Kritiken.
Trotzdem fand sich zun&auml;chst kein Verleiher.
Erst durch das Zugest&auml;ndnis an den auf
Sexfilme spezialisierten Joseph Brenner, der
auf den Einbau einer Bettszene bestand, f&uuml;r
die der Hauptdarsteller Harvey Keitel
kurzerhand nach Amsterdam geschickt wurde,
konnte "Who's that knocking at my door?"
endlich 1968 uraufgef&uuml;hrt werden. Keitel: <i>"Wir
holten uns eine Handvoll Girls und drehten
diese verr&uuml;ckten Nacktszenen, das war
unglaublich. Wir hatten eine Menge Spa&szlig;
damit ..."</i>.</p>


<p>Scorsese selbst hatte sicher keinen Spa&szlig; an
der Einmischung in den Inhalt des Films, aber
da ansonsten keine Eingriffe von au&szlig;en
erfolgten, war ihm eine Entfaltungsfreiheit
m&ouml;glich, die einzig vom finanziellen Etat
beschr&auml;nkt wurde.<br>
Somit konnte der erste Akt zu Scorseses
gro&szlig;em Film, (<i>"Ein guter Regisseur macht nur
einen Film"</i>), zum wahrscheinlich
pers&ouml;nlichsten Dokument &uuml;ber die damalige
Welt seines Regisseurs werden. Die Cutterin
Thelma Schoonmaker, eine Studienfreundin
Scorseses, die seine Werke bis heute durch
den Schneideraum begleitet, hatte an der
Bildsprache, die oft mehr und st&auml;rker spricht
als der Dialog, auch einen wichtigen Anteil.<br>
Der Film erz&auml;hlt die Geschichte von J.R.
(Harvey Keitel), einem "Schwarzen Engel",
der, eingeengt im Kreis von alptraumhaften
&Auml;ngsten von Gewalt auf New Yorker
Stra&szlig;en und vor allem vom Drama von
Schuld und Erl&ouml;sung, dem zentralen Thema,
das sich durch alle Filme Scorseses zieht,
versucht, sein Leben zu bestreiten. Eines
Tages trifft er im F&auml;hrhafen ein M&auml;dchen (sie
bleibt im ganzen Film namenlos). &Uuml;ber seine
Leidenschaft f&uuml;r Kinofilme kommen sie ins
Gespr&auml;ch. Sp&auml;ter, nachdem Sie sich
gemeinsam "Rio Bravo" angesehen haben:
Sie: Was meinst Du mit Mieze(broad)?
Er: (...)Da sind eben M&auml;dchen und dann sind
da Miezen. - Eine Mieze ist das Gegenteil
von einer Jungfrau. Man macht mit ihr rum.
Miezen heiratet man nicht.</p>


<p>Als er sp&auml;ter mit ihr im Bett liegt, kann er nicht
mit ihr schlafen, da er sie sonst, nach seinen
Moralvorstellungen, von der Heiligen, als die
er sie liebt, zur Hure machen w&uuml;rde. Beim
Zuschauer werden die Eindr&uuml;cke durch
wechselnde Schnitte, die immer wieder eine
Madonnenfigur zeigen, verdichtet. Als das
M&auml;dchen ihm von einer Vergewaltigung durch
ihren fr&uuml;heren Freund erz&auml;hlt, ist J.R.s Bild
von ihr als Heiliger (Jungfrau) besch&auml;digt,
und  er  gibt  ihr  die  Schuld  an  der
Vergewaltigung.<br>
J.R. :<i>"Ich kann das nicht verstehen (...) wie
kann ich glauben da&szlig; das wahr ist?"</i>
</p>


<p>Nachdem Scorsese 1969 mit "Woodstock"
einen erfolgreichen, und f&uuml;r viele bis heute
den besten, Musikfilm geschnitten hatte,
was in diesem Fall wohl die gr&ouml;&szlig;ere Leistung
bedeutete,  als  die  Durchf&uuml;hrung  der
Dreharbeiten, f&uuml;r die er Michael Wadleigh
assistierte,  galt  es  doch  aus  einem
un&uuml;berschaubaren Haufen mit konfusestem
Material  (&uuml;ber  1OO  Stunden)  eine
harmonische Einheit zusammenzuf&uuml;gen,
bekam er 1911  von Roger Corman den
Auftrag, den Folgefilm zu "Bloody Mama" (1970)
zu drehen. Das Ergebnis, "Boxcar Bertha",
ist  ein  f&uuml;r  Scorsese  eher
unpers&ouml;nliches Werk (<i>"entworfen f&uuml;r die
Jungs der 42.Stra&szlig;e"</i>), das die Handschrift
des Regisseurs vermissend, gleichwohl aber
unter dem Blickwinkel betrachtet werden
sollte, dass mit dem gleichen Team und bei
dem gleichen Studio ein Jahr sp&auml;ter "Mean
Streets" entstehen sollte.</p>


<h4>Mean Streets</h4>


<p> Mit einigem Recht kann man "Mean Streets"
in gewisser Weise als Fortsetzung von
"Who's  that  knocking  at  my  door?"
bezeichnen. Denn wenngleich die &auml;u&szlig;eren
Umst&auml;nde der Handlung die Hauptperson
Charlie (wieder gespielt von Harvey Keitel)
in eine etwas andere Perspektive Little Italys
r&uuml;cken,  so werden doch  grunds&auml;tzlich
Probleme, ebenso wie deren L&ouml;sungen,
wieder von den gleichen Ursachen und
Wirkungen hervorgerufen, und von den
gleichen Hintergr&uuml;nden beeinflusst, wie schon
bei  J. R..  Little  Italy,  das  Abbild  der
sch&uuml;tzenden und fordernden Familie, der
anheimelnden Nachbarschaft ebenso wie
der Schauplatz von Stra&szlig;engewalt, Tempel
religi&ouml;ser Uberzeugung und erstickender
Moral.<br>
"Mean Streets" ist Martin Scorseses erster
Film mit Robert de Niro, der nach Harvey
Keitel der zweite Schauspieler werden sollte,
mit dem Scorsese seinen Figuren eine H&uuml;lle
verleihen konnte, die in der Lage war, jene
komplexen Figuren zu verk&ouml;rpern, die auf
der Suche nach Erl&ouml;sung viele S&uuml;hnen auf
sich  nehmend, Konfrontationen  und
Schmerzen suchen, weil ihnen die einfache
Vergebung der S&uuml;nden durch Kirche und
Gesellschaft  nicht  (mehr)  ausreicht.
Charaktere, die, konfrontiert mit Liebe, in ihre
Festen der Moral zur&uuml;ckfallen, und mit Gewalt
und Hilflosigkeit auf ihre eigenen Sehns&uuml;chte
reagieren. Diesmal spielt de Niro jedoch noch
Johnny  Boy,  den  Widerpart  Charlies
(Keitel). Einen verspielten, schie&szlig;w&uuml;tigen
Kindskopf,   der  sich  in  seiner
Anpassungsunf&auml;higkeit dauernd neue Feinde
macht und dadurch des Schutzes von
Charlie bedarf. Charlie nimmt mit dieser
Aufgabe die Bu&szlig;e auf sich, die er zu leisten er
als sein Schicksal ansieht.<br>
Die Kritiken feierten mit "Mean Streets" vor
allem eine neue Wahrhaftigkeit des Films (<i>"ein
wahres Werk unserer Zeit, ein Triumph des
individuellen Filmemachens"</i>). Scorsese
feierte mit ihm seinen ersten gro&szlig;en Erfolg,
denn, wenngleich der Verleiher Warner
Brothers angeblich kein Geld verdiente, war
Hollywood nun interessiert.<br>
"Mean Streets" wurde zum gr&ouml;&szlig;ten Teil in
den Studios von Roger Corman gedreht. Nur
f&uuml;r die relativ sp&auml;rlichen Au&szlig;enaufnahmen
flog man nach New York, um dort, bei
schlechtem Wetter, sparsame Stadtansichten
einzufangen, u.a. die Atmosph&auml;re des 
San-Gennaro-Festes, die die labyrinthartige Enge
der  R&auml;ume,  die den  Studioaufnahmen
anhaftet,  zu  dem  Eindruck  des
abgeschlossenen Ghettos erg&auml;nzen.</p>


<h4>Alice doesn't live here anymore</h4>


<p>F&uuml;r seinen ersten Major Studio Film, "Alice
doesn't live here anymore" (1974) bekam
Scorsese von Warner Brothers ein Budget
von 1,6  Mio Dollar,  was  zwar  nicht
umwerfend viel ist, aber doch das dreifache
des Etats von "Mean Streets" und eine
Unmenge Geld im Vergleich zu den 35000 $,
die f&uuml;r die Produktion von "Who's that..."
ausgegeben wurden.  Im Mittelpunkt steht,
ungew&ouml;hnlich f&uuml;r einen Studiofilm, eine Frau,
Alice (Ellen Burstyn), die versucht sich einen
Platz  im  Leben  einzurichten.  Das  ist
zwischen  Kind,  Arbeit und oft
selbstbezogenen M&auml;nnern eine Aufgabe
ohne wirklich m&ouml;gliche L&ouml;sung, also eigentlich
kein Thema f&uuml;r das alte Hollywood.
Nach  dieser Abgrenzung gegen  die
Festlegung auf ein festes Genre (nach "Mean
Streets" waren  Scorsese  massenweise
Drehb&uuml;cher f&uuml;r Gangsterfilme angeboten
worden) begannen 1975 die Vorbereitungen
f&uuml;r Taxi Driver.</p>


<h4>Taxi Driver</h4>


<p>In diesem Film, der in enger Zusammenarbeit
mit dem Autor Paul Schrader entstand, findet
sich Scorseses Typus des "Schwarzen Engels"
in der Gestalt von Travis Bickle (Robert de Niro) wieder.<br>

<i>"Eine  Figur,  die der unaufhebbaren
Gleichzeitigkeit von Gut und B&ouml;se entspricht,
(...) in der das Gute das B&ouml;se und das B&ouml;se
das Gute nicht ausschlie&szlig;t, in der es die
dialektische  Utopie, die Aufhebung der
Gegens&auml;tze in einem Dritten nicht gibt."</i>
<br>
Travis, der durch den Sumpf der New Yorker
Stra&szlig;en, bevorzugt bei Nacht, f&auml;hrt, der sich
selbst vor dem Spiegel beim Revolverziehen
im Selbstgespr&auml;ch (<i>"Sprichst Du mit mir?"</i>)
sucht, er ist eine m&ouml;gliche Entwicklungsstufe
von J.R. und Charlie. Eine Fortsetzung, in
der sich Travis selbst als Erl&ouml;ser einsetzt, in
der er die S&uuml;ndhaftigkeit der Welt nicht mehr
nur durch Schmerzen und Opfer abgilt,
sondern durch Gewalt vertreiben will.
Die Variationsbreite, in der "Taxi Driver"
interpretiert wurde, legt das Potential an
Mi&szlig;verst&auml;ndnissen  deutlich  mit der
Scorseses Filme oft angegangen wurden,
reichen doch beispielsweise die Deutungen
der exzessiven Gewaltszene zum Schluss
des  Films von  der  Rechtfertigung der
Selbstjustiz  bis  zur  Auslegung  als
faschistoide Verherrlichung einer Asthetik des
T&ouml;tens.<br>
Doch <i>"im Grunde geht es um Einsamkeit,
sexuelle Frustration und verdr&auml;ngte Gewalt."</i> <small>(Scorsese)</small>
</p>

<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Coppola', array('site/page', 'view'=>'docs.newhollywood.coppola'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Die BBS-Gruppe und ihre Filme', array('site/page', 'view'=>'docs.newhollywood.bbsfilms'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Meister der Verwandlung: Robert de Niro', array('site/page', 'view'=>'docs.newhollywood.nhbobby'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Martin Scorsese: Die Zeit bis Taxi Driver', array('site/page', 'view'=>'docs.newhollywood.nhscorsese'));?></small>
</li>
<li>
<small><?php echo CHtml::link('John Cassavetes', array('site/page', 'view'=>'docs.newhollywood.nhcassavetes'));?></small>
</li>
</ul>