<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<img alt="New Hollywood" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/newhollywood/newholly.gif"><br>
<small><?php echo ContentHelper::createKapitelJumper('docs.newhollywood.bbsfilms','docs.newhollywood.bbsfilms')?>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-coppola"></a>
<h3>Coppola</h3>


<p>
YOU'RE A BIG BOY NOW erz&auml;hlt die am&uuml;sante Geschichte
von Bernard (Peter Kastner), einem Mutters&ouml;hnchen, das
auf Wunsch seines Vaters nach New York ziehen soll, um
sich dort <i>"die H&ouml;rner abzusto&szlig;en und ein Mann zu werden"</i>.
In New York wird Bernard durch ein &Uuml;berma&szlig; an (sexuellen)
M&ouml;glichkeiten angeregt. Er verschm&auml;ht die offensichtlichen
Angebote Amys (Karen Black) und verf&auml;llt der m&auml;nnerhassenden 
Schauspielerin Barbara Darling (Elizabeth
Hartman). Schon die erste Nacht endet im Totalfrust.
Als seine Mutter und Amy nach dem verlorenen Sohn
suchen, sieht er die drohende Gefahr der Bevormundung
und nimmt Rei&szlig;aus ...</p>


<p>
Nach der Premiere von BIG BOY sah man in
Hollywood ein neues Genie in der Nachfolge von
Orson Welles heraufziehen.
Coppolas  <i>"eindeutige Parteinahme f&uuml;r den
Emanzipationsdrang einer aufbrechenden jungen
Generation gegen die verkrusteten
Verhaltensma&szlig;regeln gutb&uuml;rgerlichen Benehmens
hatte zwar nicht die Radikalit&auml;t von Dennis
Hoppers EASY RIDER, der den Glauben an den
amerikanischen Traum gleich begrub, aber er
feierte Bernards Ausbruch aus den Klauen
elterlicher Bevormundung als Punktsieg des
"poppigen feelings" &uuml;ber neurotisches
Spie&szlig;b&uuml;rgertum."</i> <small>(Ulli Weis in "Das Neue Hollywood")</small>

</p>


<p>
BIG BOY wurde zum Ausl&ouml;ser einer Reihe von
Filmen, die sich mit sp&ouml;ttischem Anstand &uuml;ber
biederm&auml;nnische Augenwischerei hermachten; ein
Trend, von dem auch Mike Nichols' THE
GRADUATE mit Dustin Hoffman profitierte.
Wenn sich BIG BOY auch nicht explizit auf Filme
wie A HARD DAY'S NIGHT oder THE KNACK
(beide von R. Lester) st&uuml;tzt, so sind zumindest
gewisse <i>"Affinit&auml;ten zur Neuen Welle, zu Richard
Lester und John Cassavetes so offensichtlich,
dass zumindest von einem Gleichklang im
Zeitgef&uuml;hl und von einer gewissen Revolte gegen
die Standards einer l&auml;ngst industrialisierten
Filmsprache auszugehen ist. Es ist eine
Bewegung, an der Coppola teilhat."</i> <small>(Peter W. Jansen in "Francis Ford Coppola" -- Reihe Film 33, Carl Hanser Verlag)</small>

</p>


<h4>Die Ern&uuml;chterung - FINIAN'S RAINBOW</h4>


<p>
YOU'RE A BIG BOY NOW war erfolgreich
gelaufen und Coppola arbeitete gerade an einem
Drehbuch, aus dem sp&auml;ter THE
CONVERSATION werden sollte, als er von
Warner-Seven Arts das &uuml;berraschende Angebot
erhielt, das popul&auml;re Musical "Finian's Rainbow"
f&uuml;r die Leinwand zu adaptieren. Coppola willigte
ein, obwohl er sich vorgenommen hatte, nicht zum
Studioregisseur zu werden und in die F&auml;nge der
gro&szlig;en Produktionsgesellschaften zu geraten.
(Die Aussicht auf ein sicheres Studiogehalt nach
dem Reinfall an der B&ouml;rse mu&szlig; ausschlaggebend
gewesen sein; in Zukunft w&uuml;rde Coppola noch
&ouml;fter nach einem finanziellen Reinfall einen
Studiojob annehmen m&uuml;ssen.)
</p>


<p>
Zum Zeitpunkt der Adaption war das Musical
hinsichtlich seiner gesellschafts- und ideologiekritischen Aspekte schon recht veraltet (es hatte
seine Urauff&uuml;hrung bereits im Januar 1947 erlebt);
dennoch glaubte Coppola, die Oberfl&auml;chlichkeit
und Spannungslosigkeit des Skripts durch eigene
Arbeit wieder ausgleichen zu k&ouml;nnen - eine
Rechnung, die letztendlich nicht aufging. Die Dreharbeiten waren von Kompromissen und
Auseinandersetzungen gepr&auml;gt. So hatte
Coppola beispielsweise eigene 
Choreografievorstellunqen, die nicht mit denen von Fred
Astaire &uuml;bereinstimmten, worauf dessen
Choreograph die Produktion verlie&szlig;.<br>
Hinzu kamen finanzielle Probleme: Coppola wollte
den Film on location drehen, um der Geschichte
einen authentischeren Rahmen zu geben. Die
Studiobosse verlangten jedoch, dass aus
Kostengr&uuml;nden in den noch stehenden Kulissen
von CAMELOT gedreht wurde. F&uuml;r 
Au&szlig;enaufnahmen standen nur acht Tage zur Verf&uuml;gung.
Das gewonnene Material aus diesen acht Tagen
versuchte Coppola krampfhaft am Schneidetisch
in den Film zu integrieren, um ihm doch noch
etwas Realismus zu geben. Zahlreiche
Anschlussfehler waren das Ergebnis.<br>
Auch nach den Dreharbeiten waren die
Ent&auml;uschungen nicht vorbei: das Studio blies
den fertigen Film von 35 auf 70 mm auf, um ihn an
den Kinokassen besser verkaufen zu k&ouml;nnen. Bei
diesem Vorgang hatte wohl niemand an die 
unterschiedlichen Bildformate gedacht, so dass bei allen
Tanznummern die F&uuml;&szlig;e von Fred Astaire 
abgeschnitten waren - f&uuml;r ein Musical unverzeihlich.
</p>


<h4>Auf dem Weg In die Unabh&auml;ngigkeit: THE RAIN PEOPLE (1969)</h4>


<p>
Mit FINIAN'S RAINBOW hatte Coppola die
unangenehme Erfahrung der st&auml;ndigen
Bevormundung durch ein gro&szlig;es Studio gemacht.
Vom Aufbegehren gegen Bevormundung hatte
auch sein vorheriger Film BIG BOY gehandelt; f&uuml;r
die Zukunft nahm sich Coppola nun vor, sich nie
wieder von jemandem bestimmen zu lassen.
Seinen n&auml;chsten Film - THE RAIN PEOPLE -
wollte er nun endlich on location drehen,
ausserhalb der k&uuml;nstlerisch einengenden
Bedingungen der Studios.
</p>


<p>

<i>"Coppola hatte THE RAIN PEOPLE aus einem
Skript entwickelt, das er 1960 f&uuml;r eine "creative
writing class" am College unter dem Titel "Echoes"
begonnen hatte. Es war die Geschichte von drei
Frauen, die ihre M&auml;nner verlassen."</i> <small>(Peter W. Jansen in "Francis Ford Coppola", Reihe Film 33, Hanser Verlag)</small>
Ihren Ursprung hatte die Idee in einem Ereignis in
Coppolas Kindheit: seine Mutter hatte nach einem
heftigen Streit mit ihrem Mann einmal f&uuml;r zwei
Tage das Haus verlassen. <i>"Sie fuhr zum Haus
ihrer Schwester und blieb dort f&uuml;r zwei Tage,
ohne uns zu sagen, wo sie war. Sie sagte, sie
h&auml;tte in einem Hotel &uuml;bernachtet. Da hat
irgendetwas in mir reagiert, wissen Sie, die
Vorstellung, dass eine Frau fortgeht und in einem
Hotel &uuml;bernachtet. Ich stellte mir vor, wie sie in
diesem Hotel sa&szlig; und Angst hatte. Ich glaube,
dass die Ideen, die man hat und aus denen sp&auml;ter
etwas wird, so etwas &auml;hnliches sind wie zuf&auml;llige
Schnappsch&uuml;sse, wie eine zuf&auml;llige Stimmung."</i>

</p>


<p>
Mit etwas finanziellem R&uuml;ckhalt von Warner und
seinem Honorar von FINIAN'S RAINBOW stellte
er ein Team von rund zwanzig Mitarbeitern auf,
viele von ihnen seine Freunde, und kaufte einen
Kleinbus, in dem er ein bescheidenes Filmstudio
installierte. Dieses sog. Cinemobile sollte ihm
technische und r&auml;umliche Unabh&auml;ngigkeit von
Hollywood garantieren.
</p>


<p>
Natalie (Shirley Knight) ist eine Frau, in deren Leben alles
nach Plan verlaufen ist. Sie hat einen verst&auml;ndnisvollen
Mann, von dem sie ein Kind erwartet. Dennoch verl&auml;lsst sie
ihn eines Morgens. Einmal im Leben m&ouml;chte sie frei sein,
f&uuml;nf Minuten, eine halbe Stunde, einen halben Tag, ... Sie
will ihren Platz alleine finden. Ihre Schwangerschaft macht
alles noch komplizierter, f&uuml;rchtet sie sich doch vor der
gro&szlig;en Verantwortung. Mit ihrem Auto f&auml;hrt sie durch
Amerika, die diffusen Spuren von Freiheit und
Unabh&auml;ngigkeit verfolgend. Unterwegs nimmt sie einen
Anhalter mit: Jimmie Kilgannon (James Caan), den
ehemaligen College-Footballstar, genannt Killer. Killer ist in
Folge einer Sportverletzung geistig behindert und verh&auml;lt
sich wie ein zur&uuml;ckgebliebenes Kind. Zuerst m&ouml;chte Natalie
Killer wieder loswerden - zu sehr verlangt er ihr die
Verantwortung ab, vor der sie eigentlich hatte fliehen
wollen. Doch allm&auml;hlich entwickelt sich eine Beziehung, die
jedoch mit einer Katastrophe endet, als sich ein Fremder -
der Polizist Gordon (Robert Duvall) - zwischen die beiden
dr&auml;ngt.
</p>


<p>
Im Mittelpunkt des Films steht die von Killer
erz&auml;hlte Fabel von den Regenmenschen, <i>"who
are made of rain. When they cry, they disappear
altogether because they cry themselves away."</i>

</p>


<p>
In kaum einem anderen Film arbeitet Coppola so
deutlich seine pers&ouml;nlichen ikonografischen
Notate heraus: das Wasser (der Regen), die Zeit
und das Geld.<br>
Die Zeit spielt eine wichtige Rolle: f&uuml;r Killer gibt es
keine messbare Zeit mehr, nur noch die Zeit, die
ihm andere Menschen zuwenden. F&uuml;r Natalie
bedeutet die Zeit, die sie f&uuml;r sich selbst gewinnt,
so etwas wie Freiheit.<br>
Die Zeit bestimmt auch den Erz&auml;hlrhythmus des
Films: THE RAIN PEOPLE ist ein road movie mit
langen Einstellungen. Coppola nimmt sich viel
Zeit, erz&auml;hlt langsam. Am Anfang zeigt er den
Regen und f&uuml;hrt ihn als Handlungselement ein.
Der Regen wird Natalie auf ihrer Suche begleiten
und mit dem str&ouml;menden Regen verstr&ouml;mt auch
die Zeit; der Regen l&auml;sst ein Gef&uuml;hl f&uuml;r die Zeit
entstehen.
</p>


<p>
Die Dreharbeiten fanden - gem&auml;&szlig; der Vorlage
eines road-movies - fast ausschlie&szlig;lich <i>on the
road</i> statt. Zusammen mit seinem Team fuhr
Coppola vier Monate lang durch insgesamt
achtzehn Bundesstaaten, eine Reise, die &uuml;ber
weite Strecken der von Natalie und Killer
entspricht. Diese &Uuml;bereinstimmung war Coppola
wichtig: f&uuml;r Natalie ist der Ausbruch aus dem
Alltag eine Losl&ouml;sung, der Versuch einer 
Selbstfindung. F&uuml;r das Filmteam sollte es eine &auml;hnliche
Erfahrung werden; deshalb durfte auch niemand
seine Familie mitnehmen. (F&uuml;r sich selber machte
Coppola eine kleine Ausnahme...)<br>
Kameramann Bill Butler erinnert sich: <i>"Die Idee
war, dass wir in New York starteten und so weit
fuhren, wie wir wollten. Coppola hatte einen
gro&szlig;en Campingbus, in dem wir einen Schneidetisch eingerichtet hatten und au&szlig;erdem gab es
noch mehrere Wohnmobile."</i> Coppolas Frau
Eleanor fuhr zusammen mit den Kindern in einem
VW-Bus hinterher.
</p>


<p>
Im Team befand sich auch George Lucas, der
schon bei FINIAN'S RAINBOW Coppolas
Assistent gewesen war. Diesmal war Lucas
zweiter Kameramann, Art Director, 
Produktionsleiter und Tontechniker in einer Person. Au&szlig;erdem
entschloss sich Lucas, begleitend zu den
Dreharbeiten ein filmisches Tagebuch &agrave; la <i>cinema
verite</i> zu drehen. Das Ergebnis war FILMMAKER,
ein etwa halbst&uuml;ndiger Dokumentarfilm, ein
pers&ouml;nliches Statement &uuml;ber die t&auml;glichen
Spannungen und den Stress einer Filmproduktion,
die st&auml;ndig auf Achse ist.
</p>


<p>
Coppola war nat&uuml;rlich begeistert, zumal er
nicht nur gerne hinter der Kamera, sondern auch
gerne vor der Kamera steht. In FILMMAKER zeigt
er sich als der b&auml;rtige Prophet, der dem veralteten
Studiosystem den Untergang vorhersagt.<br>
In einem w&uuml;sten Telefongespr&auml;ch mit einem
Produktionsleiter von Warner Bros. tobt er: <i>"Das
System wird unter seinem eigenen Gewicht
zusammenbrechen. Es kann gar nicht anders."</i>
FILMMAKER ist eine der besten
Dokumentationen, die je &uuml;ber die Entstehung
eines Films gedreht wurden und wird noch heute
an den Filmklassen der UCLA als Beispiel f&uuml;r
einen hervorragenden Dokumentarfilm gezeigt.
</p>


<p>
Die letzte Szene zeigt alle Schauspieler und das
Kamerateam vor ihrer Karawane von
Ausstattungs- und Wohnmobilen. Das ganze
macht den Eindruck einer Theatergruppe aus dem
19. Jahrhundert, die von einer Tournee aus der
Provinz zur&uuml;ckkehrt.
</p>


<p>
Die Arbeit mit dem Unfertigen scheint einen
besonderen Reiz auf Coppola auszu&uuml;ben.
Improvisation war w&auml;hrend der Dreharbeiten ein
wichtiger Faktor; jedoch nicht hinsichtlich Dialogen
und Plotentwicklung (hier war das Buch bereits
pr&auml;zise ausgearbeitet; pr&auml;ziser zumindest als das
zu sp&auml;teren Filmen, wie z.B. zu THE
CONVERSATION). Wichtig war vielmehr, da&szlig;
die von Coppola gew&auml;hlte Drehform (<i>on the road</i>)
ihm viel Spielraum bei der Auswahl der Drehorte
lie&szlig;. Au&szlig;erdem hatte er die Freiheit, Ereignisse der
Reise spontan als Hintergrundelemente in seine
Geschichte einzubauen (z. B. die Parade, in die
sich Killer kurzfristig einreiht.)<br>
Was in seinen vorherigen Filmen FINIAN'S
RAINBOW und BIG BOY - wo sich Coppola
nicht gut genug vorbereitet hatte - noch ein Manko
gewesen war, erwies sich hier als Kunstgriff, um
der Geschichte mehr Wirklichkeitsn&auml;he zu geben.
</p>


<p>Abgesehen davon, dass Coppola beim
Filmemachen st&auml;ndig in Zeit- und Geldnot ist (in
seinen Filmen sind Zeit und Geld oft
gleichbedeutend), scheint er aus der Arbeit mit
dem Unfertigen seine Kreativit&auml;t zu beziehen.
(Die Schnittfassung von APOCALYPSE NOW,
die er 1979 in Cannes vorstellte, pr&auml;sentierte er
als <i>work in progress</i>.)<br>
Ein &auml;hnlicher Rhythmus pr&auml;gt auch seine rastlose
Lebensweise. Georges Lucas' Ehefrau Marcia
(Cutterin von TAXI DRIVER und ALICE
DOESN'T LIVE HERE ANYMORE) &uuml;ber
Coppola: <i>"Es ist nie langweilig mit ihm. Es sind
immer zehn oder zwanzig oder drei&szlig;ig Leute da,
irgendeiner setzt sich ans Klavier und klimpert, ein
paar Kinder tanzen herum und die Intellektuellen
sitzen da und sind in ein Gespr&auml;ch &uuml;ber Kunst
vertieft. Sein Leben befindet sich in einem
konstanten Zustand von Wandel."</i>
</p>


<h4>Der Traum von American Zoetrope</h4>


<p>

<i>"Das wesentliche Ziel der Gesellschaft ist es, sich
in den verschiedensten Bereichen des
Filmemachens zu engagieren und dabei die
begabtesten jungen Talente und die modernste
Technik und Ausr&uuml;stung einzusetzen, die auf dem
Markt zu haben sind."</i> <small>(aus einer Ank&uuml;ndigung zur Gr&uuml;ndung von American Zoetrope)</small>
</p>


<p>
W&auml;hrend der Dreharbeiten zu THE RAIN
PEOPLE hatten Francis Coppola und George
Lucas die Vision einer <i>"Filmkommune, deren
Mitglieder bis zu drei&szlig;ig Jahre alt waren, Ideen
und Ausr&uuml;stung in der gr&uuml;nen Einsamkeit von
Marin County (nahe San Francisco) miteinander
teilten und damit eine friedliche Alternative zu der
sinnlosen Verschwendung von Hollywood
demonstrierten. Das Ziel bestand darin, einen
Hafen zu schaffen, in dem Vertr&auml;ge unmoralisch
und Agenten nebens&auml;chlich waren, eine l&auml;ndliche
Basis statt einer sterilen Reflexion der Leere von
Los Angeles, eine Zuflucht, in der junge und
unerfahrene Filmemacher der Philosophie
Coppolas folgen konnten: "Film ist die endg&uuml;ltige
Form des Ausdrucks.""</i>
</p>


<p>
Nachdem sie THE RAIN PEOPLE bei Warner
abgeliefert hatten, reisten Coppola und sein
Produzent Ron Colby nach D&auml;nemark und
besuchten Lanterna Films, ein kleines Filmstudio,
das eine der vollst&auml;ndigsten Sammlungen von
Laternae Magicae und anderen fr&uuml;hen
Filmprojektoren beherbergte. Das Studio lag etwa
f&uuml;nfzig Meilen au&szlig;erhalb von Kopenhagen in
einem wundersch&ouml;nen Landhaus mitten im
Gr&uuml;nen. Die einzelnen Wohnr&auml;ume waren zu
Schneidekabinen umgebaut und mit der
modernsten Ausr&uuml;stung ausgestattet. In einer
Scheune war die Tonmischanlage untergebracht.
Coppola war begeistert; Lanterna Films war
genau das, was ihm vorschwebte. Er und Colby
beschlossen, dieses Environment nach Marin
County zu transferieren. <i>"Ich glaubte, dass die
neue Technologie genau die magische Zutat sein
w&uuml;rde, die uns den Erfolg garantieren w&uuml;rde. Wir
hatten die naive Vorstellung, dass es die
Ausr&uuml;stung war, die uns die Produktionsmittel
verschaffen w&uuml;rde. Nat&uuml;rlich lernten wir mit der
Zeit, dass es nicht die Ausr&uuml;stung, sondern das
Geld war."</i>
</p>


<p>
Auf der Heimreise machte Coppola 
Zwischenstation auf der Fotokina in K&ouml;ln, wo er aus einer
Laune heraus ein komplettes Tonmischstudio f&uuml;r
80.000 Dollar bestellte - ohne das Geld, einen
Platz f&uuml;r die Anlage oder Filme zu haben, die er
h&auml;tte nachvertonen k&ouml;nnen.<br>
Dieser sehr intuitive und spontane Umgang mit
Produktionsmitteln w&uuml;rde ihn bei zuk&uuml;nftigen
Projekten immer wieder in Schwierigkeiten bringen.
Eine treffende Umschreibung der Coppolaschen
Produktionspolitik lieferte sein alter Freund und
Kollege John Milius (Drehbuchautor von
APOCALYPSE NOW): <i>"Wie schon Talleyrand
&uuml;ber Napoleon sagte: er ist eben so gro&szlig;, wie
man ohne Tugend gro&szlig; sein kann."</i>
</p>


<p>
Im Juni 1969 begannen sich Coppola, Lucas und
Mona Skager (Produktionsassistentin bei RAIN
PEOPLE) im Raum San Francisco nach einem
gro&szlig;en viktorianischen Haus umzusehen, das als
Kopie des Lanterna-Geb&auml;udes in Betracht kam.
Als passenden Angebote ausblieben und die Zeit
immer mehr dr&auml;ngte (die teure Tonanlage w&uuml;rde
bald eintreffen), unterschrieb Coppola ohne
Z&ouml;gern einen langfristigen Mietvertrag f&uuml;r ein
freigewordenes Lagerhaus in der Innenstadt von
San Francisco. Um das n&ouml;tige Geld zusammenzukratzen, musste er sein eigenes Haus
verkaufen.<br>
American Zoetrope war geboren, aber Lanterna
Films war es nicht. (Einer Kopie der Lanterna-Studios kam etliche Jahre sp&auml;ter
George Lucas mit seiner in den Weinbergen von Lucas-Valley
gelegenen Skywalker-Ranch viel n&auml;her.... Lucas-Valley hie&szlig; &uuml;brigens
schon vor George Lucas so ...)<br>
Den Einfluss Lanternas reflektierte jedoch der
Name der neuen Company: ein Zoetrope ist ein
stroboskopischer Zylinder, der bewegte Bilder
entstehen l&auml;sst, w&auml;hrend er sich um seine eigene
Achse dreht. Der griechische Ursprung von
Zoetrope bedeutet au&szlig;erdem "lebendige
Bewegung", nach Coppolas Ansicht genau das
richtige Symbol f&uuml;r seine dynamische junge
Filmgesellschaft.</p>


<p>
Coppola war &uuml;berzeugt, dass Zoetrope die neue
Elite des Filmgesch&auml;fts werden w&uuml;rde. Lucas sah
darin eher eine Neugruppierung des Dreckigen
Dutzend von der Filmklasse der UCLA; er lud
John Milius, Matthew Robbins, Hal Barwood,
Willard Huyck, Gloria Katz und Walter Murch ein,
sich dem Abenteuer anzuschlie&szlig;en. Die Idee einer
verschworenen Gemeinde von Filmemachern, die
den moralischen Abgrund von Los Angeles hinter
sich lie&szlig;, faszinierte ihn.</p>


<p>
Lucas erinnert sich an die ersten chaotischen
Tage von Zoetrope: <i>"Es war, als wollte man den
Kindern zu Weihnachten ein Fahrrad zusammenbauen 
und w&uuml;sste, dass sie in einer Stunde
aufwachen. Man mu&szlig; sich unheimlich beeilen,
alles geht schief und die Gebrauchsanweisung ist
auch noch verschwunden."</i>
</p>


<p>
Doch Coppola hatte gro&szlig;e Pl&auml;ne f&uuml;r sein Studio.
Am 4. November 1969 wurde American Zoetrope
ins Handelsregister eingetragen. Der einzige
Aktion&auml;r der neuen Gesellschaft war Coppola
selber; gleichzeitig war er Pr&auml;sident. Lucas war
Vizepr&auml;sident und Mona Skager Schatzmeisterin.
Zusammen bildeten die drei ein ungew&ouml;hnliches
Direktorium: Coppola sch&auml;umte &uuml;ber vor
Begeisterung, Skager blieb cool und skeptisch
und Lucas war laut ihrer Einsch&auml;tzung ein
<i>"sch&uuml;chterner, ernsthafter Mensch, eher
unauff&auml;llig."</i>
</p>


<p>
&Uuml;ber Coppolas Firmenpolitik, junge Filmemacher -
h&auml;ufig Absolventen der Filmschule - unentgeltlich
f&uuml;r sich arbeiten zu lassen, kam es zur
Auseinandersetzung: nach Ansicht Coppolas
hatte Roger Corman einen Fehler gemacht, weil er
begabte junge Filmemacher nicht unter Vertrag
genommen und an sich gebunden hatte, so dass
viele gute Regisseure letztendlich umsonst f&uuml;r ihn
gearbeitet h&auml;tten. Diesen Fehler wollte er nicht
machen.<br>
Coppolas Wertesystem schien sich gewandelt
zu haben: war "Vertrag" noch vor einigen Jahren
ein schmutziges Wort f&uuml;r ihn gewesen, wollte er
nun <i>"eine Menge begabter junger Filmemacher
umsonst verpflichten, hoffen, dass einer ihrer Filme
ein Hit wird und das ganze Studio auf dieser
Basis aufbauen."</i> <small>(Lucas)</small>
</p>


<p>
Trotzdem wirkte Coppolas Enthusiasmus
ansteckend. Ger&uuml;chte an den Filmschulen der
UCS und der UCLA besagten, da&szlig; sich die
Zukunft der Filmindustrie bei Zoetrope abspiele.
Coppolas kleines Studio war ein Sammelbecken
neuer Ideen, mit denen er zu Warner Bros. ging
und sich um eine Finanzierung bem&uuml;hte.</p>


<p>
Eines der neuen Filmprojekte war THX 1138, die
Spielfilmfassung des Studentenfilms THX 1138:
4EB (ELECTRONIC LABYRINTH), den George
Lucas 1967 gedreht hatte und der den ersten
Preis beim Third Student Film Festival gewonnen
hatte.</p>


<p>THX ist ein in klinisches Wei&szlig; getauchter Science-Fiction 
Film, der in einem unterirdischen &Uuml;berwachungsstaat 
spielt, dessen Bewohner ihre Gef&uuml;hle durch die
Einnahme von Drogen regeln und keinen Sex mehr haben.
Als THX 1138 (Robert Duvall) und seine Frau LUH 3417
(Maggie McOmie) eines Tages ihre Drogen nicht mehr
nehmen, entdecken sie ihre Sexualit&auml;t. LUH wird schwanger
und, als dies herauskommt, get&ouml;tet. THX wird ins Gef&auml;ngnis
geworfen, den <i>white limbo</i>, einen unendlich weiten,
perspektivelosen wei&szlig;en Raum, dessen Bewohner wirr vor
sich hinreden. Als THX die Flucht gelingt, startet das
System eine gnadenlose Jagd auf ihn.</p>


<p>
Die Themen von Lucas' Deb&uuml;tfilm sind
altbekannt: das Verh&auml;ltnis zwischen Mensch und
Maschine, die Notwendigkeit, einer bedr&uuml;ckenden
Umgebung zu entfliehen und Verantwortung zu
&uuml;bernehmen, nicht nur in zwischenmenschlichen
Beziehungen, sondern auch f&uuml;r moralische
Konsequenzen. Lucas portr&auml;tiert eine repressive
Gesellschaft, die ihre B&uuml;rger einlullt, ihre
zuk&uuml;nftigen Generationen im Labor z&uuml;chtet und
Minderheiten auf den Stand von Untermenschen
reduziert. Hauptmotiv des Films ist der <i>white
limbo</i>, der keine anderen Schranken hat als die
&Auml;ngste seiner Insassen.</p>


<p>
THX 1138 war Coppolas Vorzeigeobjekt, sein
Beweis, dass das Studio funktionierte. Er trug den
Film zu Warner Bros. und f&uuml;hrte ihn dem
Verantwortlichen f&uuml;r Filmdeals - Ted Ashley - vor.
In seiner Tasche hatte er gleich noch sieben
weitere Projekte, f&uuml;r die er sich eine Finanzierung
erhoffte. Doch als Ashley den Film gesehen hatte,
wurde er so w&uuml;tend, dass er den ganzen
Zoetrope-Handel platzen lie&szlig; und sich weigerte,
auch nur einen Blick in die anderen Drehb&uuml;cher zu
werfen (Zwei von ihnen verfilmte Coppola sp&auml;ter als THE
CONVERSATION und APOCALYPSE NOW).<br>
Dieser Tag ging als Schwarzer Donnerstag in die
Geschichte von <i>American Zoetrope</i> ein. Es war
ein Schlag, von dem sich die kleine Gesellschaft
nie wieder ganz erholte. Lucas war verzweifelt:
man hatte ihm seinen Film weggenommen und ihn
verspottet (Warner nahm THX - nachdem man ihn
schon finanziert hatte - nat&uuml;rlich doch noch in den
Verleih, allerdings nicht, ohne vorher an dem Film
herumzuschneiden; dieser Schlag gegen seine
kreative Freiheit verletzte ihn mehr, als er zugeben
wollte). Au&szlig;erdem f&uuml;hlte er sich f&uuml;r das Schicksal
von Zoetrope mitverantwortlich.<br>
Coppola floh nach Europa, um seine Wunden zu
lecken.</p>


<p>
THX lief zun&auml;chst gar nicht so schlecht, reduzierte
sich dann aber auf den Status eines Kultfilms, der
h&ouml;chstens noch in den Mitternachtsvorstellungen
der Programmkinos gezeigt wurde. <i>American
Zoetrope</i> produzierte noch f&uuml;r eine kurze Zeit
Commercials, Pornos, Lehr- und Dokumentarfilme,
bevor das Studio 1970 geschlossen wurde.<br>
In dieser pers&ouml;nlichen und finanziellen Krise
erreichte Coppola das rettende Angebot: er sollte
im Auftrag von Paramount Mario Puzos Bestseller
THE GODFATHER verfilmen.</p>



<h4>Die Zerst&ouml;rung der Pers&ouml;nlichkeit:<br>
THE CONVERSATION (1973)</h4>


<p>
Nach dem gescheiterten Experiment mit American
Zoetrope und dem kassensprengenden Erfolg
des PATEN unternahm Francis Coppola einen
zweiten Versuch, sich (wenigstens teilweise) vom
etablierten Studiosystem zu l&ouml;sen. 1972 gr&uuml;ndete
er zusammen mit seinen Regiekollegen William
Friedkin und Peter Bogdanovich die <i>Director's
Company</i>. Paramount beteiligte sich mit 31,5
Millionen Dollar und sicherte sich damit die H&auml;lfte
der erwarteten Box-office Gewinne.<br>
Daf&uuml;r verpflichtete sich das damals 
hoffnungsvollste Triumvirat des Neuen Hollywood, in den
kommenden zw&ouml;lf jahren insgesamt zw&ouml;lf Filme
zu drehen. Die ersten Produkte der Company
waren PAPER MOON von Bogdanovich und
THE BUNKER HILL BOYS von Friedkin.
Coppolas Beitrag war THE CONVERSATION,
ein pers&ouml;nlicher Film, an dessen Drehbuch er
bereits seit 1966 gearbeitet hatte.</p>


<p>Mittagspause auf dem Union Square in San Francisco. Ein
Paar schlendert &uuml;ber den Platz, vertieft in ein Gespr&auml;ch.
Anonym und gesch&uuml;tzt inmitten der vielen Menschen
glauben sie sich an einem Ort der gr&ouml;&szlig;tm&ouml;glichen
&Ouml;ffentlichkeit und zugleich der gr&ouml;&szlig;tm&ouml;glichen Intimit&auml;t.
Doch gerade diese Intimit&auml;t wird systematisch zerst&ouml;rt: das
Gespr&auml;ch des Paares wird abgeh&ouml;rt, von allen Seiten sind
Mikrofone auf die beiden gerichtet. Leiter dieser Aktion ist
Harry Gaul (Gene Hackman), ein gefragter Abh&ouml;rspezialist
und Meister seines Fachs. In seinem Tonstudio f&auml;hrt Harry
mehrere Aufnahmen des Gespr&auml;chs zu einem Band
zusammen. Dieses bringt er seinem Auftraggeber. Als
dieser jedoch nicht da ist, nimmt er das Band wieder mit. In
seiner Werkstatt &uuml;berpr&uuml;ft Harry noch einmal die
Tonb&auml;nder. Aus dem bisher vom Stra&szlig;enl&auml;rm &uuml;bert&ouml;nten
Satz <i>"He'd kill us if he got the chance."</i> 
glaubt Harry die Anzeichen einer bevorstehenden 
Gewalttat herauszuh&ouml;ren.
In ihm w&auml;chst der Verdacht, durch seine Arbeit einem
Verbrechen Vorschub zu leisten. Allm&auml;hlich gibt er seine
passive Betrachterrolle auf und verstrickt sich immer mehr
in einem Klima aus Misstrauen und Bedrohung.</p>


<p>THE CONVERSATION ist ein Film &uuml;ber den
Verlust von Intimit&auml;t und die Zerst&ouml;rung der
Pers&ouml;nlichkeit, also ein Film &uuml;ber Gewalt.<br>
Die Gewalt ist zun&auml;chst nur ein Verdacht, eine
Bef&uuml;rchtung, die aus dem Satz <i>"He'd kill us if he
got the chance."</i> erw&auml;chst und den ganzen Film
&uuml;ber pr&auml;sent ist (durch das wiederholte Abh&ouml;ren
des Bandes).</p>


<p>Harrys Aufgabe ist es, das Leben anderer
Menschen offenzulegen. Dass er es dabei sogar
zerst&ouml;ren kann - bei einer fr&uuml;heren Abh&ouml;raktion
waren aufgrund seiner Arbeit drei Menschen
ermordet worden - verdr&auml;ngt er. Zweifel an seiner
T&auml;tigkeit darf er sich nicht leisten. Sein 
wirkungsvollstes Schutzschild ist die Anonymit&auml;t. Die
Menschen, die er im Auftrag anderer belauscht,
bleiben f&uuml;r ihn Objekte; er will und darf sie nicht
kennen.<br>
Die Angst vor Kontakten pr&auml;gt auch sein
Privatleben: niemand kennt seine Telefonnummer,
niemand hat einen Zweitschl&uuml;ssel zu seiner
Wohnung; als einmal beide Tabus verletzt
werden, reagiert er mit Panik.</p>


<p>Coppola hat THE CONVERSATION zwischen
den beiden PATEN Filmen gedreht. In allen dreien
arbeitet er mit seinen bevorzugten Ikonen.<br>
In THE CONVERSATION geht es um die
zerrissene Familie. Im ersten Teil des PATEN hatte
sich die Zerst&ouml;rung der Familie (von innen heraus)
bereits angedeutet: Michael Corleone hat im
Machtkampf um die Stellung der Corleones den
Rat seines Vaters, die Familie als h&ouml;chstes Gut zu
besch&uuml;tzen, missachtet und seinen Schwager
ermorden lassen (in DER PATE. TEIL II wird er
sogar seinen Bruder erschie&szlig;en lassen).<br>
Harry Caul geht sogar noch weiter: aus Angst vor
Verwundbarkeit hat er erst gar keine Familie
gegr&uuml;ndet. Er hat jegliche Privatsph&auml;re und
Intimit&auml;t aus seinem Leben verbannt. Als 
Abh&ouml;rspezialist wei&szlig; er, wie angreifbar und 
verwundbar sie den einzelnen machen kann.<br>
Wie Natalie in THE RAIN PEOPLE und Michael
Corleone ist auch Harry Caul eine isolierte Figur.</p>


<p>In THE CONVERSATION geht es au&szlig;erdem um
Zeit (die Zeit, die gegen Harry arbeitet) und um
Geld (das als Motiv f&uuml;r die Gewalttat angesehen
werden kann).</p>


<p>Auch in den PATEN Filmen geht es um Zeit (die
Sehnsucht nach einer vergangenen, besseren
Zeit) und um Geld (wobei Geld vom umfassenderen Begriff Macht abgel&ouml;st wird).<br>
Das Wasser spielt ebenfalls eine Rolle und wird,
wie in THE RAIN PEOPLE und THE CONVERSATION, 
mit Gewalt in Verbindung
gebracht: als Michael Corleone am Ende von THE
GODFATHER. PART I seinen Sohn taufen l&auml;&szlig;t,
und gleichzeitig die Feinde der Familie liquidiert
werden, wird die Verbindung von Wasser und
Blut zun&auml;chst mittels einer Parallelmontage
hergestellt.<br>
Als Harry Gaul in THE CONVERSATION in
einem Hotelzimmer die Wassersp&uuml;lung bet&auml;tigt,
quillt ihm Blut entgegen und &uuml;bersp&uuml;lt den Boden.
Hier sind Wasser und Blut eine direkte Verbindung
eingegangen.</p>


<p>Der Regen hat noch eine andere Bedeutung:
Er ist standig pr&auml;sent, auch wenn es eigentlich nie
regnet. Trotzdem tr&auml;gt Harry Caul fast den
ganzen Film &uuml;ber einen Regenmantel. Der Sinn
dieses Regenmantels erschlie&szlig;t sich am ehesten
&uuml;ber Harrys Nachnamen: "caul" ist das englische
Wort f&uuml;r "Gl&uuml;ckshaube". Eine Gl&uuml;ckshaube ist
<i>"die unverletzte Eihauth&uuml;lle, in der das Kind bei
ausgebliebenem Blasensprung geboren wird; die
Gl&uuml;ckshaube muss sofort zerrissen werden, um
ein Ersticken zu verhindern... der Volksglaube
spricht so geborene oder mit Resten der
Eihauth&uuml;lle behaftete Kinder als Gl&uuml;ckskinder an,
zumal wenn sie im Besitz der Gl&uuml;ckshaut bleiben
oder Teile in die Kleider vern&auml;ht bekommen."</i> <small>(aus: Brockhaus Enzklop&auml;die in zwanzig B&auml;nden. Band 7)</small>
</p>


<p>Ausgehend von diesem Gedanken ergibt sich
eine interessante M&ouml;glichkeit, den Charakter
Harry Caul zu verstehen: Harry ist das
Gl&uuml;ckskind, das seine Gl&uuml;ckshaube noch tr&auml;gt, in
Gestalt seines Regenmantels aus Plastik.<br>
Der amerikanische Filmkritiker James W. Palmer
baut auf dieser Verbindung des Namens "Caul"
mit dem Plastikmantel eine ganze Interpretation
von THE CONVERSATION auf: er sieht den Film
als <i>"die Biographie eines ungeborenen Mannes,
eine Studie &uuml;ber Harrys erfolglosen Kampf, sich
selbst als einen moralischen Menschen zu
geb&auml;ren."</i> <small>(James W. Palmer: THE CONVERSATION: Coppola's Biography of an Unborn Man. in: Film Heritage, Vol. 12, Nr.1)</small>
</p>


<p>
<i>"Dieser Abh&ouml;rspezialist Harry Gaul, der von
einem Agenten des Systems schliesslich zu
dessen Opfer wird, der die kompliziertesten
Apparate bedient und gleichwohl in einem Vakuum
lebt, gepeinigt von untergr&uuml;ndigen &Auml;ngsten, vom
Zweifel und vom Verdacht, geh&ouml;rt zu den
interessantesten Figuren des neuen
amerikanischen Kinos."</i> <small>(Ulrich Gregor in "Geschichte des Films ab 1960", Band 4)</small>
</p>


<h4>Der letzte Tycoon</h4>


<p>Eingerahmt wird THE CONVERSATION von
THE GODFATHER, PART I und PART II - ein
pers&ouml;nlicher Film zwischen zwei Auftragsfilmen,
mit denen er so etwas wie eine Symbiose
eingeht: nur durch die Arbeit f&uuml;r die gro&szlig;en
Studios kann sich Coppola immer wieder seine
pers&ouml;nlichen Projekte verwirklichen; gleichzeitig
entwickelt er auch in seinen Auftragsfilmen seine
pers&ouml;nlichen Themen weiter.<br>
Alle Figuren Coppolas haben ein besonderes
Verh&auml;ltnis zur Ikonografie seiner Filme; sie
definieren sich durch sie. So werden auch seine
Auftragsfilme beinahe zu pers&ouml;nlichen Filmen.</p>


<p>Coppola ist sich der M&ouml;glichkeiten und Gefahren
seiner Zugest&auml;ndnisse durchaus bewusst.
W&auml;hrend andere Filmemacher der selben &Auml;ra den
Konsens scheuten und zu Hollywood-Au&szlig;enseitern 
wurden (Monte Hellman, Sam Peckinpah), 
oder einfach von den Bedingungen
der Unterhaltungsindustrie aufgefressen wurden
(Matthew Robbins, John Korty) hat sich Coppola
als letzter Tycoon der Filmgeschichte etabliert.
Er hat es seiner Originalit&auml;t hinsichtlich seiner
Themen und Stoffe, aber auch seinem Kalk&uuml;l zu
verdanken, dass er nie zu einem reinen 
Auftragsregisseur, zu einer <i>hired gun</i> wurde.</p>


<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Coppola', array('site/page', 'view'=>'docs.newhollywood.coppola'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Die BBS-Gruppe und ihre Filme', array('site/page', 'view'=>'docs.newhollywood.bbsfilms'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Meister der Verwandlung: Robert de Niro', array('site/page', 'view'=>'docs.newhollywood.nhbobby'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Martin Scorsese: Die Zeit bis Taxi Driver', array('site/page', 'view'=>'docs.newhollywood.nhscorsese'));?></small>
</li>
<li>
<small><?php echo CHtml::link('John Cassavetes', array('site/page', 'view'=>'docs.newhollywood.nhcassavetes'));?></small>
</li>
</ul>