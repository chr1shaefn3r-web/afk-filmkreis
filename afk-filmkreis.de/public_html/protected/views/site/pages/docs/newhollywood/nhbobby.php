<p align="right">
<small></small>
</p>
<hr size="1" noshade="noshade">
<img alt="New Hollywood" src="<?php echo Yii::app()->params['baseurls']['img']?>veroeffentlichungen/newhollywood/newholly.gif"><br>
<small>
<?php echo ContentHelper::createKapitelJumper('docs.newhollywood.bbsfilms','docs.newhollywood.nhscorsese')?>
</small>
<hr size="1" noshade="noshade">
<p>&nbsp;</p>
<a name="sec-nhbobby"></a>
<h3>Meister der Verwandlung: Robert de Niro</h3>


<p>Der Mann ist ein Ph&auml;nomen. 
<i>"In Gegenwart
einer Jazzband verwandelt er sich in einen
schwarzen Jazz-Trompeter. Trifft er einen
Indianer, wird er zur Rothaut mit einer Feder
am Hinterkopf. Kommt er mit dicken Leuten in
Kontakt, steigert er prompt sein K&ouml;rpergewicht auf
250 Pfund. In Gesellschaft von Politikern sieht er aus
wie ein Politiker....."</i> <small>((Kath. Filmdienst))</small>
<br>
Der Schauspieler Robert de Niro macht im wahrsten
Sinne des Wortes Metamorphosen durch, um eine
Rolle zu verk&ouml;rpern. Dazu greift er auf eigene
Erfahrungen zur&uuml;ck bzw. verschafft sich diese, indem
er z.B. f&uuml;r seine Rolle in
DER PATE, TEIL II nach
Sizilien f&auml;hrt, um sich dort
den sizilianischen Dialekt
anzueignen. Diese Liebe
zum Detail, die De Niro
einbringt, verleihen den
von ihm verk&ouml;rperten
Charakteren Tiefe und
Glaubw&uuml;rdigkeit.</p>


<p>1943 in New York,
Little Italy, geboren, wuchs
De Niro in zwar
wirtschaftlich mageren,
kulturell jedoch um so
reicheren Verh&auml;ltnissen auf.
Seine Mutter, Herausgeberin einer
literarischen Zeitschrift und
Malerin, sein Vater,
ebenfalls Maler und
Bildhauer, trennten sich
jedoch schon bald nach der 
Geburt des Sohnes, und
"Bobby" wuchs allein bei
seiner Mutter in Little Italy
auf. Der etwas scheue
Junge verbrachte seine
Freizeit mit Freunden aus einer "Stra&szlig;enbande". Mit
ca. 10 Jahren entdeckte De Niro dann seine
Leidenschaft f&uuml;r die Schauspielerei, als er n&auml;mlich im
Schultheater in dem St&uuml;ck "Der Zauberer von Oz"
auftrat. Dadurch kam er auch in Kontakt zu Stella
Adler und Erwin Piscator die eine Gruppe namens
"The Dramatic Workshop" leiteten. Besonders Stella
Adler begleitete ihn auf seinem weiteren Weg und
f&ouml;rderte seine Karriere.<br>
Sechs Jahre sp&auml;ter, De Niro hatte mittlerweile etliche
Male die Schule gewechselt, um sich seine
<i>"Ausbildung so zusammenzusuchen, wie er sie zu
ben&ouml;tigten glaubte"</i>, verlie&szlig; er die Schule ganz, um
eine Karriere als Schauspieler zu beginnen. Mit
kleinen Engagements jenseits des Broadways an
noch kleineren Dinner-Theatern hielt er sich die
ersten Jahre &uuml;ber Wasser - h&auml;ufig nicht einmal als
Schauspieler, sondern nur als Handlanger, was laut
seiner eigenen Aussage eine wichtige Erfahrung war:
<i>"Eine Menge Darsteller fanden es unter ihrer W&uuml;rde.
Aber ich machte Erfahrungen, w&auml;hrend ich wartete
und nicht spielte. Ich lernte genau dabei viel &uuml;ber
den Zauber des Spielens und die Profanit&auml;t des
Nicht-Spielens. Ich begriff, was es hei&szlig;t, endlich mit
der Darstellung zu beginnen."</i>
<br>
Seit 1960 besuchte der leidenschaftliche
Amateur-Mime regelm&auml;&szlig;ig Schauspielkurse am
renommierten New Yorker "Actor's Studio" und an
Stella Adlers "Conservatory of Acting" wo er viel &uuml;ber
die Stanislawski-Methode lernte: Nach Stanislawski
muss der Schauspieler in die Gef&uuml;hle und Haltungen
seiner Charaktere eindringen und verwandte
Emotionen und Reaktionen aus seinem eigenen Leben
einbringen. De Niro ist daf&uuml;r ein Paradebeispiel.</p>


<p>Auch De Niro wurde sozusagen entdeckt,
n&auml;mlich von der altgedienten Hollywood-Schauspielerin Shelley
Winters, als er an einem der Off-Broadway Theater
arbeitete. 1963 kam er dann zum Film: Ebenfalls
ein Deb&uuml;tant in seinem Gebiet, engagierte der
junge Regisseur Brian de Palma De Niro f&uuml;r eine
Rolle in THE WEDDING PARTY, die ihm zwar nicht
zum gro&szlig;en Durchbruch verhalf, doch seine
Bekanntheit steigerte. THE WEDDING PARTY war
auch der Beginn einer langj&auml;hrigen Zusammenarbeit
zwischen De Palma und De Niro, denn schon 1966
folgte GREETINGS. Hier spielt De Niro den jungen
New Yorker Jon Rubin, der, wie seine Freunde, mit
allerlei Tricks versucht, dem Wehrdienst zu
entkommen. Bis es soweit ist, schl&auml;gt er die Zeit tot,
u.a. indem er Pornos dreht. Doch auch Jon wird
schlie&szlig;lich nach Vietnam eingezogen. GREETINGS
war so erfolgreich, - er wurde z.B. mit einem
"Silbernen B&auml;ren" ausgezeichnet - dass sich De
Palma zu einer Fortsetzung entschloss: In HI MOM!
ist Jon Rubin aus Vietnam zur&uuml;ckgekehrt und beginnt
seinen Nachkriegsalltag als Filmemacher; sein
berufliches als auch privates Scheitern veranlassen
ihn am Ende zum Amoklauf.<br>
Obwohl er auch am Off-Broadway von den Kritikern
einhellig gelobt wurde, zog es De Niro immer wieder
zum Film zur&uuml;ck und mit der Rolle des Jon Rubin
erhielt er seine erste Hauptrolle, die ihm gleichzeitig
auch zum Durchbruch verhalf. HI MOM! wirkte wie
ein zeitgen&ouml;ssischer Kommentar zu der
Desorientierung der Jugend in der
nordamerikanischen Gesellschaft w&auml;hrend des
Vietnamkrieges. Der Film stellte eine scharfe, mit
radikalen Ansichten vorgebrachte Kritik an der
geistigen und moralischen Verfassung dieser
Gesellschaft dar. (Vip-Cinema, R. De Niro).</p>


<p>Nach HI MOM! folgten BLOODY MAMA (1969) von
Roger Corman und f&uuml;nf weitere Filme, in denen De
Niro mit Brillianz Nebenrollen verk&ouml;rperte. 1973 dann
ein weiterer gro&szlig;er Erfolg: Unter der Regie von
Martin Scorsese spielte De Niro an der Seite von
Harvey Keitel den Kleingangster
Johnny Boy in HEXENKESSEL.
Der Film spielt im New Yorker
Stadtteil Little Italy
und ist somit ein sehr pers&ouml;nliches
Werk, sowohl f&uuml;r De Niro als auch
f&uuml;r Martin Scorsese, die
beide aus diesem Stadtteil stammen.
Beide hatten sich schon Ende der
f&uuml;nziger Jahre in NewYork
kennengelernt, doch erst jetzt
entwickelte sich neben der
beruflichen Beziehung eine tiefe Freundschaft.
Durch HEXENKESSEL war Francis Ford Coppola
auf De Niro aufmerksam geworden und bot ihm die
Rolle des jungen Don Vito Corleone in DER PATE,
TEIL II (1974) an, die ihm einen Oscar einbrachte.
Die Story des Filmes ist allgemeinhin bekannt: Vito
Corleone alias Robert de Niro kommt als armer
sizilianischer Einwanderer nach New York. Schon
gleich zu Beginn hebt er sich von den <i>"glamour&ouml;sen,
geheimnisvoll ausgeleuchteten Gestalten ab, von
denen der Film bis dahin erz&auml;hlt hat"</i>. Seine Gestik
verr&auml;t die Entwicklung vom proletarischen Underdog
zum neuen Paten: Die w&uuml;rdevolle Haltung, <i>"ein
Heben des Zeigefingers, eine Wegwerfbewegung mit
der ganzen Hand, ..., der Zeigefinger an die Schl&auml;fe
gelegt"</i>. Mit der kaltbl&uuml;tigen Ermordung des
erpresserischen Patrons des Viertels macht er einen
gro&szlig;en Schritt in seiner Entwicklung zum "Paten".
Wie schon eingangs erw&auml;hnt geh&ouml;rte zu De Niros
Vorbereitung auf die Rolle eine Reise nach Sizilien,
wo er sich sowohl mit Dialekt als auch mit der
landes&uuml;blichen Gestik vertraut machte. Das
Ergebnis ist verbl&uuml;ffend!</p>


<p>In den darauffolgenden zwei Jahren drehte De Niro
unter der Regie von Bernado Bertolucci "1900"
(1975/76) und zusammen mit Freund Scorcese TAXI
DRIVER (1976). Trotz mehrerer eintr&auml;glicherer
Angebote entschied sich De Niro f&uuml;r TAXI DRIVER,
zum einen um Freund Scorcese einen Gefallen zu
tun, jedoch haupts&auml;chlich, weil ihn die Story
faszinierte. Um sich mit der Rolle des Travis Bickle
vertraut zu machen, erwarb De Niro extra den
Taxischein und verbrachte zahllose N&auml;chte hinter
dem Steuer in den Stra&szlig;en New Yorks.<br>
Es folgten die Filme DER LETZTE TYCOON
(1976), NEW YORK, NEW YORK (1977) von M.
Scorcese, THE DEER HUNTER (1978) von Michael
Cimino, RAGING BULL (1980) ebenfalls von M.
Scorcese. In THE DEER HUNTER spielt De Niro
den Stahlkocher Michael Vronsky. Zusammen mit
einigen Freunden geht Michael zu Beginn des Filmes
auf Hirschjagd und der Zuschauer erkennt sofort den
geborenen J&auml;ger in ihm. Die Jagd ist eine
M&ouml;glichkeit seiner <i>"kleinen, mausgrauen,
sozialgebundenen Existenz zu entkommen"</i>.
Schon bald ist nicht mehr der Hirsch das
alleinige Opfer und Michael und seine Freunde
erfahren die H&ouml;lle auf Erden, den Vietnamkrieg
(dt. Titel: DIE DURCH DIE H&Ouml;LLE GEHEN).
Regisseur Michael Cimino
wurde h&auml;ufig wegen der Brutalit&auml;t und Gewalt des
Filmes kritisiert, doch Ziel Ciminos ist nicht deren
Verherrlichung. Beim Anblick des T&ouml;tens und
Mordens kommen beim Zuschauer keine
Siegesgef&uuml;hle auf, denn man erkennt, dass in diesem
Krieg eigentlich alle Verlierer sind. Aus Vietnam
zur&uuml;ckgekehrt wird Michael als Held empfangen.
Doch der Mensch Michael Vronsky hat sich ver&auml;ndert
und die schreckliche Erfahrung des Krieges noch
lange nicht &uuml;berwunden. Er geht zwar wieder auf
Hirschjagd, doch die Jagd ist f&uuml;r ihn schon seit
langem zu Ende. Er will nicht mehr t&ouml;ten: Der Kreis
schlie&szlig;t sich.<br>
1984 folgte ONCE UPON A TIME IN AMERICA
unter der Regie von Sergio Leone. Mit viel Liebe zum
Detail erz&auml;hlt Leone die Geschichte einer
Freundschaft, der Freundschaft zwischen Noodles
(Robert De Niro) und Max (James Woods). &Uuml;ber fast
ein halbes Jahrhundert hinweg verfolgt er den
Aufstieg der beiden Freunde vom kleinen
Stra&szlig;enganoven bis hin zum Politiker. Auch in
diesem Werk macht De Niro eine Metamorphose
durch, vom jungen Stra&szlig;enganoven bis hin zum
alten, gebrochenen Mann und er wirkt in jeder dieser
Rollen glaubw&uuml;rdig.<br>
Im Privatleben geh&ouml;rt Robert de Niro zu den
&ouml;ffentlichkeitsscheuen Stars, und so verlief auch
seine Hochzeit 1976 mit der schwarzen S&auml;ngerin und
Musikerin Diahnne Abbot fast unbemerkt von der
&Ouml;ffentlichkeit. Trotz vieler gegenteiliger Ger&uuml;chte
hielt die Ehe bis Anfang der 90er Jahre. Aus der Ehe
ging ein Sohn hervor.</p>


<p>New Hollywood 1967-1976, in dieser &Auml;ra
liegen auch die Anf&auml;nge Robert de Niros. An
der Seite so gro&szlig;er Regisseure wie Brian de
Palma, Martin Scorsese, Roger Corman,
Francis Ford Coppola wurde auch De Niro zum
Star. Was sowohl ihn als auch Schauspieler wie Al
Pacino und Harvey Keitel, und damit die ganze &Auml;ra
"New Hollywood" von den altbekannten Filmen bzw.
Rollen der Schauspieler der Major Studios absetzt ist,
dass sie in den meisten F&auml;llen eine eher
unscheinbare Story erz&auml;hlen, in denen es eigentlich
keine Helden gibt. Menschen wie Jon Rubin, Travis
Bickle,... sind keine Gewinner, sie sind sehr einsame
Menschen - Verlierer? Nur indem De Niro, Keitel,
Pacino nicht auf einen Rollentyp festgelegt sind,
k&ouml;nnen sie wirklich jede Rolle glaubw&uuml;rdig verk&ouml;rpern
- anders als viele ihrer Vorg&auml;nger wie z.B. John
Wayne als Westernheld. Zu dieser F&auml;higkeit geh&ouml;rt
wahrscheinlich auch eine gewisse Unscheinbarkeit
und Bescheidenheit. Nur indem De Niro, wenn er
spielt, seine eigene Pers&ouml;nlichkeit aufgeben kann,
wirkt er &uuml;berzeugend.<br>
"New Hollywood" bezieht die Glaubw&uuml;rdigkeit seiner
Filme nicht nur aus deren Story, die nichts besch&ouml;nigt
und wahrheitsgetreu die Fakten pr&auml;sentiert, sondern
auch durch die einzelnen Schauspieler.
Die Karriere De Niros ist nicht zuletzt
deshalb so faszinierend, weil sie durchweg von
Erfolgen gepr&auml;gt ist. Auch in den 80er und 90er
Jahren war und ist Robert de Niro ein gefragter
Mann: Filme wie DIE UNBESTECHLICHEN (1987),
MIDNIGHT RUN (1987), GOOD FELLAS (1990)
und ZEIT DES ERWACHENS (1990) zeugen von
der schauspielerischen Vielf&auml;ltigkeit De Niros.</p>

      
<center>
<br clear="all">
<table summary="" cellspacing="0" cellpadding="2" border="0">
<tr>
<td bgcolor="#dddddd">
<table summary="" cellspacing="0" cellpadding="24" border="0">
<tr>
<td bgcolor="#eeeeee">
<h3 style="margin-top: 0;" align="center">Kleine Filmographie 1963 - 1984</h3>
<p>(Reihenfolge entspricht Zeitpunkt der Fertigstellung!)</p>
</td>
</tr>
</table>
</td>
</tr>
</table>
</center>

<br>
<hr size="1" noshade="noshade">
<h3>Inhalt:</h3>
<ul>
<li>
<small><?php echo CHtml::link('Coppola', array('site/page', 'view'=>'docs.newhollywood.coppola'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Die BBS-Gruppe und ihre Filme', array('site/page', 'view'=>'docs.newhollywood.bbsfilms'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Meister der Verwandlung: Robert de Niro', array('site/page', 'view'=>'docs.newhollywood.nhbobby'));?></small>
</li>
<li>
<small><?php echo CHtml::link('Martin Scorsese: Die Zeit bis Taxi Driver', array('site/page', 'view'=>'docs.newhollywood.nhscorsese'));?></small>
</li>
<li>
<small><?php echo CHtml::link('John Cassavetes', array('site/page', 'view'=>'docs.newhollywood.nhcassavetes'));?></small>
</li>
</ul>
