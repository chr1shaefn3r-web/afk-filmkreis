<?php
$this->breadcrumbs=array(
	'Vorstellungens'=>array('index'),
	$model->id,
);

?>

<h1>View Vorstellungen #<?php echo $model->id; ?></h1>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'vorstellungen-form',
	'enableAjaxValidation'=>false,
)); ?>
	<div class="row">
		<?php echo $form->dropDownList($model,'reihen_id', FilmHelper::createSemesterDropDownList(),array(
			'ajax' => array(
			'type'=>'POST', //request type
			'url'=>CController::createUrl('Vorstellungen/ajax'), //url to call.
			'update'=>'#ajax', //selector to update
			//'data'=>'js:javascript statement' 
			//leave out the data key to pass all form values through
		))); ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->

<div id="ajax">
<?php echo $this->renderPartial('_grid', array('model'=>$model,'dataProvider' => $model->getSemester('w',1954))); ?>
</div>
