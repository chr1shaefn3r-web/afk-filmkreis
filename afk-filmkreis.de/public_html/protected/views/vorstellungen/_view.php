<div class="viewfilm">
	<div class="viewfilmreihe">
		<?php if(CHtml::encode($data->reihe->id) != 0){?>
		<a href="reihen/<?php echo CHtml::encode($data->reihe->id); ?>">
			<?php echo CHtml::encode($data->reihe->title); ?>
		</a>
		<?php }?>
	&nbsp;
	</div>
	<div class="viewfilmdate"><?php echo CHtml::encode($data->date); ?></div>
	<div class="viewfilminfo">
		<div class="viewfilmtitle">
			<a href="film/<?php echo CHtml::encode($data->film->id); ?>">
				<?php echo CHtml::encode($data->film->title); ?>
			</a>
		</div>
		<div class="viewfilmtext"><?php echo CHtml::encode($data->text); ?></div>
		<div>
			<?php echo CHtml::encode($data->film->country); ?>
			<?php echo CHtml::encode($data->film->year); ?>&nbsp;-&nbsp;
			<?php echo (($data->film->duration > 0)?(CHtml::encode($data->film->duration)."min&nbsp;-&nbsp;"):""); ?>
			<?php echo CHtml::encode($data->film->language); ?>
		</div>
	</div>
</div>