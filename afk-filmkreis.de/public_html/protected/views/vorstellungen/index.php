<?php
$this->breadcrumbs=array(
	'Vorstellungens'=>array('index')
);

?>

<h1>Vorstellungen im Unikino</h1>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'vorstellungen-form',
	'enableAjaxValidation'=>false,
)); ?>
	<div class="row">
		<?php echo $form->dropDownList($model,'reihen_id', FilmHelper::createSemesterDropDownList(),array(
			'ajax' => array(
			'type'=>'POST', //request type
			'url'=>CController::createUrl('Vorstellungen/ajax'), //url to call.
			'update'=>'#ajax', //selector to update
			//'data'=>'js:javascript statement' 
			//leave out the data key to pass all form values through
		))); ?>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->

<div id="ajax">
<?php echo $this->renderPartial('_grid', array('model'=>$model,'dataProvider' => $model->getActSemester(), 'msg' => 'Noch ist der Spielplan für dieses Semester nicht veröffentlicht. Es kann aber nicht mehr lange dauern.')); ?>
<p><h1>In diesem Semester bereits gezeigte Filme:</h1></p>
<?php echo $this->renderPartial('_grid', array('model'=>$model,'dataProvider' => $model->getActSemesterHistory())); ?>
</div>
