<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'guestbook-form',
	'enableAjaxValidation'=>false,
)); ?>


	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'titel'); ?>
		<?php echo $form->textField($model,'titel',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'titel'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'mail'); ?>
		<?php echo $form->textField($model,'mail',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'mail'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo $form->textArea($model,'text',array('rows'=>10, 'cols'=>50)); ?>
		<?php echo $form->error($model,'text'); ?>
	</div>
	<div class="bagrfihi">
		<?php echo $form->labelEx($model,'text'); ?>
		<?php echo CHtml::textField('mail') ?>
		<?php echo $form->error($model,'text'); ?>
	</div>
	<?php echo CHtml::activeLabel($model, 'validacion'); ?>
	<?php $this->widget('application.extensions.recaptcha.EReCaptcha', 
	   array('model'=>$model, 'attribute'=>'validacion',
	         'theme'=>'white', 'language'=>'de', 
	         'publicKey'=>Yii::app()->params['reCaptcha']['publicKey'] )) ?>
	<?php echo CHtml::error($model, 'validacion'); ?>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->