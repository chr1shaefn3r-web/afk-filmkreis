<div class="post">
	<div class="title">
		<?php echo CHtml::encode($data->titel); ?>
	</div>
	<div class="author">
		geschrieben von <?php echo CHtml::encode($data->name); ?> am <?php echo CHtml::encode($data->timestamp); ?>
	</div>
	<div class="content">
		<?php
			$this->beginWidget('CMarkdown', array('purifyOutput'=>true));
			echo CHtml::encode($data->text);
			$this->endWidget();
		?>
	</div>
	<div class="nav">
	<?php //echo ($data->update_time != 0) ?  ("Letztes Update am ". date('d.m.Y',$data->update_time)) : ""; ?>
		
	</div>
</div>


<?php 
	/*

<div class="gbcontainer">
	<div class="gbcontainerheader"><?php echo CHtml::encode($data->titel); ?></div>
	<div>
	dfgihdfspog hpsdf gudhfspguds gdsupgfdshgifu doifdu hfdus hg dush
	</div>

</div>

<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('text')); ?>:</b>
	<?php echo CHtml::encode($data->text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('timestamp')); ?>:</b>
	<?php echo CHtml::encode($data->timestamp); ?>
	<br />*/
?>