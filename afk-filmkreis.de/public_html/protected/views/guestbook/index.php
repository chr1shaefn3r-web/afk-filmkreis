<?php
$this->breadcrumbs=array(
	'Guestbooks',
);

$this->menu=array(
	array('label'=>'Create Guestbook', 'url'=>array('create')),
	array('label'=>'Manage Guestbook', 'url'=>array('admin')),
);
?>

<h1>Gästebuch</h1>

<?php echo CHtml::Link('Ins Gästebuch eintragen', array('guestbook/create')); ?>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
	'summaryText'=>'Ergebnisse {start} - {end} von insgesamt {count} Einträgen.',
)); ?>
