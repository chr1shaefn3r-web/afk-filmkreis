<?php
$this->breadcrumbs=array(
	'Guestbooks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Guestbook', 'url'=>array('index')),
	array('label'=>'Manage Guestbook', 'url'=>array('admin')),
);
?>

<h1>Ins Gästebuch eintragen</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>

<?php echo CHtml::Link('Zurück zum Gästebuch', array('guestbook/index')); ?>