<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="de">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<style type="text/css">
		<!--
		body{
			background-attachment: scroll;
			background-clip: border-box;
			background-color: transparent;
			background-image: none;
			background-origin: padding-box;
			color: #333;
			direction: ltr;
			display: block;
			font-family: 'lucida grande', tahoma, verdana, arial, sans-serif;
			font-size: 11px;
			line-height: 16px;
			margin-bottom: 5px;
			margin-left: 0px;
			margin-right: 0px;
			margin-top: 0px;
			min-height: 0px;
			padding-bottom: 0px;
			padding-left: 0px;
			padding-right: 0px;
			padding-top: 0px;
			text-align: left;
			unicode-bidi: normal;
			visibility: visible;
		}
		h1{
			background-clip: border-box;
			background-color: transparent;
			background-image: none;
			background-origin: padding-box;
			color: #1C2A47;
			direction: ltr;
			display: block;
			font-family: 'lucida grande', tahoma, verdana, arial, sans-serif;
			font-size: 16px;
			font-weight: bold;
			height: 20px;
			line-height: 20px;
			margin-bottom: 0px;
			margin-left: 0px;
			margin-right: 0px;
			margin-top: 0px;
			min-height: 20px;
			padding-bottom: 2px;
			padding-left: 0px;
			padding-right: 0px;
			padding-top: 0px;
			text-align: left;
			unicode-bidi: normal;
			vertical-align: bottom;
			visibility: visible;
		}
		
		.viewfilm {
	height: 90px;
	background: #D8DFEA;
	border-top-style: solid;
	border-top-width: thin;
	border-top-color: #AAA;
	color: #474747;
}

.viewfilm a{
		color: #474747;
	}

.viewfilminfo {
	background: #FFF;
	width: 320px;
	height: 70px;
	position: relative;
	left: 78px;
}

.viewfilmdate{
	float: left;
	position: relative;
	left: 5px;
}

.viewfilmtitle{
	position: relative;
	left: -55px;
	font-weight: bolder;
}

.viewfilmreihe{
	text-align: right;
	font-variant: small-caps;
}
	
.viewfilmtext{
	font-weight: bold;
	font-style: italic;
}
		-->
		</style>
	</head>
	<body>
	<?php echo $content;?>
	</body>
</html>