<!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		<meta name="description" content="Die Website des Vereins Akademischer Filmkreis Karlsruhe e.V. (AFK)"/>
		<title><?php echo CHtml::encode($this -> pageTitle); ?></title>

		<link rel="icon" sizes="16x16" type="image/x-icon" href="/images/favicon.ico">

		<link id="theme" rel="stylesheet" type="text/css" href="<?php echo Yii::app() -> request -> baseUrl; ?>/css/afk.min.css" />
<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
    _paq.push(["setDomains", ["*.test.afk-filmkreis.de"]]);
    _paq.push(['trackPageView']);
	  _paq.push(['enableLinkTracking']);
	  (function() {
		      var u="//christophhaefner.de/piwik/";
			      _paq.push(['setTrackerUrl', u+'piwik.php']);
			      _paq.push(['setSiteId', 10]);
				      var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
				      g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
					    })();
</script>
<noscript><p><img src="//christophhaefner.de/piwik/piwik.php?idsite=10" style="border:0;" alt="" /></p></noscript>
<!-- End Piwik Code -->

	</head>
	<body>
		<div>TESTVERSION</div>
		<div class="fixed bag">
			<header>
				<img src="images/banner.gif" class="img-responsive center-block" />
			</header>
			<main>
				<div class="row">
			<nav>
				<input type="checkbox" id="button">
				<label for="button"></label>

				<ul>
					<?php

					$menu = Yii::app() -> cache -> get('menu');
					if ($menu === false) {
						$menu = Categories::createMenu();
						Yii::app() -> cache -> set('menu', $menu, 60);
					}
					$this -> widget('application.extensions.mbmenu.MbMenu', array('items' => $menu, ));
					?>
				</ul>
			</nav>
				</div>
				<div class="row">
					<div class="col centerbar">
						<!--  content -->
						<?php echo $content; ?>
						<!-- end content -->
					</div>
					<div class="col col-quarter sidebar">
						<div class="content">
							<?php $this -> widget('NextMovie'); ?>
						</div>
						<div class="content shariff">
							<ul class="orientation-horizontal">
								<li class="shariff-button facebook">
									<a aria-label="Bei Facebook teilen" role="button" title="Bei Facebook teilen" href="https://www.facebook.com/sharer.php?u=http%3A%2F%2Fafk-filmkreis.de%2F">
										<span class="icon-social icon-facebook"></span>
										<span class="share_text">teilen</span>&nbsp;<span class="share_count">940</span>
									</a>
								</li>
								<li class="shariff-button twitter">
									<a aria-label="Bei Twitter teilen" role="button" title="Bei Twitter teilen" href="https://twitter.com/intent/tweet?text=Akademischer%20Filmkreis%20Karlsrue%20e.V.%20-%20http%3A%2F%2Fafk-filmkreis.de%2F">
										<span class="icon-social icon-twitter">
										</span><span class="share_text">tweet</span>
									</a>
								</li>
								<li class="shariff-button whatsapp">
									<a aria-label="Bei Whatsapp teilen" role="button" title="Bei Whatsapp teilen" href="whatsapp://send?text=Akademischer%20Filmkreis%20Karlsrue%20e.V.%20-%20http%3A%2F%2Fafk-filmkreis.de%2F">
										<span class="icon-social icon-whatsapp"></span>
										<span class="share_text">teilen</span>
									</a>
								</li>
							</ul>
					</div>
				</div>
			</main>
		</div>
	</body>
</html>
