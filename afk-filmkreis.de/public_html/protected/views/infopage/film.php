<div class="filminfo">
	<h1><?php echo $model->title; ?></h1>
	<div class="filminfodiverse">
		<?php 	
		echo FilmHelper::createFilmInfoByArray(
		array(
			array('info'=>CHtml::encode($model->country), 'replace'=>'{I} '),
			array('info'=>CHtml::encode($model->year), 'replace'=>'{I}; '),
			array('info'=>CHtml::encode($model->duration), 'replace'=>'{I} min; '),
			array('info'=>CHtml::encode($model->language), 'replace'=>'{I}; <br>'),
			
			array('info'=>CHtml::encode($model->director), 'replace'=>'Regie: {I}; '),
			array('info'=>CHtml::encode($model->book).",".CHtml::encode($model->story), 'replace'=>'Drehbuch: {I}; '),
			array('info'=>CHtml::encode($model->producer), 'replace'=>'Produktion: {I}; '),
			array('info'=>CHtml::encode($model->cinematographer), 'replace'=>'Kamera: {I}; '),
			array('info'=>CHtml::encode($model->design), 'replace'=>'<br> Design: {I}; '),
			array('info'=>CHtml::encode($model->editor), 'replace'=>'Schnitt: {I};'),
			
			array('info'=>CHtml::encode($model->cast), 'replace'=>'<br> Darsteller: {I}; '),	
			array('info'=>CHtml::encode($model->voice), 'replace'=>'Stimmen: {I}; '),
			array('info'=>CHtml::encode($model->misc), 'replace'=>'Sonstiges: {I}; '),
				
				
		));
		?>
	</div>
	<?php if($model->youtube){?>
	<div style="margin-top: 20px; margin-bottom: 20px;"><iframe width="500" height="284" src="http://www.youtube.com/embed/<?php echo $model->youtube; ?>?hd=1" frameborder="0" allowfullscreen></iframe></div>
	<?php }?>
	<div class="filminfotext"><?php echo preg_replace("'<[\/\!]*?[^<>]*?>'si","",$model->beschreibung);?></div>
			<?php 	
		echo FilmHelper::createFilmInfoByArray(
		array(
			array('info'=>CHtml::encode($model->url), 'replace'=>'<a target="_BLANK" href="{I}">Mehr über diesen Film</a><br>'),	
			array('info'=>CHtml::encode($model->imdb), 'replace'=>'<a target="_BLANK" href="http://www.imdb.com/title/{I}">'.$model->title.' in der IMDB</a><br>'),	
		));
		?>
</div>

<?php 
echo (!empty(Yii::app()->request->urlReferrer)) ? (CHtml::link('Zurück',Yii::app()->request->urlReferrer)) : '';
?>

