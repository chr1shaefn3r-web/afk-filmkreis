<?php $str = FilmHelper::getCurrentSemester();
$str = explode(".", $str);
if(strtolower($str[0])=="w"){
	$semester = "Wintersemester ". $str[1] . "/". ($str[1]+1);
}else{
	$semester = "Sommersemester ". $str[1];
}
?>

<h1>Vorstellungen im <?php echo $semester?></h1>
<?php echo $this->renderPartial('_grid', array('model'=>$model,'dataProvider' => $model->getSemester(FilmHelper::getCurrentSemester()))); ?>
