<div class="post">
	<div class="title">
		<?php echo CHtml::link(CHtml::encode($data->title), $data->url);?>
	</div>
	<div class="author">
		<?php //echo ($data->create_time != 0) ?  (date('d.m.Y',$data->create_time)) : ""; ?>
	</div>
	<div class="content">
		<?php
			$this->beginWidget('CMarkdown', array('purifyOutput'=>false));
			echo ContentHelper::parseContent($data->content);
			
			$this->endWidget();
		?>
	</div>
	<div class="nav">
	<?php //echo ($data->update_time != 0) ?  ("Letztes Update am ". date('d.m.Y',$data->update_time)) : ""; ?>
	<?php 
	if(YII_DEBUG){
		echo CHtml::link('bearbeiten','/afk/admin/index.php/post/update?id='.$data->id);
	}
	?>
		
	</div>
</div>
