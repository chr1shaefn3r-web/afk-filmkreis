<?php
$this->breadcrumbs=array(
	'Vorstellungens'=>array('index'),
	$model->id,
);

?>
<div class="filminfo">
	<h1><?php echo $model->title; ?></h1>
	<div class="filminfotext"><?php echo preg_replace("'<[\/\!]*?[^<>]*?>'si","",$model->text);?></div>
</div>
<?php 
echo CHtml::link('Zurück',Yii::app()->request->urlReferrer);
?>
