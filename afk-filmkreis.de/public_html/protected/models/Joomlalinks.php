<?php

/**
 * This is the model class for table "{{joomlalinks}}".
 *
 * The followings are the available columns in table '{{joomlalinks}}':
 * @property integer $id
 * @property string $org_id
 * @property string $newurl
 */
class Joomlalinks extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Joomlalinks the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{joomlalinks}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('org_id, newurl', 'required'),
			array('org_id, newurl', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, org_id, newurl', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'org_id' => 'Org',
			'newurl' => 'Newurl',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('org_id',$this->org_id,true);
		$criteria->compare('newurl',$this->newurl,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function getURL($id){
		$criteria=new CDbCriteria;
		$criteria->select = array('org_id' => $id);
		
		return (Joomlalinks::model()->find($criteria)!=null)?(Joomlalinks::model()->find($criteria)->newurl):(null);
	}
}