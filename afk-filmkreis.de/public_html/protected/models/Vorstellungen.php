<?php

/**
 * This is the model class for table "{{vorstellungen}}".
 *
 * The followings are the available columns in table '{{vorstellungen}}':
 * @property integer $id
 * @property string $date
 * @property integer $film_id
 * @property integer $theme
 * @property string $text
 * @property integer $published
 * @property integer $reihen_id
 */
class Vorstellungen extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Vorstellungen the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{vorstellungen}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		array('text, reihen_id', 'required'),
		array('film_id, published, reihen_id', 'numerical', 'integerOnly'=>true),
		array('date', 'safe'),
		// The following rule is used by search().
		// Please remove those attributes that should not be searched.
		array('id, date, film_id, text, published, reihen_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'film' => array(self::BELONGS_TO, 'Filme', 'film_id'),
			'reihe' => array(self::BELONGS_TO, 'Reihen', 'reihen_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'date' => 'Date',
			'film_id' => 'Film',
			'text' => 'Text',
			'published' => 'Published',
			'reihen_id' => 'Reihen',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('film_id',$this->film_id);
		$criteria->compare('text',$this->text,true);
		$criteria->compare('published',$this->published);
		$criteria->compare('reihen_id',$this->reihen_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public function afterFind(){
			$arr = explode("-",$this->date);
			$this->date = date("d.m.Y",mktime(0,0,0,$arr[1],$arr[2],$arr[0]));
	}


	public function getSemester($ws, $year=-1){
		$year = (int) $year;
		if($year != -1 && !is_int($year)){
			throw new CHttpException(401,'Never do this again!');
		}
		if(strlen($ws)==6){
			$arr = explode(".", $ws);
			$ws = $arr[0];
			$year = $arr[1];
		}
		
		
		if(strtolower($ws)=='s'){
			$start = $year.'-04-01';
			$end = $year.'-09-30';
		}elseif(strtolower($ws)=='w'){
			$start = $year.'-10-01';
			$end = ($year+1).'-03-31';
		}else{
			throw new CHttpException(401,'Never do this again!');
		}


		
		$criteria=new CDbCriteria;
		$criteria->select = array('date', 'film_id', 'text', 'published', 'reihen_id', 'date AS sortdate');
		$criteria->condition = 'date BETWEEN :start AND :end';
		$criteria->params = array(':start'=>$start, ':end'=>$end);
		$criteria->order = '`sortdate`';

		return new CActiveDataProvider($this, array(
		//'criteria'=>$criteria,
		'data'=>$this->model()->with(array('film', 'reihe'))->findAll($criteria)
		));
	}
	
	public function getActSemesterHistory(){
		$year = date('Y');
		$month = date('m');
		$day = date('d');
		if(date('m')>1 and date('m')<8){
			$ws = 's';
		}else{
			$ws = 'w';
		}
		
		if(strtolower($ws)=='s'){
			$start = $year.'-04-01';
			$end = $year.'-'.$month.'-'.$day;
		}elseif(strtolower($ws)=='w'){
			$start = $year.'-10-01';
			$end = $year.'-'.$month.'-'.$day;
		}else{
			throw new CHttpException(401,'Never do this again!');
		}


		
		$criteria=new CDbCriteria;
		$criteria->select = array('date', 'film_id', 'text', 'published', 'reihen_id', 'date AS sortdate');
		$criteria->condition = 'date BETWEEN :start AND :end';
		$criteria->params = array(':start'=>$start, ':end'=>$end);
		$criteria->order = '`sortdate`';

		return new CActiveDataProvider($this, array(
		//'criteria'=>$criteria,
		'data'=>$this->model()->with(array('film', 'reihe'))->findAll($criteria)
		));
	}
	
	public function getActSemester(){
		$year = date('Y');
		$month = date('m');
		$day = date('d');
		if(date('m')>1 and date('m')<8){
			$ws = 's';
		}else{
			$ws = 'w';
		}
		
		if(strtolower($ws)=='s'){
			$start = $year.'-'.$month.'-'.$day;
			$end = $year.'-09-30';
		}elseif(strtolower($ws)=='w'){
			$start = $year.'-'.$month.'-'.$day;
			$end = ($year+1).'-03-31';
		}else{
			throw new CHttpException(401,'Never do this again!');
		}


		
		$criteria=new CDbCriteria;
		$criteria->select = array('date', 'film_id', 'text', 'published', 'reihen_id', 'date AS sortdate');
		$criteria->condition = 'date BETWEEN :start AND :end';
		$criteria->params = array(':start'=>$start, ':end'=>$end);
		$criteria->order = '`sortdate`';

		return new CActiveDataProvider($this, array(
		//'criteria'=>$criteria,
		'data'=>$this->model()->with(array('film', 'reihe'))->findAll($criteria)
		));
	}

	
	public static function getNextVorstellung(){
		$criteria=new CDbCriteria;
		$criteria->addCondition('date >= \''. (date('Y-m-d')) . '\'');
		$criteria->addCondition('t.published = \'1\'');
		$criteria->limit = 1;
		$criteria->order = 'date';


		return Vorstellungen::model()->with(array('film'))->find($criteria);
	}
}