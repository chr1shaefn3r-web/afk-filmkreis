<?php

/**
 * This is the model class for table "{{filme}}".
 *
 * The followings are the available columns in table '{{filme}}':
 * @property integer $id
 * @property string $title
 * @property string $beschreibung
 * @property string $country
 * @property string $language
 * @property string $year
 * @property integer $duration
 * @property string $producer
 * @property string $director
 * @property string $editor
 * @property string $book
 * @property string $story
 * @property string $music
 * @property string $cinematographer
 * @property string $cast
 * @property string $voice
 * @property string $design
 * @property string $misc
 * @property string $url
 * @property string $imdb
 * @property integer $published
 * @property string $youtube
 */
class Filme extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Filme the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{filme}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, beschreibung, country, language, year, producer, director, editor, book, story, music, cinematographer, cast, voice, design, misc, url, imdb, youtube', 'required'),
			array('duration, published', 'numerical', 'integerOnly'=>true),
			array('title, producer, director, editor, book, story, music, cinematographer, voice, design, misc, url, imdb, youtube', 'length', 'max'=>255),
			array('country, language', 'length', 'max'=>10),
			array('year', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, beschreibung, country, language, year, duration, producer, director, editor, book, story, music, cinematographer, cast, voice, design, misc, url, imdb, published, youtube', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'vorstellungen'=>array(self::HAS_ONE, 'Vorstellungen', 'film_id')
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'beschreibung' => 'Beschreibung',
			'country' => 'Country',
			'language' => 'Language',
			'year' => 'Year',
			'duration' => 'Duration',
			'producer' => 'Producer',
			'director' => 'Director',
			'editor' => 'Editor',
			'book' => 'Book',
			'story' => 'Story',
			'music' => 'Music',
			'cinematographer' => 'Cinematographer',
			'cast' => 'Cast',
			'voice' => 'Voice',
			'design' => 'Design',
			'misc' => 'Misc',
			'url' => 'Url',
			'imdb' => 'Imdb',
			'published' => 'Published',
			'youtube' => 'Youtube',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('beschreibung',$this->beschreibung,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('year',$this->year,true);
		$criteria->compare('duration',$this->duration);
		$criteria->compare('producer',$this->producer,true);
		$criteria->compare('director',$this->director,true);
		$criteria->compare('editor',$this->editor,true);
		$criteria->compare('book',$this->book,true);
		$criteria->compare('story',$this->story,true);
		$criteria->compare('music',$this->music,true);
		$criteria->compare('cinematographer',$this->cinematographer,true);
		$criteria->compare('cast',$this->cast,true);
		$criteria->compare('voice',$this->voice,true);
		$criteria->compare('design',$this->design,true);
		$criteria->compare('misc',$this->misc,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('imdb',$this->imdb,true);
		$criteria->compare('published',$this->published);
		$criteria->compare('youtube',$this->youtube,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	public static function getName($id)
	{
		$criteria = new CDbCriteria;
		$criteria->condition = "id = :id";
		$criteria->params = array(":id"=> $id);
		$criteria->limit = 1;
		return Filme::model()->find($criteria)->title;

	}
}