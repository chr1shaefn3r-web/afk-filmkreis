<?php

/**
 * This is the model class for table "{{categories}}".
 *
 * The followings are the available columns in table '{{categories}}':
 * @property integer $id
 * @property integer $parent_id
 * @property string $name
 * @property string $description
 * @property string $url
 * @property integer $active
 * @property integer $orderby
 */
class Categories extends CActiveRecord
{
	private static $_menu = array();
	/**
	 * Returns the static model of the specified AR class.
	 * @return Categories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{categories}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
		array('parent_id, name, description, url, active, orderby', 'required'),
		array('parent_id, active, orderby', 'numerical', 'integerOnly'=>true),
		array('name, url', 'length', 'max'=>255),
		// The following rule is used by search().
		// Please remove those attributes that should not be searched.
		array('id, parent_id, name, description, url, active, orderby', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'getparent' => array(self::BELONGS_TO, 'Categories', 'parent_id'),
      		'childs' => array(self::HAS_MANY, 'Categories', 'parent_id', 'order' => 'orderby ASC', 'condition' => 'active=1'),
      		'posts' =>  array(self::HAS_MANY, 'Post', 'categorie_id', 'condition' => 'status=2'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'parent_id' => 'Parent',
			'name' => 'Name',
			'description' => 'Description',
			'url' => 'Url',
			'active' => 'Active',
			'orderby' => 'Orderby',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('orderby',$this->orderby);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function createMenu($id = 0){
		$array = array();
		$root = Categories::model()->findByPk($id);

		foreach ($root->childs as $child) {
			$flag = false;
			if(strtolower(substr($child->url,0,4))=="http"){
				$url = $child->url;
			}elseif($child->url!= ""){
				$url = array($child->url);
			} else{
				$url = array('posti/'.$child->id);
				if($child->id != 1){
					$flag = true;
				}
				
			}
				
			if(count($child->childs)>0){
				$add = array ('label' => $child->name, 'url'=>$url, 'items' => Categories::createMenu($child->id));
			}else{
				if($flag && count($child->posts) > 0){
					$add = array ('label' => $child->name, 'url'=>$url, 'items' => Categories::createPostMenu($child->posts));
				}else{
					$add = array ('label' => $child->name, 'url'=>$url);
				}
			}

				
			$array[] = $add;
		}

		return $array;

	}
	
	private static function createPostMenu($posts){
		//print_r($posts);
		$array = array();
		foreach ($posts as $post) {
			$array[] = array ('label' => $post->title, 'url'=>array('post/'.$post->id.'/'.ContentHelper::parseURLCreate($post->title)));
		}
		
		return $array;
	}

}