<?php

class GuestbookController extends Controller {
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout = '//layouts/column2';

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id) {
		$this -> render('view', array('model' => $this -> loadModel($id), ));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate() {
		$model = new Guestbook;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$model -> scenario = 'entrywcaptcha';

		if (isset($_POST['Guestbook']) && empty($_POST['mail'])) {
			$model -> attributes = $_POST['Guestbook'];
			$model -> iphash = md5(Yii::app() -> request -> userHostAddress);

			if (strpos($_POST['Guestbook']['Text'], "href") !== false) {
				$model -> public = 0;
			} else {
				$model -> public = 1;
			}

			if ($model -> validate()) {
				$model -> scenario = NULL;
				Yii::log("Neuer Gästebucheintrag", 'guestbook', 'guestbook');
				if ($model -> save(false))
					$this -> redirect(array('index'));
			}

		}

		$this -> render('create', array('model' => $model, ));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex() {
		$dataProvider = new CActiveDataProvider('Guestbook', array('criteria' => array('condition'=>'public=1','order' => 'timestamp DESC', ), ));
		$this -> render('index', array('dataProvider' => $dataProvider, ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id) {
		$model = Guestbook::model() -> findByPk($id);
		if ($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model) {
		if (isset($_POST['ajax']) && $_POST['ajax'] === 'guestbook-form') {
			echo CActiveForm::validate($model);
			Yii::app() -> end();
		}
	}

}
