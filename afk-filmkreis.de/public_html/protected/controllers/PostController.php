<?php

class PostController extends Controller
{
	public $layout='column2';

	/**
	 * @var CActiveRecord the currently loaded data model instance.
	 */
	private $_model;
	

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}


	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
		array('allow',  // allow all users to access 'index' and 'view' actions.
				'actions'=>array('index','view'),
				'users'=>array('*'),
		),
		array('allow', // allow authenticated users to access all actions
				'users'=>array('@'),
		),
		array('deny',  // deny all users
				'users'=>array('*'),
		),
		);
	}

	/**
	 * Displays a particular model.
	 */
	public function actionView()
	{
		$post=$this->loadModel();

		$this->render('view',array(
			'model'=>$post
		));
	}


	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		if(isset($_GET['id']))
		{
			$criteria=new CDbCriteria(array(
			'condition'=>'status='.Post::STATUS_PUBLISHED." AND categorie_id=" . $_GET['id'],
			'order'=>'id DESC',
			));


		}
		else{
			$criteria=new CDbCriteria(array(
			'condition'=>'status='.Post::STATUS_PUBLISHED." AND categorie_id=1",
			'order'=>'id DESC',
			));	
		}


		$dataProvider=new CActiveDataProvider('Post', array(
			'pagination'=>array(
				'pageSize'=>Yii::app()->params['postsPerPage'],
		),
			'criteria'=>$criteria,
		));

		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));

	}


	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 */
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
			{
	
				$this->_model=Post::model()->findByPk($_GET['id']);
			}
			if($this->_model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		}
		return $this->_model;
	}

}
