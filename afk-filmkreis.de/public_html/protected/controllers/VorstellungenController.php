<?php

class VorstellungenController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}



	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Vorstellungen');
		$this->render('index',array(
			'model'=>new Vorstellungen(),
		));
	}


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionAjax()
	{
		$arr = explode(".", $_POST['Vorstellungen']['reihen_id']);
		$model = new Vorstellungen();
		$this->renderPartial('_grid',array('model'=>$model,'dataProvider' => $model->getSemester($arr[0],$arr[1])));

	}


	/** * Returns the data model based on the primary key given in the GET
	 variable. * If the data model is not found, an HTTP exception will be
	 raised. * @param integer the ID of the model to be loaded */
	public function loadModel($id) {
		$model=Vorstellungen::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**  * Performs the AJAX validation. *
	@param CModel the model to be validated */
	protected function performAjaxValidation($model) {
		if(isset($_POST['ajax']) && $_POST['ajax']==='vorstellungen-form') {
			echo CActiveForm::validate($model); Yii::app()->end();
		}
	}

}
