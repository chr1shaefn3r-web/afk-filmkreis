<?php

class InfoPageController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/infopage';



	/**
	 * Lists all models.
	 */
	public function actionProgramm()
	{
		$dataProvider=new CActiveDataProvider('Vorstellungen');
		$this->render('index',array(
			'model'=>new Vorstellungen(),
		));
	}
	
	public function actionFilm($id)
	{
		$this->render('film',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	public function loadModel($id) {
		$model=Filme::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}


}
